CREATE OR REPLACE PACKAGE IPJ.PKG_REPORTE_PROCESO AS
  /*
   ORA-02292 integrity constraint (string.string) violated - child record found
   Cause: An attempt was made to delete a row that is referenced by a foreign key.
  */

  /* invalid number*/
  V_ERROR_01722 EXCEPTION;
  PRAGMA EXCEPTION_INIT(V_ERROR_01722, -01722);

  V_ERROR_2292 EXCEPTION;
  PRAGMA EXCEPTION_INIT(V_ERROR_2292, -2292);

  /*
  ORA-00001 unique constraint (string.string) violated
  Cause: An UPDATE or INSERT statement attempted to insert a duplicate key.
  */
  V_ERROR_00001 EXCEPTION;
  PRAGMA EXCEPTION_INIT(V_ERROR_00001, -00001);

  /*
  ORA-01407 cannot update (string) to NULL
  Cause: An attempt was made to update a table column USER.TABLE.COLUMN with a NULL val
  */
  V_ERROR_01407 EXCEPTION;
  PRAGMA EXCEPTION_INIT(V_ERROR_01407, -01407);

  TYPE TY_CURSOR IS REF CURSOR;

  PROCEDURE INS_TRAMITES_INGRESADOS;

  PROCEDURE INS_TRAMITES_PARA_FIRMA;

  PROCEDURE TABLERO_DE_CONTROL_SUAC_vs_GESTION_2018;

  PROCEDURE TABLERO_ACCIONES_EXISTENTES_GESTION_ANTES_2018;

  PROCEDURE TABLERO_Tramites_Acciones_Sistema_Gestion;

    PROCEDURE INFO_EQUIDAD_GENERO;


END PKG_REPORTE_PROCESO;
/

CREATE OR REPLACE PACKAGE BODY IPJ.PKG_REPORTE_PROCESO AS

  PROCEDURE INS_TRAMITES_INGRESADOS IS
  
  BEGIN
    EXECUTE IMMEDIATE 'ALTER SESSION SET NLS_DATE_FORMAT=' || '''' ||
                      'DD/MM/YYYY' || '''';
    EXECUTE IMMEDIATE 'ALTER SESSION SET NLS_NUMERIC_CHARACTERS='',.''';
  
    INSERT INTO IPJ.T_TRAMITES_INGRESADOS TI
      (id_tramites_ingresados,
       fecha_ejecucion,
       area,
       expedientE,
       sticker,
       tipo_tramite,
       asunto,
       ini_suac,
       FECHA_FIN)
    
      (SELECT IPJ.SEQ_TRAMITES_INGRESADOS.NEXTVAL, SYSDATE, V.*
         FROM (SELECT (SELECT N_UBICACION
                         FROM IPJ.T_UBICACIONES U
                        WHERE U.ID_UBICACION = TR.ID_UBICACION_ORIGEN) AREA,
                      TR.EXPEDIENTE,
                      TR.STICKER,
                      (SELECT N_TIPO_TRAMITE_OL
                         FROM IPJ.T_TIPOS_TRAMITES_OL TT
                        WHERE TT.ID_TIPO_TRAMITE_OL = O.ID_TIPO_TRAMITE_OL) TIPO_TRAMITE,
                      IPJ.TRAMITES_SUAC.FC_BUSCAR_ASUNTO_SUAC(TR.ID_TRAMITE) ASUNTO,
                      TO_CHAR(TR.FECHA_INI_SUAC, 'DD/MM/RRRR') INI_SUAC,
                      O.FECHA_FIN
                 FROM IPJ.T_OL_ENTIDADES O
                 JOIN IPJ.T_TRAMITESIPJ TR
                   ON O.CODIGO_ONLINE = TR.CODIGO_ONLINE
                WHERE O.ID_TIPO_TRAMITE_OL NOT IN (4, 5, 6, 8, 7, 9)
                  AND TRUNC(FECHA_FIN) <= TRUNC(SYSDATE - 1)
                ORDER BY TR.ID_UBICACION_ORIGEN, O.ID_TIPO_TRAMITE_OL) V);
  
  EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
  END INS_TRAMITES_INGRESADOS;

  PROCEDURE INS_TRAMITES_PARA_FIRMA IS
  
  BEGIN
    EXECUTE IMMEDIATE 'ALTER SESSION SET NLS_DATE_FORMAT=' || '''' ||
                      'DD/MM/YYYY' || '''';
    EXECUTE IMMEDIATE 'ALTER SESSION SET NLS_NUMERIC_CHARACTERS='',.''';
  
    INSERT INTO IPJ.T_TRAMITES_PARA_FIRMA TI
      (id_tramites_para_firma,
       fecha_ejecucion,
       area,
       expediente,
       sticker,
       asunto_suac,
       tipo_accion,
       ini_suac,
       paso_firma)
    
      (SELECT IPJ.SEQ_TRAMITES_PARA_FIRMA.NEXTVAL, SYSDATE, V.*
         FROM (Select (select n_ubicacion
                         from ipj.t_ubicaciones u
                        where u.id_ubicacion = t.id_ubicacion_origen) Area,
                      t.expediente,
                      t.sticker,
                      IPJ.TRAMITES_SUAC.FC_BUSCAR_ASUNTO_SUAC(t.id_tramite) Asunto_Suac,
                      (select TA.N_TIPO_ACCION
                         from ipj.t_tipos_accionesipj ta
                        where ta.id_tipo_accion = a.id_tipo_accion) Tipo_Accion,
                      to_char(t.fecha_ini_suac, 'dd/mm/rrrr') Ini_Suac,
                      to_char((select max(fecha_pase)
                                from ipj.t_tramitesipj_acciones_estado ae
                               where ae.id_tramite_ipj = a.id_tramite_ipj
                                 and ae.id_tramiteipj_accion =
                                     a.id_tramiteipj_accion),
                              'dd/mm/rrrr') Paso_Firma
                 from IPJ.T_TRAMITESIPJ_ACCIONES a
                 join ipj.t_tramitesipj t
                   on a.id_tramite_ipj = t.id_tramite_ipj
                where a.id_estado = 10
                order by id_ubicacion_origen) V);
  
  EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
  END INS_TRAMITES_PARA_FIRMA;

  --TABLERO 3
  PROCEDURE TABLERO_DE_CONTROL_SUAC_vs_GESTION_2018 IS
  
  BEGIN
    EXECUTE IMMEDIATE 'ALTER SESSION SET NLS_DATE_FORMAT=' || '''' ||
                      'DD/MM/YYYY' || '''';
    EXECUTE IMMEDIATE 'ALTER SESSION SET NLS_NUMERIC_CHARACTERS='',.''';
  
    INSERT INTO IPJ.T_CONTROL_SUAC_VS_GESTION_2018
      (ID_CTROL_SUAC_VS_GESTION_2018,
       FECHA_EJECUCION,
       UNIDAD_SUAC,
       AREA_GESTION,
       TIPO_SUAC,
       SUBTIPO_SUAC,
       ASUNTO,
       STICKER,
       EXPEDIENTE,
       CREACION_SUAC,
       INICIO_SUAC,
       INICIO_GESTION,
       CLASIFICACION,
       CANT_DIAS_ESPERA,
       N_ESTADO,
       ESTADO_GESTION,
       USUARIO_SUAC,
       USUARIO_GESTION,
       ACCIONES_GESTION,
       ARCHIVO_SUAC,
       FIN_GESTION,
       CANT_DIAS_SUAC,
       CANT_NOTAS_CERRADAS,
       CANT_NOTAS_PEND,
       MIN_FEC_NOTA,
       MAX_FEC_NOTA,
       CANT_DIAS_GESTION,
       USUARIOS_TRAMITE,
       LEGAJO,
       DENOMINACION
       
       )
    
      (SELECT IPJ.SEQ_SUAC_VS_GESTION_2018.NEXTVAL, SYSDATE, V.*
         FROM (select trim(vsuac.unidad_actual) Unidad_Suac,
                      (select n_ubicacion
                         from ipj.t_ubicaciones u
                        where u.id_ubicacion = tr.id_ubicacion) Area_Gestion,
                      (select N_TIPO_TRAMITE
                         from nuevosuac.vt_subtipos_tramite s
                        where s.id_subtipo_tramite = vsuac.id_subtipo_tramite) Tipo_SUAC,
                      vsuac.n_subtipo_tramite Subtipo_SUAC,
                      trim(vsuac.asunto) asunto,
                      vsuac.nro_sticker Sticker,
                      vsuac.nro_tramite Expediente,
                      trunc(vsuac.fecha_creacion) Creacion_Suac,
                      trunc(vsuac.fecha_inicio) Inicio_Suac,
                      trunc(tr.fecha_inicio) Inicio_Gestion,
                      (select n_clasif_ipj
                         from ipj.t_tipos_clasif_ipj tc
                        where tc.id_clasif_ipj = tr.id_clasif_ipj) Clasificacion,
                      IPJ.VARIOS.FC_Dias_Laborables(trunc(vsuac.fecha_inicio),
                                                    trunc(nvl(tr.fecha_inicio,
                                                              sysdate)),
                                                    'N') Cant_Dias_Espera,
                      vsuac.n_estado,
                      nvl((select n_estado
                            from ipj.t_estados e
                           where e.id_estado = tr.id_estado_ult),
                          'NO REGISTRADO') Estado_Gestion,
                      nvl((select descripcion
                            from ipj.t_usuarios us
                           where 'd0' || substr(us.cuil_usuario, 3, 8) =
                                 vsuac.id_usuario_actual),
                          'NO REGISTRADO (' || vsuac.id_usuario_actual || ')') Usuario_SUAC,
                      nvl((select descripcion
                            from ipj.t_usuarios us
                           where us.cuil_usuario = tr.cuil_ult_estado),
                          (select n_grupo
                             from ipj.t_grupos g
                            where g.id_grupo = tr.id_grupo_ult_estado)) Usuario_Gestion,
                      (select distinct listagg(ta.n_tipo_accion, '; ') within group(order by ta.n_tipo_accion) over(partition by tra.id_tramite_ipj) lista_nombres
                         from ipj.t_tramitesipj_acciones tra
                         join ipj.t_tipos_accionesipj ta
                           on ta.id_tipo_accion = tra.id_tipo_accion
                        where id_tramite_ipj = tr.id_tramite_ipj) Acciones_Gestion,
                      trunc(vsuac.fecha_archivo) Archivo_Suac,
                      to_date((select max(fecha_pase)
                                from IPJ.t_tramites_ipj_estado te
                               where te.Id_Tramite_Ipj = tr.id_tramite_ipj),
                              'dd/mm/rrrr') Fin_Gestion,
                      IPJ.VARIOS.FC_Dias_Laborables(trunc(vsuac.fecha_creacion),
                                                    trunc(nvl(vsuac.fecha_archivo,
                                                              sysdate)),
                                                    'N') Cant_Dias_SUAC,
                      (select count(1)
                         from ipj.t_entidades_notas n
                        where n.id_tramite_ipj = tr.id_tramite_ipj
                          and N.ID_ESTADO_NOTA in (1, 3)) Cant_Notas_Cerradas,
                      (select count(1)
                         from ipj.t_entidades_notas n
                        where n.id_tramite_ipj = tr.id_tramite_ipj
                          and N.ID_ESTADO_NOTA not in (1, 3)) Cant_Notas_Pend,
                      trunc((select min(n.fec_modif)
                              from ipj.t_entidades_notas n
                             where n.id_tramite_ipj = tr.id_tramite_ipj)) Min_Fec_Nota,
                      trunc((select max(nvl(n.fec_cumpl, n.fec_modif))
                              from ipj.t_entidades_notas n
                             where n.id_tramite_ipj = tr.id_tramite_ipj)) Max_Fec_Nota,
                      IPJ.VARIOS.FC_Dias_Laborables(trunc(tr.fecha_inicio),
                                                    trunc(nvl(to_date((select max(fecha_pase)
                                                                        from IPJ.t_tramites_ipj_estado te
                                                                       where te.Id_Tramite_Ipj =
                                                                             tr.id_tramite_ipj),
                                                                      'dd/mm/rrrr'),
                                                              sysdate)),
                                                    'N') Cant_Dias_GESTION,
                      (select distinct listagg(u.descripcion, '; ') within group(order by u.descripcion) over(partition by tra.id_tramite_ipj) lista_nombres
                         from ipj.T_Tramites_Ipj_Estado tra
                         join ipj.t_usuarios u
                           on tra.cuil_usuario = u.cuil_usuario
                        where tr.id_tramite_ipj = tra.id_tramite_ipj) Usuarios_Tramite,
                      
                      (select distinct listagg(l.id_legajo, '; ') within group(order by l.id_legajo) over(partition by tr.id_tramite_ipj) lista_nombres
                         from ipj.t_entidades en
                         join ipj.t_legajos l
                           on l.id_legajo = en.id_legajo
                        where en.id_tramite_ipj = TR.ID_TRAMITE_IPJ) LEGAJO,
                      
                      (select distinct listagg(l.denominacion_sia, '; ') within group(order by l.denominacion_sia) over(partition by tr.id_tramite_ipj) lista_nombres
                         from ipj.t_entidades en
                         join ipj.t_legajos l
                           on l.id_legajo = en.id_legajo
                        where en.id_tramite_ipj = TR.ID_TRAMITE_IPJ) DENOMINACION
               
                 from NUEVOSUAC.VT_TRAMITES_IPJ_CRUCE_SGES vsuac
                 left join ipj.t_tramitesipj tr
                   on vsuac.id_tramite = tr.id_tramite
                where
               --vsuac.fecha_creacion between to_date('01/01/2018', 'dd/mm/rrrr')and to_date ('31/12/2018','dd/mm/rrrr')
                vsuac.fecha_creacion >= to_date('01/01/2018', 'dd/mm/rrrr')
               
               ) V);
  
    COMMIT;
  
  EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
  END TABLERO_DE_CONTROL_SUAC_vs_GESTION_2018;

  -- TABLERO 1 
  PROCEDURE TABLERO_ACCIONES_EXISTENTES_GESTION_ANTES_2018 IS
  
  BEGIN
    EXECUTE IMMEDIATE 'ALTER SESSION SET NLS_DATE_FORMAT=' || '''' ||
                      'DD/MM/YYYY' || '''';
    EXECUTE IMMEDIATE 'ALTER SESSION SET NLS_NUMERIC_CHARACTERS='',.''';
  
    INSERT INTO IPJ.T_Tablero_Acciones_Antes_2018
      (id_tab_acciones_antes_2018,
       fecha_ejecucion,
       id_tramite_ipj,
       id_tramiteipj_accion,
       expediente,
       sticker,
       fecha_inicio,
       fecha_ini_suac,
       tipo_suac,
       n_subtipo_tramite,
       estado_tramite,
       n_tipo_accion,
       area_accion,
       estado_accion,
       usuario,
       fec_ini_acc,
       est_ini_acc,
       fec_fin_acc,
       est_fin_acc,
       matricula,
       user_ing,
       fec_ing,
       user_act,
       fec_act)
    
      (SELECT IPJ.SEQ_Acciones_Antes_2018.NEXTVAL,
              SYSDATE FECHA_EJECUCION,
              V.*
         FROM (select tr.id_tramite_ipj,
                      ac.id_tramiteipj_accion,
                      (case
                        when nvl(tr.id_tramite, 0) > 0 then
                         tr.expediente
                        else
                         nvl(tr.expediente, 'Histórico')
                      end) expediente,
                      (case
                        when nvl(tr.id_tramite, 0) > 0 then
                         tr.sticker
                        else
                         'Histórico'
                      end) sticker,
                      tr.fecha_inicio,
                      tr.fecha_ini_suac,
                      (SELECT ttip.n_tipo_tramite
                         FROM nuevosuac.t_tramites         tsua,
                              nuevosuac.t_subtipos_tramite sbt,
                              nuevosuac.t_tipos_tramite    ttip
                        WHERE tsua.id_tramite = tr.id_tramite
                          AND tsua.id_subtipo_tramite =
                              sbt.id_subtipo_tramite
                          AND sbt.id_tipo_tramite = ttip.id_tipo_tramite) tipo_suac,
                      (SELECT sbts.n_subtipo_tramite
                         FROM nuevosuac.t_tramites         tsuas,
                              nuevosuac.t_subtipos_tramite sbts
                        WHERE tsuas.id_tramite = tr.id_tramite
                          AND tsuas.id_subtipo_tramite =
                              sbts.id_subtipo_tramite) n_subtipo_tramite,
                      (select n_estado
                         from ipj.t_estados e
                        where e.id_estado = tr.id_estado_ult) Estado_tramite,
                      TA.N_TIPO_ACCION,
                      (select n_ubicacion
                         from ipj.t_ubicaciones u
                        where u.id_ubicacion = tc.id_ubicacion) Area_Accion,
                      (select n_estado
                         from ipj.t_estados e
                        where e.id_estado = ac.id_estado) Estado_Accion,
                      (select descripcion
                         from ipj.t_usuarios us
                        where us.cuil_usuario = ac.cuil_usuario) Usuario,
                      (select fecha_pase
                         from ipj.t_tramitesipj_acciones_estado ae
                        where ae.id_pases_acciones_ipj =
                              IPJ.TRAMITES.FC_Buscar_Historial_Acc(tr.id_tramite_ipj,
                                                                   ac.id_tramiteipj_accion,
                                                                   'S',
                                                                   null,
                                                                   null)) Fec_Ini_Acc,
                      (select e.n_estado
                         from ipj.t_tramitesipj_acciones_estado ae
                         join ipj.t_estados e
                           on ae.id_estado = e.id_estado
                        where ae.id_pases_acciones_ipj =
                              IPJ.TRAMITES.FC_Buscar_Historial_Acc(tr.id_tramite_ipj,
                                                                   ac.id_tramiteipj_accion,
                                                                   'S',
                                                                   null,
                                                                   null)) Est_Ini_Acc,
                      (select fecha_pase
                         from ipj.t_tramitesipj_acciones_estado ae
                        where ae.id_pases_acciones_ipj =
                              IPJ.TRAMITES.FC_Buscar_Historial_Acc(tr.id_tramite_ipj,
                                                                   ac.id_tramiteipj_accion,
                                                                   'N',
                                                                   null,
                                                                   null)) Fec_Fin_Acc,
                      (select e.n_estado
                         from ipj.t_tramitesipj_acciones_estado ae
                         join ipj.t_estados e
                           on ae.id_estado = e.id_estado
                        where ae.id_pases_acciones_ipj =
                              IPJ.TRAMITES.FC_Buscar_Historial_Acc(tr.id_tramite_ipj,
                                                                   ac.id_tramiteipj_accion,
                                                                   'N',
                                                                   null,
                                                                   null)) Est_Fin_Acc,
                      (select nro_ficha
                         from ipj.t_legajos l
                        where l.id_legajo = ac.id_legajo) Matricula,
                      USER USER_ING,
                      SYSDATE FEC_ING,
                      USER USER_ACT,
                      SYSDATE FEC_ACT
                 from ipj.t_tramitesipj tr
                 join ipj.t_tramitesipj_acciones ac
                   on tr.id_tramite_ipj = ac.id_tramite_ipj
                 join ipj.t_tipos_accionesipj ta
                   on ac.id_tipo_accion = ta.id_tipo_accion
                 join ipj.t_tipos_clasif_ipj tc
                   on ta.id_clasif_ipj = tc.id_clasif_ipj
               
                where nvl(tr.fecha_ini_suac, tr.fecha_inicio) <
                      to_date('01/01/2018', 'dd/mm/rrrr')
                order by nvl(tr.fecha_ini_suac, tr.fecha_inicio)
               
               ) V);
    COMMIT;
  
  EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
  END TABLERO_ACCIONES_EXISTENTES_GESTION_ANTES_2018;

  --TABLEREO 7
  PROCEDURE TABLERO_Tramites_Acciones_Sistema_Gestion IS
  
  BEGIN
    EXECUTE IMMEDIATE 'ALTER SESSION SET NLS_DATE_FORMAT=' || '''' ||
                      'DD/MM/YYYY' || '''';
    EXECUTE IMMEDIATE 'ALTER SESSION SET NLS_NUMERIC_CHARACTERS='',.''';
  
    INSERT INTO IPJ.T_TABLERO_TRAMITE_ACCIONES_GESTION TI
      (ID_TRAMITE_ACCION_GES,
       FECHA_EJECUCION,
       BOTONES,
       CLASE,
       ID_TRAMITE_SUAC,
       STICKER,
       FECHA_INICIO_SUAC,
       FECHA_RECEPCION,
       TIPO,
       ASUNTO,
       NRO_EXPEDIENTE,
       Id_Tramite_Ipj,
       id_clasif_ipj,
       id_tipo_accion,
       Observacion,
       id_estado,
       cuil_usuario,
       n_estado,
       Tipo_IPJ,
       Fecha_Asignacion,
       Acc_Abiertas,
       Acc_Cerradas,
       URGENTE,
       usuario,
       Error_Dato,
       CUIT,
       razon_social,
       id_legajo,
       N_UBICACION,
       CUIL_CREADOR,
       id_ubicacion,
       id_tramiteipj_accion,
       id_protocolo,
       ID_INTEGRANTE,
       NRO_DOCUMENTO,
       Cuit_PersJur,
       Obs_Rubrica,
       identificacion,
       persona,
       ID_FONDO_COMERCIO,
       id_proceso,
       id_pagina,
       cuil_usuario_old,
       ID_DOCUMENTO,
       N_DOCUMENTO,
       simple_tramite,
       Agrupable,
       codigo_online,
       CONST_SA,
       ID_OBJ_SOCIAL,
       ID_TRAMITE_IPJ_PADRE,
       ID_SUBTIPO_TRAMITE,
       DOM_ELECTRONICO,
       ES_DIGITAL,
       USUARIOS_TRAMITE,
       AREA_ACTUAL,
       AREA_ORIGEN,
       CANT_NOTAS_CERRADAS,
       CANT_NOTAS_PEND,
       ESTADO_SUAC
       
       )
    
      (SELECT IPJ.SEQ_TRAMITE_ACCIONES_GESTION.NEXTVAL, SYSDATE, V.*
         FROM (
               
               select distinct tmp.botones,
                                tmp.Clase,
                                tmp.id_tramite Id_Tramite_Suac,
                                tmp.Sticker,
                                tmp.fecha_ini_suac Fecha_Inicio_Suac,
                                null Fecha_recepcion,
                                (case tmp.id_proceso
                                  when 4 then
                                   'GENERACION DE HISTORICOS - ' ||
                                   tmp.N_UBICACION
                                  when 5 then
                                   'CARGA DE HISTORICOS'
                                  else
                                   IPJ.TRAMITES_SUAC.FC_BUSCAR_SUBTIPO_SUAC(tmp.id_tramite)
                                end) Tipo,
                                (case tmp.id_proceso
                                  when 4 then
                                   'GENERACION DE HISTORICOS - ' ||
                                   tmp.N_UBICACION
                                  when 5 then
                                   tmp.observacion
                                  else
                                   IPJ.TRAMITES_SUAC.FC_BUSCAR_ASUNTO_SUAC(tmp.id_tramite)
                                end) Asunto,
                                
                                tmp.Expediente Nro_Expediente,
                                tmp.Id_Tramite_Ipj,
                                tmp.id_clasif_ipj,
                                tmp.id_tipo_accion,
                                tmp.Observacion,
                                tmp.id_estado,
                                tmp.cuil_usuario,
                                tmp.n_estado,
                                tmp.Tipo_IPJ,
                                tmp.Fecha_Asignacion,
                                tmp.Acc_Abiertas,
                                tmp.Acc_Cerradas,
                                tmp.URGENTE,
                                tmp.usuario,
                                nvl(tmp.Error_Dato, 'N') Error_Dato,
                                tmp.CUIT,
                                tmp.razon_social,
                                tmp.id_legajo,
                                tmp.N_UBICACION,
                                tmp.CUIL_CREADOR,
                                tmp.id_ubicacion,
                                tmp.id_tramiteipj_accion,
                                tmp.id_protocolo,
                                tmp.ID_INTEGRANTE,
                                tmp.NRO_DOCUMENTO,
                                tmp.Cuit_PersJur,
                                tmp.Obs_Rubrica,
                                tmp.identificacion,
                                tmp.persona,
                                tmp.ID_FONDO_COMERCIO,
                                tmp.id_proceso,
                                tmp.id_pagina,
                                tmp.cuil_usuario cuil_usuario_old,
                                tmp.ID_DOCUMENTO,
                                tmp.N_DOCUMENTO,
                                tmp.simple_tramite,
                                tmp.Agrupable,
                                tmp.codigo_online,
                                
                                (select count(1)
                                   from ipj.t_ol_entidades eo
                                  where eo.codigo_online = tmp.codigo_online
                                    and ((eo.id_tipo_tramite_ol = 1 and
                                        eo.id_sub_tramite_ol = 1) or
                                        (eo.id_tipo_tramite_ol = 20 and
                                        eo.id_sub_tramite_ol = 5) or
                                        eo.id_tipo_tramite_ol = 19)) Const_SA,
                                
                                (select eo.id_obj_social
                                   from ipj.t_ol_entidades eo
                                  where eo.codigo_online = tmp.codigo_online) id_Obj_Social,
                                
                                tmp.id_tramite_ipj_padre,
                                
                                IPJ.TRAMITES.FC_N_Subtipo_Suac(IPJ.TRAMITES_SUAC.FC_BUSCAR_SUBTIPO_SUAC(tmp.id_tramite),
                                                               tmp.id_tramite) id_subtipo_tramite,
                                (select count(1)
                                   from IPJ.T_ENTIDADES_AUTORIZADOS ea
                                  where ea.id_tramite_ipj = tmp.id_tramite_ipj
                                    and nvl(ea.mail_autorizado, 'N') = 'S') dom_electronico,
                                
                                IPJ.TRAMITES.FC_Es_Tram_Digital(tmp.id_tramite_ipj) Es_Digital,
                                
                                (select distinct listagg(u.descripcion, '; ') within group(order by u.descripcion) over(partition by tra.id_tramite_ipj) lista_nombres
                                   from ipj.T_Tramites_Ipj_Estado tra
                                   join ipj.t_usuarios u
                                     on tra.cuil_usuario = u.cuil_usuario
                                  where tmp.id_tramite_ipj = tra.id_tramite_ipj) Usuarios_Tramite,
                                
                                (select u.n_ubicacion
                                   from ipj.t_ubicaciones u
                                  where u.id_ubicacion = tmp.id_ubicacion) Area_Actual,
                                
                                tmp.N_ubicacion_origen Area_Origen,
                                
                                (select count(1)
                                   from ipj.t_entidades_notas n
                                  where n.id_tramite_ipj = tmp.id_tramite_ipj
                                    and N.ID_ESTADO_NOTA in (1, 3)) Cant_Notas_Cerradas,
                                
                                (select count(1)
                                   from ipj.t_entidades_notas n
                                  where n.id_tramite_ipj = tmp.id_tramite_ipj
                                    and N.ID_ESTADO_NOTA not in (1, 3)) Cant_Notas_Pend,
                                
                                (select n_estado
                                   from NUEVOSUAC.VT_TRAMITES_IPJ_CRUCE_SGES s
                                  where s.id_tramite = tmp.id_tramite) Estado_SUAC
               
                 from (
                        
                        select *
                          from IPJ.VT_TRAMITE_TECN_IPJ vtIPJ
                         where VTIPJ.ID_ESTADO < 100
                           and (VTIPJ.ID_GRUPO_ULT_ESTADO not in (164, 185) or
                               VTIPJ.ID_ESTADO not in (1, 5, 6))
                           and (nvl(0, 0) = 0 or vtIPJ.Id_Estado = 0)
                           and (nvl(0, 0) = 0 or vtIPJ.id_clasif_ipj = 0)
                        
                        UNION ALL
                        select *
                          from IPJ.VT_TRAMITE_TECN_IPJ vtIPJ
                         where VTIPJ.ID_ESTADO < 100
                           and vtIPJ.Clase = 1
                           and (VTIPJ.ID_GRUPO_ULT_ESTADO not in (164, 185) or
                               VTIPJ.ID_ESTADO not in (1, 5, 6))
                           and vtIPJ.Id_Tramite_Ipj in
                               (select distinct Id_Tramite_Ipj
                                  from ipj.t_tramitesipj_acciones acc
                                 where acc.Id_Estado < 100)
                           and (nvl(0, 0) = 0 or vtIPJ.Id_Estado = 0)
                           and (nvl(0, 0) = 0 or vtIPJ.id_clasif_ipj = 0)
                        
                        UNION ALL
                        select distinct 0                    botones,
                                        clase,
                                        id_tramite,
                                        id_tramite_ipj,
                                        id_clasif_ipj,
                                        id_tipo_accion,
                                        observacion,
                                        id_estado,
                                        id_estado_tramite,
                                        id_estado_accion,
                                        cuil_usuario,
                                        n_estado,
                                        tipo_ipj,
                                        fecha_asignacion,
                                        acc_abiertas,
                                        acc_cerradas,
                                        urgente,
                                        usuario,
                                        error_dato,
                                        cuit,
                                        razon_social,
                                        id_legajo,
                                        n_ubicacion,
                                        cuil_creador,
                                        id_ubicacion,
                                        id_tramiteipj_accion,
                                        id_protocolo,
                                        id_integrante,
                                        nro_documento,
                                        cuit_persjur,
                                        obs_rubrica,
                                        identificacion,
                                        persona,
                                        id_fondo_comercio,
                                        id_proceso,
                                        id_pagina,
                                        agrupable,
                                        null                 cuil_usuario_grupo,
                                        id_grupo_ult_estado,
                                        sticker,
                                        expediente,
                                        id_documento,
                                        n_documento,
                                        simple_tramite,
                                        n_ubicacion_origen,
                                        codigo_online,
                                        id_tramite_ipj_padre,
                                        fecha_ini_suac
                          from IPJ.VT_TRAMITE_TECN_IPJ vtIPJ
                         WHERE VTIPJ.ID_ESTADO < 100
                           AND vtIPJ.Clase = 1
                           and (VTIPJ.ID_GRUPO_ULT_ESTADO not in (164, 185) or
                               VTIPJ.ID_ESTADO not in (1, 5, 6))
                           AND (nvl(vtIPJ.id_Grupo_Ult_Estado, 0) <> 0 AND
                               vtIPJ.CUIL_USUARIO IS NULL)
                           AND vtIPJ.Id_Tramite_Ipj in
                               (select distinct Id_Tramite_Ipj
                                  from ipj.t_tramitesipj_acciones acc
                                 where acc.Id_Estado < 100)
                           AND vtIPJ.Id_Tramite_Ipj NOT IN
                               (select vtIPJ.id_tramite_ipj
                                  from IPJ.VT_TRAMITE_TECN_IPJ vtIPJ
                                 WHERE VTIPJ.ID_ESTADO < 100
                                   AND (nvl(0, 0) = 0 or vtIPJ.Id_Estado = 0)
                                   AND (nvl(0, 0) = 0 or vtIPJ.id_clasif_ipj = 0)
                                   AND vtIPJ.Clase = 1)
                        
                        ) tmp
                order by Const_SA desc, id_tramite_ipj asc, clase asc
               
               ) V);
    COMMIT;
  
  EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
  END TABLERO_Tramites_Acciones_Sistema_Gestion;

    PROCEDURE INFO_EQUIDAD_GENERO IS
    
    BEGIN
      EXECUTE IMMEDIATE 'ALTER SESSION SET NLS_DATE_FORMAT=' || '''' ||
                        'DD/MM/YYYY' || '''';
      EXECUTE IMMEDIATE 'ALTER SESSION SET NLS_NUMERIC_CHARACTERS='',.''';
    
      DECLARE
        v_rdo          VARCHAR2(1);
        o_tipo_mensaje NUMBER;
      
        CURSOR cur_eg IS
          SELECT l.id_legajo, l.denominacion_sia, l.cuit, te.tipo_entidad
            FROM ipj.t_legajos l, ipj.t_tipos_entidades te
           WHERE l.id_tipo_entidad = te.id_tipo_entidad
             AND l.fecha_baja_entidad IS NULL
             AND l.eliminado <> 1
          --  AND l.id_legajo BETWEEN 1 AND 80253
          --  and l.fecha_alta between '01/02/2015' and '31/05/2021'
           ORDER BY l.id_legajo ASC;
      
      BEGIN
      
        FOR c IN cur_eg LOOP
        
          ipj.entidad_persjur.sp_validar_equidad_genero(o_rdo          => v_rdo,
                                                        o_tipo_mensaje => o_tipo_mensaje,
                                                        p_id_legajo    => c.id_legajo);
        
          insert into IPJ.T_INFO_EQUIDAD_GENERO TI
            (ID_INFO_EQUI_GENERO,
             FECHA_EJECOCION,
             LEGAJO,
             CUIT,
             POSEE_EQUIDAD,
             TIPO_ENTIDAD,
             DENOMINACION)
          
            (SELECT IPJ.SEQ_INFO_EQUIDAD_GENERO.NEXTVAL,
                    trunc(SYSDATE),
                    c.id_legajo,
                    nvl(c.cuit, '           '),
                    nvl(v_rdo, 'N'),
                    rpad(c.tipo_entidad, 41),
                    c.denominacion_sia
               FROM DUAL);
        
        END LOOP;
    commit;  
      END;
    
    EXCEPTION
      WHEN OTHERS THEN
        ROLLBACK;
    END INFO_EQUIDAD_GENERO;
  


END PKG_REPORTE_PROCESO;
/

