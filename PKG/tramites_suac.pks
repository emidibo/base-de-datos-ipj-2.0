create or replace package ipj.TRAMITES_SUAC
as

  TYPE Tipo_SUAC IS RECORD(
    v_id varchar2(4000),
    v_nro_tramite varchar2(4000),
    v_nro_sticker_completo varchar2(4000),
    v_no_informatizado varchar2(4000),
    v_estado varchar2(4000),
    v_reparticion_caratuladora varchar2(4000),
    v_expediente varchar2(4000),
    v_tipo varchar2(4000),
    v_subtipo varchar2(4000),
    v_asunto varchar2(4000),
    v_cuerpos varchar2(4000),
    v_fojas varchar2(4000),
    v_mesa_propietaria varchar2(4000),
    v_origen_iniciador varchar2(4000),
    v_tipo_iniciador varchar2(4000),
    v_iniciador varchar2(4000),
    v_iniciador_sexo varchar2(4000),
    v_iniciador_nro_documento  varchar2(4000),
    v_iniciador_pai_cod_pais  varchar2(4000),
    v_iniciador_id_numero  varchar2(4000),
    v_iniciador_cuit varchar2(4000),
    v_iniciador_unidad_suac  varchar2(4000),
    v_unidad_actual varchar2(4000),
    v_usuario_actual varchar2(4000),
    v_fecha_creacion varchar2(4000),
    v_fecha_inicio varchar2(4000),
    v_nro_tramite_origen  varchar2(4000),
    v_horas varchar2(4000),
    v_fecha_archivo  varchar2(4000),
    v_ubicacion varchar2(4000),
    v_armario  varchar2(4000),
    v_caja varchar2(4000),
    v_estante varchar2(4000),
    v_carpeta varchar2(4000),
    v_fecha_reactivacion varchar2(4000)
  );

  FUNCTION FC_Armar_Stiker_Publico (p_sticker_completo in varchar2) return varchar2;
  FUNCTION FC_Buscar_Stiker_Publico (p_id_tramite_suac in number) return varchar2;
  FUNCTION FC_Buscar_Expediente (p_id_tramite_suac in number) return varchar2;
  FUNCTION FC_Buscar_Autoriz_SUAC (p_Id_Tramite in number) return varchar2;
  FUNCTION FC_Formatear_Nota (p_nro in number, p_obs in varchar2, p_fecha in date, p_estado in number) return varchar2;
  FUNCTION FC_Obtener_Sticker_Archivo(p_id_tramite_SUAC in number) return varchar2;
  FUNCTION FC_Buscar_Stiker_SUAC (p_expediente in varchar2) return varchar2;
  FUNCTION FC_Buscar_Asunto_SUAC (p_id_tramite in number) return varchar2;
  FUNCTION FC_Buscar_Fojas_SUAC (p_id_tramite in number) return varchar2;
  FUNCTION FC_Buscar_Cuerpos_SUAC (p_id_tramite in number) return varchar2;
  FUNCTION FC_Buscar_SubTipo_SUAC (p_id_tramite in number) return varchar2;
  FUNCTION FC_Buscar_Fec_Ini_SUAC (p_id_tramite in number) return date;
  FUNCTION FC_Buscar_Ult_Fec_SUAC (p_id_tramite in number) return date;
  FUNCTION FC_Buscar_Fec_Arch_SUAC (p_id_tramite in number) return date;
  FUNCTION FC_Buscar_Iniciador_SUAC (p_id_tramite in number) return varchar2;
  FUNCTION FC_Buscar_ID_SUAC (p_Sticker in varchar2) return number;
  FUNCTION FC_Buscar_Cod_Unidad_SUAC (p_n_unidad in varchar2) return varchar2;
  FUNCTION FC_Armar_Usuario_Suac (p_cuil in varchar2) return varchar2;

  PROCEDURE SP_Sleep(
    p_segundos in int
  );

  PROCEDURE SP_Asignar_Tramites_IPJ(
    o_rdo out varchar2,
    o_tipo_mensaje out number);

  PROCEDURE SP_Realizar_Tranf_IPJ(
     o_rdo out varchar2,
     o_tipo_mensaje out number,
     p_id_tramite_suac in number,
     p_id_ubicacion_origen in number,
     p_id_ubicacion_destino in number,
     p_cuil_usuario_origen in varchar2,
     p_cuil_usuario_destino in varchar2 );

  PROCEDURE SP_Insertar_Pase(
    p_nro_sticker_completo in varchar2,
    p_unidad_origen in varchar2,
    p_unidad_destino in varchar2,
    p_fecha_entrada in date,
    p_cuerpos in number,
    p_folios in number,
    p_usuario in varchar2,
    o_rdo out varchar2,
    o_tipo_mensaje out number) ;

   PROCEDURE SP_Aceptar_Pase(
    p_nro_sticker_completo in varchar2,
    p_cod_unidad_receptora in varchar2,
    p_usuario_receptor in varchar2,
    p_cuerpos in number,
    p_folios in number,
    o_rdo out varchar2,
    o_tipo_mensaje out number);

  PROCEDURE SP_Buscar_Pases(
    p_nro_sticker_completo in varchar2,
    o_fecha_pase out TIMESTAMP,
    o_usuario_pase out varchar2,
    o_unidad_pase out varchar2,
    o_Prox_pase out number,
    o_rdo out varchar2,
    o_tipo_mensaje out number);

  PROCEDURE SP_Cerrar_Tramite(
    p_id_tramite_suac in number,
    p_cod_unidad in varchar2,
    p_usuario in varchar2,
    o_rdo out varchar2,
    o_tipo_mensaje out number);


  PROCEDURE SP_Crear_Tramite(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_sticker out VARCHAR2,
    o_sticker_iniciador out VARCHAR2,
    o_expediente out varchar2,
    o_id_tramite_suac out number,
    p_cod_reparticion in varchar2,
    p_cod_mesa in varchar2,
    p_tipo_tramite in number,
    p_subtipo_tramite in number,
    p_asunto in varchar2,
    p_tipo_persona in number, --1 = Persona fisica // 3 = Persona juridica // 4 = Unidad del organigrama //  5 = Unidad externa
    p_apellido in varchar2, -- Apellido de Persona F�sica o Raz�n Social de Empresa
    p_nombre in varchar2,
    p_tipo_documento in varchar2,
    p_nro_documento in varchar2,
    p_id_sexo in varchar2,
    p_pai_cod_pais in varchar2,
    p_id_numero in number,
    p_tipo_expediente in varchar2
    );

  PROCEDURE SP_Subir_Notas_SUAC(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_cant_sincroniz out number,
    o_sticker out varchar,
    p_id_tramite_ipj in number,
    p_transaccion char) ;

  PROCEDURE SP_Bajar_Notas_SUAC(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in number);

  PROCEDURE SP_Subir_Datos_Part(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in number) ;

  PROCEDURE SP_Buscar_Suac (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_SUAC out Tipo_SUAC,
    p_id_tramite in number,
    p_loguear in char);

  PROCEDURE SP_Buscar_Sticker_Suac (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_SUAC out Tipo_SUAC,
    p_Sticker_14 in varchar2);

  PROCEDURE SP_Anexar_Notas_SUAC(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_suac_anexo in number,
    p_id_tramite_ipj_padre in number);

  PROCEDURE SP_Anexar_Expediente(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj_padre in number,
    p_sticker14_anexo in varchar2,
    p_cuil_usuario in varchar2,
    p_Adjunta_Notas in char
  );

  PROCEDURE SP_Reabrir_Tramite(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_sticker_14 in varchar2,
    p_usuario in varchar2,
    p_unidad in varchar2,
    p_unidad_destino in varchar2
  );

  PROCEDURE SP_Realizar_Tranf_Mesa_Suac(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_suac in number,
    p_cuil_usuario in varchar2
  );

  PROCEDURE SP_Recibir_Tramite_Mesa_Suac(
     o_rdo out varchar2,
     o_tipo_mensaje out number,
     p_id_tramite_suac in number,
     p_cuil_usuario in varchar2
   );

  PROCEDURE SP_Buscar_Tipo_SUAC (
    o_n_tipo_suac out varchar2,
    o_n_subtipo_suac out varchar2,
    p_id_tramite in number
  );

  PROCEDURE SP_Transformar_Nota (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_nuevo_expedinete out varchar2,
    p_id_tramite_ipj in number,
    p_cuil_usuario in varchar2
  );

  PROCEDURE SP_Transferir_Expediente (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_suac in number,
    p_usuario_dest_suac in varchar2
  );

  /**********************************************************
        SOBRECARGA DE PARAMETROS
  **********************************************************/

  PROCEDURE SP_Subir_Notas_SUAC(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_cant_sincroniz out number,
    o_sticker out varchar,
    p_id_tramite_ipj in number
  ) ;
end TRAMITES_SUAC;
/

create or replace package body ipj.TRAMITES_SUAC is

  FUNCTION FC_Armar_Stiker_Publico (p_sticker_completo in varchar2) return varchar2
  IS
  BEGIN
    if length(p_sticker_completo) >= 14 then
      return substr(p_sticker_completo, 0, length(p_sticker_completo) - 5) || substr(p_sticker_completo, -3);
    else
      return p_sticker_completo;
    end if;
  END FC_Armar_Stiker_Publico;

  FUNCTION FC_Buscar_Stiker_Publico (p_id_tramite_suac in number) return varchar2
  IS
    -- Dado el ID_TRAMITE, devuelve el sticker sin d�gitos ocultos
    v_Cursor_Tramite  types.cursorType;
    v_error varchar2(2000);
    v_SUAC Tipo_SUAC;
    v_id_SUAC number;
  BEGIN
    -- Si viene 0 o null, devuelve vacio
    if nvl(p_id_tramite_suac, 0) = 0 then
      return '';
    else
      -- Busco los datos del tramite en suac
      NUEVOSUAC.PCK_INTERFAZ_VERTICALES_V2.P_GET_TRAMITE_BY_ID(v_id_suac, v_Cursor_Tramite, v_error);
      if nvl(v_error, IPJ.TYPES.C_RESP_OK) = IPJ.TYPES.C_RESP_OK then
        loop
          fetch v_Cursor_Tramite into v_SUAC;
          EXIT WHEN v_Cursor_Tramite%NOTFOUND or v_Cursor_Tramite%NOTFOUND is null;

        end loop;
        close v_Cursor_Tramite;
      end if;

      return substr(v_SUAC.v_nro_sticker_completo, 1, 9) || substr(v_SUAC.v_nro_sticker_completo, 12, 14);
    end if;
  exception
      when NO_DATA_FOUND then
        return '';
  END FC_Buscar_Stiker_Publico;

  FUNCTION FC_Buscar_Expediente (p_id_tramite_suac in number) return varchar2
  IS
    -- Dado el ID_TRAMITE, devuelve el expediente
    v_Cursor_Tramite  IPJ.types.cursorType;
    v_error varchar2(2000);
    v_SUAC Tipo_SUAC;
    v_id_SUAC number;
  BEGIN
    -- Si viene 0 o null, devuelve vacio
    if nvl(p_id_tramite_suac, 0) = 0 then
      return '';
    else
      -- Busco los datos del tramite en suac
      NUEVOSUAC.PCK_INTERFAZ_VERTICALES_V2.P_GET_TRAMITE_BY_ID(v_id_suac, v_Cursor_Tramite, v_error);
      if nvl(v_error, IPJ.TYPES.C_RESP_OK) = IPJ.TYPES.C_RESP_OK then
        loop
          fetch v_Cursor_Tramite into v_SUAC;
          EXIT WHEN v_Cursor_Tramite%NOTFOUND or v_Cursor_Tramite%NOTFOUND is null;

        end loop;
        close v_Cursor_Tramite;
      end if;

      return v_SUAC.v_nro_tramite;
    end if;
  exception
      when NO_DATA_FOUND then
        return '';
  END FC_Buscar_Expediente;
  FUNCTION FC_Armar_Usuario_Suac (p_cuil in varchar2) return varchar2
  IS
    v_result varchar2(20);
  BEGIN
    -- El usuario SUAC se forma con D0 + DNI
    v_result := 'd0' ||  substr(p_cuil, 3, 8);

    return v_result;
  exception
    when NO_DATA_FOUND then
      return '';
  END FC_Armar_Usuario_Suac;

  FUNCTION FC_Buscar_Autoriz_SUAC (p_Id_Tramite in number) return varchar2
  IS
    /* Trae los Autorizados cargados en la solapa de Datos Particulares de SUAC */
    v_result varchar2(2000);
    v_estado varchar2(100);
  BEGIN
    -- Busco los datos del tramite en suac
    NUEVOSUAC.PCK_NUEVOSUAC_IPJ.pr_get_autorizados(p_id_tramite, v_result, v_estado);

    return v_result;
  exception
    when OTHERS then
      return '';
  END FC_Buscar_Autoriz_SUAC;

  FUNCTION FC_Buscar_Cod_Unidad_SUAC (p_n_unidad in varchar2) return varchar2
  IS
    /* Trae el C�digo de una unidad SUAC*/
    v_result varchar2(2000);
    v_estado varchar2(100);
  BEGIN
    -- Busco los datos del tramite en suac
    select codigo into v_result
    from NUEVOSUAC.VT_UNIDADES
    where
      n_unidad =  p_n_unidad and
      fecha_hasta is null and
      nvl(desactivado, 0) = 0 and
      nvl(externa, 0) = 0 and
      nvl(eliminado, 0) = 0;

    return v_result;
  exception
    when OTHERS then
      return '';
  END FC_Buscar_Cod_Unidad_SUAC;

  FUNCTION FC_Formatear_Nota (p_nro in number, p_obs in varchar2, p_fecha in date, p_estado in number) return varchar2
  IS
  /*
  El formato del cursor recibido deber� ser un registro por entrada de la grilla con 3 columnas divididas por el caracter especial "~"
    - la primera columna es el orden con que se visualizar� el registro en la grilla,
    - la segunda columna es la observaci�n en si misma
    - la tercera columna es el Cumplimiento (generalmente Fecha y OK)
  No deben enviarse dentro del registro otros caracteres especiales fuera del mencionado.
  */
    v_result varchar2(4000);
    v_n_estado varchar2(100);
  BEGIN
    -- Busco el Nombre del Estado
    select N_ESTADO_NOTA into v_n_estado
    from IPJ.T_TIPOS_ESTADO_NOTA
    where
      id_estado_nota = p_estado;

    if p_estado in (0, 2) then -- Estados Pendientes
      v_result := to_char(p_nro) || IPJ.Types.c_Separador_SUAC || p_obs || IPJ.Types.c_Separador_SUAC;
    else -- 1 y 3 son Finales
      v_result := to_char(p_nro) || IPJ.Types.c_Separador_SUAC || p_obs || IPJ.Types.c_Separador_SUAC ||
        to_char(p_fecha, 'dd/mm/rrrr') || ' ' || v_n_estado;
    end if;
   return v_result;
  END FC_Formatear_Nota;

  FUNCTION FC_Codigo_Unidad_Suac (p_nombre in varchar2) return varchar2
  IS
    v_result varchar2(20);
  BEGIN
    -- Dado los nombres que devuelven las vistas y SP de SUAC, se buscan los c�digos (no el ID)
    select codigo into v_result
    from NUEVOSUAC.vt_unidades
    where
      n_unidad = p_nombre and
      fecha_hasta is null and
      nvl(desactivado, 0) = 0 and
      nvl(externa, 0) = 0 and
      nvl(eliminado, 0) = 0;

    return v_result;
  exception
    when NO_DATA_FOUND then
      return '';
  END FC_Codigo_Unidad_Suac;

  FUNCTION FC_Obtener_Sticker_Archivo(p_id_tramite_SUAC in number) return varchar2
  IS
    v_result varchar2(4000);
    v_SUAC Tipo_SUAC;
    v_rdo varchar2(1000);
    v_tipo_mensaje number;
  BEGIN
    -- Busco los codigos necesarios
    SP_Buscar_Suac (
      o_rdo => v_rdo,
      o_tipo_mensaje => v_tipo_mensaje,
      o_SUAC => v_SUAC,
      p_id_tramite => p_id_tramite_SUAC,
      p_loguear => 'N');

    v_result :=  v_SUAC.v_nro_sticker_completo;

    return v_result;
  exception
    when NO_DATA_FOUND then
      Return 'ERROR: ' ||To_Char(SQLCODE) || '-' || SQLERRM;
  END FC_Obtener_Sticker_Archivo;

  FUNCTION FC_Buscar_ID_SUAC (p_Sticker in varchar2) return number
  IS
    -- Dado el Sitker de 14, devuelve el id del tramite
    v_Cursor_Tramite  types.cursorType;
    v_error varchar2(2000);
    v_SUAC Tipo_SUAC;
  BEGIN
    -- Busco los datos del tramite en suac
    NUEVOSUAC.PCK_INTERFAZ_VERTICALES_V2.P_GET_TRAMITE(p_Sticker, v_Cursor_Tramite);
    loop
      fetch v_Cursor_Tramite into v_SUAC;

      EXIT WHEN v_Cursor_Tramite%NOTFOUND or v_Cursor_Tramite%NOTFOUND is null;
    end loop;
    close v_Cursor_Tramite;

    return v_SUAC.v_id;
  exception
    when NO_DATA_FOUND then
      return '';
  END FC_Buscar_ID_SUAC;

  PROCEDURE SP_Sleep(
    p_segundos in int
  )
  IS
  BEGIN
    /*********************************************************
      Espera la cantidad de segundos indicada
   **********************************************************/
     DBMS_OUTPUT.PUT_LINE('  Fecha: ' || to_char(sysdate, 'dd/mm/rrrr HH:MM:SS'));
     --APEX_UTIL.PAUSE(p_segundos);
     DBMS_LOCK.sleep(p_segundos);
    DBMS_OUTPUT.PUT_LINE('  Fecha: ' || to_char(sysdate, 'dd/mm/rrrr HH:MM:SS'));
  END SP_Sleep;

  PROCEDURE SP_Buscar_Suac (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_SUAC out Tipo_SUAC,
    p_id_tramite in number,
    p_loguear in char)
  IS
    -- Dado el ID TRAMITE SUAC, busca el Sticker completo en SUAC
    v_Cursor_Tramite  types.cursorType;
    v_error varchar2(2000);
  BEGIN
    -- Busco los datos del tramite en suac
    NUEVOSUAC.PCK_INTERFAZ_VERTICALES_V2.P_GET_TRAMITE_BY_ID(p_id_tramite, v_Cursor_Tramite, v_error);
    if nvl(v_error, IPJ.TYPES.C_RESP_OK) = IPJ.TYPES.C_RESP_OK then
      loop
        fetch v_Cursor_Tramite into o_SUAC;
        EXIT WHEN v_Cursor_Tramite%NOTFOUND or v_Cursor_Tramite%NOTFOUND is null;

      end loop;
      close v_Cursor_Tramite;

    else
      if p_loguear = 'S' then
        IPJ.VARIOS.SP_GUARDAR_LOG(
          p_METODO => 'SUAC - Buscar Tramite SUAC',
          p_NIVEL => 'P_GET_TRAMITE_BY_ID ' || to_char(p_id_tramite),
          p_ORIGEN => 'DB',
          p_USR_LOG  => 'DB',
          p_USR_WINDOWS => 'ERROR-DB',
          p_IP_PC  => '',
          p_EXCEPCION => SubStr(v_error, 1, 4000));
      end if;
    end if;

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  exception
    when NO_DATA_FOUND then
      o_rdo :=  To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Buscar_Suac;

  PROCEDURE SP_Buscar_Sticker_Suac (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_SUAC out Tipo_SUAC,
    p_Sticker_14 in varchar2)
  IS
    -- Dado Sticker de 14, devuelve el tramite completo en SUAC
    v_Cursor_Tramite  types.cursorType;
    v_error varchar2(2000);
  BEGIN
    -- Busco los datos del tramite en suac
    NUEVOSUAC.PCK_INTERFAZ_VERTICALES_V2.P_GET_TRAMITE(p_Sticker_14, v_Cursor_Tramite);
    begin
      loop
        fetch v_Cursor_Tramite into o_SUAC;
        EXIT WHEN v_Cursor_Tramite%NOTFOUND or v_Cursor_Tramite%NOTFOUND is null;

      end loop;
      close v_Cursor_Tramite;

      o_rdo := IPJ.TYPES.C_RESP_OK;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;

    exception
      when OTHERS then
        IPJ.VARIOS.SP_GUARDAR_LOG(
          p_METODO => 'SUAC - Buscar Tramite SUAC',
          p_NIVEL => 'P_GET_TRAMITE_BY ' || p_Sticker_14,
          p_ORIGEN => 'DB',
          p_USR_LOG  => 'DB',
          p_USR_WINDOWS => 'ERROR-DB',
          p_IP_PC  => '',
          p_EXCEPCION => SubStr(To_Char(SQLCODE) || '-' || SQLERRM, 1, 4000));

        o_rdo :=  'No se encontro el expediente en SUAC.';
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
    end;

  exception
    when NO_DATA_FOUND then
      o_rdo :=  To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Buscar_Sticker_Suac;

  PROCEDURE SP_Cambiar_Mesa_SUAC(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_suac in number,
    p_mesa_suac_destino in varchar2
  )
  IS
   /*********************************************************
     Si se necesita cambiar el expediente de mesa SUAC, para hacer una transferencia luego
     No valida estrados, solo inserta y recibe el pase de una mesa a otra
   *********************************************************/
    v_area_suac_origen varchar2(50);
    v_error varchar2(500);
    v_Cursor_Tramite  types.cursorType;
    v_Fecha_Pase_Ultimo TIMESTAMP;
    v_Usuario_Pase_Ultimo Varchar2(100);
    v_Unidad_Pasev_Id_Ultimo Varchar2 (500);
    v_Unidad_Pase_Ultimo varchar2(500);
    v_prox_pase number;
    v_resp_mesa_dest varchar2(20);

    -- tramite suac
    v_SUAC Tipo_SUAC;
  BEGIN
     -- Busco los datos del tr�mite SUAC
    DBMS_OUTPUT.PUT_LINE('    -- Inicia SP_Cambiar_Mesa_SUAC tr�mite: ' || to_char(p_id_tramite_suac));
    NUEVOSUAC.PCK_INTERFAZ_VERTICALES_V2.P_GET_TRAMITE_BY_ID(p_id_tramite_suac, v_Cursor_Tramite, v_error);
    DBMS_OUTPUT.PUT_LINE('         - Resultado: ' || v_error);
    if nvl(v_error, IPJ.TYPES.C_RESP_OK) <> IPJ.TYPES.C_RESP_OK then
      o_rdo :=  'SP_Cambiar_Mesa_SUAC - Buscar Tramite: ' || v_error ;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      return;
    end if;

    loop
      fetch v_Cursor_Tramite into v_SUAC;

      EXIT WHEN v_Cursor_Tramite%NOTFOUND or v_Cursor_Tramite%NOTFOUND is null;
    end loop;
    close v_Cursor_Tramite;

    -- Busco la ubicacion actual
    select codigo into v_area_suac_origen
    from NUEVOSUAC.VT_UNIDADES
    where
      n_unidad = v_SUAC.v_unidad_actual and
      fecha_hasta is null and
      nvl(desactivado, 0) = 0 and
      nvl(externa, 0) = 0 and
      nvl(eliminado, 0) = 0;

    -- Inserto un pase a la otra mesa
    DBMS_OUTPUT.PUT_LINE('    1 -- Inserta Pase');
    DBMS_OUTPUT.PUT_LINE('       - Unidad Origen = ' || v_area_suac_origen);
    DBMS_OUTPUT.PUT_LINE('       - Unidad Detino = ' || p_mesa_suac_destino);
    DBMS_OUTPUT.PUT_LINE('       - Usuario = ' || v_SUAC.v_usuario_actual);

    IPJ.TRAMITES_SUAC.SP_Insertar_Pase(
      p_nro_sticker_completo => v_SUAC.v_nro_sticker_completo,
      p_unidad_origen => v_area_suac_origen,
      p_unidad_destino => p_mesa_suac_destino,
      p_fecha_entrada => to_date(sysdate, 'dd/mm/rrrr'),
      p_cuerpos => v_SUAC.v_cuerpos,
      p_folios => v_SUAC.v_fojas,
      p_usuario => v_SUAC.v_usuario_actual,
      o_rdo => o_rdo,
      o_tipo_mensaje => o_tipo_mensaje
    );
    DBMS_OUTPUT.PUT_LINE('       - Resultado = ' || o_rdo);

    if o_rdo <> IPJ.TYPES.C_RESP_OK then
      o_rdo := 'Insertar Pase de Cambio de Mesa SUAC: '  || o_rdo;
      return;
    end if;

    --Se busca el pase recien cargado
    DBMS_OUTPUT.PUT_LINE('    2 -- Busca Pase creado');
    DBMS_OUTPUT.PUT_LINE('       - Sticker = ' || v_SUAC.v_nro_sticker_completo);
    IPJ.TRAMITES_SUAC.SP_Buscar_Pases(
      p_nro_sticker_completo => v_SUAC.v_nro_sticker_completo,
      o_fecha_pase => v_Fecha_Pase_Ultimo,
      o_usuario_pase => v_Usuario_Pase_Ultimo,
      o_unidad_pase => v_Unidad_Pase_Ultimo,
      o_Prox_pase => v_prox_pase,
      o_rdo => o_rdo,
      o_tipo_mensaje => o_tipo_mensaje
    );
    DBMS_OUTPUT.PUT_LINE('       - Resultado = ' || o_rdo);

    if o_rdo <> IPJ.TYPES.C_RESP_OK then
      o_rdo := 'Buscar Pase SUAC: '  || o_rdo;
      return;
    end if;

    -- Busco el responsable de la nueva mesa
    select id_resp_mesa into v_resp_mesa_dest
    from nuevosuac.vt_get_rep_ipj
    where
      codigo_mesa = p_mesa_suac_destino;

    -- Acepto el pase en la nueva mesa
    DBMS_OUTPUT.PUT_LINE('    3 -- Acepta Pase creado');
    DBMS_OUTPUT.PUT_LINE('       - Sticker = ' || v_SUAC.v_nro_sticker_completo);
    DBMS_OUTPUT.PUT_LINE('       - Unidad Receptora = ' || p_mesa_suac_destino);
    DBMS_OUTPUT.PUT_LINE('       - Usuario Receptor = ' || v_resp_mesa_dest);

    IPJ.TRAMITES_SUAC.SP_Aceptar_Pase(
      p_nro_sticker_completo => v_SUAC.v_nro_sticker_completo,
      p_cod_unidad_receptora => p_mesa_suac_destino,
      p_usuario_receptor => v_resp_mesa_dest,
      p_cuerpos => v_SUAC.v_cuerpos,
      p_folios => v_SUAC.v_fojas,
      o_rdo => o_rdo,
      o_tipo_mensaje => o_tipo_mensaje
    );
    DBMS_OUTPUT.PUT_LINE('       - Resultado = ' || o_rdo);

    if nvl(o_rdo,IPJ.TYPES.C_RESP_OK) <> IPJ.TYPES.C_RESP_OK then
      o_rdo := 'Aceptar Pase SUAC: '  || o_rdo;
      return;
    end if;
    DBMS_OUTPUT.PUT_LINE('    -- Finalizo SP_Cambiar_Mesa_SUAC');
  exception
    when OTHERS then
      o_rdo :=  'SP_Cambiar_Mesa_SUAC: ' || To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Cambiar_Mesa_SUAC;


  PROCEDURE SP_Asignar_Tramites_IPJ(
    o_rdo out varchar2,
    o_tipo_mensaje out number)
  IS
    v_SUAC Tipo_SUAC;
    v_Fecha_Pase_Ultimo TIMESTAMP;
    v_Usuario_Pase_Ultimo Varchar2(100);
    v_Unidad_Pasev_Id_Ultimo Varchar2 (500);
    v_prox_pase number;

    v_Cursor_Tramite_IPJ  types.cursorType;
    v_Cursor_Tramite  types.cursorType;
    v_Cursor_Legajos  types.cursorType;
    v_Row_vtSuac NUEVOSUAC.VT_TRAMITES_IPJ_DESA%ROWTYPE;
    v_Row_Ubicacion IPJ.t_Ubicaciones%ROWTYPE;
    v_Row_Legajo IPJ.t_legajos%ROWTYPE;
    v_error varchar2(2000);
    v_error_pase varchar2(2000);
    v_id_error_pase number;

    v_id_tramite_ipj number;
    v_rdo_ipj varchar2(3000);
    v_tipo_mensaje_ipj number;
  BEGIN
    /*********************************************************
      Este SP busca los tramites que esten en la mesa de IPJ, y no se encuentren en el
      Nuevo Sistema IPJ,
      Si cuentan con un pase definido, los asigna a la bandeja del responsable del area.
      Si falla aceptar el pase en SUAC, lo mismo se agrega el tramite en IPJ
    **********************************************************/

    -- Armo un cursor con todos los tramites pendientes de hoy, que esten en SUAC IPJ y no estene en el sistema nuevo
    OPEN v_Cursor_Tramite_IPJ FOR
      select *
      from NUEVOSUAC.VT_TRAMITES_IPJ_DESA vtsuac
      where
        --VTSUAC.ID_UNIDAD_ACTUAL = 3262 and
        --vtsuac.id_tramite= 7422781 and
       VTSUAC.FECHA_ULTIMA_RECEPCION BETWEEN
       TO_DATE('03/02/2011', 'dd/mm/rrrr') AND
       TO_DATE('04/02/2011', 'dd/mm/rrrr')
       AND NOT EXISTS (SELECT *
          FROM IPJ.T_TRAMITESIPJ TR
         WHERE TR.ID_TRAMITE = VTSUAC.ID_TRAMITE);

    loop
      fetch v_Cursor_Tramite_IPJ into v_Row_vtSuac;
      EXIT WHEN v_Cursor_Tramite_IPJ%NOTFOUND or v_Cursor_Tramite_IPJ%NOTFOUND is null;

      -- Busco los datos del tramite en suac
      NUEVOSUAC.PCK_INTERFAZ_VERTICALES_V2.P_GET_TRAMITE_BY_ID(v_Row_vtSuac.id_tramite, v_Cursor_Tramite, v_error);
      if nvl(v_error, IPJ.TYPES.C_RESP_OK) <> IPJ.TYPES.C_RESP_OK then
        IPJ.VARIOS.SP_GUARDAR_LOG(
          p_METODO => 'SUAC - SP_Asignar_Tramites_IPJ',
          p_NIVEL => 'P_GET_TRAMITE_BY_ID ' || to_char(v_Row_vtSuac.id_tramite),
          p_ORIGEN => 'DB',
          p_USR_LOG  => 'DB',
          p_USR_WINDOWS => 'ERROR-DB',
          p_IP_PC  => '',
          p_EXCEPCION => SubStr(v_error, 1, 4000));
      end if;

      loop
        fetch v_Cursor_Tramite into v_SUAC;

        EXIT WHEN v_Cursor_Tramite%NOTFOUND or v_Cursor_Tramite%NOTFOUND is null;
      end loop;
      close v_Cursor_Tramite;

     -- Si esta A Recibir, debo tomar el pase pendiente
      if v_SUAC.v_estado = 'A RECIBIR' then
          -- Consultos los pases que posee el tramite
        IPJ.TRAMITES_SUAC.SP_Buscar_Pases(
          p_nro_sticker_completo => v_SUAC.v_nro_sticker_completo,
          o_fecha_pase => V_Fecha_Pase_Ultimo,
          o_usuario_pase => V_Usuario_Pase_Ultimo,
          o_unidad_pase => V_Unidad_Pasev_Id_Ultimo,
          o_Prox_pase => v_prox_pase,
          o_rdo => v_error_pase,
          o_tipo_mensaje => v_id_error_pase);

        if v_error_pase <> IPJ.TYPES.C_RESP_OK then
          IPJ.VARIOS.SP_GUARDAR_LOG(
            p_METODO => 'SUAC - SP_Asignar_Tramites_IPJ',
            p_NIVEL => 'SP_Buscar_Pases ' || v_SUAC.v_nro_sticker_completo,
            p_ORIGEN => 'DB',
            p_USR_LOG  => 'DB',
            p_USR_WINDOWS => 'ERROR-DB',
            p_IP_PC  => '',
            p_EXCEPCION => SubStr(o_rdo, 1, 4000));
        end if;

        -- Busco si el area esta configurada en IPJ, si no esta paso como OK
        begin
          select u.* into v_Row_Ubicacion
          from IPJ.T_ubicaciones u join NUEVOSUAC.VT_UNIDADES vt
            on u.codigo_suac = vt.codigo and vt.fecha_hasta is null and nvl(vt.desactivado, 0) = 0 and nvl(vt.externa, 0) = 0 and nvl(vt.eliminado, 0) = 0
          where
             VT.N_UNIDAD = V_Unidad_Pasev_Id_Ultimo and
             U.CODIGO_SUAC is not null;
        exception
          when NO_DATA_FOUND then
           -- Si el area SUAC no esta configurada, no hago nada y sigo
            v_rdo_ipj := IPJ.TYPES.C_RESP_OK;
            v_tipo_mensaje_ipj := IPJ.TYPES.C_TIPO_MENS_OK;
        end;

        -- Si el area tiene asignado un responsable, tomo el pase en SUAC y agrego el tramite en IPJ
        if v_Row_Ubicacion.cuil_usuario is not null then
          -- Si no fallo la busqueda de Pases, acepto el pase pendiente
          if v_error_pase = IPJ.TYPES.C_RESP_OK then

            IPJ.TRAMITES_SUAC.SP_Aceptar_Pase(
              p_nro_sticker_completo => v_SUAC.v_nro_sticker_completo,
              p_cod_unidad_receptora => v_Row_Ubicacion.codigo_suac,
              p_usuario_receptor => FC_Armar_Usuario_Suac(v_Row_Ubicacion.cuil_usuario) ,
              p_cuerpos => v_SUAC.v_cuerpos,
              p_folios => v_SUAC.v_fojas,
              o_rdo => v_error_pase,
              o_tipo_mensaje => v_id_error_pase);

            -- Si falla el pase en SUAC, logue el problema y sigo
            if nvl(v_error_pase, IPJ.TYPES.C_RESP_OK) not  like IPJ.TYPES.C_RESP_OK || '%' then
              IPJ.VARIOS.SP_GUARDAR_LOG(
                p_METODO => 'SUAC - SP_Asignar_TramitesIPJ',
                p_NIVEL => 'SP_Aceptar_Pase Sticker' || v_SUAC.v_nro_sticker_completo,
                p_ORIGEN => 'DB',
                p_USR_LOG  => 'DB',
                p_USR_WINDOWS => 'ERROR-DB',
                p_IP_PC  => '',
                p_EXCEPCION => SubStr(v_error_pase, 1, 4000));
            end if;
          end if;

          -- Sin importar el pase en SUAC, Cargo el tramite en IPJ al responsable o grupo
          IPJ.TRAMITES.SP_GUARDAR_TRAMITE(
            o_id_tramite_ipj => v_id_tramite_ipj,
            o_rdo => v_rdo_ipj,
            o_tipo_mensaje => v_tipo_mensaje_ipj,
            v_id_tramite_ipj => 0,
            v_id_tramite => v_Row_vtSuac.id_tramite,
            v_id_Clasif_IPJ => null,
            v_id_Grupo => v_Row_Ubicacion.id_grupo,
            v_cuil_ult_estado => (case when v_Row_Ubicacion.id_grupo is null then v_Row_Ubicacion.Cuil_Usuario else null end),
            v_id_estado => IPJ.TYPES.C_ESTADOS_ASIGNADO ,
            v_cuil_creador => v_Row_Ubicacion.Cuil_Usuario,
            v_observacion => null,
            v_id_Ubicacion => v_Row_Ubicacion.id_ubicacion,
            v_urgente => 'N',
            p_sticker => v_Row_vtSuac.nro_sticker,
            p_expediente => v_Row_vtSuac.nro_tramite,
            p_simple_tramite => 'N');

          if v_rdo_ipj <> IPJ.TYPES.C_RESP_OK then
            IPJ.VARIOS.SP_GUARDAR_LOG(
                p_METODO => 'SUAC - SP_Asignar_TramitesIPJ',
                p_NIVEL => 'SP_GUARDAR_TRAMITE ID SUAc' || to_char(v_Row_vtSuac.id_tramite),
                p_ORIGEN => 'DB',
                p_USR_LOG  => 'DB',
                p_USR_WINDOWS => 'ERROR-DB',
                p_IP_PC  => '',
                p_EXCEPCION => SubStr(v_rdo_ipj, 1, 4000));
          else
            -- Si existen legajos asociados al tr�mite los cargo
            OPEN v_Cursor_Legajos FOR
              select l.*
              from IPJ.T_tramites_legajo tl join IPJ.t_legajos l
                on tl.id_legajo = l.id_legajo
              where
                sticker = v_SUAC.v_nro_sticker_completo and
                L.ELIMINADO <> 1;

            loop
              fetch v_Cursor_Legajos into v_Row_Legajo;

              if v_Row_Legajo.id_legajo is not null then
                IPJ.TRAMITES.SP_Guardar_TramiteIPJ_PersJur (
                  o_rdo => v_rdo_ipj,
                  o_tipo_mensaje => v_tipo_mensaje_ipj,
                  p_id_tramite_ipj => v_id_tramite_ipj,
                  p_id_legajo => v_Row_Legajo.id_legajo,
                  p_id_sede => '00',
                  p_error_dato => 'N',
                  p_eliminar => 0);

                if v_rdo_ipj <> IPJ.TYPES.C_RESP_OK then
                  IPJ.VARIOS.SP_GUARDAR_LOG(
                    p_METODO => 'SUAC - SP_Asignar_TramitesIPJ',
                    p_NIVEL => 'SP_Guardar_TramiteIPJ_PersJur Legajo: ' || to_char(v_Row_Legajo.id_legajo),
                    p_ORIGEN => 'DB',
                    p_USR_LOG  => 'DB',
                    p_USR_WINDOWS => 'ERROR-DB',
                    p_IP_PC  => '',
                    p_EXCEPCION => SubStr(v_rdo_ipj, 1, 4000));
                end if;
              end if;

              EXIT WHEN v_Cursor_Legajos%NOTFOUND or v_Cursor_Legajos%NOTFOUND is null;
            end loop;
          close v_Cursor_Legajos;

          end if;

        end if;
      end if;

      EXIT WHEN v_Cursor_Tramite_IPJ%NOTFOUND or v_Cursor_Tramite_IPJ%NOTFOUND is null;
    end loop;
    close v_Cursor_Tramite_IPJ;

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    commit;
  exception
    when NO_DATA_FOUND then
      o_rdo :=  To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      rollback;
  END SP_Asignar_Tramites_IPJ;

  PROCEDURE SP_Buscar_Pases(
    p_nro_sticker_completo in varchar2,
    o_fecha_pase out TIMESTAMP,
    o_usuario_pase out varchar2,
    o_unidad_pase out varchar2,
    o_Prox_pase out number,
    o_rdo out varchar2,
    o_tipo_mensaje out number)
  IS
    V_FECHA_PASE TIMESTAMP; -- fecha en que se realizo el ultimo pase
    V_USUARIO_PASE varchar2(100);-- usuario que realize el pase (formato: d0+dni)
    V_UNIDAD_PASEV_ID varchar2 (500);-- descripci�n de la unidad donde se realizo el pase

    v_Cursor_Pases  types.cursorType;
    v_nro_orden number;
  BEGIN
    /*********************************************************
      Recorre los pases de un tr�mite y devuelve el ultimo pase, y la cantidad de pases
   **********************************************************/
     -- Cuentos los pases para insertar el nuevo
    NUEVOSUAC.PCK_INTERFAZ_VERTICALES_V2.p_get_pases (p_nro_sticker_completo, v_Cursor_Pases);
    v_nro_orden := 1;
    loop
      fetch v_Cursor_Pases into V_FECHA_PASE, V_USUARIO_PASE, V_UNIDAD_PASEV_ID;
      EXIT WHEN v_Cursor_Pases%NOTFOUND or v_Cursor_Pases%NOTFOUND is null;

      if o_Fecha_Pase is null or o_fecha_pase < v_fecha_pase then
        o_fecha_pase := v_fecha_pase;
        o_usuario_pase := v_usuario_pase;
        o_unidad_pase :=  V_Unidad_Pasev_Id;
      end if;

      v_nro_orden := v_nro_orden + 1;
    end loop;
    close v_Cursor_Pases;

    o_Prox_pase :=v_nro_orden;
    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  exception
    when NO_DATA_FOUND then
      o_rdo :=  To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      o_Prox_pase := 0;
  END SP_Buscar_Pases;

  PROCEDURE SP_Insertar_Pase(
    p_nro_sticker_completo in varchar2,
    p_unidad_origen in varchar2,
    p_unidad_destino in varchar2,
    p_fecha_entrada in date,
    p_cuerpos in number,
    p_folios in number,
    p_usuario in varchar2,
    o_rdo out varchar2,
    o_tipo_mensaje out number)
  IS
    v_Prox_pase number;
    v_Fecha_Pase TIMESTAMP;
    v_Usuario_Pase Varchar2(100);
    v_Unidad_Pase Varchar2 (500);
  BEGIN
    /*
      Agrego un nuevo pase para el area y usuario indicados
    */

    -- Consultos los pases que posee el tramite
    IPJ.TRAMITES_SUAC.SP_Buscar_Pases(
      p_nro_sticker_completo => p_nro_sticker_completo,
      o_fecha_pase => v_Fecha_Pase,
      o_usuario_pase => v_Usuario_Pase,
      o_unidad_pase => v_Unidad_Pase,
      o_Prox_pase => v_Prox_pase,
      o_rdo => o_rdo,
      o_tipo_mensaje => o_tipo_mensaje);

    -- Antes de insertar un pase, espero 1 segundo para que no se pegue con uno anterior
    SP_Sleep(1);

    -- Agrego un nuevo pase, usando Prox_Pase
    DBMS_OUTPUT.PUT_LINE('   ------ INSERTAR PASE (' || to_char(sysdate) || ') :');
    DBMS_OUTPUT.PUT_LINE('        -- Sitker 14: ' || p_nro_sticker_completo);
    DBMS_OUTPUT.PUT_LINE('        -- Orden: ' || to_char(v_Prox_pase));
    DBMS_OUTPUT.PUT_LINE('        -- Unidad Origen: ' || p_unidad_origen);
    DBMS_OUTPUT.PUT_LINE('        -- Unidad Destino: ' || p_unidad_destino);
    DBMS_OUTPUT.PUT_LINE('        -- Fec Entrada: ' || to_char(p_fecha_entrada));
    DBMS_OUTPUT.PUT_LINE('        -- Fec Salida: ' || to_char(sysdate));
    DBMS_OUTPUT.PUT_LINE('        -- Usuario: ' || p_usuario);
    NUEVOSUAC.PCK_INTERFAZ_VERTICALES_V2.p_insertar_derivaciones_ext (
      i_sticker => p_nro_sticker_completo,
      i_orden => v_Prox_pase,
      i_cod_unidad_origen => p_unidad_origen,
      i_cod_unidad_destino => p_unidad_destino,
      i_fecha_entrada => p_fecha_entrada,
      i_fecha_salida => sysdate,
      i_cuerpos => p_cuerpos,
      i_folios => p_folios,
      i_usuario => p_usuario,
      i_observacion => 'Pase automatico IPJ',
      o_error => o_rdo );
    DBMS_OUTPUT.PUT_LINE('        -- Resultado (' || to_char(sysdate) || '): ' || o_rdo);
    if nvl(o_rdo, IPJ.TYPES.C_RESP_OK) like IPJ.TYPES.C_RESP_OK || '%' then
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
      o_rdo := IPJ.TYPES.C_RESP_OK;
    else
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
    end if;

  exception
    when NO_DATA_FOUND then
      o_rdo :=  To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Insertar_Pase;

  PROCEDURE SP_Aceptar_Pase(
    p_nro_sticker_completo in varchar2,
    p_cod_unidad_receptora in varchar2,
    p_usuario_receptor in varchar2,
    p_cuerpos in number,
    p_folios in number,
    o_rdo out varchar2,
    o_tipo_mensaje out number)
  IS
  BEGIN

    -- Acepto un pase pendiente
    NUEVOSUAC.PCK_INTERFAZ_VERTICALES_V2.p_recibir_derivaciones_ext (
      i_sticker => p_nro_sticker_completo,
      i_cod_unidad_receptora => p_cod_unidad_receptora,
      i_usuario_receptor => p_usuario_receptor,
      i_cuerpos => p_cuerpos,
      i_folios => p_folios,
      o_error => o_rdo );

    if nvl(o_rdo, IPJ.TYPES.C_RESP_OK) like IPJ.TYPES.C_RESP_OK || '%' then
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
      o_rdo := IPJ.TYPES.C_RESP_OK;
    else
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
    end if;

  exception
    when NO_DATA_FOUND then
      o_rdo :=  To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Aceptar_Pase;

  PROCEDURE SP_Cerrar_Tramite(
    p_id_tramite_suac in number,
    p_cod_unidad in varchar2,
    p_usuario in varchar2,
    o_rdo out varchar2,
    o_tipo_mensaje out number)
  IS
    v_id_unidad_archivo varchar2(20);
    v_SUAC Tipo_SUAC;
    v_usuario varchar2(30);
  BEGIN
    -- Busco el area de Archivo
    select codigo_suac into v_id_unidad_archivo
    from ipj.t_ubicaciones u
    where
      u.Id_Ubicacion = IPJ.TYPES.C_AREA_ARCHIVO;

    -- busco el sticker de 14 del tr�mite
    SP_Buscar_Suac (
      o_rdo => o_rdo,
      o_tipo_mensaje => o_tipo_mensaje,
      o_SUAC => v_SUAC,
      p_id_tramite => p_id_tramite_suac,
      p_loguear => 'S');

    -- Si no viee usuario, utilizo el que esta asociado al expediente suac
    v_usuario := (case when p_usuario is null then v_suac.v_usuario_actual else p_usuario end);

    -- Si encontr� el tr�mite, lo archivo en SUAC
    if o_rdo = IPJ.TYPES.C_RESP_OK then
      DBMS_OUTPUT.PUT_LINE(' -- NUEVOSUAC.PCK_INTERFAZ_VERTICALES_V2.p_archivar');
      DBMS_OUTPUT.PUT_LINE('    Sticker = ' || v_SUAC.v_nro_sticker_completo);
      DBMS_OUTPUT.PUT_LINE('    Usuario = ' || v_usuario);
      DBMS_OUTPUT.PUT_LINE('    Unidad = ' || p_cod_unidad);
      DBMS_OUTPUT.PUT_LINE('    Unidad Archivo = ' || v_id_unidad_archivo);
      NUEVOSUAC.PCK_INTERFAZ_VERTICALES_V2.p_archivar (
        i_sticker => v_SUAC.v_nro_sticker_completo,
        i_usuario => v_usuario,
        i_unidad => p_cod_unidad,
        i_unidad_archivo => v_id_unidad_archivo,
        i_fecha_env_pub => null,
        i_fecha_pub => null,
        i_armario => null,
        i_estante => null,
        i_caja => null,
        i_carpeta => null,
        o_error => o_rdo
      );
      DBMS_OUTPUT.PUT_LINE('    O_Rdo = ' || o_rdo);
    end if;

    if nvl(o_rdo, IPJ.TYPES.C_RESP_OK) like IPJ.TYPES.C_RESP_OK || '%' then
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
      o_rdo := (case when o_rdo is null then IPJ.TYPES.C_RESP_OK else o_rdo end);
    else
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
    end if;

  exception
    when NO_DATA_FOUND then
      o_rdo :=  To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Cerrar_Tramite;

  PROCEDURE SP_Realizar_Tranf_IPJ(
     o_rdo out varchar2,
     o_tipo_mensaje out number,
     p_id_tramite_suac in number,
     p_id_ubicacion_origen in number,
     p_id_ubicacion_destino in number,
     p_cuil_usuario_origen in varchar2,
     p_cuil_usuario_destino in varchar2 )
  IS
   /*********************************************************
     Este procedimieno sirve para realizar un pase interno entre areas de IPJ.
     Si el area de origen y destino tiene configurado SUAC, y el tramite esta en
     estado A ENVIAR, se agrega un nuevo pase y se lo toma
   *********************************************************/
     v_area_suac_origen varchar2(50);
     v_mesa_suac_origen varchar2(50);
     v_n_unidad_origen varchar2(500);
     v_usr_suac_origen varchar2(50);
     v_area_suac_destino varchar2(50);
     v_mesa_suac_destino varchar2(50);
     v_usr_suac_destino varchar2(50);
     v_usr_mesa_origen varchar2(50);
     v_Cursor_Tramite  types.cursorType;

     v_error varchar2(2000);
     v_error_pase varchar2(2000);
     v_id_error_pase number;

     -- tramite suac
     v_SUAC Tipo_SUAC;
   BEGIN
     -- Busco las areas y mesa suac de los usuarios
     begin
       v_usr_suac_origen := upper(FC_Armar_Usuario_Suac(p_cuil_usuario_origen));
       v_usr_suac_destino := upper(FC_Armar_Usuario_Suac(p_cuil_usuario_destino));
       -- Busco los datos del origen
       select codigo, codigo_mesa, n_unidad into v_area_suac_origen, v_mesa_suac_origen, v_n_unidad_origen
       from NUEVOSUAC.VT_USUARIOS_UNI_JUS
       where
         upper(id_usuario) = v_usr_suac_origen and
         codigo in
           (select codigo_suac
            from ipj.t_relacion_ubic_suac r
            where id_ubicacion = p_id_ubicacion_origen);

       -- Busco los datos del Destino
       select codigo, codigo_mesa into v_area_suac_destino, v_mesa_suac_destino
       from NUEVOSUAC.VT_USUARIOS_UNI_JUS
       where
         upper(id_usuario) =  v_usr_suac_destino and
         codigo in
           (select codigo_suac
            from ipj.t_relacion_ubic_suac r
            where id_ubicacion = p_id_ubicacion_destino);
     exception
       when NO_DATA_FOUND then
         v_area_suac_origen := null;
         v_area_suac_destino := null;
     end;

     -- Busco los datos del tr�mite SUAC
     NUEVOSUAC.PCK_INTERFAZ_VERTICALES_V2.P_GET_TRAMITE_BY_ID(p_id_tramite_suac, v_Cursor_Tramite, v_error);
     DBMS_OUTPUT.PUT_LINE('       Busca tr�mite SUAC - v_error: ' || v_error);
     if nvl(v_error, IPJ.TYPES.C_RESP_OK) <> IPJ.TYPES.C_RESP_OK then
       o_rdo :=  'SP_Realizar_Tranf_IPJ - Buscar Tramite: ' || v_error ;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
       return;
     end if;

     loop
       fetch v_Cursor_Tramite into v_SUAC;

       EXIT WHEN v_Cursor_Tramite%NOTFOUND or v_Cursor_Tramite%NOTFOUND is null;
     end loop;
     close v_Cursor_Tramite;

     -- Si el tr�mite no est� en la misma unidad donde el usuario trabaja, no avanza
     if upper(v_SUAC.v_unidad_actual) <> upper(v_n_unidad_origen) then
       o_rdo :=  'El tr�mite en SUAC no se encuentra en la unidad del usuario de origen.';
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
       DBMS_OUTPUT.PUT_LINE('SUAC - Realizar Transferencia IPJ: ' || o_rdo);
       return;
     end if;

     DBMS_OUTPUT.PUT_LINE('    Estado Tr�mite: ' || v_SUAC.v_estado);
     if v_SUAC.v_estado = 'A RECIBIR' then
       o_rdo :=  'El tr�mite SUAC esta A RECIBIR, no se puede cargar un nuevo pase.' ;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
       DBMS_OUTPUT.PUT_LINE('SUAC - Realizar Transferencia IPJ: ' || o_rdo);
       return;
     end if;

     -- Si ambas areas estan configurada en SUAC, procedo
    DBMS_OUTPUT.PUT_LINE('SUAC - Realizar Transferencia IPJ');
    DBMS_OUTPUT.PUT_LINE('       - Area Origen: ' || v_area_suac_origen);
    DBMS_OUTPUT.PUT_LINE('       - Mesa Origen: ' || v_mesa_suac_origen);
    DBMS_OUTPUT.PUT_LINE('       - Area Destino: ' || v_area_suac_destino);
    DBMS_OUTPUT.PUT_LINE('       - Mesa Destino: ' || v_mesa_suac_destino);

     -- Si hay datos para Origen y Destino, prosigue
     if v_area_suac_origen is not null and v_area_suac_destino is not null then
       -- Si el tramite esta A ENVIAR, inserto el pase y lo tomo
       if v_SUAC.v_estado = 'A ENVIAR' then

         --Si los usuario estan en distintas mesas, lo paso a Mesa de origen y luego lo toma el destinatario
         if v_mesa_suac_origen <> v_mesa_suac_destino then
           -- Inserto un pase y lo tomo al responsable de la mesa de origen
           DBMS_OUTPUT.PUT_LINE('    - Pasa a Mesa Origen');
           select ID_RESP_MESA into v_usr_mesa_origen
           from nuevosuac.VT_GET_REP_IPJ
           where
             codigo_mesa = v_mesa_suac_origen;

           DBMS_OUTPUT.PUT_LINE('    - SP Inserta Pase (Mesa ' || v_mesa_suac_origen || ')' );
           IPJ.TRAMITES_SUAC.SP_Insertar_Pase(
             p_nro_sticker_completo => v_SUAC.v_nro_sticker_completo,
             p_unidad_origen => v_area_suac_origen,
             p_unidad_destino => v_mesa_suac_origen,
             p_fecha_entrada => sysdate,
             p_cuerpos => v_SUAC.v_cuerpos,
             p_folios => v_SUAC.v_fojas,
             p_usuario => v_usr_suac_origen,
             o_rdo => v_error_pase,
             o_tipo_mensaje => o_tipo_mensaje);

           DBMS_OUTPUT.PUT_LINE('       Error: '  || v_error_pase);
           -- Si fallo la inserci�n del pase, no continuo
           if v_error_pase like IPJ.TYPES.C_RESP_OK || '%' then
             DBMS_OUTPUT.PUT_LINE('    - SP Aceptar Pase (Mesa ' || v_mesa_suac_origen || ')' );
             IPJ.TRAMITES_SUAC.SP_Aceptar_Pase(
               p_nro_sticker_completo => v_SUAC.v_nro_sticker_completo,
               p_cod_unidad_receptora => v_mesa_suac_origen,
               p_usuario_receptor => v_usr_mesa_origen,
               p_cuerpos => v_SUAC.v_cuerpos,
               p_folios => v_SUAC.v_fojas,
               o_rdo => v_error_pase,
               o_tipo_mensaje => o_tipo_mensaje);

             DBMS_OUTPUT.PUT_LINE('       Error: '  || v_error_pase);
             -- Si no se pudo aceptar el pase, lo informo
             if v_error_pase like IPJ.TYPES.C_RESP_OK || '%' then
                o_rdo := IPJ.TYPES.C_RESP_OK;
                o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
             else
               o_rdo :=  v_error_pase ;
               o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
               return;
             end if;
           else
             o_rdo :=  v_error_pase ;
             o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
             return;
           end if;

           -- Si la mesa destino es -1, es poque se deriva a la mesa
           if v_mesa_suac_destino = '-1' then
             -- Inserto un pase a la otra mesa
             DBMS_OUTPUT.PUT_LINE('   Distintas Mesas, se llama a SP_Cambiar_Mesa_SUAC');
             SP_Cambiar_Mesa_SUAC(
               o_rdo => o_rdo,
               o_tipo_mensaje => o_tipo_mensaje,
               p_id_tramite_suac => p_id_tramite_suac,
               p_mesa_suac_destino => v_area_suac_destino
             );
             DBMS_OUTPUT.PUT_LINE('     - Resultado = ' || o_rdo);

             if nvl(o_rdo, IPJ.TYPES.C_RESP_OK) <> IPJ.TYPES.C_RESP_OK then
               o_rdo := 'Transferencia entre Mesas: '  || o_rdo;
               return;
             end if;

             -- Tranfiero al usuario de la mesa, no al responsable
             DBMS_OUTPUT.PUT_LINE('    - Se transfiere el usuario (SP_Transferir_Expediente) (1)');
             IPJ.TRAMITES_SUAC.SP_Transferir_Expediente(
               o_rdo => v_error_pase,
               o_tipo_mensaje => o_tipo_mensaje,
               p_id_tramite_suac => p_id_tramite_suac,
               p_usuario_dest_suac => v_usr_suac_destino
             );
             DBMS_OUTPUT.PUT_LINE('            - Transferencia: ' || v_error_pase);
             if v_error_pase <> IPJ.TYPES.C_RESP_OK then
               o_rdo :=  v_error_pase;
               return;
             end if;
           else
             -- Si el area de destino no es una mesa
             -- Tomo el tr�mite desde la mesa al nuevo responsable
             DBMS_OUTPUT.PUT_LINE('    - Transfiere de Mesa a Usuario Destino' );
             IPJ.TRAMITES_SUAC.SP_Recibir_Tramite_Mesa_Suac(
               o_rdo => v_error_pase,
               o_tipo_mensaje => o_tipo_mensaje,
               p_id_tramite_suac => p_id_tramite_suac,
               p_cuil_usuario => p_cuil_usuario_destino
             );

             if v_error_pase <> IPJ.TYPES.C_RESP_OK then
               o_rdo :=  v_error_pase;
               o_tipo_mensaje := o_tipo_mensaje;
               return;
             end if;
           end if;

         else
           if v_area_suac_origen = v_area_suac_destino then
             DBMS_OUTPUT.PUT_LINE('    - Se transfiere el usuario (SP_Transferir_Expediente) (2)');
             IPJ.TRAMITES_SUAC.SP_Transferir_Expediente(
               o_rdo => o_rdo,
               o_tipo_mensaje => o_tipo_mensaje,
               p_id_tramite_suac => p_id_tramite_suac,
               p_usuario_dest_suac => v_usr_suac_destino
             );
             DBMS_OUTPUT.PUT_LINE('          - Transferencia: ' || v_error_pase);
             if v_error_pase <> IPJ.TYPES.C_RESP_OK then
               o_rdo :=  v_error_pase;
               o_tipo_mensaje := o_tipo_mensaje;
               return;
             end if;
           else
             --Si es otra area de la misma mesa, inserta y toma el pase
             DBMS_OUTPUT.PUT_LINE('    - SP Inserta Pase: ' );
             IPJ.TRAMITES_SUAC.SP_Insertar_Pase(
               p_nro_sticker_completo => v_SUAC.v_nro_sticker_completo,
               p_unidad_origen => v_area_suac_origen,
               p_unidad_destino => v_area_suac_destino,
               p_fecha_entrada => sysdate,
               p_cuerpos => v_SUAC.v_cuerpos,
               p_folios => v_SUAC.v_fojas,
               p_usuario => FC_Armar_Usuario_Suac(p_cuil_usuario_origen),
               o_rdo => v_error_pase,
               o_tipo_mensaje => o_tipo_mensaje);

             DBMS_OUTPUT.PUT_LINE('       Error: '  || v_error_pase);
             -- Si fallo la inserci�n del pase, no continuo
             if v_error_pase like IPJ.TYPES.C_RESP_OK || '%' then
               DBMS_OUTPUT.PUT_LINE('    - SP Aceptar Pase: ' );
               IPJ.TRAMITES_SUAC.SP_Aceptar_Pase(
                 p_nro_sticker_completo => v_SUAC.v_nro_sticker_completo,
                 p_cod_unidad_receptora => v_area_suac_destino,
                 p_usuario_receptor => (p_cuil_usuario_destino),
                 p_cuerpos => v_SUAC.v_cuerpos,
                 p_folios => v_SUAC.v_fojas,
                 o_rdo => v_error_pase,
                 o_tipo_mensaje => o_tipo_mensaje);

               DBMS_OUTPUT.PUT_LINE('       Error: '  || v_error_pase);
               -- Si no se pudo aceptar el pase, lo informo
               if v_error_pase like IPJ.TYPES.C_RESP_OK || '%' then
                  o_rdo := IPJ.TYPES.C_RESP_OK;
                  o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
               else
                 o_rdo :=  v_error_pase ;
                 o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
               end if;
             else
               o_rdo :=  v_error_pase ;
               o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
               return;
             end if;
           end if;
         end if;
       end if;

       if v_SUAC.v_estado = 'ARCHIVADO' then
         DBMS_OUTPUT.PUT_LINE('    - SP Reabrir Tramite: ' );
         IPJ.TRAMITES_SUAC.SP_Reabrir_Tramite(
           o_rdo => v_error_pase,
           o_tipo_mensaje => o_tipo_mensaje,
           p_sticker_14 => v_SUAC.v_nro_sticker_completo,
           p_usuario => FC_Armar_Usuario_Suac(p_cuil_usuario_origen),
           p_unidad => v_area_suac_origen,
           p_unidad_destino => v_area_suac_destino
         );

         DBMS_OUTPUT.PUT_LINE('       Error: '  || v_error_pase);
         -- Si fallo la inserci�n del pase, no continuo
         if v_error_pase like IPJ.TYPES.C_RESP_OK || '%' then
           DBMS_OUTPUT.PUT_LINE('    - SP Aceptar Pases (Reabrir): ' );
           IPJ.TRAMITES_SUAC.SP_Aceptar_Pase(
             p_nro_sticker_completo => v_SUAC.v_nro_sticker_completo,
             p_cod_unidad_receptora => v_area_suac_destino,
             p_usuario_receptor => (p_cuil_usuario_destino),
             p_cuerpos => v_SUAC.v_cuerpos,
             p_folios => v_SUAC.v_fojas,
             o_rdo => v_error_pase,
             o_tipo_mensaje => o_tipo_mensaje);

           DBMS_OUTPUT.PUT_LINE('       Error: '  || v_error_pase);
           -- Si no se pudoo aceptar el pase, lo informo
           if v_error_pase like IPJ.TYPES.C_RESP_OK || '%' then
              o_rdo := IPJ.TYPES.C_RESP_OK;
              o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
           else
             o_rdo :=  v_error_pase ;
             o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
           end if;
         else
           o_rdo :=  v_error_pase ;
           o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
           return;
         end if;
       end if;

     else
       o_rdo :=  'No se encontr� datos de Origen o Destino para transferir el tr�mite.' ;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
     end if;

     DBMS_OUTPUT.PUT_LINE('    - O_Rdo ' || o_rdo );
   END SP_Realizar_Tranf_IPJ;

  PROCEDURE SP_Crear_Tramite(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_sticker out VARCHAR2,
    o_sticker_iniciador out VARCHAR2,
    o_expediente out varchar2,
    o_id_tramite_suac out number,
    p_cod_reparticion in varchar2,
    p_cod_mesa in varchar2,
    p_tipo_tramite in number,
    p_subtipo_tramite in Number,
    p_asunto in varchar2,
    p_tipo_persona in number, --1 = Persona fisica // 3 = Persona juridica // 4 = Unidad del organigrama //  5 = Unidad externa
    p_apellido in varchar2, -- Apellido de Persona F�sica o Raz�n Social de Empresa
    p_nombre in varchar2,
    p_tipo_documento in varchar2,
    p_nro_documento in varchar2,
    p_id_sexo in varchar2,
    p_pai_cod_pais in varchar2,
    p_id_numero in number,
    p_tipo_expediente in varchar2
    )
  IS
  /* Crea un nuevo expediente en Mesa SUAC */
    v_usuario varchar2(20);
    v_row_relacion IPJ.T_RELACION_SUAC_GESTION%rowtype;
  BEGIN
    --buso el usuario responsable del area indicada
    begin
      -- solo para testing, no existe la vista
      --select 'd021340069' ID_RESP_MESA into v_usuario from dual;

      select ID_RESP_MESA into v_usuario
      from nuevosuac.VT_GET_REP_IPJ
      where
        CODIGO_REPARTICION = p_cod_reparticion;

    exception
      when OTHERS then
        o_rdo :=  'No se encuentra un responsable asociada al Area.';
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
        return;
    end;

    --    el usuario lo puedo obtener con nuevosuac.fn_get_nombre_persona
    NUEVOSUAC.PCK_INTERFAZ_VERTICALES_V2.P_INSERTAR_TRA_EXT(
      i_usuario => v_usuario, -- es el nombre de usuario Notes de un integrante de la mesa de entradas, ejemplo Monica Vallejo/GOBCBA. En el caso de que no exista en la tabla T_USUARIOS_SUAC_RUP
      i_cod_reparticion => p_cod_reparticion, --nuevosuac.t_unidades es el campo c�digo e identificas a las reparticiones como aquellas que tienen n�mero de historia (campo nro_historia)
      i_cod_mesa => p_cod_mesa, --la tabla nuevosuac.t_unidades, identificas a las mesas con el campo (nro_sume)
      i_fecha_creacion => sysdate,
      i_tipo_expediente => nvl(p_tipo_expediente, 'N'), -- Creo una Nota, y deben pasar a Expediente
      i_tipo_tramite => p_tipo_tramite, -- la descripci�n del tipo de tr�mite (utilizar pr_get_tipos, ver ejemplo se debe pasar el ID)
      i_subtipo_tramite => p_subtipo_tramite,--la descripci�n del subtipo de tr�mite dependiente del tipo de tr�mite, (utilizar pr_get_subtipos, se debe pasar el ID)
      i_asunto => p_asunto,
      i_tipo_persona => p_tipo_persona,-- persona f�sica, persona jur�dica, unidad del organigrama o unidad externa
      i_apellido => p_apellido, -- el apellido del iniciador, en caso de ser una persona jur�dica aqu� debe ir la raz�n social
      i_nombre => p_nombre,
      i_tipo_documento => p_tipo_documento,-- las siglas del tipo de documento del iniciador (DNI, LC,...). (Para consultar valores ver la vista: RCIVIL.VT_TIPOS_DOCUMENTOS)
      i_numero_documento => p_nro_documento,
      i_sexo => p_id_sexo,
      i_pai_cod_pais => p_pai_cod_pais,
      i_id_numero => p_id_numero,
      i_calle => null, --IN  VARCHAR2, -- nombre calle del iniciador
      i_altura => null, --IN  VARCHAR2,
      i_piso => null, --IN  VARCHAR2,
      i_dpto => null, --IN  VARCHAR2,
      i_barrio => null, --IN VARCHAR2, -- nombre brarrio del iniciador
      i_localidad => null, --IN VARCHAR2, -- nombre localidad del iniciador
      i_departamento => null, --IN  VARCHAR2, -- nombre departamento del iniciador
      i_provincia => null, --IN VARCHAR2, -- nombre de la provincia del iniciador
      i_telefono => null, --IN VARCHAR2, -- del iniciador
      i_codigo_postal => null, --IN VARCHAR2,
      i_email => null, --IN VARCHAR2, -- del iniciador
      o_sticker => o_sticker, --el n�mero de sticker de control interno generado para el tr�mite
      o_sticker_iniciador => o_sticker_iniciador, --el sticker que se le entrega al iniciador del tr�mite
      o_error => o_rdo,
      i_cuerpos => 1,-- ????
      i_folios => 1, -- ????
      i_nuevo => 'S', -- indica si el tr�mite que se est� insertando es nuevo o uno ya existente con n�mero no registrado en el NuevoSuac (N->Nuevo, S->Iniciado)
      i_numero_tramite_ini => null, --: par�metro de entrada para el n�mero de tr�mite en caso de que el mismo sea un expediente iniciado.
      o_numero_tramite => o_expediente --par�metro de salida con el n�mero de tr�mite generado
     );

    if o_rdo is not null then
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
    else
      o_id_tramite_suac := FC_Buscar_ID_SUAC (o_sticker);
      o_rdo := IPJ.TYPES.c_Resp_OK;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    end if;

  exception
    when OTHERS then
      o_rdo :=  To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Crear_Tramite;

  PROCEDURE SP_Subir_Notas_SUAC(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_cant_sincroniz out number,
    o_sticker out varchar,
    p_id_tramite_ipj in number,
    p_transaccion char)
  IS
  /**********************************************************
     Sube a SUAC las notas de un determinado tramite
     El formato del cursor recibido deber� ser un registro por entrada de la grilla con 3 columnas divididas por el caracter especial "~"
       - La primera columna es el orden con que se visualizar� el registro en la grilla,
       - La segunda columna es la observaci�n en si misma
       - La tercera columna es si esta Cumplimentada (suele ser fecha y OK).
     No deben enviarse dentro del registro otros caracteres especiales fuera del mencionado.
  **********************************************************/
    v_CursorNotas  types.cursorType;
    v_existe number;
    v_row_tramite ipj.t_tramitesipj%rowtype;
  BEGIN
    -- Busco el tramite SUAC
    select * into v_row_tramite
    from IPJ.t_tramitesipj
    where
      id_tramite_ipj = p_id_tramite_ipj;

    -- Si es un tr�mite d�gital, no se cambia de �rea y se encuentra en estudio, no sincroniza con SUAC
    if IPJ.TRAMITES.FC_ES_TRAM_DIGITAL(p_id_tramite_ipj) > 0 and
      v_row_tramite.id_ubicacion = v_row_tramite.id_ubicacion_origen and v_row_tramite.id_estado_ult in (1, 2, 3, 4, 10, 15, 16, 17, 18) then
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
      o_rdo := IPJ.TYPES.C_RESP_OK;
      o_cant_sincroniz := 0;
      return;
    end if;

    -- Si no tiene tramite SUAC, no hago nada
    if nvl(v_row_tramite.id_tramite, 0) <> 0 then
      -- Verifico si hay alguna nota nueva para sincronizar.
      select count(1) into v_existe
      from ipj.t_entidades_notas
        where
          id_tramite_ipj = p_id_tramite_ipj and
          Informar_Suac = 'S';

      if v_existe > 0 then
        OPEN v_CursorNotas FOR
          select FC_Formatear_Nota(nro, observacion, fec_cumpl, id_estado_nota) Formateado
          from IPJ.t_entidades_notas
          where
            id_tramite_ipj = p_id_tramite_ipj
          order by nro asc;

        -- No existe en testing, devuelvo siempre OK
        --o_rdo := IPJ.TYPES.C_RESP_OK;
        NUEVOSUAC.PCK_NUEVOSUAC_IPJ.PR_UPDATE_OBSERVACIONES_IPJ(
          p_id_tramite => v_row_tramite.id_tramite,
          p_observaciones => v_CursorNotas,
          o_estado => o_rdo
        );
        Close v_CursorNotas;

        if nvl(o_rdo, IPJ.TYPES.C_RESP_OK) = IPJ.TYPES.C_RESP_OK then
          -- Marco todas las notas con informadas
          update ipj.t_entidades_notas
          set
            Informar_Suac = 'N',
            es_nuevo = 'N'
          where
            id_tramite_ipj = p_id_tramite_ipj;
        end if;
      end if;
    else
      o_rdo := IPJ.TYPES.C_RESP_OK;
    end if;

    -- Dependiendo de la respuesta, marco OK o ERROR
    o_cant_sincroniz := nvl(v_existe, 0);
    o_sticker := v_row_tramite.sticker;
    if nvl(o_rdo, IPJ.TYPES.C_RESP_OK) like IPJ.TYPES.C_RESP_OK || '%' then
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
      o_rdo := IPJ.TYPES.C_RESP_OK;
      if p_transaccion = 'S' then
        commit;
      end if;
    else
      o_rdo := 'No se pudo sincronizar Notificaciones con SUAC (' || o_rdo || ').';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      o_cant_sincroniz := 0;
      if p_transaccion = 'S' then
        rollback;
      end if;
    end if;

  exception
    when NO_DATA_FOUND then
      o_rdo :=  To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      o_cant_sincroniz := 0;
      rollback;
  END SP_Subir_Notas_SUAC;

  PROCEDURE SP_Bajar_Notas_SUAC(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in number)
  IS
  /**********************************************************
     Trae de SUAC las notas de un determinado tramite
     El formato del cursor recibido deber� ser un registro por entrada de la grilla con 3 columnas divididas por el caracter especial "~"
       - La primera columna es el orden con que se visualizar� el registro en la grilla,
       - La segunda columna es la observaci�n en si misma
       - La tercera columna es si esta Cumplimentada (suele ser fecha y OK).
     No deben enviarse dentro del registro otros caracteres especiales fuera del mencionado.
  **********************************************************/
    v_CursorNotas   types.cursorType ;
    v_id_tramite_SUAC number(10);
    v_cant_notas number;
    v_id_tramite number;
    v_id_componente number;
    v_n_componente varchar2(30);
    v_etiqueta varchar2(30);
    v_valores varchar2(4000);
    v_columnas varchar2(200);
    v_estado number(10);
    v_Nro_Orden varchar2(100);
    v_Observacion varchar2(4000);
    v_cumplimiento varchar2(4000);
    v_id_legajo number;
    v_hay_repetidos number;
    v_cuil_tramite varchar2(20);
    v_nro_linea number;
    v_existe_nro number;
  BEGIN
    o_rdo := IPJ.TYPES.C_RESP_OK;
    -- Si el tramite ya tiene observaciones en Gestion, no traigo nada
    select count(1) into v_cant_notas
    from ipj.t_entidades_notas
    where
      id_tramite_ipj = p_id_tramite_ipj;

    if v_cant_notas = 0 then
      -- Busco el tramite SUAC
      select id_tramite, cuil_ult_estado into v_id_tramite_SUAC, v_cuil_tramite
      from IPJ.t_tramitesipj
      where
        id_tramite_ipj = p_id_tramite_ipj;

      --if v_cuil_tramite <> '1' then
        -- me quedo con el primer legajo
        begin
          select id_legajo into v_id_legajo
          from ipj.t_tramitesipj_persjur
          where
            id_tramite_ipj = p_id_tramite_ipj and
            rownum = 1;
        exception
          when NO_DATA_FOUND then
            v_id_legajo := null;
        end;

        -- Si no tiene tramite SUAC, no hago nada
        if (nvl(v_id_tramite_SUAC, 0) <> 0) then
          -- Busco las Notas en SUAC (no existe en testing, devuelvo OK)
          --o_rdo := IPJ.TYPES.C_RESP_OK;
          DBMS_OUTPUT.PUT_LINE(' Busca Observaciones SUAC Tramite = ' || to_char(v_id_tramite_SUAC));
          NUEVOSUAC.PCK_NUEVOSUAC_IPJ.PR_GET_OBSERVACIONES_IPJ (
            p_id_tramite => v_id_tramite_SUAC,
            P_resultado => v_CursorNotas,
            o_estado => o_rdo
          );

          if nvl(o_rdo, IPJ.TYPES.C_RESP_OK) = IPJ.TYPES.C_RESP_OK and v_CursorNotas is not null then
           -- Parseo las lineas y las inserto en la base.
           v_nro_linea := 0;
            LOOP
              FETCH v_CursorNotas INTO  v_id_tramite, v_id_componente, v_n_componente, v_etiqueta, v_valores, v_columnas, v_estado;
              EXIT WHEN v_CursorNotas%NOTFOUND or v_CursorNotas%NOTFOUND is null;

              if v_valores <> '~~' then
                v_nro_linea := v_nro_linea +1;

                DBMS_OUTPUT.PUT_LINE('        - Linea = ' || v_valores);
                v_nro_orden := substr(v_valores, 1, Instr(v_valores, IPJ.Types.c_Separador_SUAC, 1, 1)-1);
                -- Si no tiene n�mero de orden, saleto el ~ y sigo con el texto
                if v_nro_orden is null then
                  v_Observacion := substr(v_valores, 2, Instr(v_valores, IPJ.Types.c_Separador_SUAC, 1, 2)-(2));
                  v_cumplimiento := substr(v_valores, length(v_Observacion)+3, length(v_valores));
                else
                  v_Observacion := substr(v_valores, length(v_nro_orden)+2, Instr(v_valores, IPJ.Types.c_Separador_SUAC, 1, 2)-(length(v_nro_orden)+2));
                  v_cumplimiento := substr(v_valores, length(v_nro_orden)+length(v_Observacion)+3, length(v_valores));

                  -- si el indice esta repetido, lo paso nulo
                  select count(1) into v_existe_nro
                  from ipj.t_entidades_notas
                  where
                    id_tramite_ipj = p_id_tramite_ipj and
                    nro = to_number(v_nro_orden);

                  if v_existe_nro > 0 then
                    v_nro_orden := null;
                  end if;
                end if;

               -- Si viene algun texto, lo cargo
                if nvl(length(v_observacion), 0) > 0 then
                  --  Inserto la notificacion en el tr�mite: estado 0 = Pendiente / 1 = OK
                  Insert Into Ipj.T_Entidades_Notas (Id_Tramite_Ipj, Id_Legajo, Nro, Fec_Modif,
                     Id_Estado_Nota, Fec_Cumpl, Observacion, Informar_Suac, es_nuevo)
                  values (p_id_tramite_ipj, v_id_legajo, nvl(to_number(v_nro_orden), v_nro_linea * -1), null,
                    (Case when upper(v_cumplimiento) like '%OK%' then 1 else 0 end), null,
                     (case
                       when upper(v_cumplimiento) like '%OK%' then v_Observacion
                       when upper(v_cumplimiento) not like '%OK%' and v_cumplimiento is null then v_Observacion
                       when v_cumplimiento is null then v_Observacion
                       else v_cumplimiento||chr(13) ||v_Observacion end),
                     'N', 'N');
                end if;
              end if;

            END LOOP;

            close v_CursorNotas;

            --Si hay numeros negativos, es por orden nulos o repetidos, entondes reindexo
            begin
              select count(1) into v_hay_repetidos
              from ipj.T_Entidades_Notas
              where
                id_tramite_ipj = p_id_tramite_ipj and
                nro < 0;
            exception
              when NO_DATA_FOUND then
                v_hay_repetidos := 0;
            end;

            -- Si hay repetidos, los ordeno segun en nro de fila.
            if v_hay_repetidos > 0 then
              update ipj.T_Entidades_Notas
              set nro = rownum
              where
               id_tramite_ipj = p_id_tramite_ipj;
            end if;
          else
            o_rdo := IPJ.TYPES.C_RESP_OK;
          end if;

        end if;
      --end if;
    end if;

    -- Dependiendo de la respuesta, marco OK o ERROR
    if o_rdo like IPJ.TYPES.C_RESP_OK || '%' then
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    else
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
    end if;

  exception
    when NO_DATA_FOUND then
      o_rdo :=  'SP_Bajar_Notas_SUAC: ' || To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
    when OTHERS then
      o_rdo :=  'SP_Bajar_Notas_SUAC: ' || To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Bajar_Notas_SUAC;

  PROCEDURE SP_Anexar_Notas_SUAC(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_suac_anexo in number,
    p_id_tramite_ipj_padre in number)
  IS
  /**********************************************************
     Trae de SUAC las notas de un determinado tramite, y las asigna al tr�mite padre
     El formato del cursor recibido deber� ser un registro por entrada de la grilla con 3 columnas divididas por el caracter especial "~"
       - La primera columna es el orden con que se visualizar� el registro en la grilla,
       - La segunda columna es la observaci�n en si misma
       - La tercera columna es si esta Cumplimentada (suele ser fecha y OK).
     No deben enviarse dentro del registro otros caracteres especiales fuera del mencionado.
  **********************************************************/
    v_CursorNotas_Anexo  types.cursorType ;
    v_Cursor_Tramite  types.cursorType;
    v_SUAC_Anexo Tipo_SUAC;
    v_id_tramite number;
    v_id_componente number;
    v_n_componente varchar2(30);
    v_etiqueta varchar2(30);
    v_valores varchar2(4000);
    v_columnas varchar2(200);
    v_estado number(10);
    v_Nro_Orden varchar2(100);
    v_Observacion varchar2(4000);
    v_cumplimiento varchar2(100);
    v_nro_orden_padre number;
    v_id_tramite_ipj_anexo number;
    v_error varchar2(2000);
    v_hay_notas number;
  BEGIN
    o_rdo := IPJ.TYPES.C_RESP_OK;

    -- Busco el tramite SUAC ANEXO
    NUEVOSUAC.PCK_INTERFAZ_VERTICALES_V2.P_GET_TRAMITE_BY_ID(p_id_tramite_suac_anexo, v_Cursor_Tramite, v_error);
    if nvl(v_error, IPJ.TYPES.C_RESP_OK) = IPJ.TYPES.C_RESP_OK then
      loop
        fetch v_Cursor_Tramite into v_SUAC_Anexo;

        EXIT WHEN v_Cursor_Tramite%NOTFOUND or v_Cursor_Tramite%NOTFOUND is null;
      end loop;
      close v_Cursor_Tramite;

      -- Veo si ya se cargaron las notas para ese expediente
      select count(1) into v_hay_notas
      from ipj.t_entidades_notas
      where
        id_tramite_ipj = p_id_tramite_ipj_padre and
        observacion like v_SUAC_Anexo.v_nro_tramite || '%';

      -- Si no tiene tramite SUAC o ya se pasaron las notas, no hago nada
      if (nvl(v_SUAC_Anexo.v_id, 0) <> 0) and v_hay_notas = 0 then
        -- Busco las Notas en SUAC (no existe en testing, devuelvo OK)
        DBMS_OUTPUT.PUT_LINE(' Busca Observaciones SUAC Tramite = ' || to_char(v_SUAC_Anexo.v_id));
        NUEVOSUAC.PCK_NUEVOSUAC_IPJ.PR_GET_OBSERVACIONES_IPJ (
          p_id_tramite => v_SUAC_Anexo.v_id,
          P_resultado => v_CursorNotas_Anexo,
          o_estado => o_rdo
        );

        if nvl(o_rdo, IPJ.TYPES.C_RESP_OK) = IPJ.TYPES.C_RESP_OK and v_CursorNotas_Anexo is not null then
         -- Parseo las lineas y las inserto en el expediente padre
          LOOP
            FETCH v_CursorNotas_Anexo INTO  v_id_tramite, v_id_componente, v_n_componente, v_etiqueta, v_valores, v_columnas, v_estado;
            EXIT WHEN v_CursorNotas_Anexo%NOTFOUND or v_CursorNotas_Anexo%NOTFOUND is null;

            if v_valores <> '~~' then
              DBMS_OUTPUT.PUT_LINE('        - Linea = ' || v_valores);
              v_nro_orden := substr(v_valores, 1, Instr(v_valores, IPJ.Types.c_Separador_SUAC, 1, 1)-1);
              v_Observacion := substr(v_valores, length(v_nro_orden)+2, Instr(v_valores, IPJ.Types.c_Separador_SUAC, 1, 2)-(length(v_nro_orden)+2));
              v_cumplimiento := substr(v_valores, length(v_nro_orden)+length(v_Observacion)+3, length(v_valores));

              -- Busco en que nro se asigna las notas
              select max(nro) +1 into v_nro_orden_padre
              from ipj.t_entidades_notas
              where
                id_tramite_ipj = p_id_tramite_ipj_padre;

              if nvl(length(v_observacion), 0) > 0 then
                --  Inserto la notificacion en el tr�mite: estado 0 = Pendiente / 1 = OK
                Insert Into Ipj.T_Entidades_Notas (Id_Tramite_Ipj, Id_Legajo, Nro, Fec_Modif,
                   Id_Estado_Nota, Fec_Cumpl, Observacion, Informar_Suac)
                values (p_id_tramite_ipj_padre, null, v_nro_orden_padre, null,
                  (Case when v_cumplimiento is null then 0 else 1 end), null,
                  v_SUAC_Anexo.v_nro_tramite || ' (' || v_nro_orden || '): ' || v_Observacion, 'S');
              end if;
            end if;

          END LOOP;
          close v_CursorNotas_Anexo;

        else
          o_rdo := IPJ.TYPES.C_RESP_OK;
        end if;
      end if;
    end if;

    -- Dependiendo de la respuesta, marco OK o ERROR
    if o_rdo like IPJ.TYPES.C_RESP_OK || '%' then
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    else
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
    end if;

  exception
    when NO_DATA_FOUND then
      o_rdo :=  To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
    when OTHERS then
      o_rdo :=  To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Anexar_Notas_SUAC;

 PROCEDURE SP_Subir_Datos_Part(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in number)
  IS
  /**********************************************************
     Sube a SUAC los nombres de los autorizados para un tr�mite
  **********************************************************/
    v_CursorAutoriz  types.cursorType;
    v_Row_Autoriz IPJ.t_entidades_autorizados%ROWTYPE;
    v_SUAC Tipo_SUAC;
    v_id_tramite_SUAC number(10);
    v_existe_informar number;
    v_existe_total number;
    v_lista_autoriz varchar2(2000);
    v_nombre varchar2(200);
    v_id_ubicacion number(6);
    v_ubicacion_origen number(6);
    v_cuil_tramite varchar2(20);
    v_cod_reparticion varchar2(50);
    v_campo varchar2(50);
    v_responsable varchar2(200);
    v_fec_estudio date;
    v_denominacion varchar2(250);
    v_denominacion_new varchar2(250);
    v_mensaje_final varchar2(1000);
    v_codigo_online number;
    v_Es_Const_OL number;
    v_tipo_suac varchar2(30);
  BEGIN
    -- Busco el tramite SUAC
    select id_tramite, id_ubicacion, cuil_ult_estado, id_ubicacion_origen, codigo_online
      into v_id_tramite_SUAC, v_id_ubicacion, v_cuil_tramite, v_ubicacion_origen, v_codigo_online
    from IPJ.t_tramitesipj
    where
      id_tramite_ipj = p_id_tramite_ipj;

    -- Si es un tramite online
    if nvl(v_codigo_online, 0) > 0 then
      -- Veo si es de constituci�n, para esperar a que traigan los documentos.
      select count(1) into v_Es_Const_OL
      from ipj.t_ol_entidades
      where
        codigo_online = v_codigo_online and
        id_tipo_tramite_ol in (1, 19, 20); -- SA, FID, SAS

      -- Busco el tipo en suac, para ver si lo pasan a expediente
      begin
        select nota_expediente into v_Tipo_Suac
        from NUEVOSUAC.VT_TRAMITES_IPJ_DESA
        where
          id_tramite = v_id_tramite_SUAC;
      exception
        when NO_DATA_FOUND then
          v_Tipo_Suac := null;
      end;
    else
      v_Es_Const_OL := 0;
    end if;

    --if v_cuil_tramite <> '1' then
      -- Si no tiene tramite SUAC, no hago nada
      if nvl(v_id_tramite_SUAC, 0) <> 0 then
        -- Verifico si hay autorizados para el tr�mite.
        select SUM(case when nvl(Informar_Suac, 'S') = 'S' then 1 else 0 end), count(1) into v_existe_informar, v_existe_total
        from ipj.t_entidades_autorizados
        where
          id_tramite_ipj = p_id_tramite_ipj;

        if v_existe_informar > 0 then
          OPEN v_CursorAutoriz FOR
            select *
            from IPJ.t_entidades_autorizados
            where
              id_tramite_ipj = p_id_tramite_ipj;

          LOOP
            FETCH v_CursorAutoriz INTO  v_Row_Autoriz;
            EXIT WHEN v_CursorAutoriz%NOTFOUND or v_CursorAutoriz%NOTFOUND is null;

            select (select p.nombre||' '||p.apellido from rcivil.vt_pk_persona p where p.id_sexo = i.id_sexo and p.nro_documento = i.nro_documento and p.pai_cod_pais = i.pai_cod_pais and p.id_numero = i.id_numero) into v_nombre
            from ipj.t_integrantes i
            where
              id_integrante = v_Row_Autoriz.id_integrante;

            if v_lista_autoriz is not null then
              v_lista_autoriz := v_lista_autoriz || ', ';
            end if;
            v_lista_autoriz := v_lista_autoriz || v_nombre ;
          END LOOP;
          close v_CursorAutoriz;
        else
          -- Si no hay nada, dejo NULL y dejo lo que habia en SUAC
          v_lista_autoriz := null;
        end if;

        -- Busco los codigos necesarios
        DBMS_OUTPUT.PUT_LINE(' Subir Datos Particulares Tramite SUAC = ' || to_char(v_id_tramite_SUAC));
        SP_Buscar_Suac (
          o_rdo => o_rdo,
          o_tipo_mensaje => o_tipo_mensaje,
          o_SUAC => v_SUAC,
          p_id_tramite => v_id_tramite_SUAC,
          p_loguear => 'S');

        DBMS_OUTPUT.PUT_LINE('      Buscar Datos SUAC = ' || o_rdo);

        -- Si encuentro los datos del tramite, actualizo los datos particulares
        if o_rdo = IPJ.TYPES.C_RESP_OK then
          v_cod_reparticion := FC_Codigo_Unidad_Suac(v_SUAC.v_reparticion_caratuladora);
          DBMS_OUTPUT.PUT_LINE('           - Reparticion = ' || v_SUAC.v_reparticion_caratuladora);
          DBMS_OUTPUT.PUT_LINE('           - Cod Reparticion = ' || v_cod_reparticion);

          -- **************** AUTORIZADOS **********************
          v_campo:=
            case v_cod_reparticion
              when 'DIPJ01' then 'txt_autorizados_ipj'
              when 'DSFDIPJ01' then 'txt_autorizados_ipjSF'
              when 'DBVDIPJ01' then 'txt_autorizados_ipjBV'
              when 'DIPJRC01' then 'txt_autorizados_ipjRC'
              when 'DVDIPJ01' then 'txt_autorizados_vd'
              when 'DVMDIPJ01' then 'txt_autorizados_vm'
              when 'DMCDIPJ01' then 'txt_autorizados'
              else null
            end;

          DBMS_OUTPUT.PUT_LINE('           AUTORIZADOS' );
          DBMS_OUTPUT.PUT_LINE('                     - Sticker = ' || v_SUAC.v_nro_sticker_completo);
          DBMS_OUTPUT.PUT_LINE('                     - Campo = ' || v_campo);
          DBMS_OUTPUT.PUT_LINE('                     - Valor = ' || v_lista_autoriz);
          if v_lista_autoriz is not null and v_campo is not null then
            NUEVOSUAC.PCK_INTERFAZ_VERTICALES_V2.P_ACT_DATOS_PARTICULARES (
              i_sticker => v_SUAC.v_nro_sticker_completo,
              i_nombre_campo => v_campo,
              i_nombre_subform => 'SF' || v_cod_reparticion,
              i_valor => v_lista_autoriz,
              o_error => o_rdo
            );

            DBMS_OUTPUT.PUT_LINE('                     - O_Rdo = ' || o_rdo);
            if nvl(o_rdo, Ipj.Types.C_Resp_Ok) <> Ipj.Types.C_Resp_Ok then
              v_mensaje_final := v_mensaje_final || (case when v_mensaje_final is not null then ' - ' else '' end) || o_rdo;
            end if;
          end if;

          -- **************** NRO TRAMITE ****************
          v_campo:=
            case v_cod_reparticion
              when 'DIPJ01' then 'txt_nroTramite_ipj'
              when 'DSFDIPJ01' then 'txt_nroTramite_ipjSF'
              when 'DBVDIPJ01' then 'txt_nroTramite_ipjBV'
              when 'DIPJRC01' then 'txt_nroTramite_ipjRC'
              when 'DVDIPJ01' then 'txt_ntramite_vd'
              when 'DVMDIPJ01' then 'txt_tramite_vm'
              when 'DMCDIPJ01' then 'txt_tramite'
              else null
            end;

          if v_campo is not null  then
            NUEVOSUAC.PCK_INTERFAZ_VERTICALES_V2.P_ACT_DATOS_PARTICULARES (
              i_sticker => v_SUAC.v_nro_sticker_completo,
              i_nombre_campo => v_campo,
              i_nombre_subform => 'SF' || v_cod_reparticion,
              i_valor => v_SUAC.v_nro_tramite ,
              o_error => o_rdo
            );
            DBMS_OUTPUT.PUT_LINE('           NRO. TRAMITE' );
            DBMS_OUTPUT.PUT_LINE('                     - Sticker = ' || v_SUAC.v_nro_sticker_completo);
            DBMS_OUTPUT.PUT_LINE('                     - Campo = ' || v_campo);
            DBMS_OUTPUT.PUT_LINE('                     - Valor = ' || v_SUAC.v_nro_tramite);
            DBMS_OUTPUT.PUT_LINE('                     - O_Rdo = ' || o_rdo);
            if nvl(o_rdo, Ipj.Types.C_Resp_Ok) <> Ipj.Types.C_Resp_Ok then
              v_mensaje_final := v_mensaje_final || (case when v_mensaje_final is not null then ' - ' else '' end) || o_rdo;
            end if;
          end if;

          -- **************** RESPONSABLE ****************
          if nvl(v_cuil_tramite, '1') = '1' then
            v_responsable := Null;
          else
            select descripcion into v_responsable
            from ipj.t_usuarios
            where cuil_usuario = v_cuil_tramite;
          end if;

          v_campo:=
            case v_cod_reparticion
              when 'DIPJ01' then 'txt_responsable_ipj'
              when 'DSFDIPJ01' then 'txt_responsable_ipjSF'
              when 'DBVDIPJ01' then 'txt_responsable_ipjBV'
              when 'DIPJRC01' then 'txt_responsable_ipjRC'
              when 'DVDIPJ01' then 'txt_responsable_vd'
              when 'DVMDIPJ01' then 'txt_responsable_vm'
              when 'DMCDIPJ01' then 'txt_responsable'
              else null
            end;

          DBMS_OUTPUT.PUT_LINE('           RESPONSABLE' );
          DBMS_OUTPUT.PUT_LINE('                     - Sticker = ' || v_SUAC.v_nro_sticker_completo);
          DBMS_OUTPUT.PUT_LINE('                     - Campo = ' || v_campo);
          DBMS_OUTPUT.PUT_LINE('                     - Valor = ' || v_responsable);
          if v_responsable is not null and v_campo is not null then
            NUEVOSUAC.PCK_INTERFAZ_VERTICALES_V2.P_ACT_DATOS_PARTICULARES (
              i_sticker => v_SUAC.v_nro_sticker_completo,
              i_nombre_campo => v_campo,
              i_nombre_subform => 'SF' || v_cod_reparticion,
              i_valor => v_responsable ,
              o_error => o_rdo
            );

            DBMS_OUTPUT.PUT_LINE('                     - O_Rdo = ' || o_rdo);
            if nvl(o_rdo, Ipj.Types.C_Resp_Ok) <> Ipj.Types.C_Resp_Ok then
              v_mensaje_final := v_mensaje_final || (case when v_mensaje_final is not null then ' - ' else '' end) || o_rdo;
            end if;
          end if;

          -- **************** FECHA INICIO ****************
          -- Si el tramite es OL, la fecha de inicio se informa a partir de que traigan los documentos y cambien el tr�mite de NOTA a EXPEDIENTE
          if v_Es_Const_OL = 0 or nvl(v_tipo_suac, 'NOTA') = 'EXPEDIENTE' then
            v_campo:=
              case v_cod_reparticion
                when 'DIPJ01' then 'txt_fechaIngreso_ipj'
                when 'DSFDIPJ01' then 'txt_fingreso_ipjSF'
                when 'DBVDIPJ01' then 'txt_fingreso_ipjBV'
                when 'DIPJRC01' then 'txt_fingreso_ipjRC'
                when 'DVDIPJ01' then 'txt_fechaing_vd'
                when 'DVMDIPJ01' then 'txt_fecha_vm'
                when 'DMCDIPJ01' then 'txt_fechaingreso'
                else null
              end;

            if v_campo is not null then
              NUEVOSUAC.PCK_INTERFAZ_VERTICALES_V2.P_ACT_DATOS_PARTICULARES (
                i_sticker => v_SUAC.v_nro_sticker_completo,
                i_nombre_campo => v_campo,
                i_nombre_subform => 'SF' || v_cod_reparticion,
                i_valor => v_SUAC.v_fecha_inicio, --v_SUAC.v_fecha_creacion
                o_error => o_rdo
              );
              DBMS_OUTPUT.PUT_LINE('           FECHA INICIO' );
              DBMS_OUTPUT.PUT_LINE('                     - Sticker = ' || v_SUAC.v_nro_sticker_completo);
              DBMS_OUTPUT.PUT_LINE('                     - Campo = ' || v_campo);
              DBMS_OUTPUT.PUT_LINE('                     - Valor = ' || v_SUAC.v_fecha_inicio);
              DBMS_OUTPUT.PUT_LINE('                     - O_Rdo = ' || o_rdo);
              if nvl(o_rdo, Ipj.Types.C_Resp_Ok) <> Ipj.Types.C_Resp_Ok then
                v_mensaje_final := v_mensaje_final || (case when v_mensaje_final is not null then ' - ' else '' end) || o_rdo;
              end if;
            end if;
          end if;

          -- **************** FECHA ESTUDIO ****************
          /*  Por el momento no se va a informar, porque trae confusion
          begin
            select min(to_date(h.fecha_pase, 'dd/mm/rrrr')) into v_fec_estudio
            from ipj.t_tramitesipj_Acciones ac join ipj.t_tramitesipj_acciones_estado h
              on ac.Id_Tramiteipj_Accion = h.Id_Tramiteipj_Accion and ac.id_tramite_ipj = h.id_tramite_ipj and ac.id_tipo_accion = h.id_tipo_accion
            where
              ac.id_tramite_ipj = p_id_tramite_ipj and
              h.Id_Estado = IPJ.TYPES.C_ESTADOS_PROCESO and
              ac.id_tipo_accion in
                (select id_tipo_accion
                 from ipj.t_tipos_accionesipj ta join ipj.t_tipos_clasif_ipj tc
                   on ta.id_clasif_ipj =tc.id_clasif_ipj
                 where id_ubicacion = v_ubicacion_origen);
          exception
            when OTHERS then
              v_fec_estudio := null;
          end;

          -- Si no encontre algo en estado EN PROCESO, busco el primer historial
          if v_fec_estudio is null then
            begin
              select min(to_date(h.fecha_pase, 'dd/mm/rrrr')) into v_fec_estudio
              from ipj.t_tramitesipj_Acciones ac join ipj.t_tramitesipj_acciones_estado h
                on ac.Id_Tramiteipj_Accion = h.Id_Tramiteipj_Accion and ac.id_tramite_ipj = h.id_tramite_ipj and ac.id_tipo_accion = h.id_tipo_accion
              where
                ac.id_tramite_ipj = p_id_tramite_ipj and
                ac.id_tipo_accion in
                  (select id_tipo_accion
                   from ipj.t_tipos_accionesipj ta join ipj.t_tipos_clasif_ipj tc
                     on ta.id_clasif_ipj =tc.id_clasif_ipj
                   where id_ubicacion = v_ubicacion_origen);
            exception
              when OTHERS then
                v_fec_estudio := null;
            end;
          end if;

          v_campo:=
            case v_cod_reparticion
              when 'DIPJ01' then 'txt_fechaEstudio_ipj'
              when 'DSFDIPJ01' then 'txt_festudio_ipjSF'
              when 'DBVDIPJ01' then 'txt_festudio_ipjBV'
              when 'DIPJRC01' then 'txt_festudio_ipjRC'
              when 'DVDIPJ01' then 'txt_fechaest_vd'
              when 'DVMDIPJ01' then 'fecha_estudio_vm'
              when 'DMCDIPJ01' then 'txt_fechaestudio'
              else null
            end;

          DBMS_OUTPUT.PUT_LINE('           FECHA ESTUDIO' );
          DBMS_OUTPUT.PUT_LINE('                     - Sticker = ' || v_SUAC.v_nro_sticker_completo);
          DBMS_OUTPUT.PUT_LINE('                     - Campo = ' || v_campo);
          DBMS_OUTPUT.PUT_LINE('                     - Valor = ' || to_char(v_fec_estudio, 'dd-MON-rr'));
          if v_fec_estudio is not null and v_campo is not null then
            NUEVOSUAC.PCK_INTERFAZ_VERTICALES_V2.P_ACT_DATOS_PARTICULARES (
              i_sticker => v_SUAC.v_nro_sticker_completo,
              i_nombre_campo => v_campo,
              i_nombre_subform => 'SF' || v_cod_reparticion,
              i_valor => to_char(v_fec_estudio, 'dd-MON-rr'),
              o_error => o_rdo
            );

            DBMS_OUTPUT.PUT_LINE('                     - O_Rdo = ' || o_rdo);
            if nvl(o_rdo, Ipj.Types.C_Resp_Ok) <> Ipj.Types.C_Resp_Ok then
              v_mensaje_final := v_mensaje_final || (case when v_mensaje_final is not null then ' - ' else '' end) || o_rdo;
            end if;
          end if;
          */

          -- **************** DENOMINACION ****************
          -- Busco la denominaci�n de entrada en el legajo
          begin
            select denominacion_sia into v_denominacion
            from ipj.t_tramitesipj_persjur tp join ipj.t_legajos l
              on tp.id_legajo = l.id_legajo
            where
              id_tramite_ipj = p_id_tramite_ipj and
              rownum = 1;
          exception
            when NO_DATA_FOUND then
              v_denominacion := null;
          end;

          -- Busco el nombre que se carg� en el tr�mite (por si se modifica)
          begin
            select e.denominacion_1 into v_denominacion_new
            from ipj.t_tramitesipj_persjur tp join ipj.t_entidades e
              on tp.id_tramite_ipj = e.id_tramite_ipj and tp.id_legajo = e.id_legajo
            where
              tp.id_tramite_ipj = p_id_tramite_ipj and
              rownum = 1;
          exception
            when NO_DATA_FOUND then
              v_denominacion_new := null;
          end;

          v_campo:=
            case v_cod_reparticion
              when 'DIPJ01' then 'txt_denominacion_ipj'
              when 'DSFDIPJ01' then 'txt_denominacion_ipjSF'
              when 'DBVDIPJ01' then 'txt_denominacion_ipjBV'
              when 'DIPJRC01' then 'txt_denominacion_ipjRC'
              when 'DVDIPJ01' then 'txt_denomin_vd'
              when 'DVMDIPJ01' then 'txt_denominacion_vm'
              when 'DMCDIPJ01' then 'txt_denominacion'
              else null
            end;

          DBMS_OUTPUT.PUT_LINE('           DENOMINACION' );
          DBMS_OUTPUT.PUT_LINE('                     - Sticker = ' || v_SUAC.v_nro_sticker_completo);
          DBMS_OUTPUT.PUT_LINE('                     - Campo = ' || v_campo);
          DBMS_OUTPUT.PUT_LINE('                     - Valor = ' ||
                (case
                  when v_denominacion <> v_denominacion_new and nvl(v_denominacion_new, '-') <> '-' then v_denominacion_new || ' (antes ' || v_denominacion || ')'
                  else v_denominacion
                end));
          if v_denominacion is not null and v_campo is not null then
            NUEVOSUAC.PCK_INTERFAZ_VERTICALES_V2.P_ACT_DATOS_PARTICULARES (
              i_sticker => v_SUAC.v_nro_sticker_completo,
              i_nombre_campo => v_campo,
              i_nombre_subform => 'SF' || v_cod_reparticion,
              i_valor => (case
                                 when v_denominacion <> v_denominacion_new and nvl(v_denominacion_new, '-') <> '-' then v_denominacion_new || ' (antes ' || v_denominacion || ')'
                                 else v_denominacion
                               end),
              o_error => o_rdo
            );

            DBMS_OUTPUT.PUT_LINE('                     - O_Rdo = ' || o_rdo);
            if nvl(o_rdo, Ipj.Types.C_Resp_Ok) <> Ipj.Types.C_Resp_Ok then
              v_mensaje_final := v_mensaje_final || (case when v_mensaje_final is not null then ' - ' else '' end) || o_rdo;
            end if;
          end if;
        end if;

        if nvl(v_mensaje_final, IPJ.TYPES.C_RESP_OK) = IPJ.TYPES.C_RESP_OK then
          -- Marco todas las notas con informadas
          update ipj.t_entidades_autorizados
          set Informar_Suac = 'N'
          where
            id_tramite_ipj = p_id_tramite_ipj;

          o_rdo := IPJ.TYPES.C_RESP_OK;
        else
          o_rdo := substr(v_mensaje_final, 1, 4000);
        end if;

      else
        o_rdo := IPJ.TYPES.C_RESP_OK;
      end if;
    --end if;

    -- Dependiendo de la respuesta, marco OK o ERROR
    if nvl(o_rdo, IPJ.TYPES.C_RESP_OK) like IPJ.TYPES.C_RESP_OK || '%' then
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
      o_rdo := IPJ.TYPES.C_RESP_OK;
      commit;
    else
      o_rdo := 'No se pudo sincronizar Autorizados con SUAC (' || o_rdo || ').';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      rollback;
    end if;
    DBMS_OUTPUT.PUT_LINE('  Subir Datos Particulares = ' || o_rdo);
  exception
    when NO_DATA_FOUND then
      o_rdo :=  'SP_Subir_Datos_Part: ' || To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      rollback;
  END SP_Subir_Datos_Part;

  FUNCTION FC_Buscar_Stiker_SUAC (p_expediente in varchar2) return varchar2
  IS
    -- Dado el expediente, devuelve el sticker de 14
    v_Cursor_Tramite  types.cursorType;
    v_error varchar2(2000);
    v_SUAC Tipo_SUAC;
    v_id_SUAC number;
  BEGIN

    begin
      select id_tramite into v_id_suac
      from NUEVOSUAC.VT_TRAMITES_IPJ_DESA vt
      where
        VT.NRO_TRAMITE = p_expediente;
    exception
      when NO_DATA_FOUND then
        select id_tramite into v_id_suac
        from ipj.t_tramitesipj
        where
          expediente = p_expediente;
    end;

    -- Busco los datos del tramite en suac
    NUEVOSUAC.PCK_INTERFAZ_VERTICALES_V2.P_GET_TRAMITE_BY_ID(v_id_suac, v_Cursor_Tramite, v_error);
    if nvl(v_error, IPJ.TYPES.C_RESP_OK) = IPJ.TYPES.C_RESP_OK then
      loop
        fetch v_Cursor_Tramite into v_SUAC;
        EXIT WHEN v_Cursor_Tramite%NOTFOUND or v_Cursor_Tramite%NOTFOUND is null;

      end loop;
      close v_Cursor_Tramite;
    end if;

    return v_SUAC.v_nro_sticker_completo;
  exception
    when NO_DATA_FOUND then
      return '';
  END FC_Buscar_Stiker_SUAC;

  FUNCTION FC_Buscar_Asunto_SUAC (p_id_tramite in number) return varchar2
  IS
    -- Dado el expediente, devuelve el Asunto de SUAC
    v_Cursor_Tramite  types.cursorType;
    v_error varchar2(2000);
    v_SUAC Tipo_SUAC;
  BEGIN

    -- Busco los datos del tramite en suac
    NUEVOSUAC.PCK_INTERFAZ_VERTICALES_V2.P_GET_TRAMITE_BY_ID(p_id_tramite, v_Cursor_Tramite, v_error);
    if nvl(v_error, IPJ.TYPES.C_RESP_OK) = IPJ.TYPES.C_RESP_OK then
      loop
        fetch v_Cursor_Tramite into v_SUAC;
        EXIT WHEN v_Cursor_Tramite%NOTFOUND or v_Cursor_Tramite%NOTFOUND is null;

      end loop;
      close v_Cursor_Tramite;
    end if;

    return v_SUAC.v_asunto;
  exception
    when NO_DATA_FOUND then
      return '';
  END FC_Buscar_Asunto_SUAC;

  FUNCTION FC_Buscar_Fojas_SUAC (p_id_tramite in number) return varchar2
  IS
    -- Dado el expediente, devuelve las Fojas de SUAC
    v_Cursor_Tramite  types.cursorType;
    v_error varchar2(2000);
    v_SUAC Tipo_SUAC;
  BEGIN

    -- Busco los datos del tramite en suac
    NUEVOSUAC.PCK_INTERFAZ_VERTICALES_V2.P_GET_TRAMITE_BY_ID(p_id_tramite, v_Cursor_Tramite, v_error);
    if nvl(v_error, IPJ.TYPES.C_RESP_OK) = IPJ.TYPES.C_RESP_OK then
      loop
        fetch v_Cursor_Tramite into v_SUAC;
        EXIT WHEN v_Cursor_Tramite%NOTFOUND or v_Cursor_Tramite%NOTFOUND is null;

      end loop;
      close v_Cursor_Tramite;
    end if;

    return v_SUAC.v_fojas;
  exception
    when NO_DATA_FOUND then
      return '';
  END FC_Buscar_Fojas_SUAC;

  FUNCTION FC_Buscar_Cuerpos_SUAC (p_id_tramite in number) return varchar2
  IS
    -- Dado el expediente, devuelve lso Cuerpos de SUAC
    v_Cursor_Tramite  types.cursorType;
    v_error varchar2(2000);
    v_SUAC Tipo_SUAC;
  BEGIN

    -- Busco los datos del tramite en suac
    NUEVOSUAC.PCK_INTERFAZ_VERTICALES_V2.P_GET_TRAMITE_BY_ID(p_id_tramite, v_Cursor_Tramite, v_error);
    if nvl(v_error, IPJ.TYPES.C_RESP_OK) = IPJ.TYPES.C_RESP_OK then
      loop
        fetch v_Cursor_Tramite into v_SUAC;
        EXIT WHEN v_Cursor_Tramite%NOTFOUND or v_Cursor_Tramite%NOTFOUND is null;

      end loop;
      close v_Cursor_Tramite;
    end if;

    return v_SUAC.v_cuerpos;
  exception
    when NO_DATA_FOUND then
      return '';
  END FC_Buscar_Cuerpos_SUAC;

  FUNCTION FC_Buscar_SubTipo_SUAC (p_id_tramite in number) return varchar2
  IS
    -- Dado el expediente, devuelve el Subtipo del expediente SUAC
    v_Cursor_Tramite  types.cursorType;
    v_error varchar2(2000);
    v_SUAC Tipo_SUAC;
  BEGIN

    -- Busco los datos del tramite en suac
    NUEVOSUAC.PCK_INTERFAZ_VERTICALES_V2.P_GET_TRAMITE_BY_ID(p_id_tramite, v_Cursor_Tramite, v_error);
    if nvl(v_error, IPJ.TYPES.C_RESP_OK) = IPJ.TYPES.C_RESP_OK then
      loop
        fetch v_Cursor_Tramite into v_SUAC;
        EXIT WHEN v_Cursor_Tramite%NOTFOUND or v_Cursor_Tramite%NOTFOUND is null;

      end loop;
      close v_Cursor_Tramite;
    end if;

    return v_SUAC.v_subtipo;
  exception
    when NO_DATA_FOUND then
      return '';
  END FC_Buscar_SubTipo_SUAC;

  FUNCTION FC_Buscar_Fec_Ini_SUAC (p_id_tramite in number) return date
  IS
    -- Dado el expediente, devuelve el sticker de 14
    v_Cursor_Tramite  types.cursorType;
    v_error varchar2(2000);
    v_SUAC Tipo_SUAC;
  BEGIN
    -- Busco los datos del tramite en suac
    DBMS_OUTPUT.PUT_LINE('Buscar Fecha Tramite SUAC = ' || to_char(p_id_tramite));
    NUEVOSUAC.PCK_INTERFAZ_VERTICALES_V2.P_GET_TRAMITE_BY_ID(p_id_tramite, v_Cursor_Tramite, v_error);
    DBMS_OUTPUT.PUT_LINE('    Resultado = ' || v_error);
    if nvl(v_error, IPJ.TYPES.C_RESP_OK) = IPJ.TYPES.C_RESP_OK then
      loop
        fetch v_Cursor_Tramite into v_SUAC;
        EXIT WHEN v_Cursor_Tramite%NOTFOUND or v_Cursor_Tramite%NOTFOUND is null;

      end loop;
      close v_Cursor_Tramite;
    end if;
    DBMS_OUTPUT.PUT_LINE('    Fecha = ' || v_SUAC.v_fecha_inicio);

    return to_date(v_SUAC.v_fecha_inicio, 'dd/mm/rrrr');
  exception
    when NO_DATA_FOUND then
      return null;
  END FC_Buscar_Fec_Ini_SUAC;

  FUNCTION FC_Buscar_Ult_Fec_SUAC (p_id_tramite in number) return date
  IS
    -- Dado el expediente, devuelve el sticker de 14
    v_Cursor_Tramite  types.cursorType;
    v_error varchar2(2000);
    v_SUAC Tipo_SUAC;
    v_ultima_fecha date;
  BEGIN
    -- Busco en la vista el ultimo pase
    begin
      select fecha_ultima_recepcion into v_ultima_fecha
      from NUEVOSUAC.VT_TRAMITES_IPJ_DESA
      where
        id_tramite = p_id_tramite;
    exception
      when NO_DATA_FOUND then
        -- Si no esta en la vista de pendientes, lbusco la fecha de cierre en SUAC
        NUEVOSUAC.PCK_INTERFAZ_VERTICALES_V2.P_GET_TRAMITE_BY_ID(p_id_tramite, v_Cursor_Tramite, v_error);
        if nvl(v_error, IPJ.TYPES.C_RESP_OK) = IPJ.TYPES.C_RESP_OK then
          loop
            fetch v_Cursor_Tramite into v_SUAC;
            EXIT WHEN v_Cursor_Tramite%NOTFOUND or v_Cursor_Tramite%NOTFOUND is null;

          end loop;
          close v_Cursor_Tramite;
        end if;
        v_ultima_fecha := v_SUAC.v_fecha_archivo;
    end;

    return to_date(v_ultima_fecha, 'dd/mm/rrrr');
  exception
    when NO_DATA_FOUND then
      return null;
  END FC_Buscar_Ult_Fec_SUAC;

  FUNCTION FC_Buscar_Fec_Arch_SUAC (p_id_tramite in number) return date
  IS
    v_Cursor_Tramite  types.cursorType;
    v_error varchar2(2000);
    v_SUAC Tipo_SUAC;
    v_ultima_fecha date;
  BEGIN
    -- Si no esta en la vista de pendientes, lbusco la fecha de cierre en SUAC
    NUEVOSUAC.PCK_INTERFAZ_VERTICALES_V2.P_GET_TRAMITE_BY_ID(p_id_tramite, v_Cursor_Tramite, v_error);
    if nvl(v_error, IPJ.TYPES.C_RESP_OK) = IPJ.TYPES.C_RESP_OK then
      loop
        fetch v_Cursor_Tramite into v_SUAC;
        EXIT WHEN v_Cursor_Tramite%NOTFOUND or v_Cursor_Tramite%NOTFOUND is null;

      end loop;
      close v_Cursor_Tramite;
    end if;
    v_ultima_fecha := v_SUAC.v_fecha_archivo;

    return to_date(v_ultima_fecha, 'dd/mm/rrrr');
  exception
    when OTHERS then
      return null;
  END FC_Buscar_Fec_Arch_SUAC;

  FUNCTION FC_Buscar_Iniciador_SUAC (p_id_tramite in number) return varchar2
  IS
    -- Dado el expediente, devuelve el sticker de 14
    v_Cursor_Tramite  types.cursorType;
    v_error varchar2(2000);
    v_SUAC Tipo_SUAC;
  BEGIN
    -- Busco los datos del tramite en suac
    DBMS_OUTPUT.PUT_LINE('Buscar Iniciador SUAC = ' || to_char(p_id_tramite));
    NUEVOSUAC.PCK_INTERFAZ_VERTICALES_V2.P_GET_TRAMITE_BY_ID(p_id_tramite, v_Cursor_Tramite, v_error);
    DBMS_OUTPUT.PUT_LINE('    Resultado = ' || v_error);
    if nvl(v_error, IPJ.TYPES.C_RESP_OK) = IPJ.TYPES.C_RESP_OK then
      loop
        fetch v_Cursor_Tramite into v_SUAC;
        EXIT WHEN v_Cursor_Tramite%NOTFOUND or v_Cursor_Tramite%NOTFOUND is null;

      end loop;
      close v_Cursor_Tramite;
    ELSE
      v_SUAC.v_iniciador := NULL;
    end if;
    DBMS_OUTPUT.PUT_LINE('    Iniciador = ' || v_SUAC.v_iniciador);

    return v_SUAC.v_iniciador;
  exception
    when NO_DATA_FOUND then
      return null;
  END FC_Buscar_Iniciador_SUAC;

  PROCEDURE SP_Anexar_Expediente(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj_padre in number,
    p_sticker14_anexo in varchar2,
    p_cuil_usuario in varchar2,
    p_Adjunta_Notas in char
    )
  IS
  /**********************************************************
     Ese procedimiento anexa un expediente (o nota) a otro expediente.
     - Tanto el padre como el anexo, se pueden pasar con Tramite IPJ, Expediente o Sticker
  **********************************************************/
    v_Cursor_Tramite  types.cursorType;
    v_Id_SUAC_Padre number(10);
    v_id_tramite_ipj_anexo number;
    v_existe number;
    v_SUAC_Padre Tipo_SUAC;
    v_SUAC_Anexo Tipo_SUAC;
    v_Unidad_Padre varchar2(20);
    v_Fecha_Pase_Ultimo TIMESTAMP;
    v_Usuario_Pase_Ultimo Varchar2(100);
    v_Unidad_Pase_Ultimo Varchar2 (500);
    v_prox_pase number;

    v_id_ubicacion_padre ipj.t_tramitesipj.id_ubicacion%TYPE;
    v_id_ubicacion_hijo ipj.t_tramitesipj.id_ubicacion%TYPE;
    v_Unidad_Hijo varchar2(20);

    v_codigo_online number;
    v_existen_docs number;
    v_id_legajo_anexo ipj.t_legajos.id_legajo%TYPE;
    v_id_legajo_padre ipj.t_legajos.id_legajo%TYPE;

  BEGIN
    if IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('HAB_LOG_SUAC') = 'S' then
      IPJ.VARIOS.SP_GUARDAR_LOG(
        p_METODO => 'SP_Anexar_Expediente',
        p_NIVEL => 'Anexar SUAC',
        p_ORIGEN => 'DB',
        p_USR_LOG  => 'DB',
        p_USR_WINDOWS => 'Sistema',
        p_IP_PC  => null,
        p_EXCEPCION =>
         ' Id Tramite Padre = ' || to_char(p_id_tramite_ipj_padre)
         || ' / Sitker 14 anexo = ' || p_sticker14_anexo
         || ' / Cuil Usuario = ' || p_cuil_usuario
         || ' / Adjunta Nota = ' || p_Adjunta_Notas
      );
    end if;

    -- Si falta Padre o Anexo, no hago nada
    if nvl(p_id_tramite_ipj_padre, 0) <> 0 and p_sticker14_anexo is not null then

      /***** BUSCO LOS DATOS DEL TRAMITE PADRE *************/
      if nvl(p_id_tramite_ipj_padre, 0) > 0 then
        --Bug 10112:[SG] - [Anexados] - Anexar un expediente a otro expediente
        select to_number(t.id_tramite), t.id_ubicacion, tp.id_legajo
          into v_Id_SUAC_Padre, v_id_ubicacion_padre, v_id_legajo_padre
          from IPJ.t_tramitesipj t LEFT JOIN ipj.t_tramitesipj_persjur tp ON t.id_tramite_ipj = tp.id_tramite_ipj
         WHERE t.id_tramite_ipj = p_id_tramite_ipj_padre;
      end if;

      /***** BUSCO LOS DATOS DEL TRAMITE ANEXO *************/
      NUEVOSUAC.PCK_INTERFAZ_VERTICALES_V2.P_GET_TRAMITE(p_Sticker14_Anexo, v_Cursor_Tramite);
      loop
        fetch v_Cursor_Tramite into v_SUAC_Anexo;

        EXIT WHEN v_Cursor_Tramite%NOTFOUND or v_Cursor_Tramite%NOTFOUND is null;
      end loop;
      close v_Cursor_Tramite;

      --Bug 10112:[SG] - [Anexados] - Anexar un expediente a otro expediente
      BEGIN
        SELECT tp.id_legajo
          INTO v_id_legajo_anexo
          FROM ipj.t_tramitesipj_persjur tp JOIN ipj.t_tramitesipj t ON tp.id_tramite_ipj = t.id_tramite_ipj
         WHERE t.id_tramite = v_SUAC_Anexo.v_ID;
      EXCEPTION
        WHEN no_data_found THEN
          v_id_legajo_anexo := NULL;
      END;

      /***** SI ENCUENTRO PADRE Y ANEXO - PROSIGO *************/
      --Bug 10112:[SG] - [Anexados] - Anexar un expediente a otro expediente
      --Bug 12527:[SG] - [Anexados] - El legajo del padre
      if nvl(v_Id_SUAC_Padre, 0) > 0 and nvl(v_SUAC_Anexo.v_ID, 0) > 0
        AND (v_id_legajo_padre = v_id_legajo_anexo OR v_id_legajo_anexo IS NULL OR v_id_legajo_padre IS NULL) then
        -- Busco el padre
        DBMS_OUTPUT.PUT_LINE('SP_Buscar_Suac - Tramite Padre = ' || to_char(v_Id_SUAC_Padre));
        SP_Buscar_Suac (
          o_rdo => o_rdo,
          o_tipo_mensaje => o_tipo_mensaje,
          o_SUAC => v_SUAC_Padre,
          p_id_tramite => v_Id_SUAC_Padre,
          p_loguear => 'S');

        DBMS_OUTPUT.PUT_LINE('   o_rdo = ' || o_rdo);

        -- Si el usuario no tiene el tr�mite asignado, no se permite anexar
        if upper(FC_Armar_Usuario_Suac(p_cuil_usuario)) <> upper(v_SUAC_Padre.v_usuario_actual) then
          DBMS_OUTPUT.PUT_LINE(' - ERROR Due�o tr�mite: Usr Padre = ' ||
                               UPPER(V_SUAC_PADRE.V_USUARIO_ACTUAL) ||
                               ' / Usr Gestion = ' ||
                               UPPER(FC_ARMAR_USUARIO_SUAC(P_CUIL_USUARIO)));
          o_rdo := 'El usuario logueado no posee asignado el tr�mite.';
          o_tipo_mensaje := IPJ.TYPES.c_Tipo_Mens_ERROR;
          return;
        end if;


        -- Padre e hijo deben estar en la misma area y el mismo usuario
        DBMS_OUTPUT.PUT_LINE('- Control de Area y Usuarios :');
        DBMS_OUTPUT.PUT_LINE('    - Unidad Padre :' || v_suac_padre.v_unidad_actual);
        DBMS_OUTPUT.PUT_LINE('    - Usr Padre :' || v_suac_padre.v_usuario_actual);
        DBMS_OUTPUT.PUT_LINE('    - Unidad Anexo :' || v_suac_anexo.v_unidad_actual);
        DBMS_OUTPUT.PUT_LINE('    - Usr Anexo :' || v_suac_anexo.v_usuario_actual);

        -- Busco el codigo de la unidad SUAC del padre
        select codigo into v_unidad_padre
        from nuevosuac.vt_unidades
        where
          id_unidad =
            ( select id_unidad_actual
              from nuevosuac.vt_tramites_ipj_desa
              where
                nro_tramite = v_suac_padre.v_nro_tramite)
         and fecha_hasta is null
         and nvl(desactivado, 0) = 0
         and nvl(externa, 0) = 0
         and nvl(eliminado, 0) = 0;

        -- Busco el codigo de la unidad SUAC del hijo
        select codigo into v_unidad_hijo
        from nuevosuac.vt_unidades
        where
          id_unidad =
            ( select id_unidad_actual
              from nuevosuac.vt_tramites_ipj_desa
              where
                nro_tramite = v_suac_anexo.v_nro_tramite)
          and fecha_hasta is null
          and nvl(desactivado, 0) = 0
          and nvl(externa, 0) = 0
          and nvl(eliminado, 0) = 0;

        -- Padre e hijo deben estar en la misma area y el mismo usuario
        if v_SUAC_Padre.v_unidad_actual <> v_SUAC_Anexo.v_unidad_actual or
          v_SUAC_Padre.v_usuario_actual <> v_SUAC_Anexo.v_usuario_actual then

          -- Si no es del usuario, o esta en otra area trata de moverlo al usuario y al area
          if v_SUAC_Anexo.v_estado = 'A RECIBIR' then
            -- Consultos los pases que posee el tramite
            IPJ.TRAMITES_SUAC.SP_Buscar_Pases(
              p_nro_sticker_completo => v_SUAC_Anexo.v_nro_sticker_completo,
              o_fecha_pase => v_Fecha_Pase_Ultimo,
              o_usuario_pase => v_Usuario_Pase_Ultimo,
              o_unidad_pase => v_Unidad_Pase_Ultimo,
              o_Prox_pase => v_prox_pase,
              o_rdo => o_rdo,
              o_tipo_mensaje => o_tipo_mensaje);

            if o_rdo <> IPJ.TYPES.C_RESP_OK then
              o_rdo := 'Buscar Pase SUAC: '  || o_rdo;
              return;
            end if;

            -- Si el ultimo pase, es al area del padre, lo acepto
            if v_Unidad_Pase_Ultimo = v_SUAC_Padre.v_unidad_actual then
              -- Acepto el ultimo pase
              IPJ.TRAMITES_SUAC.SP_Aceptar_Pase(
                p_nro_sticker_completo => v_SUAC_Anexo.v_nro_sticker_completo,
                p_cod_unidad_receptora => v_Unidad_Padre,
                p_usuario_receptor => v_SUAC_Padre.v_usuario_actual,
                p_cuerpos => v_SUAC_Anexo.v_cuerpos,
                p_folios => v_SUAC_Anexo.v_fojas,
                o_rdo => o_rdo,
                o_tipo_mensaje => o_tipo_mensaje);

              if o_rdo <> IPJ.TYPES.C_RESP_OK then
                o_rdo := 'Aceptar Pase SUAC: '  || o_rdo;
                return;
              end if;
            else
              o_rdo := 'El anexo posee un pase pendiente a otra �rea. No se puede anexar.';
              o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
              return;
            end if;
          else
            -- Si esta A ENVIAR, pero en otra �rea, realizo un pase completo
            DBMS_OUTPUT.PUT_LINE('SP_Realizar_Tranf_IPJ');
            DBMS_OUTPUT.PUT_LINE('   - p_id_tramite_suac = ' || to_char( v_SUAC_Anexo.v_ID));
            DBMS_OUTPUT.PUT_LINE('   - Ubicacion origen = ' || v_SUAC_Anexo.v_unidad_actual);
            DBMS_OUTPUT.PUT_LINE('   - p_id_ubicacion_origen = ' || FC_Buscar_Cod_Unidad_SUAC (v_SUAC_Anexo.v_unidad_actual));--10080:[SG] - [Anexados] - v_SUAC_Anexo.v_unidad_actual
            DBMS_OUTPUT.PUT_LINE('   - Ubicacion_destino = ' || v_SUAC_Padre.v_usuario_actual);
            DBMS_OUTPUT.PUT_LINE('   - p_id_ubicacion_destino = ' || FC_Buscar_Cod_Unidad_SUAC (v_SUAC_Padre.v_unidad_actual));--10080:[SG] - [Anexados] - v_SUAC_Padre.v_usuario_actual
            DBMS_OUTPUT.PUT_LINE('   - p_cuil_usuario_origen = ' || v_SUAC_Anexo.v_usuario_actual);
            DBMS_OUTPUT.PUT_LINE('   - p_cuil_usuario_destino = ' || v_SUAC_Padre.v_usuario_actual);

            --Busco la ubicaci�n del hijo
            BEGIN
              SELECT t.id_ubicacion
                INTO v_id_ubicacion_hijo
                FROM ipj.t_tramitesipj t
               WHERE t.id_tramite = v_SUAC_Anexo.v_ID;
            EXCEPTION
              WHEN no_data_found THEN
                SELECT u.id_ubicacion
                  INTO v_id_ubicacion_hijo
                  FROM ipj.t_ubicaciones u
                 WHERE u.codigo_suac = v_unidad_hijo;
            END;

            IPJ.TRAMITES_SUAC.SP_Realizar_Tranf_IPJ(
              o_rdo => o_rdo,
              o_tipo_mensaje => o_tipo_mensaje,
              p_id_tramite_suac => v_SUAC_Anexo.v_ID,
              p_id_ubicacion_origen => v_id_ubicacion_hijo,--10080:[SG] - [Anexados] - v_SUAC_Padre.v_usuario_actual
              p_id_ubicacion_destino => v_id_ubicacion_padre,--10080:[SG] - [Anexados] - v_SUAC_Padre.v_usuario_actual
              p_cuil_usuario_origen => v_SUAC_Anexo.v_usuario_actual,
              p_cuil_usuario_destino => v_SUAC_Padre.v_usuario_actual
            );
            DBMS_OUTPUT.PUT_LINE('   - o_rdo = ' || o_rdo);

            if o_rdo <> IPJ.TYPES.C_RESP_OK then
              o_rdo := 'Error al realizar el pase del Anexo al �rea del padre: '  || o_rdo;
              rollback;
              return;
            end if;
          end if;


        end if;

        /********* REALIZA EL ANEXADO ***************************/
        DBMS_OUTPUT.PUT_LINE('Anexar Tramite SUAC (PCK_INTERFAZ_VERTICALES_V2.p_agregar_tramites)');
        DBMS_OUTPUT.PUT_LINE('  - i_sticker_padre = ' || v_SUAC_Padre.v_nro_sticker_completo);
        DBMS_OUTPUT.PUT_LINE('  - i_sticker_hijo = ' || v_SUAC_Padre.v_nro_sticker_completo);
        DBMS_OUTPUT.PUT_LINE('  - i_usuario = ' || v_SUAC_Padre.v_usuario_actual);
        DBMS_OUTPUT.PUT_LINE('  - Unidad = ' || v_SUAC_Padre.v_unidad_actual);
        DBMS_OUTPUT.PUT_LINE('  - i_unidad (funcion) = ' || FC_Buscar_Cod_Unidad_SUAC(v_SUAC_Padre.v_unidad_actual));
        DBMS_OUTPUT.PUT_LINE('  - i_unidad (vista) = ' || v_Unidad_Padre);

        NUEVOSUAC.PCK_INTERFAZ_VERTICALES_V2.p_agregar_tramites(
          i_sticker_padre => v_SUAC_Padre.v_nro_sticker_completo,
          i_sticker_hijo => v_SUAC_Anexo.v_nro_sticker_completo,
          i_usuario => v_SUAC_Padre.v_usuario_actual,
          i_unidad => v_Unidad_Padre,
          i_tipo => 'A', --A->Anexado; E->Encordado
          i_sentido => 'P', --P->Con sentido positivo (anexar o encordar); N->Con sentido negativo (desanexar o desencordar)
          i_cuerpos => v_SUAC_Padre.v_cuerpos,
          i_folios => v_SUAC_Padre.v_fojas + 1,
          o_error => o_rdo);

        DBMS_OUTPUT.PUT_LINE('  - o_error = ' || o_rdo);

        -- Si se pudo anexar, realizo el resto de las operaciones de Gestion
        if nvl(o_rdo, IPJ.TYPES.C_RESP_OK) like IPJ.TYPES.C_RESP_OK || '%' then
          /*******Si el tr�mite Anexo existe en Gesti�n, lo marco como ANEXADO *****/
          begin
            select id_tramite_ipj into v_id_tramite_ipj_anexo
            from ipj.t_tramitesipj
            where
              id_tramite = v_SUAC_Anexo.v_id;
          exception
            when NO_DATA_FOUND then
              v_id_tramite_ipj_anexo := null;
          end;

          if v_id_tramite_ipj_anexo is not null then
            -- Marco el tr�mite con estado ANEXADO y le asocio el padre
            update ipj.t_tramitesipj
            set
              id_estado_ult = IPJ.Types.c_Estados_Anexado, -- Anexado
              id_tramite_ipj_padre = p_id_tramite_ipj_padre,
              id_tramite = null
            where
              id_tramite_ipj = v_id_tramite_ipj_anexo;

            -- Pongo las acciones en esta ANEXADO
            update ipj.t_tramitesipj_acciones
            set id_estado = IPJ.Types.c_Estados_Anexado -- Anexado
            where
              id_tramite_ipj = v_id_tramite_ipj_anexo;

            -- Elimino los registros de Archivo del tr�mite anexado
            delete IPJ.T_ARCHIVO_DESARCHIVOS where id_archivo_tramite in (select id_archivo_tramite from ipj.t_archivo_tramite where id_tramite_ipj = v_id_tramite_ipj_anexo);
            delete IPJ.T_ARCHIVO_MOVIMIENTOS where id_archivo_tramite in (select id_archivo_tramite from ipj.t_archivo_tramite where id_tramite_ipj = v_id_tramite_ipj_anexo);
            delete IPJ.T_ARCHIVO_PRESTAMOS where id_archivo_tramite in (select id_archivo_tramite from ipj.t_archivo_tramite where id_tramite_ipj = v_id_tramite_ipj_anexo);
            delete IPJ.T_ARCHIVO_TRAMITE_HIST where id_archivo_tramite in (select id_archivo_tramite from ipj.t_archivo_tramite where id_tramite_ipj = v_id_tramite_ipj_anexo);
            delete IPJ.T_ARCHIVO_TRAMITE where id_tramite_ipj = v_id_tramite_ipj_anexo;

            --Bug 10112:[SG] - [Anexados] - Si no tiene los docs los bajo del tr�mite online
            -- Busco el codigo online asociado al tr�mite
            BEGIN
              SELECT nvl(codigo_online, 0)
                INTO v_codigo_online
                FROM ipj.t_tramitesipj
               WHERE id_tramite_ipj = v_id_tramite_ipj_anexo;

              -- Busco el legajo
              SELECT tp.id_legajo
                INTO v_id_legajo_anexo
                FROM ipj.t_tramitesipj t
                JOIN ipj.t_tramitesipj_persjur tp ON tp.id_tramite_ipj = t.id_tramite_ipj
               WHERE t.codigo_online = v_codigo_online;

              -- Veo que no tenga documentos asociados
              SELECT COUNT(1)
                INTO v_existen_docs
                FROM ipj.t_entidades_documentos
               WHERE id_tramite_ipj = v_id_tramite_ipj_anexo
                 AND id_legajo = v_id_legajo_anexo;

            EXCEPTION
              WHEN no_data_found THEN
                v_codigo_online := 0;
            END;

            -- Si es de un tr�mite online, y no tiene documentos, inserto los del portal
            if v_codigo_online > 0 and v_existen_docs = 0 then
              INSERT INTO IPJ.T_ENTIDADES_DOCUMENTOS (Id_Tramite_Ipj, Id_Legajo, id_documento,
                n_documento, observacion, cuil_usr, fecha_alta, id_tipo_documento_cdd,
                fecha_notarial, cuil_escrib_orig, nro_escribania_orig, id_cargo_escrib_orig,
                n_cargo_escrib_orig, detalle_escribania_orig )
              select
                p_id_tramite_ipj_padre, v_id_legajo_anexo, id_documento, n_documento, observacion,
                cuil_usr, fecha_alta, id_tipo_documento_cdd, fecha_notarial, cuil_escrib_orig,
                nro_escribania_orig, id_cargo_escrib_orig, n_cargo_escrib_orig, detalle_escribania_orig
              from IPJ.t_ol_entidad_documentos
              where
                codigo_online = v_codigo_online and
                nvl(id_documento, 0) > 0; -- (PBI 9444 - Contingencia Documentos
            end if;


          end if;

          -- Si desea adjunta las notas, las cargo
          if nvl(p_Adjunta_Notas, 'N') = 'S' then
            IPJ.TRAMITES_SUAC.SP_Anexar_Notas_SUAC(
              o_rdo => o_rdo,
              o_tipo_mensaje => o_tipo_mensaje,
              p_id_tramite_suac_anexo => v_SUAC_Anexo.v_ID,
              p_id_tramite_ipj_padre => p_id_tramite_ipj_padre);
          end if;
        end if;
      else
        --Bug 10112:[SG] - [Anexados] - Si no tiene los docs los bajo del tr�mite online
        IF nvl(v_Id_SUAC_Padre, 0) = 0 OR nvl(v_SUAC_Anexo.v_ID, 0) = 0 THEN
          o_rdo := 'No se encontro el expediente indicado.';
        ELSE
          o_rdo := 'El expediente padre y el anexo pertenecen a distintas empresas.';
        END IF;
      end if;
    else
      o_rdo := 'Se deben pasar los expedientes padre e hijo para anexar.';
    end if;

    -- Dependiendo de la respuesta, marco OK o ERROR
    if nvl(o_rdo, IPJ.TYPES.C_RESP_OK) like IPJ.TYPES.C_RESP_OK || '%' then
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
      o_rdo := IPJ.TYPES.C_RESP_OK;
      commit;
    else
      o_rdo := 'No se pudo Anexar en SUAC (' || o_rdo || ').';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      rollback;
    end if;

    if IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('HAB_LOG_SUAC') = 'S' then
      IPJ.VARIOS.SP_GUARDAR_LOG(
        p_METODO => 'SP_Anexar_Expediente',
        p_NIVEL => 'Anexar SUAC',
        p_ORIGEN => 'DB',
        p_USR_LOG  => 'DB',
        p_USR_WINDOWS => 'Sistema',
        p_IP_PC  => null,
        p_EXCEPCION =>
         ' Id Tramite Padre = ' || to_char(p_id_tramite_ipj_padre)
         || ' / Sitker 14 anexo = ' || p_sticker14_anexo
         || ' / Cuil Usuario = ' || p_cuil_usuario
         || ' / Adjunta Nota = ' || p_Adjunta_Notas
         || ' / i_sticker_padre = ' || v_SUAC_Padre.v_nro_sticker_completo
         ||'  / i_sticker_hijo = ' || v_SUAC_Anexo.v_nro_sticker_completo
         ||'  / i_usuario padre = ' || v_SUAC_Padre.v_usuario_actual
         ||'  / i_usuario hijo = ' || v_SUAC_Anexo.v_usuario_actual
         ||'  / Unidad padre= ' || v_SUAC_Padre.v_unidad_actual
         ||'  / Unidad hijo= ' || v_SUAC_Anexo.v_unidad_actual
         ||'  / i_unidad (funcion padre) = ' || FC_Buscar_Cod_Unidad_SUAC(v_SUAC_Padre.v_unidad_actual)
         ||'  / i_unidad (funcion hijo) = ' || FC_Buscar_Cod_Unidad_SUAC(v_SUAC_Anexo.v_unidad_actual)
         ||'  / i_unidad (vista padre) = ' || v_Unidad_Padre
         ||'  / i_unidad (vista hijo) = ' || v_Unidad_Hijo
         || ' / Mensaje = ' || o_rdo
      );
    end if;

  exception
    when OTHERS then
      o_rdo :=  'SP_Anexar_Expediente : ' || To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      rollback;
      IPJ.VARIOS.SP_GUARDAR_LOG(
        p_METODO => 'SP_Anexar_Expediente',
        p_NIVEL => 'Anexar SUAC',
        p_ORIGEN => 'DB',
        p_USR_LOG  => 'DB',
        p_USR_WINDOWS => 'Sistema',
        p_IP_PC  => null,
        p_EXCEPCION =>
         ' Id Tramite Padre = ' || to_char(p_id_tramite_ipj_padre)
         || ' / Sitker 14 anexo = ' || p_sticker14_anexo
         || ' / Cuil Usuario = ' || p_cuil_usuario
         || ' / Adjunta Nota = ' || p_Adjunta_Notas
         || ' / i_sticker_padre = ' || v_SUAC_Padre.v_nro_sticker_completo
         ||'  / i_sticker_hijo = ' || v_SUAC_Anexo.v_nro_sticker_completo
         ||'  / i_usuario padre = ' || v_SUAC_Padre.v_usuario_actual
         ||'  / i_usuario hijo = ' || v_SUAC_Anexo.v_usuario_actual
         ||'  / Unidad padre= ' || v_SUAC_Padre.v_unidad_actual
         ||'  / Unidad hijo= ' || v_SUAC_Anexo.v_unidad_actual
         ||'  / i_unidad (funcion padre) = ' || FC_Buscar_Cod_Unidad_SUAC(v_SUAC_Padre.v_unidad_actual)
         ||'  / i_unidad (funcion hijo) = ' || FC_Buscar_Cod_Unidad_SUAC(v_SUAC_Anexo.v_unidad_actual)
         ||'  / i_unidad (vista padre) = ' || v_Unidad_Padre
         ||'  / i_unidad (vista hijo) = ' || v_Unidad_Hijo
         || ' / Mensaje = ' || o_rdo
      );
  END SP_Anexar_Expediente;

  PROCEDURE SP_Reabrir_Tramite(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_sticker_14 in varchar2,
    p_usuario in varchar2,
    p_unidad in varchar2,
    p_unidad_destino in varchar2
  )
  IS
  BEGIN

    -- Desarchivo el tr�mite indicado
    DBMS_OUTPUT.PUT_LINE('    - Desarchivar: ' );
    DBMS_OUTPUT.PUT_LINE('         - Sticker 14: ' || p_sticker_14 );
    DBMS_OUTPUT.PUT_LINE('         - Usuario: ' || p_usuario );
    DBMS_OUTPUT.PUT_LINE('         - Unidad: ' || p_unidad );
    DBMS_OUTPUT.PUT_LINE('         - Destino: ' || p_unidad_destino );
    NUEVOSUAC.PCK_INTERFAZ_VERTICALES_V2. p_desarchivar(
      i_sticker => p_sticker_14,
      i_usuario => p_usuario,
      i_unidad => p_unidad,
      i_unidad_destino => p_unidad_destino,
      o_error => o_rdo
    );

    DBMS_OUTPUT.PUT_LINE('         - Error: ' || o_rdo );
    if nvl(o_rdo, IPJ.TYPES.C_RESP_OK)  like IPJ.TYPES.C_RESP_OK || '%' then
      o_rdo := IPJ.TYPES.C_RESP_OK;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    else
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
    end if;

  exception
    when NO_DATA_FOUND then
      o_rdo :=  To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Reabrir_Tramite;


  PROCEDURE SP_Realizar_Tranf_Mesa_Suac(
     o_rdo out varchar2,
     o_tipo_mensaje out number,
     p_id_tramite_suac in number,
     p_cuil_usuario in varchar2
  )
  IS
   /*********************************************************
     Realiza el pase a MESA SUAC en SUAC.
     Se pasa a la Mesa Suac que corresponda al �rea actual del tr�mite y el usuario
   *********************************************************/
     v_area_suac_origen varchar2(50);
     v_area_suac_destino varchar2(50);
     v_Cursor_Tramite  types.cursorType;
     v_error varchar2(2000);
     v_error_pase varchar2(2000);
     v_Fecha_Pase_Ultimo TIMESTAMP;
     v_Usuario_Pase_Ultimo Varchar2(100);
     v_Unidad_Pasev_Id_Ultimo Varchar2 (500);
     v_Unidad_Pase_Ultimo varchar2(500);
     v_prox_pase number;
     v_Unidad_Destino varchar2(50);
     v_usuario_suac varchar2(50);
     v_id_tramite_ipj number;
     v_cant_notas number;
     v_sticker varchar2(20);

     -- tramite suac
     v_SUAC Tipo_SUAC;
   BEGIN
     -- Busco los datos del tr�mite SUAC
     DBMS_OUTPUT.PUT_LINE('Buscar SUAC: ' || to_char(p_id_tramite_suac));
     NUEVOSUAC.PCK_INTERFAZ_VERTICALES_V2.P_GET_TRAMITE_BY_ID(p_id_tramite_suac, v_Cursor_Tramite, v_error);
     DBMS_OUTPUT.PUT_LINE('       - v_error: ' || v_error);
     if nvl(v_error, IPJ.TYPES.C_RESP_OK) <> IPJ.TYPES.C_RESP_OK then
       o_rdo :=  'SP_Realizar_Tranf_Mesa_Suac - Buscar Tramite: ' || v_error ;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
       return;
     end if;

     loop
       fetch v_Cursor_Tramite into v_SUAC;

       EXIT WHEN v_Cursor_Tramite%NOTFOUND or v_Cursor_Tramite%NOTFOUND is null;
     end loop;
     close v_Cursor_Tramite;

     -- Si esta ARCHIVADO no se hace nada
     DBMS_OUTPUT.PUT_LINE('    Estado Tr�mite: ' || v_SUAC.v_estado);
     if v_SUAC.v_estado in ('ARCHIVADO') then
       o_rdo :=  'El tr�mite SUAC esta ARCHIVADO, no se puede cargar un nuevo pase.' ;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
       return;
     end if;

     -- Si esta A RECIBIR y puedo tomar el pase, continuo.
     if v_SUAC.v_estado = 'A RECIBIR' then
       -- Consultos los pases que posee el tramite
       IPJ.TRAMITES_SUAC.SP_Buscar_Pases(
         p_nro_sticker_completo => v_SUAC.v_nro_sticker_completo,
         o_fecha_pase => v_Fecha_Pase_Ultimo,
         o_usuario_pase => v_Usuario_Pase_Ultimo,
         o_unidad_pase => v_Unidad_Pase_Ultimo,
         o_Prox_pase => v_prox_pase,
         o_rdo => o_rdo,
         o_tipo_mensaje => o_tipo_mensaje);

       if o_rdo <> IPJ.TYPES.C_RESP_OK then
         o_rdo := 'Buscar Pase SUAC: '  || o_rdo;
         return;
       end if;

       -- Verifico si el usuario tiene permisos en el area del pase encontrado
       v_usuario_suac := FC_Armar_Usuario_Suac(p_cuil_usuario);
       begin
         select codigo into v_Unidad_Destino
         from NUEVOSUAC.VT_USUARIOS_UNI_JUS
         where
           upper(id_usuario) =  v_usuario_suac and
           upper(n_unidad) = upper(v_Unidad_Pase_Ultimo);
       exception
         when NO_DATA_FOUND then
           v_Unidad_Destino := null;
       end;

       -- Si el ultimo pase, es al area del padre, lo acepto
       if v_Unidad_Destino is not null then
         -- Acepto el ultimo pase
         IPJ.TRAMITES_SUAC.SP_Aceptar_Pase(
           p_nro_sticker_completo => v_SUAC.v_nro_sticker_completo,
           p_cod_unidad_receptora => v_Unidad_Destino,
           p_usuario_receptor => FC_Armar_Usuario_Suac(p_cuil_usuario),
           p_cuerpos => v_SUAC.v_cuerpos,
           p_folios => v_SUAC.v_fojas,
           o_rdo => o_rdo,
           o_tipo_mensaje => o_tipo_mensaje);

         if o_rdo <> IPJ.TYPES.C_RESP_OK then
           o_rdo := 'Aceptar Pase SUAC: '  || o_rdo;
           return;
         end if;

         -- Recargo los datos del tr�mite para continuar
         NUEVOSUAC.PCK_INTERFAZ_VERTICALES_V2.P_GET_TRAMITE_BY_ID(p_id_tramite_suac, v_Cursor_Tramite, v_error);
         loop
           fetch v_Cursor_Tramite into v_SUAC;

           EXIT WHEN v_Cursor_Tramite%NOTFOUND or v_Cursor_Tramite%NOTFOUND is null;
         end loop;
         close v_Cursor_Tramite;
       else
         o_rdo := 'El expediente posee un pase pendiente a otra �rea. No tiene permisos. Usuario: ' || p_cuil_usuario;
         o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
         return;
       end if;
     end if;

     -- Subo las observaciones antes de devolver a Mesa Suac
     select id_tramite_ipj into v_id_tramite_ipj
     from ipj.t_tramitesipj
     where
       id_tramite = p_id_tramite_suac;

     IPJ.TRAMITES_SUAC.SP_Subir_Notas_SUAC(
       o_rdo => o_rdo,
       o_tipo_mensaje => o_tipo_mensaje,
       o_cant_sincroniz => v_cant_notas,
       o_sticker => v_sticker,
       p_id_tramite_ipj => v_id_tramite_ipj
     );

     if o_rdo <> IPJ.TYPES.C_RESP_OK then
       o_rdo :=  'Pasar a SUAC - Subir Notas: ' || o_rdo;
       return;
     end if;

     -- Busco las areas de suac
     begin
       select codigo, codigo_mesa into v_area_suac_origen, v_area_suac_destino
       from NUEVOSUAC.VT_USUARIOS_UNI_JUS
       where
         id_usuario = v_SUAC.v_usuario_actual and
         n_unidad = v_SUAC.v_unidad_actual;

     exception
       when NO_DATA_FOUND then
         v_area_suac_origen := null;
         v_area_suac_destino := null;
     end;

     -- Si ambas areas estan configurada en SUAC, procedo
    DBMS_OUTPUT.PUT_LINE('SUAC - Realizar Transferencia IPJ');
    DBMS_OUTPUT.PUT_LINE('       - Area Origen: ' || v_area_suac_origen);
    DBMS_OUTPUT.PUT_LINE('       - Area Destino: ' || v_area_suac_destino);
     if v_area_suac_origen is not null and v_area_suac_destino is not null then

        -- Si el tramite esta A ENVIAR, inserto el pase y lo tomo
       if v_SUAC.v_estado = 'A ENVIAR' then
         if v_area_suac_origen <> v_area_suac_destino then
           DBMS_OUTPUT.PUT_LINE('    - SP Inserta Pase: ' );
           IPJ.TRAMITES_SUAC.SP_Insertar_Pase(
             p_nro_sticker_completo => v_SUAC.v_nro_sticker_completo,
             p_unidad_origen => v_area_suac_origen,
             p_unidad_destino => v_area_suac_destino,
             p_fecha_entrada => sysdate,
             p_cuerpos => v_SUAC.v_cuerpos,
             p_folios => v_SUAC.v_fojas,
             p_usuario => v_SUAC.v_usuario_actual,
             o_rdo => v_error_pase,
             o_tipo_mensaje => o_tipo_mensaje);

           DBMS_OUTPUT.PUT_LINE('       Error: '  || v_error_pase);
           -- Si fallo la inserci�n del pase, no continuo
           if v_error_pase not like IPJ.TYPES.C_RESP_OK || '%' then
             o_rdo :=  v_error_pase ;
             o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
            return;
           end if;
         else
           o_rdo :=  'El expediente se encuentra en Mesa SUAC, no se puede realizar un pase.';
           o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
           return;
         end if;
       end if;

     else
       o_rdo :=  IPJ.TYPES.C_RESP_OK ;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
     end if;

     DBMS_OUTPUT.PUT_LINE('    - O_Rdo ' || o_rdo );
   END SP_Realizar_Tranf_Mesa_Suac;

   PROCEDURE SP_Recibir_Tramite_Mesa_Suac(
     o_rdo out varchar2,
     o_tipo_mensaje out number,
     p_id_tramite_suac in number,
     p_cuil_usuario in varchar2
     )
  IS
   /*********************************************************
     Realiza el pase desde MESA SUAC en SUAC.
     Se toma de la mesa SUAC de origen, y se pasa al �rea que correnda al usuario que estudia
       - Si el area de estudio esta aprobada y corresponde a la mesa, se usa
       - Si el area de estudio no existe en la mesa, se cambia de mesa y luego se pasa al area
   *********************************************************/
    v_area_suac_origen varchar2(50);
    v_mesa_suac_origen varchar2(50);
    v_area_suac_destino varchar2(50);
    v_mesa_suac_destino varchar2(50);
    v_Cursor_Tramite  types.cursorType;
    v_error varchar2(2000);
    v_error_pase varchar2(2000);
    v_Fecha_Pase_Ultimo TIMESTAMP;
    v_Usuario_Pase_Ultimo Varchar2(100);
    v_Unidad_Pasev_Id_Ultimo Varchar2 (500);
    v_Unidad_Pase_Ultimo varchar2(500);
    v_prox_pase number;
    v_Unidad_Destino varchar2(50);
    v_usuario_suac varchar2(50);
    v_id_tramite_ipj number;
    v_cant_notas number;
    v_sticker varchar2(20);
    v_ubicacion_tramite number;
    v_resp_mesa_dest varchar2(20);

    -- tramite suac
    v_SUAC Tipo_SUAC;
  BEGIN
     -- Busco los datos del tr�mite SUAC
    DBMS_OUTPUT.PUT_LINE('Inicia SP_Recibir_Tramite_Mesa_Suac  tr�mite: ' || to_char(p_id_tramite_suac));
    NUEVOSUAC.PCK_INTERFAZ_VERTICALES_V2.P_GET_TRAMITE_BY_ID(p_id_tramite_suac, v_Cursor_Tramite, v_error);
    DBMS_OUTPUT.PUT_LINE('    - Resultado: ' || v_error);
    if nvl(v_error, IPJ.TYPES.C_RESP_OK) <> IPJ.TYPES.C_RESP_OK then
      o_rdo :=  'SP_Recibir_Tramite_Mesa_Suac - Buscar Tramite: ' || v_error ;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      return;
    end if;

    loop
      fetch v_Cursor_Tramite into v_SUAC;

      EXIT WHEN v_Cursor_Tramite%NOTFOUND or v_Cursor_Tramite%NOTFOUND is null;
    end loop;
    close v_Cursor_Tramite;

    -- Busco la ubicacion actual del tr�mite
    select id_ubicacion into v_ubicacion_tramite
    from ipj.t_tramitesipj
    where
      id_tramite = p_id_tramite_suac;

    -- Si esta ARCHIVADO no se hace nada
    DBMS_OUTPUT.PUT_LINE('    Estado Tr�mite: ' || v_SUAC.v_estado);
    if v_SUAC.v_estado in ('ARCHIVADO') then
      o_rdo :=  'El tr�mite SUAC esta ARCHIVADO, no se puede cargar un nuevo pase.' ;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      return;
    end if;

    -- Busco los datos de la mesa de origen
    begin
      select codigo, codigo_mesa into v_area_suac_origen, v_mesa_suac_origen
        from NUEVOSUAC.VT_USUARIOS_UNI_JUS
       where
         id_usuario = v_SUAC.v_usuario_actual and
         n_unidad = v_SUAC.v_unidad_actual;
    exception
      when NO_DATA_FOUND then
         v_area_suac_origen := null;
         v_mesa_suac_origen := null;
    end;

    -- Busco los datos del destino, segun el usuario
    v_usuario_suac := FC_Armar_Usuario_Suac(p_cuil_usuario);
    begin
      -- Busco si el usuario tiene permisos en un �rea de la misma mesa
      select codigo, codigo_mesa into v_area_suac_destino, v_mesa_suac_destino
      from NUEVOSUAC.VT_USUARIOS_UNI_JUS
      where
        upper(id_usuario) =  upper(v_usuario_suac) and
        upper(mesa) = upper(v_suac.v_unidad_actual) and
        codigo in
          (select codigo_suac
           from ipj.t_relacion_ubic_suac r
           where id_ubicacion = v_ubicacion_tramite);
    exception
      when NO_DATA_FOUND then
         v_area_suac_destino := '-1';
         v_mesa_suac_destino := '-1';
    end;

    -- Si el usuario no tiene permiso en la misma mesa, busco el permiso dentro del area
    if  v_area_suac_destino = '-1' then
      begin
        select codigo, codigo_mesa into v_area_suac_destino, v_mesa_suac_destino
        from NUEVOSUAC.VT_USUARIOS_UNI_JUS
        where
          upper(id_usuario) =  upper(v_usuario_suac) and
          codigo in
            (select codigo_suac
             from ipj.t_relacion_ubic_suac r
             where id_ubicacion = v_ubicacion_tramite);
      exception
        when NO_DATA_FOUND then
           v_area_suac_destino := null;
           v_mesa_suac_destino := null;
      end;
    end if;

    -- Si ambas areas estan configurada en SUAC, procedo
    DBMS_OUTPUT.PUT_LINE('   1- SUAC - Realizar Transferencia IPJ');
    DBMS_OUTPUT.PUT_LINE('       - Area Origen: ' || v_area_suac_origen);
    DBMS_OUTPUT.PUT_LINE('       - Mesa Origen: ' || v_mesa_suac_origen);
    DBMS_OUTPUT.PUT_LINE('       - Area Destino: ' || v_area_suac_destino);
    DBMS_OUTPUT.PUT_LINE('       - Mesa Destino: ' || v_mesa_suac_destino);

    -- Si tengo las areas de origen y destino, prosigo
    if v_area_suac_origen is not null and v_area_suac_destino is not null then

        -- Si el tramite esta A ENVIAR, inserto el pase y lo tomo
      if v_SUAC.v_estado = 'A ENVIAR' then

        -- Si no estan en la misma mesa, realizo el pase entre las mesas primero
        if v_mesa_suac_destino <> v_area_suac_origen then
          -- Inserto un pase a la otra mesa
          DBMS_OUTPUT.PUT_LINE('   Distintas Mesas, se llama a SP_Cambiar_Mesa_SUAC');
          SP_Cambiar_Mesa_SUAC(
            o_rdo => o_rdo,
            o_tipo_mensaje => o_tipo_mensaje,
            p_id_tramite_suac => p_id_tramite_suac,
            p_mesa_suac_destino => v_mesa_suac_destino
          );
          DBMS_OUTPUT.PUT_LINE('     - Resultado = ' || o_rdo);

          if nvl(o_rdo, IPJ.TYPES.C_RESP_OK) <> IPJ.TYPES.C_RESP_OK then
            o_rdo := 'Transferencia entre Mesas: '  || o_rdo;
            return;
          end if;

          -- Busco el responsable de la nueva mesa
          select id_resp_mesa into v_resp_mesa_dest
          from nuevosuac.vt_get_rep_ipj
          where
            codigo_mesa = v_mesa_suac_destino;

          v_area_suac_origen := v_mesa_suac_destino;
          v_SUAC.v_usuario_actual := v_resp_mesa_dest;
        end if;

        -- Inserto un nuevo pase al �rea, si esta en la mesa correspondiente al usuario
        DBMS_OUTPUT.PUT_LINE('   2-  Inserta Pase');
        DBMS_OUTPUT.PUT_LINE('       - Sticker 14: ' || v_SUAC.v_nro_sticker_completo);
        DBMS_OUTPUT.PUT_LINE('       - Unidad Origen: ' || v_area_suac_origen);
        DBMS_OUTPUT.PUT_LINE('       - Undiad Destino: ' || v_area_suac_destino);
        DBMS_OUTPUT.PUT_LINE('       - Usuario: ' || v_SUAC.v_usuario_actual);
        IPJ.TRAMITES_SUAC.SP_Insertar_Pase(
          p_nro_sticker_completo => v_SUAC.v_nro_sticker_completo,
          p_unidad_origen => v_area_suac_origen,
          p_unidad_destino => v_area_suac_destino,
          p_fecha_entrada => to_date(sysdate, 'dd/mm/rrrr'),
          p_cuerpos => v_SUAC.v_cuerpos,
          p_folios => v_SUAC.v_fojas,
          p_usuario => v_SUAC.v_usuario_actual,
          o_rdo => o_rdo,
          o_tipo_mensaje => o_tipo_mensaje
        );
        DBMS_OUTPUT.PUT_LINE('       - Resultado: ' || o_rdo);

        if o_rdo <> IPJ.TYPES.C_RESP_OK then
          o_rdo := 'Insertar Pase SUAC: '  || o_rdo;
          return;
        end if;

        -- Marco el tr�mite SUAC son estado A RECIBI para que siga con el proximo bloque
        v_SUAC.v_estado := 'A RECIBIR';
      end if;

      -- Si esta A RECIBIR y puedo tomar el pase, continuo.
      if v_SUAC.v_estado = 'A RECIBIR' then
        -- Consultos los pases que posee el tramite
        DBMS_OUTPUT.PUT_LINE('   3-  Buscar Pase Creado');
        DBMS_OUTPUT.PUT_LINE('       - Sticker 14: ' || v_SUAC.v_nro_sticker_completo);

        IPJ.TRAMITES_SUAC.SP_Buscar_Pases(
          p_nro_sticker_completo => v_SUAC.v_nro_sticker_completo,
          o_fecha_pase => v_Fecha_Pase_Ultimo,
          o_usuario_pase => v_Usuario_Pase_Ultimo,
          o_unidad_pase => v_Unidad_Pase_Ultimo,
          o_Prox_pase => v_prox_pase,
          o_rdo => o_rdo,
          o_tipo_mensaje => o_tipo_mensaje);
        DBMS_OUTPUT.PUT_LINE('       - Resultado: ' || o_rdo);

        if o_rdo <> IPJ.TYPES.C_RESP_OK then
          o_rdo := 'Buscar Pase SUAC: '  || o_rdo;
          return;
        end if;

        -- Verifico si el usuario tiene permisos en el area del pase encontrado,
        -- y que este dentro de las areas aprobadas por IPJ
        begin
          DBMS_OUTPUT.PUT_LINE('   Verifica Permisos Usuario para pase');
          DBMS_OUTPUT.PUT_LINE('   Usuario SUAC = ' || v_usuario_suac);
          DBMS_OUTPUT.PUT_LINE('   Area Pase = ' || v_Unidad_Pase_Ultimo);
          DBMS_OUTPUT.PUT_LINE('   Area Tramite = ' || to_char(v_ubicacion_tramite));
          select codigo into v_Unidad_Destino
          from NUEVOSUAC.VT_USUARIOS_UNI_JUS
          where
            upper(id_usuario) =  upper(v_usuario_suac) and
            upper(n_unidad) = upper(v_Unidad_Pase_Ultimo) and
            codigo in
              (select codigo_suac
               from ipj.t_relacion_ubic_suac r
               where id_ubicacion = v_ubicacion_tramite);
        exception
          when NO_DATA_FOUND then
            v_Unidad_Destino := null;
        end;

        -- Si el ultimo pase, es al area del padre, lo acepto
        DBMS_OUTPUT.PUT_LINE('   Unidad Destino = ' || v_Unidad_Destino);
        if v_Unidad_Destino is not null then
          -- Acepto el ultimo pase
          DBMS_OUTPUT.PUT_LINE('   4-  Acepta Pase Creado');
          DBMS_OUTPUT.PUT_LINE('       - Sticker 14: ' || v_SUAC.v_nro_sticker_completo);
          DBMS_OUTPUT.PUT_LINE('       - Unidad Receptora: ' || v_Unidad_Destino);
          DBMS_OUTPUT.PUT_LINE('       - Usuario Receptor: ' || v_usuario_suac);
          IPJ.TRAMITES_SUAC.SP_Aceptar_Pase(
            p_nro_sticker_completo => v_SUAC.v_nro_sticker_completo,
            p_cod_unidad_receptora => v_Unidad_Destino,
            p_usuario_receptor => v_usuario_suac,
            p_cuerpos => v_SUAC.v_cuerpos,
            p_folios => v_SUAC.v_fojas,
            o_rdo => o_rdo,
            o_tipo_mensaje => o_tipo_mensaje);
          DBMS_OUTPUT.PUT_LINE('       - Resultado: ' || o_rdo);

          if nvl(o_rdo,IPJ.TYPES.C_RESP_OK) <> IPJ.TYPES.C_RESP_OK then
            o_rdo := 'Aceptar Pase SUAC: '  || o_rdo;
            return;
          end if;

        else
          o_rdo := 'El expediente posee un pase pendiente a otra �rea. No tiene permisos. Usuario: ' || p_cuil_usuario;
          o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
          return;
        end if;
      end if;
      DBMS_OUTPUT.PUT_LINE('    - O_Rdo ' || o_rdo );

    else
      o_rdo :=  'No se encuentran las �reas o mesas de SUAC para realizar los pases' ;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      return;
    end if;
    DBMS_OUTPUT.PUT_LINE('Finaliza SP_Recibir_Tramite_Mesa_Suac');
  EXCEPTION
    when OTHERS then
      o_rdo :=  'SP_Recibir_Tramite_Mesa_Suac: ' || To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Recibir_Tramite_Mesa_Suac;

  PROCEDURE SP_Buscar_Tipo_SUAC (
    o_n_tipo_suac out varchar2,
    o_n_subtipo_suac out varchar2,
    p_id_tramite in number)
  IS
    -- Dado el expediente, devuelve el Subtipo del expediente SUAC
    v_Cursor_Tramite  types.cursorType;
    v_error varchar2(2000);
    v_SUAC Tipo_SUAC;
  BEGIN

    -- Busco los datos del tramite en suac
    NUEVOSUAC.PCK_INTERFAZ_VERTICALES_V2.P_GET_TRAMITE_BY_ID(p_id_tramite, v_Cursor_Tramite, v_error);
    if nvl(v_error, IPJ.TYPES.C_RESP_OK) = IPJ.TYPES.C_RESP_OK then
      loop
        fetch v_Cursor_Tramite into v_SUAC;
        EXIT WHEN v_Cursor_Tramite%NOTFOUND or v_Cursor_Tramite%NOTFOUND is null;

      end loop;
      close v_Cursor_Tramite;
    end if;

    o_n_subtipo_suac := v_SUAC.v_subtipo;
    o_n_tipo_suac := v_SUAC.v_tipo;
  exception
    when NO_DATA_FOUND then
      o_n_tipo_suac := '';
      o_n_subtipo_suac := '';
  END SP_Buscar_Tipo_SUAC;

  PROCEDURE SP_Transformar_Nota (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_nuevo_expedinete out varchar2,
    p_id_tramite_ipj in number,
    p_cuil_usuario in varchar2)
  IS
    -- Dado un tr�mite de IPJ, si es una Nota la transforma en Expediente
    v_Cursor_Tramite  types.cursorType;
    v_row_tramite ipj.t_tramitesipj%ROWTYPE;
    v_SUAC Tipo_SUAC;
    v_error varchar2(2000);
    v_nuevo_expedinete varchar2(30);
  BEGIN
    select * into v_row_tramite
    from ipj.t_tramitesipj
    where
      id_tramite_ipj = p_id_tramite_ipj;

    -- Busco los datos del tramite en suac
    NUEVOSUAC.PCK_INTERFAZ_VERTICALES_V2.P_GET_TRAMITE_BY_ID(v_row_tramite.id_tramite, v_Cursor_Tramite, v_error);
    if nvl(v_error, IPJ.TYPES.C_RESP_OK) = IPJ.TYPES.C_RESP_OK then
      loop
        fetch v_Cursor_Tramite into v_SUAC;
        EXIT WHEN v_Cursor_Tramite%NOTFOUND or v_Cursor_Tramite%NOTFOUND is null;

      end loop;
      close v_Cursor_Tramite;

      -- Si no es una NOTA, no hago nada
      if v_SUAC.v_expediente = 'N' then
        -- Si el suuario no tiene el tr�mite, se lo transfiero
        if v_SUAC.v_usuario_actual <> FC_Armar_Usuario_Suac(p_cuil_usuario) then
          NUEVOSUAC.PCK_INTERFAZ_VERTICALES_V2.pr_transferir_tramite(
            i_sticker => v_SUAC.v_nro_sticker_completo,
            p_id_usuario => v_SUAC.v_usuario_actual, -- El jefe del area
            p_id_usuario_ori => v_SUAC.v_usuario_actual,
            p_id_usuario_des => FC_Armar_Usuario_Suac(p_cuil_usuario),
            p_resultado => o_rdo
          );

          -- Si no se peude asignar el usuario, salgo
          if nvl(o_rdo, IPJ.TYPES.C_RESP_OK) <> IPJ.TYPES.C_RESP_OK then
            o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
            o_nuevo_expedinete := null;
            return;
          else
            -- Para que asiente la transferencia
            commit;
          end if;
        end if;

        -- Transformo el expedinete en Nota
        NUEVOSUAC.PCK_INTERFAZ_VERTICALES_V2.P_CONVERTIR_NOTA(
          i_sticker => v_SUAC.v_nro_sticker_completo,
          i_usuario => p_cuil_usuario,
          i_unidad => FC_Buscar_Cod_Unidad_SUAC(v_SUAC.v_unidad_actual),
          o_nuevo_numero => v_nuevo_expedinete,
          o_error => o_rdo
        );

        if nvl(o_rdo, IPJ.TYPES.C_RESP_OK) = IPJ.TYPES.C_RESP_OK then
          update ipj.t_tramitesipj
          set expediente = v_nuevo_expedinete
          where
            id_tramite_ipj = p_id_tramite_ipj;
        end if;
      end if;

    end if;

    if nvl(o_rdo, IPJ.TYPES.C_RESP_OK) = IPJ.TYPES.C_RESP_OK then
      o_rdo := IPJ.TYPES.C_RESP_OK;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
      o_nuevo_expedinete := v_nuevo_expedinete;
      commit;
    else
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      o_nuevo_expedinete := null;
    end if;
  exception
    when NO_DATA_FOUND then
      o_rdo :=  'SP_Transformar_Nota: ' || To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Transformar_Nota;

  PROCEDURE SP_Transferir_Expediente (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_suac in number,
    p_usuario_dest_suac in varchar2)
  IS
    -- Dado un tr�mite de IPJ, si es una Nota la transforma en Expediente
    v_Cursor_Tramite  types.cursorType;
    v_row_tramite ipj.t_tramitesipj%ROWTYPE;
    v_SUAC Tipo_SUAC;
    v_error varchar2(2000);
  BEGIN
    select * into v_row_tramite
    from ipj.t_tramitesipj
    where
      id_tramite = p_id_tramite_suac;

    -- Busco los datos del tramite en suac
    NUEVOSUAC.PCK_INTERFAZ_VERTICALES_V2.P_GET_TRAMITE_BY_ID(v_row_tramite.id_tramite, v_Cursor_Tramite, v_error);
    --if nvl(v_error, IPJ.TYPES.C_RESP_OK) = IPJ.TYPES.C_RESP_OK then
    loop
      fetch v_Cursor_Tramite into v_SUAC;
      EXIT WHEN v_Cursor_Tramite%NOTFOUND or v_Cursor_Tramite%NOTFOUND is null;

    end loop;
    close v_Cursor_Tramite;

    -- Si el usuario no tiene el tr�mite, se lo transfiero
    if upper(v_SUAC.v_usuario_actual) <> upper(p_usuario_dest_suac) then
      DBMS_OUTPUT.PUT_LINE('        PR_TRANSFERIR_TRAMITE');
      DBMS_OUTPUT.PUT_LINE('              - i_sticker: ' || v_SUAC.v_nro_sticker_completo);
      DBMS_OUTPUT.PUT_LINE('              - p_id_usuario: ' || v_SUAC.v_usuario_actual);
      DBMS_OUTPUT.PUT_LINE('              - p_id_usuario_ori: ' || v_SUAC.v_usuario_actual);
      DBMS_OUTPUT.PUT_LINE('              - p_id_usuario_des: ' || p_usuario_dest_suac);
      NUEVOSUAC.PCK_INTERFAZ_VERTICALES_V2.pr_transferir_tramite(
        i_sticker => v_SUAC.v_nro_sticker_completo,
        p_id_usuario => v_SUAC.v_usuario_actual, -- El jefe del area
        p_id_usuario_ori => v_SUAC.v_usuario_actual,
        p_id_usuario_des => lower(p_usuario_dest_suac),
        p_resultado => o_rdo
      );
      DBMS_OUTPUT.PUT_LINE('            -  Resultado: ' || o_rdo);
      -- Si no se puede asignar el usuario, salgo
      if nvl(o_rdo, IPJ.TYPES.C_RESP_OK) <> IPJ.TYPES.C_RESP_OK then
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
        return;
      end if;
    end if;

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  exception
    when NO_DATA_FOUND then
      o_rdo :=  'SP_Transferir_Expediente: ' || To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Transferir_Expediente;


  /*********************************************************
      SOBRECARGA DE PARAMETROS
  ********************************************************/
  PROCEDURE SP_Subir_Notas_SUAC(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_cant_sincroniz out number,
    o_sticker out varchar,
    p_id_tramite_ipj in number)
  IS
  BEGIN
    SP_Subir_Notas_SUAC(
      o_rdo => o_rdo,
      o_tipo_mensaje => o_tipo_mensaje,
      o_cant_sincroniz => o_cant_sincroniz,
      o_sticker => o_sticker,
      p_id_tramite_ipj => p_id_tramite_ipj,
      p_transaccion => 'S'
    );

  END SP_Subir_Notas_SUAC;


end TRAMITES_SUAC;
/

