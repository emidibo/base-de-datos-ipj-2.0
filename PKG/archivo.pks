create or replace package ipj.Archivo
as
  PROCEDURE SP_Traer_Archivo(
    o_Cursor OUT types.cursorType,
    p_id_archivo_tramite in number,
    p_id_tramite_ipj in number,
    p_id_tramiteipj_accion in number);

  PROCEDURE SP_Traer_Archivo_Movimientos(
    o_Cursor OUT types.cursorType,
    p_id_archivo_tramite in number);


  PROCEDURE SP_Guardar_Archivo(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_archivo_tramite out number,
    p_id_archivo_tramite in number,
    p_id_tramite_ipj in number,
    p_id_tipo_archivo_tramite in number,
    p_nro_tabla in number,
    p_nro_caja in number,
    p_id_proveedor in number,
    p_nro_expurgo in number,
    p_n_expurgo in varchar2,
    p_fuera_expurgo in varchar2,
    p_estanteria in varchar2,
    p_estante in varchar2,
    p_columna in varchar2,
    p_observacion in varchar2,
    P_Expediente In Varchar2,
    P_Id_Tramiteipj_Accion In Number,
    p_nro_paquete in number,
    p_anio in number,
    p_id_ubicacion in number,
    p_cuil_usuario in varchar2,
    p_n_paquete in varchar2,
    p_Asienta_Expurgo varchar2,
    p_id_legajo in number,
    p_id_tramite_suac in number,
    p_sticker in varchar2,
    p_id_integrante in number,
    p_digitalizado number
  );

  PROCEDURE SP_Guardar_Archivo_Hist(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_archivo_tramite in number,
    p_fec_cambio in date,
    p_id_tramite_ipj in number,
    p_id_tipo_archivo_tramite in number,
    p_nro_tabla in number,
    p_nro_caja in number,
    p_id_proveedor in number,
    p_nro_expurgo in number,
    p_fuera_expurgo in varchar2,
    p_estanteria in varchar2,
    p_estante in varchar2,
    p_columna in varchar2,
    p_observacion in varchar2,
    P_Expediente In Varchar2,
    P_Id_Tramiteipj_Accion In Number,
    p_id_paquete in number,
    p_anio in number,
    p_id_ubicacion in number,
    p_cuil_usuario in varchar2,
    p_id_tramite in varchar2);

  PROCEDURE SP_Guardar_Archivo_Mov(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Archivo_Tramite in number,
    p_Id_Proveedor in number,
    p_Fecha in varchar2,
    p_Id_Mov_Archivo in number,
    p_Id_Tramite_Ipj_Solicitud in number);

  PROCEDURE SP_Buscar_Archivo(
    o_Cursor OUT types.cursorType,
    p_id_ubicacion in number,
    p_cuit in varchar2,
    p_razon_social in varchar2,
    p_nro_registro in varchar2,
    p_ficha in varchar2,
    p_matricula in varchar2,
    p_expediente in varchar2,
    p_Es_Reapertura in char);

    PROCEDURE SP_Buscar_Archivo_CP(
        o_Cursor OUT types.cursorType,
        o_total_pages out number,
        p_id_ubicacion in number,
        p_id_accion_archivo in number,
        p_expediente in varchar2,
        p_anio in number,
        p_nro_registro in varchar2,
        p_razon_social in varchar2,
        p_id_legajo in number,
        p_n_paquete in varchar2,
        p_nro_paquete in number,
        p_nro_caja in number,
        p_id_proveedor in number,
        p_nro_expurgo in number,
        p_page_number in number,
        p_page_size in number);

  PROCEDURE SP_Traer_Archivo_Proveedores(
    o_Cursor OUT types.cursorType);

  PROCEDURE SP_Traer_Archivo_Origen(
    o_Cursor OUT types.cursorType);

  PROCEDURE SP_Traer_Archivo_Acciones(
    o_Cursor OUT types.cursorType);

  PROCEDURE SP_Traer_Archivo_Expurgo(
    o_Cursor OUT types.cursorType);

  PROCEDURE SP_Traer_Tipo_Movimiento(
    o_Cursor OUT types.cursorType);

  PROCEDURE SP_Guardar_Archivo_Proveedores(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_proveedor OUT number,
    p_id_proveedor in number,
    p_n_proveedor in varchar2);

  PROCEDURE SP_Buscar_Paquete(
    o_Nro_Paquete OUT Number,
    o_Nro_Tabla out number,
    p_anio in number,
    p_n_paquete in varchar2,
    p_id_ubicacion number,
    p_id_accion_archivo number);

  PROCEDURE SP_Buscar_Expediente_SUAC(
    o_Cursor OUT types.cursorType,
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_sticker_14 in varchar2,
    p_id_archivo_tramite number);

  PROCEDURE SP_Traer_Archivo_Prest_Pend(
    o_Cursor OUT types.cursorType,
    p_cuil_usuario in varchar2,
    p_id_ubicacion in number);

  PROCEDURE SP_Traer_Archivo_Prestamos(
    o_Cursor OUT types.cursorType,
    p_cuil_usuario in varchar2,
    p_id_archivo_tramite in number,
    p_id_estado_prestamo in number
  );

  PROCEDURE SP_Guardar_Archivo_Prestamo(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Archivo_Tramite in number,
    p_Fecha_Pedido in varchar2, -- es fecha
    p_Cuil_Solicitante in varchar2,
    p_Fecha_Entrega in varchar2, -- es fecha
    p_Ok_Entrega in varchar2,
    p_Fecha_Devolucion in varchar2, -- es fecha
    p_Ok_Devolucion in varchar2,
    p_Id_Estado_Prestamo in number,
    p_eliminar in number, -- 1 elimina
    p_cuil_prestador in varchar2,
    p_Id_Motivo_Vista ipj.t_tipos_motivos_vista.id_motivo_vista%TYPE,
    p_prioridad number,
    p_Id_Ubicacion NUMBER
    );

  PROCEDURE SP_Reabrir_Tramite (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_cuil_usuario_Area in varchar2,
    p_Cuil_Usuario_Reabre in varchar2,
    p_observacion in varchar2 ,
    p_id_ubicacion in number
  );

  PROCEDURE SP_Reabrir_Accion (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in number,
    p_id_tramiteipj_accion in number,
    p_cuil_usuario in varchar2,
    p_observacion in varchar2);

  PROCEDURE SP_Archivar_Expediente(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in number,
    p_id_tramite_suac in number);

  PROCEDURE SP_Informar_Prestamos(
    o_Cursor OUT types.cursorType,
    p_Lista_Tramites in varchar2);

  PROCEDURE SP_Traer_Archivo_Desarchivo(
    o_Cursor OUT types.cursorType,
    p_cuil_usuario in varchar2,
    p_id_estado_desarchivo in number);

   PROCEDURE SP_Guardar_Archivo_Desarchivo(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_archivo_tramite in number,
    p_fecha_pedido in varchar2, -- es fecha
    p_cuil_solicitante in varchar2,
    p_fecha_desarchivo in varchar2, -- es fecha
    p_id_estado_desarchivo in number,
    p_cuil_usuario in varchar2,
    p_id_documento in number,
    p_prioridad in number,
    p_Id_Ubicacion in NUMBER, -- ubicacion del solicitante del desarchivo
    p_id_motivo_vista in number
  );

  PROCEDURE SP_Traer_Movimientos_Solicitud(
    o_Cursor OUT types.cursorType,
    p_Id_Ubicacion in NUMBER,
    p_TipoSolicitud in VARCHAR2,
    p_Expediente IN VARCHAR2,
    p_Fecha_Desde IN VARCHAR2,
    p_Fecha_Hasta IN VARCHAR2);

  PROCEDURE SP_Validar_Usuario_Desarchivo(
    o_Res OUT NUMBER,
    p_Cuil_Usuario IN VARCHAR2);
    
  PROCEDURE SP_Realizar_Expurgo(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_cant_errores out number,
    p_nro_expurgo in number,
    p_fecha in varchar2, -- Es Fecha
    p_resolucion in varchar2,
    p_cuil_usuario in varchar2
  );
  
  PROCEDURE SP_Traer_Error_Expurgo(
    o_Cursor OUT types.cursorType
  );
  
  PROCEDURE SP_Desarchivo_Digital (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_cuil_usuario in varchar2,
    p_Cuil_Usuario_asigna in varchar2,
    p_id_motivo_vista in number,
    p_observacion in varchar2,
    p_id_ubicacion in number
  );
    
  /**********************************************************
  ***********************************************************
  -------- SOBRECARGA DE PARAMETROS --------------------------------------------
  ***********************************************************
  **********************************************************/
   

end Archivo;
/

create or replace package body ipj.Archivo is

  PROCEDURE SP_Traer_Archivo(
    o_Cursor OUT types.cursorType,
    p_id_archivo_tramite in number,
    p_id_tramite_ipj in number,
    p_id_tramiteipj_accion in number)
  IS
    v_valid_parametros varchar2(500);
    v_nro_registro varchar2(15);
    v_id_legajo number(15);
    v_id_accion_archivo number(5);
    v_nro_tabla number(5);
    v_nro_paquete number(6);
    v_n_accion_archivo varchar2(200);
    v_anio number;
    v_n_paquete varchar2(50);
    v_id_mes varchar2(4);
    v_id_ubicacion number;
    v_Archivado varchar2(20);
    v_id_estado_tram number;
    v_sticker varchar2(20);
    v_Expurgado number;
  BEGIN
    -- Si tiene historial, es que alguien lo carg� en Archivo
    select count(1) into v_Archivado
    from IPJ.T_ARCHIVO_TRAMITE_HIST
    where
      id_archivo_tramite = p_id_archivo_tramite;
      
    -- Busco el estado del archivo, para validar expurgados 
    select nvl(id_estado_archivo, 0) into v_Expurgado
    from IPJ.T_ARCHIVO_TRAMITE
    where
      id_archivo_tramite = p_id_archivo_tramite;

    --Si ya esta archivado, uso los datos del legajo
    begin
      select l.id_legajo, l.nro_ficha into v_id_legajo, v_nro_registro
      from ipj.t_legajos l join IPJ.T_ARCHIVO_TRAMITE arch
          on l.id_legajo = arch.id_legajo
      where
        arch.id_archivo_tramite = p_id_archivo_tramite;
    exception
      when NO_DATA_FOUND then
        -- Si no esta archivado, busco los datos de la accion
        begin
          select acc.id_legajo, l.nro_ficha into v_id_legajo, v_nro_registro
          from ipj.t_tramitesipj_acciones acc join IPJ.t_legajos l
              on l.id_legajo = acc.id_legajo
          where
            acc.id_tramite_ipj = p_id_tramite_ipj and
            acc.id_tramiteipj_accion = p_id_tramiteipj_accion;
        exception
          when NO_DATA_FOUND then
            v_id_legajo := null;
            v_nro_registro := null;
        end;
    end;
    
    --******************************************************
    -- CUERPO DEL SP
    --******************************************************
    --Ya esta archivado, o se expurg�
    if nvl(v_Archivado, 0) > 0 or v_Expurgado = 4 then
      OPEN o_Cursor FOR
        select
           arch.id_estado_archivo, arch.id_archivo_tramite, arch.ID_TRAMITE_IPJ,
           arch.ID_ACCION_ARCHIVO, arch.NRO_TABLA, arch.NRO_CAJA, arch.ID_paquete,
           arch.anio, arch.ID_PROVEEDOR, arch.NRO_EXPURGO, arch.FUERA_EXPURGO,
           arch.ESTANTERIA, arch.ESTANTE, arch.COLUMNA, arch.OBSERVACION,
           arch.FEC_ARCHIVO, arch.EXPEDIENTE, arch.ID_TRAMITEIPJ_ACCION,
           arch.id_legajo, arch.id_integrante, ARCH.ID_TRAMITE id_tramite_suac,
           u.id_ubicacion, U.N_UBICACION, AA.N_ACCION_ARCHIVO,
           l.denominacion_sia, v_nro_registro nro_registro, pq.n_paquete, pq.nro_paquete,
           ex.n_expurgo, ea.n_estado_archivo, 'N' Asienta_Expurgo,
           (case when EX.FECHA is null then 'N' else 'S' end) Asienta_Expurgo,
           i.nro_documento,
           (select p.nombre||' '||p.apellido from rcivil.vt_pk_persona p where p.id_sexo = i.id_sexo and p.nro_documento = i.nro_documento and p.pai_cod_pais = i.pai_cod_pais and p.id_numero = i.id_numero) detalle,
           IPJ.TRAMITES_SUAC.FC_Obtener_Sticker_Archivo(id_tramite) Sticker,
           nvl(nvl(l.denominacion_sia, i.detalle), IPJ.TRAMITES_SUAC.FC_BUSCAR_INICIADOR_SUAC(to_number(arch.id_tramite))) Iniciador,
           arch.digitalizado, to_char(ex.fecha, 'dd/mm/rrrr') fecha_expurgo
        from IPJ.T_ARCHIVO_TRAMITE arch join IPJ.t_ubicaciones u
            on U.ID_UBICACION = arch.ID_UBICACION
          left join ipj.t_legajos l
            on l.id_legajo = arch.id_legajo
          left join IPJ.T_TIPOS_ESTADO_ARCHIVO ea
            on ea.id_estado_Archivo = arch.id_estado_archivo
          left join IPJ.T_ACCIONES_ARCHIVO aa
            on AA.ID_ACCION_ARCHIVO = ARCH.ID_ACCION_ARCHIVO
          left join IPJ.T_PAQUETES_ARCHIVO pq
            on PQ.ID_PAQUETE = ARCH.ID_PAQUETE
          left join ipj.t_expurgo ex
            on ex.nro_expurgo = arch.nro_expurgo
          left join ipj.t_integrantes i
            on arch.id_integrante = i.id_integrante
        where
          arch.ID_ARCHIVO_TRAMITE = p_id_archivo_tramite;

    else -- No se archivo NUNCA, calculo los valores
      -- busco el tipo de accion de archivo, asociado a las acciones de modificacion
      begin
        select AccArch.id_accion_archivo, AccArch.N_Accion_Archivo, tr.id_estado_ult
          into v_id_accion_archivo, v_n_accion_archivo, v_id_estado_tram
        from ipj.t_tramitesipj_acciones acc join IPJ.t_tipos_accionesipj ta
            on acc.id_tipo_accion = ta.id_tipo_accion
          join ipj.t_tramitesipj tr
            on tr.id_tramite_ipj = acc.id_tramite_ipj
          join IPJ.T_ACCIONES_ARCHIVO AccArch
            on TA.ID_ACCION_ARCHIVO = AccArch.id_accion_archivo
        where
          acc.id_tramite_ipj = p_id_tramite_ipj and
          acc.id_legajo = v_id_legajo and
          acc.id_tramiteipj_accion <> p_id_tramiteipj_accion and
          nvl(ta.id_protocolo, 0) <> 0
          and rownum = 1;

        if v_id_estado_tram = IPJ.TYPES.C_ESTADOS_PERIMIDO then
          v_id_accion_archivo := 6;
          v_n_accion_archivo := 'Perimidas';
        end if;
      exception
        when NO_DATA_FOUND then
          v_id_accion_archivo := 7;
          v_n_accion_archivo := 'Varios';
      end;

      -- Busco el A�o y Nombre de Paquete para consulta el Nro_Paquete y Nro_Taba
      begin
        -- Si en Archivo es tipo ASAMBLEA, usa el Mes como Nombre del paquete
        if v_id_accion_archivo = IPJ.Types.c_Tipo_Arch_Asambleas then
          select
            to_number(to_char(max(fec_acta), 'rrrr')), to_char(max(fec_acta), 'MM')
              into v_anio, v_id_mes
          from IPJ.T_entidades_acta ea
          where
            ea.id_tramite_ipj = p_id_tramite_ipj and
            ea.id_legajo = v_id_legajo;

          -- Busco el nombre del mes en espa�ol
          select n_mes into v_n_paquete
          from T_COMUNES.t_meses
          where id_mes = v_id_mes;
        else
          select
            to_number(to_char(Fecha_Inicio, 'rrrr')), null  into v_anio, v_n_paquete
          from IPJ.t_tramitesipj tr
          where
            tr.id_tramite_ipj = p_id_tramite_ipj;
        end if;

        -- como siempre existe, busco el area, y si no tenia tipo accion de archivo, uso la calculada
        select  id_ubicacion, nvl(id_accion_archivo, v_id_accion_archivo) into v_id_ubicacion, v_id_accion_archivo
        from IPJ.T_ARCHIVO_TRAMITE
        where  id_archivo_tramite = p_id_archivo_tramite;

        IPJ.ARCHIVO.SP_Buscar_Paquete(
          o_Nro_Paquete => v_nro_paquete,
          o_Nro_Tabla => v_nro_tabla,
          p_anio => v_anio,
          p_n_paquete => v_n_paquete,
          p_id_ubicacion => v_id_ubicacion,
          p_id_accion_archivo => v_id_accion_archivo);
      exception
         when NO_DATA_FOUND then
           v_nro_tabla := 1;
           v_nro_paquete := 1;
      end;

      -- Si no tengo los datos, los muestro desde el tramite, la accion y la vista de suac
      OPEN o_Cursor FOR
        select
           Arch.Id_Archivo_Tramite, Arch.Id_Tramite_Ipj, nvl(arch.id_Accion_archivo, v_Id_Accion_Archivo) Id_Accion_Archivo,
           nvl(arch.nro_tabla, v_Nro_Tabla) Nro_Tabla, arch.Nro_Caja,  v_Nro_Paquete Nro_Paquete,  nvl(arch.anio, v_Anio) Anio,
           arch.Id_Proveedor, arch.Nro_Expurgo, arch.Fuera_Expurgo,
           arch.Estanteria, arch.Estante, arch.Columna, arch.Observacion, arch.Fec_Archivo,
           Arch.Expediente, Arch.Id_Tramiteipj_Accion, arch.id_tramite id_tramite_suac,
           Arch.Id_Ubicacion, U.N_Ubicacion, Arch.id_integrante,
           v_N_Accion_Archivo N_Accion_Archivo,
           Arch.Id_Legajo, L.Denominacion_Sia,
           v_Nro_Registro Nro_Registro, v_N_Paquete N_Paquete,
           (select n_expurgo from IPJ.T_EXPURGO e where e.nro_expurgo = arch.Nro_Expurgo) N_Expurgo,
           'N' Asienta_Expurgo, Ipj.Types.C_Estados_Archivo_Inicial Id_Estado_Archivo,
           (Select N_Estado_Archivo From Ipj.T_Tipos_Estado_Archivo Where Id_Estado_Archivo = Ipj.Types.C_Estados_Archivo_Inicial) N_Estado_Archivo,
           i.nro_documento,
           (select p.nombre||' '||p.apellido from rcivil.vt_pk_persona p where p.id_sexo = i.id_sexo and p.nro_documento = i.nro_documento and p.pai_cod_pais = i.pai_cod_pais and p.id_numero = i.id_numero) detalle,
           IPJ.TRAMITES_SUAC.FC_Obtener_Sticker_Archivo(id_tramite) Sticker,
           nvl(nvl(l.denominacion_sia, (select p.nombre||' '||p.apellido from rcivil.vt_pk_persona p where p.id_sexo = i.id_sexo and p.nro_documento = i.nro_documento and p.pai_cod_pais = i.pai_cod_pais and p.id_numero = i.id_numero)),
           IPJ.TRAMITES_SUAC.FC_BUSCAR_INICIADOR_SUAC(to_number(arch.id_tramite))) Iniciador,
           arch.digitalizado,
           (select to_char(fecha, 'dd/mm/rrrr') from IPJ.T_EXPURGO e where e.nro_expurgo = arch.Nro_Expurgo) fecha_expurgo
        from IPJ.T_ARCHIVO_TRAMITE arch join IPJ.t_ubicaciones u
            on U.ID_UBICACION = arch.ID_UBICACION
          left join ipj.t_legajos l
            on l.id_legajo = arch.id_legajo
          left join ipj.t_integrantes i
            on arch.id_integrante = i.id_integrante
        where
          arch.id_archivo_tramite = p_id_archivo_tramite;
    end if;

  END SP_Traer_Archivo;

  PROCEDURE SP_Traer_Archivo_Movimientos(
    o_Cursor OUT types.cursorType,
    p_id_archivo_tramite in number)
  IS
    v_valid_parametros varchar2(500);
  BEGIN

    -- Cuerpo del SP
    OPEN o_Cursor FOR
      select
         am.ID_ARCHIVO_TRAMITE, am.ID_PROVEEDOR, to_char(am.FECHA, 'dd/mm/rrrr') Fecha,
         am.ID_MOV_ARCHIVO, am.ID_TRAMITE_IPJ_SOLICITUD, AP.N_PROVEEDOR,
         TMA.N_MOV_ARCHIVO
      from IPJ.T_ARCHIVO_MOVIMIENTOS am join IPJ.T_ARCHIVO_PROVEEDORES ap
          on am.id_proveedor = AP.ID_PROVEEDOR
        join IPJ.T_TIPOS_MOV_ARCHIVO tma
          on TMA.ID_MOV_ARCHIVO = AM.ID_MOV_ARCHIVO
      where
        am.ID_ARCHIVO_TRAMITE = p_id_archivo_tramite
      order by am.fecha desc;

  END SP_Traer_Archivo_Movimientos;


  PROCEDURE SP_Guardar_Archivo_Hist(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_archivo_tramite in number,
    p_fec_cambio in date,
    p_id_tramite_ipj in number,
    p_id_tipo_archivo_tramite in number,
    p_nro_tabla in number,
    p_nro_caja in number,
    p_id_proveedor in number,
    p_nro_expurgo in number,
    p_fuera_expurgo in varchar2,
    p_estanteria in varchar2,
    p_estante in varchar2,
    p_columna in varchar2,
    p_observacion in varchar2,
    P_Expediente In Varchar2,
    P_Id_Tramiteipj_Accion In Number,
    p_id_paquete in number,
    p_anio in number,
    p_id_ubicacion in number,
    p_cuil_usuario in varchar2,
    p_id_tramite in varchar2)
  IS
  BEGIN

    insert into IPJ.T_ARCHIVO_TRAMITE_HIST (ID_ARCHIVO_TRAMITE, FEC_CAMBIO, ID_TRAMITE_IPJ, ID_ACCION_ARCHIVO,
      NRO_TABLA, NRO_CAJA, ID_PROVEEDOR, NRO_EXPURGO, FUERA_EXPURGO, ESTANTERIA, ESTANTE,
      COLUMNA, OBSERVACION, FEC_ARCHIVO, EXPEDIENTE, ID_TRAMITEIPJ_ACCION, ID_PAQUETE, ANIO, cuil_usuario, id_ubicacion,id_tramite)
    values (p_id_archivo_tramite, p_fec_cambio, p_id_tramite_ipj, p_id_tipo_archivo_tramite, p_nro_tabla, p_nro_caja,
      p_id_proveedor, p_nro_expurgo, p_fuera_expurgo, p_estanteria, p_estante, p_columna, p_observacion,
      sysdate, p_expediente, P_Id_Tramiteipj_Accion, p_id_paquete, p_anio, p_cuil_usuario, p_id_ubicacion,p_id_tramite);

    o_rdo := TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
    WHEN OTHERS THEN
      o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;

  END SP_Guardar_Archivo_Hist;

  PROCEDURE SP_Guardar_Archivo(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_archivo_tramite out number,
    p_id_archivo_tramite in number,
    p_id_tramite_ipj in number,
    p_id_tipo_archivo_tramite in number,
    p_nro_tabla in number,
    p_nro_caja in number,
    p_id_proveedor in number,
    p_nro_expurgo in number,
    p_n_expurgo in varchar2,
    p_fuera_expurgo in varchar2,
    p_estanteria in varchar2,
    p_estante in varchar2,
    p_columna in varchar2,
    p_observacion in varchar2,
    p_Expediente In Varchar2,
    P_Id_Tramiteipj_Accion In Number,
    p_nro_paquete in number,
    p_anio in number,
    p_id_ubicacion in number,
    p_cuil_usuario in varchar2,
    p_n_paquete in varchar2,
    p_Asienta_Expurgo varchar2,
    p_id_legajo in number,
    p_id_tramite_suac in number,
    p_sticker in varchar2,
    p_id_integrante in number,
    p_digitalizado in number
    )
  IS
    /*********************************************************
      Este procedimiento guarda un registro en Archivo, completanto los datos faltantes de ser necesario:
      - Si no existe el tr�mite IPJ, lo crea y lo asocia al expediente SUAC indicado.
      - Si no tenia la accion de archivo, la crea.
      - Al guardar en archivo, el tramite se marca como finalizado
    *********************************************************/
    v_id_archivo_tramite number(10);
    v_existe_expurgo number(5);
    v_existe_paquete number(5);
    v_nro_tabla number(5);
    v_nro_caja number(10);
    v_id_paquete number(6);
    v_estanteria varchar2 (50);
    v_estante varchar2 (50);
    v_columna varchar2 (50);
    v_fuera_expurgo varchar2(1);
    v_nro_expurgo number (10);
    v_id_proveedor number(5);
    v_id_estado_archivo number(5);
    v_anio number(4);
    v_Row_Tramite  IPJ.t_tramitesipj%ROWTYPE;
    v_Row_Accion  IPJ.t_tramitesipj_acciones%ROWTYPE;
    v_Row_Arch_Tramite IPJ.t_archivo_tramite%ROWTYPE;
    v_Row_Integ ipj.t_integrantes%ROWTYPE;
    v_id_tramite_ipj number(10);
    v_id_pagina number(6);
    v_id_tramite_suac number;
    v_id_ubicacion_area number;
    v_cuil_responsable varchar2(20);
    v_id_clasif_archivo number;
    v_Expediente Varchar2(50);
    v_id_estado_tramite number(5);
    v_area_SUAC varchar2(500);
    v_rdo_SUAC varchar2(2000);
    v_tipo_mensaje_SUAC number;
    v_tipo_SUAC ipj.tramites_suac.Tipo_SUAC;
    v_fecha_pase date;
    v_usuario_pase varchar2(200);
    v_unidad_pase varchar2(500);
    v_Prox_pase number;
    v_area_gestion varchar2(200);
    v_usr_gestion varchar2(200);
    v_grupo_gestion varchar2(200);
    v_error_Gestion varchar(2000);
    v_error_Suac varchar(2000);
    v_En_Mesa_SUAC number;
    v_codigo_archivo_suac varchar2(50);
  BEGIN
    if IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('HAB_LOG_ARCHIVO') = '1' then
      IPJ.VARIOS.SP_GUARDAR_LOG(
        p_METODO => 'SP_Guardar_Archivo',
        p_NIVEL => 'Gestion - Archivo',
        p_ORIGEN => 'DB',
        p_USR_LOG  => 'DB',
        p_USR_WINDOWS => 'Sistema',
        p_IP_PC  => null,
        p_EXCEPCION =>
          'Id Archivo Tramites = ' || to_char(p_id_archivo_tramite)
          || ' / Id Tramite IPJ = ' || to_char(p_id_tramite_ipj)
          || ' / Id Tipo_Archivo Tramite = ' || to_char(p_id_tipo_archivo_tramite)
          || ' / Nro Tabla = ' || to_char(p_nro_tabla)
          || ' / Nro Caja = ' || to_char(p_nro_caja)
          || ' / Id Proveedor = ' || to_char(p_id_proveedor)
          || ' / Nro Expurgo = ' || to_char(p_nro_expurgo)
          || ' / Nom Expurgo = ' || p_n_expurgo
          || ' / Fuera Expurgo = ' || p_fuera_expurgo
          || ' / Estanteria = ' || p_estanteria
          || ' / Estante = ' || p_estante
          || ' / Columna = ' || p_columna
          || ' / Obs = ' || p_observacion
          || ' / Expediente = ' || p_Expediente
          || ' / ID Tramite Accion = ' || to_char(P_Id_Tramiteipj_Accion)
          || ' / Nro Paquete = ' || to_char(p_nro_paquete)
          || ' / A�o = ' || to_char(p_anio)
          || ' / Id Ubicacion = ' || to_char(p_id_ubicacion)
          || ' / Cuil Usuario = ' || p_cuil_usuario
          || ' / Nom Paquete = ' || p_n_paquete
          || ' / Asienta Expurgo = ' || p_Asienta_Expurgo
          || ' / Id Legajo = ' || to_char(p_id_legajo)
          || ' / Id SUAC = ' || to_char(p_id_tramite_SUAC)
          || ' / Sticker = ' || p_sticker
          || ' / Id Integrante = ' || to_char(p_id_integrante)
          || ' / Digitalizado = ' || to_char(p_digitalizado)
      );
    end if;

    -- Busco si existe en Gestion y donde esta.
    if p_id_archivo_tramite <> 0 then
      select * into v_Row_Arch_Tramite
      from IPJ.T_ARCHIVO_TRAMITE
      where
        id_archivo_tramite = p_id_archivo_tramite;
        
      -- Si ya esta expurgado, no permito editarlo
      if v_Row_Arch_Tramite.id_estado_archivo = 4 then
        o_rdo := 'El expediente ya fue expurgado, no es posible editarlo.';
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR; 
        o_id_archivo_tramite := v_Row_Arch_Tramite.id_archivo_tramite;
        return;
      end if;

      -- Si en Gestion no existe, lo manejo como En Archivo
      begin
        select * into v_Row_Tramite
        from ipj.t_tramitesipj
        where
          id_tramite_ipj = v_Row_Arch_Tramite.id_tramite_ipj;
      exception
        when NO_DATA_FOUND then
          v_Row_Tramite.id_ubicacion := IPJ.TYPES.C_AREA_ARCHIVO;
      end;
    else
      -- Si es Nuevo, veo si el expediente est� en Gestion
      begin
        select * into v_Row_Tramite
        from ipj.t_tramitesipj
        where
          id_tramite = p_id_tramite_SUAC;
      exception
        when NO_DATA_FOUND then
          v_Row_Tramite.id_ubicacion := IPJ.TYPES.C_AREA_ARCHIVO;
      end;
    end if;

    -- Si en Gestion no esta en Archivo o Mesa y el tr�mite no esta rechazado, lo informo y no procede.
    if v_Row_Tramite.id_ubicacion not in (IPJ.TYPES.C_AREA_ARCHIVO, IPJ.TYPES.C_AREA_MESASUAC ) and v_Row_Tramite.id_estado_ult not in (203, 204, 200) then
      select n_ubicacion into v_area_gestion
      from ipj.t_ubicaciones
      where
        id_ubicacion =  v_Row_Tramite.id_ubicacion;

      if v_Row_Tramite.cuil_ult_estado is not null then
        select descripcion into v_usr_gestion
        from ipj.t_usuarios
        where
          cuil_usuario =  v_Row_Tramite.cuil_ult_estado;
      end if;

      if nvl(v_Row_Tramite.ID_GRUPO_ULT_ESTADO, 0) > 0 then
        select n_grupo into v_grupo_gestion
        from ipj.t_grupos
        where
          id_grupo =  v_Row_Tramite.ID_GRUPO_ULT_ESTADO;
      end if;
      
      v_error_Gestion := 'El tr�mite en Gesti�n se encuentra en ' || v_area_gestion ||
        ' a nombre de ' || nvl(v_usr_gestion, v_grupo_gestion) ||
        '. Solicte que lo pase a Archivo o Mesa SUAC.';
    end if;

    -- Verifico el tr�mite en SUAC, que este en Mesa o con un pase a Mesa
    if p_id_tramite_SUAC > 0 or p_sticker is not null then
      -- Busco el tr�mite en SUAC
      if nvl(p_id_tramite_SUAC, 0) > 0 then
        IPJ.TRAMITES_SUAC.SP_Buscar_Suac (
          o_rdo => v_rdo_SUAC,
          o_tipo_mensaje => v_tipo_mensaje_SUAC,
          o_SUAC => v_Tipo_SUAC,
          p_id_tramite => p_id_tramite_SUAC,
          p_loguear => 'N'
        );
      else
        IPJ.TRAMITES_SUAC.SP_Buscar_Sticker_Suac (
          o_rdo => v_rdo_suac,
          o_tipo_mensaje => v_tipo_mensaje_suac,
          o_SUAC => v_Tipo_SUAC,
          p_Sticker_14 => p_sticker
        );
      end if;

      -- Busco si el nombre del area corresponde a alguno de los utilizados para la mesa (habilitados o no)
      select count(1) into v_En_Mesa_SUAC
      from nuevosuac.vt_unidades v join ipj.t_ubicaciones u
        on v.codigo = u.codigo_suac
      where
        u.id_ubicacion = IPJ.TYPES.C_AREA_ARCHIVO and
        v.n_unidad = v_Tipo_SUAC.v_unidad_actual;

      -- Si esta no esta en MESA SUAC
      if v_En_Mesa_SUAC = 0 then
        if v_Tipo_SUAC.v_estado <> 'A RECIBIR' then
          v_area_SUAC := v_Tipo_SUAC.v_unidad_actual;
        else
          IPJ.TRAMITES_SUAC.SP_Buscar_Pases(
            p_nro_sticker_completo => v_Tipo_SUAC.v_nro_sticker_completo,
            o_fecha_pase => v_fecha_pase,
            o_usuario_pase => v_usuario_pase,
            o_unidad_pase => v_unidad_pase,
            o_Prox_pase => v_Prox_pase,
            o_rdo => v_rdo_SUAC,
            o_tipo_mensaje => v_tipo_mensaje_SUAC
          );

          v_area_SUAC := v_unidad_pase;

          -- Busco si el pase es a un area de Mesa
          select count(1) into v_En_Mesa_SUAC
          from nuevosuac.vt_unidades v join ipj.t_ubicaciones u
            on v.codigo = u.codigo_suac
          where
            u.id_ubicacion = IPJ.TYPES.C_AREA_ARCHIVO and
            v.n_unidad = v_unidad_pase;
        end if;
      else
        v_area_SUAC := v_Tipo_SUAC.v_unidad_actual;
      end if;
    else
      -- Si tiene relacion a SUAC, lo uso como En Mesa
      v_En_Mesa_SUAC := 1;
    end if;

    -- Si no esta en MESA o pasa a MESA, lo informo
    if v_En_Mesa_SUAC = 0 then
      v_error_Suac := 'El tr�mite en SUAC se encuentra o tiene un pase pendiente a ' || v_area_SUAC;
    end if;

    -- Si alguna de las 2 validaciones falla, lo informo
    if v_error_Gestion is not null or v_error_suac is not null  then
      o_rdo := v_error_gestion || chr(10) || chr(13) || v_error_suac;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      o_id_archivo_tramite := nvl(p_id_archivo_tramite, 0);
      return;
    end if;

    --********************************************************
    -- SI ESTA CORRECTAMENTE UBICADO EN SUAC Y GESTION, procedo con el archivado.
    --********************************************************
    -- Busco si el expurgo existe
    select count(*) into v_existe_expurgo
    from ipj.t_expurgo
    where
      nro_expurgo = p_nro_expurgo;

    -- Si el expurgo es nuevo, lo agrego a la lista
    if nvl(v_existe_expurgo, 0) = 0 and p_n_expurgo is not null then
      insert into ipj.t_expurgo (nro_expurgo, n_expurgo, fecha, resolucion)
      values (p_nro_expurgo, nvl(p_n_expurgo, 'Expurgo Nro. ' || to_char(p_nro_expurgo)), null, null);
    end if;
    
    v_nro_expurgo := p_nro_expurgo;
    v_anio := (case when p_anio = 0 then null else p_anio end);

    -- Si no esta expurgado, veo si el paquete es nuevo, lo agrego a la lista de paquetes
    if nvl(p_nro_expurgo, 0) = 0 then
      begin
        select id_paquete into v_id_paquete
        from ipj.t_paquetes_archivo
        where
          nro_paquete = p_nro_paquete and
          ltrim(upper(nvl(n_paquete, '-'))) = ltrim(upper(nvl(decode(p_id_tipo_archivo_tramite, 2, p_n_paquete, null), '-'))) and -- Solo se tiene en cuenta para ASAMBLEAS
          nvl(anio, 0) = nvl((case when p_id_tipo_archivo_tramite in (1, 2, 3, 9, 11) then v_anio else null end), 0) and -- Solo se cuenta para Const. Asambleas, Reformas y Rechazos
          Id_Accion_Archivo = p_id_tipo_archivo_tramite and
          id_ubicacion = p_id_ubicacion;
      exception
        when NO_DATA_FOUND then
          -- Si no existe un registro, lo agrego
          select max(id_paquete)+1 into v_id_paquete
          from ipj.t_paquetes_archivo;

          insert into ipj.t_paquetes_archivo (id_paquete, n_paquete, nro_paquete, anio, Id_Accion_Archivo, id_ubicacion)
          values (v_id_paquete, decode(p_id_tipo_archivo_tramite, 2, p_n_paquete, null), p_nro_paquete,
            (case when p_id_tipo_archivo_tramite in (1, 2, 3, 9, 11) then v_anio else null end), p_id_tipo_archivo_tramite, p_id_ubicacion);
      end;
    end if;

    -- seteo el calculos de algunos valores
    v_nro_tabla := (case when p_Asienta_Expurgo = 'S' then null else p_nro_tabla end);
    v_id_paquete := (case when p_Asienta_Expurgo = 'S' then null else v_id_paquete end);
    v_nro_caja := (case when p_Asienta_Expurgo = 'S' then null else p_nro_caja end);
    v_id_proveedor := (case when p_Asienta_Expurgo = 'S' then null else p_id_proveedor end);
    v_estanteria := (case when nvl(p_nro_caja, 0) > 0 then '' else p_estanteria end);
    v_estante := (case when nvl(p_nro_caja, 0) > 0 then '' else p_estante end);
    v_columna := (case when nvl(p_nro_caja, 0) > 0 then '' else p_columna end);
    v_fuera_expurgo := p_fuera_expurgo;
    v_nro_expurgo := (case when p_nro_expurgo = 0 then null else v_nro_expurgo end);

    --Si vienen en 0 los paso a NULL para respetar las claves foraneas
    v_id_paquete := (case when v_id_paquete = 0 then null else v_id_paquete end);
    v_id_proveedor := (case when p_id_proveedor = 0 then null else p_id_proveedor end);
    v_id_tramite_suac := (case when p_id_tramite_suac = 0 then null else p_id_tramite_suac end);
    v_id_tramite_ipj := (case when p_id_tramite_ipj = 0 then null else p_id_tramite_ipj end);

    -- Calculo el estado de archivo: 1 Archivado / 2 En Proveedor / 3 Exp. Pendiente / 4 Expurgado / 5 Fuera Expurgo
    v_id_estado_archivo := (
      case
        when p_fuera_expurgo = 'S' then 5
        when p_Asienta_Expurgo = 'S' and v_nro_expurgo is not null then 4
        when nvl(p_nro_caja, 0) = 0 and nvl(p_id_proveedor, 0) = 0 then 1
        when nvl(p_nro_caja, 0) <> 0 and nvl(p_id_proveedor, 0) <> 0 then 2
        else 0
      end);

    --********************************************************
    -- REALIZA EL GUARDADO DE LOS DATOS DE ARCHIVO
    --********************************************************
    -- Busco el responsable del area de Archivo
    select Cuil_Usuario into v_cuil_responsable
    from ipj.t_ubicaciones
    where
      id_ubicacion = IPJ.TYPES.C_AREA_ARCHIVO;

    -- Busco la clasificacion de Archivo, para pasar el tr�mite al area
    select id_clasif_ipj into v_id_clasif_archivo
    from ipj.t_tipos_clasif_ipj
    where
      id_ubicacion = IPJ.TYPES.C_AREA_ARCHIVO;

    --********************************************************
    -- Si el expediente no existe en Gesti�n y el de SUAC, lo creo
    --********************************************************
    if nvl(v_Row_Tramite.id_tramite_ipj, 0) = 0 and p_id_tramite_SUAC > 0 then
      -- Guardo un nuevo tr�mite
      IPJ.TRAMITES.SP_GUARDAR_TRAMITE(
        o_Id_Tramite_Ipj => v_Row_Tramite.id_tramite_ipj,
        o_rdo => o_rdo,
        o_tipo_mensaje => o_tipo_mensaje,
        v_Id_Tramite_Ipj => 0,
        v_id_tramite => p_id_tramite_SUAC,
        v_id_Clasif_IPJ => v_id_clasif_archivo, -- Clasificacion de Archivo
        v_id_Grupo => null,
        v_cuil_ult_estado => v_cuil_responsable,
        v_id_estado => IPJ.TYPES.C_ESTADOS_CERRADO,
        v_cuil_creador => v_cuil_responsable,
        v_observacion => 'Creado por Archivo',
        v_id_Ubicacion => IPJ.TYPES.C_AREA_ARCHIVO,
        v_urgente => 'N',
        p_sticker => substr(v_Tipo_SUAC.v_nro_sticker_completo, 1, 9) || substr(v_Tipo_SUAC.v_nro_sticker_completo, 10, 12),
        p_expediente => v_Tipo_SUAC.v_nro_tramite,
        p_simple_tramite => 'S');

      -- Seteo la ubicacion de origen, en el �rea del tr�mite
      -- Seteo el tramite de la siguiente manera: Inactivos (210), Perimidas (201), Rechazados (204), Otros (110)
      update ipj.t_tramitesipj
      set 
        id_ubicacion_origen = p_id_ubicacion,
        id_estado_ult = decode (p_id_tipo_archivo_tramite, 6, 210, 10, 201, 11, 204, IPJ.TYPES.C_ESTADOS_CERRADO)
      where
        id_tramite_ipj = v_Row_Tramite.id_tramite_ipj;

      DBMS_OUTPUT.PUT_LINE(' Crear nuevo tr�mite en Gesti�n (IPJ.TRAMITES.SP_GUARDAR_TRAMITE): ' || o_rdo);
      if o_rdo <> IPJ.TYPES.C_RESP_OK then
        o_rdo := 'Guardar Archivo - Crear tr�mite en Gesti�n: ' || o_rdo;
        o_id_archivo_tramite := nvl(p_id_archivo_tramite, 0);
        return;
      end if;

      -- Agrego la empresa al tr�mite
      if nvl(p_id_legajo, 0) <> 0 then
        DBMS_OUTPUT.PUT_LINE('    - Cargo la Persona Juridica ');
        DBMS_OUTPUT.PUT_LINE('          - id tramite ipj = ' || to_char(v_id_tramite_ipj));
        DBMS_OUTPUT.PUT_LINE('          - id legajo = ' || to_char(p_id_legajo));
        IPJ.TRAMITES.SP_Guardar_TramiteIPJ_PersJur (
          o_rdo => o_rdo,
          o_tipo_mensaje => o_tipo_mensaje,
          p_Id_Tramite_Ipj => v_Row_Tramite.id_tramite_ipj,
          p_id_legajo => p_id_legajo,
          p_id_sede => '00',
          p_error_dato => 'N',
          p_eliminar => 0);

        DBMS_OUTPUT.PUT_LINE('          - o_rdo = ' || o_rdo);
      end if;
      -- Agrego la persona f�sica
      if nvl(p_id_integrante, 0) <> 0 then
        DBMS_OUTPUT.PUT_LINE('    - Cargo la Persona F�sica ');
        DBMS_OUTPUT.PUT_LINE('          - id tramite ipj = ' || to_char(v_id_tramite_ipj));
        DBMS_OUTPUT.PUT_LINE('          - id integrante = ' || to_char(p_id_integrante));

        IPJ.TRAMITES.SP_Guardar_TramiteIPJ_Int (
          o_rdo => o_rdo,
          o_tipo_mensaje => o_tipo_mensaje,
          p_Id_Tramite_Ipj => v_Row_Tramite.id_tramite_ipj,
          p_id_integrante => p_id_integrante,
          p_error_dato => 'N',
          p_eliminar => 0,
          p_id_sexo => null,
          p_nro_documento => null,
          p_pai_cod_pais => null,
          p_id_numero => null,
          p_cuil => null,
          p_detalle => null,
          p_nombre => null,
          p_apellido => null,
          p_n_tipo_documento => null
        );

        DBMS_OUTPUT.PUT_LINE('          - o_rdo = ' || o_rdo);
      end if;
    end if;

    -- Actualizo el tr�mite
    if p_id_archivo_tramite <> 0 then
      update IPJ.T_ARCHIVO_TRAMITE
      set
        id_accion_archivo = p_id_tipo_archivo_tramite,
        nro_tabla = p_nro_tabla,
        nro_caja = v_nro_caja,
        id_paquete = v_id_paquete,
        id_proveedor = v_id_proveedor,
        nro_expurgo = v_nro_expurgo,
        fuera_expurgo = v_fuera_expurgo,
        estanteria = v_estanteria,
        estante = v_estante,
        columna = v_columna,
        observacion = p_observacion,
        anio = v_anio,
        expediente  = nvl(v_Row_Tramite.expediente, p_expediente),
        id_tramite_ipj = v_Row_Tramite.id_tramite_ipj,
        id_legajo = (case when p_id_legajo = 0 then null else p_id_legajo end),
        id_tramite = p_id_tramite_SUAC,
        id_estado_archivo = (case when v_id_estado_archivo = 0 then id_estado_archivo else v_id_estado_archivo end),
        id_integrante = (case when p_id_integrante = 0 then null else p_id_integrante end),
        digitalizado = p_digitalizado
      where
        id_archivo_tramite = p_id_archivo_tramite;

    else
      -- Si no estaba archivado, veo si viene con los datos de SUAC y Gestion, y creo las entradas necesarias
      -- INSERTO EL NUEVO REGISTRO DE ARCHIVO
      SELECT IPJ.SEQ_ARCHIVO.nextval INTO v_id_archivo_tramite FROM dual;

      insert into ipj.t_archivo_tramite (id_archivo_tramite, id_tramite_ipj, id_accion_archivo,
        nro_tabla, nro_caja, id_proveedor, nro_expurgo, fuera_expurgo, estanteria, estante,
        columna, observacion, fec_archivo, expediente, id_tramiteipj_accion, id_paquete, anio,
        id_estado_archivo, id_ubicacion, id_legajo, id_tramite, id_integrante, digitalizado)
      values (v_id_archivo_tramite, v_Row_Tramite.id_tramite_ipj, p_id_tipo_archivo_tramite, v_nro_tabla, v_nro_caja,
        v_id_proveedor, v_nro_expurgo, v_fuera_expurgo, v_estanteria, v_estante, v_columna, p_observacion,
        sysdate, p_expediente, P_Id_Tramiteipj_Accion, v_id_paquete, v_anio,
        IPJ.TYPES.C_ESTADOS_ARCHIVO_INICIAL, p_id_ubicacion,
        (case when p_id_legajo = 0 then null else p_id_legajo end), p_id_tramite_SUAC,
        (case when p_id_integrante = 0 then null else p_id_integrante end), p_digitalizado);

    end if;

    /*********************************************************
      UNA VEZ REGISTRADO LOS DATOS, REVISO:
      - Si en Gesti�n no esta finalizado en Archivo, lo finalizo (estado 110)
      - Si en SUAC no esta ARCHIVADO, tomo el pase pendiente si existe y lo archivo.
    ********************************************************/
    -- Busco el tr�mite en Archivo, para ver sus relaciones
    select * into v_Row_Arch_Tramite
    from IPJ.T_ARCHIVO_TRAMITE
    where
      id_archivo_tramite = nvl(v_id_archivo_tramite, p_id_archivo_tramite);

    --  Si tiene relaci�n a tr�mite en Gesti�n, lo valido para cerrar
    if nvl(v_Row_Arch_Tramite.id_tramite_ipj, 0) <> 0 then
      select * into v_Row_Tramite
      from ipj.t_tramitesipj
      where
        id_tramite_ipj = v_Row_Arch_Tramite.id_tramite_ipj;

     -- Si no esta cerrado, o caulquier estado mayor a 200 (rechazados) se cierra
      if v_Row_Tramite.id_estado_ult < 110 then
        -- Marco la acci�n de archivo como cerrada
        update ipj.t_tramitesipj_acciones
        set id_estado = IPJ.TYPES.C_ESTADOS_CERRADO
        where
          id_tipo_accion = IPJ.TYPES.C_TIPO_ACC_ARCHIVAR_EXPEDIENTE;

        -- Cierro el tr�mite
        IPJ.TRAMITES.SP_GUARDAR_TRAMITE(
          o_Id_Tramite_Ipj => v_Row_Tramite.id_tramite_ipj,
          o_rdo => o_rdo,
          o_tipo_mensaje => o_tipo_mensaje,
          v_Id_Tramite_Ipj => v_Row_Tramite.id_tramite_ipj,
          v_id_tramite => v_Row_Tramite.id_tramite,
          v_id_Clasif_IPJ => 9, -- Clasificacion de Archivo
          v_id_Grupo => v_Row_Tramite.id_grupo_ult_estado,
          v_cuil_ult_estado => v_Row_Tramite.cuil_ult_estado,
          v_id_estado => IPJ.TYPES.C_ESTADOS_CERRADO,
          v_cuil_creador => v_row_tramite.cuil_creador,
          v_observacion => null,
          v_id_Ubicacion => IPJ.TYPES.C_AREA_ARCHIVO,
          v_urgente => v_Row_Tramite.urgente,
          p_sticker => v_Row_Tramite.sticker,
          p_expediente => v_Row_Tramite.expediente,
          p_simple_tramite => 'S'
        );

        DBMS_OUTPUT.PUT_LINE(' Cerrar tr�mite en Gesti�n (IPJ.TRAMITES.SP_GUARDAR_TRAMITE): ' || o_rdo);
        if o_rdo <> IPJ.TYPES.C_RESP_OK then
          o_rdo := 'Guardar Archivo - Cerrar tr�mite en Gesti�n: ' || o_rdo;
          o_id_archivo_tramite := nvl(p_id_archivo_tramite, 0);
          rollback;
          return;
        end if;
      else
      -- Si esta en estado RECHAZADO, lo muevo a Archivo
        if v_Row_Tramite.id_estado_ult in (200, 203, 204) then
          update ipj.t_tramitesipj
          set
            id_ubicacion = IPJ.TYPES.C_AREA_ARCHIVO,
            id_clasif_ipj = 9, -- Clasificacion de Archivo
            cuil_ult_estado = p_cuil_usuario
          where
            id_tramite_ipj = v_Row_Tramite.id_tramite_ipj ;
        end if;
      end if;
    end if;

    --  Si tiene relaci�n a SUAC, lo valido para Archivar
    if nvl(v_Row_Arch_Tramite.id_tramite, 0) <> 0 then
       IPJ.TRAMITES_SUAC.SP_Buscar_Suac (
        o_rdo => v_rdo_SUAC,
        o_tipo_mensaje => v_tipo_mensaje_SUAC,
        o_SUAC => v_Tipo_SUAC,
        p_id_tramite => v_Row_Arch_Tramite.id_tramite,
        p_loguear => 'N'
      );

      if v_Tipo_SUAC.v_estado = 'A RECIBIR' then
        select U.CODIGO_SUAC into v_area_SUAC
        from ipj.t_ubicaciones u
        where
          id_ubicacion = IPJ.TYPES.C_AREA_ARCHIVO;

        IPJ.TRAMITES_SUAC.SP_Aceptar_Pase(
          p_nro_sticker_completo => v_Tipo_SUAC.v_nro_sticker_completo,
          p_cod_unidad_receptora => v_area_SUAC,
          p_usuario_receptor => p_cuil_usuario,
          p_cuerpos => v_Tipo_SUAC.v_cuerpos,
          p_folios => v_Tipo_SUAC.v_fojas,
          o_rdo => o_rdo,
          o_tipo_mensaje => o_tipo_mensaje
        );

        DBMS_OUTPUT.PUT_LINE('       Tomar pase de SUAC: '  || o_rdo);
        -- Si no se pudo aceptar el pase, lo informo
        if o_rdo not like IPJ.TYPES.C_RESP_OK || '%' then
          o_rdo :=  'Guardar Archivo - Tomar pase en SUAC:' ||  o_rdo ;
          o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
          o_id_archivo_tramite := nvl(p_id_archivo_tramite, 0);
          rollback;
          return;
        end if;

        -- Si pudo tomar el pase, lo pone en estado A ENVIAR para ingresar al ciclo de ARCHIVADO
        v_Tipo_SUAC.v_estado := 'A ENVIAR';
      end if;

      if v_Tipo_SUAC.v_estado <> 'ARCHIVADO' then
        -- Busco el c�digo SUAC asociado a Archivo
        select codigo_suac into v_codigo_archivo_suac
        from ipj.t_ubicaciones
        where
          id_ubicacion = IPJ.TYPES.C_AREA_ARCHIVO;

        IPJ.TRAMITES_SUAC.SP_Cerrar_Tramite(
          p_id_tramite_suac => v_Tipo_SUAC.v_id,
          p_cod_unidad => v_codigo_archivo_suac,
          p_usuario => p_cuil_usuario,
          o_rdo => o_rdo,
          o_tipo_mensaje => o_tipo_mensaje
        );
      end if;

      if o_rdo not like IPJ.TYPES.C_RESP_OK || '%' then
          o_rdo :=  'Guardar Archivo - Archivar en SUAC:' ||  o_rdo ;
          o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
          o_id_archivo_tramite := nvl(p_id_archivo_tramite, 0);
          rollback;
          return;
        end if;
    end if;

    if nvl(o_rdo, ipj.TYPES.C_RESP_OK) <> ipj.TYPES.C_RESP_OK then
      o_id_archivo_tramite := nvl(p_id_archivo_tramite, 0);
    else
      o_id_archivo_tramite := nvl(v_id_archivo_tramite, p_id_archivo_tramite);
      o_rdo := ipj.TYPES.C_RESP_OK;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    end if;

  EXCEPTION
    WHEN OTHERS THEN
      o_rdo := 'SP_Guardar_Archivo - ' || To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      o_id_archivo_tramite := nvl(p_id_archivo_tramite, 0);
  END SP_Guardar_Archivo;

  PROCEDURE SP_Guardar_Archivo_Mov(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Archivo_Tramite in number,
    p_Id_Proveedor in number,
    p_Fecha in varchar2,
    p_Id_Mov_Archivo in number,
    p_Id_Tramite_Ipj_Solicitud in number)
  IS
    v_valid_parametros varchar2(2000);
    v_id_proveedor number(6);
    v_existe number;
  BEGIN
    -- La fecha es obligatoria.
    if IPJ.VARIOS.Valida_Fecha(p_Fecha) = 'N'  then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT');
    end if;

    if v_valid_parametros is not null then
      o_rdo := 'ARCHIVO MOVIMIENTO - Parametros : ' || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      return;
    end if;

    -- Busco si tiene id_proveedor
    if p_id_proveedor = 0 then
      select nvl(max(id_proveedor), 0) into v_id_proveedor
      from IPJ.T_Archivo_Tramite
      where
        id_Archivo_tramite = p_id_archivo_tramite;
    else
      v_id_proveedor := p_id_proveedor;
    end if;

    -- Si no tiene asignado PROVEEDOR, se advierte porque no es un movimiento v�lido
    if v_valid_parametros is not null then
      o_rdo := 'ARCHIVO MOVIMIENTO - Proveedor Inv�lido';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING ;
      return;
    end if;

    -- Busco el proximo Id_Mov_Archivo
    select count(*) into v_existe
    from IPJ.T_ARCHIVO_MOVIMIENTOS
    where
      id_archivo_tramite = p_id_archivo_tramite and
      id_proveedor = p_id_proveedor and
      fecha = to_Date(p_fecha, 'dd/mm/rrrr') and
      id_mov_archivo = p_id_mov_archivo;

    -- Si no fallan las validaciones y no existe lo agrego
    if v_existe = 0 then
      insert into IPJ.T_ARCHIVO_MOVIMIENTOS (ID_ARCHIVO_TRAMITE, ID_PROVEEDOR, FECHA,
        ID_MOV_ARCHIVO, ID_TRAMITE_IPJ_SOLICITUD)
      values (p_Id_Archivo_Tramite, v_id_proveedor, to_date(p_Fecha, 'dd/mm/rrrr'),
        p_Id_Mov_Archivo, p_Id_Tramite_Ipj_Solicitud);
     end if;

    o_rdo := TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
    when OTHERS then
      o_rdo := 'ARCHIVO MOVIMIENTO: ' || To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Guardar_Archivo_Mov;

  PROCEDURE SP_Traer_Archivo_Proveedores(
    o_Cursor OUT types.cursorType)
  IS
  BEGIN

  -- Cuerpo del SP
    OPEN o_Cursor FOR
      select
         id_proveedor, n_proveedor
      from IPJ.T_ARCHIVO_PROVEEDORES
      order by n_proveedor desc;

  END SP_Traer_Archivo_Proveedores;

  PROCEDURE SP_Traer_Archivo_Origen(
    o_Cursor OUT types.cursorType)
  IS
  BEGIN

  -- Cuerpo del SP
    OPEN o_Cursor FOR
      select distinct id_ubicacion, n_ubicacion,  OBSERVACION, ORDEN
      from ipj.t_workflow w join ipj.t_ubicaciones u
        on w.id_ubicacion_origen = u.id_ubicacion
      where
        id_clasif_ipj_proxima = 9 and
        id_clasif_ipj_actual <> 9
      order by n_ubicacion;

  END SP_Traer_Archivo_Origen;

  PROCEDURE SP_Traer_Archivo_Acciones(
    o_Cursor OUT types.cursorType)
  IS
  BEGIN

  -- Cuerpo del SP
    OPEN o_Cursor FOR
      select id_accion_archivo, n_accion_archivo
      from IPJ.T_ACCIONES_ARCHIVO
      order by n_accion_archivo;

  END SP_Traer_Archivo_Acciones;

  PROCEDURE SP_Traer_Archivo_Expurgo(
    o_Cursor OUT types.cursorType)
  IS
  BEGIN

  -- Cuerpo del SP
    OPEN o_Cursor FOR
      select nro_expurgo, n_expurgo, fecha
      from IPJ.T_EXPURGO
      order by nro_expurgo desc;

  END SP_Traer_Archivo_Expurgo;

  PROCEDURE SP_Traer_Tipo_Movimiento(
    o_Cursor OUT types.cursorType)
  IS
  BEGIN

  -- Cuerpo del SP
    OPEN o_Cursor FOR
      select ID_MOV_ARCHIVO, N_MOV_ARCHIVO
      from IPJ.T_TIPOS_MOV_ARCHIVO
      order by N_MOV_ARCHIVO desc;

  END SP_Traer_Tipo_Movimiento;

  PROCEDURE SP_Buscar_Archivo(
    o_Cursor OUT types.cursorType,
    p_id_ubicacion in number,
    p_cuit in varchar2,
    p_razon_social in varchar2,
    p_nro_registro in varchar2,
    p_ficha in varchar2,
    p_matricula in varchar2,
    p_expediente in varchar2,
    p_Es_Reapertura in char)
  IS
  /*
    Realiza una busqueda sobre los tramites archivados, para solicitar prestamos
  */
  BEGIN

    OPEN o_Cursor FOR
      select arch.id_archivo_tramite,
         arch.id_estado_archivo, arch.id_tramite_ipj,
         arch.id_accion_archivo, arch.nro_tabla, arch.nro_caja,
         arch.id_paquete, arch.anio, arch.id_proveedor, arch.nro_expurgo,
         arch.fuera_expurgo, arch.estanteria, arch.estante, arch.columna,
         arch.observacion, to_char(arch.fec_archivo, 'dd/mm/rrrr') fec_archivo, arch.expediente, 
         arch.id_tramiteipj_accion, arch.id_ubicacion, arch.id_legajo,
         l.denominacion_sia, L.registro nro_registro, l.nro_ficha,
         aarch.n_accion_archivo,
         (case
            when nvl(arch.digitalizado,0) = 1 then 'Digitalizado'--Bug13647:[SG] - [Pr�stamo/Vista] - Al buscar un archivo que ha sido digitalizado
            when nvl(arch.digitalizado,0) = 0 and (select count(1) from ipj.t_archivo_prestamos a where a.id_Archivo_tramite= arch.id_Archivo_tramite and id_estado_prestamo <> 5) > 0 then 'Prestado'
            when nvl(arch.digitalizado,0) = 0 and (select count(1) from ipj.t_archivo_desarchivos a where a.id_Archivo_tramite= arch.id_Archivo_tramite and a.id_estado_desarchivo < 3) > 0 then 'Desarchivo'
            when nvl(arch.digitalizado,0) = 0 and (select count(1) from ipj.t_tramitesipj a where a.id_tramite_ipj = arch.id_tramite_ipj and a.id_ubicacion <> IPJ.TYPES.C_AREA_ARCHIVO ) > 0 then 'En Gesti�n'
          else
            Null
          end) Estado, --(PBI 11286)
          NULL asunto_suac, NULL sticker, NULL fecha_ini_suac,
          null id_tramite_suac, null id_ubicacion_origen,
          'Sin SUAC' Estado_Reapertura, 0 Reabrir
      from IPJ.T_ARCHIVO_TRAMITE arch left join ipj.t_legajos l
          on l.id_legajo = arch.id_legajo
        left join IPJ.T_ACCIONES_ARCHIVO aarch
          on aarch.id_accion_archivo = arch.id_accion_archivo
      where
        nvl(ARCH.ID_TRAMITE_IPJ, 0) = 0 and-- que no esten en Gestion
        (p_cuit is null or l.cuit = p_cuit) and
        (p_razon_social is null or trim(upper(l.denominacion_sia)) like '%' || trim(upper(p_razon_social)) || '%') and
        (p_nro_registro is null or
          (p_id_ubicacion = IPJ.TYPES.C_AREA_SXA and L.REGISTRO = p_nro_registro)
          or (p_id_ubicacion = IPJ.TYPES.C_AREA_CYF and L.NRO_FICHA = p_nro_registro)
        ) and
        (p_matricula is null or IPJ.VARIOS.FC_FORMATEAR_MATRICULA(L.Nro_Ficha) = IPJ.VARIOS.FC_FORMATEAR_MATRICULA(p_matricula)) and
        (p_expediente is null or ARCH.EXPEDIENTE like '%' || p_expediente || '%') and
        (nvl(p_Es_Reapertura, 'N') = 'N' or p_id_ubicacion =  arch.id_ubicacion) -- Es para Archivo o Reapertura de la misma area
        
      UNION ALL
      
      select arch.id_archivo_tramite,
         arch.id_estado_archivo, tr.id_tramite_ipj,
         arch.id_accion_archivo, arch.nro_tabla, arch.nro_caja,
         arch.id_paquete, arch.anio, arch.id_proveedor, arch.nro_expurgo,
         arch.fuera_expurgo, arch.estanteria, arch.estante, arch.columna,
         arch.observacion, to_char(arch.fec_archivo, 'dd/mm/rrrr') fec_archivo, 
         nvl(arch.expediente,tr.expediente), arch.id_tramiteipj_accion,
         arch.id_ubicacion, nvl(arch.id_legajo, trp.id_legajo) id_legajo,
         l.denominacion_sia, L.registro nro_registro, l.nro_ficha,
         aarch.n_accion_archivo,
         (case
            when nvl(arch.digitalizado,0) = 1 then 'Digitalizado'--Bug13647:[SG] - [Pr�stamo/Vista] - Al buscar un archivo que ha sido digitalizado
            when nvl(arch.digitalizado,0) = 0 and (select count(1) from ipj.t_archivo_prestamos a where a.id_Archivo_tramite= arch.id_Archivo_tramite and id_estado_prestamo <> 5) > 0 
                then 'Prestado'
            when nvl(arch.digitalizado,0) = 0 and (select count(1) from ipj.t_archivo_desarchivos a where a.id_Archivo_tramite= arch.id_Archivo_tramite and a.id_estado_desarchivo < 3) > 0 
                then 'Desarchivo'
            when nvl(arch.digitalizado,0) = 0 and (IPJ.TRAMITES.FC_ES_TRAM_DIGITAL(tr.id_tramite_ipj) = 0 and tr.id_ubicacion <> IPJ.TYPES.C_AREA_ARCHIVO ) 
                then 'En Gesti�n'
            when nvl(arch.digitalizado,0) = 0 and IPJ.TRAMITES.FC_ES_TRAM_DIGITAL(tr.id_tramite_ipj) > 0  
                then 'Digital'
          else
            Null
          end) Estado,
          ipj.tramites_suac.FC_Buscar_Asunto_SUAC(tr.id_tramite) asunto_suac, tr.sticker, 
          to_char(tr.fecha_ini_suac, 'dd/mm/rrrr') fecha_ini_suac,
          tr.id_tramite id_tramite_suac, tr.id_ubicacion_origen,
          (case
              when IPJ.TRAMITES.FC_ES_TRAM_DIGITAL(tr.id_tramite_ipj) = 0 and tr.id_estado_ult between 100 and 199 then 'Papel - Cerrado'
              when IPJ.TRAMITES.FC_ES_TRAM_DIGITAL(tr.id_tramite_ipj) > 0 and tr.id_estado_ult between 100 and 199 then 'Digital - Cerrado'
              when tr.id_estado_ult < 100 and tr.id_estado_ult <> 50  then 'En Estudio'
              when tr.id_estado_ult = 50  then 'Anexado'
              when tr.id_estado_ult >= 200 and tr.id_estado_ult <> 210 then 'Rechazado' 
              when tr.id_estado_ult = 210 then 'Inactivo'
            end) Estado_Reapertura,
          (case
              when IPJ.TRAMITES.FC_ES_TRAM_DIGITAL(tr.id_tramite_ipj) = 0 and tr.id_estado_ult between 100 and 199 then 1
              else 0
          end) Reabrir
      from ipj.t_tramitesipj tr left join ipj.t_tramitesipj_persjur trp
          on tr.id_tramite_ipj = trp.id_tramite_ipj
        left join IPJ.T_ARCHIVO_TRAMITE arch
          on arch.id_tramite_ipj = tr.id_tramite_ipj
        left join ipj.t_legajos l
          on l.id_legajo = nvl(arch.id_legajo, trp.id_legajo)
        left join IPJ.T_ACCIONES_ARCHIVO aarch
          on aarch.id_accion_archivo = arch.id_accion_archivo
      where
        (p_cuit is null or l.cuit = p_cuit) and
        (p_razon_social is null or trim(upper(l.denominacion_sia)) like '%' || trim(upper(p_razon_social)) || '%') and
        (p_nro_registro is null or
          (p_id_ubicacion = IPJ.TYPES.C_AREA_SXA and L.REGISTRO = p_nro_registro)
          or (p_id_ubicacion = IPJ.TYPES.C_AREA_CYF and L.NRO_FICHA = p_nro_registro)
        ) and
        (p_matricula is null or IPJ.VARIOS.FC_FORMATEAR_MATRICULA(L.Nro_Ficha) = IPJ.VARIOS.FC_FORMATEAR_MATRICULA(p_matricula)) and
        (p_expediente is null or tr.EXPEDIENTE like '%' || p_expediente || '%') and
        (nvl(p_Es_Reapertura, 'N') = 'N' or p_id_ubicacion =  tr.id_ubicacion_origen) -- Es para Archivo o Reapertura de la misma area
      ;

  END SP_Buscar_Archivo;

  PROCEDURE SP_Buscar_Archivo_CP(
    o_Cursor OUT types.cursorType,
    o_total_pages out number,
    p_id_ubicacion in number,
    p_id_accion_archivo in number,
    p_expediente in varchar2,
    p_anio in number,
    p_nro_registro in varchar2,
    p_razon_social in varchar2,
    p_id_legajo in number,
    p_n_paquete in varchar2,
    p_nro_paquete in number,
    p_nro_caja in number,
    p_id_proveedor in number,
    p_nro_expurgo in number,
    p_page_number in number,
    p_page_size in number)
  IS
  /* Este procedimiento trae los datos registrados de Archivo de manera paginada
      por la cantidad de registros existentes, para que no sea muy lento el lado del cliente.
      La aplicaci�n indica el seteo del paginado, y el procedimiento busca la indicada.
  */
  BEGIN
     if IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('HAB_LOG_ARCHIVO') = '1' then
      IPJ.VARIOS.SP_GUARDAR_LOG(
        p_METODO => 'SP_Buscar_Archivo_CP',
        p_NIVEL => 'Gestion',
        p_ORIGEN => 'DB',
        p_USR_LOG  => 'DB',
        p_USR_WINDOWS => 'Sistema',
        p_IP_PC  => null,
        p_EXCEPCION =>
          'Id Ubicacion = ' || to_char(p_id_ubicacion)
          || ' / Id Accion Archivo = ' ||  to_char(p_id_accion_archivo)
          || ' / Expediente = ' || p_expediente
          || ' / A�o = ' || to_char(p_anio)
          || ' / Nro Registro = ' || p_nro_registro
          || ' / Razon Social = ' || p_razon_social
          || ' / Id Legajo = ' || to_char(p_id_legajo)
          || ' / Nom Paquete = ' || p_n_paquete
          || ' / Nro Paquete = ' || to_char(p_nro_paquete)
          || ' / Nro Caja = ' || to_char(p_nro_caja)
          || ' / Id Proveedor = ' || to_char(p_id_proveedor)
          || ' / Nro Expurgo = ' || to_char(p_nro_expurgo)
          || ' / Page Number = ' || to_char(p_page_number)
          || ' / Page Size = ' || to_char(p_page_size)
      );
    end if;

    -- Cuento la cantidad de p�ginas del resultado buscado.
    select (count(arch.id_archivo_tramite) / p_page_size) into o_total_pages
    from IPJ.T_ARCHIVO_TRAMITE arch join IPJ.t_ubicaciones u
        on U.ID_UBICACION = arch.ID_UBICACION
      left join IPJ.T_ACCIONES_ARCHIVO aa
        on AA.ID_ACCION_ARCHIVO = ARCH.ID_ACCION_ARCHIVO
      left join IPJ.T_TIPOS_ESTADO_ARCHIVO ea
        on ea.id_estado_Archivo = arch.id_estado_archivo
      left join IPJ.T_TRAMITESIPJ tr
        on arch.id_tramite_ipj = tr.id_tramite_ipj
      left join ipj.t_legajos l
        on l.id_legajo = arch.id_legajo
      left join IPJ.T_PAQUETES_ARCHIVO pq
        on PQ.ID_PAQUETE = ARCH.ID_PAQUETE
      left join ipj.t_expurgo ex
        on arch.nro_expurgo = ex.nro_expurgo
    where
      (nvl(p_id_ubicacion, 0) = 0 or  arch.id_ubicacion = p_id_ubicacion) and
      (nvl(p_id_accion_archivo, 0) = 0 or arch.id_accion_archivo = p_id_accion_archivo) and
      ( p_expediente is null or upper(arch.expediente) like '%' || upper(p_expediente) || '%') and
      ( nvl(p_anio, 0) = 0 or arch.anio = p_anio) and
      --( nvl(p_anio, 0) = 0 or pq.anio = p_anio) and
      (p_nro_registro is null or upper(L.NRO_FICHA) = upper(p_nro_registro)) and
      (p_razon_social is null or IPJ.VARIOS.FC_QUITAR_ACENTOS(l.denominacion_sia) like '%' || IPJ.VARIOS.FC_QUITAR_ACENTOS(p_razon_social) || '%') and
      (nvl(p_id_legajo, 0) = 0 or l.id_legajo = p_id_legajo) and
      (p_n_paquete is null or trim(upper(pq.n_paquete)) like '%' || trim(upper(p_n_paquete)) || '%') and
      (nvl(p_nro_paquete, 0) = 0 or pq.nro_paquete = p_nro_paquete) and
      (nvl(p_nro_caja, 0) = 0 or arch.nro_caja = p_nro_caja) and
      (nvl(p_id_proveedor, 0) = 0 or arch.id_proveedor = p_id_proveedor) and
      (nvl(p_nro_expurgo, 0) = 0 or arch.nro_expurgo = p_nro_expurgo);

    o_total_pages :=  ceil(o_total_pages);


    -- Delvuelve los registros de la pagina indicada, segun su filtro
    OPEN o_Cursor FOR
      select
        Id_Estado_Archivo, Id_Archivo_Tramite, Id_Tramite_Ipj, Id_Accion_Archivo, Nro_Tabla,
        Nro_Caja, Id_Paquete, Anio, Id_Proveedor, Nro_Expurgo, Fuera_Expurgo, Estanteria,
        Estante, Columna, Observacion, Fec_Archivo, Expediente, Id_Tramiteipj_Accion,
        Id_Ubicacion, Id_Legajo, N_Ubicacion, N_Accion_Archivo, Denominacion_Sia,
        Nro_Registro, N_Paquete, Nro_Paquete, N_Expurgo, Asienta_Expurgo,
        n_estado_archivo, nvl(Id_Ubicacion_Tramite_Ipj, IPJ.TYPES.C_AREA_ARCHIVO) Id_Ubicacion_Tramite_Ipj,
        (select n_ubicacion from ipj.t_ubicaciones ub where ub.id_ubicacion = nvl(Id_Ubicacion_Tramite_Ipj, IPJ.TYPES.C_AREA_ARCHIVO)) N_Ubicacion_Tramite_Ipj,
        Id_Ubicacion_Origen, Cuil_Creador,
        id_tramite_suac, id_integrante, detalle, n_proveedor,
        nvl(nvl(Denominacion_Sia, detalle), IPJ.TRAMITES_SUAC.FC_BUSCAR_INICIADOR_SUAC(id_tramite_suac)) Iniciador,
        digitalizado,
        decode(Fec_Archivo, null, null, '01/01/' || to_char(EXTRACT(YEAR FROM to_date(Fec_Archivo, 'dd/mm/rrrr'))+1)) Ini_Expurgo,
        (select siglas from ipj.t_ubicaciones ub where ub.id_ubicacion = tmp.id_ubicacion) Siglas
      FROM
        ( select
            arch.id_estado_archivo, arch.id_archivo_tramite, arch.ID_TRAMITE_IPJ,
            arch.ID_ACCION_ARCHIVO, arch.NRO_TABLA, arch.NRO_CAJA,
            arch.ID_paquete, arch.anio, arch.ID_PROVEEDOR, arch.NRO_EXPURGO,
            arch.FUERA_EXPURGO, arch.ESTANTERIA, arch.ESTANTE, arch.COLUMNA,
            arch.OBSERVACION,
            to_char((case when arch.FEC_ARCHIVO is null then (select min(fec_cambio) from ipj.t_archivo_tramite_hist h where h.id_archivo_tramite = arch.id_archivo_tramite)
                           else arch.FEC_ARCHIVO
                         end)
               , 'dd/mm/rrrr') FEC_ARCHIVO,
            arch.EXPEDIENTE, arch.ID_TRAMITEIPJ_ACCION, arch.id_ubicacion, arch.id_legajo,
            U.N_UBICACION, AA.N_ACCION_ARCHIVO, l.denominacion_sia,
            L.NRO_FICHA nro_registro, pq.n_paquete, pq.nro_paquete, ex.n_expurgo,
            (case when EX.FECHA is null then 'N' else 'S' end) Asienta_Expurgo,
            ea.n_estado_archivo, TR.ID_UBICACION Id_Ubicacion_Tramite_Ipj,
            tr.Id_Ubicacion_Origen, tr.Cuil_Creador, arch.id_tramite id_tramite_suac,
            arch.id_integrante,
            (select p.nombre||' '||p.apellido from rcivil.vt_pk_persona p where p.id_sexo = i.id_sexo and p.nro_documento = i.nro_documento and p.pai_cod_pais = i.pai_cod_pais and p.id_numero = i.id_numero) detalle,
            arch.digitalizado,
            (select n_proveedor from ipj.t_archivo_proveedores p where p.id_proveedor = arch.id_proveedor) n_proveedor,
            COUNT(*) OVER () TotalRows, rownum rnum
          from IPJ.T_ARCHIVO_TRAMITE arch join IPJ.t_ubicaciones u
              on U.ID_UBICACION = arch.ID_UBICACION
            left join IPJ.T_ACCIONES_ARCHIVO aa
              on AA.ID_ACCION_ARCHIVO = ARCH.ID_ACCION_ARCHIVO
            left join IPJ.T_TIPOS_ESTADO_ARCHIVO ea
              on ea.id_estado_Archivo = arch.id_estado_archivo
            left join IPJ.T_TRAMITESIPJ tr
              on arch.id_tramite_ipj = tr.id_tramite_ipj
            left join ipj.t_legajos l
              on l.id_legajo = arch.id_legajo
            left join IPJ.T_PAQUETES_ARCHIVO pq
              on PQ.ID_PAQUETE = ARCH.ID_PAQUETE
            left join ipj.t_expurgo ex
              on arch.nro_expurgo = ex.nro_expurgo
            left join ipj.t_integrantes i
              on arch.id_integrante = i.id_integrante
          where
            (nvl(p_id_ubicacion, 0) = 0 or  arch.id_ubicacion = p_id_ubicacion) and
            (nvl(p_id_accion_archivo, 0) = 0 or arch.id_accion_archivo = p_id_accion_archivo) and
            ( p_expediente is null or upper(arch.expediente) like '%' || upper(p_expediente) || '%') and
            ( nvl(p_anio, 0) = 0 or arch.anio = p_anio) and
            --( nvl(p_anio, 0) = 0 or pq.anio = p_anio) and
            (p_nro_registro is null or upper(L.NRO_FICHA) = upper(p_nro_registro)) and
            (p_razon_social is null or IPJ.VARIOS.FC_QUITAR_ACENTOS(l.denominacion_sia) like '%' || IPJ.VARIOS.FC_QUITAR_ACENTOS(p_razon_social) || '%') and
            (nvl(p_id_legajo, 0) = 0 or l.id_legajo = p_id_legajo) and
            (p_n_paquete is null or trim(upper(pq.n_paquete)) like '%' || trim(upper(p_n_paquete)) || '%') and
            (nvl(p_nro_paquete, 0) = 0 or pq.nro_paquete = p_nro_paquete) and
            (nvl(p_nro_caja, 0) = 0 or arch.nro_caja = p_nro_caja) and
            (nvl(p_id_proveedor, 0) = 0 or arch.id_proveedor = p_id_proveedor) and
            (nvl(p_nro_expurgo, 0) = 0 or arch.nro_expurgo = p_nro_expurgo)
        ) tmp
      WHERE
        rnum between ((p_page_number * p_page_size) - p_page_size)+1 and  (p_page_number * p_page_size) ;

  END SP_Buscar_Archivo_CP;

   PROCEDURE SP_Guardar_Archivo_Proveedores(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_proveedor OUT number,
    p_id_proveedor in number,
    p_n_proveedor in varchar2)
  IS
  v_valid_parametros varchar(2000);
  /**************************************************
    Guarda o modifica un Proveedor de Archivo
  ***************************************************/
  BEGIN
    -- Valido que venga Descripcion
    if p_n_proveedor is null then
      v_valid_parametros := v_valid_parametros || 'Nombre Inv�lido';
    end if;

    if v_valid_parametros is not null then
      o_rdo := 'Parametros : ' || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    --Si estan bien los datos, actualizo, si no hay registro lo agrege y se devuelve el valor insertado.
    update IPJ.T_ARCHIVO_PROVEEDORES
    set
      n_proveedor = p_n_proveedor
    where
      id_proveedor = p_id_proveedor;

    if sql%rowcount = 0 then
      select max(id_proveedor) +1 into o_id_proveedor
      from IPJ.T_ARCHIVO_PROVEEDORES;

      insert into IPJ.T_ARCHIVO_PROVEEDORES (id_proveedor, n_proveedor)
      values (o_id_proveedor, p_n_proveedor);

    else
      o_id_proveedor := p_id_proveedor;
    end if;

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     when OTHERS then
        o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Guardar_Archivo_Proveedores;

  PROCEDURE SP_Buscar_Paquete(
    o_Nro_Paquete OUT Number,
    o_Nro_Tabla out number,
    p_anio in number,
    p_n_paquete in varchar2,
    p_id_ubicacion number,
    p_id_accion_archivo number)
  IS
   /*********************************************************
     Busca el proximo Nro. Tabla y Nro. Paquete para el Nombre de paquete y a�o indicado
   *********************************************************/
    v_Row_Paquete  IPJ.T_Paquetes_Archivo%ROWTYPE;
    v_nro_tabla number;
    v_SQL varchar2(4000);
    v_where varchar2(2000);
    v_Max_Tablas number;
  BEGIN
    if IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('HAB_LOG_ARCHIVO') = '1' then
      IPJ.VARIOS.SP_GUARDAR_LOG(
        p_METODO => 'SP_Buscar_Paquete',
        p_NIVEL => 'Gestion - Archivo',
        p_ORIGEN => 'DB',
        p_USR_LOG  => 'DB',
        p_USR_WINDOWS => 'Sistema',
        p_IP_PC  => null,
        p_EXCEPCION =>
          'A�o = ' || to_char(p_anio)
          || ' / N_Paquete = ' || p_n_paquete
          || ' / Ubicacion = ' || to_char(p_id_ubicacion)
          || ' / Id Accion Archivo = ' || to_char(p_id_accion_archivo)
        );
    end if;
    -- Busco el maximo paquete, si no existe NRO_PAQUETE inicia en 1
    begin
      if p_id_accion_archivo = IPJ.TYPES.c_Tipo_Arch_Asambleas then
        select * into v_Row_Paquete
        from
          ( select *
             from IPJ.T_Paquetes_Archivo
             where
               trim(upper(n_paquete)) = trim(upper(p_n_paquete)) and
               anio = p_anio and
               id_ubicacion = p_id_ubicacion and
               id_accion_archivo = p_id_accion_archivo
             order by nro_paquete desc
           )
        where rownum = 1;
      else
        -- (PBI 12896) Sumo Fideicomisos como Const, Reformas y Rechazados
        if p_id_accion_archivo in (IPJ.TYPES.c_Tipo_Arch_Constituciones, IPJ.TYPES.c_Tipo_Arch_Reformas, 9, 11) then
          select * into v_Row_Paquete
          from
            ( select *
               from IPJ.T_Paquetes_Archivo
               where
                 n_paquete is null and
                 anio = p_anio and
                 id_ubicacion = p_id_ubicacion and
                 id_accion_archivo = p_id_accion_archivo
                 order by nro_paquete desc
            )
          where rownum = 1;
        else
          select * into v_Row_Paquete
          from
            ( select *
              from IPJ.T_Paquetes_Archivo
              where
                n_paquete is null and
                anio is null and
                id_ubicacion = p_id_ubicacion and
                id_accion_archivo = p_id_accion_archivo
                order by nvl(nro_paquete, 0) desc
            )
          where rownum = 1;
        end if;
      end if;
    exception when NO_DATA_FOUND then
      v_Row_Paquete.id_paquete := 0;
      v_Row_Paquete.nro_paquete := 1;
    end;

    -- Busco el maximo nro_tabla del paquete indicado, si no existe empiesa en 1
    begin
      select max(nro_tabla) into v_nro_tabla
      from IPJ.t_Archivo_Tramite
      where
        id_paquete = v_Row_Paquete.id_paquete;
    exception
      when NO_DATA_FOUND then
        v_nro_tabla := 0;
    end;

    -- Armo los valores a devolver, cortando las tablas en 25 e incrementando nro_paquete si es necesario
    v_Max_Tablas := to_number(IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('MAX_TABLAS'));
    o_nro_tabla :=  CASE WHEN  nvl(v_nro_tabla, 0) < v_Max_Tablas  THEN nvl(v_nro_tabla, 0) + 1 ELSE MOD( nvl(v_nro_tabla, 0) + 1, v_Max_Tablas ) END;
    o_nro_paquete := (case when nvl(v_nro_tabla, 0) +1 > v_Max_Tablas then v_Row_Paquete.nro_paquete + 1 else v_Row_Paquete.nro_paquete end);

  END SP_Buscar_Paquete;

  PROCEDURE SP_Buscar_Expediente_SUAC(
    o_Cursor OUT types.cursorType,
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_sticker_14 in varchar2,
    p_id_archivo_tramite number)
  IS
    /***********************************************
      Busca un expediente que no existe en Gestion, para agregarlo.
      Se requiere el Sticker de 14 para poder buscarlo en SUAC
    *************************************************/
    v_Exp_SUAC IPJ.TRAMITES_SUAC.Tipo_SUAC;
    v_existe_archivo number;
  BEGIN
     if IPJ.Types.c_habilitar_log_SP = 'S' then
      IPJ.VARIOS.SP_GUARDAR_LOG(
        p_METODO => 'SP_Buscar_Expediente_SUAC',
        p_NIVEL => 'Gestion - Archivo - Buscar Suac',
        p_ORIGEN => 'DB',
        p_USR_LOG  => 'DB',
        p_USR_WINDOWS => 'Sistema',
        p_IP_PC  => null,
        p_EXCEPCION =>
          'Sticker 14 = ' || p_sticker_14
          || ' / id_archivo_tramite = ' || to_char(p_id_archivo_tramite)
      );
    end if;

    -- Busco los datos del expediente SUAC
    IPJ.TRAMITES_SUAC.SP_BUSCAR_STICKER_SUAC(
      o_rdo => o_rdo,
      o_tipo_mensaje => o_tipo_mensaje,
      o_SUAC => v_exp_SUAC,
      p_Sticker_14 => p_Sticker_14);

    -- (BUG  9148) Busco si el expdiente no se encuentra archivado
    select count(1) into v_existe_archivo
    from ipj.t_archivo_tramite
    where
      expediente = v_exp_suac.v_nro_tramite and
      id_archivo_tramite <> p_id_archivo_tramite;

    -- Si el expediente no esta archivado, continua
    if v_existe_archivo = 0 then
      OPEN o_Cursor FOR
        select
          v_exp_SUAC.v_ID Id_Tramite_Suac ,
          IPJ.TRAMITES.FC_Quitar_Ocultos(v_exp_SUAC.v_nro_sticker_completo) Sticker,
          (v_exp_SUAC.v_Tipo || ' - ' || v_exp_SUAC.v_Subtipo) Tipo ,
         v_exp_SUAC.v_asunto Asunto,  v_exp_SUAC.v_nro_tramite nro_expediente,
          to_char(to_date(v_exp_SUAC.v_fecha_inicio, 'dd/mm/rrrr'), 'dd/mm/rrrr') Fecha_Inicio_Suac,
          null Fecha_recepcion, 0 Id_Tramite_Ipj, null id_clasif_ipj, null Observacion,
          null id_estado_ult, null cuil_ult_estado, 'En SUAC' n_estado, null CUIL_CREADOR,
          null Fecha_Inicio, null URGENTE, null simple_tramite,
          (case
             when v_exp_SUAC.v_tipo_iniciador = 'PERSONA FISICA' then v_exp_SUAC.v_iniciador
             when v_exp_SUAC.v_tipo_iniciador <> 'PERSONA FISICA' and nvl(v_exp_SUAC.v_iniciador_Cuit, '0') = '0' then  v_exp_SUAC.v_iniciador
             else v_exp_SUAC.v_iniciador
          end) razon_social_persona_juridica,
          v_exp_SUAC.v_tipo_iniciador Tipo_Iniciador, v_exp_SUAC.v_iniciador_sexo id_sexo,
          v_exp_SUAC.v_iniciador_nro_documento nro_documento, v_exp_SUAC.v_iniciador_pai_cod_pais pai_cod_pais,
          v_exp_SUAC.v_iniciador_id_numero id_numero, v_exp_SUAC.v_iniciador_cuit cuit
        from dual;

      o_rdo := IPJ.TYPES.C_RESP_OK;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    else
      o_rdo := 'El expediente ' || v_exp_suac.v_nro_tramite || ' ya se encuentra en Archivo, no es posible asociarlo nuevamente';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;

      OPEN o_Cursor FOR
        select  null Id_Tramite_Suac , 'Inv�lido'  Asunto
        from dual;
    end if;

  END SP_Buscar_Expediente_SUAC;

  PROCEDURE SP_Traer_Archivo_Prest_Pend(
    o_Cursor OUT types.cursorType,
    p_cuil_usuario in varchar2,
    p_id_ubicacion in number)
  IS
  /*********************************************************
    Agrupa los pendientes de un usuario por estado, dependiendo del area:
    - Archivo = 1 y 4 de Vistas / 1 y 2 de Desarchivo
    - Areas = 2, 3, 4, 7 de Vistas / 3 y 4 de Desarchivo
  *********************************************************/
    v_valid_parametros varchar2(500);
  BEGIN
    -- Cuerpo del SP
    OPEN o_Cursor FOR
      select 'VISTA' TIPO_SOLICITUD, tp.Id_Estado_Prestamo, tp.N_Estado_Prestamo, tp.Mensaje, count(1) Cantidad
      from IPJ.T_ARCHIVO_PRESTAMOS ap join ipj.T_TIPOS_ESTADO_PREST tp
          on ap.Id_Estado_Prestamo = tp.Id_Estado_Prestamo
      where
        (p_id_ubicacion = IPJ.TYPES.C_AREA_ARCHIVO and tp.Id_Estado_Prestamo in (1, 4)) or
        (ap.cuil_solicitante = p_cuil_usuario and tp.Id_Estado_Prestamo in (2, 3, 4, 7))
      group by tp.Id_Estado_Prestamo, tp.N_Estado_Prestamo, tp.Mensaje

      UNION ALL

      select 'DESARCHIVO' TIPO_SOLICITUD, td.id_estado_desarchivo Id_Estado_Prestamo,
        td.n_estado_desarchivo N_Estado_Prestamo, 'Desarchivo ' || td.n_estado_desarchivo Mensaje, count(1) Cantidad
      from IPJ.T_ARCHIVO_DESARCHIVOS ad join IPJ.T_TIPOS_ESTADO_DESARCH td
          on ad.id_estado_desarchivo = td.id_estado_desarchivo
      where
        (p_id_ubicacion = IPJ.TYPES.C_AREA_ARCHIVO and td.ID_ESTADO_DESARCHIVO in (1, 2)) or -- 1 Pendinete / 2 Solic. Proveedor
        (ad.cuil_solicitante = p_cuil_usuario and td.ID_ESTADO_DESARCHIVO in (3, 4)) -- 3 Anulado / 4 Desarchivado
      group by td.ID_ESTADO_DESARCHIVO, td.N_ESTADO_DESARCHIVO;

  END SP_Traer_Archivo_Prest_Pend;

  PROCEDURE SP_Traer_Archivo_Prestamos(
    o_Cursor OUT types.cursorType,
    p_cuil_usuario in varchar2,
    p_id_archivo_tramite in number,
    p_id_estado_prestamo in number)
  IS
  /*********************************************************
    Muestra los prestamos de una persona o un archivo.
    Para el area de archivo, muestra todos los prestamos no archivados.
  *********************************************************/
    v_es_archivo number;
    v_valid_parametros varchar2(500);
  BEGIN
    if IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('HAB_LOG_ARCHIVO') = '1' then
      IPJ.VARIOS.SP_GUARDAR_LOG(
        p_METODO => 'SP_Traer_Archivo_Prestamos',
        p_NIVEL => 'ARCHIVO',
        p_ORIGEN => 'DB',
        p_USR_LOG  => 'DB',
        p_USR_WINDOWS => 'Sistema',
        p_IP_PC  => null,
        p_EXCEPCION =>
          'Cuil Usuario = ' || p_cuil_usuario
         || ' / Id Archivo Tramite = ' || to_char(p_id_archivo_tramite)
      );
    end if;

    /******** CUERPO DEL SP  *******************/
    --Busco si el usuario pertenece a Archivo
    select count(1) into v_es_archivo
    from IPJ.T_GRUPOS_TRAB_UBICACION
    where
      cuil = p_cuil_usuario and
      id_ubicacion = IPJ.TYPES.C_AREA_ARCHIVO;

    OPEN o_Cursor FOR
      select Ap.Cuil_Solicitante, to_char(Ap.Fecha_Devolucion, 'dd/mm/rrrr') Fecha_Devolucion,
        to_char(Ap.Fecha_Entrega, 'dd/mm/rrrr') Fecha_Entrega, to_char(Ap.Fecha_Pedido, 'dd/mm/rrrr') Fecha_Pedido,
        Ap.Id_Archivo_Tramite, Ap.Id_Estado_Prestamo, Ap.Ok_Devolucion, Ap.Ok_Entrega,
        Tp.N_Estado_Prestamo, u.Descripcion, at.expediente, u.descripcion detalle,
        at.Id_Tramite_Ipj, at.Id_Tramiteipj_Accion, ap.cuil_prestador,
        (select u2.Descripcion from ipj.t_usuarios u2 where u2.cuil_usuario = ap.cuil_prestador)  Detalle_Prestador ,
        at.id_ubicacion, at.id_legajo, -- (PBI 8589)
        (select n_ubicacion from ipj.t_ubicaciones u where u.id_ubicacion = at.id_ubicacion) n_ubicacion,
        (select siglas from ipj.t_ubicaciones u where u.id_ubicacion = at.id_ubicacion) siglas,
        (select denominacion_sia from ipj.t_legajos l where l.id_legajo = at.id_legajo) denominacion,
        ap.id_motivo_vista, nvl(ap.prioridad,0) prioridad,
        (SELECT n_motivo_vista FROM ipj.t_tipos_motivos_vista v WHERE v.id_motivo_vista = ap.id_motivo_vista) motivo_vista,
        (to_date(SYSDATE,'dd/mm/rrrr') - ap.fecha_entrega) dias_prestado,
        (SELECT a.n_accion_archivo FROM ipj.t_acciones_archivo a WHERE at.id_accion_archivo = a.id_accion_archivo) n_accion_archivo
      from IPJ.T_ARCHIVO_PRESTAMOS ap join ipj.T_TIPOS_ESTADO_PREST tp
          on ap.Id_Estado_Prestamo = tp.Id_Estado_Prestamo
        join ipj.t_usuarios u
          on u.cuil_usuario = ap.cuil_solicitante
        join ipj.t_archivo_tramite at
          on at.Id_Archivo_Tramite = ap.Id_Archivo_Tramite
      where
        (nvl(p_id_estado_prestamo, 0) = 0 or ap.id_estado_prestamo = p_id_estado_prestamo) and
        ( (nvl(p_id_archivo_tramite, 0) <> 0 and ap.id_archivo_tramite = p_id_archivo_tramite) OR
          (v_es_archivo > 0 and ap.id_estado_prestamo NOT IN (5,7)) OR
          (v_es_archivo = 0 and p_cuil_usuario is not null and ap.cuil_solicitante = p_cuil_usuario and ap.id_estado_prestamo <> 5)
        )
      order by prioridad DESC, ap.fecha_pedido DESC;

  END SP_Traer_Archivo_Prestamos;

  PROCEDURE SP_Guardar_Archivo_Prestamo(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Archivo_Tramite in number,
    p_Fecha_Pedido in varchar2, -- es fecha
    p_Cuil_Solicitante in varchar2,
    p_Fecha_Entrega in varchar2, -- es fecha
    p_Ok_Entrega in varchar2,
    p_Fecha_Devolucion in varchar2, -- es fecha
    p_Ok_Devolucion in varchar2,
    p_Id_Estado_Prestamo in number,
    p_eliminar in number, -- 1 elimina
    p_cuil_prestador in varchar2,
    p_Id_Motivo_Vista ipj.t_tipos_motivos_vista.id_motivo_vista%TYPE,
    p_prioridad number,
    p_Id_Ubicacion NUMBER -- ubicacion del solicitante del prestamo
    )
  IS
  /*********************************************************
    Agrega o actualiza los datos de un prestamo de archivo.
    Son obligatorios el archivo prestado, el solicitante y la fecha de pedido.
  *********************************************************/
    v_valid_parametros varchar2(500);
    v_id_ubicacion NUMBER(10);
    v_id_tramite_suac number;
    v_existe_archivado number;
  BEGIN
    if IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('HAB_LOG_ARCHIVO') = '1' then
      IPJ.VARIOS.SP_GUARDAR_LOG(
        p_METODO => 'SP_Guardar_Archivo_Prestamo',
        p_NIVEL => 'ARCHIVO',
        p_ORIGEN => 'DB',
        p_USR_LOG  => 'DB',
        p_USR_WINDOWS => 'Sistema',
        p_IP_PC  => null,
        p_EXCEPCION =>
         'Id Archivo Tramite = ' || to_char(p_Id_Archivo_Tramite)
         || ' / Fecha Pedido = ' || p_Fecha_Pedido
         || ' / Cuil Solicitante = ' || p_Cuil_Solicitante
         || ' / Fecha Entrega = ' || p_Fecha_Entrega
         || ' / Ok Entrega = ' || p_Ok_Entrega
         || ' / Fecha Devolucion = ' || p_Fecha_Devolucion
         || ' / Ok Devolucion = ' || p_Ok_Devolucion
         || ' / Id Estado Prestamo = ' || to_char(p_Id_Estado_Prestamo)
         || ' / Eliminar = ' || to_char(p_eliminar)
         || ' / Cuil Prestado = ' || p_cuil_prestador
         || ' / Motivo Vista = ' || to_char(p_Id_Motivo_Vista)
         || ' / Prioridad = ' || to_char(p_prioridad)
         || ' / Ubicacion = ' || to_char(p_Id_Ubicacion)
      );
    end if;

    if IPJ.VARIOS.Valida_Fecha(p_Fecha_Pedido) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT') || ' (Fecha Pedido)';
    end if;
    if p_Fecha_Entrega is not null and IPJ.VARIOS.Valida_Fecha(p_Fecha_Entrega) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT') || ' (Fecha Entrega)';
    end if;
    if p_Fecha_Devolucion is not null and IPJ.VARIOS.Valida_Fecha(p_Fecha_Devolucion) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT') || ' (Fecha Devoluci�n)';
    end if;
    if p_cuil_solicitante is null then
      v_valid_parametros := v_valid_parametros || 'No hay solicitante.';
    end if;
    IF p_Id_Ubicacion = 0 THEN
      BEGIN
        SELECT u.id_ubicacion_preferencia
          INTO v_Id_Ubicacion
          FROM ipj.t_usuarios u
         WHERE u.cuil_usuario = p_Cuil_Solicitante;
      EXCEPTION
        WHEN OTHERS THEN
          v_Id_Ubicacion := NULL;
      END;
      IF v_Id_Ubicacion IS NULL THEN
         SELECT u.n_ubicacion
           INTO v_Id_Ubicacion
           FROM ipj.t_grupos_trab_ubicacion g
           JOIN ipj.t_ubicaciones u ON g.id_ubicacion = u.id_ubicacion
          WHERE g.cuil = p_Cuil_Solicitante
            AND ROWNUM = 1
         ORDER BY u.orden;
      END IF;
    ELSE
      v_Id_Ubicacion := p_Id_Ubicacion;
    END IF;
    
    -- Se se pasa a Digitalizado, se valida que este asociado a SUAC
    if p_Id_Estado_Prestamo = 7 then
      -- Busco el id de SUAC 
      select id_tramite into v_id_tramite_suac
      from ipj.t_archivo_tramite
      where
        id_archivo_tramite = p_Id_Archivo_Tramite;
      
      if nvl(v_id_tramite_suac, 0) = 0 then  
        v_valid_parametros := v_valid_parametros || 'El expediente no se encuentra asociado a SUAC, cargue el Sticker Completo.';
      end if;
    end if;

    -- Si los parametros no estan OK, no continuo
    if v_valid_parametros is not null then
      o_rdo := v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      return;
    end if;

    -- CUERPO DEL  PROCEDIMIENTO
    if nvl(p_eliminar, 0) = 1 then
      delete IPJ.T_ARCHIVO_PRESTAMOS
      where
        id_archivo_tramite = p_id_archivo_tramite and
        fecha_pedido = to_date(p_fecha_pedido, 'dd/mm/rrrr');
    else
      -- Verifico si el tramite ya estaba archivado, y se manda archivado para no cambiar nada
      select count(1) into  v_existe_archivado
      from IPJ.T_ARCHIVO_PRESTAMOS
      where
        id_archivo_tramite = p_id_archivo_tramite and
        fecha_pedido = to_date(p_fecha_pedido, 'dd/mm/rrrr') and
        id_estado_prestamo = p_id_estado_prestamo;
        
      -- Si no existe o no estaba archivado, lo actualizo o inserto
      if v_existe_archivado = 0 or p_id_estado_prestamo <> 5 then
        update IPJ.T_ARCHIVO_PRESTAMOS
        set
          fecha_entrega = to_date(p_fecha_entrega, 'dd/mm/rrrr'),
          ok_entrega = p_ok_entrega,
          fecha_devolucion = to_date(p_fecha_devolucion, 'dd/mm/rrrr'),
          ok_devolucion = p_ok_devolucion,
          id_estado_prestamo = p_id_estado_prestamo,
          cuil_prestador = (case when p_cuil_prestador is not null then p_cuil_prestador else cuil_prestador end)
        where
          id_archivo_tramite = p_id_archivo_tramite and
          fecha_pedido = to_date(p_fecha_pedido, 'dd/mm/rrrr') and
          id_estado_prestamo <> 5;

        -- si no existe, lo agrego
        if sql%rowcount = 0 then
          insert into IPJ.T_ARCHIVO_PRESTAMOS (id_archivo_tramite, fecha_pedido,
            cuil_solicitante, fecha_entrega, ok_entrega, fecha_devolucion, ok_devolucion,
            id_estado_prestamo, cuil_prestador, id_motivo_vista, prioridad, id_ubicacion)
          values (p_id_archivo_tramite, to_date(p_fecha_pedido, 'dd/mm/rrrr'),
            p_cuil_solicitante, to_date(p_fecha_entrega, 'dd/mm/rrrr'), p_ok_entrega,
            to_date(p_fecha_devolucion, 'dd/mm/rrrr'), p_ok_devolucion, p_id_estado_prestamo,
            (case when p_id_estado_prestamo = 1 then null else p_cuil_prestador end), decode(p_Id_Motivo_Vista,0,NULL,p_Id_Motivo_Vista),
            p_prioridad, v_Id_Ubicacion);
        end if;
      end if;

      -- si el estado es digitalizado, se marca en la tabla
      IF p_id_estado_prestamo = 7 THEN
        UPDATE ipj.t_archivo_tramite t
           SET t.digitalizado = 1
         WHERE t.id_archivo_tramite = p_Id_Archivo_Tramite;
      END IF;

    end if;

    o_rdo := TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
    WHEN OTHERS THEN
      o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;

  END SP_Guardar_Archivo_Prestamo;

  PROCEDURE SP_Reabrir_Tramite (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_cuil_usuario_Area in varchar2,
    p_Cuil_Usuario_Reabre in varchar2,
    p_observacion in varchar2,
    p_id_ubicacion in number)
  IS
  /**************************************************
    Pasa el tramite a Asignado nuevamente al area originaria del tr�mite.
  ***************************************************/
    v_id_ubicacion number;
    v_id_clasif_ipj number;
    v_bloque varchar2(100);
    v_min_accion number;
    v_row_Tramite IPJ.T_TRAMITESIPJ%ROWTYPE;
    v_id_ubicacion_reapertura number;
    v_rdo_SUAC varchar2(2000);
    v_tipo_mensaje_SUAC number;
    v_tipo_SUAC ipj.tramites_suac.Tipo_SUAC;
    v_estado varchar2(100);
  BEGIN
    if IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('HAB_LOG_ARCHIVO') = '1' then
      IPJ.VARIOS.SP_GUARDAR_LOG(
        p_METODO => 'SP_Reabrir_Tramite',
        p_NIVEL => 'Gestion - Archivo',
        p_ORIGEN => 'DB',
        p_USR_LOG  => 'DB',
        p_USR_WINDOWS => 'Sistema',
        p_IP_PC  => null,
        p_EXCEPCION =>
          ' Id Tramite IPJ = ' || to_char(p_Id_Tramite_Ipj)
          || ' / Cuil Usuario Area = ' || p_cuil_usuario_Area
          || ' / Cuil Usuario Reabre = ' || p_Cuil_Usuario_Reabre
          || ' / Observacion = ' || p_observacion
          || ' / Id Ubicacion = ' || to_char(p_id_ubicacion)
      );
    end if;

    -- Si no tiene tr�mite asociado, no se puede desarchivar
    if nvl(p_id_tramite_ipj , 0) = 0 then
      o_rdo := 'ERROR: No posee un tr�mite de Gesti�n asociado.';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      return;
    end if;

    -- Busco los datos del tramite
    select * into v_row_Tramite
    from ipj.t_tramitesipj
    where
      Id_Tramite_Ipj = p_Id_Tramite_Ipj;

   -- Si no es un tr�mite real, no se desarchiva
    if nvl(v_row_Tramite.id_tramite, 0) = 0 then
      o_rdo := 'ERROR: El tr�mite no esta asociado a un expediente en SUAC.';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      return;
    end if;

    -- Si el tr�mite no esta en Archivo o Mesa SUAC, no se puede desarchivar
    if v_row_Tramite.id_ubicacion not in (IPJ.TYPES.C_AREA_MESASUAC, IPJ.TYPES.C_AREA_ARCHIVO) then
      o_rdo := 'ERROR: El tr�mite no est� en Archivo en Gesti�n, no se puede Desarchivar.';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      return;
    end if;

    -- Verifico que en SUAC este archivado
    IPJ.TRAMITES_SUAC.SP_Buscar_Suac (
      o_rdo => v_rdo_SUAC,
      o_tipo_mensaje => v_tipo_mensaje_SUAC,
      o_SUAC => v_Tipo_SUAC,
      p_id_tramite => v_row_Tramite.id_tramite,
      p_loguear => 'N'
    );

    if v_rdo_SUAC <> IPJ.TYPES.C_RESP_OK then
      o_rdo := 'ERROR: No se encontr� el tr�mite en SUAC (' || v_rdo_SUAC || ').';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      return;
    else
      if v_Tipo_SUAC.v_estado <> 'ARCHIVADO' then
        o_rdo := 'ERROR: El tr�mite no se encuentra ARCHIVADO en SUAC, no se puede Desarchivar.';
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
        return;
      end if;
    end if;
    
    -- Si esta en estados Rechazado (no inactivo), no desarchiva
    if v_row_Tramite.id_estado_ult >= 200 and  v_row_Tramite.id_estado_ult <> 210 then -- Estados Rechazados
      -- Busco el nombre del estado
      select n_estado into v_estado
      from ipj.t_estados
      where 
        id_estado = v_row_Tramite.id_estado_ult;
       
      o_rdo := 'ERROR: El tr�mite se encuentra ' || upper(v_estado) || ', no es posible desarchivarlo.';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      return;
    end if;

    -- Si no viene la ubicacion, trabaja como lo hacia antes con la ubicacion de origen
    if nvl(p_id_ubicacion, 0) = 0 then
      -- Uso el area de origen del tr�mite
      v_id_ubicacion_reapertura := v_row_Tramite.id_ubicacion_origen;
      -- Busco la clasificaci�n de la primer accion del area de origen
      begin
        select id_clasif_ipj into v_id_clasif_ipj
        from
          ( select ac.id_tramiteipj_accion, ta.id_clasif_ipj
            from ipj.t_tramitesipj_acciones ac join ipj.t_tipos_accionesipj ta
                on ac.id_tipo_accion = ta.id_tipo_accion
              join ipj.t_tipos_clasif_ipj tc
                on tc.id_clasif_ipj = ta.id_clasif_ipj
            where
              tc.id_ubicacion =  v_row_Tramite.id_ubicacion_origen and
              id_tramite_ipj = p_id_tramite_ipj
            order by ac.id_tramiteipj_accion asc
          ) tmp
          where
            rownum = 1;
      exception
        -- Sino, tomo la primer clasificacion del area
        when NO_DATA_FOUND then
          select id_clasif_ipj into v_id_clasif_ipj
          from ipj.t_tipos_clasif_ipj
          where
            id_ubicacion = v_row_tramite.id_ubicacion_origen and
           rownum = 1;
      end;
    else
      -- Utilizo la ubicacion del par�metro
      v_id_ubicacion_reapertura := p_id_ubicacion;
      -- Si viene la ubicacion, utilizo la clasificacion de las acciones esa ubicacion
      begin
        select id_clasif_ipj into v_id_clasif_ipj
        from
          ( select ac.id_tramiteipj_accion, ta.id_clasif_ipj
            from ipj.t_tramitesipj_acciones ac join ipj.t_tipos_accionesipj ta
                on ac.id_tipo_accion = ta.id_tipo_accion
              join ipj.t_tipos_clasif_ipj tc
                on tc.id_clasif_ipj = ta.id_clasif_ipj
            where
              tc.id_ubicacion =  p_id_ubicacion and
              id_tramite_ipj = p_id_tramite_ipj
            order by ac.id_tramiteipj_accion asc
          ) tmp
          where
            rownum = 1;
      exception
        -- Sino, tomo la primer clasificacion del area
        when NO_DATA_FOUND then
          select id_clasif_ipj into v_id_clasif_ipj
          from ipj.t_tipos_clasif_ipj
          where
            id_ubicacion = p_id_ubicacion and
           rownum = 1;
      end;
    end if;

    -- Reabro el tr�mite en el area original, al usuario indicado y en la primer clasifcacion del area.
    update ipj.t_tramitesipj
    set
      id_estado_ult = IPJ.TYPES.c_Estados_Asignado,
      cuil_ult_estado = p_cuil_usuario_Area,
      id_grupo_ult_estado = null,
      id_ubicacion = v_id_ubicacion_reapertura,
      id_clasif_ipj = v_id_clasif_ipj,
      observacion = (case
                               when p_observacion is null then observacion
                               else substr(p_observacion || chr(13) || observacion, 1, 2000)
                             end)
    where
      Id_Tramite_Ipj = p_Id_Tramite_Ipj;

    -- Actualizo el historial
    IPJ.TRAMITES.SP_GUARDAR_TRAMITES_IPJ_ESTADO(
      o_rdo => o_rdo,
      o_tipo_mensaje => o_tipo_mensaje,
      v_fecha_pase => sysdate,
      v_id_estado => IPJ.TYPES.c_Estados_Asignado,
      v_cuil_usuario => p_Cuil_Usuario_Reabre,
      v_id_grupo => null,
      v_observacion => p_observacion,
      v_Id_Tramite_Ipj => p_Id_Tramite_Ipj
    );

    -- Si esta asociado a SUAC, realizo el pase si es necesario
    if p_cuil_usuario_Area <> '1' then
      v_bloque := 'Reabrir Tramite (Pases en SUAC): ';
      DBMS_OUTPUT.PUT_LINE('ARCHIVO - SP_Reabrir_Tramite: ' || to_char(v_Row_Tramite.id_tramite));
      if nvl(v_Row_Tramite.id_tramite, 0) > 0  and v_Row_Tramite.id_ubicacion in (IPJ.TYPES.C_AREA_ARCHIVO, IPJ.TYPES.C_AREA_MESASUAC) then
        IPJ.TRAMITES_SUAC.SP_Realizar_Tranf_IPJ(
          o_rdo => o_rdo,
          o_tipo_mensaje => o_tipo_mensaje,
          p_id_tramite_suac => v_Row_Tramite.id_tramite,
          p_id_ubicacion_origen => v_Row_Tramite.id_ubicacion,
          p_id_ubicacion_destino => v_id_ubicacion_reapertura   ,
          p_cuil_usuario_origen => p_Cuil_Usuario_Reabre,
          p_cuil_usuario_destino => p_cuil_usuario_Area );

       DBMS_OUTPUT.PUT_LINE('       o_rdo: ' || o_rdo);
        if upper(trim(o_rdo)) <> IPJ.TYPES.C_RESP_OK then
          o_rdo :=  v_bloque || '- ' || o_rdo;
          rollback;
          return;
        end if;
      end if;
    end if;

   -- Al reabrir el tr�mite, se marcan todos los datos en borrador
   update IPJ.T_COMERCIANTES_INS_LEGAL set borrador = 'S'  where Id_Tramite_Ipj = p_Id_Tramite_Ipj;
   update IPJ.T_COMERCIANTES_RUBROS set borrador = 'S' where Id_Tramite_Ipj = p_Id_Tramite_Ipj;
   update IPJ.T_COMERCIANTES_RUBRICA set borrador = 'S' where Id_Tramite_Ipj = p_Id_Tramite_Ipj;
   update IPJ.T_GRAVAMENES_INS_LEGAL set borrador = 'S' where Id_Tramite_Ipj = p_Id_Tramite_Ipj;
   update IPJ.T_MANDATOS_INS_LEGAL set borrador = 'S' where Id_Tramite_Ipj = p_Id_Tramite_Ipj;
   update IPJ.T_MANDATOS_MANDATARIOS set borrador = 'S' where Id_Tramite_Ipj = p_Id_Tramite_Ipj;
   update IPJ.T_fondos_comercio_rubro set borrador = 'S' where Id_Tramite_Ipj = p_Id_Tramite_Ipj;
   update IPJ.T_fondos_comercio_vendedor set borrador = 'S' where Id_Tramite_Ipj = p_Id_Tramite_Ipj;
   update IPJ.T_fondos_comercio_comprador set borrador = 'S' where Id_Tramite_Ipj = p_Id_Tramite_Ipj;
   update IPJ.T_fondos_comercio_ins_legal set borrador = 'S' where Id_Tramite_Ipj = p_Id_Tramite_Ipj;
   update IPJ.T_GRAVAMENES_INS_LEGAL set borrador = 'S' where Id_Tramite_Ipj = p_Id_Tramite_Ipj;
   update IPJ.T_MANDATOS_INS_LEGAL set borrador = 'S' where Id_Tramite_Ipj = p_Id_Tramite_Ipj;
   update IPJ.T_MANDATOS_MANDATARIOS set borrador = 'S' where Id_Tramite_Ipj = p_Id_Tramite_Ipj;

   IPJ.ENTIDAD_PERSJUR.SP_Actualizar_Borrador(
     o_rdo => o_rdo,
     o_tipo_mensaje => o_tipo_mensaje,
     p_Id_Tramite_Ipj => p_Id_Tramite_Ipj,
     p_id_legajo => null,
     p_borrador => 'S'
   );

   --Quito la marca en las observaciones de sistema
   update IPJ.T_ENTIDADES_NOTAS
   set id_nota_sistema = 0
   where
     id_tramite_ipj = p_id_tramite_ipj;

   if nvl(o_rdo, IPJ.TYPES.C_RESP_OK) = IPJ.TYPES.C_RESP_OK then
     o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
     o_rdo := IPJ.TYPES.C_RESP_OK;
     commit;
   else
     rollback;
   end if;

  EXCEPTION
    WHEN OTHERS THEN
       o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Reabrir_Tramite;

  PROCEDURE SP_Reabrir_Accion (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_id_tramiteipj_accion in number,
    p_cuil_usuario in varchar2,
    p_observacion in varchar2)
  IS
    v_id_tipo_accion number;
    v_id_documento number;
    v_n_documento varchar2(50);
    v_id_legajo number;
    v_id_fondo number;
    v_id_integrante number;
  /**************************************************
    Pasa la accion a En Proceso nuevamente.
  ***************************************************/
  BEGIN
    -- Busco los datos de la acci�n
    select id_tipo_accion, id_documento, n_documento, id_legajo, id_integrante, id_fondo_comercio
       into v_id_tipo_accion, v_id_documento, v_n_documento, v_id_legajo, v_id_integrante, v_id_fondo
    from IPJ.t_tramitesipj_acciones
    where
      Id_Tramite_Ipj = p_Id_Tramite_Ipj and
      id_tramiteipj_accion = p_id_tramiteipj_accion;

    -- Reabro la accion
    update ipj.t_tramitesipj_acciones
    set
      id_estado = IPJ.TYPES.c_Estados_Proceso,
       observacion = (case
                                when p_observacion is null then observacion
                                else substr(p_observacion || chr(13) || observacion, 1, 2000)
                             end),
        cuil_usuario = p_cuil_usuario
    where
      Id_Tramite_Ipj = p_Id_Tramite_Ipj and
      id_tramiteipj_accion = p_id_tramiteipj_accion;

    -- Ajusto el historial de la acci�n.
    IPJ.TRAMITES.SP_GUARDAR_TRAMITES_ACC_ESTADO(
      o_rdo => o_rdo,
      o_tipo_mensaje => o_tipo_mensaje,
      p_fecha_pase => sysdate,
      p_id_estado => IPJ.TYPES.c_Estados_Proceso,
      p_cuil_usuario => p_Cuil_Usuario,
      p_observacion => p_observacion,
      p_Id_Tramite_Ipj => p_Id_Tramite_Ipj,
      p_id_tramite_Accion => p_id_tramiteIpj_Accion,
      p_id_tipo_accion => v_id_tipo_accion,
      p_id_documento => v_id_documento,
      p_n_documento => v_n_documento 
    );

    --Marco nuevamente los datos como borradores
    if v_id_integrante is not null then
      update IPJ.T_COMERCIANTES_INS_LEGAL
      set borrador = 'S'
      where Id_Tramite_Ipj = p_Id_Tramite_Ipj;

      update IPJ.T_COMERCIANTES_RUBROS
      set borrador = 'S'
      where Id_Tramite_Ipj = p_Id_Tramite_Ipj;

      update IPJ.T_COMERCIANTES_RUBRICA
      set borrador = 'S'
      where Id_Tramite_Ipj = p_Id_Tramite_Ipj;

      update IPJ.T_GRAVAMENES_INS_LEGAL
      set borrador = 'S'
      where Id_Tramite_Ipj = p_Id_Tramite_Ipj;

      update IPJ.T_MANDATOS_INS_LEGAL
      set borrador = 'S'
      where Id_Tramite_Ipj = p_Id_Tramite_Ipj;

      update IPJ.T_MANDATOS_MANDATARIOS
      set borrador = 'S'
      where Id_Tramite_Ipj = p_Id_Tramite_Ipj;
    end if;

    if v_id_fondo is not null then
      update IPJ.T_fondos_comercio_rubro
      set borrador = 'S'
      where Id_Tramite_Ipj = p_Id_Tramite_Ipj;

      update IPJ.T_fondos_comercio_vendedor
      set borrador = 'S'
      where Id_Tramite_Ipj = p_Id_Tramite_Ipj;

      update IPJ.T_fondos_comercio_comprador
      set borrador = 'S'
      where Id_Tramite_Ipj = p_Id_Tramite_Ipj;

      update IPJ.T_fondos_comercio_ins_legal
      set borrador = 'S'
      where Id_Tramite_Ipj = p_Id_Tramite_Ipj;
    end if;

    if v_id_legajo is not null then
      IPJ.ENTIDAD_PERSJUR.SP_Actualizar_Borrador(
        o_rdo => o_rdo,
        o_tipo_mensaje => o_tipo_mensaje,
        p_Id_Tramite_Ipj => p_Id_Tramite_Ipj,
        p_id_legajo => v_id_legajo,
        p_borrador => 'S'
      );

      update IPJ.T_GRAVAMENES_INS_LEGAL
      set borrador = 'S'
      where Id_Tramite_Ipj = p_Id_Tramite_Ipj;

      update IPJ.T_MANDATOS_INS_LEGAL
      set borrador = 'S'
      where Id_Tramite_Ipj = p_Id_Tramite_Ipj;

      update IPJ.T_MANDATOS_MANDATARIOS
      set borrador = 'S'
      where Id_Tramite_Ipj = p_Id_Tramite_Ipj;
    end if;

    if nvl(o_rdo, IPJ.TYPES.C_RESP_OK) = IPJ.TYPES.C_RESP_OK then
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
      o_rdo := IPJ.TYPES.C_RESP_OK;
      commit;
    else
      rollback;
    end if;

  EXCEPTION
    WHEN OTHERS THEN
       o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Reabrir_Accion;

  PROCEDURE SP_Archivar_Expediente(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in number,
    p_id_tramite_suac in number)
  IS
    v_SUAC IPJ.TRAMITES_SUAC.Tipo_SUAC;
    v_cuil_usuario varchar2(20);
    v_id_ubicacion number;
    v_unid_archivo varchar2(500);
    v_Fecha_Pase_Ultimo TIMESTAMP;
    v_Usuario_Pase_Ultimo Varchar2(100);
    v_Unidad_Pase_Ultimo Varchar2 (500);
    v_prox_pase number;
  BEGIN
    DBMS_OUTPUT.PUT_LINE('ARCHIVAR EXPEDIENTE');
    -- Veo en que area se encuentra el tr�mite en Gesti�n
    select id_ubicacion, cuil_ult_estado into v_id_ubicacion, v_cuil_usuario
    from ipj.t_tramitesipj
    where
      id_tramite_ipj = p_id_tramite_ipj;

    DBMS_OUTPUT.PUT_LINE('  Id Ubicacion = ' || to_char(v_id_ubicacion));
    DBMS_OUTPUT.PUT_LINE('  Usuario = ' || v_cuil_usuario);
    -- Si el expediente se encuentra en ARCHIVO, trato de archivar en SUAC
    if v_id_ubicacion = IPJ.TYPES.C_AREA_ARCHIVO then
      -- busco el sticker de 14 del tr�mite para ver su estado
      IPJ.TRAMITES_SUAC.SP_Buscar_Suac (
        o_rdo => o_rdo,
        o_tipo_mensaje => o_tipo_mensaje,
        o_SUAC => v_SUAC,
        p_id_tramite => p_id_tramite_suac,
        p_loguear => 'N');

      DBMS_OUTPUT.PUT_LINE('  Buscar Expediente ' || to_char(p_id_tramite_suac) || ' = ' || o_rdo);

      --Busco la unidad de Archivo
      select n_unidad into v_unid_archivo
      from nuevosuac.vt_unidades
      where
        codigo = 'MEDIPJ01' and
        fecha_hasta is null and
        nvl(desactivado, 0) = 0 and
        nvl(externa, 0) = 0 and
        nvl(eliminado, 0) = 0;

      DBMS_OUTPUT.PUT_LINE('  Estado Expediente = ' || v_SUAC.v_estado);
      -- Dependiendo del estado en SUAC, opero
      case
        -- Si esta A ENVIAR, y esta en MESA, lo cierro; sino esta en MESA lo informo
        when v_SUAC.v_estado = 'A ENVIAR' then
          DBMS_OUTPUT.PUT_LINE('  A ENVIAR - Tr�mite = ' || to_char(p_id_tramite_suac));
          DBMS_OUTPUT.PUT_LINE('  A ENVIAR - Unidad Actual = ' || v_SUAC.v_unidad_actual);
          DBMS_OUTPUT.PUT_LINE('  A ENVIAR - Unidad Archivo = ' || v_unid_archivo);
          DBMS_OUTPUT.PUT_LINE('  A ENVIAR - Usuario = ' || v_cuil_usuario);
          if v_SUAC.v_unidad_actual = v_unid_archivo then
            IPJ.TRAMITES_SUAC.SP_Cerrar_Tramite(
              p_id_tramite_suac => p_id_tramite_suac,
              p_cod_unidad => 'MEDIPJ01', --v_SUAC.v_unidad_actual,
              p_usuario => v_cuil_usuario,
              o_rdo => o_rdo,
              o_tipo_mensaje => o_tipo_mensaje
            );
            DBMS_OUTPUT.PUT_LINE('  A ENVIAR - Cierra Tr�mite = ' || o_rdo);
          else
            o_rdo := 'El tr�mite no se encuentra en MESA SUAC en SUAC';
            o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
          end if;
        -- Si esta A RECIBIR, si tiene un pase a mesa lo tomo y lo archivo
        when v_SUAC.v_estado = 'A RECIBIR' then
          begin
            -- Consultos los pases que posee el tramite
            IPJ.TRAMITES_SUAC.SP_Buscar_Pases(
              p_nro_sticker_completo => v_SUAC.v_nro_sticker_completo,
              o_fecha_pase => v_Fecha_Pase_Ultimo,
              o_usuario_pase => v_Usuario_Pase_Ultimo,
              o_unidad_pase => v_Unidad_Pase_Ultimo,
              o_Prox_pase => v_prox_pase,
              o_rdo => o_rdo,
              o_tipo_mensaje => o_tipo_mensaje);

            if o_rdo <> IPJ.TYPES.C_RESP_OK then
              o_rdo := 'Archivar Expediente - Buscar Pase SUAC: '  || o_rdo;
              return;
            end if;

            -- Si el ultimo pase, es a la mesa suac, lo acepto
            if v_Unidad_Pase_Ultimo = v_unid_archivo then
              -- Acepto el ultimo pase
              IPJ.TRAMITES_SUAC.SP_Aceptar_Pase(
                p_nro_sticker_completo => v_SUAC.v_nro_sticker_completo,
                p_cod_unidad_receptora => 'MEDIPJ01',
                p_usuario_receptor => v_cuil_usuario,
                p_cuerpos => v_SUAC.v_cuerpos,
                p_folios => v_SUAC.v_fojas,
                o_rdo => o_rdo,
                o_tipo_mensaje => o_tipo_mensaje);

              if o_rdo <> IPJ.TYPES.C_RESP_OK then
                o_rdo := 'Archivar Expediente - Aceptar Pase SUAC: '  || o_rdo;
                return;
              end if;

              IPJ.TRAMITES_SUAC.SP_Cerrar_Tramite(
                p_id_tramite_suac => p_id_tramite_suac,
                p_cod_unidad => 'MEDIPJ01', --v_SUAC.v_unidad_actual,
                p_usuario => v_cuil_usuario,
                o_rdo => o_rdo,
                o_tipo_mensaje => o_tipo_mensaje
              );
            else
              o_rdo := 'El tr�mite posee un pase en SUAC y no es a MESA SUAC.';
              o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
            end if;
          end;
        -- Si ya esta ARCHIVADO no hago nada
        when v_SUAC.v_estado = 'ARCHIVADO' then
          o_rdo := TYPES.C_RESP_OK;
          o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
      end case;

    else
      o_rdo := TYPES.C_RESP_OK;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    end if;

  EXCEPTION
    WHEN OTHERS THEN
      o_rdo := 'SP_Archivar_Expediente: ' || To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;

  END SP_Archivar_Expediente;

  PROCEDURE SP_Informar_Prestamos(
    o_Cursor OUT types.cursorType,
    p_Lista_Tramites in varchar2)
  IS
  /*
    Para los tr�mites indicados en la lista, muestra los siguientes datos:
    - Expediente: De archivo o SUAC
    - Fojas: de SUAC
    - Cuerpos: de SUAC
    - Asunto: del tr�mite o SUAC
  */
    v_SQL varchar2(4000);
  BEGIN
      v_SQL:=
        'select ARCH.EXPEDIENTE, ' ||
        '  IPJ.TRAMITES_SUAC.FC_Buscar_Fojas_SUAC(arch.id_tramite)Fojas, ' ||
        '  IPJ.TRAMITES_SUAC.FC_Buscar_Cuerpos_SUAC(arch.id_tramite) Cuerpos, ' ||
        '  IPJ.TRAMITES_SUAC.FC_Buscar_Asunto_SUAC (arch.id_tramite) Asunto ' ||
        'from IPJ.T_ARCHIVO_TRAMITE arch ' ||
        'where ' ||
        '  ARCH.ID_ARCHIVO_TRAMITE in (' || p_Lista_Tramites || ') ';

    OPEN o_Cursor FOR v_SQL;

  exception
    when OTHERS then
      IPJ.VARIOS.SP_GUARDAR_LOG(
        p_METODO => 'SP_Informar_Prestamos',
        p_NIVEL => 'Gestion - Archivo',
        p_ORIGEN => 'DB',
        p_USR_LOG  => 'DB',
        p_USR_WINDOWS => 'Sistema',
        p_IP_PC  => null,
        p_EXCEPCION =>
          'Consulta = ' || v_SQL
      );
      commit;
  END SP_Informar_Prestamos;

  PROCEDURE SP_Traer_Archivo_Desarchivo(
    o_Cursor OUT types.cursorType,
    p_cuil_usuario in varchar2,
    p_id_estado_desarchivo in number)
  IS
  /*********************************************************
    Muestra los desarchivos de una persona.
    Para el area de archivo, muestra todos los prestamos no archivados.
  *********************************************************/
    v_es_archivo number;
    v_valid_parametros varchar2(500);
  BEGIN

    /******** CUERPO DEL SP  *******************/
    --Busco si el usuario pertenece a Archivo
    select count(1) into v_es_archivo
    from IPJ.T_GRUPOS_TRAB_UBICACION
    where
      cuil = p_cuil_usuario and
      id_ubicacion = IPJ.TYPES.C_AREA_ARCHIVO;

    OPEN o_Cursor FOR
      select ad.Cuil_Solicitante, to_char(ad.Fecha_Pedido, 'dd/mm/rrrr') Fecha_Pedido,
        ad.Id_Archivo_Tramite, ad.id_estado_desarchivo, ad.id_documento,
        to_char(ad.fecha_desarchivo, 'dd/mm/rrrr') fecha_desarchivo,
        ed.N_estado_desarchivo, at.expediente, u.descripcion detalle,
        at.Id_Tramite_Ipj, at.Id_Tramiteipj_Accion, ad.cuil_usuario,
        (select u2.Descripcion from ipj.t_usuarios u2 where u2.cuil_usuario = ad.cuil_usuario)  Detalle_Usuario ,
        ad.id_ubicacion, at.id_legajo,
        (select n_ubicacion from ipj.t_ubicaciones u where u.id_ubicacion = ad.id_ubicacion) n_ubicacion,
        v_es_archivo es_archivo, ad.prioridad, ad.id_motivo_vista,
        (SELECT n_motivo_vista FROM ipj.t_tipos_motivos_vista v WHERE v.id_motivo_vista = ad.id_motivo_vista) motivo_vista,
        at.id_tramite_ipj
      from IPJ.T_ARCHIVO_DESARCHIVOS ad join ipj.T_TIPOS_ESTADO_DESARCH ed
          on ad.id_estado_desarchivo = ed.id_estado_desarchivo
        join ipj.t_usuarios u
          on u.cuil_usuario = ad.cuil_solicitante
        join ipj.t_archivo_tramite at
          on at.Id_Archivo_Tramite = ad.Id_Archivo_Tramite
      where
        (v_es_archivo > 0 and ad.id_estado_desarchivo < 3 and (nvl(p_id_estado_desarchivo, 0) = 0 or ad.id_estado_desarchivo = p_id_estado_desarchivo)) OR
        (v_es_archivo = 0 and ad.cuil_solicitante = p_cuil_usuario and ad.id_estado_desarchivo < 5) -- 5 Informado Anulado
      order by ad.prioridad desc, ad.fecha_pedido DESC;

  END SP_Traer_Archivo_Desarchivo;

  PROCEDURE SP_Guardar_Archivo_Desarchivo(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_archivo_tramite in number,
    p_fecha_pedido in varchar2, -- es fecha
    p_cuil_solicitante in varchar2,
    p_fecha_desarchivo in varchar2, -- es fecha
    p_id_estado_desarchivo in number,
    p_cuil_usuario in varchar2,
    p_id_documento in number,
    p_prioridad in number,
    p_Id_Ubicacion in NUMBER, -- ubicacion del solicitante del desarchivo
    p_id_motivo_vista in number
    )
  IS
  /*********************************************************
    Muestra los desarchivos de una persona.
    Para el area de archivo, muestra todos los prestamos no archivados.
  *********************************************************/
    v_es_archivo number;
    v_valid_parametros varchar2(500);
    v_id_ubicacion NUMBER(10);
    v_id_tramite_ipj number;
    v_id_estado number;
    v_estado varchar2(100);
  BEGIN
     if IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('HAB_LOG_ARCHIVO') = '1' then
      IPJ.VARIOS.SP_GUARDAR_LOG(
        p_METODO => 'SP_Guardar_Archivo_Desarchivo',
        p_NIVEL => 'Gestion - Archivo',
        p_ORIGEN => 'DB',
        p_USR_LOG  => 'DB',
        p_USR_WINDOWS => 'Sistema',
        p_IP_PC  => null,
        p_EXCEPCION =>
          'Id_Archivo_Tramite = ' || to_char(p_id_archivo_tramite)
          || ' / Fecha Pedido = ' || p_fecha_pedido
          || ' / Cuil Solicitante = ' || p_cuil_solicitante
          || ' / Fecha Desarchivo = ' || p_fecha_desarchivo
          || ' / Id Estado Desarchivo = ' || to_char(p_id_estado_desarchivo)
          || ' / Cuil Usuario = ' || p_cuil_usuario
          || ' / Id Documento = ' || to_char(p_id_documento)
          || ' / Prioridad = ' || to_char(p_prioridad)
          || ' / Id Ubicacion = ' || to_char(p_Id_Ubicacion)
          || ' / Id Motivos Vista = ' || to_char(p_id_motivo_vista)
      );
    end if;
    
    -- busco el tr�mite IPJ relacionado al archivo
    select id_tramite_ipj into v_id_tramite_ipj
    from ipj.t_archivo_tramite
    where 
      id_archivo_tramite = p_id_archivo_tramite;
      
    -- busco el estado del tr�mite ipj
    if nvl(v_id_tramite_ipj, 0) <> 0 then
      select e.n_estado, tr.id_estado_ult into v_estado, v_id_estado
      from ipj.t_tramitesipj tr join ipj.t_estados e
        on tr.id_estado_ult = e.id_estado
      where
        id_tramite_ipj = v_id_tramite_ipj;
    else
      v_estado := 'Cerrado'; -- Si no tiene tr�mite en gestion, se toma como Cerrado 
      v_id_estado := 110; --Por defexto se sobreentiende que el tr�mites esta ARCHIVADO OK
    end if;
    
    -- Si esta en estado rechazados (no inactivo), no se puede pedir desarchivo
    if v_id_estado >= 200 and v_id_estado <> 210 then
      o_rdo := 'ERROR: No es posible solicitar el desarchivo de un tr�mite en estado ' || upper(v_estado);
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      return;
    end if;

    IF p_Id_Ubicacion = 0 THEN
      SELECT u.id_ubicacion_preferencia
        INTO v_Id_Ubicacion
        FROM ipj.t_usuarios u
       WHERE u.cuil_usuario = p_Cuil_Solicitante;
      IF p_Id_Ubicacion IS NULL THEN
         SELECT u.n_ubicacion
           INTO v_Id_Ubicacion
           FROM ipj.t_grupos_trab_ubicacion g
           JOIN ipj.t_ubicaciones u ON g.id_ubicacion = u.id_ubicacion
          WHERE g.cuil = p_Cuil_Solicitante
            AND ROWNUM = 1
         ORDER BY u.orden;
      END IF;
    else
      v_Id_Ubicacion := p_Id_Ubicacion;
    END IF;

    -- Actualizo los datos de la solicitud de desarchivo
    update ipj.t_archivo_desarchivos
    set
      cuil_solicitante = p_cuil_solicitante,
      fecha_desarchivo = to_date(p_fecha_desarchivo, 'dd/mm/rrrr'),
      id_estado_desarchivo = p_id_estado_desarchivo,
      cuil_usuario = p_cuil_usuario,
      id_documento = p_id_documento,
      id_motivo_vista = decode(p_id_motivo_vista, 0, null, p_id_motivo_vista)
    where
      id_archivo_tramite = p_id_archivo_tramite and
      fecha_pedido = to_date(p_fecha_pedido, 'dd/mm/rrrr');

     -- si no existe, lo agrego
     if sql%rowcount = 0 then
       -- Inserta un nuevo pedido si no existe
       insert into ipj.t_archivo_desarchivos (id_archivo_tramite, fecha_pedido,
         cuil_solicitante, fecha_desarchivo, id_estado_desarchivo, cuil_usuario,
         id_documento, id_ubicacion, prioridad, id_motivo_vista)
       values (p_id_archivo_tramite, to_date(p_fecha_pedido, 'dd/mm/rrrr'),
         p_cuil_solicitante, to_Date(p_fecha_desarchivo, 'dd/mm/rrrr'),
         p_id_estado_desarchivo, p_cuil_usuario, p_id_documento, v_id_ubicacion,
         p_prioridad, decode(p_id_motivo_vista, 0, null, p_id_motivo_vista));
     end if;

    o_rdo := TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    commit;
  EXCEPTION
    WHEN OTHERS THEN
      o_rdo := 'SP_Guardar_Archivo_Desarchivo: ' || To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      rollback;
  END SP_Guardar_Archivo_Desarchivo;

  PROCEDURE SP_Traer_Movimientos_Solicitud(
    o_Cursor OUT types.cursorType,
    p_Id_Ubicacion in NUMBER,
    p_TipoSolicitud in VARCHAR2,
    p_Expediente IN VARCHAR2,
    p_Fecha_Desde IN VARCHAR2,
    p_Fecha_Hasta IN VARCHAR2)
  IS
  /*********************************************************
    Muestra los movimientos de los pedidos solicitados
  *********************************************************/
  BEGIN

    if IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('HAB_LOG_ARCHIVO') = '1' then
      IPJ.VARIOS.SP_GUARDAR_LOG(
        p_METODO => 'SP_Traer_Movimientos_Solicitud',
        p_NIVEL => 'ARCHIVO',
        p_ORIGEN => 'DB',
        p_USR_LOG  => 'DB',
        p_USR_WINDOWS => 'Sistema',
        p_IP_PC  => null,
        p_EXCEPCION =>
         ' / Ubicacion = ' || to_char(p_Id_Ubicacion)
         || ' / Tipo Solicitud = ' || p_TipoSolicitud
         || ' / Expediente = ' || p_Expediente
         || ' / Fecha Desde = ' || p_Fecha_Desde
         || ' / Fecha Hasta = ' || p_Fecha_Hasta
      );
    end if;

    OPEN o_Cursor FOR
      SELECT 'VISTA' TIPO_SOLICITUD, to_char(ap.fecha_pedido,'dd/mm/rrrr') fecha_pedido, to_char(ap.fecha_entrega,'dd/mm/rrrr') fecha_entrega
           , to_char(ap.fecha_devolucion,'dd/mm/rrrr') fecha_devolucion, a.expediente, u.id_ubicacion, u.n_ubicacion
           , ap.cuil_solicitante, us.descripcion solicitante, ap.cuil_prestador, up.descripcion prestador
        FROM ipj.t_archivo_prestamos ap
        JOIN ipj.t_archivo_tramite a ON ap.id_archivo_tramite = a.id_archivo_tramite
        LEFT JOIN ipj.t_ubicaciones u ON ap.id_ubicacion = u.id_ubicacion
        LEFT JOIN ipj.t_usuarios us ON ap.cuil_solicitante = us.cuil_usuario
        LEFT JOIN ipj.t_usuarios up ON ap.cuil_prestador = up.cuil_usuario
       WHERE nvl(p_TipoSolicitud,'0') IN ('0','1') AND
             (p_Id_Ubicacion = 0 OR ap.id_ubicacion = p_Id_Ubicacion) AND
             (to_date(p_Fecha_Desde, 'dd/mm/rrrr') is null or (ap.fecha_pedido between to_date(p_Fecha_Desde, 'dd/mm/rrrr') and to_date(p_Fecha_Hasta, 'dd/mm/rrrr'))) AND
             (p_Expediente is null or a.expediente like '%' || p_Expediente || '%')
   UNION ALL

      SELECT 'DESARCHIVO' TIPO_SOLICITUD, to_char(ad.fecha_pedido,'dd/mm/rrrr') fecha_pedido, to_char(ad.fecha_desarchivo,'dd/mm/rrrr') fecha_entrega
           , NULL fecha_devolucion, a.expediente, u.id_ubicacion, u.n_ubicacion
           , ad.cuil_solicitante, us.descripcion solicitante, NULL cuil_prestador, NULL descripcion_prestador
        FROM ipj.t_archivo_desarchivos ad
        JOIN ipj.t_archivo_tramite a ON ad.id_archivo_tramite = a.id_archivo_tramite
        JOIN ipj.t_ubicaciones u ON ad.id_ubicacion = u.id_ubicacion
        LEFT JOIN ipj.t_usuarios us ON ad.cuil_solicitante = us.cuil_usuario
        --LEFT JOIN ipj.t_usuarios up ON ad.cuil_usuario = up.cuil_usuario
       WHERE nvl(p_TipoSolicitud,'0') IN ('0','2') AND
             (p_Id_Ubicacion = 0 OR ad.id_ubicacion = p_Id_Ubicacion) AND
             (to_date(p_Fecha_Desde, 'dd/mm/rrrr') is null or (ad.fecha_pedido between to_date(p_Fecha_Desde, 'dd/mm/rrrr') and to_date(p_Fecha_Hasta, 'dd/mm/rrrr'))) AND
             (p_Expediente is null or a.expediente like '%' || p_Expediente || '%');

  END SP_Traer_Movimientos_Solicitud;

  PROCEDURE SP_Validar_Usuario_Desarchivo(
    o_Res OUT NUMBER, --0 sin permiso 1 con permiso
    p_Cuil_Usuario IN VARCHAR2)
  IS
  /*********************************************************
    Valida si el usuario tiene permiso de desarchivo
  *********************************************************/
  BEGIN

    SELECT COUNT(1)
      INTO o_Res
      FROM ipj.t_usuarios u
     WHERE u.cuil_usuario = p_Cuil_Usuario
       AND u.desarchivo = 'S';

  EXCEPTION
    WHEN OTHERS THEN
      o_Res := 0;

  END SP_Validar_Usuario_Desarchivo;
  
  PROCEDURE SP_Realizar_Expurgo(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_cant_errores out number,
    p_nro_expurgo in number,
    p_fecha in varchar2, -- Es Fecha
    p_resolucion in varchar2,
    p_cuil_usuario in varchar2)
  IS
  /*********************************************************
    Dado un expurgo, marca los expedientes como expurgados en SUAC y Gesti�n
  *********************************************************/
    v_row_expurgo IPJ.t_expurgo%rowtype;
    v_row_Archivo IPJ.t_archivo_tramite%rowtype;
    v_Cursor_Expurgo types.cursorType;
    v_SUAC IPJ.TRAMITES_SUAC.Tipo_SUAC;
    v_rdo varchar2(2000);
    v_tipo_mensaje number;   
    v_cant_errores number; 
    v_codigo_suac varchar2(100);
  BEGIN
    -- Busco el expurgo, para verificar que no ese completo
    select * into v_row_expurgo
    from ipj.t_expurgo
    where
      nro_expurgo = p_nro_expurgo;
      
    if v_row_expurgo.fecha is not null then
      o_rdo := 'El expurgo seleccionado ya fue procesado seg�n Resoluci�n ' || v_row_expurgo.resolucion || ' de fecha ' || to_date(v_row_expurgo.fecha, 'dd/mm/rrrr');
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      return;
    end if;
    
    -- Actualizo los datos en el expurgo
    update ipj.t_expurgo
    set
      fecha = to_date(p_fecha, 'dd/mm/rrrr'),
      resolucion = p_resolucion
    where
      nro_expurgo = p_nro_expurgo;
      
     -- Inicia sin errores
     v_cant_errores := 0;
     
    -- Busco los expedientes del expurgo, y proceso uno a uno
    OPEN v_Cursor_Expurgo FOR
      select *
      from ipj.t_archivo_tramite
      where
        nro_expurgo = p_nro_expurgo;
    
    -- Para cada expediente del expurgo, lo expurgo en Gestion, e intento expurgarlo en SUAC      
    loop
      fetch v_Cursor_Expurgo into v_row_Archivo;
      EXIT WHEN v_Cursor_Expurgo%NOTFOUND or v_Cursor_Expurgo%NOTFOUND is null;
      
      -- Quito los valores de Archivo en y cargo la observaci�n del expurgo
      update ipj.t_archivo_tramite
      set
        observacion = ' Expurgado por Resoluci�n ' || p_resolucion || ' de fecha ' || p_fecha,
        nro_tabla = null,
        id_paquete = null,
        nro_caja = null,
        id_proveedor = null,
        estanteria = null,
        estante = null,
        columna = null,
        id_estado_archivo = 4
      where
        id_archivo_tramite = v_row_Archivo.id_archivo_tramite;
      
      --Si no tiene relacion a SUAC, lo marco como error y no hago nada con SUAC
      if nvl(v_row_Archivo.id_tramite, 0) = 0 then
        insert into ipj.t_expurgo_error (id_archivo_tramite, nro_expurgo, msj_error, corregido)
        values (v_row_Archivo.id_archivo_tramite, p_nro_expurgo, 'Expediente no asociado a SUAC' , 'N');
        
        v_cant_errores := v_cant_errores + 1; 
      else
        -- Busco los codigos necesarios
        IPJ.TRAMITES_SUAC.SP_Buscar_Suac (
          o_rdo => v_rdo,
          o_tipo_mensaje => v_tipo_mensaje,
          o_SUAC => v_SUAC,
          p_id_tramite => v_row_Archivo.id_tramite,
          p_loguear => 'N'
        );
        
        -- Si no encuentro el tr�mite en SUAC, lo marco como error 
        if v_rdo <> IPJ.TYPES.c_Resp_OK then
          insert into ipj.t_expurgo_error (id_archivo_tramite, nro_expurgo, msj_error, corregido)
          values (v_row_Archivo.id_archivo_tramite, p_nro_expurgo, 'SP_Buscar_Suac: ' || v_rdo, 'N');
          
          v_cant_errores := v_cant_errores + 1;
        else
          -- Busco el c�digo de la unidad actual
          select codigo into v_codigo_suac 
          from NUEVOSUAC.VT_UNIDADES 
          where 
            n_unidad = v_SUAC.v_unidad_actual and 
            fecha_hasta is null and 
            nvl(desactivado, 0) = 0 and 
            nvl(externa, 0) = 0 and 
            nvl(eliminado, 0) = 0;
            
          -- Marco como Expurgado en SUAC
          NUEVOSUAC.PCK_INTERFAZ_VERTICALES_V2.pr_expurgar_expediente (
            i_sticker => v_SUAC.v_nro_sticker_completo,
            i_fecha => to_date(p_fecha, 'dd/mm/rrrr'),
            i_decreto => p_resolucion,
            i_id_usuario => IPJ.TRAMITES_SUAC.FC_ARMAR_USUARIO_SUAC(p_cuil_usuario),
            i_cod_unidad => v_codigo_suac,
            o_resultado => v_rdo
          );
          
          -- Si falla en SUAC, se marca como error
          if v_rdo <> IPJ.TYPES.c_Resp_OK then
            insert into ipj.t_expurgo_error (id_archivo_tramite, nro_expurgo, msj_error, corregido)
            values (v_row_Archivo.id_archivo_tramite, p_nro_expurgo, 'SUAC.PR_Expurgar_Expediente: ' || v_rdo, 'N');
            
            v_cant_errores := v_cant_errores + 1;
          end if;
        end if;
      end if;
    end loop;
    
    close v_Cursor_Expurgo;
    
    o_cant_errores := v_cant_errores;
    if v_cant_errores = 0 then
      o_rdo := ipj.TYPES.C_RESP_OK;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    else
      o_rdo := 'Se cerr� el expurgo, pero hubo problemas con algunos expedientes. Consulte la lista de errores del expurgo.';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
    end if;
    
    commit;
  EXCEPTION
    WHEN OTHERS THEN
      o_rdo := 'SP_Realizar_Expurgo - ' || To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      o_cant_errores := -1;
  END SP_Realizar_Expurgo;
  
  PROCEDURE SP_Traer_Error_Expurgo(
    o_Cursor OUT types.cursorType)
  IS
  /*********************************************************
    Lista los expedientes que fallaron al asentar los expurgos.
    Primero actualiza los estados, luego lista el cursor
  *********************************************************/
    v_row_Errores IPJ.t_expurgo_error%rowtype;
    v_Cursor_Expurgo types.cursorType;
    v_SUAC IPJ.TRAMITES_SUAC.Tipo_SUAC;
    v_rdo varchar2(2000);
    v_tipo_mensaje number;
    v_id_tramite_suac number;
  BEGIN
     -- Busco los expedientes del expurgo, y proceso uno a uno
    OPEN v_Cursor_Expurgo FOR
      select *
      from ipj.t_expurgo_error
      where
        corregido = 'N';
    
    -- Para cada expediente del expurgo, lo expurgo en Gestion, e intento expurgarlo en SUAC      
    loop
      fetch v_Cursor_Expurgo into v_row_Errores;
      EXIT WHEN v_Cursor_Expurgo%NOTFOUND or v_Cursor_Expurgo%NOTFOUND is null;
      -- Busco el tr�mite SUAC
      select id_tramite into v_id_tramite_suac
      from ipj.t_archivo_tramite 
      where id_archivo_tramite = v_row_Errores.id_archivo_tramite;
      
      -- Busco los codigos necesarios
      IPJ.TRAMITES_SUAC.SP_Buscar_Suac (
        o_rdo => v_rdo,
        o_tipo_mensaje => v_tipo_mensaje,
        o_SUAC => v_SUAC,
        p_id_tramite => v_id_tramite_suac,
        p_loguear => 'N'
      );
      
      -- Si lo encuentr en SUAC, y ya esta expurgado, desmarco el error
      if v_rdo = IPJ.TYPES.c_Resp_OK and v_SUAC.v_estado = 'EXPURGADO'  then -- Id 8 de SUAC
        update ipj.t_expurgo_error
        set corregido = 'S'
        where
          id_archivo_tramite = v_row_Errores.id_archivo_tramite;
      
      end if;
    
    end loop;
    
    close v_Cursor_Expurgo;
    
    commit;
  
    OPEN o_Cursor FOR
      select ee.nro_expurgo, ex.resolucion , ar.expediente, ee.msj_error, ee.corregido,
        ex.n_expurgo
      from ipj.t_expurgo_error ee join ipj.t_archivo_tramite ar 
          on ee.id_archivo_tramite = ar.id_archivo_tramite
        join ipj.t_expurgo ex
          on ee.nro_expurgo = ex.nro_expurgo
      where 
        corregido = 'N'
      order by ee.nro_expurgo asc;
  END SP_Traer_Error_Expurgo;
  
   PROCEDURE SP_Desarchivo_Digital (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_cuil_usuario in varchar2,
    p_Cuil_Usuario_asigna in varchar2,
    p_id_motivo_vista in number,
    p_observacion in varchar2,
    p_id_ubicacion in number)
  IS
  /**************************************************
    Desarchiva un tr�mite digital, si cumple las validaciones definidas
  ***************************************************/
    v_area_jefe number;
    v_row_usudes  ipj.t_usuarios%ROWTYPE;
    v_row_tramite ipj.t_tramitesipj%rowtype;
    v_row_motivos ipj.t_tipos_motivos_vista%rowtype;
    v_Tiene_Permisos number;
    v_SUAC IPJ.TRAMITES_SUAC.Tipo_SUAC;
    v_rdo_SUAC varchar2(2000);
    v_tipo_mensaje_SUAC number;
    v_resp_mesa varchar2(20);
    v_area_destino varchar2(20);
    v_id_clasif_ipj number;
    v_Cursor ipj.types.CURSORTYPE;
    v_row_acciones ipj.t_tramitesipj_acciones%rowtype;
    v_obs_mtv VARCHAR2(2000);
    v_obs VARCHAR2(2000);
    v_cant_exp NUMBER;
    v_id_legajo NUMBER;

  BEGIN

   -- Verifico que el usuario posea permisos de desarchivo Digital
    BEGIN

      SELECT *
        INTO v_row_usudes
        FROM ipj.t_usuarios
       WHERE cuil_usuario = p_cuil_usuario;
    
    EXCEPTION
      WHEN no_data_found THEN
        v_row_usudes.desarch_dig := 'N'; 
      WHEN OTHERS THEN 
        v_row_usudes.desarch_dig := 'N';
    END;

    IF nvl(v_row_usudes.desarch_dig, 'N') <> 'S' then
      o_rdo := 'Usted no tiene permiso para desarchivar Expedientes Digitales.';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      return;
    end if;
    
    -- Optengo los datos del tr�mite para varias validaciones
    select * into v_row_tramite
    from ipj.t_tramitesipj
    where
      id_tramite_ipj = p_id_tramite_ipj;
      
    -- Verifico que sea un tr�mite digital
    if IPJ.TRAMITES.FC_ES_TRAM_DIGITAL(v_row_tramite.id_tramite_ipj) = 0 then
      o_rdo := 'El tr�mite seleccionado no es Digital.';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      return;
    end if;
    
    --Verifico que el tr�mite se encuentre en un estado v�lido para desarchivar
    if v_row_tramite.id_estado_ult not in (100, 110, 200, 203, 204) then
      o_rdo := 'El tr�mite seleccionado no se encuentra en un estado que habilite el Desarchivo.';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      return;
    end if;
    
    SELECT COUNT(id_tramite_ipj)
      INTO v_cant_exp
      FROM ipj.t_tramitesipj
     WHERE expediente = v_row_tramite.expediente;
    
    --Verifico que el exediente no est� duplicado.
    IF v_cant_exp > 1  THEN 
       o_rdo :='El expediente que intenta desarchivar se encuentra duplicado, comun�quese con el �rea de sistemas.';
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      RETURN;
    END IF;
    
    -- Verifico que la persona a quien asigna, tenga permisos en el �rea del tr�mite
    select count(1) into v_Tiene_Permisos
    from ipj.t_grupos_trab_ubicacion g
    where
      g.cuil = p_cuil_usuario_asigna and
      g.id_ubicacion = v_row_tramite.id_ubicacion_origen;
    
    if v_Tiene_Permisos = 0 then
      o_rdo := 'La persona a la que intenta asignar el expediente no pertenece a su �rea.';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      return;
    end if;
    
    -- Busco los datos del tr�mite SUAC
    IPJ.TRAMITES_SUAC.SP_Buscar_Suac (
      o_rdo => v_rdo_SUAC,
      o_tipo_mensaje => v_tipo_mensaje_SUAC,
      o_SUAC => v_SUAC,
      p_id_tramite => v_row_tramite.id_tramite,
      p_loguear => 'N'
    );
     
    if v_rdo_SUAC <> ipj.TYPES.C_RESP_OK then
      o_rdo := v_rdo_SUAC;
      o_tipo_mensaje := v_tipo_mensaje_SUAC;
      return;
    end if;
    
    -- Si en SUAC no esta archivado, no continuo.
    if v_SUAC.v_estado <> 'ARCHIVADO' then
      o_rdo := 'El expediente en SUAC no esta en estado ARCHIVADO, no se puede desarchivar.';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      return;
    end if;
    
    /*********************************************************
       INICIO EL PROCESO DE DESARCHIVO EN SUAC
    *********************************************************/
    -- Busco el responsable de Mesa de Entrada en SUAC
    select id_resp_mesa into v_resp_mesa
    from nuevosuac.vt_get_rep_ipj
    where
      codigo_mesa = (select u.codigo_suac from ipj.t_ubicaciones u where u.id_ubicacion = IPJ.TYPES.C_AREA_ARCHIVO);
      
    -- Busco el �rea SUAC de Destino
    select u.codigo_suac into v_area_destino
    from ipj.t_ubicaciones u 
    where 
      u.id_ubicacion = v_row_tramite.id_ubicacion_origen;

    -- Se reabre el tr�mite y se deja un pase al usuario
    IPJ.TRAMITES_SUAC.SP_Reabrir_Tramite(
      o_rdo => o_rdo,
      o_tipo_mensaje => o_tipo_mensaje,
      p_sticker_14 => v_SUAC.v_nro_sticker_completo,
      p_usuario => v_resp_mesa,
      p_unidad => ipj.TRAMITES_SUAC.FC_Buscar_Cod_Unidad_SUAC(v_SUAC.v_unidad_actual),
      p_unidad_destino => v_area_destino
    );
    
    -- Si falla la reapertura, paro
    if o_rdo <> IPJ.TYPES.C_RESP_OK then
      o_rdo :=  'Desarchivar Tr�mite SUAC - ' || o_rdo;
      rollback;
      return;
    end if;
    
    -- Acepto el pase que genera el desarchivo
    IPJ.TRAMITES_SUAC.SP_Aceptar_Pase(
      p_nro_sticker_completo => v_SUAC.v_nro_sticker_completo,
      p_cod_unidad_receptora => v_area_destino,
      p_usuario_receptor => ipj.tramites_suac.FC_Armar_Usuario_Suac (p_Cuil_Usuario_asigna),
      p_cuerpos => v_SUAC.v_cuerpos,
      p_folios => v_SUAC.v_fojas,
      o_rdo => o_rdo,
      o_tipo_mensaje => o_tipo_mensaje
    );
    -- Si falla tomar el pase, paro.
    if o_rdo <> IPJ.TYPES.C_RESP_OK then
      o_rdo :=  'Aceptar Pase SUAC - ' || o_rdo;
      rollback;
      return;
    end if;
    
    /********************************************************* 
       INICIO EL PROCESO DE DESARCIVO DE TRAMITE EN GESTION
    ********************************************************/
    -- Busco la clasificacion del tr�mite.
    begin
        select id_clasif_ipj into v_id_clasif_ipj
        from
          ( select ac.id_tramiteipj_accion, ta.id_clasif_ipj
            from ipj.t_tramitesipj_acciones ac join ipj.t_tipos_accionesipj ta
                on ac.id_tipo_accion = ta.id_tipo_accion
              join ipj.t_tipos_clasif_ipj tc
                on tc.id_clasif_ipj = ta.id_clasif_ipj
            where
              tc.id_ubicacion =  v_row_Tramite.id_ubicacion_origen and
              id_tramite_ipj = p_id_tramite_ipj
            order by ac.id_tramiteipj_accion asc
          ) tmp
          where
            rownum = 1;
    exception
      -- Sino, tomo la primer clasificacion del area
      when NO_DATA_FOUND then
        select id_clasif_ipj into v_id_clasif_ipj
        from ipj.t_tipos_clasif_ipj
        where
          id_ubicacion = v_row_tramite.id_ubicacion_origen and
         rownum = 1;
    end;
    
    SELECT *
      INTO v_row_motivos
      FROM ipj.t_tipos_motivos_vista
     WHERE id_motivo_vista = p_id_motivo_vista;

    IF TRIM(p_observacion) IS NULL THEN
      v_obs_mtv := TRIM(v_row_motivos.n_motivo_vista);
      
      IF v_obs_mtv IS NOT NULL THEN
         v_obs  := v_obs_mtv;
      ELSE
         v_obs := NULL;
      END IF;
    ELSE
      v_obs_mtv := TRIM(p_observacion);
      
      IF TRIM(v_row_motivos.n_motivo_vista) IS NOT NULL THEN
         v_obs  := TRIM(v_row_motivos.n_motivo_vista) ||' - '|| v_obs_mtv;
      ELSE
         v_obs := v_obs_mtv;
      END IF;
    END IF;  

    -- Reabro el tr�mite en el area original, al usuario indicado y en la primer clasifcacion del area.
    update ipj.t_tramitesipj
    set
      id_estado_ult = IPJ.TYPES.c_Estados_Asignado,
      cuil_ult_estado = p_Cuil_Usuario_asigna,
      id_grupo_ult_estado = null,
      id_ubicacion = v_row_tramite.id_ubicacion_origen,
      id_clasif_ipj = v_id_clasif_ipj,
      id_motivo_vista = p_id_motivo_vista,
      observacion = (CASE
                      WHEN v_obs_mtv IS NULL then observacion
                      ELSE SUBSTR( v_obs ||' - ' || to_char(TRUNC(SYSDATE), 'DD/MM/YYYY') || ' - DESARCHIVADO POR: ' ||v_row_usudes.descripcion || chr(13) || observacion, 1, 2000)
                     END)
                     
    where
      Id_Tramite_Ipj = p_Id_Tramite_Ipj;

    -- Actualizo el historial
    IPJ.TRAMITES.SP_GUARDAR_TRAMITES_IPJ_ESTADO(
      o_rdo => o_rdo,
      o_tipo_mensaje => o_tipo_mensaje,
      v_fecha_pase => sysdate,
      v_id_estado => IPJ.TYPES.c_Estados_Asignado,
      v_cuil_usuario => p_Cuil_Usuario_asigna,
      v_id_grupo => null,
      v_observacion => v_row_motivos.n_motivo_vista,
      v_Id_Tramite_Ipj => p_Id_Tramite_Ipj
    );

    -- Al reabrir el tr�mite, se marcan todos los datos en borrador
    update IPJ.T_COMERCIANTES_INS_LEGAL set borrador = 'S'  where Id_Tramite_Ipj = p_Id_Tramite_Ipj;
    update IPJ.T_COMERCIANTES_RUBROS set borrador = 'S' where Id_Tramite_Ipj = p_Id_Tramite_Ipj;
    update IPJ.T_COMERCIANTES_RUBRICA set borrador = 'S' where Id_Tramite_Ipj = p_Id_Tramite_Ipj;
    update IPJ.T_GRAVAMENES_INS_LEGAL set borrador = 'S' where Id_Tramite_Ipj = p_Id_Tramite_Ipj;
    update IPJ.T_MANDATOS_INS_LEGAL set borrador = 'S' where Id_Tramite_Ipj = p_Id_Tramite_Ipj;
    update IPJ.T_MANDATOS_MANDATARIOS set borrador = 'S' where Id_Tramite_Ipj = p_Id_Tramite_Ipj;
    update IPJ.T_fondos_comercio_rubro set borrador = 'S' where Id_Tramite_Ipj = p_Id_Tramite_Ipj;
    update IPJ.T_fondos_comercio_vendedor set borrador = 'S' where Id_Tramite_Ipj = p_Id_Tramite_Ipj;
    update IPJ.T_fondos_comercio_comprador set borrador = 'S' where Id_Tramite_Ipj = p_Id_Tramite_Ipj;
    update IPJ.T_fondos_comercio_ins_legal set borrador = 'S' where Id_Tramite_Ipj = p_Id_Tramite_Ipj;
    update IPJ.T_GRAVAMENES_INS_LEGAL set borrador = 'S' where Id_Tramite_Ipj = p_Id_Tramite_Ipj;
    update IPJ.T_MANDATOS_INS_LEGAL set borrador = 'S' where Id_Tramite_Ipj = p_Id_Tramite_Ipj;
    update IPJ.T_MANDATOS_MANDATARIOS set borrador = 'S' where Id_Tramite_Ipj = p_Id_Tramite_Ipj;

    IPJ.ENTIDAD_PERSJUR.SP_Actualizar_Borrador(
      o_rdo => o_rdo,
      o_tipo_mensaje => o_tipo_mensaje,
      p_Id_Tramite_Ipj => p_Id_Tramite_Ipj,
      p_id_legajo => null,
      p_borrador => 'S'
    );

    --Quito la marca en las observaciones de sistema
    update IPJ.T_ENTIDADES_NOTAS
    set id_nota_sistema = 0
    where
      id_tramite_ipj = p_id_tramite_ipj;
      
    
    /********************************************************* 
       INICIO EL PROCESO DE REAPERTURAS DE TRAMITE EN GESTION
    ********************************************************/
    -- Armo un listado de las acciones del �rea de origen, que no son resoluciones digitales o se guarden en CDD.
    open v_Cursor for 
      select a.*
      from ipj.t_tramitesipj_acciones a join ipj.t_tipos_Accionesipj ta
        on a.id_tipo_accion = ta.id_tipo_accion
      where 
        id_tramite_ipj = p_id_tramite_ipj and
        ta.id_clasif_ipj in (select id_clasif_ipj from ipj.t_tipos_Clasif_ipj c where c.id_ubicacion = v_row_tramite.id_ubicacion_origen) and
        ta.id_prot_dig is null and
        ta.id_tipo_documento_cdd is null ;
    
    loop
      fetch v_Cursor into v_row_acciones;
      EXIT WHEN v_Cursor%NOTFOUND or v_Cursor%NOTFOUND is null;
      
      -- Reabro las acciones, y lo marco en su historial
      SP_Reabrir_Accion (
        o_rdo => o_rdo,
        o_tipo_mensaje => o_tipo_mensaje,
        p_Id_Tramite_Ipj => p_Id_Tramite_Ipj,
        p_id_tramiteipj_accion => v_row_acciones.id_tramiteipj_accion,
        p_cuil_usuario => p_cuil_usuario_asigna,
        p_observacion => 'Reabierto'
      );
    END LOOP;
    close v_Cursor; 
    
    --Si es tr�mite de constituci�n Se blanquea la fecha de baja
    IF ipj.tramites.fc_es_tram_const(v_row_tramite.id_tramite_ipj) > 0 AND v_row_tramite.id_estado_ult in (200, 203, 204) THEN
    
     BEGIN
       SELECT e.id_legajo
         INTO v_id_legajo
         FROM ipj.t_tramitesipj t, ipj.t_entidades e
        WHERE t.id_tramite_ipj = e.id_tramite_ipj
          AND expediente = v_row_tramite.expediente;
     EXCEPTION
       WHEN no_data_found THEN
         NULL;
     END;
     
      UPDATE ipj.t_entidades
         SET baja_temporal = NULL,
             id_estado_entidad = NULL
       WHERE id_tramite_ipj = v_row_tramite.id_tramite_ipj
         AND id_legajo = v_id_legajo;
       
       UPDATE ipj.t_legajos
          SET fecha_baja_entidad = NULL
        WHERE id_legajo = v_id_legajo;  
    
    END IF;
       
    o_rdo := ipj.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    commit;
  EXCEPTION
    WHEN OTHERS THEN
      o_rdo := 'SP_Desarchivo_Digital - ' || To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Desarchivo_Digital;
  
  /**********************************************************
  ***********************************************************
  -------- SOBRECARGA DE PARAMETROS --------------------------------------------
  ***********************************************************
  **********************************************************/
    
  
end Archivo;
/

