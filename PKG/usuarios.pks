create or replace package ipj.Usuarios is

  -- Author  : D24385600
  -- Created : 11/08/2014 11:16:18 a.m.
  -- Purpose : Administracion de Usuarios y Persmisos
  
  PROCEDURE SP_TRAER_Ubicaciones(
    p_Cursor OUT types.cursorType,
    p_ubicacion_original in number, 
    p_clasif_actual in number);
  
  PROCEDURE SP_TRAER_Ubicacion_Usr(
    p_Cursor OUT types.cursorType,
    p_ubicacion_original in number, 
    p_clasif_actual in number,
    p_cuil_usuario in varchar2);
    
   PROCEDURE SP_TRAER_Grupos_Trabajo(
    o_Cursor OUT types.cursorType );
  
  PROCEDURE SP_TRAER_Usuarios(
    v_id_ubicacion in number,
    v_id_grupo_trabajo in number,
    p_Cursor OUT types.cursorType);
    
  PROCEDURE SP_TRAER_Grupos(
    p_Cursor OUT types.cursorType);
    
  PROCEDURE SP_TRAER_Grupos_Usuario(
    o_Cursor OUT types.cursorType,
    p_id_grupo in number);
    
  PROCEDURE SP_TRAER_Usuarios_CIDI(
    v_cuil in varchar2,
    p_Cursor OUT types.cursorType);
  
  PROCEDURE SP_TRAER_UsuariosCuil(
    v_cuil in varchar2,
    p_Cursor OUT types.cursorType);
  
  PROCEDURE SP_TRAER_Roles_Cuil_Area(
    o_Cursor OUT types.cursorType,
    p_cuil in varchar2,
    p_id_ubicacion in number
    );
    
  PROCEDURE SP_GUARDAR_UBICACIONES(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_ubicacion in number,
    p_n_ubicacion in varchar2, 
    p_orden in number, 
    p_observacion in varchar2,
    p_cuil_usuario in varchar2,
    p_id_grupo in number,
    p_codigo_suac in varchar2);
    
  PROCEDURE SP_GUARDAR_GRUPOS_TRABAJO(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_grupo_trabajo in number, 
    p_n_grupo_trabajo in varchar2, 
    p_nro_orden in number);
    
  PROCEDURE SP_GUARDAR_USUARIOS(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_cuil_usuario in varchar2, 
    p_descripcion in varchar2);
    
  PROCEDURE SP_GUARDAR_GRUPOS_TRAB_UBIC(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_grupo_trabajo in number, 
    p_id_ubicacion in number, 
    p_cuil in varchar,
    p_eliminar in number);
  
  PROCEDURE SP_GUARDAR_GRUPOS(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_Id_grupo out number,
    p_id_grupo in number, 
    p_n_grupo in varchar2);

  PROCEDURE SP_GUARDAR_GRUPOS_USUARIO(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_grupo in number, 
    p_cuil_usuario in varchar,
    p_eliminar in number);
  
  PROCEDURE SP_TRAER_Areas_Usuario(
    o_Cursor OUT types.cursorType,
    p_cuil_usuario in varchar2);
end Usuarios;
/

create or replace package body ipj.Usuarios is

  PROCEDURE SP_TRAER_Ubicaciones(
    p_Cursor OUT types.cursorType,
    p_ubicacion_original in number, 
    p_clasif_actual in number)
  IS
  BEGIN
  /**************************************************
    Lista las Ubicaciones o Areas de IPJ
  ***************************************************/
    OPEN p_Cursor FOR
      select distinct u.id_ubicacion, u.n_ubicacion, U.OBSERVACION, U.ORDEN
      from  IPJ.t_ubicaciones u join IPJ.t_tipos_clasif_ipj c
          on u.id_ubicacion = c.id_ubicacion
        left join IPJ.t_workflow w
          on W.ID_CLASIF_IPJ_PROXIMA = C.ID_CLASIF_IPJ
      where
        (nvl(p_ubicacion_original, 0) = 0 or W.ID_UBICACION_ORIGEN = p_ubicacion_original) and
        (p_clasif_actual = 0 or W.ID_CLASIF_IPJ_ACTUAL = p_clasif_actual) ;
    
  END SP_TRAER_Ubicaciones;
  
  PROCEDURE SP_TRAER_Ubicacion_Usr(
    p_Cursor OUT types.cursorType,
    p_ubicacion_original in number, 
    p_clasif_actual in number,
    p_cuil_usuario in varchar2)
  IS
  BEGIN
  /**************************************************
    Lista las Ubicaciones o Areas de IPJ
  ***************************************************/
    OPEN p_Cursor FOR
      select distinct u.id_ubicacion, u.n_ubicacion, U.OBSERVACION, U.ORDEN
      from  IPJ.t_ubicaciones u join IPJ.t_tipos_clasif_ipj c
          on u.id_ubicacion = c.id_ubicacion
        left join IPJ.t_workflow w
          on W.ID_CLASIF_IPJ_PROXIMA = C.ID_CLASIF_IPJ
      where
        ( (nvl(p_ubicacion_original, 0) = 0  and u.id_ubicacion in (select distinct gr.id_ubicacion from ipj.t_grupos_trab_ubicacion gr where gr.cuil = p_cuil_usuario))
          or W.ID_UBICACION_ORIGEN = p_ubicacion_original) and
        (p_clasif_actual = 0 or W.ID_CLASIF_IPJ_ACTUAL = p_clasif_actual) ;
    
  END SP_TRAER_Ubicacion_Usr;
  
  
  PROCEDURE SP_TRAER_Grupos_Trabajo(
    o_Cursor OUT types.cursorType )
  IS
  BEGIN
  /**************************************************
    Lista las Ubicaciones o Areas de IPJ
  ***************************************************/
    OPEN o_Cursor FOR
      select G.ID_GRUPO_TRABAJO, G.N_GRUPO_TRABAJO, G.NRO_ORDEN
      from ipj.T_GRUPOS_TRABAJO g
      order by g.nro_orden, g.id_grupo_trabajo;
    
  END SP_TRAER_Grupos_Trabajo;
  
  
  PROCEDURE SP_TRAER_Usuarios(
    v_id_ubicacion in number,
    v_id_grupo_trabajo in number,
    p_Cursor OUT types.cursorType)
  IS
  BEGIN
  /****************************************************************** 
    Lista todos los usuarios. Puede filtrarse con lo paramentros de la siguiente manera:
      - v_id_ubicacion: Indica el Area a la que debe pertenecer el usuario o todos si es NULL.
      - v_id_grupo_trabajo: Indica el roll del usuario o todos si es NULL
   *******************************************************************/
    OPEN p_Cursor FOR
      select distinct u.cuil_usuario, u.descripcion, substr(u.cuil_usuario, 3, 8) DNI
      from ipj.t_usuarios u  join ipj.t_grupos_trab_ubicacion gu
          on u.cuil_usuario = gu.cuil 
        join ipj.t_grupos_trabajo gt
          on gt.id_grupo_trabajo= gu.id_grupo_trabajo 
      where 
        nvl(U.Habilitado, 'S') = 'S' and u.fecha_baja is null and -- Para estar seguro que esta habilitado
        (nvl(v_id_ubicacion, 0) = 0 or id_ubicacion = v_id_ubicacion) and
        (nvl(v_id_grupo_trabajo, 0) = 0 or gt.id_grupo_trabajo = v_id_grupo_trabajo) 
      order by U.DESCRIPCION;
  END SP_TRAER_Usuarios;
  
  
  PROCEDURE SP_TRAER_Grupos(
    p_Cursor OUT types.cursorType)
  IS
  BEGIN
  /****************************************************************** 
    Lista todos los gupos de usuarios. 
   *******************************************************************/
    OPEN p_Cursor FOR
    select G.ID_GRUPO, G.N_GRUPO
    from ipj.t_grupos g
    order by g.n_grupo;
    
  END SP_TRAER_Grupos;
  
  
  PROCEDURE SP_TRAER_Grupos_Usuario(
    o_Cursor OUT types.cursorType,
    p_id_grupo in number)
  IS
  BEGIN
  /****************************************************************** 
    Lista los usuarios pertenecientes a un grupo 
   *******************************************************************/
    OPEN o_Cursor FOR
      select G.ID_GRUPO, G.CUIL_USUARIO, u.descripcion
      from ipj.t_grupos_usuario g join IPJ.t_usuarios u
        on G.CUIL_USUARIO = U.CUIL_USUARIO
      where
        g.id_grupo = p_id_grupo
      order by u.descripcion;
    
  END SP_TRAER_Grupos_Usuario;
  
  PROCEDURE SP_TRAER_Usuarios_CIDI(
    v_cuil in varchar2,
    p_Cursor OUT types.cursorType)
  IS
  BEGIN
  /**************************************************
    Busca el usuario logueado en CIDI en IPJ
  ***************************************************/
    OPEN p_Cursor FOR
      select distinct u.cuil_usuario, u.descripcion
     -- , gt.n_grupo_trabajo , GT.ID_GRUPO_TRABAJO,
        ,GU.ID_UBICACION, ub.n_ubicacion
      from ipj.t_usuarios u 
        join ipj.t_grupos_trab_ubicacion gu
          on u.cuil_usuario = gu.cuil  
        join ipj.t_grupos_trabajo gt
          on gt.id_grupo_trabajo= gu.id_grupo_trabajo
        join IPJ. t_ubicaciones ub
          on GU.ID_UBICACION = ub.id_ubicacion  
      where
        nvl(U.Habilitado, 'S') = 'S' and
        (v_cuil is null or v_cuil ='' or u.cuil_usuario = v_cuil)
        order by u.descripcion, ub.n_ubicacion;
  END SP_TRAER_Usuarios_CIDI;

  PROCEDURE SP_TRAER_UsuariosCuil(
    v_cuil in varchar2,
    p_Cursor OUT types.cursorType)
  IS
  BEGIN
  /*********************************************************** 
    Lista todos los usuarios. Puede filtrarse por CUIL para buscar 1 solo usuario
   ************************************************************/  
    OPEN p_Cursor FOR
    select distinct u.cuil_usuario, u.descripcion, gt.n_grupo_trabajo Rol, GT.ID_GRUPO_TRABAJO, GT.NRO_ORDEN
    from ipj.t_usuarios u join ipj.t_grupos_trab_ubicacion gu
        on u.cuil_usuario = gu.cuil  
      join ipj.t_grupos_trabajo gt
        on gt.id_grupo_trabajo= gu.id_grupo_trabajo
      join IPJ. t_ubicaciones ub
        on GU.ID_UBICACION = ub.id_ubicacion 
    where  
      nvl(v_cuil, '') is null or 
      u.cuil_usuario = v_cuil 
    order by GT.NRO_ORDEN, u.descripcion ;
  END SP_TRAER_UsuariosCuil;
  
  PROCEDURE SP_TRAER_Roles_Cuil_Area(
    o_Cursor OUT types.cursorType,
    p_cuil in varchar2,
    p_id_ubicacion in number
    )
  IS
  BEGIN
  /*********************************************************** 
    Lista los roles de un usuario para un area.
   ************************************************************/  
    OPEN o_Cursor FOR
      select gt.n_grupo_trabajo, GT.ID_GRUPO_TRABAJO, GT.NRO_ORDEN, ub.id_ubicacion, ub.n_ubicacion
      from ipj.t_grupos_trab_ubicacion gu join ipj.t_grupos_trabajo gt
          on gt.id_grupo_trabajo= gu.id_grupo_trabajo
        join IPJ. t_ubicaciones ub
          on GU.ID_UBICACION = ub.id_ubicacion 
      where  
        gu.cuil = p_cuil and
        gu.id_ubicacion = p_id_ubicacion 
      order by GT.NRO_ORDEN, Ub.N_ubicacion;
      
  END SP_TRAER_Roles_Cuil_Area;
  
  PROCEDURE SP_GUARDAR_UBICACIONES(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_ubicacion in number,
    p_n_ubicacion in varchar2, 
    p_orden in number, 
    p_observacion in varchar2,
    p_cuil_usuario in varchar2,
    p_id_grupo in number,
    p_codigo_suac in varchar2)
  IS
    v_id_ubicacion number;
    v_valid_parametros varchar2(200);
    v_id_grupo number;
  BEGIN
  /********************************************************** 
    Actualiza o inserta los datos de una ubicacion (Area IPJ).
   **********************************************************/
    if nvl(p_n_ubicacion, '') = '' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('NOMBRE_NOT');
    end if;
    
    -- si los parametros no estan OK, nocontinuo
    if v_valid_parametros is not null then
      o_rdo := v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    -- si vienen en 0 los paso a NULL para respetar las FK   
    v_id_grupo := (case when p_id_grupo = 0 then null else p_id_grupo end);
    
   --Si tiene codigo, lo actualizo
    if nvl(p_id_ubicacion, 0) > 0 then
      update IPJ.T_UBICACIONES
      set 
        n_ubicacion = p_n_ubicacion,
        orden = p_orden,
        observacion = p_observacion,
        cuil_usuario = p_cuil_usuario, 
        id_grupo = v_id_grupo,
        codigo_suac = p_codigo_suac
      where
        id_ubicacion = p_id_ubicacion;
   
    else --Si no tiene codigo, lo inserto
      SELECT IPJ.SEQ_UBICACIONES.nextval INTO v_ID_UBICACION FROM dual;
       
      insert into IPJ.t_ubicaciones (id_ubicacion, n_ubicacion, orden, observacion, cuil_usuario,id_grupo,codigo_suac)
      values (v_id_ubicacion, p_n_ubicacion, p_orden, p_observacion, p_cuil_usuario, v_id_grupo, p_codigo_suac);
    end if;
    
    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    --commit;
  exception
     WHEN OTHERS THEN 
      -- rollback;
       o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_GUARDAR_UBICACIONES;


  PROCEDURE SP_GUARDAR_GRUPOS_TRABAJO(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_grupo_trabajo in number, 
    p_n_grupo_trabajo in varchar2, 
    p_nro_orden in number)
  IS
    v_id_grupo_trabajo number;
    v_valid_parametros varchar2(200);
  BEGIN
  /********************************************************** 
    Actualiza o inserta los datos de una ubicacion (Area IPJ).
   **********************************************************/
    if nvl(p_n_grupo_trabajo, '') = '' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('NOMBRE_NOT');
    end if;
    
    -- si los parametros no estan OK, nocontinuo
    if v_valid_parametros is not null then
      o_rdo := v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;
    
   
   --Si tiene codigo, lo actualizo
    if nvl(p_id_grupo_trabajo, 0) > 0 then
      update IPJ.T_GRUPOS_TRABAJO
      set 
        n_grupo_trabajo = p_n_grupo_trabajo,
        nro_orden = p_nro_orden
      where
        id_grupo_trabajo = p_id_grupo_trabajo;
   
    else --Si no tiene codigo, lo inserto
      SELECT IPJ.SEQ_GRUPOS_TRABAJO.nextval INTO v_ID_GRUPO_TRABAJO FROM dual;
       
      insert into IPJ.t_grupos_trabajo (id_grupo_trabajo, n_grupo_trabajo, nro_orden)
      values (v_id_grupo_trabajo, p_n_grupo_trabajo, p_nro_orden);
    end if;
    
    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    --commit;
  exception
     WHEN OTHERS THEN 
       --rollback;
       o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_GUARDAR_GRUPOS_TRABAJO;


  PROCEDURE SP_GUARDAR_USUARIOS(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_cuil_usuario in varchar2, 
    p_descripcion in varchar2)
  IS
    v_id_grupo_trabajo number;
    v_valid_parametros varchar2(200);
    v_mensaje varchar2(200);
  BEGIN
  /********************************************************** 
    Actualiza o inserta los datos de un usuario
    No maneja transacciones, porque lo hace la aplicacion.
   **********************************************************/
    t_comunes.pack_persona_juridica.VerificarCUIT (p_cuil_usuario, v_mensaje);
    if Upper(trim(v_mensaje)) != IPJ.TYPES.C_RESP_OK then
      v_valid_parametros := v_valid_parametros || v_Mensaje || ' .';
    end if;
    if p_descripcion is null then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('NOMBRE_NOT');
    end if;
    
    -- si los parametros no estan OK, nocontinuo
    if v_valid_parametros is not null then
      o_rdo := v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;
    
    
   -- Si existe se actualiza el nombre
    update IPJ.T_USUARIOS
    set 
      descripcion = p_descripcion
    where
      cuil_usuario = p_cuil_usuario;
   
   -- si no existe lo agrego
    if sql%rowcount = 0 then 
      insert into IPJ.t_usuarios (cuil_usuario, descripcion)
      values (p_cuil_usuario, p_descripcion);
    end if;
    
    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  exception
     WHEN OTHERS THEN 
       o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_GUARDAR_USUARIOS;
  
  
  PROCEDURE SP_GUARDAR_GRUPOS_TRAB_UBIC(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_grupo_trabajo in number, 
    p_id_ubicacion in number, 
    p_cuil in varchar,
    p_eliminar in number)
  IS
    v_existe number;
    v_valid_parametros varchar2(200);
  BEGIN
  /*********************************************************** 
    Actualiza los roles de un usuario
    No maneja transacciones, porque lo hace la aplicacion.
   ************************************************************/
    if p_id_grupo_trabajo <= 0 then
      v_valid_parametros := v_valid_parametros || 'Rol Inv�lido. ';
    end if;
    if p_id_ubicacion <= 0 then
      v_valid_parametros := v_valid_parametros || 'Area Inv�lida. ';
    end if;
    if nvl(p_cuil, '') = '' then
      v_valid_parametros := v_valid_parametros || 'Usuario Inv�lido. ';
    end if;
    
    -- si los parametros no estan OK, nocontinuo
    if v_valid_parametros is not null then
      o_rdo := v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;
    
    
    if p_eliminar = 1 then
      delete IPJ.T_GRUPOS_TRAB_UBICACION
      where
        id_grupo_trabajo = p_id_grupo_trabajo and
        id_ubicacion = p_id_ubicacion and
        cuil = p_cuil;
    else
     -- si ya existe no hago nada, sino lo inserto
     select count(*) into v_existe
     from IPJ.T_GRUPOS_TRAB_UBICACION
     where
       id_grupo_trabajo = p_id_grupo_trabajo and
       id_ubicacion = p_id_ubicacion and
       cuil = p_cuil;
   
     -- si no existe lo agrego
      if v_existe = 0 then 
        insert into IPJ.t_grupos_trab_ubicacion (id_grupo_trabajo, id_ubicacion, cuil)
        values (p_id_grupo_trabajo, p_id_ubicacion, p_cuil);
      end if;
    end if;
    
    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  exception
     WHEN OTHERS THEN 
       o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_GUARDAR_GRUPOS_TRAB_UBIC;
  
 
  PROCEDURE SP_GUARDAR_GRUPOS(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_grupo out number,
    p_id_grupo in number, 
    p_n_grupo in varchar2)
  IS
    v_id_grupo number;
    v_valid_parametros varchar2(200);    
  BEGIN
  /********************************************************** 
    Actualiza o inserta los datos de un usuario
    No maneja transacciones, porque lo hace la aplicacion.
   **********************************************************/
   if p_n_grupo is null then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('NOMBRE_NOT');
    end if;
    
    -- si los parametros no estan OK, nocontinuo
    if v_valid_parametros is not null then
      o_rdo := v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;
    
    
   -- Si existe se actualiza el nombre
    if nvl(p_id_grupo, 0) > 0 then
      update IPJ.T_GRUPOS
      set 
        n_Grupo = p_n_grupo
      where
        id_grupo = p_id_grupo;
   
     v_id_grupo := p_id_grupo;
    else-- si no existe lo agrego
      SELECT IPJ.SEQ_GRUPOS.nextval INTO v_ID_GRUPO FROM dual;
     
      insert into IPJ.t_grupos (id_grupo, n_grupo)
      values (v_id_grupo, p_n_grupo);
    end if;
    
    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    o_id_grupo := v_ID_GRUPO;
  exception
     WHEN OTHERS THEN 
       o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_GUARDAR_GRUPOS;
  
  
  PROCEDURE SP_GUARDAR_GRUPOS_USUARIO(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_grupo in number, 
    p_cuil_usuario in varchar,
    p_eliminar in number)
  IS
    v_existe number;
    v_valid_parametros varchar2(200);
  BEGIN
  /********************************************************** 
    Actualiza los usuarios de un grupo
    No maneja transacciones, porque lo hace la aplicacion.
   **********************************************************/
    if p_id_grupo <= 0 then
      v_valid_parametros := v_valid_parametros || 'Grupo Inv�lido. ';
    end if;
    if nvl(p_cuil_usuario, '') = '' then
      v_valid_parametros := v_valid_parametros || 'Usuario Inv�lido. ';
    end if;
    
    -- si los parametros no estan OK, nocontinuo
    if v_valid_parametros is not null then
      o_rdo := v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;
    
    
    if p_eliminar = 1 then
      delete IPJ.T_GRUPOS_USUARIO
      where
        id_grupo = p_id_grupo and
        cuil_usuario = p_cuil_usuario;
    else
     -- si ya existe no hago nada, sino lo inserto
     select count(*) into v_existe
     from IPJ.T_GRUPOS_USUARIO
      where
        id_grupo = p_id_grupo and
        cuil_usuario = p_cuil_usuario;
   
     -- si no existe lo agrego
      if v_existe = 0 then 
        insert into IPJ.t_grupos_usuario (id_grupo, cuil_usuario)
        values (p_id_grupo, p_cuil_usuario);
      end if;
    end if;
    
    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  exception
     WHEN OTHERS THEN 
       o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_GUARDAR_GRUPOS_USUARIO;
  
  PROCEDURE SP_TRAER_Areas_Usuario(
    o_Cursor OUT types.cursorType,
    p_cuil_usuario in varchar2)
  IS
    v_existe_usuario number;
  BEGIN
  /**************************************************
    Lista las Ubicaciones o Areas de IPJ
  ***************************************************/
    OPEN o_Cursor FOR
      select distinct 
        ub.id_ubicacion, 
        ub.n_ubicacion, 
        decode(u.id_ubicacion_preferencia,ub.id_ubicacion,0,to_number(ub.orden)) ORDEN
      from  IPJ.t_Usuarios u join IPJ.t_Grupos_Trab_Ubicacion gtu
          on U.CUIL_USUARIO = GTU.CUIL
        join IPJ.t_ubicaciones ub
          on GTU.ID_UBICACION = ub.id_ubicacion
      where
        u.cuil_usuario = p_cuil_usuario and
         ( gtu.id_ubicacion in (IPJ.Types.c_area_juridico, IPJ.Types.c_area_archivo, IPJ.Types.c_area_rrhh, IPJ.Types.c_area_direccion, IPJ.Types.c_area_sistema, IPJ.Types.c_area_mesaSuac) or
           gtu.id_ubicacion in (select distinct id_ubicacion_origen from ipj.t_workflow))
      order by ORDEN;
      
    -- Veo que el usuario exista en IPJ (no importa si esta o no habilitado)
    select count(1) into v_existe_usuario
    from ipj.t_usuarios
    where
      cuil_usuario = p_cuil_usuario;
    
    -- Si existe, logue su ingreso
    if v_existe_usuario > 0 then
      insert into IPJ.T_LOGS_USUARIOS (fec_event, cuil_usr, ip_pc)
      values( sysdate, p_cuil_usuario, null);
    end if;
    
  END SP_TRAER_Areas_Usuario; 
  
end Usuarios;
/

