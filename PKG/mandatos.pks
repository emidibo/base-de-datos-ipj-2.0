CREATE OR REPLACE PACKAGE IPJ.MANDATOS AS

   FUNCTION FC_LISTAR_PODERES(p_id_mandato_mandatario in number) return varchar2;

  PROCEDURE SP_Traer_Mandato(
    p_Cursor OUT types.cursorType,
    p_id_tramite_ipj in number,
    p_id_tramiteipj_accion in number,
    p_id_tipo_accion in number);
  
  PROCEDURE SP_Traer_Mandato_Mandatarios(
    p_Cursor OUT types.cursorType,
    p_id_tramite_ipj in number,
    p_id_tramiteipj_accion in number,
    p_id_tipo_accion in number);
    
  PROCEDURE SP_Traer_Mandato_InsLegal(
    p_Cursor OUT types.cursorType,
    p_id_tramite_ipj in number,
    p_id_tramiteipj_accion in number,
    p_id_tipo_accion in number);
   
   
 PROCEDURE SP_GUARDAR_MANDATO(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in NUMBER,
    p_id_tramiteipj_accion in NUMBER,
    p_id_tipo_accion in NUMBER,
    p_tipo_persona in NUMBER,
    p_id_integrante in NUMBER,
    p_id_legajo in NUMBER,
    p_fec_inscripcion in varchar2,
    p_matricula in VARCHAR2,
    p_matricula_version in NUMBER,
    p_folio in VARCHAR2,
    p_tomo in VARCHAR2,
    p_ano in NUMBER,
    p_protocolo in VARCHAR2,
    p_letra in varchar2
  );
      
  PROCEDURE SP_GUARDAR_MANDATO_IL(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in Number,
    p_Id_Tramiteipj_Accion in Number,
    p_Id_Tipo_Accion in Number,
    p_Id_Juzgado in Number,
    p_Il_Tipo in Number,
    p_Il_Numero in Number,
    p_Il_Fecha in varchar2,
    p_Titulo in Varchar2,
    p_Descripcion in Varchar2,
    p_Borrador in Varchar2,
    p_fecha in varchar2,
    p_eliminar in number);


   PROCEDURE SP_GUARDAR_MANDATO_MANDATARIO(
     o_rdo out varchar2,
     o_tipo_mensaje out number,
     o_Id_Mandato_Mandatario out number,
     p_Id_Mandato_Mandatario in Number,
     p_Id_Tramite_Ipj in Number,
     p_Id_Tramiteipj_Accion in Number,
     p_Id_Tipo_Accion in Number,
     p_Id_Integrante in Number,
     p_nro_documento in varchar2,
     p_id_sexo in varchar2,
     p_detalle in varchar2,
     p_nombre in varchar2,
     p_apellido in varchar2,
     p_error_dato in varchar2,
     p_pai_cod_pais in varchar2,
     p_id_numero in number,
     p_id_legajo in Number,
     p_Fecha_Alta in varchar2,
     p_Fecha_Baja in varchar2,
     p_Borrador in Varchar2,
     p_n_tipo_documento in varchar2,
     p_eliminar in number); -- 1 elimina
   
   
  PROCEDURE SP_GUARDAR_MANDATARIO_PODER(
     o_rdo out varchar2,
     o_tipo_mensaje out number,
     p_Id_Mandato_Mandatario_Poder in Number,
     p_Id_Tipo_Poder_Mandato in Number,
     p_Id_Mandato_Mandatario in Number,
     p_Id_Integrante in Number,
     p_nro_documento in varchar2,
     p_id_sexo in varchar2,
     p_detalle in varchar2,
     p_nombre in varchar2,
     p_apellido in varchar2,
     p_error_dato in varchar2,
     p_pai_cod_pais in varchar2,
     p_id_numero in number,
     p_id_legajo in number,
     p_id_tramite_ipj in number,
     p_id_tramite_accion in number,
     p_id_integrante_mandato in number,
     p_id_legajo_mandato in number,
     p_n_tipo_documento in varchar2,
     p_eliminar in number); -- 1 elimina
     
     
  PROCEDURE SP_Traer_Mandatario_Poderes(
    p_Cursor OUT types.cursorType,
    p_id_tramite_ipj in number,
    p_id_tramiteipj_accion in number,
    p_id_tipo_accion in number,
    p_id_mandato_mandatario in number);
  
  PROCEDURE SP_Traer_Tipos_Poderes_Mandato(
    p_Cursor OUT types.cursorType,
    p_Id_Mandato_Mandatario in number);
  
  PROCEDURE SP_Guardar_Tipo_Poder_Mandato(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_tipo_poder_mandato OUT number,
    p_id_tipo_poder_mandato in number,
    p_n_tipo_poder_mandato in varchar2);
     
END MANDATOS;
/

create or replace package body ipj.Mandatos is

  FUNCTION FC_LISTAR_PODERES(p_id_mandato_mandatario in number) return varchar2 is
    v_res varchar2(200);
    v_Cursor_Poderes IPJ.TYPES.CURSORTYPE;
    v_Row_Poder_Mandato IPJ.t_tipo_poderes_mandatos%ROWTYPE;
  BEGIN
    OPEN v_Cursor_Poderes FOR
      select TP.*
      from IPJ.t_mandatos_mandatarios_poderes mmp join IPJ.t_tipo_poderes_mandatos tp
        on MMP.ID_TIPO_PODER_MANDATO = TP.ID_TIPO_PODER_MANDATO
      where
        mmp.id_mandato_mandatario = p_id_mandato_mandatario and
        mmp.fecha_baja is null;

    v_res := null;
    LOOP
       fetch v_Cursor_Poderes into v_Row_Poder_Mandato;
       EXIT WHEN v_Cursor_Poderes%NOTFOUND;
       if v_res is null then
          v_res := v_Row_Poder_Mandato.N_TIPO_PODER_MANDATO;
       else
          v_res := v_res || ', ' || v_Row_Poder_Mandato.N_TIPO_PODER_MANDATO;
       end if;
    END LOOP;

   CLOSE v_Cursor_Poderes;

   Return nvl(v_res, '');
  Exception
    When Others Then
      Return '--';
  End FC_LISTAR_PODERES;


  PROCEDURE SP_Traer_Mandato(
    p_Cursor OUT types.cursorType,
    p_id_tramite_ipj in number,
    p_id_tramiteipj_accion in number,
    p_id_tipo_accion in number
    )
  is
    v_id_integrante number;
    v_Tipo_Persona number;
    v_id_legajo number;
    v_existe number;
    v_id_tramite_ipj number;
    v_id_tramiteipj_accion number;
    v_id_tipo_accion number;
    V_TIPO_SEXO varchar2(50);
    v_nro_documento varchar2(15);
    v_detalle varchar2(200);
    v_cuit varchar2(20);
    v_hay_protocolo number;
  BEGIN
    -- busco si la accion usa protocolo, para saber si se incrementa o no la matricula y su versionado
    select nvl(AC.ID_PROTOCOLO, 0) into v_hay_protocolo
    from IPJ.T_TIPOS_ACCIONESIPJ ac
    where id_tipo_accion = p_id_tipo_accion;

    select count(*) into v_existe
    from IPJ.T_Mandatos m
    where
      m.id_tramite_ipj = p_id_tramite_ipj and
      m.id_tramiteipj_accion = p_id_tramiteipj_accion and
      m.id_tipo_accion = p_id_tipo_accion;

    if v_existe > 0 then -- Si ya existe traigo los datos configurados
      OPEN p_Cursor FOR
        select M.ID_TRAMITE_IPJ, M.ID_TRAMITEIPJ_ACCION, M.ID_TIPO_ACCION,
          M.TIPO_PERSONA, M.ID_INTEGRANTE, M.id_legajo,
          M.A�O ano, to_char(M.FEC_INSCRIPCION, 'dd/mm/rrrr') FEC_INSCRIPCION, M.FOLIO,
          trim(TRANSLATE(M.MATRICULA, 'ABCDEF','      ')) MATRICULA,
          trim(TRANSLATE(M.MATRICULA, '0123456789','          ')) Letra,
          M.MATRICULA_VERSION, M.PROTOCOLO, M.TOMO,
          L.Cuit, L.Denominacion_Sia Razon_Social,
          S.TIPO_SEXO, i.nro_documento, (select p.nombre||' '||p.apellido from rcivil.vt_pk_persona p where p.id_sexo = i.id_sexo and p.nro_documento = i.nro_documento and p.pai_cod_pais = i.pai_cod_pais and p.id_numero = i.id_numero) detalle
        from IPJ.T_Mandatos m
          left join IPJ. t_integrantes i
            on m.id_integrante = i.id_integrante
          left join IPJ.t_Legajos L
            on m.id_legajo = L.id_legajo
          left join T_COMUNES.VT_SEXOS s
            on i.id_sexo = s.id_sexo
        where
          m.id_tramite_ipj = p_id_tramite_ipj and
          m.id_tramiteipj_accion = p_id_tramiteipj_accion and
          m.id_tipo_accion = p_id_tipo_accion;

    else -- Si es nuevo, busco los datos y armo un cursor
      begin
        select case nvl(id_integrante, 0) when 0 then 1 else 2 end case, id_integrante, id_legajo
          into v_Tipo_Persona, v_id_integrante, v_id_legajo
        from IPJ.T_TRAMITESIPJ_ACCIONES tAcc
        where
          id_tramite_ipj = p_id_tramite_ipj and
          id_tramiteipj_accion = p_id_tramiteipj_accion and
          id_tipo_accion = p_id_tipo_accion;
      exception
         when NO_DATA_FOUND then
           v_id_integrante := 0;
           v_id_legajo := 0;
      end;

      if nvl(v_id_integrante, 0) = 0 and nvl(v_id_legajo, 0) = 0 then
        --o_rdo := IPJ.VARIOS.MENSAJE_ERROR('PERS_NOT');
        return;
      end if;

      if v_tipo_persona = 2 then --Persona F�sica, busco el ultimo mandato
        begin
          select id_tramite_ipj, id_tramiteipj_accion, id_tipo_accion
            into v_id_tramite_ipj, v_id_tramiteipj_accion, v_id_tipo_accion
          from
            ( select m.*
              from ipj.t_mandatos m
              where
                m.id_integrante = v_id_integrante
              order by m.MATRICULA, m.MATRICULA_VERSION
             ) t
          where
            rownum = 1;
        exception
          when no_data_found then
            v_id_tramite_ipj := 0;
            v_id_tramiteipj_accion := 0;
            v_id_tipo_accion := 0;
        end;

        select S.TIPO_SEXO, i.nro_documento, (select p.nombre||' '||p.apellido from rcivil.vt_pk_persona p where p.id_sexo = i.id_sexo and p.nro_documento = i.nro_documento and p.pai_cod_pais = i.pai_cod_pais and p.id_numero = i.id_numero) detalle
          into V_TIPO_SEXO, v_nro_documento, v_detalle
        from IPJ. t_integrantes i join T_COMUNES.VT_SEXOS s
            on i.id_sexo = s.id_sexo
        where
          i.id_integrante = v_id_integrante;

      else -- Persona Jur�dica
        v_cuit := IPJ.TRAMITES.FC_Cuit_PersJur (v_id_legajo );

        begin
          select id_tramite_ipj, id_tramiteipj_accion, id_tipo_accion
            into v_id_tramite_ipj, v_id_tramiteipj_accion, v_id_tipo_accion
          from
            ( select m.*
              from ipj.t_mandatos m
              where
                m.id_legajo = v_id_legajo
              order by m.MATRICULA, m.MATRICULA_VERSION
             ) t
          where
            rownum = 1;
        exception
          when no_data_found then
            v_id_tramite_ipj := 0;
            v_id_tramiteipj_accion := 0;
            v_id_tipo_accion := 0;
        end;
      end if;

      if v_id_tramite_ipj <> 0 and v_id_tramiteipj_accion <>0 and v_id_tipo_accion <>0 then -- Hay Mandato anterior
        OPEN p_Cursor FOR
          select  p_id_tramite_ipj ID_TRAMITE_IPJ, p_id_tramiteipj_accion ID_TRAMITEIPJ_ACCION, p_id_tipo_accion ID_TIPO_ACCION,
            v_tipo_persona TIPO_PERSONA, v_id_integrante ID_INTEGRANTE, v_id_legajo id_legajo,
            M.A�O ano, to_char(M.FEC_INSCRIPCION, 'dd/mm/rrrr') FEC_INSCRIPCION, M.FOLIO,
            trim(TRANSLATE(M.MATRICULA, 'ABCDEF','      ')) MATRICULA,
            trim(TRANSLATE(M.MATRICULA, '0123456789','          ')) Letra,
            M.PROTOCOLO, M.TOMO,
            l.Cuit, l.denominacion_sia Razon_Social,
            v_tipo_sexo TIPO_SEXO, v_nro_documento nro_documento, v_detalle detalle,
            (case when v_hay_protocolo = 0 then M.MATRICULA_VERSION else M.MATRICULA_VERSION +1end) MATRICULA_VERSION
          from IPJ.T_Mandatos m left join ipj.t_legajos l
            on m.id_legajo = l.id_legajo
          where
            m.id_tramite_ipj = v_id_tramite_ipj and
            m.id_tramiteipj_accion = v_id_tramiteipj_accion and
            m.id_tipo_accion = v_id_tipo_accion;

      else -- NO EXISTE un mandato anterior, traigo la persona o empresa
        if v_tipo_persona = 2 then --Persona F�sica, busco el ultimo mandato
          OPEN p_Cursor FOR
            select p_id_tramite_ipj ID_TRAMITE_IPJ, p_id_tramiteipj_accion ID_TRAMITEIPJ_ACCION, p_id_tipo_accion ID_TIPO_ACCION,
              v_tipo_persona TIPO_PERSONA, i.ID_INTEGRANTE, null id_legajo,
              null ano, null FEC_INSCRIPCION, null FOLIO, null MATRICULA, '' letra, 0 MATRICULA_VERSION, null PROTOCOLO, null TOMO,
              null Cuit, null  Razon_Social,
              v_tipo_sexo TIPO_SEXO, i.nro_documento , (select p.nombre||' '||p.apellido from rcivil.vt_pk_persona p where p.id_sexo = i.id_sexo and p.nro_documento = i.nro_documento and p.pai_cod_pais = i.pai_cod_pais and p.id_numero = i.id_numero) detalle
            from ipj.t_integrantes i
            where
              id_integrante = v_id_integrante;
        else
          OPEN p_Cursor FOR
            select p_id_tramite_ipj ID_TRAMITE_IPJ, p_id_tramiteipj_accion ID_TRAMITEIPJ_ACCION, p_id_tipo_accion ID_TIPO_ACCION,
              v_tipo_persona TIPO_PERSONA, v_id_integrante ID_INTEGRANTE, l.id_legajo,
              null ano, null FEC_INSCRIPCION, null FOLIO, null MATRICULA, '' letra, 0 MATRICULA_VERSION, null PROTOCOLO, null TOMO,
              l.Cuit, l.denominacion_sia Razon_Social,
              v_tipo_sexo TIPO_SEXO, v_nro_documento nro_documento, v_detalle detalle
            from ipj.t_legajos l
            where
              id_legajo = v_id_legajo;
        end if;
      end if;
    end if;

  END SP_Traer_Mandato;


  PROCEDURE SP_Traer_Mandato_Mandatarios(
    p_Cursor OUT types.cursorType,
    p_id_tramite_ipj in number,
    p_id_tramiteipj_accion in number,
    p_id_tipo_accion in number)
  IS
    v_lista_poderes varchar2(500);
    v_id_integrante number;
    v_id_legajo number;
    v_Tipo_Persona number;
    v_cuit varchar2(20);
  BEGIN
    begin
      select case nvl(id_integrante, 0) when 0 then 1 else 2 end case, id_integrante, id_legajo
        into v_Tipo_Persona, v_id_integrante, v_id_legajo
      from IPJ.T_TRAMITESIPJ_ACCIONES tAcc
      where
        id_tramite_ipj = p_id_tramite_ipj and
        id_tramiteipj_accion = p_id_tramiteipj_accion and
        id_tipo_accion = p_id_tipo_accion;
    exception
       when NO_DATA_FOUND then
         v_Tipo_Persona := 0;
         v_id_integrante := 0;
         v_id_legajo := 0;
    end;

    OPEN p_Cursor FOR
      select MM.BORRADOR, L.CUIT, to_char(MM.FECHA_ALTA, 'dd/mm/rrrr') FECHA_ALTA,
        to_char(MM.FECHA_BAJA, 'dd/mm/rrrr') FECHA_BAJA, MM.ID_INTEGRANTE,
        MM.ID_MANDATO_MANDATARIO,MM.ID_TIPO_ACCION, MM.ID_TRAMITE_IPJ, MM.ID_TRAMITEIPJ_ACCION,
        IPJ.MANDATOS.FC_LISTAR_PODERES(MM.ID_MANDATO_MANDATARIO) Lista_Poderes,
        I.DETALLE, i.nro_documento, I.ID_NUMERO, I.ID_SEXO, I.PAI_COD_PAIS,
        L.Denominacion_Sia Razon_Social,
        (case nvl(MM.ID_INTEGRANTE, 0) when 0 then L.cuit else i.nro_documento end) identificacion,
        (case nvl(MM.ID_INTEGRANTE, 0) when 0 then L.Denominacion_Sia else I.DETALLE end) persona,
        L.id_legajo, 1 imprimir,
        (case nvl(MM.ID_INTEGRANTE, 0) when 0 then 2 else 1 end) tipoperosona
      from ipj.t_mandatos m join IPJ.t_mandatos_mandatarios mm
          on m.id_tramite_ipj = mm.id_tramite_ipj and m.id_tramiteipj_accion = mm.id_tramiteipj_accion and m.id_tipo_accion = mm.id_tipo_accion
        left join ipj.t_Legajos L
          on mm.id_legajo = L.id_legajo
        left join IPJ.t_INTEGRANTES i
          on mm.id_integrante = i.id_integrante
      where
       (m.id_tramite_ipj  = p_id_tramite_ipj and
        M.Id_Tramiteipj_Accion = p_id_tramiteipj_accion and
        M.Id_Tipo_Accion = p_id_tipo_accion
       )
       or
       ( mm.borrador = 'N' and
         ( (m.tipo_persona = 2 and m.id_integrante = v_id_integrante)
           or
           (m.tipo_persona = 1 and m.id_legajo = v_id_legajo)
         )
       ) and
      (mm.fecha_baja is null or mm.fecha_baja > sysdate or mm.borrador = 'S') ;

  END SP_Traer_Mandato_Mandatarios;

  PROCEDURE SP_Traer_Mandato_InsLegal(
    p_Cursor OUT types.cursorType,
    p_id_tramite_ipj in number,
    p_id_tramiteipj_accion in number,
    p_id_tipo_accion in number)
  IS
     v_id_integrante number;
    v_id_legajo number;
    v_Tipo_Persona number;
    v_cuit varchar2(20);
  BEGIN
     begin
      select case nvl(id_integrante, 0) when 0 then 1 else 2 end case, id_integrante, id_legajo
        into v_Tipo_Persona, v_id_integrante, v_id_legajo
      from IPJ.T_TRAMITESIPJ_ACCIONES tAcc
      where
        id_tramite_ipj = p_id_tramite_ipj and
        id_tramiteipj_accion = p_id_tramiteipj_accion and
        id_tipo_accion = p_id_tipo_accion;
    exception
       when NO_DATA_FOUND then
         v_Tipo_Persona := 0;
         v_id_integrante := 0;
         v_id_legajo := 0;
    end;

    OPEN p_Cursor FOR
      select
         mil.ID_TRAMITE_IPJ, mil.ID_TRAMITEIPJ_ACCION, mil.ID_TIPO_ACCION,
         mil.ID_JUZGADO,
         mil.IL_TIPO, mil.IL_NUMERO, To_Char(mil.IL_FECHA, 'dd/mm/rrrr') IL_FECHA,
         mil.TITULO, mil.DESCRIPCION, mil.BORRADOR, To_Char(mil.FECHA, 'dd/mm/rrrr') FECHA,
         J.N_JUZGADO, L.N_IL_TIPO
      from ipj.t_mandatos_ins_legal mil join IPJ.t_mandatos m
          on m.id_tramite_ipj = mil.id_tramite_ipj and m.id_tramiteipj_accion = mil.id_tramiteipj_accion and m.id_tipo_accion = mil.id_tipo_accion
        join ipj.t_juzgados j
          on mil.ID_JUZGADO = J.ID_JUZGADO
        join IPJ.T_TIPOS_INS_LEGAL L
          on mil.IL_TIPO = L.IL_TIPO
        left join ipj.t_Legajos L
          on m.id_legajo = L.id_legajo
      where
        (mil.id_tramite_ipj  = p_id_tramite_ipj and
         Mil.Id_Tramiteipj_Accion = p_id_tramiteipj_accion and
         Mil.Id_Tipo_Accion = p_id_tipo_accion
        )
        or
        ( mil.borrador = 'N' and
         ( (m.tipo_persona = 2 and m.id_integrante = v_id_integrante)
           or
           (m.tipo_persona = 1 and m.id_legajo = v_id_legajo)
         )
       )
      order by mil.fecha desc, mil.IL_FECHA desc;

  END SP_Traer_Mandato_InsLegal;


  PROCEDURE SP_GUARDAR_MANDATO(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in NUMBER,
    p_id_tramiteipj_accion in NUMBER,
    p_id_tipo_accion in NUMBER,
    p_tipo_persona in NUMBER,
    p_id_integrante in NUMBER,
    p_id_legajo in NUMBER,
    p_fec_inscripcion in varchar2,
    p_matricula in VARCHAR2,
    p_matricula_version in NUMBER,
    p_folio in VARCHAR2,
    p_tomo in VARCHAR2,
    p_ano in NUMBER,
    p_protocolo in VARCHAR2,
    p_letra in varchar2
  )
  IS
    v_mensaje varchar2(200);
    v_valid_parametros varchar2(500);
    v_matricula_exists varchar2(500);
    v_id_tramiteipj_accion number;
  BEGIN
  /********************************************************
    Guarda los datos de un Mandato.
    Si el Mandato ya existe, los actualiza, en caso contrario los ingresa
    No maneja transaccion, ya que se usa en cascada desde la aplicacion y lo administra el codigo.
  *********************************************************/
    --  VALIDACION DE PARAMETROS
    if p_fec_inscripcion is not null and IPJ.VARIOS.Valida_Fecha(p_fec_inscripcion) = 'N' or To_Date(p_fec_inscripcion, 'dd/mm/rrrr') > sysdate then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT')  || ' (Fecha Inscripci�n)';
    end if;

    if IPJ.VARIOS.TONUMBER(p_ano) is null then
      v_valid_parametros := v_valid_parametros || 'A�o Invalido.';
    end if;

    if p_matricula is not null then
      IPJ.VARIOS.SP_Validar_Matricula(
         o_rdo => v_matricula_exists,
         o_tipo_mensaje => o_tipo_mensaje,
         p_id_tramite_ipj => p_id_tramite_ipj,
         p_id_legajo => null,
         p_id_tramiteipj_accion => p_id_tramiteipj_accion,
         p_cuit => null,
         p_matricula => p_letra || p_matricula);

       if v_matricula_exists != IPJ.TYPES.C_RESP_OK then
         v_valid_parametros :=  v_valid_parametros || v_matricula_exists;
       end if;
    end if;


    if (nvl(p_id_integrante, 0) = 0) and (nvl(p_id_legajo, 0) = 0) then
       v_valid_parametros := v_valid_parametros || 'Persona Inv�lida';
    end if;

    -- Si los paramentros no estan OK, no continuo
    if v_valid_parametros is not null then
      o_rdo := 'Parametros : ' || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    --  CUERPO DEL PROCEDIMIETNO
    -- Actualizo el registro (si existe)
    update ipj.t_mandatos
    set
      fec_inscripcion = to_date(p_fec_inscripcion, 'dd/mm/rrrr'),
      matricula = p_letra || p_matricula,
      matricula_version = p_matricula_version,
      folio = p_folio,
      tomo = p_tomo,
      a�o = p_ano,
      protocolo = p_protocolo
    where
      ID_TRAMITE_IPJ = p_id_tramite_ipj and
      id_tramiteipj_accion = p_ID_TRAMITEIPJ_ACCION and
      id_tipo_accion = p_ID_TIPO_ACCION ;

    -- Si no se actualizo nada, lo inserto.
    -- NO DEBERIA OCURRIR, VER SI INSERTO O MUESTRO ERROR
    if sql%rowcount = 0 then
      INSERT INTO IPJ.T_MANDATOS
        ( ID_TRAMITE_IPJ, ID_TRAMITEIPJ_ACCION, ID_TIPO_ACCION, TIPO_PERSONA,
        ID_INTEGRANTE, id_legajo, FEC_INSCRIPCION, MATRICULA,
        MATRICULA_VERSION, FOLIO, TOMO, A�O, PROTOCOLO)
      values
      ( p_ID_TRAMITE_IPJ, p_ID_TRAMITEIPJ_ACCION, p_ID_TIPO_ACCION, p_TIPO_PERSONA,
        p_ID_INTEGRANTE, p_id_legajo, to_date(p_FEC_INSCRIPCION, 'dd/mm/rrrr'), p_letra || p_MATRICULA,
        p_MATRICULA_VERSION, p_FOLIO, p_TOMO, p_ANO, p_PROTOCOLO);
    end if;

    IPJ.VARIOS.SP_Actualizar_Protocolo(
      o_rdo => o_rdo,
      o_tipo_mensaje => o_tipo_mensaje,
      p_id_tramite_ipj => p_id_tramite_ipj,
      p_id_legajo => null,
      p_id_tramiteipj_accion => p_id_tramiteipj_accion ,
      p_matricula => p_letra || p_matricula);

    if o_rdo != IPJ.TYPES.C_RESP_OK then
      o_rdo := 'SP_Actualizar_Protocolo ' || o_rdo;
      return;
    end if;

    --Una vez creada la entidad, marco la accion como EN PROCESO
    update ipj.t_tramitesipj_acciones
    set id_estado =3
    where
      id_tramite_ipj = p_id_tramite_ipj and
      id_tramiteipj_accion = p_id_tramiteipj_accion;

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     WHEN OTHERS THEN
       o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_GUARDAR_MANDATO;


  PROCEDURE SP_GUARDAR_MANDATO_IL(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in Number,
    p_Id_Tramiteipj_Accion in Number,
    p_Id_Tipo_Accion in Number,
    p_Id_Juzgado in Number,
    p_Il_Tipo in Number,
    p_Il_Numero in Number,
    p_Il_Fecha in varchar2,
    p_Titulo in Varchar2,
    p_Descripcion in Varchar2,
    p_Borrador in Varchar2,
    p_fecha in varchar2,
    p_eliminar in number)
  IS
    v_valid_parametros varchar2(500);
  BEGIN
    /*************************************************************
      Guarda los instrumentos legales asociados a un Mandato.
      No maneja transaccion, ya que se usa en cascada desde la aplicacion y lo administra el codigo.
  *************************************************************/
  -- VALIDACION DE PARAMETROS
    if IPJ.VARIOS.Valida_Fecha(p_il_fecha) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT' || '(Fecha Ins. Legal)');
    end if;
    if IPJ.VARIOS.Valida_Fecha(p_fecha) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT' || '(Fecha Insc.)');
    end if;
    if IPJ.VARIOS.Valida_Tipo_Ins_Legal(nvl(p_il_tipo, 0)) = 0 then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('INS_LEGAL_NOT');
    end if;

    if v_valid_parametros is not null then
      o_rdo := 'Parametros: '  || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    -- CUERPO PROCEDIMIENTO
    if p_eliminar = 1 then
      delete ipj.t_mandatos_ins_legal
      where
        id_tramite_ipj = p_id_tramite_ipj and
        id_tramiteipj_accion = p_id_tramiteipj_accion and
        id_tipo_accion = p_id_tipo_accion and
        id_juzgado = p_id_juzgado and
        il_tipo = p_il_tipo and
        il_numero = p_il_numero and
        borrador = 'S';

    else
      -- Si existe un registro igual no hago nada
      update ipj.t_mandatos_ins_legal
      set
         titulo = p_TITULO,
         descripcion = p_DESCRIPCION,
         fecha = to_date(p_fecha, 'dd/mm/rrrr')
      where
        id_tramite_ipj = p_id_tramite_ipj and
        id_tramiteipj_accion = p_id_tramiteipj_accion and
        id_tipo_accion = p_id_tipo_accion and
        id_juzgado = p_id_juzgado and
        il_tipo = p_il_tipo and
        il_numero = p_il_numero;

      if sql%rowcount = 0 then
        INSERT INTO IPJ.T_MANDATOS_INS_LEGAL
          ( ID_TRAMITE_IPJ, ID_TRAMITEIPJ_ACCION, ID_TIPO_ACCION, ID_JUZGADO,
            IL_TIPO, IL_NUMERO, IL_FECHA, TITULO, DESCRIPCION,  BORRADOR, FECHA)
        VALUES
          ( p_ID_TRAMITE_IPJ, p_ID_TRAMITEIPJ_ACCION, p_ID_TIPO_ACCION, p_ID_JUZGADO,
            p_IL_TIPO, p_IL_NUMERO, to_date(p_IL_FECHA, 'dd/mm/rrrr'), p_TITULO, p_DESCRIPCION,  p_BORRADOR, to_date(p_FECHA, 'dd/mm/rrrr'));
      end if;
    end if;

    o_rdo := 'OK';
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     WHEN OTHERS THEN
       o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_GUARDAR_MANDATO_IL;


  PROCEDURE SP_GUARDAR_MANDATO_MANDATARIO(
     o_rdo out varchar2,
     o_tipo_mensaje out number,
     o_Id_Mandato_Mandatario out number,
     p_Id_Mandato_Mandatario in Number,
     p_Id_Tramite_Ipj in Number,
     p_Id_Tramiteipj_Accion in Number,
     p_Id_Tipo_Accion in Number,
     p_Id_Integrante in Number,
     p_nro_documento in varchar2,
     p_id_sexo in varchar2,
     p_detalle in varchar2,
     p_nombre in varchar2,
     p_apellido in varchar2,
     p_error_dato in varchar2,
     p_pai_cod_pais in varchar2,
     p_id_numero in number,
     p_id_legajo in Number,
     p_Fecha_Alta in varchar2,
     p_Fecha_Baja in varchar2,
     p_Borrador in Varchar2,
     p_n_tipo_documento in varchar2,
     p_eliminar in number) -- 1 elimina
  IS
    v_valid_parametros varchar2(200);
    v_bloque varchar2(100);
    v_ID_MANDATO_MANDATARIO number;
    v_id_integrante number;
    v_id_legajo number;
  BEGIN
  /***********************************************************
    Guarda los mandatarios asociados a un mandato
    No maneja transaccion, ya que se usa en cascada desde la aplicacion y lo administra el codigo.
  ************************************************************/
    -- VALIDACION DE PARAMETROS
    v_bloque := 'Par�metros: ';
    if IPJ.VARIOS.Valida_Fecha(p_Fecha_Alta) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT');
    end if;
    if p_Fecha_Baja is not null and IPJ.VARIOS.Valida_Fecha(p_Fecha_Baja) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT');
    end if;
    if nvl(p_Id_Integrante, 0) = 0 and p_nro_documento is null and p_id_legajo is null then
      v_valid_parametros := v_valid_parametros || 'Persona Inv�lida';
    end if;

    -- si los parametros no estan OK, no continuo
    if v_valid_parametros is not null then
      o_rdo := v_bloque || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    -- CUERPO DEL PROCEDIMIENTO
    if p_eliminar = 1 then
      -- SI es borrador lo elimino, sino le asigno fecha de baja
      if p_borrador = 'S' then
        -- Elimino los poderes del mandatario y luego el mandatario
        delete ipj.t_mandatos_mandatarios_poderes
        where
          Id_Mandato_Mandatario = p_Id_Mandato_Mandatario;

        delete ipj.t_mandatos_mandatarios
        where
          Id_Mandato_Mandatario = p_Id_Mandato_Mandatario and
          borrador = 'S';
      else
        update ipj.t_mandatos_mandatarios_poderes
        set fecha_baja = to_date(sysdate, 'dd/mm/rrrr')
        where
          Id_Mandato_Mandatario = p_Id_Mandato_Mandatario;

        update ipj.t_mandatos_mandatarios
        set fecha_baja = to_date(sysdate, 'dd/mm/rrrr')
        where
          Id_Mandato_Mandatario = p_Id_Mandato_Mandatario and
          borrador = 'N';
      end if;
      o_Id_Mandato_Mandatario :=  p_ID_MANDATO_MANDATARIO;

    else
      -- Si el id_integrante no viene, lo cargo para usar
      if nvl(p_id_integrante, 0) = 0 and p_id_legajo = 0 and p_nro_documento is not null then
        IPJ.ENTIDAD_PERSJUR.SP_GUARDAR_INTEGRANTES(
            o_rdo => o_rdo,
            o_tipo_mensaje =>  o_tipo_mensaje,
            o_id_integrante => v_id_integrante,
            p_id_sexo => p_id_sexo,
            p_nro_documento => p_nro_documento,
            p_pai_cod_pais => p_pai_cod_pais,
            p_id_numero => p_id_numero,
            p_cuil => null,
            p_detalle => p_detalle,
            p_Nombre => p_nombre,
            p_Apellido => p_apellido,
            p_error_dato => p_error_dato,
            p_n_tipo_documento => p_n_tipo_documento);

          if o_rdo <> IPJ.TYPES.C_RESP_OK then
            return;
          end if;
      else
        if p_error_dato = 'S' then
          update ipj.t_integrantes
          set
            error_dato = p_error_dato,
            detalle = p_detalle
          where
            id_integrante = p_id_integrante;
        end if;
        v_id_integrante := p_id_integrante;
      end if;



      -- Actualizo la fecha de baja
      update ipj.t_mandatos_mandatarios
      set
        fecha_alta = to_date(p_Fecha_Alta, 'dd/mm/rrrr'),
        fecha_baja = case p_Fecha_Baja
                                when null then null
                                else to_date(p_Fecha_Baja, 'dd/mm/rrrr')
                              end
      where
        Id_Mandato_Mandatario = p_id_mandato_mandatario;

      -- si no existe, lo agrego
      if sql%rowcount = 0 then
        SELECT IPJ.SEQ_MANDATOS_MANDATARIOS.nextval INTO v_ID_MANDATO_MANDATARIO FROM dual;

        -- Los 0 se cargan como NULL para respetar las claves foraneas
        if v_id_integrante = 0 then
          v_id_integrante := null;
        end if;

        if p_id_legajo = 0 then
          v_id_legajo := null;
        else
          v_id_legajo := p_id_legajo;
        end if;

        INSERT INTO IPJ.T_MANDATOS_MANDATARIOS
          ( ID_MANDATO_MANDATARIO, ID_TRAMITE_IPJ, ID_TRAMITEIPJ_ACCION, ID_TIPO_ACCION,
            ID_INTEGRANTE, FECHA_ALTA, FECHA_BAJA, BORRADOR, ID_LEGAJO)
        VALUES
          ( v_ID_MANDATO_MANDATARIO, p_ID_TRAMITE_IPJ, p_ID_TRAMITEIPJ_ACCION, p_ID_TIPO_ACCION,
            v_id_integrante, to_date(p_FECHA_ALTA, 'dd/mm/rrrr'), null, 'S', v_id_legajo);

        o_Id_Mandato_Mandatario :=  v_ID_MANDATO_MANDATARIO;
      else
        o_Id_Mandato_Mandatario :=  p_ID_MANDATO_MANDATARIO;
      end if;
    end if;

    if o_rdo is null then
      o_rdo := 'OK';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    end if;
  EXCEPTION
    WHEN OTHERS THEN
      o_rdo := v_bloque || To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_GUARDAR_MANDATO_MANDATARIO;


  PROCEDURE SP_GUARDAR_MANDATARIO_PODER(
     o_rdo out varchar2,
     o_tipo_mensaje out number,
     p_Id_Mandato_Mandatario_Poder in Number,
     p_Id_Tipo_Poder_Mandato in Number,
     p_Id_Mandato_Mandatario in Number,
     p_Id_Integrante in Number,
     p_nro_documento in varchar2,
     p_id_sexo in varchar2,
     p_detalle in varchar2,
     p_nombre in varchar2,
     p_apellido in varchar2,
     p_error_dato in varchar2,
     p_pai_cod_pais in varchar2,
     p_id_numero in number,
     p_id_legajo in number,
     p_id_tramite_ipj in number,
     p_id_tramite_accion in number,
     p_id_integrante_mandato in number,
     p_id_legajo_mandato in number,
     p_n_tipo_documento in varchar2,
     p_eliminar in number) -- 1 elimina
  IS
    v_valid_parametros varchar2(200);
    v_bloque varchar2(100);
    v_Id_Mandato_Mandatario_Poder number;
    v_id_integrante number;
    v_Id_Mandato_Mandatario number;
  BEGIN
  /***********************************************************
    Guarda los mandatarios asociados a un mandato
    No maneja transaccion, ya que se usa en cascada desde la aplicacion y lo administra el codigo.
  ************************************************************/
    -- VALIDACION DE PARAMETROS
    v_bloque := 'Par�metros: ';
    if nvl(p_Id_Mandato_Mandatario, 0) = 0 and p_id_legajo = 0 and  p_nro_documento is null then
      v_valid_parametros := v_valid_parametros || 'Persona Inv�lida';
    end if;

    -- si los parametros no estan OK, no continuo
    if v_valid_parametros is not null then
      o_rdo := v_bloque || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    -- CUERPO DEL PROCEDIMIENTO
    if p_eliminar = 1 then
      -- En lugar de eliminar, le asigno fecha de baja
      update ipj.t_mandatos_mandatarios_poderes
      set
        fecha_baja = to_date(sysdate, 'dd/mm/rrrr')
      where
        Id_Mandato_Mandatario_Poder = p_Id_Mandato_Mandatario_Poder;

    else
      -- Si no viene el id_mandatario, lo busco
      if nvl(p_ID_MANDATO_MANDATARIO, 0) = 0  then
        if p_id_legajo = 0 and p_Id_Integrante = 0 then
          IPJ.ENTIDAD_PERSJUR.SP_GUARDAR_INTEGRANTES(
            o_rdo => o_rdo,
            o_tipo_mensaje =>  o_tipo_mensaje,
            o_id_integrante => v_id_integrante,
            p_id_sexo => p_id_sexo,
            p_nro_documento => p_nro_documento,
            p_pai_cod_pais => p_pai_cod_pais,
            p_id_numero => p_id_numero,
            p_cuil => null,
            p_detalle => p_detalle,
            p_Nombre => p_nombre,
            p_Apellido => p_apellido,
            p_error_Dato => p_error_Dato,
            p_n_tipo_documento => p_n_tipo_documento);

          if o_rdo <> IPJ.TYPES.C_RESP_OK then
            return;
          end if;
        else
          if p_error_dato = 'S' then
            update ipj.t_integrantes
            set
              error_dato = p_error_dato,
              detalle = p_detalle
            where
              id_integrante = p_id_integrante;
          end if;
          v_id_integrante := p_id_integrante;
        end if;

        -- Busco si el integrante o la empresa ya esta cargado como mandatario
        begin
          select Id_Mandato_Mandatario into v_Id_Mandato_Mandatario
          from IPJ.T_MANDATOS_MANDATARIOS mm join IPJ.T_mandatos m
              on MM.ID_TRAMITE_IPJ = M.ID_TRAMITE_IPJ and MM.ID_TRAMITEIPJ_ACCION = M.ID_TRAMITEIPJ_ACCION and M.ID_TIPO_ACCION = M.ID_TIPO_ACCION
          where
            m.id_tramite_ipj = p_id_tramite_ipj and m.id_tramiteipj_accion = p_id_tramite_accion and
            --( (nvl(p_id_legajo_mandato, 0) = 0 and M.ID_INTEGRANTE = p_id_integrante_mandato) or
            --  (nvl(p_id_legajo_mandato, 0) <> 0 and L.id_legajo = p_id_legajo_mandato)
            --) and
            nvl(mm.id_integrante, 0) = nvl(v_id_integrante, 0) and
            nvl(mm.id_legajo, 0) = nvl(p_id_legajo, 0);
        exception
          when NO_DATA_FOUND then
            v_Id_Mandato_Mandatario := -1;
        end;

      else
        v_Id_Mandato_Mandatario := p_Id_Mandato_Mandatario;
      end if;

      -- si no existe, lo agrego
      if nvl(p_Id_Mandato_Mandatario_Poder, 0) = 0 then
        SELECT IPJ.SEQ_MANDATOS_MAND_PODERES.nextval INTO v_Id_Mandato_Mandatario_Poder FROM dual;

        INSERT INTO IPJ.T_MANDATOS_MANDATARIOS_PODERES
          (ID_MANDATO_MANDATARIO_PODER, ID_TIPO_PODER_MANDATO, ID_MANDATO_MANDATARIO,
           FECHA_ALTA, FECHA_BAJA)
        VALUES
           (v_ID_MANDATO_MANDATARIO_PODER, p_ID_TIPO_PODER_MANDATO, v_ID_MANDATO_MANDATARIO,
           to_date(sysdate, 'dd/mm/rrrr'), null);
      end if;
    end if;

    if o_rdo is null then
      o_rdo := 'OK';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    end if;
  EXCEPTION
    WHEN OTHERS THEN
      o_rdo := v_bloque || To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_GUARDAR_MANDATARIO_PODER;


  PROCEDURE SP_Traer_Mandatario_Poderes(
    p_Cursor OUT types.cursorType,
    p_id_tramite_ipj in number,
    p_id_tramiteipj_accion in number,
    p_id_tipo_accion in number,
    p_id_mandato_mandatario in number)
  IS
    v_id_integrante number;
    v_id_legajo number;
    v_Tipo_Persona number;
    v_cuit varchar2(20);
  BEGIN
     begin
     -- Busco la persona mandante
      select case nvl(id_integrante, 0) when 0 then 1 else 2 end case, id_integrante, id_legajo
        into v_Tipo_Persona, v_id_integrante, v_id_legajo
      from IPJ.T_TRAMITESIPJ_ACCIONES tAcc
      where
        id_tramite_ipj = p_id_tramite_ipj and
        id_tramiteipj_accion = p_id_tramiteipj_accion and
        id_tipo_accion = p_id_tipo_accion;
    exception
       when NO_DATA_FOUND then
         v_Tipo_Persona := 0;
         v_id_integrante := 0;
         v_id_legajo := 0;
    end;

    OPEN p_Cursor FOR
      select  MMP.ID_MANDATO_MANDATARIO_PODER,
        MMP.ID_TIPO_PODER_MANDATO, to_char(MMP.FECHA_ALTA, 'dd/mm/rrrr') FECHA_ALTA,
        to_char(MMP.FECHA_BAJA, 'dd/mm/rrrr') FECHA_BAJA,
        TP.N_TIPO_PODER_MANDATO, MMP.ID_MANDATO_MANDATARIO,
        L.cuit, MM.ID_LEGAJO,
        I.ID_INTEGRANTE, I.DETALLE, I.ID_NUMERO, I.ID_SEXO,I.NRO_DOCUMENTO, I.PAI_COD_PAIS,
        M.ID_TRAMITE_IPJ, M.ID_TRAMITEIPJ_ACCION, M.ID_INTEGRANTE id_integrante_mandato,
        M.ID_TIPO_ACCION
      from IPJ.t_mandatos_mandatarios_poderes mmp join IPJ.t_Tipo_Poderes_Mandatos tp
          on MMP.ID_TIPO_PODER_MANDATO = TP.ID_TIPO_PODER_MANDATO
        join IPJ.t_mandatos_mandatarios mm
          on MMP.ID_MANDATO_MANDATARIO = MM.ID_MANDATO_MANDATARIO
        join IPJ.t_mandatos m
          on M.ID_TRAMITE_IPJ = MM.ID_TRAMITE_IPJ and M.ID_TRAMITEIPJ_ACCION = MM.ID_TRAMITEIPJ_ACCION
        left join ipj.t_legajos L
          on mm.id_legajo = L.id_legajo
        left join IPJ.T_Integrantes i
          on MM.ID_INTEGRANTE = i.id_integrante
      where
         ( (m.id_tramite_ipj  = p_id_tramite_ipj)  or
           ( mm.borrador = 'N' and
              ( (m.tipo_persona = 2 and m.id_integrante = v_id_integrante)
                or
                (m.tipo_persona = 1 and m.id_legajo = v_id_legajo)
              )
           )
        ) and
        mmp.id_mandato_mandatario = p_id_mandato_mandatario and
        mmp.fecha_baja is null;

  END SP_Traer_Mandatario_Poderes;


  PROCEDURE SP_Traer_Tipos_Poderes_Mandato(
    p_Cursor OUT types.cursorType,
    p_Id_Mandato_Mandatario in number)
  IS
  BEGIN

    OPEN p_Cursor FOR
      select TPM.ID_TIPO_PODER_MANDATO, TPM.N_TIPO_PODER_MANDATO
      from IPJ.t_tipo_poderes_mandatos tpm
      where
       TPM.ID_TIPO_PODER_MANDATO not in
         ( select distinct id_tipo_poder_mandato
           from IPJ.T_Mandatos_Mandatarios_Poderes
           where
             Id_Mandato_Mandatario = p_Id_Mandato_Mandatario and
             fecha_baja is null
          );

  END SP_Traer_Tipos_Poderes_Mandato;

  PROCEDURE SP_Guardar_Tipo_Poder_Mandato(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_tipo_poder_mandato OUT number,
    p_id_tipo_poder_mandato in number,
    p_n_tipo_poder_mandato in varchar2)
  IS
  v_valid_parametros varchar(2000);
  /**************************************************
    Guarda o modifica un Tipo de Sindico
  ***************************************************/
  BEGIN
    -- Valido que venga Descripcion
    if p_n_tipo_poder_mandato is null then
      v_valid_parametros := v_valid_parametros || 'Nombre Inv�lido';
    end if;

    if v_valid_parametros is not null then
      o_rdo := 'Parametros : ' || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    --Si estan bien los datos, actualizo, si no hay registro lo agrege y se devuelve el valor insertado.
    update IPJ.T_TIPO_PODERES_MANDATOS
    set
      n_tipo_poder_mandato = p_n_tipo_poder_mandato
    where
      id_tipo_poder_mandato = p_id_tipo_poder_mandato;

    if sql%rowcount = 0 then
      select max(id_tipo_poder_mandato) +1 into o_id_tipo_poder_mandato
      from IPJ.T_TIPO_PODERES_MANDATOS;

      insert into IPJ.T_TIPO_PODERES_MANDATOS (id_tipo_poder_mandato, n_tipo_poder_mandato)
      values (o_id_tipo_poder_mandato, p_n_tipo_poder_mandato);

    else
      o_id_tipo_poder_mandato := p_id_tipo_poder_mandato;
    end if;

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     when OTHERS then
        o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Guardar_Tipo_Poder_Mandato;

END Mandatos;
/

