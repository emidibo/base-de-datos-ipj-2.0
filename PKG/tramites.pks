create or replace package ipj.Tramites is

  -- Author  : D24385600
  -- Created : 11/08/2014 09:44:39 a.m.
  -- Purpose : Administraci�n de Tr�mites IPJ

  -- Public type declarations
  --type <TypeName> is <Datatype>;

  -- Public constant declarations
  --<ConstantName> constant <Datatype> := <Value>;

  -- Public variable declarations
  --<VariableName> <Datatype>;

  -- Public function and procedure declarations
  FUNCTION FC_Datos_Adic_Firma (p_id_tramite_ipj in number) return varchar2;
  FUNCTION FC_Quitar_Ocultos (p_sticker varchar2) return varchar2;
  FUNCTION FC_Buscar_Fecha_Inf (p_id_tramite_ipj number, p_id_tramiteipj_accion number, p_id_tipo_accion in number ) return date;
  FUNCTION FC_Buscar_Historial_Acc (p_id_tramite_ipj number, p_id_tramiteipj_accion number,
    p_Ascendente char, p_estados_in varchar2, p_estados_out varchar2) return number;
  FUNCTION FC_Es_Tram_Digital (p_id_tramite_ipj number) return number ;
  FUNCTION FC_EXPEDIENTE_TRAMITE_IPJ (p_id_tramite_ipj IN number) return varchar2;
  FUNCTION FC_Ultima_Fecha_Tramite (p_id_tramite number) return varchar2;
  FUNCTION FC_Ultima_Fecha_Accion (p_id_tramite number, p_id_tramiteipj_accion number) return date;
  FUNCTION FC_Primera_Fecha_Accion (p_id_tramite number, p_id_tramiteipj_accion number) return date;
  FUNCTION FC_Razon_Social (p_cuit varchar2) return varchar2;
  FUNCTION FC_Cuit_PersJur (p_id_legajo number) return varchar2;
  FUNCTION FC_Acciones_Abiertas (p_id_tramite_ipj number) return number;
  FUNCTION FC_Acciones_Cerradas (p_id_tramite_ipj number) return number;
  FUNCTION FC_Acciones_Observadas (p_id_tramite_ipj number) return number;
  FUNCTION FC_Control_Tramite (p_id_tramite_ipj number) return number;
  FUNCTION FC_Fecha_Tramite(p_id_legajo number, p_matricula_version number) return varchar2;
  FUNCTION FC_Usr_Accion_Tramite(p_id_legajo number, p_matricula_version number) return varchar2;
  FUNCTION FC_N_Subtipo_Suac(p_N_Tiposuac IN VARCHAR2, p_Id_Tramite IN NUMBER) RETURN VARCHAR2;
  FUNCTION FC_Es_Tram_Const (p_id_tramite_ipj number) return number;
  FUNCTION FC_Es_Tram_Transf (p_id_tramite_ipj number) return number;

  PROCEDURE SP_Traer_TramitesIPJ_Admin(
    v_Cuil in varchar2,
    v_id_clasif in number,
    p_Cursor OUT types.cursorType);

  PROCEDURE SP_Traer_TramitesIPJ_Tecnico(
    v_Cuil in varchar2,
    v_id_estado in number,
    p_id_clasif in number,
    p_cuil_logueado in varchar2,
    p_id_ubicacion in number,
    p_Cursor OUT types.cursorType);

  PROCEDURE SP_Traer_TramitesIPJ_Juridico(
    v_id_estado in number,
    p_Cursor OUT types.cursorType);

  PROCEDURE SP_Traer_TramitesIPJ_RRHH(
    v_id_estado in number,
    p_Cursor OUT types.cursorType);

 PROCEDURE SP_Traer_TramitesIPJ_Direccion(
    v_id_estado in number,
    p_Cursor OUT types.cursorType);

  PROCEDURE SP_Traer_TramitesIPJ_Archivo(
    v_id_estado in number,
    p_Cursor OUT types.cursorType);

  PROCEDURE SP_Traer_TramitesIPJ_MesaSUAC(
    v_id_estado in number,
    p_Cursor OUT types.cursorType);

  PROCEDURE SP_Traer_TramitesIPJ_Hist(
    p_Cuil in varchar2,
    p_id_estado in number,
    p_tipo_tramite in number,
    p_fecha_desde varchar2,
    p_fecha_hasta varchar2,
    o_Cursor OUT types.cursorType);


  PROCEDURE SP_Traer_TramitesIPJ_ID(
    o_Cursor OUT types.cursorType,
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_tramite_ipj out number,
    p_id_tramite_suac in varchar2,
    p_id_tramite_ipj in number,
    p_cuil_usuario in varchar2 );

  PROCEDURE SP_Traer_TramitesIPJ_ID(
    p_id_tramite_suac in varchar2,
    p_id_tramite_ipj in number,
    o_Cursor OUT types.cursorType,
    p_id_tramiteipj_accion in number);


  PROCEDURE SP_Traer_TramitesIPJ_SRL(
    p_Cursor OUT types.cursorType);


  PROCEDURE SP_GUARDAR_TRAMITE(
    o_id_tramite_ipj out number,
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    v_id_tramite_ipj in number,
    v_id_tramite IN varchar2,
    v_id_Clasif_IPJ IN NUMBER,
    v_id_Grupo IN number,
    v_cuil_ult_estado in varchar2,
    v_id_estado in number,
    v_cuil_creador in varchar2,
    v_observacion IN varchar2,
    v_id_Ubicacion in number,
    v_urgente in varchar2,
    p_sticker in varchar2,
    p_expediente in varchar2,
    p_simple_tramite in varchar2);


  PROCEDURE SP_GUARDAR_TRAMITES_IPJ_ESTADO(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    v_fecha_pase in date,
    v_id_estado in number,
    v_cuil_usuario in varchar2,
    v_id_grupo in number,
    v_observacion in varchar2,
    v_id_tramite_ipj in number);

  PROCEDURE SP_GUARDAR_TRAMITES_ACC_ESTADO(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_fecha_pase in date,
    p_id_estado in number,
    p_cuil_usuario in varchar2,
    p_observacion in varchar2,
    p_id_tramite_ipj in number,
    p_id_tramite_Accion in number,
    p_id_tipo_accion in number,
    p_id_documento in number,
    p_n_documento in varchar2);


  PROCEDURE SP_Traer_Pendientes(
    o_Cursor OUT types.cursorType,
    p_fecha_desde in varchar2,
    p_fecha_hasta in varchar2,
    p_sticker in varchar2,
    p_expediente in varchar2,
    p_asunto in varchar2
    );

  PROCEDURE SP_Traer_TramitesIPJ_Accion(
    p_id_tramiteipj_accion in number,
    p_Cursor OUT types.cursorType);

  PROCEDURE SP_Traer_Tramite_Suac(
    p_sticker in varchar2,
    p_expediente in varchar2,
    p_Cursor OUT types.cursorType);

  PROCEDURE SP_Traer_TramitesIPJ_Elab(
    p_Cuil in varchar2,
    p_tipo_tramite in number,
    p_fecha_desde varchar2,
    p_fecha_hasta varchar2,
    p_id_estado number,
    o_Cursor OUT types.cursorType);


  PROCEDURE SP_Traer_TramitesIPJ_Curs(
    p_Cuil in varchar2,
    p_tipo_tramite in number,
    p_fecha_desde varchar2,
    p_fecha_hasta varchar2,
    o_Cursor OUT types.cursorType);


  PROCEDURE SP_Buscar_Razon_Social(
    o_Cursor out IPJ.TYPES.CURSORTYPE,
    p_Cuit in varchar2,
    p_id_legajo in number,
    p_razon_social in varchar2,
    p_matricula in varchar2,
    p_ficha in varchar2,
    p_registro in number,
    p_id_ubicacion in number,
    p_folio in varchar2,
    p_anio in number);

  PROCEDURE SP_Traer_TramitesIPJ_PersJur (
    p_id_tramite_ipj in number,
    o_Cursor out types.cursorType);

  PROCEDURE SP_Traer_TramitesIPJ_Integ (
    p_id_tramite_ipj in number,
    o_Cursor out types.cursorType);

  PROCEDURE SP_Traer_TramitesIPJ_Acc (
    p_id_tramite_ipj in number,
    o_Cursor out types.cursorType);

  PROCEDURE SP_Traer_TramitesIPJ_Acc_ID (
    p_id_tramite_ipj in number,
    p_id_tramiteipj_accion in number,
    o_Cursor out types.cursorType);

   PROCEDURE SP_Guardar_TramiteIPJ_PersJur (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in number,
    p_id_legajo in number,
    p_id_sede in varchar2,
    p_error_dato in varchar2,
    p_eliminar in number );

  PROCEDURE SP_Guardar_TramiteIPJ_Int (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in number,
    p_id_integrante in number,
    p_error_dato in varchar2,
    p_eliminar in number,
    p_id_sexo in varchar2,
    p_nro_documento in varchar2,
    p_pai_cod_pais in varchar2,
    p_id_numero in number,
    p_cuil in varchar2,
    p_detalle in varchar2,
    p_nombre in varchar2,
    p_apellido in varchar2,
    p_n_tipo_documento in varchar2);

   PROCEDURE SP_Guardar_TramiteIPJ_Acc (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in number,
    p_id_tramiteIpj_Accion in number,
    p_Id_Tipo_Accion in number,
    p_id_Integrante in number,
    p_DNI_Integrante in varchar2,
    p_id_legajo in number,
    p_CUIT_persjur in varchar2,
    p_id_estado in number,
    p_Cuil_Usuario in varchar2,
    p_Obs_Rubrica in varchar2,
    p_Observacion in varchar2,
    p_id_fondo_comercio in number,
    p_fec_veeduria in varchar2,
    p_id_documento in number,
    p_n_documento in varchar2,
    p_id_pagina in number
    );

  PROCEDURE SP_Validar_TramiteIPJ_Acc (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in number,
    p_id_legajo in number,
    p_DNI_integrante in varchar2);

  PROCEDURE SP_Informar_Estado_Tramite (
    o_rdo out number,
    p_id_tramite_ipj in number,
    p_id_tramite_accion in number);

  PROCEDURE SP_Buscar_Tramites_Hist (
    o_Cursor out IPJ.TYPES.CURSORTYPE ,
    p_ubicacion in number,
    p_clasificacion in number,
    p_sticker in varchar2,
    p_cuit in varchar2,
    p_nro_documento in varchar2,
    p_nombre in varchar2,
    p_fecha_inicio in varchar2,
    p_fecha_fin in varchar2,
    p_tipo_operacion in number, --1- Tr�mite, 2-Acci�n - 3- Ambos
    p_id_legajo in number ,
    p_expediente in varchar2,
    p_id_estado number
  );


  PROCEDURE SP_Hist_Asig_Tramite (
    o_cursor out IPJ.TYPES.CURSORTYPE ,
    p_id_tramite_ipj in number);

  PROCEDURE SP_Hist_Asig_Accion (
    o_cursor out IPJ.TYPES.CURSORTYPE ,
    p_id_tramite_ipj in number,
    p_id_tramiteipj_accion  in number);

  PROCEDURE SP_Guardar_Accion_Doc(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in number,
    p_id_tramiteipj_accion in number,
    p_id_documento in number,
    p_n_documento in varchar2,
    p_asociado_cuit in char,
    p_id_tipo_documento_cdd in number);

  PROCEDURE SP_Ocupacion_Area(
    o_Cursor OUT types.cursorType,
    p_id_ubicacion in number
    );

  PROCEDURE SP_Hist_Empresa(
    o_Cursor OUT types.cursorType,
    p_id_legajo in number
    );

  PROCEDURE SP_Movimientos_Area(
    o_Cursor OUT types.cursorType,
    p_fecha_Desde in date,
    p_fecha_hasta in date,
    p_id_ubicacion in number,
    p_id_legajo in number
  );

  PROCEDURE SP_Guardar_Accion_Actuacion (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in number,
    p_id_tramiteIpj_Accion in number,
    p_nro_actuacion in varchar2,
    p_nro_dictamen in number
  );

  PROCEDURE SP_Traer_Reserva (
    o_Cursor OUT types.cursorType,
    p_id_tramite_ipj in number,
    p_id_tramiteipj_accion in number,
    p_id_tipo_accion in number,
    p_nro_reserva in number
    );

  PROCEDURE SP_Traer_Datos_Reserva (
    o_Cursor OUT types.cursorType,
    p_id_tramite_ipj in number,
    p_id_tramiteipj_accion in number
    );

  PROCEDURE SP_GUARDAR_RESERVA (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in number,
    p_id_tramiteIpj_Accion in number,
    p_id_tipo_accion in number,
    p_nro_reserva in number,
    p_n_reserva in varchar2,
    p_id_estado_reserva in number,
    p_fec_vence in varchar2
    --, p_observacion in varchar2
    );

  PROCEDURE SP_Informar_Reserva (
    o_Cursor OUT types.cursorType,
    p_id_tramite_ipj in number,
    p_id_tramiteipj_accion in number,
    p_id_tipo_accion in number
    );

   PROCEDURE SP_Traer_Documento_Accion (
    o_Cursor OUT types.cursorType,
    p_id_tramite_ipj in number,
    p_id_tramiteipj_accion in number
    );

  PROCEDURE SP_Cargar_Tasas_Accion (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in number,
    p_id_tramiteipj_accion in number,
    p_id_tipo_accion in number);

  PROCEDURE SP_Pasar_Area (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_Cursor OUT types.cursorType,
    p_id_tramite_ipj in number,
    p_id_ubicacion_dest in number);

  PROCEDURE SP_Pasar_Archivo (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_Cursor OUT types.cursorType,
    p_id_tramite_ipj in number,
    p_Observacion in varchar2);

  PROCEDURE SP_Pasar_Mesa (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_Cursor OUT types.cursorType,
    p_id_tramite_ipj in number,
    p_Observacion in varchar2);

   PROCEDURE SP_Traer_Tramite_Retiro (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_Cursor OUT types.cursorType,
    p_id_tramite_ipj in number);


  PROCEDURE SP_Guardar_Tramite_Retiro (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in number,
    p_id_integrante in number,
    p_error_dato in varchar2,
    p_id_sexo in varchar2,
    p_nro_documento in varchar2,
    p_pai_cod_pais in varchar2,
    p_id_numero in number,
    p_cuil in varchar2,
    p_detalle in varchar2,
    p_nombre in varchar2,
    p_apellido in varchar2,
    p_n_tipo_documento in varchar2);


  PROCEDURE SP_ELIMINAR_TRAMITE(
      o_rdo out nvarchar2,
      o_tipo_mensaje out number,
      p_id_tramite_ipj number);

  PROCEDURE SP_Quitar_Borrador_Accion
    (o_rdo out nvarchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj number,
    p_id_tramiteipj_accion number);

  PROCEDURE Sp_Guardar_Legajo(
    o_id_legajo_Rdo OUT number,
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_nro_ficha in VARCHAR2,
    p_matricula in varchar2,
    p_razon_social in VARCHAR2,
    p_cuit in VARCHAR2,
    p_id_tipo_entidad  in number,
    p_usuario in varchar2,
    p_id_legajo in number);

  PROCEDURE Sp_Pasar_SUAC_Gestion(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_tramite_ipj out number,
    p_cuil_usuario in varchar2,
    p_id_tramite_suac in varchar2);

  PROCEDURE SP_Traer_Datos_Expediente(
    o_Cursor OUT types.cursorType,
    p_sticker in varchar2,
    p_expediente in varchar2);

  PROCEDURE Sp_Crear_Accion_Automatica(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_cuil_usuario in varchar2,
    p_id_tramite_ipj in number);

  PROCEDURE SP_Traer_Lista_Acc (
    o_Cursor out types.cursorType,
    p_id_tramite_ipj in number,
    p_lista_acc in varchar2);

  PROCEDURE SP_Act_Tramites_Relac(
    o_rdo out nvarchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in number,
    p_id_legajo in number);

  PROCEDURE SP_Permitir_Acciones (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_clasif_ipj in number,
    p_id_legajo in number
  );

  PROCEDURE SP_Traer_Adjuntos (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_Cursor OUT types.cursorType,
    p_Id_Tramite_Ipj in number
  );

  PROCEDURE SP_GUARDAR_ANEXADOS (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj_Padre in number
    );

  PROCEDURE SP_Solicita_Certif_Gob(
    o_rdo out nvarchar2,
    o_tipo_mensaje out number,
    p_cuil_empleado in varchar2,
    p_id_legajo in number
  );

  PROCEDURE SP_Traer_Datos_OL (
    o_Cursor OUT types.cursorType,
    p_Id_Tramite_Ipj in number
  );

  PROCEDURE SP_Traer_Pendientes_Firma(
    o_Cursor OUT types.cursorType,
    p_cuil_usuario in varchar2,
    p_id_clasif_ipj in number
  );

  PROCEDURE SP_Rechazar_Firma (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_id_tramiteIpj_Accion in number,
    p_Cuil in varchar2,
    p_Fecha_Rechazo in varchar2,
    p_Observacion IN VARCHAR2
  );

  PROCEDURE SP_Finalizar_Firma (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_id_tramiteIpj_Accion in number
  );

  PROCEDURE SP_Traer_Pendientes_Boe (
    o_Cursor out types.cursorType,
    p_cuil_usuario in varchar2
  );

  PROCEDURE SP_Buscar_Reemp(
    o_Cursor OUT types.cursorType,
    p_cuit in varchar2
  );

   PROCEDURE SP_Guardar_Accion_Resolucion (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_id_tramiteIpj_Accion in number,
    p_id_prot_jur in varchar2,
    p_nro_resolucion in number,
    p_fec_resolucion in varchar2 -- Es Fecha
  );

  PROCEDURE SP_Act_Datos_Fideicomiso(
    o_rdo out nvarchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj IN number,
    p_Id_Legajo IN number,
    p_Codigo_Online IN number
  );

  PROCEDURE SP_Tramites_Inactivos(
    o_Cursor OUT types.cursorType,
    p_id_ubicacion in number
  );

  PROCEDURE SP_Tramites_Inactivos_Jur(
    o_Cursor OUT types.cursorType,
    p_id_ubicacion in number
  );

  PROCEDURE SP_GUARDAR_NOTA_INACTIVO(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_observacion in varchar2
  );

  PROCEDURE SP_Actualizar_Nota_Sistema(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_id_nota_sistema in number
  );

  PROCEDURE SP_Archivar_Inactivos (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number
  );

  PROCEDURE SP_Guardar_Transferencia(o_Id_Tramite_Ipj out number,
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_id_Grupo IN number,
    p_cuil_ult_estado in varchar2,
    p_observacion IN varchar2,
    p_TransfiereAcc IN VARCHAR2
  );

  PROCEDURE SP_Guardar_Accion_Act_Pers (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_id_tramiteIpj_Accion in number,
    p_nro_actuacion in varchar2,
    p_nro_dictamen in number,
    p_fecha_dictamen IN VARCHAR2
  );

  PROCEDURE SP_Validar_Terminos(
    p_denominacion IN VARCHAR2,
    p_id_tipo_entidad IN NUMBER,
    p_codigo_online IN NUMBER,
    o_rdo OUT VARCHAR2,
    o_tipo_mensaje OUT NUMBER
  );

  PROCEDURE SP_Validar_Insultos(
    p_palabra IN VARCHAR2,
    p_frase IN VARCHAR2,
    p_ubicacion IN NUMBER,
    p_id_tipo_entidad IN NUMBER,
    o_rdo OUT VARCHAR2,
    o_tipo_mensaje OUT NUMBER
  );

  PROCEDURE SP_Validar_Prohibidos(
    p_palabra IN VARCHAR2,
    p_frase IN VARCHAR2,
    p_ubicacion IN NUMBER,
    p_id_tipo_entidad IN NUMBER,
    o_rdo OUT VARCHAR2,
    o_tipo_mensaje OUT NUMBER
  );

  PROCEDURE SP_Validar_Excluidos(
    p_palabra IN VARCHAR2,
    p_frase IN VARCHAR2,
    p_ubicacion IN NUMBER,
    p_id_tipo_entidad IN NUMBER,
    o_rdo OUT VARCHAR2,
    o_tipo_mensaje OUT NUMBER
  );

  PROCEDURE SP_Validar_caracteres(
    p_denominacion IN VARCHAR2,
    p_ubicacion IN NUMBER,
    p_id_tipo_entidad IN NUMBER,
    o_rdo OUT VARCHAR2,
    o_tipo_mensaje OUT NUMBER
  );

  PROCEDURE SP_Validar_TiposSoc(
    p_frase IN VARCHAR2,
    p_ubicacion IN NUMBER,
    p_id_tipo_entidad IN NUMBER,
    o_rdo OUT VARCHAR2,
    o_tipo_mensaje OUT NUMBER
  );

  PROCEDURE SP_Buscar_Pendientes(
    o_Cursor out IPJ.TYPES.CURSORTYPE,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number
  );

  PROCEDURE SP_Validar_Existentes(
    o_rdo OUT VARCHAR2,
    o_tipo_mensaje OUT NUMBER,
    p_denominacion IN VARCHAR2,
    p_id_tipo_entidad IN NUMBER,
    p_id_ubicacion IN NUMBER
  );

  PROCEDURE SP_Hist_Inf_Subsistencia(
    o_Cursor out IPJ.TYPES.CURSORTYPE,
    p_Id_Accion in number,
    p_Id_Legajo in NUMBER
  );

  PROCEDURE SP_Traer_Pendientes_SUAC(
    o_Cursor OUT types.cursorType,
    p_id_ubicacion in number
  );

  PROCEDURE SP_Traer_Observados_SUAC(
    o_Cursor OUT types.cursorType,
    p_id_ubicacion in number
  );

  PROCEDURE SP_Cancelar_Edicto_Boe(
    o_rdo OUT VARCHAR2,
    o_tipo_mensaje OUT NUMBER,
    p_Id_Tramite_Ipj IN NUMBER,
    p_Id_Legajo IN NUMBER,
    p_Nro_Edicto IN NUMBER,
    p_id_tramite_ipj_accion IN NUMBER
  );

  PROCEDURE SP_Act_Estado_Accion(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj IN NUMBER,
    p_id_tramiteIpj_Accion in number,
    p_id_estado in NUMBER
  );

  PROCEDURE SP_Traer_Pend_Asoc_CUIT(
    o_Cursor OUT types.cursorType,
    p_id_ubicacion in number
  );

  PROCEDURE SP_Traer_Seccion_Inf(
    o_Cursor OUT types.cursorType,
    p_id_tramite_ipj in number
  );

  PROCEDURE SP_Perimir_Tramite(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number
  );

  PROCEDURE SP_Crear_Alerta(
    o_rdo OUT VARCHAR2,
    o_tipo_mensaje OUT NUMBER,
    p_Id_Tramite_Ipj IN NUMBER,
    p_Id_Tramiteipj_Accion IN NUMBER,
    p_Id_Tipo_Accion IN NUMBER,
    p_Fec_Alerta IN DATE,
    p_Id_Tipo_Alerta IN NUMBER,
    p_Id_Estado_Alerta IN NUMBER,
    p_Id_Ubicacion IN NUMBER,
    p_Cuil_Usr_Origen IN VARCHAR2);

  PROCEDURE SP_Traer_Alertas (
    o_rdo OUT VARCHAR2,
    o_tipo_mensaje OUT NUMBER,
    o_Cursor OUT types.cursorType,
    p_Id_Tipo_Alerta IN NUMBER,
    p_Id_Ubicacion IN NUMBER,
    p_Cuil_Usr_Origen IN VARCHAR2
  );

  PROCEDURE SP_Actualizar_Alerta(
    o_rdo OUT VARCHAR2,
    o_tipo_mensaje OUT NUMBER,
    p_Id_Alerta IN NUMBER,
    p_Id_Estado_Alerta IN NUMBER
  );

  PROCEDURE SP_Traer_TramitesIPJ_Bandeja(
    p_Cuil in varchar2,
    p_id_estado in number,
    p_id_clasif in number,
    p_id_ubicacion in number,
    p_Cursor OUT types.cursorType
  );

  PROCEDURE SP_Traer_TramitesIPJ_Band_Det(
    p_Id_Tramite_Ipj IN NUMBER,
    p_Cursor OUT types.cursorType
  );

  PROCEDURE SP_Desistir_Tramite(
    o_rdo OUT VARCHAR2,
    o_tipo_mensaje OUT NUMBER,
    p_id_tramite_ipj IN NUMBER,
    p_cuil_usuario in varchar2
  );

  PROCEDURE SP_Validar_Usuario_Desistir(
    o_Res OUT NUMBER, --0 sin permiso 1 con permiso
    p_Cuil_Usuario IN VARCHAR2
  );

  PROCEDURE SP_Guardar_Resol_Digital (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_id_tramiteIpj_Accion in number,
    p_id_prot_dig in varchar2,
    p_nro_resolucion in number,
    p_fec_resolucion in varchar2 -- Es Fecha
  );
  PROCEDURE SP_Tramites_Inactivos_Ciud(
    o_Cursor OUT types.cursorType,
    p_id_ubicacion in number
  );

  PROCEDURE SP_Rechazar_Tramite(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_observacion in varchar2
  );

  PROCEDURE SP_Guardar_Fec_Veed(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_id_tramiteipj_accion IN number,
    p_id_legajo in varchar2,
    p_fec_veeduria IN varchar2
  );

  PROCEDURE SP_Buscar_Res_Prot_Dig(
    o_Cursor OUT types.cursorType,
    p_id_legajo in number,
    p_id_prot_dig in varchar2,
    p_nro_resolucion in number,
    p_anio in number,
    p_nro_expediente in varchar2,
    p_fecha_desde in varchar2, -- es fecha
    p_fecha_hasta in varchar2, -- es fecha
    p_id_ubicacion in number
  );

  PROCEDURE SP_Reabrir_Tramite(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_cuil_usuario in varchar2,
    p_id_ubicacion in number,
    p_observacion varchar2,
    p_cuil_solicitante in varchar2
  );

  PROCEDURE SP_Buscar_Expte_Referencia(
    o_Cursor OUT types.cursorType,
    p_sticker in varchar2,
    p_expediente in varchar2);

  PROCEDURE SP_Guardar_Expte_Referencia(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_codigo_online in number,
    p_tipo_tramite_referencia number,
    p_id_tramite_ipj_referencia in number,
    p_id_legajo_referencia in number,
    p_expte_referencia in varchar2,
    p_obervacion in varchar2);

  PROCEDURE SP_Eiminar_Expte_Referencia(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in number,--Pasar p_id_tramite_ipj_referencia
    p_expediente in varchar2); --Pasar p_expte_referencia

  PROCEDURE SP_Traer_Expte_Referencia (
    o_Cursor OUT types.cursorType,
    p_codigo_online in NUMBER);

  PROCEDURE SP_Orden_Dia_Expte (
    o_Cursor OUT types.cursorType,
    p_id_entidad_Acta in number);

  /**********************************************************
  **********************************************************
        SOBRECARGA DE PARAMETROS
   **********************************************************
  **********************************************************/
  PROCEDURE SP_Buscar_Razon_Social(
    o_Cursor out IPJ.TYPES.CURSORTYPE,
    p_Cuit in varchar2,
    p_id_legajo in number,
    p_razon_social in varchar2,
    p_matricula in varchar2,
    p_ficha in varchar2,
    p_registro in number,
    p_id_ubicacion in number
  );

  PROCEDURE SP_Traer_TramitesIPJ_Tecnico_N(
    v_Cuil in varchar2,
    v_id_estado in number,
    p_id_clasif in number,
    p_cuil_logueado in varchar2,
    p_id_ubicacion in number,
    p_Cursor OUT types.cursorType);

end Tramites;
/

create or replace package body ipj.Tramites is

  FUNCTION FC_Datos_Adic_Firma (p_id_tramite_ipj in number) return varchar2 is
  -- Calcula los datos adicionales que se informan por Area en la firma de acciones
    v_result varchar2(500);
    v_area_tramite number;
    v_otras_orden number;
  BEGIN
    -- Busco el �rea del tr�mite
    select id_ubicacion_origen into v_area_tramite
    from ipj.t_tramitesipj
    where
      id_tramite_ipj = p_id_tramite_ipj;

    -- Calculo el dato pedido por cada �rea
    if v_area_tramite in (IPJ.TYPES.C_AREA_SXA) then
      -- Marca solo las asambleas un�nimes
      select decode(COUNT(1),0,'','Un�nime') into v_result
      from ipj.t_entidades_acta e
      where
        e.id_tramite_ipj = p_id_tramite_ipj and
        e.es_unanime = 'S';
    end if;

    -- Calculo los datos para Civiles
    if v_area_tramite in (IPJ.TYPES.C_AREA_CYF) then
      --verifico no no tenga algo aparte de OTROS y/o BALANCES
      select count(1) into v_otras_orden
      from ipj.t_entidades_acta e join IPJ.T_ENTIDADES_ACTA_ORDEN ao
          on E.ID_ENTIDAD_ACTA = AO.ID_ENTIDAD_ACTA
      where
        e.id_tramite_ipj = p_id_tramite_ipj and
        AO.ID_TIPO_ORDEN_DIA not in (21, 12);

      if v_otras_orden = 0 then
        -- Marco que tiene orden OTROS y/o Balances
        select distinct listagg (o.n_tipo_orden_dia , ' - ')
                         within group (order by o.n_tipo_orden_dia)
                         over (partition by e.id_tramite_ipj)
                         lista_orden into v_result
        from ipj.t_entidades_acta e join IPJ.T_ENTIDADES_ACTA_ORDEN ao
            on E.ID_ENTIDAD_ACTA = AO.ID_ENTIDAD_ACTA
          join ipj.t_tipos_orden_dia o
            on o.id_tipo_orden_dia = ao.id_tipo_orden_dia
        where
          e.id_tramite_ipj = p_id_tramite_ipj and
          AO.ID_TIPO_ORDEN_DIA in (21, 12); -- Otros , Balances
      else
        v_result := '';
      end if;
    end if;

    Return v_result;
  Exception
    When Others Then
      Return '';
  End FC_Datos_Adic_Firma;

  FUNCTION FC_Quitar_Ocultos (p_sticker varchar2) return varchar2 is
    -- Le quita los caracteres ocultos a in Sticker
    v_sticker varchar2(15);
  BEGIN
   -- Si tiene 14 o 15, quita los ocultos
    if length(p_sticker) > 13 then
      v_sticker := substr(p_sticker, 1, length(p_sticker)-5) || substr(p_sticker, length(p_sticker)-2, length(p_sticker)) ;
    else
      -- Si viene de 12 o 13, ay esta sin ocultos
      v_sticker := p_sticker;
    end if;

    Return v_sticker;
  Exception
    When Others Then
      Return 0;
  End FC_Quitar_Ocultos;

  FUNCTION FC_Buscar_Fecha_Inf (p_id_tramite_ipj number, p_id_tramiteipj_accion number,
    p_id_tipo_accion in number ) return date is
    /* Devuelve la fecha en que se genera el informe, segun los siguientes casos:
      - Informes: Fecha en que se genera el borrado
      - Resoluci�n SA: Fecha de la Resoluci�n
    */
    v_SQL varchar(2000);
    v_Usa_Borrador number;
    v_fecha_informe date;
  BEGIN
    -- Veo si la acci�n se encuentra en la lista de acciones configurada
    v_SQL :=
      ' select count(1) from dual ' ||
      ' where ' ||
       to_char(p_id_tipo_accion)  || ' in ( ' || IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('FECHA_INF_ACCION') || ')';

    EXECUTE IMMEDIATE v_SQL INTO v_Usa_Borrador;

    -- Se no es accion especial, usa la fecha del informe
    if v_Usa_Borrador = 0 then
      select fecha_informe into v_fecha_informe
      from IPJ.T_INFORMES_BORRADOR
      where
        id_tramite_ipj = p_id_tramite_ipj and
        id_tramiteipj_accion = p_id_tramiteipj_accion and
        id_tipo_accion = p_id_tipo_accion;
    else -- DISCRIMINO LAS FECHAS POR TIPO DE ACCIONES
      -- Para las resoluciones de RPC
      if p_id_tipo_accion in (132, 134, 133, 128, 131) then
        select fec_resolucion into v_fecha_informe
        from IPJ.T_ENTIDADES
        where
          id_tramite_ipj = p_id_tramite_ipj;
      end if;

      -- Para las resoluciones de protocolos Digitales
      if p_id_tipo_accion in (147, 146, 137, 136, 135, 157, 156, 155, 154, 153, 161, 160, 159, 175, 176) then
        select a.fec_resolucion_dig into v_fecha_informe
        from IPJ.t_tramitesipj_acciones a
        where
          id_tramite_ipj = p_id_tramite_ipj and
          id_tramiteipj_accion = p_id_tramiteipj_accion;
      end if;

      -- Para las resoluciones de Fideicomiso
      if p_id_tipo_accion in (151) then
        select fecha_versionado into v_fecha_informe
        from IPJ.T_ENTIDADES
        where
          id_tramite_ipj = p_id_tramite_ipj;
      end if;
    end if;

    Return v_fecha_informe;
  Exception
    When Others Then
      Return null;
  End FC_Buscar_Fecha_Inf;

  FUNCTION FC_Buscar_Historial_Acc (p_id_tramite_ipj number, p_id_tramiteipj_accion number,
    p_Ascendente char, p_estados_in varchar2, p_estados_out varchar2) return number is
    /* Devuelve el primer ID_PASES_ACCIONES_IPJ segun la siguientes condiciones:
       - Orden ascendente o descendente
       - Los pases incluidos en estados_in
       - Los pases que excluyen estados_out
    */
    v_SQL varchar(2000);
    v_id_pase number;
  BEGIN
    -- Armo en encabecesado de la consulta
    -- Ordeno Ascendente o Descendente
    if nvl(p_Ascendente, 'N') = 'N' then
      v_SQL :=
      ' select max(id_pases_acciones_ipj) ';
    else
      v_SQL :=
      ' select min(id_pases_acciones_ipj) ';
    end if;

    v_SQL := v_SQL ||
      ' from ipj.t_tramitesipj_acciones_estado ae ' ||
      ' where ' ||
      '   ae.id_tramite_ipj = ' || to_char(p_id_tramite_ipj)  || ' and ' ||
      '   ae.id_tramiteipj_accion = ' || to_char(p_id_tramiteipj_accion) ;

    -- Sumo los estados incluidos, si viene alguno
    if p_estados_in is not null then
      v_SQL := v_SQL ||
        '    and ae.id_estado in (' || p_estados_in ||') ';
    end if;

    -- Sumo los estados excluidos si viene alguno
    if p_estados_out is not null then
      v_SQL := v_SQL ||
        '   and ae.id_estado not in (' || p_estados_out ||') ';
    end if;

    EXECUTE IMMEDIATE v_SQL INTO v_id_pase;

    Return v_id_pase;
  Exception
    When Others Then
      Return 0;
  End FC_Buscar_Historial_Acc;

  FUNCTION FC_Es_Tram_Digital (p_id_tramite_ipj number) return number is
    -- Si un tr�mite tiene documento adjunto, o es un tr�mite digital, se indica la cantidad de documentos o 1
    v_doc_adjuntos number;
    v_tram_dig number;
  BEGIN
    -- Cuento documento digitales
    select count(1) into v_doc_adjuntos
    from IPJ.T_ENTIDADES_DOC_ADJUNTOS
    where
      id_tramite_ipj = p_id_tramite_ipj;

    -- Cuento si el tr�mite es digital
    select count(1) into v_tram_dig
    from ipj.t_tramitesipj tr join ipj.t_ol_entidades o
      on tr.codigo_online = o.codigo_online
    where
      tr.id_tramite_ipj = p_id_tramite_ipj and
      o.id_tipo_tramite_ol in (10, 12, 13, 47, 48);-- Form. A - Inf. ACyF - Form. B - Form. H

    Return v_doc_adjuntos + v_tram_dig;
  Exception
    When Others Then
      Return 0;
  End FC_Es_Tram_Digital;

  FUNCTION FC_EXPEDIENTE_TRAMITE_IPJ (p_Id_Tramite_Ipj IN number) return varchar2 is
    v_res varchar2(50);
  /**********************************************************
     Devuelve el Nro. Expediendte de Suac, si existe el tr�mite en la vista
 **********************************************************/
  BEGIN

    select vtSuac.nro_tramite into v_res
    from IPJ.T_TRAMITESIPJ tr join NUEVOSUAC.VT_TRAMITES_IPJ_DESA vtSuac
      on tr.id_tramite = vtSuac.id_tramite
    where
      tr.Id_Tramite_Ipj = p_Id_Tramite_Ipj;

    Return ltrim(rtrim(v_res));
  Exception
    When Others Then
      Return '-';
  End FC_EXPEDIENTE_TRAMITE_IPJ;

  FUNCTION FC_Ultima_Fecha_Tramite (p_id_tramite number) return varchar2 is
   res varchar2(25);
  BEGIN
    select to_char(max(fecha_pase), 'dd/mm/rrrr') into res
    from IPJ.t_tramites_ipj_estado
    where Id_Tramite_Ipj = p_id_tramite;

    Return res;
  Exception
    When Others Then Return null;
  End FC_Ultima_Fecha_Tramite;

  FUNCTION FC_Ultima_Fecha_Accion (p_id_tramite number, p_id_tramiteipj_accion number) return date is
   res date;
  BEGIN
    select max(fecha_pase) into res
    from IPJ.t_tramitesipj_acciones_estado
    where
      Id_Tramite_Ipj = p_id_tramite and
      id_tramiteipj_accion = p_id_tramiteipj_accion;

    Return res;
  Exception
    When Others Then Return null;
  End FC_Ultima_Fecha_Accion;

  FUNCTION FC_Primera_Fecha_Accion (p_id_tramite number, p_id_tramiteipj_accion number) return date is
   res date;
  BEGIN
    select min(fecha_pase) into res
    from IPJ.t_tramitesipj_acciones_estado
    where
      Id_Tramite_Ipj = p_id_tramite and
      id_tramiteipj_accion = p_id_tramiteipj_accion;

    Return res;
  Exception
    When Others Then Return null;
  End FC_Primera_Fecha_Accion;

 FUNCTION FC_Razon_Social (p_cuit varchar2) return varchar2 is
    res varchar2(100);
    /*********************************************************
      Busca el nombre del CUIT indicado en Entidades locales, si no lo encuentra lo
      busca en Personas Juridicas
    *********************************************************/
  BEGIN
    begin
      select denominacion_1 into res
      from ( select * from IPJ.T_Entidades where cuit =  p_cuit order by Id_Tramite_Ipj desc)
      where rownum = 1;
    exception
      when NO_DATA_FOUND then
        select razon_social into res
        from ( select * from T_COMUNES.VT_PERS_JURIDICAS_ABRV where cuit =  p_cuit  order by id_sede asc)
        where rownum = 1;
    end;

    Return res;
  Exception
    When Others Then Return IPJ.VARIOS.MENSAJE_ERROR('EMPRESA_NOT');
  End FC_Razon_Social;

  FUNCTION FC_Cuit_PersJur (p_id_legajo number) return varchar2 is
   res varchar2(11);
  BEGIN
    select nvl(cuit, '') into res
    from IPJ.T_Legajos
    where
      id_legajo = p_id_legajo;

    Return res;
  Exception
    When Others Then Return null;
  End FC_Cuit_PersJur;

  FUNCTION FC_Acciones_Abiertas (p_Id_Tramite_Ipj number) return number is
    res number;
  BEGIN
    select
      count(*) INTO res
    from IPJ.T_TRAMITESIPJ_ACCIONES ta
    where
      TA.Id_Tramite_Ipj = p_Id_Tramite_Ipj and
      TA.ID_ESTADO < 100 and
      TA.ID_TIPO_ACCION <> IPJ.Types.c_Tipo_Acc_Retiro;

    return res;
  END FC_Acciones_Abiertas;

  FUNCTION FC_Acciones_Cerradas (p_Id_Tramite_Ipj number) return number is
    res number;
  BEGIN
    select
      count(*) INTO res
    from IPJ.T_TRAMITESIPJ_ACCIONES ta
    where
      TA.Id_Tramite_Ipj = p_Id_Tramite_Ipj and
      TA.ID_ESTADO >= 100 and
      TA.ID_TIPO_ACCION <> IPJ.Types.c_Tipo_Acc_Retiro;

      return res;
  END FC_Acciones_Cerradas;

  FUNCTION FC_Acciones_Observadas (p_Id_Tramite_Ipj number) return number is
    res number;
  BEGIN
    select
      count(*) INTO res
    from IPJ.T_TRAMITESIPJ_ACCIONES ta
    where
      TA.Id_Tramite_Ipj = p_Id_Tramite_Ipj and
      TA.ID_ESTADO = IPJ.Types.c_Estados_Observado;

      return res;
  END FC_Acciones_Observadas;

  FUNCTION FC_Control_Tramite (p_Id_Tramite_Ipj number) return number is
     v_cant_persjur number;
     v_cant_interg number;
     v_cant_acc number;
    res number;
  BEGIN
    /* Esta funcion controla que la cantidad de acciones sea mayo o igual
        que la cantidad de personas
     */
    select count(*) into v_cant_persjur
    from IPJ.T_TRAMITESIPJ_PERSJUR
    where
      Id_Tramite_Ipj = p_Id_Tramite_Ipj;

    select count(*) into v_cant_interg
    from IPJ.T_TRAMITESIPJ_INTEGRANTE
    where
      Id_Tramite_Ipj = p_Id_Tramite_Ipj;

    select count(*) into v_cant_acc
    from IPJ.T_TRAMITESIPJ_ACCIONES
    where
      Id_Tramite_Ipj = p_Id_Tramite_Ipj;

    res := v_cant_acc - v_cant_persjur - v_cant_interg;
    return res;
  END FC_Control_Tramite;

   FUNCTION FC_Control_Existe_Pers (p_Id_Tramite_Ipj number) return number is
     v_cant_persjur number;
     v_cant_interg number;
     v_cant_FC number;
    res number;
  BEGIN
    /*********************************************************
    Esta funcion controla que la cantidad de acciones sea mayo o igual
        que la cantidad de personas
     ********************************************************/
     -- Controlo la cantidad de Personas Juridicas
    select count(*) into v_cant_persjur
    from IPJ.T_TRAMITESIPJ_PERSJUR
    where
      Id_Tramite_Ipj = p_Id_Tramite_Ipj;

    --  Controlo la cantidad de Personas F�sicas
    select count(*) into v_cant_interg
    from IPJ.T_TRAMITESIPJ_INTEGRANTE
    where
      Id_Tramite_Ipj = p_Id_Tramite_Ipj;

    -- Controlo la cantidad de Fondos de Comercios
    select count(*) into v_cant_FC
    from IPJ.T_TRAMITESIPJ_ACCIONES
    where
      Id_Tramite_Ipj = p_Id_Tramite_Ipj and
      nvl(id_fondo_comercio, 0) <> 0;

    res := v_cant_FC + v_cant_persjur + v_cant_interg;
    return res;
  END FC_Control_Existe_Pers;

  FUNCTION FC_Fecha_Tramite(p_id_legajo number, p_matricula_version number) return varchar2 is
    v_result date;
  BEGIN
    select tipj.fecha_inicio into v_result
    from ipj.t_entidades ent join IPJ.t_tramitesipj tipj
      on ent.Id_Tramite_Ipj = tipj.Id_Tramite_Ipj
    where
      ent.id_legajo = p_id_legajo and
      ent.matricula_version = p_matricula_version and
      TIPJ.ID_ESTADO_ULT < IPJ.TYPES.C_ESTADOS_RECHAZADO and
      rownum = 1;

    return to_char(v_result, 'dd/mm/rrrr');

  END FC_Fecha_Tramite;


  FUNCTION FC_Usr_Accion_Tramite(p_id_legajo number, p_matricula_version number) return varchar2 is
    v_result varchar2(100);
  BEGIN
    select U.DESCRIPCION into v_result
    from ipj.t_entidades ent join IPJ.t_tramitesipj tipj
        on ent.Id_Tramite_Ipj = tipj.Id_Tramite_Ipj
      join IPJ.T_TRAMITESIPJ_ACCIONES tacc on
        tacc.Id_Tramite_Ipj = tipj.Id_Tramite_Ipj and tacc.id_legajo = p_id_legajo
      join IPJ.T_TIPOS_ACCIONESIPJ tt
        on TT.ID_TIPO_ACCION = TACC.ID_TIPO_ACCION
      join ipj.t_usuarios u
        on u.cuil_usuario = TACC.CUIL_USUARIO
    where
      ent.id_legajo = p_id_legajo and
      ent.matricula_version = p_matricula_version and
      TIPJ.ID_ESTADO_ULT < IPJ.TYPES.C_ESTADOS_RECHAZADO and
      nvl(TT.ID_PROTOCOLO, 0) > 0 and
      rownum = 1;

    return v_result;

  END FC_Usr_Accion_Tramite;

  FUNCTION FC_N_Subtipo_Suac(p_N_Tiposuac IN VARCHAR2, p_Id_Tramite IN NUMBER) RETURN VARCHAR2 IS
  v_res VARCHAR2(100):=0;
  v_Where_Config ipj.t_config.valor_variable%TYPE;
  v_Sql VARCHAR2(1000);
  v_tipo_suac varchar2(500);
  v_subtipo_suac varchar2(500);
  -- Retorna el c�digo de subtipo de Suac
  BEGIN
    IF nvl(p_Id_Tramite,0) <> 0 THEN

      SELECT c.valor_variable
        INTO v_Where_Config
      FROM ipj.t_config c
      WHERE c.id_variable = 'EXCLUSION_DOM_ELECT';

      -- Busco el tipo y subtipo del tr�mite
      IPJ.TRAMITES_SUAC.SP_Buscar_Tipo_SUAC (v_tipo_suac, v_subtipo_suac, p_Id_Tramite);

      v_sql:='SELECT nvl(st.id_subtipo_tramite,0) '||
             ' FROM nuevosuac.vt_subtipos_tramite st '||
             'WHERE ' ||
             ' st.id_subtipo_tramite IN ('|| v_Where_Config ||')'||
             ' AND st.n_subtipo_tramite = ''' ||v_subtipo_suac|| '''' ||
             ' AND st.n_tipo_tramite = ''' ||v_tipo_suac || '''';

      EXECUTE IMMEDIATE v_sql INTO v_res;

    END IF;

    return(v_res);

  EXCEPTION
    WHEN no_data_found THEN
      RETURN(v_res);
    WHEN OTHERS THEN
      RETURN '-'||SQLERRM||'--'||SQLCODE;
  END FC_N_Subtipo_Suac;


  PROCEDURE SP_Buscar_Razon_Social(
    o_Cursor out IPJ.TYPES.CURSORTYPE,
    p_Cuit in varchar2,
    p_id_legajo in number,
    p_razon_social in varchar2,
    p_matricula in varchar2,
    p_ficha in varchar2,
    p_registro in number,
    p_id_ubicacion in number,
    p_folio in varchar2,
    p_anio in number
    )
  IS
  /****************************************************
       Dado un CUIT, LEGAJO, NOMBRE, MATRICULA, FOLIO, A�O o FICHA/REGISTRO
       busca en T_Legajos quienes cumplan la condicion.
       Busca filtrando las empresa de cada area y todos los que no tienen area.
       En el caso de RPC, se permite buscar en RPC y SxA.
  *****************************************************/
    v_id_ubicacion number;
  BEGIN
    if IPJ.Types.c_habilitar_log_SP = 'S' then
      IPJ.VARIOS.SP_GUARDAR_LOG(
        p_METODO => 'SP_Buscar_Razon_Social',
        p_NIVEL => '',
        p_ORIGEN => 'DB',
        p_USR_LOG  => 'DB',
        p_USR_WINDOWS => 'Sistema',
        p_IP_PC  => null,
        p_EXCEPCION =>
          'Id Legajo = ' || to_char(p_id_legajo)
          || ' / Razon Social = ' || p_razon_social
          || ' / Matricula = ' || p_matricula
          || ' / Ficha = ' || p_ficha
          || ' / Registro = ' || to_char(p_registro)
          || ' / Ubicacion = ' || to_char(p_id_ubicacion)
          || ' / Folio = ' || p_folio
          || ' / A�o = ' || to_char(p_anio)
       );
    end if;

    OPEN o_Cursor FOR
      select
         l.id_legajo, l.cuit Cuit_Empresa, l.cuit, l.denominacion_sia N_Empresa,
         l.denominacion_sia, l.nro_ficha, l.nro_ficha Matricula, l.registro nro_registro,
         l.id_tipo_entidad, TE.TIPO_ENTIDAD, l.id_legajo id_legajo_socio,
         l.folio, l.anio, D.N_Localidad, te.id_ubicacion,
         (select to_char(max(fec_inscripcion), 'dd/mm/rrrr') from ipj.t_entidades where id_legajo = l.id_legajo) Fec_Inscripcion,
         (select to_char(max(to_date(acta_contitutiva, 'dd/mm/rrrr')), 'dd/mm/rrrr') from ipj.t_entidades where id_legajo = l.id_legajo) Acta_Contitutiva,
         upper(IPJ.ENTIDAD_PERSJUR.FC_ESTADO_GRAVAMEN_PERSJUR(l.id_legajo)) Resolucion_Gravamen,
         decode (l.fecha_baja_entidad, null, l.denominacion_sia, l.denominacion_sia || '(Cancelada ' || to_char(l.fecha_baja_entidad, 'dd/mm/rrrr') || ')')  N_Empresa_Est
      from IPJ.t_legajos L left join IPJ.t_tipos_entidades te
          on l.id_tipo_entidad = te.id_tipo_entidad
        left join (select id_legajo, nvl(max(id_vin_real), 0) id_vin
                from ipj.t_entidades
                group by id_legajo) e
           on l.id_legajo = e.id_legajo
        left join DOM_MANAGER.VT_DOMICILIOS_COND d
          on D.ID_VIN =  e.id_vin
      where
        nvl(eliminado, 0) = 0 and
        ( nvl(l.id_tipo_entidad, 0) = 0 or
          p_id_ubicacion = 0 or
          ( p_id_ubicacion = 1 and te.id_ubicacion in (IPJ.TYPES.C_AREA_SRL, IPJ.TYPES.C_AREA_SxA)) or
           te.id_ubicacion = p_id_ubicacion
        ) and
        (
          (nvl(p_id_legajo, 0) <> 0 and L.Id_Legajo = p_id_legajo) OR
          (p_cuit is not null and replace(L.cuit, '-', '') = replace(p_cuit, '-', '')) OR
          (p_razon_social is not null and IPJ.VARIOS.FC_Quitar_Acentos(L.DENOMINACION_SIA) like '%' || IPJ.VARIOS.FC_Quitar_Acentos(p_razon_social) || '%' ) OR
          (p_matricula is not null and p_id_ubicacion in (IPJ.Types.c_area_SxA, IPJ.Types.c_area_SRL, 0) and -- El 0 es para Archivo
            replace(IPJ.Varios.FC_Formatear_Matricula (l.nro_ficha), '#', '') = replace(IPJ.Varios.FC_Formatear_Matricula (p_matricula), '#', '')) OR
          (p_folio is not null and nvl(p_anio, 0) <> 0 and upper(trim(l.folio)) = upper(trim(p_folio)) and l.anio = p_anio) OR
          (p_ficha is not null and p_id_ubicacion in (IPJ.Types.c_area_CyF, 0) and
            upper(trim(l.nro_ficha)) = upper(trim(p_ficha))) OR
          (nvl(p_registro,0) <> 0and p_id_ubicacion in (IPJ.Types.c_area_SxA, 0) and l.registro = p_registro)
        );
  END SP_Buscar_Razon_Social;


  PROCEDURE SP_Traer_TramitesIPJ_Admin(
    v_Cuil in varchar2,
    v_id_clasif in number,
    p_Cursor OUT types.cursorType)
  IS
  /****************************************************
   Este procedimiento muestra los tramites ya ingresados en IPJ (unido a SUAC para ver el resto de los datos)
   Por defecto muentra los tramites activos (ni pendientes ni cerrado) pero se puede indicar que estado filtrar.
   Indica para cada tramite la cantidad de acciones abiertas y cerradas.

   Es la union de los tramites con SUAC y los SIN SUAC.
   Los tramites de IPJ se traen desde una vista local
  *****************************************************/
  BEGIN
    OPEN p_Cursor FOR
      select
        vtIPJ.botones,
        vtIPJ.id_tramite Id_Tramite_Suac, VTIPJ.Sticker,
        (case id_proceso
            when 4 then 'GENERACION DE HISTORICOS - ' || VTIPJ.N_UBICACION
            when 5 then 'CARGA DE HISTORICOS'
            else IPJ.TRAMITES_SUAC.FC_BUSCAR_SUBTIPO_SUAC(vtIPJ.id_tramite)
        end) Tipo,
        (case vtIPJ.id_proceso
            when 4 then ''
            when 5 then vtIPJ.Observacion
            else IPJ.TRAMITES_SUAC.FC_BUSCAR_ASUNTO_SUAC(vtIPJ.id_tramite)
        end) Asunto,
        vtIPJ.Expediente Nro_Expediente,
        vtIPJ.fecha_ini_suac Fecha_Inicio_Suac,
        null Fecha_recepcion,
        vtIPJ.Id_Tramite_Ipj, vtIPJ.id_clasif_ipj, vtIPJ.Observacion, vtIPJ.Id_Estado,
        vtIPJ.cuil_usuario, vtIPJ.n_estado, vtIPJ.N_Clasif_Ipj, vtIPJ.Fecha_Asignacion,
        vtIPJ.Acc_Abiertas, vtIPJ.Acc_Cerradas, vtIPJ.Acc_Observadas,
        to_char(vtIPJ.Acc_Cerradas) || '/' || to_char(vtIPJ.Acc_Abiertas + vtIPJ.Acc_Cerradas) || ' (' || to_char(vtIPJ.Acc_Observadas) || ')' Acciones,
        vtIPJ.URGENTE, vtIPJ.usuario, vtIPJ.n_ubicacion,
        vtIPJ.Cuil_Creador, vtIPJ.id_ubicacion, vtIPJ.id_grupo, vtIPJ.id_proceso,
        vtIPJ.cuil_usuario cuil_usuario_old,vtIPJ.simple_tramite,
        (select vt.id_subtipo_tramite from NUEVOSUAC.VT_TRAMITES_IPJ_DESA vt where vt.id_tramite = vtIPJ.id_tramite) id_subtipo_tramite
      from IPJ.VT_TRAMITE_ADMIN_IPJ vtIPJ
      where
        vtIPJ.ID_ESTADO < IPJ.TYPES.C_ESTADOS_COMPLETADO and
        (v_Cuil is null or v_Cuil = vtIPJ.cuil_usuario or v_cuil = vtIPJ.CUIL_USUARIO_GRUPO) and
        (nvl(v_id_clasif, 0) = 0 or vtIPJ.id_clasif_ipj = v_id_clasif)
      order by vtIPJ.id_proceso desc, vtIPJ.urgente, vtIPJ.sticker ;

  END SP_Traer_TramitesIPJ_Admin;

  PROCEDURE SP_Traer_TramitesIPJ_Tecnico(
    v_Cuil in varchar2,
    v_id_estado in number,
    p_id_clasif in number,
    p_cuil_logueado in varchar2,
    p_id_ubicacion in number,
    p_Cursor OUT types.cursorType)
  IS
  /****************************************************
   Este procedimiento muestra los tramites y acciones asignadas a un t�cnico o
   a un grupo al cual pertenezca.
   Al unir los datos, indica en la primer columna si es TRAMITE o ACCION
  *****************************************************/
    v_EsAdmin number;
    v_grupo_admin_sa number;
    v_grupo_certif_rpc number;
    v_grupo_asign_acyf NUMBER;
  BEGIN
    -- Busco los grupos que manejan los tr�mites digitales recien ingresados
    v_grupo_admin_sa := to_number(IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('GRUPO_ADM_SA'));
    v_grupo_certif_rpc := to_number(IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('GRUPO_ADM_RP'));
    v_grupo_asign_acyf := to_number(IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('GRUPO_ADM_ACYF'));

    -- Verifico si el usuario posee permisos de Administrador
    select count(*) into v_EsAdmin
    from IPJ.t_grupos_trab_ubicacion gru
    where
      gru.Cuil = p_cuil_logueado and
      gru.id_ubicacion = p_id_ubicacion and
      gru.Id_Grupo_Trabajo = IPJ.Types.C_Rol_Administrador;

    OPEN p_Cursor FOR
      select
        tmp.botones,
        tmp.Clase, tmp.id_tramite Id_Tramite_Suac, tmp.Sticker, tmp.fecha_ini_suac Fecha_Inicio_Suac,
        null Fecha_recepcion,
        (case tmp.id_proceso
           when 4 then 'GENERACION DE HISTORICOS - ' || tmp.N_UBICACION
           when 5 then 'CARGA DE HISTORICOS'
           else IPJ.TRAMITES_SUAC.FC_BUSCAR_SUBTIPO_SUAC(tmp.id_tramite)
        end) Tipo,
        (case tmp.id_proceso
            when 4 then 'GENERACION DE HISTORICOS - ' || tmp.N_UBICACION
            when 5 then tmp.observacion
            else IPJ.TRAMITES_SUAC.FC_BUSCAR_ASUNTO_SUAC(tmp.id_tramite)
        end) Asunto,
        tmp.Expediente Nro_Expediente, tmp.Id_Tramite_Ipj, tmp.id_clasif_ipj,
        tmp.id_tipo_accion, tmp.Observacion, tmp.id_estado, tmp.cuil_usuario, tmp.n_estado,
        tmp.Tipo_IPJ, tmp.Fecha_Asignacion, tmp.Acc_Abiertas, tmp.Acc_Cerradas, tmp.URGENTE,
        tmp.usuario, nvl(tmp.Error_Dato, 'N') Error_Dato, tmp.CUIT, tmp.razon_social, tmp.id_legajo,
        tmp.N_UBICACION, tmp.CUIL_CREADOR, tmp.id_ubicacion, tmp.id_tramiteipj_accion,
        tmp.id_protocolo, tmp.ID_INTEGRANTE, tmp.NRO_DOCUMENTO, tmp.Cuit_PersJur,
        tmp.Obs_Rubrica, tmp.identificacion, tmp.persona, tmp.ID_FONDO_COMERCIO,
        tmp.id_proceso, tmp.id_pagina, tmp.cuil_usuario cuil_usuario_old, tmp.ID_DOCUMENTO,
        tmp.N_DOCUMENTO,tmp.simple_tramite, tmp.Agrupable, tmp.codigo_online,
        ( select count(1)
          from ipj.t_ol_entidades eo
          where
            eo.codigo_online = tmp.codigo_online and
            ((eo.id_tipo_tramite_ol = 1 and eo.id_sub_tramite_ol = 1) or
             (eo.id_tipo_tramite_ol = 20 and eo.id_sub_tramite_ol = 5) or
             (eo.id_tipo_tramite_ol = 17 and eo.id_sub_tramite_ol = 7) or
             (eo.id_tipo_tramite_ol = 23 and eo.id_sub_tramite_ol = 9) or
             (eo.id_tipo_tramite_ol = 52 and eo.id_sub_tramite_ol = 11) or
             eo.id_tipo_tramite_ol = 19)
        ) Const_SA,
        (select eo.id_obj_social from ipj.t_ol_entidades eo where eo.codigo_online = tmp.codigo_online) id_Obj_Social,
        tmp.id_tramite_ipj_padre,
        --IPJ.TRAMITES_SUAC.FC_BUSCAR_SUBTIPO_SUAC(tmp.id_tramite) id_subtipo_tramite,
        FC_N_Subtipo_Suac(IPJ.TRAMITES_SUAC.FC_BUSCAR_SUBTIPO_SUAC(tmp.id_tramite), tmp.id_tramite) id_subtipo_tramite,
        (select count(1) from IPJ.T_ENTIDADES_AUTORIZADOS ea where ea.id_tramite_ipj = tmp.id_tramite_ipj and nvl(ea.mail_autorizado, 'N') = 'S') dom_electronico,
        FC_Es_Tram_Digital(tmp.id_tramite_ipj) Es_Digital,
        (select nvl(tr.enviar_mail, 0) from ipj.t_tramitesipj tr where tr.id_tramite_ipj = tmp.id_tramite_ipj) enviar_mail,
        (select o.cod_seguridad from ipj.t_ol_entidades o where o.codigo_online = tmp.codigo_online) cod_seguridad,
        ( select te.id_ubicacion
          from ipj.t_tramitesipj_persjur tp join ipj.t_legajos l on tp.id_legajo = l.id_legajo
            join ipj.t_tipos_entidades te  on l.id_tipo_entidad = te.id_tipo_entidad
          where
            tp.id_tramite_ipj = tmp.id_tramite_ipj and
            rownum = 1 -- Por si hay m�s de 1 empresa, uso la primera
         ) id_ubicacion_entidad,
         ( select l.cuit
           from ipj.t_tramitesipj_persjur tp join ipj.t_legajos l on tp.id_legajo = l.id_legajo
           where
             tp.id_tramite_ipj = tmp.id_tramite_ipj and
             rownum = 1 -- Por si hay m�s de 1 empresa, uso la primera
         ) Cuit_Gravamen,
         ( select l.denominacion_sia
           from ipj.t_tramitesipj_persjur tp join ipj.t_legajos l on tp.id_legajo = l.id_legajo
           where
             tp.id_tramite_ipj = tmp.id_tramite_ipj and
             rownum = 1 -- Por si hay m�s de 1 empresa, uso la primera
         ) Denominacion_Gravamen,
         (SELECT t.id_motivo_vista FROM ipj.t_tramitesipj t WHERE t.id_tramite_ipj = tmp.id_tramite_ipj)id_motivo_vista
      from
        (
          -- filtra que el usuario sea el asignado, o este en el grupo
          select *
          from IPJ.VT_TRAMITE_TECN_IPJ vtIPJ
          where
            VTIPJ.ID_ESTADO < IPJ.TYPES.C_ESTADOS_COMPLETADO and
            (VTIPJ.ID_GRUPO_ULT_ESTADO not in (v_grupo_admin_sa, v_grupo_certif_rpc, v_grupo_asign_acyf) or VTIPJ.ID_ESTADO not in (1, 5, 6)) and
            ( vtIPJ.cuil_usuario = v_Cuil or vtIPJ.cuil_usuario_grupo = v_Cuil  ) and
            (nvl(v_id_estado, 0) = 0 or vtIPJ.Id_Estado = v_id_estado) and
            (nvl(p_id_clasif, 0) = 0 or vtIPJ.id_clasif_ipj = p_id_clasif)

          UNION ALL
          -- Un tramite que tenga una accion asignada al usuario, pero no el tr�mite
          select *
          from IPJ.VT_TRAMITE_TECN_IPJ vtIPJ
          where
            VTIPJ.ID_ESTADO < IPJ.TYPES.C_ESTADOS_COMPLETADO and
            vtIPJ.Clase = 1 and
            (VTIPJ.ID_GRUPO_ULT_ESTADO not in (v_grupo_admin_sa, v_grupo_certif_rpc, v_grupo_asign_acyf) or VTIPJ.ID_ESTADO not in (1, 5, 6)) and
            vtIPJ.cuil_usuario <> v_Cuil and
            (vtIPJ.cuil_usuario_grupo is null or vtIPJ.cuil_usuario_grupo <> v_Cuil) and
            vtIPJ.Id_Tramite_Ipj in (select distinct Id_Tramite_Ipj
                                              from ipj.t_tramitesipj_acciones  acc
                                              where Acc.Cuil_Usuario = v_Cuil and acc.Id_Estado < IPJ.TYPES.C_ESTADOS_COMPLETADO) and
            (nvl(v_id_estado, 0) = 0 or vtIPJ.Id_Estado = v_id_estado) and
            (nvl(p_id_clasif, 0) = 0 or vtIPJ.id_clasif_ipj = p_id_clasif)

        UNION ALL
        -- Bug 10941:[SG] - [Transferencia de Acciones] - Visualizar tr�mites que no son del usuario, pero que tienen acciones del usuario
        select distinct 0 botones, clase, id_tramite, id_tramite_ipj, id_clasif_ipj, id_tipo_accion, observacion, id_estado, id_estado_tramite, id_estado_accion
             , cuil_usuario, n_estado, tipo_ipj, fecha_asignacion, acc_abiertas, acc_cerradas, urgente, usuario, error_dato, cuit, razon_social, id_legajo
             , n_ubicacion, cuil_creador, id_ubicacion, id_tramiteipj_accion, id_protocolo, id_integrante, nro_documento, cuit_persjur, obs_rubrica
             , identificacion, persona, id_fondo_comercio, id_proceso, id_pagina, agrupable, null cuil_usuario_grupo, id_grupo_ult_estado, sticker, expediente
             , id_documento, n_documento, simple_tramite, n_ubicacion_origen, codigo_online, id_tramite_ipj_padre, fecha_ini_suac
          from IPJ.VT_TRAMITE_TECN_IPJ vtIPJ
         WHERE
           VTIPJ.ID_ESTADO < IPJ.TYPES.C_ESTADOS_COMPLETADO
           AND vtIPJ.Clase = 1
           and (VTIPJ.ID_GRUPO_ULT_ESTADO not in (v_grupo_admin_sa, v_grupo_certif_rpc, v_grupo_asign_acyf) or VTIPJ.ID_ESTADO not in (1, 5, 6))
           AND (nvl(vtIPJ.id_Grupo_Ult_Estado,0) <> 0 AND vtIPJ.CUIL_USUARIO IS NULL)
           AND vtIPJ.Id_Tramite_Ipj in (select distinct Id_Tramite_Ipj
                                          from ipj.t_tramitesipj_acciones  acc
                                         where Acc.Cuil_Usuario = v_Cuil
                                           and acc.Id_Estado < IPJ.TYPES.C_ESTADOS_COMPLETADO)
           AND vtIPJ.Id_Tramite_Ipj NOT IN (select vtIPJ.id_tramite_ipj
                                              from IPJ.VT_TRAMITE_TECN_IPJ vtIPJ
                                             WHERE VTIPJ.ID_ESTADO < IPJ.TYPES.C_ESTADOS_COMPLETADO
                                               -- filtra que el usuario sea el asignado, o este en el grupo
                                               AND (vtIPJ.cuil_usuario = v_Cuil or vtIPJ.cuil_usuario_grupo = v_Cuil)
                                               AND (nvl(v_id_estado, 0) = 0 or vtIPJ.Id_Estado = v_id_estado)
                                               AND (nvl(p_id_clasif, 0) = 0 or vtIPJ.id_clasif_ipj = p_id_clasif)
                                               AND vtIPJ.Clase = 1)

        ) tmp
      order by Const_SA desc , id_tramite_ipj asc, clase asc;

  END SP_Traer_TramitesIPJ_Tecnico;

 PROCEDURE SP_Traer_TramitesIPJ_Juridico(
    v_id_estado in number,
    p_Cursor OUT types.cursorType)
  IS
  /****************************************************
   Este procedimiento muestra los tramites y acciones asignadas a un t�cnico o
   a un grupo al cual pertenezca.
   Al unir los datos, indica en la primer columna si es TRAMITE o ACCION
  *****************************************************/
  BEGIN
    OPEN p_Cursor FOR
      select distinct
        vtIPJ.botones,
        vtIPJ.Clase, vtIPJ.id_tramite Id_Tramite_Suac, vtIPJ.Sticker,
        vtIPJ.fecha_ini_suac Fecha_Inicio_Suac,  null Fecha_recepcion,
        (case vtIPJ.id_proceso
           when 4 then 'GENERACION DE HISTORICOS - ' || VTIPJ.N_UBICACION
           when 5 then 'CARGA DE HISTORICOS'
           else IPJ.TRAMITES_SUAC.FC_BUSCAR_SUBTIPO_SUAC(vtIPJ.id_tramite)
        end) Tipo,
        (case vtIPJ.id_proceso
            when 4 then 'GENERACION DE HISTORICOS - ' || VTIPJ.N_UBICACION
            when 5 then vtIPJ.observacion
            else IPJ.TRAMITES_SUAC.FC_BUSCAR_ASUNTO_SUAC(vtIPJ.id_tramite)
        end) Asunto,
        vtIPJ.Expediente Nro_Expediente,
        vtIPJ.Id_Tramite_Ipj, vtIPJ.id_clasif_ipj, vtIPJ.id_tipo_accion, vtIPJ.Observacion, vtIPJ.id_estado,
        vtIPJ.cuil_usuario, vtIPJ.n_estado, vtIPJ.Tipo_IPJ,
        vtIPJ.Fecha_Asignacion,
        vtIPJ.Acc_Abiertas,
        vtIPJ.Acc_Cerradas, vtIPJ.URGENTE,
        vtIPJ.usuario, nvl(vtIPJ.Error_Dato, 'N') Error_Dato,
        vtIPJ.CUIT, vtIPJ.razon_social, vtIPJ.id_legajo,
        vtIPJ.N_UBICACION, vtIPJ.CUIL_CREADOR, vtIPJ.id_ubicacion, vtIPJ.id_tramiteipj_accion,
        vtIPJ.id_protocolo,
        vtIPJ.ID_INTEGRANTE, vtIPJ.NRO_DOCUMENTO, vtIPJ.Cuit_PersJur, vtIPJ.Obs_Rubrica,
        vtIPJ.identificacion,
        vtIPJ.persona,
        vtIPJ.ID_FONDO_COMERCIO, vtIPJ.id_proceso, vtIPJ.id_pagina,
        vtIPJ.cuil_usuario cuil_usuario_old,vtIPJ.simple_tramite, vtIPJ.Agrupable,
        (select vt.id_subtipo_tramite from NUEVOSUAC.VT_TRAMITES_IPJ_DESA vt where vt.id_tramite = vtIPJ.id_tramite) id_subtipo_tramite,
        vtIPJ.codigo_online,
        (select o.cod_seguridad from ipj.t_ol_entidades o where o.codigo_online = vtIPJ.codigo_online) cod_seguridad,
        vtIPJ.codigo_online,
        ( select count(1)
          from ipj.t_ol_entidades eo
          where
            eo.codigo_online = vtIPJ.codigo_online and
            eo.id_tipo_tramite_ol in (40, 41) -- Denuncias AC y F
        ) Denuncia
      from IPJ.VT_TRAMITE_TECN_IPJ vtIPJ
      where
        VTIPJ.ID_ESTADO < IPJ.TYPES.C_ESTADOS_COMPLETADO and
        ( nvl(v_id_estado, 0) = 0 or vtIPJ.Id_Estado = v_id_estado) and
        ( ( vtIPJ.clase = 1 and
            (vtIPJ.id_ubicacion = IPJ.TYPES.C_AREA_JURIDICO or
             vtIPJ.Id_Tramite_Ipj in ( select distinct acc.Id_Tramite_Ipj
                                               from ipj.t_tramitesipj_acciones  acc join IPJ.t_tipos_accionesipj tacc
                                                   on  Acc.Id_Tipo_Accion = Tacc.Id_Tipo_Accion
                                                 join IPJ.t_tipos_clasif_ipj clas
                                                   on tacc.id_clasif_ipj = CLAS.ID_CLASIF_IPJ
                                               where
                                                 clas.id_ubicacion = IPJ.TYPES.C_AREA_JURIDICO and
                                                 acc.Id_Estado < IPJ.TYPES.C_ESTADOS_COMPLETADO))
          ) or
          (vtIPJ.clase = 2 and vtIPJ.id_tipo_accion in ( select acc.id_tipo_accion
                                           from IPJ.t_tipos_accionesipj acc join IPJ.t_tipos_clasif_ipj clas
                                             on acc.id_clasif_ipj = CLAS.ID_CLASIF_IPJ
                                           where id_ubicacion = IPJ.TYPES.C_AREA_JURIDICO))
        )
      order by Denuncia desc, Clase desc, urgente, Sticker;

  END SP_Traer_TramitesIPJ_Juridico;

  PROCEDURE SP_Traer_TramitesIPJ_RRHH(
    v_id_estado in number,
    p_Cursor OUT types.cursorType)
  IS
  /****************************************************
   Este procedimiento muestra los tramites y acciones asignadas a un t�cnico o
   a un grupo al cual pertenezca.
   Al unir los datos, indica en la primer columna si es TRAMITE o ACCION
  *****************************************************/
  BEGIN
    OPEN p_Cursor FOR
      select
        vtIPJ.botones,
        vtIPJ.Clase, vtIPJ.id_tramite Id_Tramite_Suac, vtIPJ.Sticker,
        vtIPJ.fecha_ini_suac Fecha_Inicio_Suac,  null Fecha_recepcion,
        (case vtIPJ.id_proceso
           when 4 then 'GENERACION DE HISTORICOS - ' || VTIPJ.N_UBICACION
           when 5 then 'CARGA DE HISTORICOS'
           else IPJ.TRAMITES_SUAC.FC_BUSCAR_SUBTIPO_SUAC(vtIPJ.id_tramite)
        end) Tipo,
        (case vtIPJ.id_proceso
            when 4 then 'GENERACION DE HISTORICOS - ' || VTIPJ.N_UBICACION
            when 5 then vtIPJ.observacion
            else IPJ.TRAMITES_SUAC.FC_BUSCAR_ASUNTO_SUAC(vtIPJ.id_tramite)
        end) Asunto,
        vtIPJ.Expediente Nro_Expediente,
        vtIPJ.Id_Tramite_Ipj, vtIPJ.id_clasif_ipj, vtIPJ.id_tipo_accion, vtIPJ.Observacion, vtIPJ.id_estado,
        vtIPJ.cuil_usuario, vtIPJ.n_estado, vtIPJ.Tipo_IPJ,
        vtIPJ.Fecha_Asignacion,
        vtIPJ.Acc_Abiertas,
        vtIPJ.Acc_Cerradas, vtIPJ.URGENTE,
        vtIPJ.usuario, nvl(vtIPJ.Error_Dato, 'N') Error_Dato,
        vtIPJ.CUIT, vtIPJ.razon_social, vtIPJ.id_legajo,
        vtIPJ.N_UBICACION, vtIPJ.CUIL_CREADOR, vtIPJ.id_ubicacion, vtIPJ.id_tramiteipj_accion,
        vtIPJ.id_protocolo,
        vtIPJ.ID_INTEGRANTE, vtIPJ.NRO_DOCUMENTO, vtIPJ.Cuit_PersJur, vtIPJ.Obs_Rubrica,
        vtIPJ.identificacion,
        vtIPJ.persona,
        vtIPJ.ID_FONDO_COMERCIO, vtIPJ.id_proceso, vtIPJ.id_pagina,vtIPJ.simple_tramite,
        vtIPJ.Agrupable,
        (select vt.id_subtipo_tramite from NUEVOSUAC.VT_TRAMITES_IPJ_DESA vt where vt.id_tramite = vtIPJ.id_tramite) id_subtipo_tramite
      from IPJ.VT_TRAMITE_TECN_IPJ vtIPJ
      where
        VTIPJ.ID_ESTADO < IPJ.TYPES.C_ESTADOS_COMPLETADO and
        ( nvl(v_id_estado, 0) = 0 or vtIPJ.Id_Estado = v_id_estado) and
        ( (vtIPJ.clase = 1 and vtIPJ.id_ubicacion = IPJ.TYPES.C_AREA_RRHH) or
          (vtIPJ.clase = 2 and vtIPJ.id_tipo_accion in ( select acc.id_tipo_accion
                                           from IPJ.t_tipos_accionesipj acc join IPJ.t_tipos_clasif_ipj clas
                                             on acc.id_clasif_ipj = CLAS.ID_CLASIF_IPJ
                                           where id_ubicacion = IPJ.TYPES.C_AREA_RRHH))
       )
    order by Clase desc, urgente, Sticker;

  END SP_Traer_TramitesIPJ_RRHH;

  PROCEDURE SP_Traer_TramitesIPJ_Direccion(
    v_id_estado in number,
    p_Cursor OUT types.cursorType)
  IS
  /****************************************************
   Este procedimiento muestra los tramites y acciones asignadas a un t�cnico o
   a un grupo al cual pertenezca.
   Al unir los datos, indica en la primer columna si es TRAMITE o ACCION
  *****************************************************/
  BEGIN
    OPEN p_Cursor FOR
      select
        vtIPJ.botones,
        vtIPJ.Clase, vtIPJ.id_tramite Id_Tramite_Suac, vtIPJ.Sticker,
        vtIPJ.fecha_ini_suac Fecha_Inicio_Suac,  null Fecha_recepcion,
        (case vtIPJ.id_proceso
           when 4 then 'GENERACION DE HISTORICOS - ' || VTIPJ.N_UBICACION
           when 5 then 'CARGA DE HISTORICOS'
           else IPJ.TRAMITES_SUAC.FC_BUSCAR_SUBTIPO_SUAC(vtIPJ.id_tramite)
        end) Tipo,
        (case vtIPJ.id_proceso
            when 4 then 'GENERACION DE HISTORICOS - ' || VTIPJ.N_UBICACION
            when 5 then vtIPJ.observacion
            else IPJ.TRAMITES_SUAC.FC_BUSCAR_ASUNTO_SUAC(vtIPJ.id_tramite)
        end) Asunto,
        vtIPJ.Expediente Nro_Expediente,
        vtIPJ.Id_Tramite_Ipj, vtIPJ.id_clasif_ipj, vtIPJ.id_tipo_accion, vtIPJ.Observacion, vtIPJ.id_estado,
        vtIPJ.cuil_usuario, vtIPJ.n_estado, vtIPJ.Tipo_IPJ,
        vtIPJ.Fecha_Asignacion,
        vtIPJ.Acc_Abiertas,
        vtIPJ.Acc_Cerradas, vtIPJ.URGENTE,
        vtIPJ.usuario, nvl(vtIPJ.Error_Dato, 'N') Error_Dato,
        vtIPJ.CUIT, vtIPJ.razon_social, vtIPJ.id_legajo,
        vtIPJ.N_UBICACION, vtIPJ.CUIL_CREADOR, vtIPJ.id_ubicacion, vtIPJ.id_tramiteipj_accion,
        vtIPJ.id_protocolo,
        vtIPJ.ID_INTEGRANTE, vtIPJ.NRO_DOCUMENTO, vtIPJ.Cuit_PersJur, vtIPJ.Obs_Rubrica,
        vtIPJ.identificacion,
        vtIPJ.persona,
        vtIPJ.ID_FONDO_COMERCIO, vtIPJ.id_proceso, vtIPJ.id_pagina,
        vtIPJ.cuil_usuario cuil_usuario_old, vtIPJ.simple_tramite, vtIPJ.Agrupable,
        (select vt.id_subtipo_tramite from NUEVOSUAC.VT_TRAMITES_IPJ_DESA vt where vt.id_tramite = vtIPJ.id_tramite) id_subtipo_tramite
      from IPJ.VT_TRAMITE_TECN_IPJ vtIPJ
      where
        ( nvl(v_id_estado, 0) = 0 or vtIPJ.Id_Estado = v_id_estado) and
        ( (vtIPJ.clase = 1 and vtIPJ.id_ubicacion = IPJ.TYPES.C_AREA_DIRECCION) or
          (vtIPJ.clase = 2 and vtIPJ.id_tipo_accion in ( select acc.id_tipo_accion
                                           from IPJ.t_tipos_accionesipj acc join IPJ.t_tipos_clasif_ipj clas
                                             on acc.id_clasif_ipj = CLAS.ID_CLASIF_IPJ
                                           where id_ubicacion = IPJ.TYPES.C_AREA_DIRECCION ))
       )
     order by Clase desc, urgente, Sticker;

  END SP_Traer_TramitesIPJ_Direccion;

  PROCEDURE SP_Traer_TramitesIPJ_Archivo(
    v_id_estado in number,
    p_Cursor OUT types.cursorType)
  IS
  /****************************************************
   Este procedimiento muestra los tramites y acciones asignadas a un t�cnico o
   a un grupo al cual pertenezca.
   Al unir los datos, indica en la primer columna si es TRAMITE o ACCION
  *****************************************************/
  BEGIN
    OPEN p_Cursor FOR
      select
        vtIPJ.botones, vtIPJ.Clase, vtIPJ.id_tramite Id_Tramite_Suac, vtIPJ.Sticker,
        vtIPJ.fecha_ini_suac Fecha_Inicio_Suac,  null Fecha_recepcion,
        (case vtIPJ.id_proceso
           when 4 then 'GENERACION DE HISTORICOS - ' || VTIPJ.N_UBICACION
           when 5 then 'CARGA DE HISTORICOS'
           else IPJ.TRAMITES_SUAC.FC_BUSCAR_SUBTIPO_SUAC(vtIPJ.id_tramite)
        end) Tipo,
        (case vtIPJ.id_proceso
            when 4 then 'GENERACION DE HISTORICOS - ' || VTIPJ.N_UBICACION
            when 5 then vtIPJ.observacion
            else IPJ.TRAMITES_SUAC.FC_BUSCAR_ASUNTO_SUAC(vtIPJ.id_tramite)
        end) Asunto,
        vtIPJ.Expediente Nro_Expediente, vtIPJ.Id_Tramite_Ipj, vtIPJ.id_clasif_ipj,
        vtIPJ.id_tipo_accion, vtIPJ.Observacion, vtIPJ.id_estado, vtIPJ.cuil_usuario,
        vtIPJ.n_estado, vtIPJ.Tipo_IPJ, vtIPJ.Fecha_Asignacion, vtIPJ.Acc_Abiertas,
        vtIPJ.Acc_Cerradas, vtIPJ.URGENTE, vtIPJ.usuario, nvl(vtIPJ.Error_Dato, 'N') Error_Dato,
        vtIPJ.CUIT, vtIPJ.razon_social, vtIPJ.id_legajo, vtIPJ.N_UBICACION,
        vtIPJ.CUIL_CREADOR, vtIPJ.id_ubicacion, vtIPJ.id_tramiteipj_accion,
        vtIPJ.id_protocolo, vtIPJ.ID_INTEGRANTE, vtIPJ.NRO_DOCUMENTO,
        vtIPJ.Cuit_PersJur, vtIPJ.Obs_Rubrica, vtIPJ.identificacion, vtIPJ.persona,
        vtIPJ.ID_FONDO_COMERCIO, vtIPJ.id_proceso, vtIPJ.id_pagina,
        nvl(at.id_archivo_tramite, 0) id_archivo_tramite, vtIPJ.cuil_usuario cuil_usuario_old,
        vtIPJ.simple_tramite, vtIPJ.Agrupable, vtIPJ.n_ubicacion_origen,
        (select vt.id_subtipo_tramite from NUEVOSUAC.VT_TRAMITES_IPJ_DESA vt where vt.id_tramite = vtIPJ.id_tramite) id_subtipo_tramite
      from IPJ.VT_TRAMITE_TECN_IPJ vtIPJ left join IPJ.t_archivo_tramite at
          on at.Id_Tramite_Ipj = vtIPJ.Id_Tramite_Ipj and at.id_tramiteipj_accion = vtIPJ.id_tramiteipj_accion
      where
        VTIPJ.ID_ESTADO < IPJ.TYPES.C_ESTADOS_RECHAZADO and
        ( nvl(v_id_estado, 0) = 0 or vtIPJ.Id_Estado = v_id_estado) and
        (  (vtIPJ.clase = 1 and VTIPJ.ID_ESTADO >= IPJ.TYPES.C_ESTADOS_COMPLETADO and
           ( vtIPJ.id_ubicacion = IPJ.TYPES.C_AREA_ARCHIVO or
             vtIPJ.Id_Tramite_Ipj in ( select distinct acc.Id_Tramite_Ipj
                                              from ipj.t_tramitesipj_acciones  acc join IPJ.t_tipos_accionesipj tacc
                                                  on  Acc.Id_Tipo_Accion = Tacc.Id_Tipo_Accion
                                                join IPJ.t_tipos_clasif_ipj clas
                                                  on tacc.id_clasif_ipj = CLAS.ID_CLASIF_IPJ
                                              where
                                                clas.id_ubicacion = IPJ.TYPES.C_AREA_ARCHIVO and
                                                acc.Id_Estado < IPJ.TYPES.C_ESTADOS_COMPLETADO))
         ) or
          (vtIPJ.clase = 2 and vtIPJ.id_tipo_accion in ( select acc.id_tipo_accion
                                            from IPJ.t_tipos_accionesipj acc join IPJ.t_tipos_clasif_ipj clas
                                              on acc.id_clasif_ipj = CLAS.ID_CLASIF_IPJ
                                            where id_ubicacion = IPJ.TYPES.C_AREA_ARCHIVO) and
          VTIPJ.ID_ESTADO < IPJ.TYPES.C_ESTADOS_COMPLETADO
        ) )
     order by Clase desc, urgente, Sticker;

  END SP_Traer_TramitesIPJ_Archivo;

  PROCEDURE SP_Traer_TramitesIPJ_MesaSUAC(
    v_id_estado in number,
    p_Cursor OUT types.cursorType)
  IS
  /****************************************************
   Este procedimiento muestra los tramites y acciones asignadas a un t�cnico o
   a un grupo al cual pertenezca.
   Al unir los datos, indica en la primer columna si es TRAMITE o ACCION
  *****************************************************/
  BEGIN
    OPEN p_Cursor FOR
      select
        vtIPJ.botones, vtIPJ.Clase, vtIPJ.id_tramite Id_Tramite_Suac, vtIPJ.Sticker,
        vtIPJ.fecha_ini_suac Fecha_Inicio_Suac,  null Fecha_recepcion,
        (case vtIPJ.id_proceso
           when 4 then 'GENERACION DE HISTORICOS - ' || VTIPJ.N_UBICACION
           when 5 then 'CARGA DE HISTORICOS'
           else IPJ.TRAMITES_SUAC.FC_BUSCAR_SUBTIPO_SUAC(vtIPJ.id_tramite)
        end) Tipo,
        (case vtIPJ.id_proceso
            when 4 then 'GENERACION DE HISTORICOS - ' || VTIPJ.N_UBICACION
            when 5 then vtIPJ.observacion
            else IPJ.TRAMITES_SUAC.FC_BUSCAR_ASUNTO_SUAC(vtIPJ.id_tramite)
        end) Asunto,
        vtIPJ.Expediente Nro_Expediente, vtIPJ.Id_Tramite_Ipj, vtIPJ.id_clasif_ipj, vtIPJ.id_tipo_accion,
        vtIPJ.Observacion, vtIPJ.id_estado, vtIPJ.cuil_usuario, vtIPJ.n_estado, vtIPJ.Tipo_IPJ,
        vtIPJ.Fecha_Asignacion, vtIPJ.Acc_Abiertas, vtIPJ.Acc_Cerradas, vtIPJ.URGENTE,
        vtIPJ.usuario, nvl(vtIPJ.Error_Dato, 'N') Error_Dato, vtIPJ.CUIT, vtIPJ.razon_social, vtIPJ.id_legajo,
        vtIPJ.N_UBICACION, vtIPJ.CUIL_CREADOR, vtIPJ.id_ubicacion, vtIPJ.id_tramiteipj_accion,
        vtIPJ.id_protocolo, vtIPJ.ID_INTEGRANTE, vtIPJ.NRO_DOCUMENTO, vtIPJ.Cuit_PersJur, vtIPJ.Obs_Rubrica,
        vtIPJ.identificacion, vtIPJ.persona, vtIPJ.ID_FONDO_COMERCIO, vtIPJ.id_proceso, vtIPJ.id_pagina,
        nvl(at.id_archivo_tramite, 0) id_archivo_tramite, vtIPJ.cuil_usuario cuil_usuario_old,
        vtIPJ.simple_tramite, vtIPJ.Agrupable
      from IPJ.VT_TRAMITE_TECN_IPJ vtIPJ left join IPJ.t_archivo_tramite at
         on at.Id_Tramite_Ipj = vtIPJ.Id_Tramite_Ipj and at.id_tramiteipj_accion = vtIPJ.id_tramiteipj_accion
      where
        VTIPJ.ID_ESTADO < IPJ.TYPES.C_ESTADOS_RECHAZADO and
        ( nvl(v_id_estado, 0) = 0 or vtIPJ.Id_Estado = v_id_estado) and
        ( (vtIPJ.clase = 1 and VTIPJ.ID_ESTADO >= IPJ.TYPES.C_ESTADOS_COMPLETADO and
           ( vtIPJ.id_ubicacion = IPJ.TYPES.c_area_mesaSuac or
             vtIPJ.Id_Tramite_Ipj in ( select distinct acc.Id_Tramite_Ipj
                                              from ipj.t_tramitesipj_acciones  acc join IPJ.t_tipos_accionesipj tacc
                                                  on  Acc.Id_Tipo_Accion = Tacc.Id_Tipo_Accion
                                                join IPJ.t_tipos_clasif_ipj clas
                                                  on tacc.id_clasif_ipj = CLAS.ID_CLASIF_IPJ
                                              where
                                                clas.id_ubicacion = IPJ.TYPES.c_area_mesaSuac and
                                                acc.Id_Estado < IPJ.TYPES.C_ESTADOS_COMPLETADO))
          ) or
          (vtIPJ.clase = 2 and vtIPJ.id_tipo_accion in ( select acc.id_tipo_accion
                                            from IPJ.t_tipos_accionesipj acc join IPJ.t_tipos_clasif_ipj clas
                                              on acc.id_clasif_ipj = CLAS.ID_CLASIF_IPJ
                                            where id_ubicacion = IPJ.TYPES.c_area_mesaSuac) and
          VTIPJ.ID_ESTADO < IPJ.TYPES.C_ESTADOS_COMPLETADO
        ) )
     order by Clase desc, urgente, Sticker;

  END SP_Traer_TramitesIPJ_MesaSUAC;


  PROCEDURE SP_Traer_TramitesIPJ_Hist(
    p_Cuil in varchar2,
    p_id_estado in number,
    p_tipo_tramite in number,
    p_fecha_desde varchar2,
    p_fecha_hasta varchar2,
    o_Cursor OUT types.cursorType)
  IS
    v_valid_parametros varchar (200);
  /****************************************************
   Devuelve el listado de tramites de alguna persona en algun rango de tiempo.
  *****************************************************/
  BEGIN
    if p_fecha_desde is not null and IPJ.VARIOS.valida_fecha(p_fecha_desde) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT');
    end if;

    if p_fecha_hasta is not null and IPJ.VARIOS.valida_fecha(p_fecha_hasta) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT');
    end if;

    if v_valid_parametros is not null then
      return;
    end if;

    OPEN o_Cursor FOR
    select distinct VT_SUAC.ID_TRAMITE Id_Tramite_Suac, nvl(tIPJ.Sticker, VT_SUAC.nro_sticker) Sticker, nvl(tIPJ.Expediente, VT_SUAC.nro_tramite) Nro_Expediente,
      VT_SUAC."Tipo/Subtipo" Tipo, VT_SUAC.Asunto, /*VT_SUAC."Iniciador" */ ''  Iniciador,
      to_char(VT_SUAC.fecha_inicio, 'dd/mm/rrrr') Fecha_Inicio,
      to_char(VT_SUAC.fecha_ultima_recepcion, 'dd/mm/rrrr') Fecha_recepcion,
      tIPJ.Id_Tramite_Ipj, tIPJ.id_clasif_ipj , tIPJ.Observacion, tIPJ.id_estado_ult,
      tIPJ.cuil_ult_estado, e.n_estado, u.descripcion usuario, '' N_TIPO_TRAMITE,
      IPJ.TRAMITES.FC_Ultima_Fecha_Tramite (tIPJ.Id_Tramite_Ipj) Fecha_Asignacion,
      0 ID_PROTOCOLO
    from
      NUEVOSUAC.VT_TRAMITES_IPJ_DESA vt_suac join IPJ.t_tramitesIPJ tIPJ
        on VT_SUAC.ID_TRAMITE = tIPJ.id_tramite
      join IPJ.t_estados e
        on tIPJ.id_estado_ult = e.id_estado
      left join IPJ.t_usuarios u
        on tIPJ.Cuil_Ult_Estado = u.cuil_usuario
      join IPJ.t_tramites_ipj_estado te
        on tIPJ.Id_Tramite_Ipj = te.Id_Tramite_Ipj
    where
      ( ( p_fecha_desde is null
          and te.fecha_pase between sysdate -30 and sysdate) OR
          te.fecha_pase between to_date(p_fecha_desde, 'dd/mm/rrrr') and to_date(p_fecha_hasta, 'dd/mm/rrrr')) and
      ( p_Cuil is null or p_Cuil = te.cuil_usuario ) and
      ( nvl(p_tipo_tramite, 0) = 0 or p_tipo_tramite = tIPJ.ID_CLASIF_IPJ) and
      ( nvl(p_id_estado, 0) = 0 or p_id_estado = tIPJ.id_estado_ult)
      order by VT_SUAC.nro_sticker;
  END SP_Traer_TramitesIPJ_Hist;

  PROCEDURE SP_Traer_TramitesIPJ_ID(
    o_Cursor OUT types.cursorType,
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_Id_Tramite_Ipj out number,
    p_id_tramite_suac in varchar2,
    p_Id_Tramite_Ipj in number,
    p_cuil_usuario in varchar2
    )
  IS
  /****************************************************
   Trae un tramite por ID con  Join  a vista Suac
  *****************************************************/
    v_acciones number(6);
    v_acc_abiertas number(6);
    v_acc_cerradas number(6);
  BEGIN
    if nvl(p_Id_Tramite_Ipj, 0) = 0 then
      -- Si no tiene tramite creado en IPJ, busco si se puede crear por la relacion a SUAC
      IPJ.TRAMITES.Sp_Pasar_SUAC_Gestion(
        o_rdo => o_rdo,
        o_tipo_mensaje => o_tipo_mensaje,
        o_Id_Tramite_Ipj => o_Id_Tramite_Ipj,
        p_cuil_usuario => replace(p_cuil_usuario, '-', ''),
        p_id_tramite_suac => p_id_tramite_suac);
    else
      -- Si ya esta creado, verifico si tiene creada la accion del tipo de SUAC
      IPJ.TRAMITES.Sp_Crear_Accion_Automatica(
        o_rdo => o_rdo,
        o_tipo_mensaje => o_tipo_mensaje,
        p_cuil_usuario => replace(p_cuil_usuario, '-', ''),
        p_Id_Tramite_Ipj => p_Id_Tramite_Ipj);

      o_rdo :=  IPJ.TYPES.C_RESP_OK;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
      o_Id_Tramite_Ipj := p_Id_Tramite_Ipj;
    end if;

    -- Busco las notas de SUAC, si existen el tramite SUAC y GESTION
    if to_number(nvl(p_id_tramite_suac, 0)) <> 0 and nvl(o_Id_Tramite_Ipj, 0) <> 0 then
      IPJ.TRAMITES_SUAC.SP_Bajar_Notas_SUAC(
        o_rdo => o_rdo,
        o_tipo_mensaje => o_tipo_mensaje,
        p_Id_Tramite_Ipj => o_Id_Tramite_Ipj
      );
      if o_tipo_mensaje <> IPJ.TYPES.C_TIPO_MENS_OK then
        return;
      end if;
    end if;


    if nvl(o_Id_Tramite_Ipj, 0) = 0 and to_number(nvl(p_id_tramite_suac, 0)) <> 0 then
      OPEN o_Cursor FOR
        select VT_SUAC.ID_TRAMITE Id_Tramite_Suac, VT_SUAC.nro_sticker Sticker,
          VT_SUAC."Tipo/Subtipo" Tipo, VT_SUAC.Asunto,  VT_SUAC.NRO_TRAMITE Nro_Expediente,
          to_char(VT_SUAC.fecha_inicio, 'dd/mm/rrrr') Fecha_Inicio_Suac,
          to_char(VT_SUAC.fecha_ultima_recepcion, 'dd/mm/rrrr') Fecha_recepcion,
          0 Id_Tramite_Ipj, 0 id_clasif_ipj, null Observacion, 2 id_estado,
          null cuil_usuario, 0 id_grupo, 0 ID_UBICACION,
          null n_estado, null usuario,  null N_Clasif_Ipj,
          null Fecha_Asignacion,
          0 Acc_Abiertas,
          0 Acc_Cerradas,
          'N' URGENTE, 0 ID_UBICACION_ORIGEN, 0 id_proceso,'N' SIMPLE_TRAMITE,
          IPJ.TRAMITES_SUAC.FC_BUSCAR_AUTORIZ_SUAC(vt_suac.id_tramite) Autoridades_SUAC,
          vt_suac.id_subtipo_tramite, 0 Es_Digital, 0 id_motivo_vista
        from
          NUEVOSUAC.VT_TRAMITES_IPJ_DESA vt_suac
        where
          VT_SUAC.ID_TRAMITE = to_number(nvl(p_id_tramite_suac, 0))
        order by VT_SUAC.nro_sticker;

    else
      OPEN o_Cursor FOR
        select nvl(tIPJ.ID_TRAMITE, 0) Id_Tramite_Suac, tIPJ.Sticker,
          (select VT_SUAC."Tipo/Subtipo" from NUEVOSUAC.VT_TRAMITES_IPJ_DESA vt_suac where vt_suac.id_tramite = tIPJ.id_tramite) Tipo, --VT_SUAC."Tipo/Subtipo"
          IPJ.TRAMITES_SUAC.FC_BUSCAR_SUBTIPO_SUAC(tIPJ.id_tramite) Asunto,
          tIPJ.Expediente Nro_Expediente,
          to_char(tIPJ.fecha_ini_suac , 'dd/mm/rrrr') Fecha_Inicio_Suac,
          null Fecha_recepcion, --DIPJ01-837466040-216
          tIPJ.Id_Tramite_Ipj, tIPJ.id_clasif_ipj, tIPJ.Observacion, tIPJ.id_estado_ult id_estado,
          u.cuil_usuario, TIPJ.ID_GRUPO_ULT_ESTADO id_grupo,
          nvl(TIPJ.ID_UBICACION, 0) ID_UBICACION,
          e.n_estado, u.descripcion usuario,  tc.N_Clasif_Ipj, tIPJ.id_tramite_ipj_padre,
          IPJ.TRAMITES.FC_Ultima_Fecha_Tramite (tIPJ.Id_Tramite_Ipj) Fecha_Asignacion,
          IPJ.TRAMITES.FC_Acciones_Abiertas (tIPJ.Id_Tramite_Ipj) Acc_Abiertas,
          IPJ.TRAMITES.FC_Acciones_Cerradas (tIPJ.Id_Tramite_Ipj) Acc_Cerradas,
          TIPJ.URGENTE, tIPJ.ID_UBICACION_ORIGEN, tIPJ.id_proceso,TIPJ.simple_tramite,
          IPJ.TRAMITES_SUAC.FC_BUSCAR_AUTORIZ_SUAC(tIPJ.id_tramite) Autoridades_SUAC,
          (select id_subtipo_tramite from NUEVOSUAC.VT_TRAMITES_IPJ_DESA vt_suac where vt_suac.id_tramite = tIPJ.id_tramite) id_subtipo_tramite,
          FC_Es_Tram_Digital(tIPJ.id_tramite_ipj) Es_Digital, tipj.id_motivo_vista
        from IPJ.t_tramitesIPJ tIPJ left join IPJ.t_estados e
            on tIPJ.id_estado_ult = e.id_estado
          left join IPJ.t_usuarios u
            on tIPJ.Cuil_Ult_Estado = u.cuil_usuario
          left join IPJ.t_tipos_Clasif_ipj tc
            on TIPJ.Id_Clasif_Ipj = tc.Id_Clasif_Ipj
        where
          ( (nvl(o_Id_Tramite_Ipj, 0) = 0 and tIPJ.ID_TRAMITE = to_number(nvl(p_id_tramite_suac, 0)) )
            or tipj.Id_Tramite_Ipj = o_Id_Tramite_Ipj)
        order by tIPJ.sticker;

    end if;

  END SP_Traer_TramitesIPJ_ID;

  PROCEDURE SP_Traer_TramitesIPJ_ID(
    p_id_tramite_suac in varchar2,
    p_Id_Tramite_Ipj in number,
    o_Cursor OUT types.cursorType,
    p_id_tramiteipj_accion in number)
  IS
  /****************************************************
   Trae un tramite por ID con  Join  a vista Suac y a la accion indicada
  *****************************************************/
    v_acciones number(6);
    v_acc_abiertas number(6);
    v_acc_cerradas number(6);
  BEGIN
    if nvl(p_Id_Tramite_Ipj, 0) = 0 and to_number(nvl(p_id_tramite_suac, 0)) <> 0 then
      OPEN o_Cursor FOR
        select VT_SUAC.ID_TRAMITE Id_Tramite_Suac, VT_SUAC.nro_sticker Sticker,
          VT_SUAC."Tipo/Subtipo" Tipo, VT_SUAC.Asunto,  VT_SUAC.NRO_TRAMITE Nro_Expediente,
          to_char(VT_SUAC.fecha_inicio, 'dd/mm/rrrr') Fecha_Inicio_Suac,
          to_char(VT_SUAC.fecha_ultima_recepcion, 'dd/mm/rrrr') Fecha_recepcion,
          0 Id_Tramite_Ipj, 0 id_clasif_ipj, null Observacion, 2 id_estado,
          null cuil_usuario, 0 id_grupo, 0 ID_UBICACION,
          null n_estado, null usuario,  null N_Clasif_Ipj,
          null Fecha_Asignacion,
          0 Acc_Abiertas,
          0 Acc_Cerradas,
          'N' URGENTE, 0 ID_UBICACION_ORIGEN, 0 id_proceso,'N' SIMPLE_TRAMITE,
          IPJ.TRAMITES_SUAC.FC_BUSCAR_AUTORIZ_SUAC(vt_suac.id_tramite) Autoridades_SUAC,
          vt_suac.id_subtipo_tramite, 0 Es_Digital
        from
          NUEVOSUAC.VT_TRAMITES_IPJ_DESA vt_suac
        where
          VT_SUAC.ID_TRAMITE = to_number(nvl(p_id_tramite_suac, 0))
        order by VT_SUAC.nro_sticker;

    else
      OPEN o_Cursor FOR
        select nvl(VT_SUAC.ID_TRAMITE, 0) Id_Tramite_Suac, nvl(VT_SUAC.nro_sticker, tIPJ.Sticker) Sticker,
          VT_SUAC."Tipo/Subtipo" Tipo, VT_SUAC.Asunto,  nvl(VT_SUAC.nro_tramite, tIPJ.Expediente) Nro_Expediente,
          to_char(VT_SUAC.fecha_inicio, 'dd/mm/rrrr') Fecha_Inicio_Suac,
          to_char(VT_SUAC.fecha_ultima_recepcion, 'dd/mm/rrrr') Fecha_recepcion,
          tIPJ.Id_Tramite_Ipj, tIPJ.id_clasif_ipj, tIPJ.Observacion, tIPJ.id_estado_ult id_estado,
          u.cuil_usuario, TIPJ.ID_GRUPO_ULT_ESTADO id_grupo,
          nvl(TIPJ.ID_UBICACION, 0) ID_UBICACION,
          e.n_estado, u.descripcion usuario,  tc.N_Clasif_Ipj, tIPJ.id_tramite_ipj_padre,
          IPJ.TRAMITES.FC_Ultima_Fecha_Tramite (tIPJ.Id_Tramite_Ipj) Fecha_Asignacion,
          IPJ.TRAMITES.FC_Acciones_Abiertas (tIPJ.Id_Tramite_Ipj) Acc_Abiertas,
          IPJ.TRAMITES.FC_Acciones_Cerradas (tIPJ.Id_Tramite_Ipj) Acc_Cerradas,
          TIPJ.URGENTE, tIPJ.ID_UBICACION_ORIGEN, tIPJ.id_proceso,
          tacc.id_documento, tacc.n_documento, tIPJ.simple_tramite,
          IPJ.TRAMITES_SUAC.FC_BUSCAR_AUTORIZ_SUAC(vt_suac.id_tramite) Autoridades_SUAC,
          vt_suac.id_subtipo_tramite,
          FC_Es_Tram_Digital(tIPJ.id_tramite_ipj) Es_Digital
        from
          IPJ.t_tramitesIPJ tIPJ left join NUEVOSUAC.VT_TRAMITES_IPJ_DESA vt_suac
            on VT_SUAC.ID_TRAMITE = to_number(nvl(p_id_tramite_suac, 0))  and VT_SUAC.ID_TRAMITE = tIPJ.id_tramite
          join IPJ.t_tramitesipj_Acciones tacc
            on tIPJ.Id_Tramite_Ipj = tacc.Id_Tramite_Ipj
          left join IPJ.t_estados e
            on tIPJ.id_estado_ult = e.id_estado
          left join IPJ.t_usuarios u
            on tIPJ.Cuil_Ult_Estado = u.cuil_usuario
          left join IPJ.t_tipos_Clasif_ipj tc
            on TIPJ.Id_Clasif_Ipj = tc.Id_Clasif_Ipj
        where
           tipj.Id_Tramite_Ipj = p_Id_Tramite_Ipj and
           tacc.id_tramiteipj_accion = p_id_tramiteipj_accion
        order by VT_SUAC.nro_sticker;
    end if;
  END SP_Traer_TramitesIPJ_ID;

  PROCEDURE SP_Traer_TramitesIPJ_SRL(
    p_Cursor OUT types.cursorType)
  IS
  /****************************************************
   Lista la cantidad de tramites por estado que tienen cada usuario, descartando pendientes y cerrados.
  *****************************************************/
  BEGIN
    OPEN p_Cursor FOR
    select cuil_ult_estado, u.descripcion,
      sum(case when id_estado_ult = 2 then 1 else 0 end) tramites_asignados,
      sum(case when id_estado_ult = 3 then 1 else 0 end) tramites_iniciados,
      sum(case when id_estado_ult = 4 then 1 else 0 end) tramites_verificacion
    from ipj.t_tramitesIPJ t join ipj.t_usuarios u
      on t.cuil_ult_estado = u.cuil_usuario
    where
      t.id_estado_ult > 1 and t.id_estado_ult < 100
    group by cuil_ult_estado, u.descripcion;
  END SP_Traer_TramitesIPJ_SRL;

  PROCEDURE SP_GUARDAR_TRAMITE(
    o_Id_Tramite_Ipj out number,
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    v_Id_Tramite_Ipj in number,
    v_id_tramite IN varchar2,
    v_id_Clasif_IPJ IN NUMBER,
    v_id_Grupo IN number,
    v_cuil_ult_estado in varchar2,
    v_id_estado in number,
    v_cuil_creador in varchar2,
    v_observacion IN varchar2,
    v_id_Ubicacion in number,
    v_urgente in varchar2,
    p_sticker in varchar2,
    p_expediente in varchar2,
    p_simple_tramite in varchar2)
  IS
   /****************************************************
   Guarda los datos de un tramite.
   Actualiza el historial de estados.
   Si se pasa a terminado, actualiza los datos en Personas Juridicas
   La transaccion la administra la aplicacion al guardar cabecera y detalles
  *****************************************************/
    p_Id_Tramite_Ipj NUMBER;
    v_rdo_estado varchar2(2000);
    v_rdo_PersJur varchar2(2000);
    v_bloque varchar2(1000);
    v_valid_parametros varchar2(3000);
    v_mensaje varchar2(1000);
    v_Cursor_PersJur types.cursorType;
    v_Cursor_Acc types.cursorType;
    v_SUAC IPJ.TRAMITES_SUAC.Tipo_SUAC;
    v_id_Estado_new number;
    v_Row_Tramite_PersJur  IPJ.T_TramitesIPJ_PersJur%ROWTYPE;
    v_Row_Acciones  IPJ.T_TramitesIPJ_Acciones%ROWTYPE;
    v_Row_Tramite  IPJ.T_TramitesIPJ%ROWTYPE;
    v_Row_Entidad  IPJ.T_Entidades%ROWTYPE;
    v_Row_Legajo  IPJ.T_Legajos%ROWTYPE;
    v_metodo varchar2(200);
    v_origen varchar2(500);
    v_tipo_mensaje number;
    v_sticker varchar2(50);
    v_expediente varchar2(50);
    v_existe number;
    v_existe_Acc number;
    v_id_tramiteIpj_Accion number;
    v_cuil_usuario varchar2(11);
    v_id_legajo number;
    v_id_ubicacion_ent number;
    v_existen_observadas number;
    v_fecha_ini_suac date;
    v_id_archivo_tramite number;
    v_Es_Anexo number;
    v_tiene_anexos number;
    v_id_integrante number;
    v_dni_suac varchar2(20);
    v_cant_notas number;
    v_tipo_tramite_suac varchar2(50);
    v_resp_grupo_dest varchar2(20);
    v_resp_grupo_or varchar2(20);
    v_cod_mesa_suac varchar2(20);
    v_Id_Tramite_Ipj_Padre NUMBER;
    v_Nro_Tramite VARCHAR2(50);
    v_cant_sincroniz number;

  BEGIN
    if IPJ.Types.c_habilitar_log_SP = 'S' then
      IPJ.VARIOS.SP_GUARDAR_LOG(
        p_METODO => 'SP_GUARDAR_TRAMITE',
        p_NIVEL => 'Tramites Online',
        p_ORIGEN => 'DB',
        p_USR_LOG  => 'DB',
        p_USR_WINDOWS => 'Sistema',
        p_IP_PC  => null,
        p_EXCEPCION =>
          'Id Tramite IPJ = ' || to_char(v_Id_Tramite_Ipj)
          || ' / Id Tramite = ' || v_id_tramite
          || ' / Id Clasif = ' || to_char(v_id_Clasif_IPJ)
          || ' / Id Grupo = ' || to_char(v_id_Grupo)
          || ' / Cuil Ult Estado = ' || v_cuil_ult_estado
          || ' / Id Estado = ' || to_char(v_id_estado)
          || ' / Cuil Creador = ' || v_cuil_creador
          || ' / Obs = ' || v_observacion
          || ' / Id Ubicacion = ' || to_char(v_id_Ubicacion)
          || ' / Urgente = ' || v_urgente
          || ' / Sticker = ' || p_sticker
          || ' / Expediente = ' || p_expediente
          || ' / Simple Tr = ' || p_simple_tramite
      );
    end if;

    -- Busco el tipo de proceso parra saber que tipo de tramite es
    begin
      select * into v_Row_Tramite
      from IPJ.t_tramitesipj
      where
       Id_Tramite_Ipj = v_Id_Tramite_Ipj;
    exception
      when OTHERS then
        v_Row_Tramite.id_proceso := 0;
        v_Row_Tramite.id_ubicacion_origen := 0;
        v_Row_Tramite.id_estado_ult := 0;
    end;

    -- Verifico si tiene anexados - PBI6281:[SG] - [Anexados]
    BEGIN
      SELECT count(1)
        INTO v_tiene_anexos
        FROM ipj.t_tramitesipj t
       WHERE t.id_tramite_ipj_padre = v_Id_Tramite_Ipj;
    EXCEPTION
      WHEN OTHERS THEN
        v_tiene_anexos := 0;
    END;

    DBMS_OUTPUT.PUT_LINE('1- SP_GUARDAR_TRAMITE - Validacion de Parametros');
    -- VALIDACION DE PARAMETROS DE ENTARDA
    v_bloque := 'Parametros: ';
    if nvl(v_Row_Tramite.id_proceso, 0) not in (4, 5) and (v_id_tramite is null  or IPJ.VARIOS.VALIDA_TRAMITE_SUAC(v_id_tramite, v_Id_Tramite_Ipj) > 0) then
      -- verifica que el tramite suac exista y no este asociado a otro tramite ipj
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('SUAC_NOT') || ' (Tr. IPJ = ' || to_char(v_Id_Tramite_Ipj) || ' - SUAC = ' || to_char(v_id_tramite) || ')' ;
    end if;
    if IPJ.VARIOS.Valida_Estado(v_id_estado) = 0 then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('EST_NOT');
    end if;
    if v_cuil_ult_estado is not null and IPJ.VARIOS.Valida_Usuario(v_cuil_ult_estado) = 0 then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('USR_NOT');
    end if;

    -- Si fallaron los parametros de entrada no continuo
    if v_valid_parametros is not null then
      o_rdo := v_bloque || v_valid_parametros;
      o_Id_Tramite_Ipj := 0;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    -- Si estoy insertando, y ya existe uno igual, no hago nada
    if v_id_tramite is not null and v_Id_Tramite_Ipj = 0 then
      select max(Id_Tramite_Ipj) into v_existe
      from IPJ.t_tramitesipj
      where id_tramite = v_id_tramite;

      if nvl(v_existe, 0) > 0 then
        o_rdo := TYPES.C_RESP_OK;
        o_Id_Tramite_Ipj := v_existe;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
        return;
      end if;
    end if;

    /***********************************
    Si el tramite ya estaba cerrado, no acepto los cambios
    ******************************************/
    if v_Row_Tramite.id_estado_ult = IPJ.TYPES.c_Estados_Cerrado and  v_id_ubicacion <> IPJ.TYPES.C_AREA_ARCHIVO then
      o_rdo := 'ERROR: El tr�mite ' || v_id_tramite || ' ya se encuentra cerrado, refresque sus datos.';
      o_Id_Tramite_Ipj := 0;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    -- (PBI 11337) Controlo que para pasar a OBSERVADO un tr�mite digital, exista alguna observacion pendiente
    if (FC_Es_Tram_Digital(v_id_tramite_ipj) > 0) and v_id_estado = IPJ.TYPES.C_ESTADOS_OBSERVADO then
      select count(*) into v_existen_observadas
      from IPJ.t_Entidades_notas
      where
        Id_Tramite_Ipj = v_Id_Tramite_Ipj and
        nvl(id_estado_nota, 0)  not in (1, 3); -- Notas OK o SIN EFECTO

      if v_existen_observadas = 0 then
        o_rdo := 'El tr�mite ' || v_Row_Tramite.expediente || ' debe tener al menos una observaci�n en estado Pendiente u Observado para realizar esta acci�n. Verifique los datos e intente nuevamente.';
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
        o_Id_Tramite_Ipj := 0;
        return;
      end if;

    end if;

    -- Se se rechaza el tr�mite, se debe cargar una observaci�n
    if v_id_estado = IPJ.TYPES.c_Estados_Rechazado then
      select count(*) into v_existen_observadas
      from IPJ.t_Entidades_notas
      where
        Id_Tramite_Ipj = v_Id_Tramite_Ipj;

      if v_existen_observadas = 0 then
        o_rdo := 'El tr�mite ' || v_Row_Tramite.expediente || ' debe tener al menos una observaci�n.';
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
        o_Id_Tramite_Ipj := 0;
        return;
      end if;
    end if;

    /*********************************************************
      **************** CUERPO DEL PROCEDIMIENTO *****************
    *********************************************************/
    v_id_estado_new := v_id_estado;
    if nvl(v_Row_Tramite.id_proceso, 0) = IPJ.TYPES.C_PROCESO_GEN_HIST then
       -- EL TRAMITE GENERA HISTORICOS
      if v_Row_Tramite.id_estado_ult in (IPJ.TYPES.c_Estados_Completado, IPJ.Types.c_Estados_Cerrado, IPJ.Types.c_Estados_Rechazado, IPJ.Types.c_Estados_Perimido) then
         o_rdo := 'ERROR: No es posible finalizar este tr�mite';
         o_Id_Tramite_Ipj := 0;
         o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
         return;
      else
        -- Genero un nuevo tramite con los datos pasados, nuevo id, y id_proceso hist�rico
        SELECT ipj.SEQ_tramites.nextval INTO p_Id_Tramite_Ipj FROM dual;

        insert into ipj.t_tramitesipj
          (Id_Tramite_Ipj, id_tramite, id_clasif_ipj, id_grupo_ult_estado, cuil_ult_estado,
           id_estado_ult, fecha_inicio, cuil_creador, observacion, id_ubicacion, urgente,
           id_ubicacion_origen, id_proceso, sticker, expediente,simple_tramite)
        values
          (p_Id_Tramite_Ipj, v_id_tramite, v_id_clasif_ipj, v_id_grupo, v_cuil_ult_estado,
           v_id_estado, to_date(sysdate, 'dd/mm/rrrr'), v_cuil_creador, v_observacion, v_id_ubicacion, v_urgente,
           v_id_ubicacion, IPJ.TYPES.C_PROCESO_HIST, p_sticker, p_expediente, null);
      end if;

    else -- EL TRAMITE ES COMUN O HISTORICO
      -- Controlo si debo redefinir el estado
      -- Si se pasa de SRL, SxA, ACyF a Archivo, pasar a estado Completo
      if (v_Row_Tramite.id_ubicacion <> v_id_ubicacion and
           v_Row_Tramite.id_ubicacion not in (IPJ.TYPES.C_AREA_MESASUAC, IPJ.TYPES.C_AREA_ARCHIVO) and
           v_id_ubicacion in (IPJ.TYPES.C_AREA_ARCHIVO) and
           v_id_estado < IPJ.TYPES.C_ESTADOS_COMPLETADO ) then

        v_id_estado_new := IPJ.TYPES.C_ESTADOS_COMPLETADO;
      end if;

      -- Si se pasa de Archivo o Mesa a un area SRL, SxA, ACyF, se debe pasar el estado a En Proceso
      if (v_Row_Tramite.id_ubicacion <> v_id_ubicacion and
           v_Row_Tramite.id_ubicacion in (IPJ.TYPES.C_AREA_MESASUAC, IPJ.TYPES.C_AREA_ARCHIVO) and
           v_id_ubicacion not in (IPJ.TYPES.C_AREA_MESASUAC, IPJ.TYPES.C_AREA_ARCHIVO) and
           v_id_estado = IPJ.TYPES.C_ESTADOS_COMPLETADO ) then

        v_id_estado_new := IPJ.TYPES.C_ESTADOS_PROCESO;
      end if;

      -- Si se pasa a Cerrado o Rechazado, no debe tener acciones abiertas
      if v_id_estado_new in (IPJ.TYPES.c_Estados_Cerrado, IPJ.TYPES.c_Estados_Rechazado) and FC_Acciones_Abiertas (v_Id_Tramite_Ipj) > 0 then
         o_rdo := 'ERROR: No se puede Cerrar el Tr�mite porque posee Acciones Abiertas';
         o_Id_Tramite_Ipj := 0;
         o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
         return;
      end if;

      -- Si fuera de Archivo, se queire marcar como Completado, no debe tener acciones abiertas
      if v_id_ubicacion <> IPJ.TYPES.C_AREA_ARCHIVO and v_id_estado_new = IPJ.TYPES.c_Estados_Completado and FC_Acciones_Abiertas (v_Id_Tramite_Ipj) > 0 then
         o_rdo := 'ERROR: No se puede Completar el Tr�mite porque posee Acciones Abiertas';
         o_Id_Tramite_Ipj := 0;
         o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
         return;
      end if;

      -- Controlo que existan al menos tantas acciones como personas fisicas y juridicas
      if nvl(p_simple_tramite, 'N') <> 'S' and (v_id_estado_new in (IPJ.TYPES.c_Estados_Completado, IPJ.TYPES.c_Estados_Cerrado) and FC_Control_Tramite (v_Id_Tramite_Ipj) < 0) then
        o_rdo := 'ERROR: No se puede Completar/Cerrar el Tr�mite porque hay menos Acciones que Personas asociadas al tramite.';
        o_Id_Tramite_Ipj := 0;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
        return;
      end if;

      -- Para SRL, SxA o ACyF, debe haber por lo menos 1 persona fisica, juridica o fondo assignado al tr�mite.
      if v_Row_Tramite.id_ubicacion_origen in (IPJ.TYPES.c_Area_SRL, IPJ.TYPES.c_Area_SxA, IPJ.TYPES.c_Area_CyF) and
        v_id_estado_new >= IPJ.TYPES.c_Estados_Completado  and
        v_Row_Tramite.id_clasif_ipj not in (11, 13) and -- Solo se permite para clasificacion Reserva de SxA y ACyF
        FC_Control_Existe_Pers(v_Id_Tramite_Ipj) = 0
        then
        o_rdo := 'ERROR: No se puede Completar/Cerrar el Tr�mite sin Personas asociadas';
        o_Id_Tramite_Ipj := 0;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
        return;
      end if;

      v_bloque := 'Tramite IPJ: ';

      -- Verifico si el tr�mite se encuetra anexado
      select nvl(sum(v_id_padre), 0) into v_Es_Anexo
      from Nuevosuac.Vt_Get_Tramites_Ipj_Anexos
      where
         v_id = v_id_tramite;

      -- Busco el id_tramite_ipj del padre
      BEGIN
        SELECT t.id_tramite_ipj
          INTO v_Id_Tramite_Ipj_Padre
          FROM ipj.t_tramitesipj t
         WHERE t.id_tramite = v_Es_Anexo;
      EXCEPTION
        WHEN OTHERS THEN
          v_Id_Tramite_Ipj_Padre := NULL;
      END;

      -- Bug #11293: Si el tr�mite est� anexado en SUAC, pero el tr�mite padre no est� en Gesti�n, no permito continuar con el procedimiento
      IF v_Es_Anexo > 0 AND v_Id_Tramite_Ipj_Padre IS NULL THEN

        BEGIN
          SELECT nvl(n.nro_tramite, n.nro_sticker)
            INTO v_Nro_Tramite
            FROM NUEVOSUAC.VT_TRAMITES_IPJ_CRUCE_SGES n
           WHERE n.id_tramite = v_Es_Anexo;
        EXCEPTION
          WHEN OTHERS THEN
            v_Nro_Tramite := NULL;
        END;

        o_rdo := 'ERROR: No se puede Completar/Cerrar el Tr�mite si est� anexado en SUAC al tr�mite '||v_Nro_Tramite||' y el mismo no se encuentra en el Sistema de Gesti�n.';
        o_Id_Tramite_Ipj := 0;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
        return;
      END IF;

      -- Si el tr�mite es real, traigo los datos de expediente, sticker y fecha_inicio
      if v_id_tramite > 0 then
        begin
          select VT_SUAC.NRO_STICKER, VT_SUAC.NRO_TRAMITE, to_date(VT_SUAC.FECHA_INICIO, 'dd/mm/rrrr'),
            vt_suac.nota_expediente
            into v_sticker, v_expediente, v_fecha_ini_suac, v_tipo_tramite_suac
          from NUEVOSUAC.VT_TRAMITES_IPJ_DESA vt_suac
          where
            vt_suac.id_tramite = v_id_tramite;
        exception
          when NO_DATA_FOUND then
            v_sticker := v_Row_Tramite.sticker;
            v_expediente := v_Row_Tramite.expediente;
            v_fecha_ini_suac := v_Row_Tramite.fecha_ini_suac;
        end;
      end if;

      -- (PBI 11337) Si se cambia de unidad, verifico que los permisos en SUAC sean lo correctos
      /*
      if (FC_Es_Tram_Digital(v_id_tramite_ipj) > 0) and  (v_Row_Tramite.id_ubicacion <> v_id_ubicacion) then
       if ipj.tramites_suac.FC_Validar_Tramite_SUAC (v_id_tramite_ipj, v_cuil_ult_estado) <> IPJ.TYPES.C_RESP_OK then
          o_rdo := ipj.tramites_suac.FC_Validar_Tramite_SUAC (p_id_tramite_ipj, v_cuil_ult_estado);
          o_Id_Tramite_Ipj := 0;
          o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
          return;
        end if;
      end if;
     */
      /* Si el tramite existe, solo actualizo el usuario y estado */
      DBMS_OUTPUT.PUT_LINE('2- SP_GUARDAR_TRAMITE - Actualiza Tramite');
      update ipj.t_tramitesipj
      set
        observacion = (case
                                when v_observacion is null then observacion
                                when v_observacion is not null and v_observacion = observacion then observacion
                                else substr(v_observacion || chr(13) || observacion, 1, 2000)
                             end),
        id_estado_ult = (case
                                   when v_es_anexo > 0 and v_id_estado_new not in (IPJ.TYPES.c_Estados_Completado, IPJ.TYPES.c_Estados_Cerrado) then 50 -- Anexado - PBI6281:[SG] - [Anexados]
                                   when v_es_anexo = 0 and v_id_estado_new = 1 and v_id_ubicacion <> IPJ.TYPES.C_AREA_MESASUAC then 2
                                   else v_id_estado_new
                                end),
        cuil_ult_estado = v_cuil_ult_estado,
        id_grupo_ult_estado = v_id_grupo,
        id_clasif_ipj = v_id_clasif_ipj,
        id_ubicacion = v_id_ubicacion,
        urgente = v_urgente,
        sticker = (case when v_id_tramite > 0 then v_sticker else p_sticker end),
        expediente = (case when v_id_tramite > 0 then v_expediente else p_expediente end),
        simple_tramite = null,
        fecha_ini_suac = (case when v_id_tramite > 0 then v_fecha_ini_suac else fecha_ini_suac end),
        id_tramite_ipj_padre = decode(v_es_anexo, 0, NULL, v_Id_Tramite_Ipj_Padre) --Bug #11293

      where
        Id_Tramite_Ipj = nvl(v_Id_Tramite_Ipj, 0);

      /* si no se actualiza nada, agrego el tramite */
      if sql%rowcount = 0 then
        SELECT ipj.SEQ_tramites.nextval INTO p_Id_Tramite_Ipj FROM dual;

        -- busco el sticker y expediente y los guardo para cuando no este la vista
        begin
          select VT_SUAC.NRO_STICKER, VT_SUAC.NRO_TRAMITE, to_date(VT_SUAC.FECHA_INICIO, 'dd/mm/rrrr')
            into v_sticker, v_expediente, v_fecha_ini_suac
          from NUEVOSUAC.VT_TRAMITES_IPJ_DESA vt_suac
          where
            vt_suac.id_tramite = v_id_tramite;
        exception
          when NO_DATA_FOUND then
            v_sticker := null;
            v_expediente := null;
        end;

        insert into ipj.t_tramitesipj
          (Id_Tramite_Ipj, id_tramite, id_clasif_ipj, id_grupo_ult_estado, cuil_ult_estado,
           id_estado_ult, fecha_inicio, cuil_creador, observacion, id_ubicacion, urgente,
           id_ubicacion_origen, id_proceso, sticker, expediente, simple_tramite,
           fecha_ini_suac, id_tramite_ipj_padre)
        values
          (p_Id_Tramite_Ipj, (case when v_id_tramite = 0 then null else v_id_tramite end), v_id_clasif_ipj, v_id_grupo, v_cuil_ult_estado,
          (case when v_es_anexo > 0 then 50 else v_id_estado_new end), sysdate, v_cuil_creador, v_observacion, v_id_ubicacion, v_urgente,
          v_id_ubicacion, (case v_Row_Tramite.id_proceso when 0 then null else v_Row_Tramite.id_proceso end),
          nvl(v_sticker, p_sticker), nvl(v_expediente, p_expediente), null, v_fecha_ini_suac, (case when v_es_anexo = 0 then null else v_Id_Tramite_Ipj_Padre end));--Bug #11293
      else
        p_Id_Tramite_Ipj := v_Id_Tramite_Ipj;
      end if;


      -- Si es un tr�mite ONLINE, y se pasa a algun estado finalizado que no sea inactivo, se actualiza el tr�mite online
      if nvl(v_Row_Tramite.codigo_online, 0) > 0 and v_id_estado >= 100 and v_id_estado <> 210 then
        update ipj.t_ol_entidades
        set id_estado = v_id_estado
        where
          codigo_online = v_Row_Tramite.codigo_online;
      end if;

      --Bug10128:[Anexados] Si el tramite se pasa a Anexado, las acciones abiertas se pasan a cerradas
      if v_id_estado_new = IPJ.TYPES.c_Estados_Anexado then
        update ipj.t_tramitesipj_acciones
        set id_estado = IPJ.Types.c_Estados_Anexado
        where
          id_tramite_ipj = p_Id_Tramite_Ipj and
          id_estado < IPJ.TYPES.C_ESTADOS_COMPLETADO;
      END IF;

      -- (PBI 11337) Para digitales, si el tramite se pasa a Observado, las acciones abiertas se pasan a observadas
      if FC_Es_Tram_Digital(v_id_tramite_ipj) > 0 then
        if  v_id_estado_new = IPJ.TYPES.C_ESTADOS_OBSERVADO then
          update ipj.t_tramitesipj_acciones
          set
            id_estado = v_id_estado_new,
            cuil_usuario = '1' -- Usuario DB
          where
            id_tramite_ipj = p_Id_Tramite_Ipj and
            id_estado < IPJ.TYPES.C_ESTADOS_COMPLETADO;

        else --Si el tramite no esta Observado, las acciones tampoco
          update ipj.t_tramitesipj_acciones
          set id_estado = v_id_estado_new
          where
            id_tramite_ipj = p_Id_Tramite_Ipj and
            id_estado = IPJ.TYPES.C_ESTADOS_OBSERVADO;
        end if;
      end if;

      -- Si el tramite se pasa a PERIMIDO, las acciones abiertas se pasan a Perimidas
      if v_id_estado_new = IPJ.TYPES.C_ESTADOS_PERIMIDO then
        update ipj.t_tramitesipj_acciones
        set id_estado = v_id_estado_new
        where
          id_tramite_ipj = p_Id_Tramite_Ipj and
          id_estado < IPJ.TYPES.C_ESTADOS_COMPLETADO;

        -- Cargo el historial de acciones con el paso a perimidos
        OPEN v_Cursor_Acc FOR
          select ta.*
          from ipj.t_tramitesipj_acciones ta
          where
            ta.id_tramite_ipj = p_Id_Tramite_Ipj and
            ta.id_estado = IPJ.TYPES.C_ESTADOS_PERIMIDO;

        LOOP
          fetch v_Cursor_Acc into v_Row_Acciones;
          EXIT WHEN v_Cursor_Acc%NOTFOUND or v_Cursor_Acc%NOTFOUND is null;

          IPJ.TRAMITES.SP_GUARDAR_TRAMITES_ACC_ESTADO(
            o_rdo => o_rdo,
            o_tipo_mensaje => o_Tipo_mensaje,
            p_fecha_pase => sysdate,
            p_id_estado => v_id_estado_new,
            p_cuil_usuario => v_Row_Acciones.cuil_usuario,
            p_observacion => v_Row_Acciones.observacion,
            p_Id_Tramite_Ipj => v_Row_Acciones.id_tramite_ipj,
            p_id_tramite_Accion => v_Row_Acciones.id_tramiteipj_accion,
            p_id_tipo_accion => v_Row_Acciones.id_tipo_accion,
            p_id_documento => v_Row_Acciones.id_documento,
            p_n_documento => v_Row_Acciones.n_documento );
        end loop;

        CLOSE v_Cursor_Acc;
      end if;

      -- Intento sincronizar las notas con SUAC
      DBMS_OUTPUT.PUT_LINE('5- SP_GUARDAR_TRAMITE - Sincronizar OBS');
      v_bloque := ' Sincronizar Notas SUAC';
      IPJ.TRAMITES_SUAC.SP_Subir_Notas_SUAC(
        o_rdo => o_rdo,
        o_tipo_mensaje => o_tipo_mensaje,
        o_cant_sincroniz => v_cant_sincroniz,
        o_sticker => v_sticker,
        p_id_tramite_ipj => nvl(p_id_tramite_ipj, v_Id_Tramite_Ipj),
        p_transaccion => 'N'
      );

      if upper(trim(o_rdo)) <> IPJ.TYPES.C_RESP_OK then
        o_rdo :=  v_bloque || '--' || o_rdo;
        o_Id_Tramite_Ipj := 0;
        return;
      end if;

      v_bloque := 'GUARDAR TRAMITE - Pases en SUAC: ';
      /* Si el tr�mite esta asociado a SUAC y :
         - Es un tr�mite d�gital
         - Cambio de Area o esta en estado PENDIENTE, o cambio el usuario
         - Si se pasa a Mesa SUAC o Archivo, se Archiva en SUAC
       */
      if nvl(v_Row_Tramite.id_tramite, 0) > 0 and (FC_Es_Tram_Digital(v_id_tramite_ipj) > 0) and
        (  (v_Row_Tramite.id_estado_ult = IPJ.TYPES.c_Estados_Archivo_Inicial) or
           (v_Row_Tramite.id_ubicacion <> v_id_ubicacion) or
           (nvl(v_Row_tramite.cuil_ult_estado, v_Row_tramite.id_grupo_ult_estado) <> nvl(v_cuil_ult_estado, v_id_grupo))
       ) then
          -- Si el tr�mite se pasa a un grupo, se busca el responsable del mismo
          if v_id_grupo > 0 then
            select cuil_responsable into v_resp_grupo_dest
            from ipj.t_grupos
            where
              id_grupo = v_id_grupo;
          end if;

          -- Si el tr�mite estaba asignado a un grupo, se usa el responsable
          if v_Row_Tramite.id_grupo_ult_estado > 0 then
            select cuil_responsable into v_resp_grupo_or
            from ipj.t_grupos
            where
              id_grupo = v_Row_Tramite.id_grupo_ult_estado;
          end if;

          -- Si el responsable del grupo es a quien se asigna y no se cambia de area, no hacemos nada
          if nvl(v_Row_Tramite.cuil_ult_estado, v_resp_grupo_or) <> nvl(v_cuil_ult_estado, v_resp_grupo_dest) or
            v_Row_Tramite.id_ubicacion <> v_id_ubicacion then
            IPJ.TRAMITES_SUAC.SP_Realizar_Tranf_IPJ(
              o_rdo => v_rdo_estado,
              o_tipo_mensaje => v_tipo_mensaje,
              p_id_tramite_suac => v_Row_Tramite.id_tramite,
              p_id_ubicacion_origen => v_Row_Tramite.id_ubicacion,
              p_id_ubicacion_destino => v_id_ubicacion,
              p_cuil_usuario_origen => nvl(v_Row_Tramite.cuil_ult_estado, v_resp_grupo_or),
              p_cuil_usuario_destino => nvl(v_cuil_ult_estado, v_resp_grupo_dest)
            );

            if upper(trim(v_rdo_estado)) <> IPJ.TYPES.C_RESP_OK then
              o_rdo :=  v_bloque || '- ' || v_rdo_estado;
              o_Id_Tramite_Ipj := 0;
              o_tipo_mensaje := v_tipo_mensaje;
              return;
            end if;

            -- Si se pasa a Archivo o Mesa SUAC, se pone como ARCHIVADO en SUAC.
            if v_id_ubicacion in (IPJ.TYPES.C_AREA_MESASUAC, IPJ.TYPES.C_AREA_ARCHIVO) and
             v_Row_Tramite.id_ubicacion not in (IPJ.TYPES.C_AREA_MESASUAC, IPJ.TYPES.C_AREA_ARCHIVO) then
              -- Busco el c�digo de la MESA SUAC
              select codigo_suac into v_cod_mesa_suac
              from ipj.t_ubicaciones
              where
                id_ubicacion = v_id_ubicacion;

              IPJ.TRAMITES_SUAC.SP_Cerrar_Tramite(
                p_id_tramite_suac  => v_Row_Tramite.id_tramite,
                p_cod_unidad => v_cod_mesa_suac,
                p_usuario => v_cuil_ult_estado,
                o_rdo => v_rdo_estado,
                o_tipo_mensaje => v_tipo_mensaje
              );

              if upper(trim(v_rdo_estado)) <> IPJ.TYPES.C_RESP_OK then
                o_rdo :=  v_bloque || '- ' || v_rdo_estado;
                o_Id_Tramite_Ipj := 0;
                o_tipo_mensaje := v_tipo_mensaje;
                return;
              end if;
            end if;
          end if;
      end if;

      --Si el tr�mite se pasa a otra �rea y tiene anexados, �stos se deben cerrar - PBI6281:[SG] - [Anexados]
      IF v_Row_Tramite.id_ubicacion <> v_id_ubicacion AND v_tiene_anexos <> 0 THEN
        ipj.tramites.sp_guardar_anexados(o_rdo, o_tipo_mensaje,v_Id_Tramite_Ipj);
      END IF;

    end if;

    --Derivacion a Mesa Suac, se realiza el pase a la Mesa SUAC correspondiente (si no es digital)
    v_bloque := 'Transferir a Mesa Suac: ';
    if (FC_Es_Tram_Digital(v_id_tramite_ipj) = 0) and
       v_Row_Tramite.id_ubicacion <> IPJ.TYPES.C_AREA_MESASUAC and --si ubicacion anterior no es mesa suac
         v_id_ubicacion = IPJ.TYPES.C_AREA_MESASUAC  and v_id_estado = 1 then --y la actual es mesa suac y pasa pendiente

      if nvl(v_id_tramite, 0) > 0 then
        -- Realizo un pase a Mesa SUAC
        IPJ.TRAMITES_SUAC.SP_Realizar_Tranf_Mesa_Suac (
          o_rdo => v_rdo_estado,
          o_tipo_mensaje => v_tipo_mensaje,
          p_id_tramite_suac => v_id_tramite,
          p_cuil_usuario => v_cuil_ult_estado
        );

        if v_rdo_estado <> IPJ.TYPES.C_RESP_OK then
          o_rdo :=  v_bloque || ' - ' || v_rdo_estado;
          o_Id_Tramite_Ipj := 0;
          o_tipo_mensaje := v_tipo_mensaje;
          return;
        end if;

        -- Vuelvo el tramite a la la ubicacion de origen, en vez de Mesa SUAC
        update ipj.t_tramitesipj
        set
          id_ubicacion = id_ubicacion_origen,
          id_clasif_ipj = null,
          cuil_ult_estado = '1'
        where
          id_tramite_ipj = v_id_tramite_ipj;

        -- Vuelvo las acciones al usuario DB
        update ipj.t_tramitesipj_acciones
        set
          cuil_usuario = '1'
        where
          id_tramite_ipj = v_id_tramite_ipj and
          id_estado = 1;

      end if;
    end if;

    --Derivacion a Archivo, se crea unica accion posible para esta area y se asigna a responsable
    v_bloque := 'Crear Accion Archivo: ';
    --Si la ubicacion actual es Archivo, o es un expediente de SxA o ACyF y se pasa a Mesa SUAC, y no es digital
    if (FC_Es_Tram_Digital(v_id_tramite_ipj) = 0) and
      v_Row_Tramite.id_ubicacion <> IPJ.TYPES.C_AREA_ARCHIVO and --si ubicacion anterior no es Archivo
         ( v_id_ubicacion = IPJ.TYPES.C_AREA_ARCHIVO or
          (v_id_ubicacion = IPJ.TYPES.C_AREA_MESASUAC and v_tipo_tramite_suac = 'EXPEDIENTE' and v_Row_Tramite.id_ubicacion_origen in (IPJ.TYPES.c_Area_SxA, IPJ.TYPES.c_Area_CyF))
         ) then

      --Busco en el tramite si existe la accion
      select count(1) into v_existe_Acc
      from IPJ.T_TRAMITESIPJ_ACCIONES
      where
        Id_Tramite_Ipj = v_Id_Tramite_Ipj and
        id_tipo_accion = IPJ.TYPES.c_Tipo_Acc_Archivar_Expediente;

      -- Si no existe una accion de archivo, la creo
      if v_existe_Acc = 0 then
        -- Busco el tipo de persona del tr�mite SUAC
        IPJ.TRAMITES_SUAC.SP_Buscar_Suac (
          o_rdo => o_rdo,
          o_tipo_mensaje => o_tipo_mensaje,
          o_SUAC => v_SUAC,
          p_id_tramite => v_id_tramite,
          p_loguear => 'N'
        );

        v_dni_suac := v_SUAC.v_iniciador_nro_documento;

        -- Si el tr�mite es iniciado por Persona, y no es un tr�mite de Jur�dico
        if v_dni_suac is not null and v_Row_Tramite.id_ubicacion_origen <> IPJ.TYPES.C_AREA_JURIDICO then
          -- Busco el integrante, iniciador en SUAC
          begin
            select ti.id_integrante into v_id_integrante
            from ipj.t_tramitesipj_integrante ti join ipj.t_integrantes i
              on ti.id_integrante = i.id_integrante
            where
              ti.id_tramite_ipj = p_id_tramite_ipj and -- BUG 8596
              i.nro_documento = v_dni_suac;
          exception
            when NO_DATA_FOUND then
              o_rdo :=  v_bloque || '-- El tr�mite SUAC tiene un Iniciador no utilizado en Gesti�n, verifique los datos en SUAC.' ;
              o_Id_Tramite_Ipj := 0;
              o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
              return;
          end;
        else
           --Busco el legajo de la primera accion que posea el Tramite
           begin
             select tacc.id_legajo into v_id_legajo
             from  IPJ.T_TRAMITESIPJ_ACCIONES tacc join IPJ.T_TIPOS_ACCIONESIPJ ta
               on tacc.ID_TIPO_ACCION = ta.ID_TIPO_ACCION
             where
               Id_Tramite_Ipj =  v_Id_Tramite_Ipj and
               tacc.id_legajo is not null and
               rownum = 1;
           exception
             when NO_DATA_FOUND then
               begin
                 select tpj.id_legajo into v_id_legajo
                 from  IPJ.T_TRAMITESIPJ_PERSJUR tpj
                 where
                   Id_Tramite_Ipj =  v_Id_Tramite_Ipj and
                   rownum = 1;
               exception
                 when NO_DATA_FOUND then
                   v_id_legajo := null;
               end;
           end;
        end if;

        -- busco el responsable del area
        select cuil_usuario into v_cuil_usuario
        from ipj.t_ubicaciones
        where
          id_ubicacion = IPJ.TYPES.C_AREA_ARCHIVO;

        DBMS_OUTPUT.PUT_LINE('4- SP_GUARDAR_TRAMITE - Crea Accion Archivo');
        IPJ.TRAMITES.SP_GUARDAR_TRAMITEIPJ_ACC(
          o_rdo => o_rdo,
          o_tipo_mensaje => o_tipo_mensaje,
          p_Id_Tramite_Ipj => v_Id_Tramite_Ipj,
          p_id_tramiteipj_accion => v_id_tramiteipj_accion,
          p_id_tipo_accion =>  IPJ.TYPES.c_Tipo_Acc_Archivar_Expediente,
          p_id_integrante => v_id_integrante,
          p_dni_integrante => v_dni_suac,
          p_id_legajo => v_id_legajo,
          p_cuit_persjur => null,
          p_id_estado => IPJ.TYPES.C_ESTADOS_COMPLETADO,
          p_cuil_usuario => v_cuil_usuario,
          p_obs_rubrica => null,
          p_observacion => null,
          p_id_fondo_comercio => null,
          p_fec_veeduria => null,
          p_id_documento => null,
          p_n_documento => null,
          p_id_pagina => 17 );-- Pagina de Archivo

          -- Creo la entrada vacia en archivo
          if o_rdo = IPJ.TYPES.C_RESP_OK then
            -- busco la accion creada
            select id_tramiteipj_accion into v_existe_Acc
            from IPJ.T_TRAMITESIPJ_ACCIONES
            where
              Id_Tramite_Ipj = v_Id_Tramite_Ipj and
              id_tipo_accion = IPJ.TYPES.c_Tipo_Acc_Archivar_Expediente;

            -- Veo si el tr�mite ya esta asignado en Archivo
            begin
              select id_archivo_tramite into v_id_archivo_tramite
              from ipj.t_archivo_tramite
              where
                id_tramite_ipj = p_id_tramite_ipj;
            exception
              when NO_DATA_FOUND then
                v_id_archivo_tramite := null;
            end;

            -- Si el tr�mite no esta, busco el expediente
            if v_id_archivo_tramite is null then
              begin
                select id_archivo_tramite into v_id_archivo_tramite
                from ipj.t_archivo_tramite
                where
                  trim(expediente) = trim(nvl(v_expediente, p_expediente));
              exception
                when NO_DATA_FOUND then
                  v_id_archivo_tramite := null;
              end;
            end if;

            -- Si no existe hago un insert, sino actualizo el tramite y la accion en archivo
            if v_id_archivo_tramite is null then
              insert into ipj.t_archivo_tramite (id_archivo_tramite, id_tramite_ipj,
                expediente, id_tramiteipj_accion, id_ubicacion, id_legajo, id_integrante, id_tramite)
              values ( IPJ.SEQ_ARCHIVO.nextval, v_Id_Tramite_Ipj,
                nvl(v_expediente, p_expediente), v_existe_Acc, v_Row_Tramite.id_ubicacion_origen,
                v_id_legajo, v_id_integrante, v_id_tramite);
            else
              update ipj.t_archivo_tramite
              set
                id_tramite_ipj = p_id_tramite_ipj,
                id_tramiteipj_accion = v_existe_Acc,
                id_integrante = v_id_integrante,
                id_legajo = v_id_legajo,
                id_tramite = v_id_tramite
              where
                id_archivo_tramite = v_id_archivo_tramite;
            end if;
          end if;
      end if;
    end if;


    -- Si es digital y paso a Observado, valido que todas esten sincronizadas
    if (FC_Es_Tram_Digital(v_id_tramite_ipj) > 0) and v_id_estado = IPJ.TYPES.C_ESTADOS_OBSERVADO then
      -- Controlo que todas las observaciones est�n sincronizadas con SUAC
      select count(*) into v_existen_observadas
      from IPJ.t_Entidades_notas
      where
        Id_Tramite_Ipj = v_Id_Tramite_Ipj and
        nvl(informar_suac, 'S') = 'S'; -- Falta sincroniza en SUAC

      if v_existen_observadas > 0 then
        o_rdo := 'Existen observaciones en el tr�mite no enviadas al usuario, verifique las mismas desde la ficha del tr�mite.';
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
        o_Id_Tramite_Ipj := 0;
        return;
      end if;
    end if;


    /*********************************************************
      Una vez creado o actualizado el tramite, sigo con las demas acciones sin importar
      el tipo de proceso
    *********************************************************/
    -- Actualizo la lista de estados de tramites
    DBMS_OUTPUT.PUT_LINE('5- SP_GUARDAR_TRAMITE - Carga historial tr�mite');
    v_bloque := 'Estado Tramite: ';
    IPJ.TRAMITES.SP_GUARDAR_TRAMITES_IPJ_ESTADO(
      o_rdo => v_rdo_estado,
      o_tipo_mensaje => v_tipo_mensaje,
      v_fecha_pase => sysdate,
      v_id_estado => v_id_estado_new,
      v_cuil_usuario => v_cuil_ult_estado,
      v_id_grupo => v_id_grupo,
      v_observacion => v_observacion,
      v_Id_Tramite_Ipj => p_Id_Tramite_Ipj);

    if upper(trim(v_rdo_estado)) <> IPJ.TYPES.C_RESP_OK then
      o_rdo :=  v_bloque || '--' || v_rdo_estado;
      o_Id_Tramite_Ipj := 0;
      o_tipo_mensaje := v_tipo_mensaje;
      return;
    end if;

    --Si el tramite se pasa a COMPLETADO o CERRADO, se registran los cambios de todas las  Personas Juridicas
    if v_id_estado_new in (IPJ.TYPES.c_Estados_Completado, IPJ.TYPES.c_Estados_Cerrado)  then
      v_bloque := 'Personas Juridicas: ';

      -- Marco los Comerciantes y Gravamenes como NO BORRADORES y actualizo las PERS JUR
      update IPJ.T_COMERCIANTES_INS_LEGAL
      set borrador = 'N'
      where Id_Tramite_Ipj = p_Id_Tramite_Ipj;

      update IPJ.T_COMERCIANTES_RUBROS
      set borrador = 'N'
      where Id_Tramite_Ipj = p_Id_Tramite_Ipj;

      update IPJ.T_COMERCIANTES_RUBRICA
      set borrador = 'N'
      where Id_Tramite_Ipj = p_Id_Tramite_Ipj;

      update IPJ.T_GRAVAMENES_INS_LEGAL
      set borrador = 'N'
      where Id_Tramite_Ipj = p_Id_Tramite_Ipj;

      update IPJ.T_MANDATOS_INS_LEGAL
      set borrador = 'N'
      where Id_Tramite_Ipj = p_Id_Tramite_Ipj;

      update IPJ.T_MANDATOS_MANDATARIOS
      set borrador = 'N'
      where Id_Tramite_Ipj = p_Id_Tramite_Ipj;

      update IPJ.T_fondos_comercio_rubro
      set borrador = 'N'
      where Id_Tramite_Ipj = p_Id_Tramite_Ipj;

      update IPJ.T_fondos_comercio_vendedor
      set borrador = 'N'
      where Id_Tramite_Ipj = p_Id_Tramite_Ipj;

      update IPJ.T_fondos_comercio_comprador
      set borrador = 'N'
      where Id_Tramite_Ipj = p_Id_Tramite_Ipj;

      update IPJ.T_fondos_comercio_ins_legal
      set borrador = 'N'
      where Id_Tramite_Ipj = p_Id_Tramite_Ipj;

      -- listo las acciones con algun dato en entidad que no sea borrador y que su tramite
      -- no este rechazado para actualizar en PerJur
      OPEN v_Cursor_PersJur FOR
        select tpj.*
        from IPJ.T_TRAMITESIPJ_PERSJUR tpj join IPJ.t_tramitesipj_acciones tacc
            on tacc.Id_Tramite_Ipj = tpj.Id_Tramite_Ipj and tacc.id_legajo = tpj.id_legajo
        where
          tpj.Id_Tramite_Ipj = p_Id_Tramite_Ipj and
          TACC.ID_ESTADO < IPJ.TYPES.C_ESTADOS_RECHAZADO;

      LOOP
        fetch v_Cursor_PersJur into v_Row_Tramite_PersJur;
        EXIT WHEN v_Cursor_PersJur%NOTFOUND or v_Cursor_PersJur%NOTFOUND is null;

        -- Obtengo CUIT, Razon Social, Registro y Matricula de la Entidad y el Legajo.
        begin
          -- Busco los datos de la entidad
          select * into v_Row_Entidad
          from ipj.t_entidades e
          where
            e.Id_Tramite_Ipj = v_Row_Tramite_PersJur.Id_Tramite_Ipj and
            e.id_legajo = v_Row_Tramite_PersJur.id_legajo;

          -- Busco los datos del legajo
          select * into v_Row_Legajo
          from ipj.t_legajos l
          where
            l.id_legajo = v_Row_Tramite_PersJur.id_legajo;

          -- Busco la ubicacion de la entidad
          select te.id_ubicacion into v_id_ubicacion_ent
          from IPJ.T_TIPOS_ENTIDADES te
          where
            te.id_tipo_entidad = v_Row_Entidad.id_tipo_entidad;

        exception
          when NO_DATA_FOUND then
           v_id_ubicacion_ent := 0;
        end;

        --Si varia Razon Social, CUIT, Tipo Entidad o Matricula, actualizo el legajo
        if nvl(v_Row_Entidad.Cuit, '@') <> '@' or
           nvl(v_Row_Entidad.Denominacion_1, '@') <> '@' or
           nvl(v_Row_Entidad.id_tipo_entidad, 0) <> 0 or
           nvl(v_Row_Entidad.Folio, '@') <> '@' or
           nvl(v_Row_Entidad.Anio, 0) <> 0 then

         DBMS_OUTPUT.PUT_LINE('6- SP_GUARDAR_TRAMITE - Actualiza Legajo');
          update IPJ.t_legajos
          set
            denominacion_sia = nvl(v_Row_Entidad.Denominacion_1, denominacion_sia),
            cuit = nvl(v_Row_Entidad.cuit, cuit),
            id_tipo_entidad = nvl(v_Row_Entidad.id_tipo_entidad, id_tipo_entidad),
            nro_ficha = (case v_id_ubicacion_ent
                                 when IPJ.TYPES.C_AREA_CYF then v_Row_Entidad.nro_registro
                                 when IPJ.TYPES.C_AREA_SxA then IPJ.VARIOS.FC_FORMATEAR_MATRICULA(v_Row_Entidad.matricula)
                                 when IPJ.TYPES.C_AREA_SRL then IPJ.VARIOS.FC_FORMATEAR_MATRICULA(v_Row_Entidad.matricula)
                                 else nro_ficha
                               end),
            registro = (case v_id_ubicacion_ent
                               when IPJ.TYPES.C_AREA_SxA then to_number(v_Row_Entidad.nro_registro)
                               else registro
                             end),
            Fecha_Baja_Entidad = v_Row_Entidad.baja_temporal,
            folio = (case v_id_ubicacion_ent
                          when IPJ.TYPES.C_AREA_CYF then null
                          else v_Row_Entidad.folio
                       end),
            anio = v_Row_Entidad.anio,
            id_vin = v_Row_Entidad.id_vin_real
          where
            id_legajo = v_Row_Tramite_PersJur.id_legajo;
        end if;

        -- Sincroniza los tramites abiertos de la misma entidad
        DBMS_OUTPUT.PUT_LINE('7- SP_GUARDAR_TRAMITE - Actualiza Tramites Relacionados');
        IPJ.TRAMITES.SP_Act_Tramites_Relac(
          o_rdo => o_rdo,
          o_tipo_mensaje => o_tipo_mensaje,
          p_Id_Tramite_Ipj => v_Id_Tramite_Ipj,
          p_id_legajo => v_Row_Tramite_PersJur.id_legajo);


        if o_rdo != TYPES.C_RESP_OK then
          o_rdo :=  v_metodo || ' ' ||v_origen  || ' ' || o_rdo;
          o_Id_Tramite_Ipj := 0;
          return;
        end if;

        if nvl(v_Row_Tramite.id_proceso, 0) not in (IPJ.TYPES.C_PROCESO_GEN_HIST, IPJ.TYPES.C_PROCESO_HIST) then
          -- Actualizo los datos en T_COMUNES solo si tiene CUIT
          if v_Row_Entidad.cuit is not null and v_Row_Entidad.borrador <> 'N' then -- (BUG 9391)
            DBMS_OUTPUT.PUT_LINE('8- SP_GUARDAR_TRAMITE - Actualiza Personas Juridica');
            IPJ.ENTIDAD_PERSJUR.sp_actualizar_pers_juridica(
              o_rdo =>  v_rdo_PersJur,
              o_metodo => v_metodo,
              o_origen => v_origen,
              o_tipo_mensaje => v_tipo_mensaje,
              p_Id_Tramite_Ipj => v_Id_Tramite_Ipj,
              p_id_legajo => v_Row_Tramite_PersJur.id_legajo );

            if v_rdo_PersJur != TYPES.C_RESP_OK then
              o_rdo :=  v_metodo || ' ' ||v_origen  || ' ' ||v_rdo_PersJur;
              o_Id_Tramite_Ipj := 0;
              o_tipo_mensaje := v_tipo_mensaje;
              return;
            end if;
          else

            IPJ.ENTIDAD_PERSJUR.SP_Actualizar_Borrador(
              o_rdo => o_rdo,
              o_tipo_mensaje => o_tipo_mensaje,
              p_Id_Tramite_Ipj => v_Id_Tramite_Ipj,
              p_id_legajo => v_Row_Tramite_PersJur.id_legajo,
              p_borrador => 'N'
            );
            if o_rdo != TYPES.C_RESP_OK then
              o_rdo :=  v_metodo || ' ' ||v_origen  || ' ' ||v_rdo_PersJur;
              o_Id_Tramite_Ipj := 0;
              o_tipo_mensaje := v_tipo_mensaje;
              return;
            end if;
          end if;
        else

          IPJ.ENTIDAD_PERSJUR.SP_Actualizar_Borrador(
            o_rdo => o_rdo,
            o_tipo_mensaje => o_tipo_mensaje,
            p_Id_Tramite_Ipj => v_Id_Tramite_Ipj,
            p_id_legajo => v_Row_Tramite_PersJur.id_legajo,
            p_borrador => 'N'
          );
          if o_rdo != TYPES.C_RESP_OK then
            o_rdo :=  v_metodo || ' ' ||v_origen  || ' ' ||v_rdo_PersJur;
            o_Id_Tramite_Ipj := 0;
            o_tipo_mensaje := v_tipo_mensaje;
            return;
          end if;
        end if;
      end loop;

      CLOSE v_Cursor_PersJur;

      --Si el tr�mite tiene anexados y no fue cerrado a�n, los cierro como el padre - PBI6281:[SG] - [Anexados]
      IF v_tiene_anexos <> 0 AND v_id_estado < IPJ.TYPES.C_ESTADOS_COMPLETADO THEN
        ipj.tramites.sp_guardar_anexados(o_rdo, o_tipo_mensaje,v_Id_Tramite_Ipj);
      END IF;

    end if;

    o_rdo := TYPES.C_RESP_OK;
    o_Id_Tramite_Ipj := p_Id_Tramite_Ipj;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
    WHEN OTHERS THEN
      o_rdo := 'SP_GUARDAR_TRAMITE: ' ||v_bloque || To_Char(SQLCODE) || '-' || SQLERRM;
      o_Id_Tramite_Ipj := 0;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_GUARDAR_TRAMITE;


  PROCEDURE SP_GUARDAR_TRAMITES_IPJ_ESTADO(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    v_fecha_pase in date,
    v_id_estado in number,
    v_cuil_usuario in varchar2,
    v_id_grupo in number,
    v_observacion in varchar2,
    v_Id_Tramite_Ipj in number)
  IS
    p_id_pases_tramites_ipj NUMBER;
    v_valid_parametros varchar2(3000);
    v_bloque varchar2(1000);
  /****************************************************
   Guarda el cambio de estado de un tramite.
   Solo se llama desde SP_GUARDAR_TRAMITE, por lo tanto no maneja la transaccion
  *****************************************************/
  BEGIN
     --VALIDACION DE PARAMETROS
    v_bloque := 'Par�metros: ';
    if IPJ.VARIOS.Valida_Estado(v_id_estado) = 0 then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('EST_NOT');
    end if;
    if nvl(v_Id_Tramite_Ipj, 0) <= 0 then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('ENT_NOT');
    end if;
    if v_cuil_usuario is not null and IPJ.VARIOS.Valida_Usuario(v_cuil_usuario) = 0 then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('USR_NOT');
    end if;

    -- si los parametros estan OK, continuo
    if v_valid_parametros is not null then
      o_rdo := v_bloque || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

     --CUERPO DEL PROCEDIMEINTO
    -- Hist�rico de tramitest�rico de tr�mites. Se carga desde SP_GUARDAR_TRAMITE
    SELECT ipj.SEQ_TRAMITES_IPJ_ESTADO.nextval INTO p_id_pases_tramites_ipj FROM dual;

    insert into ipj.t_tramites_ipj_estado
      (id_pases_tramites_ipj, fecha_pase, id_estado, cuil_usuario, id_grupo, observacion, Id_Tramite_Ipj)
    values
      (p_id_pases_tramites_ipj, v_fecha_pase, v_id_estado, v_cuil_usuario, v_id_grupo, v_observacion, v_Id_Tramite_Ipj);

    if o_rdo is null then
      o_rdo := 'OK';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    end if;
  EXCEPTION
    WHEN OTHERS THEN
      o_rdo := 'SP_GUARDAR_TRAMITES_IPJ_ESTADO: ' ||To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;

  END SP_GUARDAR_TRAMITES_IPJ_ESTADO;


  PROCEDURE SP_GUARDAR_TRAMITES_ACC_ESTADO(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_fecha_pase in date,
    p_id_estado in number,
    p_cuil_usuario in varchar2,
    p_observacion in varchar2,
    p_Id_Tramite_Ipj in number,
    p_id_tramite_Accion in number,
    p_id_tipo_accion in number,
    p_id_documento in number,
    p_n_documento in varchar2 )
  IS
    v_id_pases_acciones_ipj  NUMBER;
    v_valid_parametros varchar2(300);
    v_bloque varchar2(100);
  /****************************************************
   Guarda el cambio de estado de una Accion.
   Solo se llama desde SP_GUARDAR_TRAMITEIPJ_Acc, por lo tanto no maneja la transaccion
  *****************************************************/
  BEGIN
     --VALIDACION DE PARAMETROS
    v_bloque := 'Par�metros: ';
    if IPJ.VARIOS.Valida_Estado(p_id_estado) = 0 then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('EST_NOT');
    end if;
    if nvl(p_Id_Tramite_Ipj, 0) <= 0 then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('ENT_NOT');
    end if;
    if p_cuil_usuario is not null and IPJ.VARIOS.Valida_Usuario(p_cuil_usuario) = 0 then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('USR_NOT');
    end if;

    -- si los parametros estan OK, continuo
    if v_valid_parametros is not null then
      o_rdo := v_bloque || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

     --CUERPO DEL PROCEDIMEINTO
    -- Hist�rico de acciones de tr�mites. Se carga desde SP_GUARDAR_TRAMITEIPJ_Acc
    SELECT IPJ.SEQ_TRAMITESIPJ_ACC_EST.nextval INTO v_id_pases_acciones_ipj FROM dual;

    insert into IPJ.T_TRAMITESIPJ_ACCIONES_ESTADO
      (id_pases_acciones_ipj, id_tipo_accion, Id_Tramite_Ipj, id_tramiteipj_accion,
      fecha_pase, id_estado, observacion, cuil_usuario, id_documento, n_documento)
    values
      (v_id_pases_acciones_ipj, p_id_tipo_accion, p_Id_Tramite_Ipj, p_id_tramite_Accion, p_fecha_pase,
       p_id_estado, p_observacion, p_cuil_usuario, p_id_documento, p_n_documento);

    if o_rdo is null then
      o_rdo := 'OK';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    end if;
  EXCEPTION
    WHEN OTHERS THEN
      o_rdo := 'SP_GUARDAR_TRAMITES_ACC_ESTADO: ' || To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_GUARDAR_TRAMITES_ACC_ESTADO;


  PROCEDURE SP_Traer_Pendientes(
    o_Cursor OUT types.cursorType,
    p_fecha_desde in varchar2,
    p_fecha_hasta in varchar2,
    p_sticker in varchar2,
    p_expediente in varchar2,
    p_asunto in varchar2
    )
  IS
  /****************************************************
   Este procedimiento trae las transacciones de SUAC que estan para comenzar a utilizarse,
   uniendo los registros con las transacciones en estado PENDIENTE
  *****************************************************/
    vSQL varchar2(4000);
    v_sticker varchar2(20);
  BEGIN

    v_sticker := IPJ.TRAMITES.FC_Quitar_Ocultos(p_sticker);

    if v_sticker is not null or p_expediente is not null or p_asunto is not null or
      (p_fecha_desde is not null and p_fecha_hasta is not null) then
      vSQL :=
       'select VT_SUAC.ID_TRAMITE Id_Tramite_Suac , nvl(tIPJ.Sticker, VT_SUAC.nro_sticker) Sticker, ' ||
       '  VT_SUAC."Tipo/Subtipo" Tipo , VT_SUAC.Asunto, nvl(tIPJ.Expediente, VT_SUAC.nro_tramite) nro_expediente, ' ||
       '  to_Char(VT_SUAC.fecha_inicio, ''dd/mm/rrrr'') Fecha_Inicio_Suac, ' ||
       '  to_char(VT_SUAC.fecha_ultima_recepcion, ''dd/mm/rrrr'') Fecha_recepcion, '||
       '  nvl(tIPJ.Id_Tramite_Ipj, 0) Id_Tramite_Ipj, tIPJ.id_clasif_ipj, tIPJ.Observacion, tIPJ.id_estado_ult, ' ||
       '  tIPJ.cuil_ult_estado, NVL(e.n_estado, ''En SUAC'') n_estado, TIPJ.CUIL_CREADOR, ' ||
       '  to_char(TIPJ.FECHA_INICIO, ''dd/mm/rrrr'') Fecha_Inicio, TIPJ.URGENTE,TIPJ.simple_tramite, ' ||
       '  (case ' ||
       '       when vt_suac.tipo_iniciador = ''PERSONA FISICA'' then vt_suac.nombre || '' '' || vt_suac.apellido ' ||
       '       when vt_suac.tipo_iniciador <> ''PERSONA FISICA'' and nvl(vt_suac.CUIT_Iniciador, ''0'') = ''0'' then  vt_suac.n_iniciador ' ||
       '       else vt_suac.razon_social_persona_juridica ' ||
       '   end) razon_social_persona_juridica, ' ||
       '  tIPJ.codigo_online, ' ||
       ' (select count(1) from ipj.t_ol_entidades eo where eo.codigo_online = tipj.codigo_online and eo.id_tipo_tramite_ol = 1 and eo.id_sub_tramite_ol = 1) Const_SA ' ||
       'from ' ||
       '  NUEVOSUAC.VT_TRAMITES_IPJ_DESA vt_suac left join ipj.t_tramitesIPJ tIPJ ' ||
       '    on VT_SUAC.ID_TRAMITE = tIPJ.id_tramite ' ||
       '  left join ipj.t_estados e ' ||
       '    on tIPJ.id_estado_ult = e.id_estado ' ||
       'where ';

       -- Agrego solo 1 condicion de busqueda
       if p_expediente is not null then
         vSQL := vSQL || 'VT_SUAC.NRO_TRAMITE = trim(''' || p_expediente || ''') ' ;
       else
         if v_sticker is not null then
           vSQL := vSQL || 'VT_SUAC.NRO_STICKER = trim(''' || v_sticker || ''') ' ;
         else
           if p_asunto is not null then
             vSQL := vSQL || 'upper(vt_suac.asunto) like ''%'' || upper(''' || p_asunto ||''') || ''%'' ' ;
           else
             vSQL := vSQL || 'vt_suac.fecha_inicio between to_date(''' || p_fecha_desde || ''', ''dd/mm/rrrr'') and to_date(''' || p_fecha_hasta || ''', ''dd/mm/rrrr'') ' ;
           end if;
         end if;
       end if;

       vSQL := vSQL ||
         ' and nvl(tIPJ.id_estado_ult, 1) = 1 ' ||
         ' order by VT_SUAC.nro_sticker';

       DBMS_OUTPUT.PUT_LINE('Pendientes SUAC vSQL = ' || vSQL);
       OPEN o_Cursor FOR  vSQL;
     end if;

  END SP_Traer_Pendientes;


  PROCEDURE SP_Traer_TramitesIPJ_Accion(
    p_id_tramiteipj_accion in number,
    p_Cursor OUT types.cursorType)
  IS
    v_id_tramite_suac number;
   /****************************************************
   Este procedimiento devuelve el detalle de la accion pasada por parametro.
  *****************************************************/
  BEGIN

    OPEN p_Cursor FOR
      select
        vtIPJ.botones, vtIPJ.Clase, vtIPJ.id_tramite Id_Tramite_Suac, vtIPJ.Sticker,
        null Fecha_recepcion, vtIPJ.Expediente Nro_Expediente, vtIPJ.Id_Tramite_Ipj,
        vtIPJ.id_clasif_ipj, vtIPJ.id_tipo_accion, vtIPJ.Observacion, vtIPJ.id_estado,
        vtIPJ.cuil_usuario, n_estado, vtIPJ.Tipo_IPJ, vtIPJ.Fecha_Asignacion,
        vtIPJ.Acc_Abiertas, vtIPJ.Acc_Cerradas, vtIPJ.URGENTE,
        vtIPJ.usuario, nvl(vtIPJ.Error_Dato, 'N') Error_Dato,  vtIPJ.CUIT,
        vtIPJ.razon_social, vtIPJ.id_legajo, vtIPJ.N_UBICACION, vtIPJ.CUIL_CREADOR,
        vtIPJ.id_ubicacion, vtIPJ.id_tramiteipj_accion, vtIPJ.id_protocolo,
        vtIPJ.ID_INTEGRANTE, vtIPJ.NRO_DOCUMENTO, vtIPJ.Cuit_PersJur, vtIPJ.Obs_Rubrica,
        vtIPJ.identificacion, vtIPJ.persona,  vtIPJ.ID_FONDO_COMERCIO, vtIPJ.id_proceso,
        vtIPJ.id_pagina,  vtIPJ.cuil_usuario cuil_usuario_old, vtIPJ.ID_DOCUMENTO,
        vtIPJ.N_DOCUMENTO,  vtIPJ.simple_tramite,
        vtIPJ.fecha_ini_suac Fecha_Inicio_Suac,
        (case vtIPJ.id_proceso
           when 4 then 'GENERACION DE HISTORICOS - ' || VTIPJ.N_UBICACION
           when 5 then ''
           else IPJ.TRAMITES_SUAC.FC_BUSCAR_ASUNTO_SUAC(vtIPJ.id_tramite)
        end) Asunto,
        vtIPJ.Agrupable
      from IPJ.VT_TRAMITE_TECN_IPJ vtIPJ
      where
        id_tramiteipj_accion = p_id_tramiteipj_accion;

  end SP_Traer_TramitesIPJ_Accion;

  PROCEDURE SP_Traer_Tramite_Suac(
    p_sticker in varchar2,
    p_expediente in varchar2,
    p_Cursor OUT types.cursorType)
  IS
  /****************************************************
   Este procedimiento trae las transacciones de SUAC  por sticker o expediente
   *****************************************************/
    vSQL varchar2(4000);
  BEGIN

    if p_sticker is not null or p_expediente is not null  then
      vSQL :=
        'select VT_SUAC.ID_TRAMITE Id_Tramite_Suac , nvl(tIPJ.Sticker, VT_SUAC.nro_sticker) Sticker, ' ||
        '  VT_SUAC."Tipo/Subtipo" Tipo , VT_SUAC.Asunto, nvl(tIPJ.Expediente, VT_SUAC.nro_tramite) nro_expediente, ' ||
        '  to_Char(VT_SUAC.fecha_inicio, ''dd/mm/rrrr'') Fecha_Inicio_Suac, ' ||
        '  to_char(VT_SUAC.fecha_ultima_recepcion, ''dd/mm/rrrr'') Fecha_recepcion, '||
        '  nvl(tIPJ.Id_Tramite_Ipj, 0) Id_Tramite_Ipj, tIPJ.id_clasif_ipj, tIPJ.Observacion, tIPJ.id_estado_ult, ' ||
        '  tIPJ.cuil_ult_estado, NVL(e.n_estado, ''En SUAC'') n_estado, TIPJ.CUIL_CREADOR, ' ||
        '  to_char(TIPJ.FECHA_INICIO, ''dd/mm/rrrr'') Fecha_Inicio, TIPJ.URGENTE,TIPJ.simple_tramite, ' ||
        '  (case ' ||
        '       when vt_suac.tipo_iniciador = ''PERSONA FISICA'' then vt_suac.nombre || '' '' || vt_suac.apellido ' ||
        '       when vt_suac.tipo_iniciador <> ''PERSONA FISICA'' and nvl(vt_suac.CUIT_Iniciador, ''0'') = ''0'' then  vt_suac.n_iniciador ' ||
        '       else vt_suac.razon_social_persona_juridica ' ||
        '   end) razon_social_persona_juridica ' ||
        'from ' ||
        '  NUEVOSUAC.VT_TRAMITES_IPJ_DESA vt_suac left join ipj.t_tramitesIPJ tIPJ ' ||
        '    on VT_SUAC.ID_TRAMITE = tIPJ.id_tramite ' ||
        '  left join ipj.t_estados e ' ||
        '    on tIPJ.id_estado_ult = e.id_estado ' ||
        'where ';

       -- Agrego solo 1 condicion de busqueda
       if p_expediente is not null then
         vSQL := vSQL || 'ltrim(upper(NRO_TRAMITE)) = trim(''' ||upper(p_expediente) || ''') ' ;
       else
         if p_sticker is not null then
           vSQL := vSQL || 'NRO_STICKER = trim(''' ||p_sticker || ''') ' ;
         end if;
       end if;

       vSQL := vSQL ||
         ' order by nro_sticker';

       OPEN p_Cursor FOR  vSQL;
     end if;

  END SP_Traer_Tramite_Suac;


  PROCEDURE SP_Traer_TramitesIPJ_Elab(
    p_Cuil in varchar2,
    p_tipo_tramite in number,
    p_fecha_desde varchar2,
    p_fecha_hasta varchar2,
    p_id_estado number,
    o_Cursor OUT types.cursorType)
  IS
    v_valid_parametros varchar (200);
  /****************************************************
   Devuelve el listado de tramites elaborados por un usuario, que ya estan aprobados.
  *****************************************************/
  BEGIN
    if p_fecha_desde is not null and IPJ.VARIOS.valida_fecha(p_fecha_desde) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT');
    end if;

    if p_fecha_hasta is not null and IPJ.VARIOS.valida_fecha(p_fecha_hasta) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT');
    end if;

    if v_valid_parametros is not null then
      return;
    end if;

    OPEN o_Cursor FOR
    select distinct VT_SUAC.ID_TRAMITE Id_Tramite_Suac, nvl(tIPJ.Sticker, VT_SUAC.nro_sticker) Sticker, nvl(tIPJ.Expediente, VT_SUAC.nro_tramite) Nro_Expediente,
      VT_SUAC."Tipo/Subtipo" Tipo, VT_SUAC.Asunto, /*VT_SUAC."Iniciador" */ '' Iniciador,
      to_char(VT_SUAC.fecha_inicio, 'dd/mm/rrrr') Fecha_Inicio,
      to_char(VT_SUAC.fecha_ultima_recepcion, 'dd/mm/rrrr') Fecha_recepcion,
      tIPJ.Id_Tramite_Ipj, tIPJ.id_clasif_ipj, tIPJ.Observacion,  tIPJ.id_estado_ult,
      tIPJ.cuil_ult_estado, e.n_estado, u.descripcion usuario, '' N_TIPO_TRAMITE,
      (select descripcion from IPJ.T_usuarios where cuil_usuario = te.cuil_usuario) Operador,
      IPJ.TRAMITES.FC_Ultima_Fecha_Tramite ( tIPJ.Id_Tramite_Ipj) Fecha_Asignacion,
      0 ID_PROTOCOLO
   from
     NUEVOSUAC.VT_TRAMITES_IPJ_DESA vt_suac join IPJ.t_tramitesIPJ tIPJ
       on VT_SUAC.ID_TRAMITE = tIPJ.id_tramite
     join IPJ.t_estados e
       on tIPJ.id_estado_ult = e.id_estado
     left join IPJ.t_usuarios u
       on tIPJ.Cuil_Ult_Estado = u.cuil_usuario
     join IPJ.t_tramites_ipj_estado te
       on tIPJ.Id_Tramite_Ipj = te.Id_Tramite_Ipj
     join ( select  Id_Tramite_Ipj, max(id_pases_tramites_ipj) id_pases_tramites_ipj
             from IPJ.t_tramites_ipj_estado tev
             where  TEv.ID_ESTADO = 4
             group by TEv.Id_Tramite_Ipj
             order by TEv.Id_Tramite_Ipj ) te2
       on TE2.Id_Tramite_Ipj = Te.Id_Tramite_Ipj and TE2.ID_PASES_TRAMITES_IPJ = TE.ID_PASES_TRAMITES_IPJ
    where
      --vt_suac."Nombre Unidad Actual" = 'REGISTRO PUBLICO DE COMERCIO-RPCDIPJ01' and
      TIPJ.ID_ESTADO_ULT = p_id_estado and
      ( ( p_fecha_desde is null
          and te.fecha_pase between sysdate -30 and sysdate) OR
          te.fecha_pase between to_date(p_fecha_desde, 'dd/mm/rrrr') and to_date(p_fecha_hasta, 'dd/mm/rrrr')) and
      ( p_Cuil is null or p_Cuil = te.cuil_usuario ) and
      ( nvl(p_tipo_tramite, 0) = 0 or p_tipo_tramite = tIPJ.ID_clasif_ipj)
      order by VT_SUAC.nro_sticker;
  END SP_Traer_TramitesIPJ_Elab;


  PROCEDURE SP_Traer_TramitesIPJ_Curs(
    p_Cuil in varchar2,
    p_tipo_tramite in number,
    p_fecha_desde varchar2,
    p_fecha_hasta varchar2,
    o_Cursor OUT types.cursorType)
  IS
    v_valid_parametros varchar (200);
  /****************************************************
   Devuelve el listado de tramites elaborados por un usuario, que ya estan aprobados.
  *****************************************************/
  BEGIN
    if p_fecha_desde is not null and IPJ.VARIOS.valida_fecha(p_fecha_desde) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT');
    end if;

    if p_fecha_hasta is not null and IPJ.VARIOS.valida_fecha(p_fecha_hasta) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT');
    end if;

    if v_valid_parametros is not null then
      return;
    end if;

    OPEN o_Cursor FOR
    select distinct VT_SUAC.ID_TRAMITE Id_Tramite_Suac, nvl(tIPJ.Sticker, VT_SUAC.nro_sticker) Sticker, nvl(tIPJ.Expediente, VT_SUAC.nro_tramite) Nro_Expediente,
      VT_SUAC."Tipo/Subtipo" Tipo, VT_SUAC.Asunto, /*VT_SUAC."Iniciador" */''  Iniciador,
      to_char(VT_SUAC.fecha_inicio, 'dd/mm/rrrr') Fecha_Inicio,
      to_char(VT_SUAC.fecha_ultima_recepcion, 'dd/mm/rrrr') Fecha_recepcion,
      tIPJ.Id_Tramite_Ipj, tIPJ.id_clasif_ipj, tIPJ.Observacion, tIPJ.id_estado_ult,
      tIPJ.cuil_ult_estado, e.n_estado, u.descripcion usuario, '' N_TIPO_TRAMITE,
      IPJ.TRAMITES.FC_Ultima_Fecha_Tramite ( tIPJ.Id_Tramite_Ipj) Fecha_Asignacion,
      0 ID_PROTOCOLO
   from
     NUEVOSUAC.VT_TRAMITES_IPJ_DESA vt_suac join IPJ.t_tramitesIPJ tIPJ
       on VT_SUAC.ID_TRAMITE = tIPJ.id_tramite
     join IPJ.t_estados e
       on tIPJ.id_estado_ult = e.id_estado
     left join IPJ.t_usuarios u
       on tIPJ.Cuil_Ult_Estado = u.cuil_usuario
    where
      --vt_suac."Nombre Unidad Actual" = 'REGISTRO PUBLICO DE COMERCIO-RPCDIPJ01' and
      TIPJ.ID_ESTADO_ULT between 2 and 99 and
     /* ( ( p_fecha_desde is null
          and te.fecha_pase between sysdate -30 and sysdate) OR
          te.fecha_pase between to_date(p_fecha_desde, 'dd/mm/rrrr') and to_date(p_fecha_hasta, 'dd/mm/rrrr')) and*/
      ( p_Cuil is null or p_Cuil = tIPJ.Cuil_Ult_Estado ) and
      ( nvl(p_tipo_tramite, 0) = 0 or p_tipo_tramite = tIPJ.ID_clasif_ipj)
      order by VT_SUAC.nro_sticker;
  END SP_Traer_TramitesIPJ_Curs;


  PROCEDURE SP_Traer_TramitesIPJ_PersJur (
    p_Id_Tramite_Ipj in number,
    o_Cursor out types.cursorType)
  IS
  BEGIN
    /*************************************************
     Lista las Personas Juridicas cargadas en un Tramite IPJ
   *************************************************/
    OPEN o_Cursor FOR
      select
        trpj.Id_Tramite_Ipj, trpj.id_legajo, L.cuit cuit_persjur, trpj.id_sede, trpj.error_dato,
        L.DENOMINACION_SIA Razon_Social,
        (select count(*) from IPJ.T_ENTIDADES_NOTAS en where en.Id_Tramite_Ipj = trpj.Id_Tramite_Ipj and en.id_legajo = trpj.id_legajo) Cant_Obs,
        l.id_tipo_entidad
      from IPJ.t_TramitesIPJ_PersJur trpj join IPJ.t_legajos L
        on trpj.id_legajo = L.id_legajo
      where
        trpj.Id_Tramite_Ipj = p_Id_Tramite_Ipj;

  END SP_Traer_TramitesIPJ_PersJur;


  PROCEDURE SP_Traer_TramitesIPJ_Integ (
    p_Id_Tramite_Ipj in number,
    o_Cursor out types.cursorType)
  IS
  BEGIN
  /*************************************************
     Lista las Personas Fisicas cargadas en un Tramite IPJ
 *************************************************/
      OPEN o_Cursor FOR
      select
        ti.Id_Tramite_Ipj, ti.id_integrante, ti.error_dato,
        I.DETALLE, I.ID_NUMERO, I.ID_SEXO, I.PAI_COD_PAIS, I.NRO_DOCUMENTO
      from IPJ.t_TramitesIPJ_Integrante ti join IPJ.T_Integrantes i
        on TI.ID_INTEGRANTE = I.ID_INTEGRANTE
      where
        ti.Id_Tramite_Ipj = p_Id_Tramite_Ipj;

  END SP_Traer_TramitesIPJ_Integ;


  PROCEDURE SP_Traer_TramitesIPJ_Acc (
    p_Id_Tramite_Ipj in number,
    o_Cursor out types.cursorType)
  IS
  BEGIN
  /*************************************************
     Lista las Acciones cargadas en un Tramite IPJ
 *************************************************/
    OPEN o_Cursor FOR
      SELECT ipj.varios.FC_Habilitar_Botones (
             tIPJ.id_ubicacion,
             tIPJ.id_clasif_ipj,
             ta.id_tipo_Accion,
             0,
             1,
             ta.id_estado,
             2,
             tIPJ.id_proceso,
             tIPJ.simple_tramite,
             ta.Cuil_Usuario,
             ta.Cuil_Usuario,
             tIPJ.id_tramite_ipj,
             ta.id_tramiteipj_accion
        ) botones,
        ta.Id_Tramite_Ipj, ta.Id_TramiteIPJ_Accion, ta.Id_Tipo_Accion, ta.Id_Integrante, ta.id_legajo,
        ta.id_estado, ta.Cuil_Usuario, ta.Obs_Rubrica, ta.Observacion,
        E.N_ESTADO, U.DESCRIPCION, TACC.N_TIPO_ACCION,
        NVL(fc.n_fondo_comercio, NVL((select p.nombre||' '||p.apellido from rcivil.vt_pk_persona p where p.id_sexo = i.id_sexo and p.nro_documento = i.nro_documento and p.pai_cod_pais = i.pai_cod_pais and p.id_numero = i.id_numero), L.Denominacion_Sia)) Persona,
        (CASE WHEN TA.id_legajo IS NULL THEN I.NRO_DOCUMENTO ELSE L.CUIT END) identificacion,
        NVL(NVL(TPJ.ERROR_DATO, TI.ERROR_DATO), 'N') Error_Dato,
        L.CUIT Cuit_PersJur, I.NRO_DOCUMENTO,
        ta.id_fondo_comercio, ta.Id_Documento, ta.N_Documento,
        tacc.id_pagina, TIPJ.ID_PROCESO, tacc.id_protocolo, tc.id_ubicacion id_ubicacion_accion
      FROM IPJ.t_TramitesIPJ_Acciones ta JOIN IPJ.T_ESTADOS e
          ON TA.ID_ESTADO = E.ID_ESTADO
        JOIN IPJ.t_tramitesipj tIPJ
          ON tIPJ.Id_Tramite_Ipj = ta.Id_Tramite_Ipj
        JOIN IPJ.t_usuarios u
          ON TA.CUIL_USUARIO = U.CUIL_USUARIO
        JOIN IPJ.T_TIPOS_ACCIONESIPJ tacc
          ON TA.ID_TIPO_ACCION = TACC.ID_TIPO_ACCION
        LEFT JOIN IPJ.T_TRAMITESIPJ_PERSJUR tpj
          ON TA.Id_Tramite_Ipj = TPJ.Id_Tramite_Ipj AND TA.id_legajo = TPJ.id_legajo
        LEFT JOIN IPJ.T_Legajos L
          ON TA.id_legajo = L.ID_Legajo
        LEFT JOIN IPJ.T_TRAMITESIPJ_Integrante ti
          ON TA.Id_Tramite_Ipj = TI.Id_Tramite_Ipj AND TA.ID_INTEGRANTE = TI.ID_INTEGRANTE
        LEFT JOIN IPJ.T_INTEGRANTES i
          ON ta.id_integrante = i.id_integrante
        LEFT JOIN IPJ.T_FONDOS_COMERCIO fc
          ON ta.id_fondo_comercio = fc.id_fondo_comercio
        JOIN ipj.t_tipos_clasif_ipj tc
          ON tacc.id_clasif_ipj = tc.id_clasif_ipj
       WHERE ta.Id_Tramite_Ipj = p_Id_Tramite_Ipj;

  END SP_Traer_TramitesIPJ_Acc;

  PROCEDURE SP_Traer_TramitesIPJ_Acc_ID (
    p_Id_Tramite_Ipj in number,
    p_id_tramiteipj_accion in number,
    o_Cursor out types.cursorType)
  IS
  BEGIN
  /*************************************************
     Trae los datos de una Accion cargada en un Tramite IPJ
 *************************************************/
      OPEN o_Cursor FOR
      select
        nvl(vt_suac.id_tramite, 0) id_tramite_suac, nvl(tipj.sticker, vt_suac.nro_sticker) sticker,
        vt_suac."Tipo/Subtipo" tipo, vt_suac.asunto,
        to_char(vt_suac.fecha_inicio, 'dd/mm/rrrr') fecha_inicio_suac,
        to_char(vt_suac.fecha_ultima_recepcion, 'dd/mm/rrrr') fecha_recepcion,
        ta.id_tramite_ipj, ta.id_tramiteipj_accion, ta.id_tipo_accion, ta.id_integrante, ta.id_legajo,
        ta.id_estado, ta.cuil_usuario, ta.obs_rubrica, ta.observacion,
        e.n_estado, u.descripcion, tacc.n_tipo_accion, tacc.n_tipo_accion tipo_ipj,
        nvl(fc.n_fondo_comercio, nvl(l.denominacion_sia, (select p.nombre||' '||p.apellido from rcivil.vt_pk_persona p where p.id_sexo = i.id_sexo and p.nro_documento = i.nro_documento and p.pai_cod_pais = i.pai_cod_pais and p.id_numero = i.id_numero))) persona,
        (case when ta.id_legajo is null then i.nro_documento else l.cuit end) identificacion,
        nvl(nvl(tpj.error_dato, ti.error_dato), 'N') error_dato, l.cuit,
        nvl(tacc.id_protocolo, 0) id_protocolo, ta.id_fondo_comercio,
        to_char(ta.fec_veeduria, 'dd/mm/rrrr HH:MI:SS AM') fec_veeduria,
        ta.id_documento, ta.n_documento, ta.nro_actuacion, ta.nro_dictamen,
        ta.id_prot_jur, ta.nro_resolucion, to_char(ta.fec_resolucion, 'dd/mm/rrrr') fec_resolucion,
        tIPJ.id_ubicacion_origen, tIPJ.expediente Nro_Expediente, -- (PBI 7711)
        (select n_ubicacion from ipj.t_ubicaciones u where u.id_ubicacion = tIPJ.id_ubicacion_origen) N_Ubicacion_Origen,
        nvl((select denominacion_1 from ipj.t_entidades e where e.id_tramite_ipj = tIPJ.id_tramite_ipj and e.id_legajo = ta.id_legajo), l.denominacion_sia) denominacion_1, --(PBI 7435)
        to_char(ta.fecha_dictamen, 'dd/mm/rrrr') fecha_dictamen,
        ta.id_prot_dig, ta.nro_resolucion_dig, to_char(ta.fec_resolucion_dig, 'dd/mm/rrrr') fec_resolucion_dig,
        (select n_prot_dig from IPJ.T_PROTOCOLOS_DIGITAL p where p.id_prot_dig = ta.id_prot_dig) n_prot_dig,
        (select id_tipo_tramite_ol from ipj.t_ol_entidades o where o.codigo_online = tIPJ.codigo_online) id_tipo_tramite_ol,
        IPJ.TRAMITES.FC_Es_Tram_Digital(tIPJ.id_tramite_ipj) Es_Digital,
        ta.anio_desde, ta.anio_hasta, ta.desde_const
      from IPJ.t_TramitesIPJ_Acciones ta join IPJ.t_tramitesipj tIPJ
          on ta.Id_Tramite_Ipj = tIPJ.Id_Tramite_Ipj
        left join NUEVOSUAC.VT_TRAMITES_IPJ_DESA vt_suac
          on VT_SUAC.ID_TRAMITE = tIPJ.id_tramite
        join IPJ.T_ESTADOS e
          on TA.ID_ESTADO = E.ID_ESTADO
        join IPJ.t_usuarios u
          on TA.CUIL_USUARIO = U.CUIL_USUARIO
        left join IPJ.T_TIPOS_ACCIONESIPJ tacc
          on TA.ID_TIPO_ACCION = TACC.ID_TIPO_ACCION
        left join IPJ.T_TRAMITESIPJ_PERSJUR tpj
          on TA.Id_Tramite_Ipj = TPJ.Id_Tramite_Ipj and TA.id_legajo = TPJ.id_legajo
        left join IPJ.T_Legajos L
          on TA.id_legajo = L.id_legajo
        left join IPJ.T_TRAMITESIPJ_Integrante ti
          on TA.Id_Tramite_Ipj = TI.Id_Tramite_Ipj and TA.ID_INTEGRANTE = TI.ID_INTEGRANTE
        left join IPJ.T_INTEGRANTES i
          on ta.id_integrante = i.id_integrante
        left join IPJ.T_FONDOS_COMERCIO fc
          on ta.id_fondo_comercio = fc.id_fondo_comercio
      where
        ta.Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        ta.id_tramiteipj_accion = p_id_tramiteipj_accion;

  END SP_Traer_TramitesIPJ_Acc_ID;

 PROCEDURE SP_Guardar_TramiteIPJ_PersJur (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number,
    p_id_sede in varchar2,
    p_error_dato in varchar2,
    p_eliminar in number )
  IS
   v_existe number;
   v_mensaje varchar2(200);
   v_valid_parametros varchar2(600);
   v_PermiteEliminar number;
   v_Row_Tramite  IPJ.T_TramitesIPJ%ROWTYPE;
   v_obs_Tramite_Hist varchar2(1000);
   v_cant number;
   v_tramite_suac varchar2(50);
   v_id_tramite_legajo number;
  BEGIN
  /*************************************************
     Guarda un nuevo registro en T_TramitesIPJ_PersJur,
     o elimina un registro si p_eliminar = S
     La transaccion la maneja la aplicacion para guardar cabecera y detalles.
  ************************************************/
    if p_id_legajo = 0 then --Valido que venga un legajo
      v_valid_parametros := v_valid_parametros || 'Empresa Inv�lida' || '. ';
    end if;

     -- Si los paramentros no estan OK, no continuo
    if v_valid_parametros is not null then
      o_rdo := 'Parametros : ' || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    --  CUERPO DEL PROCEDIMIETNO
    if p_eliminar = 1 then
      select count(*) into v_PermiteEliminar
      from IPJ.T_TRAMITESIPJ_ACCIONES ta
      where
        TA.Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        TA.id_legajo = p_id_legajo;

      -- No se permite eliminar Empresas con Acciones asociadas.
      if v_PermiteEliminar = 0 then
        delete IPJ.T_TRAMITESIPJ_PERSJUR
        where
          Id_Tramite_Ipj = p_Id_Tramite_Ipj and
          id_legajo = p_id_legajo;

        -- Elimino el sticker de los que estan asociados al legajo
        select substr(sticker, 1, length(sticker)-3) || 'XX' || substr(sticker, length(sticker)-2, length(sticker)) into v_tramite_suac
        from IPJ.t_tramitesipj
        where
          Id_Tramite_Ipj = p_Id_Tramite_Ipj;

        delete ipj.t_tramites_legajo l
          where
            l.sticker = v_tramite_suac and
            l.id_legajo = p_id_legajo;
      else
        o_rdo:= ' No se puede elimiar la Persona Jur�dica porque existen Acciones asociadas.';
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
        return;
      end if;
    else
      select count(*) into v_existe
      from IPJ.T_TRAMITESIPJ_PERSJUR
      where
        Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        id_legajo = p_id_legajo;

      if v_existe = 0 then
        --SELECT IPJ.SEQ_TramitesIpj_Pers.nextval INTO v_id_legajo FROM dual;

        insert into IPJ.T_TRAMITESIPJ_PERSJUR (Id_Tramite_Ipj, id_legajo, id_sede, error_dato)
        values (p_Id_Tramite_Ipj, p_id_legajo, p_id_sede, p_error_dato);
      end if;

      -- Busco las notas de SUAC, si existen
      IPJ.TRAMITES_SUAC.SP_Bajar_Notas_SUAC(
        o_rdo => o_rdo,
        o_tipo_mensaje => o_tipo_mensaje,
        p_Id_Tramite_Ipj => p_Id_Tramite_Ipj
      );
      if o_tipo_mensaje <> IPJ.TYPES.C_TIPO_MENS_OK then
        return;
      end if;

      --********** Asocio el tr�mite al legajo si no estaba asociado **********
      select sticker into v_tramite_suac
      from IPJ.t_tramitesipj
      where
        Id_Tramite_Ipj = p_Id_Tramite_Ipj;

      if length(v_tramite_suac) > 13 then
        v_tramite_suac := substr(v_tramite_suac, 1, length(v_tramite_suac)-5) || 'XX' || substr(v_tramite_suac, length(v_tramite_suac)-2, length(v_tramite_suac));
      else
        v_tramite_suac := substr(v_tramite_suac, 1, length(v_tramite_suac)-3) || 'XX' || substr(v_tramite_suac, length(v_tramite_suac)-2, length(v_tramite_suac));
      end if;

      if v_tramite_suac is not null then
        select count(*) into v_Cant
        from ipj.t_tramites_legajo l
        where
          l.sticker = v_tramite_suac and
          l.id_legajo = p_id_legajo;

        if v_Cant = 0 then
          -- Nuevo tramite legajo
          SELECT ipj.seq_tramites_legajo.nextval INTO v_id_tramite_legajo FROM dual;

          insert into ipj.t_tramites_legajo (id_tramite_legajo, id_legajo, sticker)
          values (v_id_tramite_legajo, p_id_legajo, v_tramite_suac);
        end if;
      end if;
    end if;

    -- Si el tramite es hist�rico le pongo como observacion el nombre de la empresa
    select * into v_Row_Tramite
    from IPJ.t_tramitesipj
    where
      Id_Tramite_Ipj = p_Id_Tramite_Ipj;

    if nvl(v_Row_Tramite.id_proceso, 0) = IPJ.TYPES.C_PROCESO_HIST then
      select denominacion_sia into v_obs_Tramite_Hist
      from IPJ.T_LEGAJOS
      where
        id_legajo = p_id_legajo;

      v_obs_Tramite_Hist := 'Para ' || v_obs_Tramite_Hist;

      update IPJ.T_TRAMITESIPJ
      set Observacion = v_obs_Tramite_Hist
      where
        Id_Tramite_Ipj = p_Id_Tramite_Ipj;
    end if;

    o_rdo := TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;

  END SP_Guardar_TramiteIPJ_PersJur;


  PROCEDURE SP_Guardar_TramiteIPJ_Int (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_id_integrante in number,
    p_error_dato in varchar2,
    p_eliminar in number,
    p_id_sexo in varchar2,
    p_nro_documento in varchar2,
    p_pai_cod_pais in varchar2,
    p_id_numero in number,
    p_cuil in varchar2,
    p_detalle in varchar2,
    p_nombre in varchar2,
    p_apellido in varchar2,
    p_n_tipo_documento in varchar2)
  IS
   v_existe number;
   v_id_integrante number;
   v_PermiteEliminar number;
   v_tipo_mensaje number;
  BEGIN
  /*************************************************
     Guarda un nuevo registro en T_TramitesIPJ_Integrante,
     o elimina un registro si p_eliminar = S
     La transaccion la maneja la aplicacion para guardar cabecera y detalles.
  ************************************************/
    if p_eliminar = 1 then
      select count(*) into v_PermiteEliminar
      from IPJ.T_TRAMITESIPJ_ACCIONES ta
      where
        TA.Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        TA.ID_INTEGRANTE = p_id_integrante;

      -- No se permite eliminar Empresas con Acciones asociadas.
      if v_PermiteEliminar = 0 then
        delete IPJ.T_TRAMITESIPJ_INTEGRANTE
        where
          Id_Tramite_Ipj = p_Id_Tramite_Ipj and
          id_integrante = p_id_integrante;
      else
        o_rdo:=  ' No se puede elimiar la Persona F�sica porque existen Acciones asociadas.';
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      end if;

    else
      select count(*) into v_existe
      from IPJ.T_TRAMITESIPJ_INTEGRANTE
      where
        Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        id_integrante = p_id_integrante;

      if v_existe = 0 then
        -- Si el integrante no existe, lo agrego y luego continuo
        if IPJ.VARIOS.Valida_Integrante(p_id_integrante) = 0 then
          IPJ.ENTIDAD_PERSJUR.SP_GUARDAR_INTEGRANTES(
            o_rdo => o_rdo,
            o_tipo_mensaje => v_tipo_mensaje,
            o_id_integrante => v_id_integrante,
            p_id_sexo => p_id_sexo,
            p_nro_documento => p_nro_documento,
            p_pai_cod_pais => p_pai_cod_pais,
            p_id_numero => p_id_numero,
            p_cuil => p_cuil,
            p_detalle => p_detalle,
            p_Nombre => p_nombre,
            p_apellido  => p_apellido,
            p_error_dato => p_error_dato,
            p_n_tipo_documento => p_n_tipo_documento);

          if o_rdo <> TYPES.c_Resp_OK then
            o_rdo := IPJ.VARIOS.MENSAJE_ERROR('INT_NOT') || '. ' || o_rdo;
            o_tipo_mensaje := v_tipo_mensaje;
            return;
          end if;
        else
          if p_error_dato = 'S' then
            update ipj.t_integrantes
            set
              error_dato = p_error_dato,
              detalle = p_detalle
            where
              id_integrante = p_id_integrante;
          end if;
          v_id_integrante := p_id_integrante;
        end if;

        insert into IPJ.T_TRAMITESIPJ_INTEGRANTE (Id_Tramite_Ipj, id_integrante, error_dato)
        values (p_Id_Tramite_Ipj, v_id_integrante, p_error_dato);
      end if;
    end if;

    o_rdo := TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;

  END SP_Guardar_TramiteIPJ_Int;


  PROCEDURE SP_Guardar_TramiteIPJ_Acc (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_id_tramiteIpj_Accion in number,
    p_Id_Tipo_Accion in number,
    p_id_Integrante in number,
    p_DNI_Integrante in varchar2,
    p_id_legajo in number,
    p_CUIT_persjur in varchar2,
    p_id_estado in number,
    p_Cuil_Usuario in varchar2,
    p_Obs_Rubrica in varchar2,
    p_Observacion in varchar2,
    p_id_fondo_comercio in number,
    p_fec_veeduria in varchar2,
    p_id_documento in number,
    p_n_documento in varchar2,
    p_id_pagina in number
    )
  IS
    v_id_tramiteIpj_Accion number;
    v_rdo_estado varchar2(2000);
    v_bloque varchar2(1000);
    v_id_legajo number;
    v_id_Integrante number;
    v_mensaje varchar2(2000);
    v_Ultimo_Estado number;
    v_valid_parametros varchar2(3000);
    v_tipo_mensaje number;
    v_id_fondo_comercio number;
    v_Row_Tramite  IPJ.T_TramitesIPJ%ROWTYPE;
    v_row_accion IPJ.T_TramitesIPJ_Acciones%ROWTYPE;
    v_existe_Acc number;
    v_Id_Tramite_Ipj number;
    v_existen_observadas number;
    v_Control_Update_Grupo varchar2(500);
    v_CursorAcciones  IPJ.TYPES.cursorType;
    v_agrupable number(1);
    v_Numerador ipj.t_protocolos_digital.numerador%TYPE;
    v_Id_Prot_Dig ipj.t_protocolos_digital.Id_Prot_Dig%TYPE;
    v_comp varchar2(1);
  BEGIN
  /*************************************************
     Guarda un nuevo registro en T_TramitesIPJ_Acciones, si no existe
     Si ya existe, si no se actualiza el usuario, entonces se reaiza un update grupal.
     La transaccion la maneja la aplicacion para guardar cabecera y detalles.
  ************************************************/
    if IPJ.Types.c_habilitar_log_SP = 'S' then
      IPJ.VARIOS.SP_GUARDAR_LOG(
        P_METODO => 'SP_Guardar_TramiteIPJ_Acc',
        P_NIVEL => 'Parametros',
        P_ORIGEN => 'DB',
        P_USR_LOG  => 'DB',
        P_USR_WINDOWS => 'Sistema',
        P_IP_PC  => NULL,
        P_EXCEPCION =>
          ' Id Tramite = ' || to_char(p_Id_Tramite_Ipj)
          || ' / Id Tramite Accion = ' || to_char(p_id_tramiteIpj_Accion)
          || ' / Id Tipo Accion = ' || to_char(p_Id_Tipo_Accion)
          || ' / Id Integrante = ' || to_char(p_id_Integrante)
          || ' / DNI = ' || p_DNI_Integrante
          || ' / Id Legajo = ' || p_id_legajo
          || ' / Cuit Empresa = ' || p_CUIT_persjur
          || ' / Id Estado = ' || p_id_estado
          || ' / Cuil Usuario = ' || p_Cuil_Usuario
          || ' / Obs Rubrica = ' || p_Obs_Rubrica
          || ' / Obs = ' || p_Observacion
          || ' / Id FC = ' || to_char(p_id_fondo_comercio)
          || ' / Fec Veeduria = ' || p_fec_veeduria
          || ' / Id Doc = ' || to_char(p_id_documento)
          || ' / Nom Doc = ' || p_n_documento
          || ' / Id Pagina = ' || to_char(p_id_pagina)
      );
    END IF;

    DBMS_OUTPUT.PUT_LINE('1- SP_Guardar_TramiteIPJ_Acc - Valida Parametros');
    if p_Cuil_Usuario <> '1' then -- Si no es el usuario generico de Base de Datos
      t_comunes.pack_persona_juridica.VerificarCUIT (p_Cuil_Usuario, v_mensaje);
      if Upper(trim(v_mensaje)) <> IPJ.TYPES.C_RESP_OK then
        v_valid_parametros := v_valid_parametros || v_mensaje;
      end if;
    end if;
    if nvl(p_Id_Tramite_Ipj, 0) <= 0 then --Debe existir un tramite
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('TRAMITE_NOT');
    end if;
    if IPJ.VARIOS.VALIDA_TIPO_ACCION(p_Id_Tipo_Accion) = 0 then --Debe existir un tipo de accion
      v_valid_parametros := v_valid_parametros ||  IPJ.VARIOS.MENSAJE_ERROR('TACC_NO');
    end if;
    if p_Id_Tipo_Accion not in  (IPJ.Types.C_Tipo_Acc_Retiro, IPJ.Types.c_Tipo_Acc_Archivar_Expediente) and -- (excluyo accion de Retiro Mesa SUAC y Archivo)
      nvl(p_id_Integrante, 0) = 0 and p_DNI_Integrante is null and -- No tiene Persona Fisica
      nvl(p_id_legajo, 0) = 0 and p_CUIT_persjur is null and --No tiene Persona Juridica
      p_id_fondo_comercio= 0 then -- No tiene Fondo de Comercio
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('PERS_NOT');
    end if;
    if IPJ.VARIOS.VALIDA_ESTADO(p_id_estado) = 0 then --Debe existir un estado
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('EST_NOT');
    end if;
    if p_fec_veeduria is not null and IPJ.VARIOS.Valida_Fecha(p_fec_veeduria, 'dd/mm/rrrr HH:MI:SS AM') = 'N'  then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT');
    end if;

    -- Si los paramentros no estan OK, no continuo
    if v_valid_parametros is not null then
      o_rdo := 'Parametros : ' || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;


    /*******************************************************
    Controlo el ultimo estado de la accion, para validar que hace
    *******************************************************/
    select  sum(nvl(TACC.ID_ESTADO , 0)) into v_Ultimo_Estado
    from IPJ.T_TRAMITESIPJ_acciones tacc
    where
      Id_Tramite_Ipj = p_Id_Tramite_Ipj and
      id_tramiteipj_accion = p_id_tramiteIpj_Accion;

    -- Si la accion estaba cerrada, no dejo realizar nada.
    if v_Ultimo_Estado > IPJ.TYPES.c_Estados_Completado and v_Ultimo_Estado != p_id_estado  and p_id_estado <> IPJ.Types.c_Estados_Proceso then
      o_rdo := 'ERROR: La Acci�n ' || to_char(p_id_tramiteIpj_Accion) || ' ya se encuentra cerrada, refresque sus datos.';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    -- Controlo que para pasar a OBSERVADO, exista alguna observacion pendiente
    if v_Ultimo_Estado <> IPJ.TYPES.C_ESTADOS_OBSERVADO and p_id_estado = IPJ.TYPES.C_ESTADOS_OBSERVADO then
      select count(*) into v_existen_observadas
      from IPJ.t_Entidades_notas
      where
        Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        id_estado_nota  <> 1 ;

      if v_existen_observadas = 0 then
        o_rdo := 'ERROR: La Acci�n ' || to_char(p_id_tramiteIpj_Accion) || ' no cuenta con observaciones pendientes.';
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
        return;
       end if;
    end if;

    -- Si viene el CUIT y no la id_legajo, lo busco.
    v_id_legajo := p_id_legajo;

    -- Si viene el DNI y no la id_integrante, lo busco.
    if nvl(p_id_Integrante, 0) = 0 and p_DNI_Integrante is not null then
      begin
        select ti.id_integrante into v_id_Integrante
        from IPJ.T_TRAMITESIPJ_INTEGRANTE ti join IPJ.T_INTEGRANTES i
          on TI.ID_INTEGRANTE = I.ID_INTEGRANTE
        where
          ti.Id_Tramite_Ipj = p_Id_Tramite_Ipj and
          I.NRO_DOCUMENTO = p_DNI_Integrante;
      EXCEPTION
        WHEN NO_DATA_FOUND then v_id_Integrante := p_id_Integrante;
      end;
    else
      v_id_Integrante := p_id_Integrante;
    end if;

    -- Si es una nueva accion
    if nvl(p_id_tramiteIpj_Accion, 0) = 0 then
      select count(*) into v_existe_Acc
      from IPJ.t_tramitesipj_acciones
      where
        Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        nvl(id_legajo, 0) = nvl(v_id_legajo, 0) and
        nvl(id_integrante, 0) = nvl(v_id_Integrante, 0) and
        Id_Tipo_Accion = p_Id_Tipo_Accion;

      if p_id_tramiteIpj_Accion > 0 then
        o_rdo := 'ERROR: La acci�n que intenta crear ya existe para �ste tr�mite, no se pueden crear dos acciones iguales.';
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
        return;
      end if;
    end if;

   -- Verifico que se encuentre los datos de las personas o fondos (excluyo accion de Retiro Mesa SUAC)
    if p_Id_Tipo_Accion <> IPJ.Types.C_Tipo_Acc_Retiro and
      nvl(v_id_Integrante, 0) = 0 and
      nvl(v_id_legajo, 0) = 0  and
      nvl(p_id_fondo_comercio, 0) = 0 then
      o_rdo := IPJ.VARIOS.MENSAJE_ERROR('PERS_NOT');
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    -- Busco el tipo de proceso del tramite
    select * into v_Row_Tramite
    from IPJ.t_tramitesipj
    where
      Id_Tramite_Ipj = p_Id_Tramite_Ipj;

    -- Si no hay integrante o entidad, pongo NULL para no activar la FK
    v_id_Integrante := (case when v_id_Integrante = 0 then null else v_id_Integrante end);
    v_id_legajo := (case when v_id_legajo = 0 then null else v_id_legajo end);
    v_id_fondo_comercio := (case when p_id_fondo_comercio = 0 then null else p_id_fondo_comercio end);

    if nvl(v_Row_Tramite.id_proceso, 0) = IPJ.TYPES.C_PROCESO_HIST then
      DBMS_OUTPUT.PUT_LINE('2- SP_Guardar_TramiteIPJ_Acc - Manejo de Historicos');
      -- Si en un tramite HISTORICO ya existe una accion de otro tipo para la misma empresa,
      -- se crea un nuevo tramite y PERSJUR para asociar la nueva accion de anera independiente
      select count(*) into v_existe_Acc
      from IPJ.t_tramitesipj_acciones
      where
        Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        nvl(id_legajo, 0) = nvl(v_id_legajo, 0) and
        nvl(id_integrante, 0) = nvl(v_id_Integrante, 0) and
        Id_Tipo_Accion <> p_Id_Tipo_Accion;

      if v_existe_Acc > 0 then
        -- Genero un nuevo tramite con los datos pasados, nuevo id, y id_proceso hist�rico
        SELECT ipj.SEQ_tramites.nextval INTO v_Id_Tramite_Ipj FROM dual;

        insert into ipj.t_tramitesipj
          (Id_Tramite_Ipj, id_tramite, id_clasif_ipj, id_grupo_ult_estado, cuil_ult_estado,
           id_estado_ult, fecha_inicio, cuil_creador, observacion, id_ubicacion, urgente,
           id_ubicacion_origen, id_proceso, sticker, expediente,simple_tramite)
        values
          (v_Id_Tramite_Ipj, (case when v_Row_Tramite.id_tramite = 0 then null else v_Row_Tramite.id_tramite end), v_Row_Tramite.id_clasif_ipj,
           v_Row_Tramite.id_grupo_ult_estado, v_Row_Tramite.cuil_ult_estado,
           v_Row_Tramite.id_estado_ult, to_date(sysdate, 'dd/mm/rrrr'), v_Row_Tramite.cuil_creador,
           v_Row_Tramite.observacion, v_Row_Tramite.id_ubicacion, v_Row_Tramite.urgente,
           v_Row_Tramite.id_ubicacion, IPJ.TYPES.C_PROCESO_HIST,
           v_Row_Tramite.sticker, v_Row_Tramite.expediente,v_Row_Tramite.Simple_Tramite);

        IPJ.TRAMITES.SP_GUARDAR_TRAMITES_IPJ_ESTADO(
          o_rdo => v_rdo_estado,
          o_tipo_mensaje => v_tipo_mensaje,
          v_fecha_pase => sysdate,
          v_id_estado => v_Row_Tramite.id_estado_ult,
          v_cuil_usuario => v_Row_Tramite.cuil_ult_estado,
          v_id_grupo => v_Row_Tramite.id_grupo_ult_estado,
          v_observacion => v_Row_Tramite.observacion,
          v_Id_Tramite_Ipj => v_Id_Tramite_Ipj);

        if upper(trim(v_rdo_estado)) <> IPJ.TYPES.C_RESP_OK then
          o_rdo :=  'Guardar Tramite Estado --' || v_rdo_estado;
          o_tipo_mensaje := v_tipo_mensaje;
          return;
        end if;

      DBMS_OUTPUT.PUT_LINE('3- SP_Guardar_TramiteIPJ_Acc - Manejo de Historicos (crea tramite y accion)');
        -- Creo una nueva entrada para la Empresa o Persona
        if nvl(p_id_legajo, 0) > 0 then
          IPJ.Tramites.SP_Guardar_TramiteIPJ_PersJur (
            o_rdo => o_rdo,
            o_tipo_mensaje => o_tipo_mensaje,
            p_Id_Tramite_Ipj => v_Id_Tramite_Ipj,
            p_id_legajo => v_id_legajo,
            p_id_sede => '00',
            p_error_dato => 'N',
            p_eliminar => 0 );
        else
          IPJ.Tramites.SP_Guardar_TramiteIPJ_Int (
            o_rdo => o_rdo,
            o_tipo_mensaje => o_tipo_mensaje,
            p_Id_Tramite_Ipj => v_Id_Tramite_Ipj,
            p_id_integrante => v_id_Integrante,
            p_error_dato => 'N',
            p_eliminar => 0,
            p_id_sexo => null,
            p_nro_documento => null,
            p_pai_cod_pais => null,
            p_id_numero => null,
            p_cuil => null,
            p_detalle => null,
            p_nombre => null,
            p_apellido => null,
            p_n_tipo_documento => null);
        end if;

        if upper(trim(o_rdo)) <> IPJ.TYPES.C_RESP_OK then
          return;
        end if;
      else
        -- si no existe una accion para el tramite, dejo el que viene
        v_Id_Tramite_Ipj := p_Id_Tramite_Ipj;
      end if;
    else
      -- si no es historio, dejo el que viene
      v_Id_Tramite_Ipj := p_Id_Tramite_Ipj;
    end if;

    v_bloque := 'Guardar Accion: ';
    -- Si la accion es nueva se agrega
    if nvl(p_id_tramiteIpj_Accion, 0) = 0 then
      DBMS_OUTPUT.PUT_LINE('4- SP_Guardar_TramiteIPJ_Acc - Inserta la accion');
      SELECT IPJ.SEQ_TramitesIpj_Acc.nextval INTO v_id_tramiteIpj_Accion FROM dual;

      insert into IPJ.T_TRAMITESIPJ_ACCIONES
        (Id_Tramite_Ipj, id_tramiteIpj_Accion, id_tipo_accion, id_integrante, id_legajo,
         id_estado, cuil_usuario, obs_rubrica, observacion, id_fondo_comercio, fec_veeduria,
         id_documento, n_documento)
      values
        (v_Id_Tramite_Ipj, v_id_tramiteIpj_Accion, p_id_tipo_accion, v_id_integrante, v_id_legajo,
         p_id_estado, p_cuil_usuario, p_obs_rubrica, p_observacion, v_id_fondo_comercio,
         to_date(p_fec_veeduria, 'dd/mm/rrrr HH:MI:SS AM'), p_id_documento, p_n_documento);

      -- Veo si la acci�n tiene asociado un protocolo digital
      select ID_PROT_DIG into v_id_prot_dig
      from ipj.t_tipos_accionesipj
      where
        id_tipo_accion = p_id_tipo_accion;

        --Resoluci�n Digital- El numero de resoluci�n no es incremental, siempre sugiere "1"
      IF v_id_prot_dig is not null THEN

        --Buscamos si es compartido o no
        SELECT compartido
          INTO v_comp
          FROM ipj.t_protocolos_digital
         WHERE id_prot_dig = v_id_prot_dig;

        IF v_comp = 'N' THEN

          UPDATE ipj.t_protocolos_digital
             SET numerador = numerador + 1
           WHERE id_prot_dig = v_id_prot_dig;

          SELECT p.numerador, Id_Prot_Dig
            INTO v_Numerador, v_Id_Prot_Dig
            FROM ipj.t_protocolos_digital p
           WHERE p.id_prot_dig = v_id_prot_dig;

        ELSIF v_comp = 'S' AND v_id_prot_dig = 'L' THEN
        -- Actualizamos el numerador
          UPDATE ipj.t_prot_dig_area
             SET numerador = numerador + 1
           WHERE id_prot_dig = v_id_prot_dig
             AND id_ubicacion IN (1,4);

          SELECT pa.numerador, pa.Id_Prot_Dig
            INTO v_Numerador, v_Id_Prot_Dig
            FROM ipj.t_prot_dig_area pa
           WHERE pa.id_prot_dig = v_id_prot_dig
             AND pa.id_ubicacion = v_Row_Tramite.Id_Ubicacion;

        ELSE
        -- Actualizamos el numerador
          UPDATE ipj.t_prot_dig_area
             SET numerador = numerador + 1
           WHERE id_prot_dig = v_id_prot_dig
             AND id_ubicacion = v_Row_Tramite.Id_Ubicacion;

          SELECT pa.numerador, pa.Id_Prot_Dig
            INTO v_Numerador, v_Id_Prot_Dig
            FROM ipj.t_prot_dig_area pa
           WHERE pa.id_prot_dig = v_id_prot_dig
             AND pa.id_ubicacion = v_Row_Tramite.Id_Ubicacion;

        END IF;

        -- Actualizo los datos del protocolo en la acci�n
        update IPJ.T_TRAMITESIPJ_ACCIONES
        set
          id_prot_dig = v_Id_Prot_Dig,
          nro_resolucion_dig = v_Numerador,
          fec_resolucion_dig = decode(fec_resolucion_dig, null, to_date(sysdate, 'dd/mm/rrrr'), fec_resolucion_dig)
        where
          Id_Tramite_Ipj = v_Id_Tramite_Ipj and
          id_tramiteIpj_Accion = v_id_tramiteIpj_Accion and
          id_tipo_accion = p_id_tipo_accion;

        --Se actualiza el n�mero de resoluci�n al momento que se incrementa el numerador, si es una const.
        if p_id_tipo_accion = 135 then
          UPDATE ipj.t_entidades
             SET nro_resolucion = nvl(nro_resolucion, trim(to_char(v_Numerador,'009')) || ' ' || v_Id_Prot_Dig || '/' || to_char(to_date(SYSDATE, 'dd/mm/rrrr'), 'yy'))
           WHERE id_legajo = v_id_legajo
             AND id_tramite_ipj = p_id_tramite_ipj;
        end if;
      END IF;

      -- Se cargan las acciones de la accion, si no es una carga hist�rica
      v_bloque := 'Carga de Tasas';
      if nvl(v_Row_Tramite.id_proceso, 0) <> IPJ.TYPES.C_PROCESO_HIST then
        IPJ.TRAMITES.SP_Cargar_Tasas_Accion (
           o_rdo => v_rdo_estado,
           o_tipo_mensaje => v_tipo_mensaje,
           p_Id_Tramite_Ipj => v_Id_Tramite_Ipj,
           p_id_tramiteipj_accion => v_id_tramiteIpj_Accion,
           p_id_tipo_accion => p_id_tipo_accion);

        if upper(trim(v_rdo_estado)) <> IPJ.TYPES.C_RESP_OK then
          o_rdo :=  v_bloque || '--' || v_rdo_estado;
          o_tipo_mensaje := v_tipo_mensaje;
          return;
        end if;
      end if;

    else
    DBMS_OUTPUT.PUT_LINE('4- SP_Guardar_TramiteIPJ_Acc - Actualiza la accion');
      -- Busco los datos anteriores de la accion
      select * into v_row_accion
      from IPJ.T_TRAMITESIPJ_ACCIONES
      where
        Id_Tramite_Ipj = v_Id_Tramite_Ipj and
        id_tramiteIpj_Accion = p_id_tramiteIpj_Accion and
        Id_Tipo_Accion = p_Id_Tipo_Accion;

      -- Busco si la accion es Agrupable
      select agrupable into v_agrupable
      from IPJ.t_paginas p
      where
        id_pagina = p_id_pagina;

      -- Si se cambia el usuario, es un rechazo o no es agrupable, actualizo esa sola accion
      if (v_Row_Accion.cuil_usuario <> p_cuil_usuario) or (p_id_estado = IPJ.TYPES.C_ESTADOS_RECHAZADO) or (v_agrupable = 0) then
        update IPJ.T_TRAMITESIPJ_ACCIONES
        set
          id_estado = p_id_estado,
          cuil_usuario = p_cuil_usuario,
          observacion = (case
                                  when p_observacion is null then observacion
                                  else substr(p_observacion || chr(13) || observacion, 1, 4000)
                               end),
          obs_rubrica = p_obs_rubrica,
          FEC_VEEDURIA = to_date(p_FEC_VEEDURIA, 'dd/mm/rrrr HH:MI:SS AM'),
          id_documento = (case when nvl(p_id_documento, 0) = 0 then id_documento else p_id_documento end),
          n_documento = (case when nvl(p_id_documento, 0) = 0 then n_documento else p_n_documento end)
        where
          Id_Tramite_Ipj = v_Id_Tramite_Ipj and
          id_tramiteIpj_Accion = p_id_tramiteIpj_Accion and
          Id_Tipo_Accion = p_Id_Tipo_Accion;
      else
        -- Controlo si se se pierde algun dato en la actualizacion grupal
        if IPJ.Types.c_habilitar_log_SP = 'S' then
          select
            (case when count(distinct observacion) <= 1 then NULL else 'Obs - ' end) ||
            (case when count(distinct obs_rubrica ) <= 1 then NULL else 'Obs Rub - ' end) ||
            (case when count(distinct FEC_VEEDURIA) <= 1 then NULL else 'Veed - ' end) ||
            (case when count(distinct id_documento) <= 1 then NULL else 'Id Doc - ' end) ||
            (case when count(distinct  n_documento) <= 1 then NULL else 'N Doc ' end)  into v_Control_Update_Grupo
          from IPJ.T_TRAMITESIPJ_ACCIONES
          where
            Id_Tramite_Ipj = v_Id_Tramite_Ipj and
            cuil_usuario = v_Row_Accion.cuil_usuario and
            nvl(id_legajo, 0) = nvl(v_Row_Accion.id_legajo, 0) and
            nvl(id_integrante, 0) = nvl(v_Row_Accion.id_integrante, 0) and
            id_estado = v_Row_Accion.id_estado and
            id_tipo_accion in (select a.id_tipo_accion
                                      from IPJ.t_tipos_accionesipj a join ipj.t_paginas p
                                        on a.id_pagina = p.id_pagina
                                      where
                                        a.id_pagina = p_id_pagina and p.agrupable = 1);

          if v_Control_Update_Grupo is not null then
            IPJ.VARIOS.SP_GUARDAR_LOG(
              p_METODO => 'SP_Guardar_TramiteIPJ_Acc',
              p_NIVEL => 'Update Grupal',
              p_ORIGEN => 'DB',
              p_USR_LOG  => 'DB',
              p_USR_WINDOWS => 'Sistema',
              p_IP_PC  => null,
              p_EXCEPCION =>
                'Se pierden datos de ' || v_Control_Update_Grupo
                || '; se uso la clave  de agrupado '
                || ' Tramite = ' || to_char(v_Id_Tramite_Ipj)
                || ' Usr = ' || v_Row_Accion.cuil_usuario
                || ' Legajo = ' || to_char(v_Row_Accion.id_legajo)
                || ' Integrante = ' || to_char(v_Row_Accion.id_integrante)
                || ' Estado = ' || to_char(v_Row_Accion.id_estado)
                || ' Accion = ' || to_char(p_id_pagina)
            );
          end if;
        end if;

        -- Actualizo todas del mismo Usuario, Pagina, Estado y Persona (fisica o jur�dica)
        update IPJ.T_TRAMITESIPJ_ACCIONES
        set
          id_estado = p_id_estado,
          cuil_usuario = p_cuil_usuario,
          observacion = (case
                                  when p_observacion is null then observacion
                                  else substr(p_observacion || chr(13) || observacion, 1, 4000)
                               end),
          obs_rubrica = p_obs_rubrica,
          FEC_VEEDURIA = to_date(p_FEC_VEEDURIA, 'dd/mm/rrrr HH:MI:SS AM'),
          id_documento = (case when nvl(p_id_documento, 0) = 0 then id_documento else p_id_documento end),
          n_documento = (case when nvl(p_id_documento, 0) = 0 then n_documento else p_n_documento end)
        where
          Id_Tramite_Ipj = v_Id_Tramite_Ipj and
          cuil_usuario = v_Row_Accion.cuil_usuario and
          nvl(id_legajo, 0) = nvl(v_Row_Accion.id_legajo, 0) and
          nvl(id_integrante, 0) = nvl(v_Row_Accion.id_integrante, 0) and
          id_estado = v_Row_Accion.id_estado and
          id_tipo_accion in (select id_tipo_accion from IPJ.t_tipos_accionesipj where id_pagina = p_id_pagina);

      end if;

      v_id_tramiteIpj_Accion := p_id_tramiteIpj_Accion;
    end if;

    --Guardo el historico de acciones
    v_bloque := 'Estado Acci�n: ';
    DBMS_OUTPUT.PUT_LINE('4- SP_Guardar_TramiteIPJ_Acc - Actualiza historia de estados');
    if p_id_tramiteIpj_Accion = 0 or v_Row_Accion.cuil_usuario <> p_cuil_usuario  or
      (p_id_estado = IPJ.TYPES.C_ESTADOS_RECHAZADO) or (v_agrupable = 0) then
      IPJ.TRAMITES.SP_GUARDAR_TRAMITES_ACC_ESTADO(
        o_rdo => v_rdo_estado,
        o_tipo_mensaje => v_tipo_mensaje,
        p_fecha_pase => sysdate,
        p_id_estado => p_id_estado,
        p_cuil_usuario => p_Cuil_Usuario,
        p_observacion => p_observacion,
        p_Id_Tramite_Ipj => v_Id_Tramite_Ipj,
        p_id_tramite_Accion => v_id_tramiteIpj_Accion,
        p_id_tipo_accion => p_id_tipo_accion,
        p_id_documento => p_id_documento,
        p_n_documento => p_n_documento );
    else
      -- Guardo los cambios en todas las acciones involucradas
      OPEN v_CursorAcciones FOR
        select *
        from IPJ.T_TRAMITESIPJ_ACCIONES
         where
            Id_Tramite_Ipj = v_Id_Tramite_Ipj and
            cuil_usuario = v_Row_Accion.cuil_usuario and
            nvl(id_legajo, 0) = nvl(v_Row_Accion.id_legajo, 0) and
            nvl(id_integrante, 0) = nvl(v_Row_Accion.id_integrante, 0) and
            id_estado = v_Row_Accion.id_estado and
            id_tipo_accion in (select id_tipo_accion from IPJ.t_tipos_accionesipj where id_pagina = p_id_pagina);

      LOOP
        fetch v_CursorAcciones into v_Row_Accion;
        EXIT WHEN v_CursorAcciones%NOTFOUND;

        IPJ.TRAMITES.SP_GUARDAR_TRAMITES_ACC_ESTADO(
          o_rdo => v_rdo_estado,
          o_tipo_mensaje => v_tipo_mensaje,
          p_fecha_pase => sysdate,
          p_id_estado => v_Row_Accion.id_estado,
          p_cuil_usuario => v_Row_Accion.Cuil_Usuario,
          p_observacion => v_Row_Accion.Observacion,
          p_Id_Tramite_Ipj => v_Row_Accion.Id_Tramite_Ipj,
          p_id_tramite_Accion => v_Row_Accion.id_tramiteIpj_Accion,
          p_id_tipo_accion => v_Row_Accion.id_tipo_accion,
          p_id_documento => v_Row_Accion.id_documento,
          p_n_documento => v_Row_Accion.n_documento);

        if upper(trim(v_rdo_estado)) <> IPJ.TYPES.C_RESP_OK then
          o_rdo :=  v_bloque || '--' || v_rdo_estado;
          o_tipo_mensaje := v_tipo_mensaje;
          return;
        end if;
      END LOOP;

      CLOSE v_CursorAcciones;
    end if;

    if upper(trim(v_rdo_estado)) <> IPJ.TYPES.C_RESP_OK then
      o_rdo :=  v_bloque || '--' || v_rdo_estado;
      o_tipo_mensaje := v_tipo_mensaje;
      return;
    end if;

    DBMS_OUTPUT.PUT_LINE('4- SP_Guardar_TramiteIPJ_Acc - Finaliza');
    if o_rdo is null then
      o_rdo := IPJ.TYPES.C_RESP_OK;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    end if;
  EXCEPTION
    WHEN OTHERS THEN
      o_rdo := 'SP_Guardar_TramiteIPJ_Acc: ' ||v_bloque || To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Guardar_TramiteIPJ_Acc;

   PROCEDURE SP_Validar_TramiteIPJ_Acc (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number,
    p_DNI_integrante in varchar2)
  IS
    v_Id_Tramite_Ipj number;
    v_usuario varchar2(50);
    v_sticker varchar2(50);
    v_expediente varchar2(50);
    v_id_proceso number;
  BEGIN
    /*********************************************************
      Verifica que no exista otro tramite con acciones abiertas, que modifican a la misma persona..
    *********************************************************/
    -- Busco el tipo de proceso parra saber que tipo de tramite es
    begin
      select nvl(id_proceso, 0) into v_id_proceso
      from IPJ.t_tramitesipj
      where Id_Tramite_Ipj = p_Id_Tramite_Ipj;
    exception
      when OTHERS then
        v_id_proceso := 0;
    end;


    if v_id_proceso not in (IPJ.TYPES.C_PROCESO_GEN_HIST, IPJ.TYPES.C_PROCESO_HIST) then
      -- Busco el primer tr�mite abierto con acciones para la misma persona
      begin
        if nvl(p_DNI_integrante, '0') <> '0' then
          select t.Id_Tramite_Ipj, u.Descripcion, nvl(t.Sticker, SUAC.nro_sticker), nvl(T.EXPEDIENTE, SUAC.NRO_TRAMITE)
            into v_Id_Tramite_Ipj, v_usuario, v_sticker, v_expediente
          from IPJ.T_TRAMITESIPJ t join IPJ.T_TRAMITESIPJ_ACCIONES tacc
              on t.Id_Tramite_Ipj = tacc.Id_Tramite_Ipj
            join IPJ.t_tipos_accionesipj ac
              on TACC.ID_TIPO_ACCION = AC.ID_TIPO_ACCION
            join NUEVOSUAC.VT_TRAMITES_IPJ_DESA suac
              on SUAC.ID_TRAMITE = T.ID_TRAMITE
            join IPJ.t_usuarios u
              on T.CUIL_ULT_ESTADO = U.CUIL_USUARIO
            join IPJ.T_INTEGRANTES i
              on TACC.ID_INTEGRANTE = I.ID_INTEGRANTE
          where
            T.ID_ESTADO_ULT < IPJ.TYPES.c_Estados_Completado and
            t.Id_Tramite_Ipj <> p_Id_Tramite_Ipj and
            nvl(AC.ID_PROTOCOLO, 0) > 0 and
            I.NRO_DOCUMENTO = p_DNI_integrante and
            ROWNUM=1;
        else
          select t.Id_Tramite_Ipj, U.DESCRIPCION, nvl(t.Sticker, SUAC.nro_sticker), nvl(T.EXPEDIENTE, SUAC.NRO_TRAMITE)
            into v_Id_Tramite_Ipj, v_usuario, v_sticker, v_expediente
          from IPJ.T_TRAMITESIPJ t join IPJ.T_TramitesIpj_PersJur TPJ
              on T.Id_Tramite_Ipj = TPJ.Id_Tramite_Ipj
            join NUEVOSUAC.VT_TRAMITES_IPJ_DESA suac
              on SUAC.ID_TRAMITE = T.ID_TRAMITE
            join IPJ.t_tramitesIpj_Acciones tacc
              on t.Id_Tramite_Ipj = tacc.Id_Tramite_Ipj and TPJ.id_legajo = TACC.id_legajo
            join IPJ.t_tipos_accionesipj ac
              on TACC.ID_TIPO_ACCION = AC.ID_TIPO_ACCION
            join IPJ.t_usuarios u
              on t.CUIL_ULT_ESTADO = U.CUIL_USUARIO
          where
            T.ID_ESTADO_ULT < IPJ.TYPES.c_Estados_Completado and
            t.Id_Tramite_Ipj <> p_Id_Tramite_Ipj and
            nvl(AC.ID_PROTOCOLO, 0) > 0 and
            tacc.id_legajo = p_id_legajo and
            ROWNUM=1;
        end if;
      exception
        when NO_DATA_FOUND then
          v_Id_Tramite_Ipj := 0;
        when OTHERS then
          v_Id_Tramite_Ipj := 0;
      end;
    else
      -- para tramites historicos no controlo la existencia de otros tramites previos
       v_Id_Tramite_Ipj := 0;
    end if;

    if v_Id_Tramite_Ipj = 0 then
      o_rdo := IPJ.TYPES.C_RESP_OK;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    else
      o_rdo := 'ADVERTENCIA: El Expediente Nro. ' || v_expediente || ' asignado a ' || v_usuario ||
        ' est� modificando la misma Persona. Desea crear esta acci�n?';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
    end if;

  END SP_Validar_TramiteIPJ_Acc;


  PROCEDURE SP_Informar_Estado_Tramite (
    o_rdo out number,
    p_Id_Tramite_Ipj in number,
    p_id_tramite_accion in number)
  IS
    v_ultimo_estado number;
  BEGIN

    begin
      if nvl(p_id_tramite_accion, 0) = 0 then
        select T.ID_ESTADO_ULT into v_ultimo_estado
        from IPJ.T_tramitesipj t
        where
          Id_Tramite_Ipj = p_Id_Tramite_Ipj;
      else
        select Tacc.ID_ESTADO into v_ultimo_estado
        from IPJ.T_tramitesipj_acciones tacc
        where
          Id_Tramite_Ipj = p_Id_Tramite_Ipj and
          id_tramiteipj_accion = p_id_tramite_accion;
      end if;
    exception
      when NO_DATA_FOUND then v_ultimo_estado := 1;
      when OTHERS then v_ultimo_estado := 0;
    end;

    o_rdo := v_ultimo_estado;
  END SP_Informar_Estado_Tramite;


  PROCEDURE SP_Buscar_Tramites_Hist (
    o_Cursor out IPJ.TYPES.CURSORTYPE ,
    p_ubicacion in number,
    p_clasificacion in number,
    p_sticker in varchar2,
    p_cuit in varchar2,
    p_nro_documento in varchar2,
    p_nombre in varchar2,
    p_fecha_inicio in varchar2,
    p_fecha_fin in varchar2,
    p_tipo_operacion in number, --1- Tr�mite, 2-Acci�n - 3- Ambos
    p_id_legajo in number ,
    p_expediente in varchar2,
    p_id_estado number
  )
  IS
    v_id_estado number := 0;
    v_tipo_tramites number := 1;
    v_tipo_acciones number := 2;
  BEGIN

    OPEN o_Cursor FOR
      select
        botones, Clase, Id_Tramite_Suac, Id_Tramite_Suac Id_Suac, Sticker,
        Nro_Expediente, Tipo, Asunto,  Fecha_Inicio_Suac,  Fecha_recepcion,
        Id_Tramite_Ipj, id_clasif_ipj, id_tipo_accion, Observacion, id_estado,
        cuil_usuario,
        /*case
          when clase = 1 and Acc_Abiertas = 0  then 'Completo'
          when clase = 1 and Acc_Abiertas <> 0  then 'Pendiente'
          when clase = 2 then n_estado
        end*/ n_estado,
        Tipo_IPJ, to_char(Fecha_Inicio_Ipj, 'dd/mm/rrrr') Fecha_Asignacion, Acc_Abiertas,
        Acc_Cerradas, URGENTE, Usuario, nvl(Error_Dato, 'N') Error_Dato,
        cuit, razon_social, id_legajo, N_UBICACION, CUIL_CREADOR, id_ubicacion,
        id_tramiteipj_accion, id_protocolo,
        ID_INTEGRANTE, NRO_DOCUMENTO, Cuit_PersJur, Obs_Rubrica,
        identificacion, persona, id_fondo_comercio, id_pagina, id_proceso,
        Id_Documento, N_Documento,simple_tramite, id_ubicacion_origen
      from
        ( select
           IPJ.varios.FC_Habilitar_Botones (
               ub.id_ubicacion,
               tIPJ.id_clasif_ipj,
               0,
               tIPJ.id_estado_ult,
               IPJ.TRAMITES.FC_Acciones_Abiertas (tIPJ.Id_Tramite_Ipj),
               0,
               2,
               tIPJ.id_proceso,
               tIPJ.simple_tramite,
               nvl(gu.CUIL_USUARIO, u.cuil_usuario),
               nvl(gu.CUIL_USUARIO, u.cuil_usuario),
               tIPJ.id_tramite_ipj,
               0
           ) botones,
           1 Clase, nvl(VT_SUAC.ID_TRAMITE, 0) Id_Tramite_Suac, nvl(tIPJ.Sticker, VT_SUAC.nro_sticker) Sticker,
           nvl(TIPJ.EXPEDIENTE , VT_SUAC.nro_tramite) Nro_Expediente,
            VT_SUAC."Tipo/Subtipo" Tipo, VT_SUAC.Asunto,
            to_char(VT_SUAC.fecha_inicio, 'dd/mm/rrrr') Fecha_Inicio_Suac,  to_char(VT_SUAC.fecha_ultima_recepcion, 'dd/mm/rrrr') Fecha_recepcion,
            tIPJ.Id_Tramite_Ipj, tIPJ.id_clasif_ipj, 0 id_tipo_accion, tIPJ.Observacion, tIPJ.id_estado_ult id_estado,
            u.cuil_usuario, e.n_estado, tc.N_Clasif_Ipj Tipo_IPJ,
            TIPJ.FECHA_INICIO Fecha_Inicio_Ipj,
            IPJ.TRAMITES.FC_Acciones_Abiertas (tIPJ.Id_Tramite_Ipj) Acc_Abiertas,
            IPJ.TRAMITES.FC_Acciones_Cerradas (tIPJ.Id_Tramite_Ipj) Acc_Cerradas, TIPJ.URGENTE,
            nvl(u.descripcion, gr.n_grupo) usuario, 'N' Error_Dato,
            '' cuit, '' razon_social, 0 id_legajo, UB.N_UBICACION, TIPJ.CUIL_CREADOR, ub.id_ubicacion,
            0 id_tramiteipj_accion, 0 id_protocolo,
            0 ID_INTEGRANTE, null NRO_DOCUMENTO, null Cuit_PersJur, '' Obs_Rubrica,
            '' identificacion, '' persona, null id_fondo_comercio, 0 id_pagina, tIPJ.id_proceso,
            null Id_Documento, null N_Documento,TIPJ.simple_tramite, tIPJ.id_ubicacion_origen
          from
            IPJ.t_tramitesIPJ tIPJ left join NUEVOSUAC.VT_TRAMITES_IPJ_DESA vt_suac
              on VT_SUAC.ID_TRAMITE = tIPJ.id_tramite
            join IPJ.t_estados e
               on tIPJ.id_estado_ult = e.id_estado
            left join IPJ.t_usuarios u
              on tIPJ.Cuil_Ult_Estado = u.cuil_usuario
            left join IPJ.t_grupos_Usuario gu
              on tIPJ.id_Grupo_Ult_Estado = gu.id_grupo
            left join IPJ.t_Grupos gr
              on  tIPJ.id_Grupo_Ult_Estado = gr.id_grupo
            left join IPJ.T_Ubicaciones ub
              on TIPJ.ID_UBICACION = ub.id_ubicacion
            left join IPJ.t_tipos_Clasif_ipj tc
               on TIPJ.Id_Clasif_Ipj = tc.Id_Clasif_Ipj
          where
            bitand(p_tipo_operacion, v_tipo_tramites) = v_tipo_tramites and -- Filtra que se consulte tramites
            ( nvl(p_id_estado, 0) = 0 or tIPJ.id_estado_ult = p_id_estado) and  -- Busca el estado indicado (no se ofrece en el filtro)
            (nvl(p_ubicacion, 0) = 0 or TIPJ.ID_UBICACION = p_ubicacion) and -- Si se pasa una area de trabajo, busca por esa
            (nvl(p_clasificacion, 0) = 0 or TIPJ.ID_CLASIF_IPJ = p_clasificacion) and -- Si se pasa una clasificacion, filtra por esa
            (p_sticker is null or nvl(tIPJ.Sticker, VT_SUAC.nro_sticker) = p_sticker) and -- Si se pasa un sticker, lo filtra
            (p_expediente is null or nvl(tIPJ.expediente, VT_SUAC.nro_tramite) = p_expediente) and -- Si se pasa un expediente, lo filtra
            (p_cuit is null or TIPJ.Id_Tramite_Ipj in                                --Si se pasa un CUIT, busca los datos de esa empresa
               ( select distinct tacc.Id_Tramite_Ipj
                  from ipj.t_tramitesipj_acciones tacc join ipj.t_legajos L
                     on TACC.id_legajo = L.id_legajo
                 where
                   L.cuit = p_cuit
            )  ) and
            (p_id_legajo = 0 or TIPJ.Id_Tramite_Ipj in                                --Si se pasa un ID_LEGAJO, busca los datos de esa empresa
               ( select distinct tacc.Id_Tramite_Ipj
                  from ipj.t_tramitesipj_acciones tacc join ipj.t_legajos L
                     on TACC.id_legajo = L.id_legajo
                 where
                   L.id_legajo = p_id_legajo
            )  ) and
            (p_nro_documento is null or TIPJ.Id_Tramite_Ipj in                          -- Si se pasa el DNI de una persona, se la busca
               ( select distinct tacc.Id_Tramite_Ipj
                 from ipj.t_tramitesipj_acciones tacc join ipj.t_integrantes i
                     on tacc.id_integrante = i.id_integrante
                 where
                   i.nro_documento = p_nro_documento
            )  ) and
            (p_nombre is null or TIPJ.Id_Tramite_Ipj in                          -- Si se pasa un nombre, se busca como persona o razon social
               ( select distinct tacc.Id_Tramite_Ipj
                 from ipj.t_tramitesipj_acciones tacc left join ipj.t_legajos L
                     on TACC.id_legajo = L.id_legajo
                   left join ipj.t_integrantes i
                     on tacc.id_integrante = i.id_integrante
                 where
                   upper((select p.nombre||' '||p.apellido from rcivil.vt_pk_persona p where p.id_sexo = i.id_sexo and p.nro_documento = i.nro_documento and p.pai_cod_pais = i.pai_cod_pais and p.id_numero = i.id_numero)) like '%' || upper(p_nombre) || '%' or
                   upper(L.denominacion_Sia) like '%' || upper(p_nombre) || '%'
            )  )

          UNION

          select
            IPJ.varios.FC_Habilitar_Botones (
                ub.id_ubicacion,
                tIPJ.id_clasif_ipj,
                AccIPJ.id_tipo_Accion,
                0,
                -1,
                AccIPJ.id_estado,
                2,
                tIPJ.id_proceso,
                tIPJ.simple_tramite,
                AccIPJ.cuil_usuario,
                AccIPJ.cuil_usuario,
                tIPJ.id_tramite_ipj,
                AccIPJ.id_tramiteipj_accion
            ) botones,
            2 Clase, nvl(VT_SUAC.ID_TRAMITE, 0) Id_Tramite_Suac, nvl(tIPJ.Sticker, VT_SUAC.nro_sticker) Sticker,
            nvl(TIPJ.EXPEDIENTE , VT_SUAC.nro_tramite) Nro_Expediente,
            VT_SUAC."Tipo/Subtipo" Tipo, VT_SUAC.Asunto,
            to_char(VT_SUAC.fecha_inicio, 'dd/mm/rrrr') Fecha_Inicio_Suac,  to_char(VT_SUAC.fecha_ultima_recepcion, 'dd/mm/rrrr') Fecha_recepcion,
            AccIPJ.Id_Tramite_Ipj, 0 id_clasif_ipj, AccIPJ.id_tipo_accion, AccIPJ.Observacion, AccIPJ.id_estado,
            AccIPJ.cuil_usuario, e.n_estado, ta.N_Tipo_Accion Tipo_IPJ,
            tIPJ.FECHA_INICIO Fecha_Inicio_Ipj,
            0 Acc_Abiertas,
            0 Acc_Cerradas, tIPJ.URGENTE,
            u.descripcion usuario, nvl(TRPJ.ERROR_DATO, trInt.Error_Dato) Error_Dato,
            L.CUIT, L.Denominacion_Sia razon_social, trPj.id_legajo,
            UB.N_UBICACION, '' CUIL_CREADOR, ub.id_ubicacion, AccIPJ.id_tramiteipj_accion,
            nvl(ta.id_protocolo, 0) id_protocolo,
            ACCIPJ.ID_INTEGRANTE, I.NRO_DOCUMENTO, L.CUIT Cuit_PersJur, AccIPJ.Obs_Rubrica,
            nvl(to_char(AccIPJ.id_fondo_comercio),nvl(I.NRO_DOCUMENTO, L.CUIT)) identificacion,
            nvl(fc.n_fondo_comercio, nvl(I.DETALLE, L.Denominacion_Sia)) persona,
            AccIPJ.id_fondo_comercio, p.id_pagina, tIPJ.id_proceso,
            Accipj.Id_Documento, Accipj.N_Documento,tIPJ.simple_tramite,
            tIPJ.id_ubicacion_origen
          from ipj.t_tramitesipj_acciones AccIPJ join ipj.t_tramitesIPJ tIPJ
              on AccIPJ.Id_Tramite_Ipj = tIPJ.Id_Tramite_Ipj
            left join NUEVOSUAC.VT_TRAMITES_IPJ_DESA vt_suac
              on VT_SUAC.ID_TRAMITE = tIPJ.id_tramite
            join IPJ.t_estados e
              on AccIPJ.id_estado = e.id_estado
            join IPJ.t_usuarios u
              on AccIPJ.Cuil_Usuario = u.cuil_usuario
            join ipj.t_tipos_AccionesIpj ta
              on ACCIPJ.ID_TIPO_ACCION = ta.id_tipo_accion
            join IPJ.t_paginas p
              on p.id_pagina = ta.id_pagina
            left join IPJ.T_Ubicaciones ub
              on TIPJ.ID_UBICACION = ub.id_ubicacion
            left join IPJ.T_TRAMITESIPJ_PERSJUR trPJ
              on AccIPJ.Id_Tramite_Ipj = trPJ.Id_Tramite_Ipj and ACCIPJ.id_legajo = TRPJ.id_legajo
            left join IPJ.t_legajos L
              on ACCIPJ.id_legajo = L.id_legajo
            left join IPJ.T_TRAMITESIPJ_INTEGRANTE trInt
              on ACCIPJ.ID_INTEGRANTE = TRINT.ID_INTEGRANTE and AccIPJ.Id_Tramite_Ipj = trInt.Id_Tramite_Ipj
            left join IPJ.T_INTEGRANTES i
              on AccIPJ.ID_INTEGRANTE = i.ID_INTEGRANTE
            left join IPJ.T_FONDOS_COMERCIO fc
              on AccIPJ.id_fondo_comercio = fc.id_fondo_comercio
          where
            bitand(p_tipo_operacion, v_tipo_acciones) = v_tipo_acciones and -- Filtra que se consulte acciones
            ( nvl(p_id_estado, 0) = 0 or AccIPJ.Id_Estado = p_id_estado) and -- Busca el estado indicado (no se ofrece en el filtro)
            (nvl(p_ubicacion, 0) = 0 or TIPJ.ID_UBICACION = p_ubicacion) and -- Si se pasa una area de trabajo, busca por esa
            (nvl(p_clasificacion, 0) = 0 or ta.ID_CLASIF_IPJ = p_clasificacion) and -- Si se pasa una clasificacion, filtra por esa
            (p_sticker is null or nvl(tIPJ.Sticker, VT_SUAC.nro_sticker) = p_sticker) and -- Si se pasa un sticker, lo filtra
            (p_expediente is null or nvl(tIPJ.expediente, VT_SUAC.nro_tramite) = p_expediente) and -- Si se pasa un sticker, lo filtra
            (p_cuit is null or L.cuit = p_cuit) and -- Si se pasa un CUIT, busca los datos de esa empresa
            (p_id_legajo = 0 or L.id_legajo = p_id_legajo) and -- Si se pasa un LEGAJO, busca los datos de esa empresa
            (p_nro_documento is null or i.nro_documento = p_nro_documento) and -- si se pasa el DNI de una persona, se la busca
            (p_nombre is null or upper( nvl(I.DETALLE, L.Denominacion_Sia)) like '%' ||upper(p_nombre) || '%') -- si se pasa un nombre, se busca cualquier persona que lo contenga
        ) Tram_Acc
       where
         p_fecha_inicio is null or (Fecha_Inicio_Ipj between to_date(p_fecha_inicio, 'dd/mm/rrrr') and to_date(p_fecha_fin, 'dd/mm/rrrr')+1) -- si hay una fecha desde, filtra el rango indicado
      order by Clase desc, urgente, Sticker;

  END SP_Buscar_Tramites_Hist;


 PROCEDURE SP_Hist_Asig_Tramite (
    o_cursor out IPJ.TYPES.CURSORTYPE ,
    p_Id_Tramite_Ipj in number)
  IS
  BEGIN

    OPEN o_Cursor FOR
      Select TE.CUIL_USUARIO, to_char(TE.FECHA_PASE, 'dd/mm/rrrr') FECHA_PASE, TE.ID_ESTADO, TE.ID_GRUPO,
        TE.ID_PASES_TRAMITES_IPJ, TE.Id_Tramite_Ipj, ltrim(rtrim(TE.OBSERVACION)) OBSERVACION,
        E.N_ESTADO, nvl(U.DESCRIPCION, G.N_GRUPO) DESCRIPCION
      from IPJ.t_tramites_ipj_estado te join IPJ.t_estados e
          on te.id_estado = e.id_estado
        left join IPJ.t_usuarios u
          on te.cuil_usuario = u.cuil_usuario
        left join IPJ.t_grupos g
          on te.id_grupo = g.id_grupo
      where
        Id_Tramite_Ipj = p_Id_Tramite_Ipj
      order by id_pases_tramites_ipj desc;

  END SP_Hist_Asig_Tramite;


  PROCEDURE SP_Hist_Asig_Accion (
    o_cursor out IPJ.TYPES.CURSORTYPE ,
    p_Id_Tramite_Ipj in number,
    p_id_tramiteipj_accion  in number)
  IS
  BEGIN

    OPEN o_Cursor FOR
     select ACCE.CUIL_USUARIO, to_char(ACCE.FECHA_PASE, 'dd/mm/rrrr') FECHA_PASE,
       ACCE.ID_ESTADO, ACCE.ID_PASES_ACCIONES_IPJ, ACCE.ID_TIPO_ACCION,
       ACCE.Id_Tramite_Ipj, ACCE.ID_TRAMITEIPJ_ACCION, ltrim(rtrim(ACCE.OBSERVACION)) OBSERVACION,
       U.DESCRIPCION, E.N_ESTADO
     from IPJ.t_tramitesipj_acciones_estado acce join IPJ.t_usuarios u
         on acce.cuil_usuario = u.cuil_usuario
       join IPJ.t_estados e
         on acce.id_estado = e.id_estado
     where
       Id_Tramite_Ipj = p_Id_Tramite_Ipj and
       id_tramiteipj_accion = p_id_tramiteipj_accion
     order by id_pases_acciones_ipj desc;

  END SP_Hist_Asig_Accion;

  PROCEDURE SP_Guardar_Accion_Doc(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_id_tramiteipj_accion in number,
    p_id_documento in number,
    p_n_documento in varchar2,
    p_asociado_cuit in char,
    p_id_tipo_documento_cdd in number)
  IS
    v_valid_parametros varchar2(2000);
    v_Row_TramiteIPJ_Accion  IPJ.T_TramitesIPJ_Acciones%ROWTYPE;
  /**************************************************
    Guarda el informe borrador de agun tramite y accion de informe
  ***************************************************/
  BEGIN
    IPJ.VARIOS.SP_GUARDAR_LOG(
              p_METODO => 'SP_Guardar_Accion_Doc',
              p_NIVEL => 'Guardar accion Gestion',
              p_ORIGEN => 'DB',
              p_USR_LOG  => 'DB',
              p_USR_WINDOWS => 'Sistema',
              p_IP_PC  => null,
              p_EXCEPCION =>
                ' Tramite = ' || to_char(p_Id_Tramite_Ipj)
                || ' accion = ' || to_char(p_id_tramiteipj_accion)
                || ' id doc = ' || to_char(p_id_documento)
                || ' n doc = ' || to_char(p_n_documento)
                || ' asoc cuit = ' || to_char(p_asociado_cuit)
                || ' doc cdd = ' || to_char(p_id_tipo_documento_cdd)
            );

    -- valido que venga un ID_DOCUMENTO y su nombre
    if nvl(p_id_documento, 0) = 0 then --Debe existir un tramite
      v_valid_parametros := v_valid_parametros || 'Documento inv�lido';
    end if;
    if p_n_documento is null then --Debe existir un tramite
      v_valid_parametros := v_valid_parametros || 'Nombre Documento inv�lido';
    end if;

    -- Si los paramentros no estan OK, no continuo
    if v_valid_parametros is not null then
      o_rdo := 'Parametros : ' || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    -- Actualizo el Informe si se pasa ID y Nombre
    if nvl(p_id_documento, 0) > 0 and p_n_documento is not null then
      update IPJ.T_TRAMITESIPJ_ACCIONES
      set
        id_documento = (case when nvl(p_id_documento, 0) = 0 then id_documento else p_id_documento end),
        n_documento = (case when nvl(p_id_documento, 0) = 0 then n_documento else p_n_documento end),
        asociado_cuit = p_asociado_cuit,
        id_tipo_documento_cdd = decode(p_id_tipo_documento_cdd, 0, null, p_id_tipo_documento_cdd)
      where
        Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        id_tramiteipj_accion = p_id_tramiteipj_accion;
    end if;

    select * into v_Row_TramiteIPJ_Accion
    from IPJ.T_TRAMITESIPJ_ACCIONES
    where
        Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        id_tramiteipj_accion = p_id_tramiteipj_accion;

    -- Guardo los cambios de estados
    IPJ.TRAMITES.SP_GUARDAR_TRAMITES_ACC_ESTADO(
      o_rdo => o_rdo,
      o_tipo_mensaje => o_tipo_mensaje,
      p_fecha_pase => sysdate,
      p_id_estado => v_Row_TramiteIPJ_Accion.id_estado,
      p_cuil_usuario => v_Row_TramiteIPJ_Accion.Cuil_Usuario,
      p_observacion => v_Row_TramiteIPJ_Accion.observacion,
      p_Id_Tramite_Ipj => v_Row_TramiteIPJ_Accion.Id_Tramite_Ipj,
      p_id_tramite_Accion => v_Row_TramiteIPJ_Accion.id_tramiteIpj_Accion,
      p_id_tipo_accion => v_Row_TramiteIPJ_Accion.id_tipo_accion,
      p_id_documento => v_Row_TramiteIPJ_Accion.id_documento,
      p_n_documento => v_Row_TramiteIPJ_Accion.n_documento );


     o_rdo := IPJ.TYPES.C_RESP_OK;
     o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     when OTHERS then
        o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Guardar_Accion_Doc;


  PROCEDURE SP_Ocupacion_Area(
    o_Cursor OUT types.cursorType,
    p_id_ubicacion in number
    )
  IS
  /****************************************************
   Lista la cantidad de acciones abiertas u observadas por usuarios del �rea
  *****************************************************/
  BEGIN

    OPEN o_Cursor FOR
      select
        u.descripcion,
        sum(case when acc.id_estado = IPJ.TYPES.c_Estados_Observado then 1 else 0 end) Acc_Observadas,
        sum(case when acc.id_estado = IPJ.TYPES.c_Estados_Observado then 0 else 1 end) Acc_Abiertas
      from ipj.t_tramitesipj_acciones acc join ipj.t_tipos_accionesipj tacc
          on acc.id_tipo_accion = tacc.id_tipo_accion
        join ipj.t_tipos_clasif_ipj clas
          on CLAS.ID_CLASIF_IPJ = tacc.id_clasif_ipj
        join ipj.t_usuarios u
          on u.cuil_usuario = acc.cuil_usuario
      where
        clas.id_ubicacion = p_id_ubicacion and
        acc.id_estado < 100
      group by u.cuil_usuario, u.descripcion;

  END SP_Ocupacion_Area;

  PROCEDURE SP_Hist_Empresa(
    o_Cursor OUT types.cursorType,
    p_id_legajo in number
    )
  IS
  /****************************************************
   Lista quien resolvio los ultimos 5 tramites de la empresa
  *****************************************************/
  BEGIN

    OPEN o_Cursor FOR
      select
        0 Id_Tramite_Ipj, expediente nro_expediente, sticker,
        (SELECT u.descripcion FROM ipj.t_tramitesipj_acciones ta JOIN ipj.t_usuarios u ON ta.cuil_usuario = u.cuil_usuario WHERE ta.id_tramiteipj_accion = t.id_tramiteipj_accion) descripcion,
        e.n_estado
      from
        ( select
            distinct tr.expediente, tr.sticker, Tr.Id_Estado_Ult, tr.id_tramite, MIN(tacc.id_tramiteipj_accion) id_tramiteipj_accion
          from ipj.t_tramitesipj tr join  ipj.t_tramitesipj_persjur tper
              on tr.Id_Tramite_Ipj = tper.Id_Tramite_Ipj
            JOIN ipj.t_tramitesipj_acciones tacc ON tr.id_tramite_ipj = tacc.id_tramite_ipj AND tper.id_legajo = tacc.id_legajo
            JOIN ipj.t_tipos_accionesipj taip ON tacc.id_tipo_accion = taip.id_tipo_accion
            JOIN ipj.t_tipos_clasif_ipj tcip ON taip.id_clasif_ipj = tcip.id_clasif_ipj AND tcip.id_ubicacion = tr.id_ubicacion_origen
          where
            TPER.ID_LEGAJO = p_id_legajo
          group by tr.expediente, tr.sticker, Tr.Id_Estado_Ult, tr.id_tramite
          order by tr.expediente desc
        ) t join IPJ.T_ESTADOS e
          on t.id_estado_ult = e.id_estado
      where
        rownum < 6
        AND NVL(t.id_tramite,0) <> 0;

      /*select
        0 Id_Tramite_Ipj, expediente nro_expediente, sticker, descripcion, e.n_estado
      from
        ( select
            distinct tr.expediente, tr.sticker, u.descripcion, Tr.Id_Estado_Ult
          from ipj.t_tramitesipj tr join  ipj.t_tramitesipj_persjur acc
              on tr.Id_Tramite_Ipj = acc.Id_Tramite_Ipj
            join ipj.t_usuarios u
              on u.cuil_usuario = tr.Cuil_Ult_Estado
          where
            ACC.ID_LEGAJO = p_id_legajo
          group by u.cuil_usuario, u.descripcion, tr.expediente, tr.sticker, Tr.Id_Estado_Ult
          order by tr.expediente desc
        ) t join IPJ.T_ESTADOS e
          on t.id_estado_ult = e.id_estado
      where
        rownum <6;*/

  END SP_Hist_Empresa;

  PROCEDURE SP_Movimientos_Area(
    o_Cursor OUT types.cursorType,
    p_fecha_Desde in date,
    p_fecha_hasta in date,
    p_id_ubicacion in number,
    p_id_legajo in number
    )
  IS
  /****************************************************
   Lista los movimientos de los tr�mites para el area indicada:
   - Rango de Fecha
   - Legajo
  *****************************************************/
  BEGIN
    if IPJ.Types.c_habilitar_log_SP = 'S' then
      IPJ.VARIOS.SP_GUARDAR_LOG(
        p_METODO => 'SP_Movimientos_Area',
        p_NIVEL => '',
        p_ORIGEN => 'DB',
        p_USR_LOG  => 'DB',
        p_USR_WINDOWS => 'Sistema',
        p_IP_PC  => null,
        p_EXCEPCION =>
          'Fecha desde = ' || to_char(p_fecha_Desde, 'dd/mm/rrrr')
          || ' / Fecha hasta = ' || to_char(p_fecha_hasta, 'dd/mm/rrrr')
          || ' / Ubicacion = ' || to_char(p_id_ubicacion)
          || ' / Id Legajo = ' || to_char(p_id_legajo)
       );
    end if;

    OPEN o_Cursor FOR
      select to_char(tr.Fecha_Ini_Suac, 'dd/mm/rrrr') fecha_inicio,
        nvl(
          ( select u.descripcion
            from ipj.t_tramitesipj_acciones ac join ipj.t_usuarios u
              on ac.cuil_usuario = u.cuil_usuario
            where
              ac.id_tramite_ipj = tr.id_tramite_ipj and
              ac.id_tipo_accion not in
                ( select id_tipo_accion
                  from ipj.t_tipos_accionesipj ac join ipj.t_tipos_clasif_ipj c
                    on ac.id_clasif_ipj = c.id_clasif_ipj
                  where
                    id_ubicacion in (IPJ.TYPES.C_AREA_ARCHIVO, IPJ.TYPES.C_AREA_MESASUAC)
                ) and
              ac.id_tipo_accion in
                ( select id_tipo_accion
                  from ipj.t_tipos_accionesipj ac join ipj.t_tipos_clasif_ipj c
                    on ac.id_clasif_ipj = c.id_clasif_ipj
                  where
                    id_ubicacion = p_id_ubicacion
                ) and
              rownum = 1
          ),
          ( select u.descripcion
            from ipj.t_usuarios u
            where
              u.cuil_usuario = Tr.Cuil_Ult_Estado
           )
        ) Descripcion,
        tr.expediente nro_expediente,
        nvl((select denominacion_sia from ipj.t_legajos l where l.id_legajo = pr.id_legajo), ipj.tramites_suac.FC_Buscar_Iniciador_SUAC(tr.id_tramite)) razon_social,
        IPJ.TRAMITES_SUAC.FC_Buscar_Asunto_SUAC(TR.ID_TRAMITE) Asunto,
        (select e.n_estado from ipj.t_estados e where e.id_estado = tr.id_estado_ult) n_estado,
        (select n_ubicacion from ipj.t_ubicaciones ub where ub.id_ubicacion = tr.id_ubicacion) n_ubicacion,
        Tr.Id_Ubicacion, tr.id_estado_ult id_estado, tr.id_tramite_ipj id_tramite_ipj,
        tr.sticker sticker, tr.id_tramite id_tramite_suac, tr.fecha_ini_suac fecha_inicio_suac,
        tr.codigo_online, tr.id_ubicacion_origen,
        (select VT.NOTA_EXPEDIENTE from NUEVOSUAC.VT_TRAMITES_IPJ_DESA vt where vt.id_tramite = tr.id_tramite) Nota_Suac
      from ipj.t_tramitesipj tr left join ipj.t_tramitesipj_persjur pr
          on pr.id_tramite_ipj = tr.id_tramite_ipj
      where
        (p_fecha_desde is null or tr.Fecha_Ini_Suac between to_date(p_fecha_desde, 'dd/mm/rrrr') and to_date(p_fecha_hasta, 'dd/mm/rrrr')) and
        tr.id_ubicacion_origen = p_id_ubicacion and
        (nvl(p_id_legajo, 0) = 0 or pr.id_legajo = p_id_legajo)
      order by tr.Fecha_Ini_Suac desc;

  END SP_Movimientos_Area;

  PROCEDURE SP_Guardar_Accion_Actuacion (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_id_tramiteIpj_Accion in number,
    p_nro_actuacion in varchar2,
    p_nro_dictamen in number )
  IS
  BEGIN
    update IPJ.t_TramitesIpj_Acciones
    set
      nro_actuacion = p_nro_actuacion,
      nro_dictamen = p_nro_dictamen
    where
      Id_Tramite_Ipj = p_Id_Tramite_Ipj and
      id_tramiteipj_accion = p_id_tramiteipj_accion;

    -- Actualizo el protocolo que corresponda de Jur�dico
    if nvl(p_nro_dictamen, 0) > 0 then
      update ipj.t_protocolos_juridico
      set numerador = (case when numerador >= p_nro_dictamen then numerador else p_nro_dictamen end)
      where
        id_prot_jur = 'D';
    end if;

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     when OTHERS then
        o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Guardar_Accion_Actuacion;

  PROCEDURE SP_Traer_Reserva (
    o_Cursor OUT types.cursorType,
    p_Id_Tramite_Ipj in number,
    p_id_tramiteipj_accion in number,
    p_id_tipo_accion in number,
    p_nro_reserva in number
    )
  IS
  BEGIN

    OPEN o_Cursor FOR
      select
        tr.Id_Tramite_Ipj, tr.id_tramiteipj_accion, tr.id_tipo_accion, tr.nro_reserva, tr.n_reserva,
        tr.id_estado_reserva, er.n_estado_reserva,
        to_char(tr.fec_vence, 'dd/mm/rrrr') fec_vence,
        (select obs_rubrica from ipj.t_tramitesipj_acciones where Id_Tramite_Ipj =  p_Id_Tramite_Ipj and id_tramiteipj_accion = p_id_tramiteipj_accion) obs_rubrica
      from IPJ.T_TRAMITESIPJ_ACC_RESERVA tr join IPJ.T_TIPOS_ESTADO_RESERVA er
          on tr.id_estado_reserva = er.id_estado_reserva
      where
        tr.Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        --tr.id_tramiteipj_accion = p_id_tramiteipj_accion and
        tr.id_tipo_accion = p_id_tipo_accion and
        (nvl(p_nro_reserva, 0) = 0 or tr.nro_reserva = p_nro_reserva)
      order by tr.nro_reserva asc;

  END SP_Traer_Reserva;

  PROCEDURE SP_Traer_Datos_Reserva (
    o_Cursor OUT types.cursorType,
    p_Id_Tramite_Ipj in number,
    p_id_tramiteipj_accion in number
    )
  IS
    v_fec_vence varchar2(10);
  BEGIN

    begin
      select max(to_char(tr.fec_vence, 'dd/mm/rrrr')) into v_fec_vence
      from IPJ.t_tramitesipj_acc_reserva tr
      where
        tr.Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        tr.id_tramiteipj_accion = p_id_tramiteipj_accion and
        tr.id_estado_reserva = 1;
    exception
      when NO_DATA_FOUND THEN
         v_fec_vence := null;
    end;

    OPEN o_Cursor FOR
      select
        tr.Id_Tramite_Ipj, tacc.id_tramiteipj_accion, tr.sticker, TR.EXPEDIENTE Nro_Expediente,
        tacc.id_legajo, l.denominacion_sia, l.cuit,
        i.nro_documento,
        (select p.nombre||' '||p.apellido from rcivil.vt_pk_persona p where p.id_sexo = i.id_sexo and p.nro_documento = i.nro_documento and p.pai_cod_pais = i.pai_cod_pais and p.id_numero = i.id_numero) detalle,
        v_fec_vence fec_vence
      from IPJ.T_TRAMITESIPJ_ACCIONES tacc join IPJ.T_TRAMITESIPJ tr
          on tacc.Id_Tramite_Ipj = tr.Id_Tramite_Ipj
        left join ipj.t_legajos l
          on tacc.id_legajo = l.id_legajo
        left join ipj.t_integrantes i
          on tacc.id_integrante = i.id_integrante
      where
        tacc.Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        tacc.id_tramiteipj_accion = p_id_tramiteipj_accion;

  END SP_Traer_Datos_Reserva;


  PROCEDURE SP_GUARDAR_RESERVA (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_id_tramiteIpj_Accion in number,
    p_id_tipo_accion in number,
    p_nro_reserva in number,
    p_n_reserva in varchar2,
    p_id_estado_reserva in number,
    p_fec_vence in varchar2
    --, p_observacion in varchar2
    )
  IS
    v_valid_parametros varchar2(2000);
    v_fec_vence date;
  /**************************************************
    Actualiza una reserva o inserta los valores nuevos
  ***************************************************/
  BEGIN
    -- valido que venga un ID_DOCUMENTO y su nombre
    if nvl(p_Id_Tramite_Ipj, 0) = 0 then --Debe existir un tramite
      v_valid_parametros := v_valid_parametros || 'Tr�mite Inv�lido';
    end if;
    if nvl(p_id_tramiteIpj_Accion, 0) = 0 then --Debe existir una accion
      v_valid_parametros := v_valid_parametros || 'Acci�n Inv�lida';
    end if;
    if p_n_reserva is null then --Debe existir nombre a reservar
      v_valid_parametros := v_valid_parametros || 'Nombre de reserva inv�lido';
    end if;

    -- Si los paramentros no estan OK, no continuo
    if v_valid_parametros is not null then
      o_rdo := 'Parametros : ' || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    -- CUERPO DEL SP
    if  nvl(p_id_estado_reserva, 0) = IPJ.TYPES.C_RESERVA_OK and p_fec_vence is null then
      v_fec_vence := to_date(sysdate + 30, 'dd/mm/rrrr');
    else
      v_fec_vence := to_date(p_fec_vence, 'dd/mm/rrrr');
    end if;

    update IPJ.T_TRAMITESIPJ_ACC_RESERVA
    set
      id_estado_reserva = nvl(p_id_estado_reserva, 0),
      n_reserva = p_n_reserva,
      fec_vence = v_fec_vence
    where
      Id_Tramite_Ipj = p_Id_Tramite_Ipj and
      id_tramiteipj_accion = p_id_tramiteipj_accion and
      id_tipo_accion = p_id_tipo_accion and
      nro_reserva = p_nro_reserva;

    /* si no se actualiza nada, agrego el tramite */
    if  sql%rowcount = 0 then
      insert into IPJ.T_TRAMITESIPJ_ACC_RESERVA (Id_Tramite_Ipj, id_tramiteIpj_Accion, id_tipo_accion, nro_reserva, n_reserva, id_estado_reserva, fec_vence)
      values (p_Id_Tramite_Ipj, p_id_tramiteIpj_Accion, p_id_tipo_accion, p_nro_reserva, p_n_reserva, nvl(p_id_estado_reserva, 0), v_fec_vence);
    end if;

    -- Actualizo la observacion en la accion.
    /*
    if p_observacion is not null then
      update ipj.t_tramitesipj_acciones
      set obs_rubrica = p_observacion
      where
        Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        id_tramiteipj_accion = p_id_tramiteipj_accion;
    end if;
    */

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     when OTHERS then
        o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_GUARDAR_RESERVA;


  PROCEDURE SP_Informar_Reserva (
    o_Cursor OUT types.cursorType,
    p_Id_Tramite_Ipj in number,
    p_id_tramiteipj_accion in number,
    p_id_tipo_accion in number
    )
  IS
    v_existe_ok number(5);
    v_existe_gru number(5);
    v_existe_no number(5);
    v_estado_reserva number(5);
    v_lista_nombres varchar2(1000);
    v_Cursor_Reserva  IPJ.types.cursorType;
    v_Row_Reserva  IPJ.T_TRAMITESIPJ_ACC_RESERVA%ROWTYPE;
    v_fecha_vence date;
  BEGIN
    -- obteng los resultados del control
    select
      sum( (case when tr.id_estado_reserva = 1 then 1 else 0 end)) Ok,
      sum( (case when tr.id_estado_reserva = 4 then 1 else 0 end)) GR,
      sum( (case when tr.id_estado_reserva = 2 then 1 else 0 end)) RECH
      into v_existe_ok, v_existe_gru, v_existe_no
    from IPJ.T_TRAMITESIPJ_ACC_RESERVA tr
    where
        tr.Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        tr.id_tramiteipj_accion = p_id_tramiteipj_accion and
        tr.id_tipo_accion = p_id_tipo_accion;

    -- Si hay OK, informo esos, sino los de Grupos, y en ultimo los rechazados
    if v_existe_ok > 0 then
      v_estado_reserva := 1;
    else
      if v_existe_gru > 0 then
        v_estado_reserva := 4;
      else
        v_estado_reserva := 2;
      end if;
    end if;

    -- Armo la lista de nombres del estado elegido
    v_lista_nombres := null;
    OPEN v_Cursor_Reserva FOR
      select *
      from IPJ.T_TRAMITESIPJ_ACC_RESERVA tr
      where
        tr.Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        tr.id_tramiteipj_accion = p_id_tramiteipj_accion and
        tr.id_tipo_accion = p_id_tipo_accion and
        TR.ID_ESTADO_RESERVA = v_estado_reserva
      order by tr.nro_reserva;

    LOOP
        fetch v_Cursor_Reserva into v_Row_Reserva;
        EXIT WHEN v_Cursor_Reserva%NOTFOUND or v_Cursor_Reserva%NOTFOUND is null;

        v_lista_nombres := v_lista_nombres || '; ' || v_Row_Reserva.n_reserva;
        v_fecha_vence := v_Row_Reserva.fec_vence;
    end loop;

    CLOSE v_Cursor_Reserva;

    OPEN o_Cursor FOR
      select
        tacc.id_tramiteipj_accion, (case when tacc.id_legajo = 0 then i.nro_documento else l.cuit end) Identificacion,
        (case when tacc.id_legajo = 0 then
                (select p.nombre||' '||p.apellido from rcivil.vt_pk_persona p where p.id_sexo = i.id_sexo and p.nro_documento = i.nro_documento and p.pai_cod_pais = i.pai_cod_pais and p.id_numero = i.id_numero)
              else
                l.denominacion_sia
         end) Nombre,
        substr(v_lista_nombres, 3, 1000) lista_nombre, v_estado_reserva ID_ESTADO_RESERVA,
        to_char(v_fecha_vence, 'dd/mm/rrrr') Fec_Vence
      from IPJ.T_TRAMITESIPJ_ACCIONES tacc left join ipj.t_legajos l
          on tacc.id_legajo = l.id_legajo
        left join ipj.t_integrantes i
          on tacc.id_integrante = i.id_integrante
      where
        tacc.Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        tacc.id_tramiteipj_accion = p_id_tramiteipj_accion;


  END SP_Informar_Reserva;

  PROCEDURE SP_Cargar_Tasas_Accion (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_id_tramiteipj_accion in number,
    p_id_tipo_accion in number)
  IS
  /**********************************************************
    Al crear una nueva accion, se copian las tasas asociadas y sus montos.
  **********************************************************/
  BEGIN
    -- agrego todas las tasas asociadas al tr�mite
    insert into IPJ.T_TRAMITESIPJ_ACC_TASA (Id_Tramite_Ipj, id_tramiteipj_accion, id_tipo_Accion, id_tasa, importe)
    select p_Id_Tramite_Ipj, p_id_tramiteipj_accion, p_id_tipo_Accion, id_tasa, 0
    from IPJ.T_ACCIONES_TASAS
    where
      id_tipo_accion = p_id_tipo_accion;

    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    o_rdo := IPJ.TYPES.C_RESP_OK;

  EXCEPTION
    WHEN OTHERS THEN
       o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Cargar_Tasas_Accion;


  PROCEDURE SP_Traer_Documento_Accion (
    o_Cursor OUT types.cursorType,
    p_Id_Tramite_Ipj in number,
    p_id_tramiteipj_accion in number
    )
  IS
  -- Muestra los datos del documento de una accion
  -- El tramite se muestra con 2 nombres, por problemas en Gestion
  BEGIN

    OPEN o_Cursor FOR
      select Id_Tramite_Ipj id_tramiteipj, Id_Tramite_Ipj,
        id_tramiteipj_accion, id_documento, n_documento, tr.id_tipo_documento_cdd id_tipo_documento
      from IPJ.T_TRAMITESIPJ_ACCIONES tr
      where
        tr.Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        tr.id_tramiteipj_accion = p_id_tramiteipj_accion;

  END SP_Traer_Documento_Accion;

  PROCEDURE SP_Pasar_Area (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_Cursor OUT types.cursorType,
    p_Id_Tramite_Ipj in number,
    p_id_ubicacion_dest in number)
  IS
    v_valid_parametros varchar2(2000);
    v_Id_Tramite_Ipj number;
    v_id_Clasif number;
    v_Row_Ubicacion  IPJ.T_Ubicaciones%ROWTYPE;
    v_Row_Tramite  IPJ.T_TramitesIPJ%ROWTYPE;
    v_cuil_usuario varchar2(20);
    v_id_grupo number;
  /**************************************************
    Arma el conjunto de datos para transferir un tr�mite a otra �rea
  ***************************************************/
  BEGIN
    if nvl(p_Id_Tramite_Ipj, 0) = 0 then --Debe existir un tramite
      v_valid_parametros := v_valid_parametros || 'Tr�mite Inv�lido. ';
    end if;
    if p_id_ubicacion_dest not in (IPJ.Types.c_area_juridico, IPJ.Types.c_area_archivo, IPJ.Types.c_area_direccion, IPJ.Types.c_area_mesaSuac,  IPJ.Types.c_area_SRL, IPJ.Types.c_area_SxA, IPJ.Types.c_area_CyF) then
      v_valid_parametros := v_valid_parametros || 'Area inv�lida. ';
    end if;

    -- Si los paramentros no estan OK, no continuo
    if v_valid_parametros is not null then
      o_rdo := 'Parametros : ' || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    -- **************  CUERPO DEL PROCEDIMIENTO ************
    -- Busco los datos del Area
    select * into v_Row_Ubicacion
    from IPJ.t_Ubicaciones
    where
      id_ubicacion = p_id_ubicacion_dest;

    -- Busco los datos del tr�mite
    select * into v_Row_Tramite
    from IPJ.t_tramitesipj
    where
      Id_Tramite_Ipj = p_Id_Tramite_Ipj;

    /*****************************************************
         DEPENDIENDO DEL AREA, BUSCO SUS DATOS
    *****************************************************/
    -- busco la primer clasificacion del area deseada en el workflow habilitado
    select Id_Clasif_Ipj_Proxima into v_id_clasif
    from ipj.t_workflow w join ipj.t_tipos_clasif_ipj c
      on w.Id_Clasif_Ipj_Proxima = c.Id_Clasif_Ipj
    where
      id_ubicacion_origen = v_Row_Tramite.id_ubicacion_origen and
      id_clasif_ipj_actual = v_Row_Tramite.id_clasif_ipj and
      c.id_ubicacion = p_id_ubicacion_dest and
      rownum = 1;

    -- Si se vuelve desde Jur�dico, busco el usuario de la primer acci�n
    if v_Row_Tramite.id_ubicacion = IPJ.TYPES.C_AREA_JURIDICO then
      begin
        -- Busco el ultimo usuario de las acciones
        select cuil_usuario into v_cuil_usuario
        from IPJ.T_TRAMITESIPJ_ACCIONES acc
        where
          id_tramite_ipj = v_Row_Tramite.id_tramite_ipj and
          id_tipo_accion in
            ( select id_tipo_accion
              from ipj.t_tipos_accionesipj ta join ipj.t_tipos_clasif_ipj tc
                on ta.id_clasif_ipj = tc.id_clasif_ipj
              where
                tc.id_ubicacion = p_id_ubicacion_dest
            ) and
          rownum = 1;
      exception
        when NO_DATA_FOUND then
          -- Si no hay acciones, busca el ultimo usuario del tr�mite
          select cuil_usuario into v_cuil_usuario
          from
            ( select cuil_usuario
              from IPJ.T_TRAMITES_IPJ_ESTADO te
              where
                te.id_tramite_ipj =  v_Row_Tramite.id_tramite_ipj
                and exists (select * from IPJ.T_GRUPOS_TRAB_UBICACION g where g.cuil = te.cuil_usuario and g.id_ubicacion = p_id_ubicacion_dest)
              order by fecha_pase desc)
          where
            rownum = 1;
      end;
    end if;

    -- Si el tr�mite se pasa de SxA a RP, se pasa al nuevo grupo
    if v_Row_Tramite.id_ubicacion_origen = IPJ.TYPES.C_AREA_SXA and  p_id_ubicacion_dest = IPJ.Types.c_area_SRL then
      v_cuil_usuario := null;
      v_id_grupo := to_number(IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('GRUPO_RP_SA'));
    else
      v_cuil_usuario := nvl(v_cuil_usuario, v_Row_Ubicacion.Cuil_Usuario);
      v_id_grupo := v_Row_Ubicacion.Id_Grupo;
    end if;

    -- paso los valores necesarios
    OPEN o_Cursor for
      select
        v_Row_Tramite.Id_Tramite_Ipj Id_Tramite_Ipj,
        v_Row_Ubicacion.id_ubicacion id_ubicacion,
        v_id_clasif id_clasif_ipj,
        v_cuil_usuario Cuil_Usuario,
        (case when v_cuil_usuario is not null then null else v_Id_Grupo end) Id_Grupo,
        (case
            when p_id_ubicacion_dest in (IPJ.Types.c_area_archivo, IPJ.Types.c_area_mesaSuac) then Ipj.Types.c_Estados_Completado
            else Ipj.Types.C_Estados_Proceso
         end ) Id_Estado
      from dual;

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     when OTHERS then
        o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Pasar_Area;

  PROCEDURE SP_Pasar_Archivo (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_Cursor OUT types.cursorType,
    p_Id_Tramite_Ipj in number,
    p_Observacion in varchar2)
  IS
    v_valid_parametros varchar2(2000);
    v_Id_Tramite_Ipj number;
    v_id_Clasif number;
    v_Row_Ubicacion  IPJ.T_Ubicaciones%ROWTYPE;
    v_Row_Tramite  IPJ.T_TramitesIPJ%ROWTYPE;
  /**************************************************
    Pasa un tramite al responsable del area Archivo, en estado Completado
  ***************************************************/
  BEGIN
    if nvl(p_Id_Tramite_Ipj, 0) = 0 then --Debe existir un tramite
      v_valid_parametros := v_valid_parametros || 'Tr�mite Inv�lido';
    end if;

    -- Si los paramentros no estan OK, no continuo
    if v_valid_parametros is not null then
      o_rdo := 'Parametros : ' || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    -- **************  CUERPO DEL PROCEDIMIENTO ************
    -- Busco el area de Archivo
    select * into v_Row_Ubicacion
    from IPJ.t_Ubicaciones
    where
      id_ubicacion = IPJ.TYPES.C_AREA_ARCHIVO;

    -- Busco el tr�mite
    select * into v_Row_Tramite
    from IPJ.t_tramitesipj
    where
      Id_Tramite_Ipj = p_Id_Tramite_Ipj;

    -- busco la primer clasificacion del area
    select id_clasif_ipj into v_id_clasif
    from IPJ.t_Tipos_Clasif_Ipj
    where
      id_ubicacion = IPJ.TYPES.C_AREA_ARCHIVO and
      rownum = 1;

    -- paso los valores necesarios
    OPEN o_Cursor for
      select
        v_Row_Tramite.Id_Tramite_Ipj Id_Tramite_Ipj,
        v_Row_Ubicacion.id_ubicacion id_ubicacion,
        v_id_clasif id_clasif_ipj,
        v_Row_Ubicacion.Cuil_Usuario Cuil_Usuario,
        v_Row_Ubicacion.Id_Grupo Id_Grupo,
        Ipj.Types.c_Estados_Completado Id_Estado
      from dual;

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     when OTHERS then
        o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Pasar_Archivo;

  PROCEDURE SP_Pasar_Mesa (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_Cursor OUT types.cursorType,
    p_Id_Tramite_Ipj in number,
    p_Observacion in varchar2)
  IS
    v_valid_parametros varchar2(2000);
    v_Id_Tramite_Ipj number;
    v_id_Clasif number;
    v_n_clasif varchar2(100);
    v_Row_Ubicacion  IPJ.T_Ubicaciones%ROWTYPE;
    v_Row_Tramite  IPJ.T_TramitesIPJ%ROWTYPE;
  /**************************************************
    Pasa un tramite al responsable del area Mesa SUAC, en estado Completado
  ***************************************************/
  BEGIN
    -- valido que venga un tramite existente
    if nvl(p_Id_Tramite_Ipj, 0) = 0 then --Debe existir un tramite
      v_valid_parametros := v_valid_parametros || 'Tr�mite Inv�lido';
    end if;

    -- Si los paramentros no estan OK, no continuo
    if v_valid_parametros is not null then
      o_rdo := 'Parametros : ' || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    -- **************  CUERPO DEL PROCEDIMIENTO ************
    -- Busco el area de Mesa SUAC
    select * into v_Row_Ubicacion
    from IPJ.t_Ubicaciones
    where
      id_ubicacion = IPJ.TYPES.C_AREA_MESASUAC;

    -- Busco el tr�mite
    select * into v_Row_Tramite
    from IPJ.t_tramitesipj
    where
      Id_Tramite_Ipj = p_Id_Tramite_Ipj;

    -- busco la primer clasificacion del area
    select id_clasif_ipj, n_clasif_ipj into v_id_clasif, v_n_clasif
    from IPJ.t_Tipos_Clasif_Ipj
    where
      id_ubicacion = IPJ.TYPES.C_AREA_MESASUAC and
      rownum = 1;

    -- paso los valores necesarios
    OPEN o_Cursor for
      select
        v_Row_Tramite.Id_Tramite_Ipj Id_Tramite_Ipj,
        v_Row_Ubicacion.id_ubicacion id_ubicacion,
        v_id_clasif id_clasif_ipj,
        v_n_clasif n_clasif_ipj,
        v_Row_Ubicacion.Cuil_Usuario Cuil_Usuario,
        v_Row_Ubicacion.Id_Grupo Id_Grupo,
        Ipj.Types.c_Estados_Completado Id_Estado
      from dual;

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     when OTHERS then
        o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Pasar_Mesa;

  PROCEDURE SP_Traer_Tramite_Retiro (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_Cursor OUT types.cursorType,
    p_Id_Tramite_Ipj in number)
  IS
  BEGIN
    Open o_Cursor for
      select tipj.Id_Tramite_Ipj, TIPJ.STICKER, tipj.expediente,
        to_char(TRET.FECHA, 'dd/mm/rrrr HH:MI:SS AM') Fecha, tret.id_integrante,
        I.ID_NUMERO, I.ID_SEXO, I.PAI_COD_PAIS, I.NRO_DOCUMENTO, I.ERROR_DATO,
        I.DETALLE
      from IPJ.T_TramitesIPJ tipj left join IPJ.T_TRAMITESIPJ_RETIRO tret
          on tipj.Id_Tramite_Ipj = tret.Id_Tramite_Ipj
       left join IPJ.t_Integrantes i
         on tret.id_integrante = i.id_integrante
      where
        tipj.Id_Tramite_Ipj = p_Id_Tramite_Ipj ;

  END SP_Traer_Tramite_Retiro;

  PROCEDURE SP_Guardar_Tramite_Retiro (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_id_integrante in number,
    p_error_dato in varchar2,
    p_id_sexo in varchar2,
    p_nro_documento in varchar2,
    p_pai_cod_pais in varchar2,
    p_id_numero in number,
    p_cuil in varchar2,
    p_detalle in varchar2,
    p_nombre in varchar2,
    p_apellido in varchar2,
    p_n_tipo_documento in varchar2)
  IS
   v_existe number;
   v_id_integrante number;
   v_PermiteEliminar number;
   v_tipo_mensaje number;
  BEGIN
  /*************************************************
     Guarda un nuevo registro en T_TramitesIPJ_RETIRO, no permite modificar
     La transaccion la maneja la aplicacion para guardar cabecera y detalles.
  ************************************************/
    select count(*) into v_existe
    from IPJ.T_TRAMITESIPJ_RETIRO
    where
      Id_Tramite_Ipj = p_Id_Tramite_Ipj;

    if v_existe = 0 then
      -- Si el integrante no existe, lo agrego y luego continuo
      if IPJ.VARIOS.Valida_Integrante(p_id_integrante) = 0 then
        IPJ.ENTIDAD_PERSJUR.SP_GUARDAR_INTEGRANTES(
          o_rdo => o_rdo,
          o_tipo_mensaje => v_tipo_mensaje,
          o_id_integrante => v_id_integrante,
          p_id_sexo => p_id_sexo,
          p_nro_documento => p_nro_documento,
          p_pai_cod_pais => p_pai_cod_pais,
          p_id_numero => p_id_numero,
          p_cuil => p_cuil,
          p_detalle => p_detalle,
          p_Nombre => p_nombre,
          p_apellido  => p_apellido,
          p_error_dato => p_error_dato,
          p_n_tipo_documento => p_n_tipo_documento);

        if o_rdo <> TYPES.c_Resp_OK then
          o_rdo := IPJ.VARIOS.MENSAJE_ERROR('INT_NOT') || '. ' || o_rdo;
          o_tipo_mensaje := v_tipo_mensaje;
          return;
        end if;
      else
        if p_error_dato = 'S' then
          update ipj.t_integrantes
          set
            error_dato = p_error_dato,
            detalle = p_detalle
          where
            id_integrante = p_id_integrante;
        end if;
        v_id_integrante := p_id_integrante;
      end if;

      insert into IPJ.T_TRAMITESIPJ_RETIRO (Id_Tramite_Ipj, id_integrante, Fecha)
      values (p_Id_Tramite_Ipj, v_id_integrante, sysdate);
    end if;

    o_rdo := TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
    WHEN OTHERS THEN
      o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Guardar_Tramite_Retiro;


  PROCEDURE SP_Eliminar_Tramite(
      o_rdo out nvarchar2,
      o_tipo_mensaje out number,
      p_Id_Tramite_Ipj number)
  IS
  BEGIN
  IF (nvl(p_Id_Tramite_Ipj,0) <> 0) THEN

        DELETE IPJ.T_ENTIDADES_ACTA_ORDEN
        WHERE ID_ENTIDAD_ACTA IN
             (SELECT EAO.ID_ENTIDAD_ACTA
              FROM IPJ.T_ENTIDADES_ACTA_ORDEN EAO JOIN IPJ.T_ENTIDADES_ACTA EA
                  ON EAO.ID_ENTIDAD_ACTA = EA.ID_ENTIDAD_ACTA
              WHERE EA.Id_Tramite_Ipj = p_Id_Tramite_Ipj);

        DELETE IPJ.T_ENTIDADES_ACTA_ARCH
        WHERE ID_ENTIDAD_ACTA IN
             (SELECT EAA.ID_ENTIDAD_ACTA
              FROM IPJ.T_ENTIDADES_ACTA_ARCH EAA JOIN IPJ.T_ENTIDADES_ACTA EA
                 ON EAA.ID_ENTIDAD_ACTA = EA.ID_ENTIDAD_ACTA
              WHERE EA.Id_Tramite_Ipj = p_Id_Tramite_Ipj);

        DELETE IPJ.T_ENTIDADES_ACTA WHERE Id_Tramite_Ipj = p_Id_Tramite_Ipj;

        DELETE IPJ.T_ENTIDADES_INS_LEGAL WHERE Id_Tramite_Ipj = p_Id_Tramite_Ipj;

        DELETE IPJ.T_ENTIDADES_RELACIONADAS WHERE Id_Tramite_Ipj = p_Id_Tramite_Ipj;

        DELETE IPJ.T_ENTIDADES_REPRESENTANTE WHERE Id_Tramite_Ipj = p_Id_Tramite_Ipj;

        DELETE IPJ.T_ENTIDADES_BALANCE WHERE Id_Tramite_Ipj = p_Id_Tramite_Ipj;

        DELETE IPJ.T_ENTIDADES_ORGANISMO WHERE Id_Tramite_Ipj = p_Id_Tramite_Ipj;

        DELETE IPJ.T_ENTIDADES_SINDICO WHERE Id_Tramite_Ipj = p_Id_Tramite_Ipj;

        DELETE IPJ.T_ENTIDADES_SOCIOS_COPRO WHERE ID_ENTIDAD_SOCIO IN (
                                                                        SELECT ID_ENTIDAD_SOCIO
                                                                        FROM IPJ.T_ENTIDADES_SOCIOS
                                                                        WHERE Id_Tramite_Ipj = p_Id_Tramite_Ipj);

        DELETE IPJ.T_ENTIDADES_SOCIOS_USUF WHERE ID_ENTIDAD_SOCIO IN (SELECT ID_ENTIDAD_SOCIO FROM IPJ.T_ENTIDADES_SOCIOS WHERE Id_Tramite_Ipj = p_Id_Tramite_Ipj);

        DELETE IPJ.T_ENTIDADES_SOCIOS_HIST WHERE Id_Tramite_Ipj = p_Id_Tramite_Ipj;

        DELETE ipj.T_ENTIDADES_SOCIO_CAPITAL WHERE Id_Tramite_Ipj = p_Id_Tramite_Ipj;

        DELETE ipj.T_ENTIDADES_SOCIOS_ACCIONES WHERE Id_Tramite_Ipj = p_Id_Tramite_Ipj;

        DELETE IPJ.T_ENTIDADES_SOCIOS WHERE Id_Tramite_Ipj = p_Id_Tramite_Ipj;

        DELETE IPJ.T_ENTIDADES_ADMIN_HIST WHERE Id_Tramite_Ipj = p_Id_Tramite_Ipj;

        DELETE IPJ.T_ENTIDADES_ADMIN WHERE Id_Tramite_Ipj = p_Id_Tramite_Ipj;

        DELETE IPJ.T_ENTIDADES_RUBROS WHERE Id_Tramite_Ipj = p_Id_Tramite_Ipj;

        DELETE IPJ.T_ENTIDADES_RUBRICA WHERE Id_Tramite_Ipj = p_Id_Tramite_Ipj;

        DELETE IPJ.T_ENTIDADES_ACCIONES WHERE Id_Tramite_Ipj = p_Id_Tramite_Ipj;

        DELETE IPJ.T_ENTIDADES_TRUBRO_IPJ WHERE Id_Tramite_Ipj = p_Id_Tramite_Ipj;

        DELETE IPJ.T_ENTIDADES WHERE Id_Tramite_Ipj = p_Id_Tramite_Ipj;

        DELETE IPJ.T_VEEDORES  WHERE Id_Tramite_Ipj = p_Id_Tramite_Ipj;

        DELETE IPJ.T_ENTIDADES_NOTAS  WHERE Id_Tramite_Ipj = p_Id_Tramite_Ipj;

        DELETE IPJ.T_TRAMITESIPJ_ACC_RESERVA  WHERE Id_Tramite_Ipj = p_Id_Tramite_Ipj;

        DELETE IPJ.T_TRAMITESIPJ_ACC_TASA  WHERE Id_Tramite_Ipj = p_Id_Tramite_Ipj;

        DELETE IPJ.T_ACCIONESIPJ_BOLETIN  WHERE Id_Tramite_Ipj = p_Id_Tramite_Ipj;

        DELETE IPJ.T_INFORMES_BORRADOR  WHERE Id_Tramite_Ipj = p_Id_Tramite_Ipj;

        DELETE IPJ.T_GRAVAMENES_INS_LEGAL  WHERE Id_Tramite_Ipj = p_Id_Tramite_Ipj;

        DELETE IPJ.T_GRAVAMENES  WHERE Id_Tramite_Ipj = p_Id_Tramite_Ipj;

        DELETE IPJ.T_COMERCIANTES_RUBRICA  WHERE Id_Tramite_Ipj = p_Id_Tramite_Ipj;

        DELETE IPJ.T_COMERCIANTES_RUBROS  WHERE Id_Tramite_Ipj = p_Id_Tramite_Ipj;

        DELETE IPJ.T_COMERCIANTES_INS_LEGAL  WHERE Id_Tramite_Ipj = p_Id_Tramite_Ipj;

        DELETE IPJ.T_COMERCIANTES  WHERE Id_Tramite_Ipj = p_Id_Tramite_Ipj;

        DELETE IPJ.T_ARCHIVO_MOVIMIENTOS WHERE ID_ARCHIVO_TRAMITE IN (
                        SELECT ID_ARCHIVO_TRAMITE
                        FROM IPJ.T_ARCHIVO_TRAMITE
                        WHERE Id_Tramite_Ipj = p_Id_Tramite_Ipj);

        DELETE IPJ.T_ARCHIVO_TRAMITE_HIST  WHERE Id_Tramite_Ipj = p_Id_Tramite_Ipj;

        DELETE IPJ.T_ARCHIVO_TRAMITE  WHERE Id_Tramite_Ipj = p_Id_Tramite_Ipj;

        DELETE IPJ.T_FONDOS_COMERCIO_VENDEDOR  WHERE Id_Tramite_Ipj = p_Id_Tramite_Ipj;

        DELETE IPJ.T_FONDOS_COMERCIO_COMPRADOR  WHERE Id_Tramite_Ipj = p_Id_Tramite_Ipj;

        DELETE IPJ.T_FONDOS_COMERCIO_RUBRO  WHERE Id_Tramite_Ipj = p_Id_Tramite_Ipj;

        DELETE IPJ.T_MANDATOS_INS_LEGAL  WHERE Id_Tramite_Ipj = p_Id_Tramite_Ipj;

        DELETE IPJ.T_MANDATOS_MANDATARIOS_PODERES WHERE ID_MANDATO_MANDATARIO IN
            (SELECT ID_MANDATO_MANDATARIO
            FROM IPJ.T_MANDATOS_MANDATARIOS
            WHERE Id_Tramite_Ipj = p_Id_Tramite_Ipj);

        DELETE IPJ.T_MANDATOS_MANDATARIOS  WHERE Id_Tramite_Ipj = p_Id_Tramite_Ipj;

        DELETE IPJ.T_MANDATOS  WHERE Id_Tramite_Ipj = p_Id_Tramite_Ipj;

        DELETE IPJ.T_TRAMITESIPJ_ACCIONES_ESTADO  WHERE Id_Tramite_Ipj = p_Id_Tramite_Ipj;

        DELETE IPJ.T_TRAMITESIPJ_ACCIONES  WHERE Id_Tramite_Ipj = p_Id_Tramite_Ipj;

        DELETE IPJ.T_TRAMITESIPJ_INTEGRANTE  WHERE Id_Tramite_Ipj = p_Id_Tramite_Ipj;

        DELETE IPJ.T_TRAMITES_IPJ_ESTADO  WHERE Id_Tramite_Ipj = p_Id_Tramite_Ipj;

        DELETE IPJ.T_TRAMITESIPJ_PERSJUR  WHERE Id_Tramite_Ipj = p_Id_Tramite_Ipj;

        DELETE IPJ.T_ENTIDADES_AUTORIZADOS WHERE Id_Tramite_Ipj = p_Id_Tramite_Ipj;

        DELETE IPJ.T_TRAMITESIPJ_AUTORIZ_BOE WHERE Id_Tramite_Ipj = p_Id_Tramite_Ipj;

        DELETE IPJ.T_TRAMITESIPJ  WHERE Id_Tramite_Ipj = p_Id_Tramite_Ipj;
    END IF;
      o_rdo := IPJ.TYPES.C_RESP_OK;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
      commit;
  EXCEPTION
    WHEN OTHERS THEN
        o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
        rollback;
  END SP_ELIMINAR_TRAMITE;


  PROCEDURE SP_Quitar_Borrador_Accion
    (o_rdo out nvarchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj number,
    p_id_tramiteipj_accion number)
 IS
    v_id_legajo number;
    v_id_integrante number;
    v_id_fondo_comercio number;
    v_id_tipo_accion number;
 BEGIN

    BEGIN
        SELECT id_Legajo, id_integrante, v_id_fondo_comercio, id_tipo_accion into v_id_legajo, v_id_integrante, v_id_fondo_comercio,v_id_tipo_accion
        FROM  IPJ.T_TRAMITESIPJ_ACCIONES
        WHERE id_tramiteipj_accion = p_id_tramiteipj_accion AND Id_Tramite_Ipj = p_Id_Tramite_Ipj;
    EXCEPTION
            WHEN NO_DATA_FOUND THEN
            v_id_legajo := null;
            v_id_integrante := null;
            v_id_fondo_comercio := null;
            v_id_tipo_accion := null;
    END;


     IF v_id_legajo is not null THEN

        DELETE FROM IPJ.T_ENTIDADES_ACTA_ARCH
        WHERE ID_ENTIDAD_ACTA IN ( SELECT ID_ENTIDAD_ACTA
                                                       FROM IPJ.T_ENTIDADES_ACTA
                                                       WHERE id_legajo = v_id_legajo
                                                                  AND Id_Tramite_Ipj = p_Id_Tramite_Ipj AND borrador = 'S');

       DELETE FROM IPJ.T_ENTIDADES_ACTA_ORDEN
        WHERE ID_ENTIDAD_ACTA IN ( SELECT ID_ENTIDAD_ACTA
                                                       FROM IPJ.T_ENTIDADES_ACTA
                                                       WHERE id_legajo = v_id_legajo AND Id_Tramite_Ipj = p_Id_Tramite_Ipj
                                                                   AND borrador = 'S');

        DELETE FROM IPJ.T_ENTIDADES_ACTA
        WHERE id_legajo = v_id_legajo
                   AND Id_Tramite_Ipj = p_Id_Tramite_Ipj AND borrador = 'S';

        DELETE FROM IPJ.T_ENTIDADES_RELACIONADAS
        WHERE id_legajo = v_id_legajo
                   AND Id_Tramite_Ipj = p_Id_Tramite_Ipj AND borrador = 'S';

        DELETE FROM IPJ.T_ENTIDADES_ADMIN_HIST
        WHERE id_legajo = v_id_legajo
                   AND Id_Tramite_Ipj = p_Id_Tramite_Ipj AND borrador = 'S';

        DELETE FROM IPJ.T_ENTIDADES_ADMIN
        WHERE id_legajo = v_id_legajo
                   AND Id_Tramite_Ipj = p_Id_Tramite_Ipj AND borrador = 'S';

        DELETE FROM IPJ.T_ENTIDADES_RELACIONADAS
        WHERE id_legajo = v_id_legajo
                   AND Id_Tramite_Ipj = p_Id_Tramite_Ipj AND borrador = 'S';

        DELETE FROM IPJ.T_ENTIDADES_INS_LEGAL
        WHERE id_legajo = v_id_legajo
                   AND Id_Tramite_Ipj = p_Id_Tramite_Ipj AND borrador = 'S';

        DELETE FROM IPJ.T_ENTIDADES_SOCIOS_COPRO
        WHERE id_entidad_socio in (SELECT id_entidad_socio
                                                FROM IPJ.T_ENTIDADES_SOCIOS
                                                WHERE id_legajo = v_id_legajo
                                                AND Id_Tramite_Ipj = p_Id_Tramite_Ipj AND borrador = 'S');

        DELETE FROM IPJ.T_ENTIDADES_SOCIOS_USUF
        WHERE id_entidad_socio  in (SELECT id_entidad_socio
                                                FROM IPJ.T_ENTIDADES_SOCIOS
                                                WHERE id_legajo = v_id_legajo
                                                AND Id_Tramite_Ipj = p_Id_Tramite_Ipj AND borrador = 'S');

        DELETE FROM IPJ.T_ENTIDADES_SOCIOS
        WHERE id_legajo = v_id_legajo
                   AND Id_Tramite_Ipj = p_Id_Tramite_Ipj AND borrador = 'S';

        DELETE FROM IPJ.T_ENTIDADES_SINDICO
        WHERE id_legajo = v_id_legajo
                   AND Id_Tramite_Ipj = p_Id_Tramite_Ipj AND borrador = 'S';

        DELETE FROM IPJ.T_ENTIDADES_BALANCE
        WHERE id_legajo = v_id_legajo
                   AND Id_Tramite_Ipj = p_Id_Tramite_Ipj AND borrador = 'S';

        DELETE FROM IPJ.T_ENTIDADES_REPRESENTANTE
        WHERE id_legajo = v_id_legajo
                   AND Id_Tramite_Ipj = p_Id_Tramite_Ipj AND borrador = 'S';

        DELETE FROM IPJ.T_ENTIDADES_RUBRICA
        WHERE id_legajo = v_id_legajo
                   AND Id_Tramite_Ipj = p_Id_Tramite_Ipj AND borrador = 'S';

        DELETE FROM IPJ.T_ENTIDADES_RUBROS
        WHERE id_legajo = v_id_legajo
                   AND Id_Tramite_Ipj = p_Id_Tramite_Ipj AND borrador = 'S';

        DELETE FROM IPJ.T_ENTIDADES
        WHERE id_legajo = v_id_legajo
                   AND Id_Tramite_Ipj = p_Id_Tramite_Ipj AND borrador = 'S';

        DELETE IPJ.T_MANDATOS_MANDATARIOS_PODERES
        WHERE id_mandato_mandatario IN (SELECT id_mandato_mandatario
                                                            FROM IPJ.T_MANDATOS_MANDATARIOS
                                                            WHERE  id_legajo = v_id_legajo
                                                            AND Id_Tramite_Ipj = p_Id_Tramite_Ipj AND borrador = 'S');

        DELETE FROM IPJ.T_MANDATOS_MANDATARIOS
        WHERE  id_legajo = v_id_legajo
                    AND Id_Tramite_Ipj = p_Id_Tramite_Ipj AND borrador = 'S';

        DELETE FROM IPJ.T_GRAVAMENES_INS_LEGAL
        WHERE  id_tramiteipj_accion = p_id_tramiteipj_accion
                    and id_tramiteipj_accion = p_id_tramiteipj_accion
                    AND id_tipo_accion = v_id_tipo_accion
                    AND borrador = 'S';


     ELSIF  v_id_integrante is not null THEN

        DELETE FROM IPJ.T_COMERCIANTES_INS_LEGAL
        WHERE id_integrante = v_id_integrante
                   AND Id_Tramite_Ipj = p_Id_Tramite_Ipj AND borrador = 'S';

        DELETE FROM IPJ.T_COMERCIANTES_RUBRICA
        WHERE id_integrante = v_id_integrante
                   AND Id_Tramite_Ipj = p_Id_Tramite_Ipj AND borrador = 'S';

        DELETE FROM IPJ.T_COMERCIANTES_RUBROS
        WHERE id_integrante = v_id_integrante
                   AND Id_Tramite_Ipj = p_Id_Tramite_Ipj AND borrador = 'S';

        DELETE IPJ.T_MANDATOS_MANDATARIOS_PODERES
        WHERE id_mandato_mandatario IN (SELECT id_mandato_mandatario
                                                            FROM IPJ.T_MANDATOS_MANDATARIOS
                                                            WHERE  id_integrante = v_id_integrante
                                                            AND Id_Tramite_Ipj = p_Id_Tramite_Ipj AND borrador = 'S');

        DELETE FROM IPJ.T_MANDATOS_MANDATARIOS
        WHERE  id_integrante = v_id_integrante
                    AND Id_Tramite_Ipj = p_Id_Tramite_Ipj AND borrador = 'S';

        DELETE FROM IPJ.T_GRAVAMENES_INS_LEGAL
        WHERE  id_tramiteipj_accion = p_id_tramiteipj_accion
                    and id_tramiteipj_accion = p_id_tramiteipj_accion
                    AND id_tipo_accion = v_id_tipo_accion
                    AND borrador = 'S';

     ELSIF  v_id_fondo_comercio is not null  THEN

        DELETE FROM IPJ.T_FONDOS_COMERCIO_INS_LEGAL
        WHERE id_fondo_comercio = v_id_fondo_comercio
                   AND Id_Tramite_Ipj = p_Id_Tramite_Ipj AND borrador = 'S';

        DELETE FROM IPJ.T_FONDOS_COMERCIO_COMPRADOR
        WHERE id_fondo_comercio = v_id_fondo_comercio
                   AND Id_Tramite_Ipj = p_Id_Tramite_Ipj AND borrador = 'S';

        DELETE FROM IPJ.T_FONDOS_COMERCIO_VENDEDOR
        WHERE id_fondo_comercio = v_id_fondo_comercio
                   AND Id_Tramite_Ipj = p_Id_Tramite_Ipj AND borrador = 'S';

     END IF;

  o_rdo :=  IPJ.TYPES.C_RESP_OK;
  o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  commit;
  EXCEPTION
    WHEN OTHERS THEN
        o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
        rollback;
END SP_Quitar_Borrador_Accion;

PROCEDURE Sp_Control_Legajo(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_nro_ficha in VARCHAR2,
    p_matricula in varchar2,
    p_razon_social in VARCHAR2,
    p_cuit in VARCHAR2,
    p_id_tipo_entidad  in number
  ) IS
    v_id_ubicacion number(5);
    v_cant number(5);
  BEGIN
    -- Busco el �rea del tipo de empresa, pasa realizar los controles
    select nvl(sum(id_ubicacion), 0)  into v_id_ubicacion
    from IPJ.t_tipos_entidades e
    where
      e.Id_Tipo_Entidad = p_id_tipo_entidad;

    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;

    -- Para SRL y SA con Matricula
    if v_id_ubicacion in (IPJ.Types.c_area_SRL, IPJ.Types.c_area_SxA) and p_matricula is not null then
      -- Controlo que la matricula no se repita en empresas de SRL o SxA
      select count(1) into v_cant
      from IPJ.t_legajos l left join IPJ.t_tipos_entidades e
        on l.id_tipo_entidad = e.id_tipo_entidad
      where
        IPJ.VARIOS.FC_Formatear_Matricula(l.nro_ficha) = IPJ.VARIOS.FC_Formatear_Matricula(p_matricula) and
        (nvl(L.Id_Tipo_Entidad, 0) = 0 or e.id_ubicacion in (IPJ.Types.c_area_SRL, IPJ.Types.c_area_SxA));

      if v_cant > 0 then
        o_rdo :=  o_rdo || chr(13) || ' Ya existe la Matr�cula ' || p_matricula || '.';
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      end if;
    end if;

    -- Para SA controlo registro si viene.
    if v_id_ubicacion = IPJ.Types.c_area_SxA and p_nro_ficha is not null then
      -- Controlo que el registro no se repita en empresas de SxA
      select count(1) into v_cant
      from IPJ.t_legajos l left join IPJ.t_tipos_entidades e
        on l.id_tipo_entidad = e.id_tipo_entidad
      where
        l.registro = p_nro_ficha and
        (nvl(L.Id_Tipo_Entidad, 0) = 0 or e.id_ubicacion = IPJ.Types.c_area_SxA);

      if v_cant > 0 then
        o_rdo :=  o_rdo || chr(13)  || ' Ya existe el Registro ' || p_nro_ficha || '.';
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      end if;
    end if;

    -- Para ACyF control la ficha si viene
    if v_id_ubicacion = IPJ.Types.c_area_CyF and p_nro_ficha is not null then
      -- Controlo que la Ficha no se repita en empresas de ACyF
      select count(1) into v_cant
      from IPJ.t_legajos l left join IPJ.t_tipos_entidades e
        on l.id_tipo_entidad = e.id_tipo_entidad
      where
        IPJ.VARIOS.FC_Formatear_Matricula(l.nro_ficha) = IPJ.VARIOS.FC_Formatear_Matricula(p_nro_ficha) and
        (nvl(L.Id_Tipo_Entidad, 0) = 0 or e.id_ubicacion = IPJ.Types.c_area_CyF);

      if v_cant > 0 then
        o_rdo :=  o_rdo || chr(13)  || ' Ya existe la Ficha ' || p_nro_ficha || '.';
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      end if;
    end if;

    -- Controlo la razon social, dentro de las empresas de la misma rama
    if v_id_ubicacion in (IPJ.Types.c_area_CyF, IPJ.Types.c_area_SxA, IPJ.Types.c_area_SRL) and p_razon_social is not null then
      select count(1) into v_cant
      from IPJ.t_legajos l left join IPJ.t_tipos_entidades e
        on l.id_tipo_entidad = e.id_tipo_entidad
      where
        upper(l.denominacion_sia) = upper(p_razon_social) and
        (nvl(L.Id_Tipo_Entidad, 0) = 0 or e.id_ubicacion = v_id_ubicacion) and
        nvl(eliminado, 0) = 0 and
        fecha_baja_entidad is null;

      if v_cant > 0 then
        o_rdo :=  o_rdo || chr(13)  || ' Ya existe la Empresa ' || p_razon_social || '.';
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      end if;
    end if;

    -- Controlo el CUIT
    if p_cuit is not null then
      select count(1) into v_cant
      from IPJ.t_legajos l
      where
        l.CUIT = p_cuit and
        nvl(eliminado, 0) = 0 and
        fecha_baja_entidad is null;

      if v_cant > 0 then
        o_rdo :=  o_rdo || chr(13)  || ' Ya existe el CUIT ' || p_cuit || '.';
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      end if;
    end if;

    if  o_tipo_mensaje = IPJ.TYPES.C_TIPO_MENS_OK then
      o_rdo :=  IPJ.TYPES.C_RESP_OK;
    end if;

  EXCEPTION
    WHEN OTHERS THEN
      o_rdo  := To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END Sp_Control_Legajo;

  PROCEDURE Sp_Guardar_Legajo(
    o_id_legajo_Rdo OUT number,
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_nro_ficha in VARCHAR2,
    p_matricula in varchar2,
    p_razon_social in VARCHAR2,
    p_cuit in VARCHAR2,
    p_id_tipo_entidad  in number,
    p_usuario in varchar2,
    p_id_legajo in number
  ) IS
  /*********************************************************
   Si p_id_legajo viene en 0 se crea un nuevo legajo con los datos indicados; sino
   se actualiza el tipo de empresa en el legajo indicado.
  *********************************************************/
    v_legajo number(15);
    v_id_tramite_legajo number(15);
    v_Cant number(15);
    v_id_ubicacion number;
    v_cuit varchar2(20);
    v_mensaje varchar2(2000);
    v_valid_parametros varchar2(2000);
  BEGIN
    -- Valido que el CUIT sea correcto
    if p_cuit is not null then
      v_cuit := trim(replace(p_cuit, '-', ''));
      t_comunes.pack_persona_juridica.VerificarCUIT (v_cuit, v_mensaje);
      if Upper(trim(v_mensaje)) <> 'OK' then --La validacion de CUIT responde siempre CUIL
        v_valid_parametros := v_valid_parametros || replace(v_mensaje, 'CUIL', 'CUIT') || '. ';
      end if;
    else
      v_cuit := null;
    end if;
    -- Si no hay legajo, requiere el nombre
    if nvl(p_id_legajo, 0) = 0 and p_razon_social is null then
       v_valid_parametros := v_valid_parametros || ' Razon Social inv�lido.';
    end if;

     if v_valid_parametros is not null then
      o_rdo := v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    -- Si viene un legajo, lo actualizo, sino verifico si se puede crear
    if nvl(p_id_legajo, 0) <> 0 then
      update ipj.t_legajos
      set id_tipo_entidad = p_id_tipo_entidad
      where
        id_legajo = p_id_legajo;

      v_legajo := p_id_legajo;
    else
      -- Controlo que no existe un legajo para los mismos datos
      Sp_Control_Legajo(
        o_rdo => v_valid_parametros,
        o_tipo_mensaje => o_tipo_mensaje,
        p_nro_ficha => p_nro_ficha,
        p_matricula => p_matricula,
        p_razon_social => p_razon_social,
        p_cuit => v_cuit,
        p_id_tipo_entidad => p_id_tipo_entidad);

      if o_tipo_mensaje <> IPJ.TYPES.C_TIPO_MENS_OK then
        o_rdo := v_valid_parametros;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
        return;
      end if;

     /**********************************************************
        Si los datos son correctos para crear un legajo, continua
     *********************************************************/
      -- Busco el �rea del tipo de empresa, pasa realizar los controles
      select nvl(sum(id_ubicacion), 0) into v_id_ubicacion
      from IPJ.t_tipos_entidades e
      where
        e.Id_Tipo_Entidad = p_id_tipo_entidad;

      -- Nuevo legajo
      SELECT seq_legajos.nextval INTO v_legajo FROM dual;

      insert into ipj.t_legajos ( id_legajo, nro_matricula, nro_ficha, denominacion_sia,
        cuit, sede, folio, tomo, anio, id_proceso, id_tipo_entidad, fecha_alta,
        usuario_alta, eliminado, registro, denominacion_sia_ant)
      values ( v_legajo, null,
        (case
          when v_id_ubicacion in  (IPJ.Types.c_area_SRL, IPJ.Types.c_area_SxA) then p_matricula
          when v_id_ubicacion = IPJ.Types.c_area_CyF then p_nro_ficha
          else null
        end), -- nro_ficha
        upper(p_razon_social),
        v_cuit, '00', null, null, null, 2, decode(p_id_tipo_entidad, 0 ,null, p_id_tipo_entidad),
        sysdate(), p_usuario, '0',
        (case
          when v_id_ubicacion = IPJ.Types.c_area_SxA then to_number(p_nro_ficha)
          else null
        end),-- registro
        null);

        /********************** Historicos *****************/
      insert into ipj.t_legajos_hist  (id_legajo, nro_matricula, nro_ficha, denominacion_sia,
        cuit, sede, folio, tomo, anio, id_tipo_entidad, fecha, usuario, eliminado,
        registro, denominacion_sia_ant)
      values  (v_legajo, null,
        (case
          when v_id_ubicacion in  (IPJ.Types.c_area_SRL, IPJ.Types.c_area_SxA) then p_matricula
          when v_id_ubicacion = IPJ.Types.c_area_CyF then p_nro_ficha
          else null
        end),
        upper(p_razon_social), v_cuit, '00', null, null, null, decode(p_id_tipo_entidad, 0, null, p_id_tipo_entidad), sysdate(), p_usuario,
        '0',
        (case
          when v_id_ubicacion = IPJ.Types.c_area_SxA then p_nro_ficha
          else null
        end),
        null);
    end if;

    o_id_legajo_Rdo := v_legajo;
    o_rdo :=  IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    Commit;
  EXCEPTION
    WHEN OTHERS THEN
      o_rdo  := To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      Rollback;
  END Sp_Guardar_Legajo;

  PROCEDURE Sp_Pasar_SUAC_Gestion(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_Id_Tramite_Ipj out number,
    p_cuil_usuario in varchar2,
    p_id_tramite_suac in varchar2
  ) IS
  /**********************************************************
    Al asignar un nuevo tr�mite SUAC a Gestion, controla varios puntos:
    - Que sea un tipo SUAC v�lido para IPJ
    - Que sea Nota / Expediente segun corresponda para cada tipo
    - Que el usuario pose permisos en el area del tramite.
    - Carga la persona Fisica o Jur�dica en el tr�mite si se encuentra bien definida
    - Se crea la acci�n necesaria si es posible y esta configurada
  **********************************************************/
    v_Row_SUAC NUEVOSUAC.VT_TRAMITES_IPJ_DESA%ROWTYPE;
    v_id_tipo_accion number(10);
    v_Id_Clasif_Ipj number(6);
    v_req_reemp char;
    v_id_ubicacion number(10);
    v_id_ubicacion_usr number(10);
    v_id_legajo number(15);
    v_id_integrante number (10);
    v_id_tipo_persona number(5); -- 0=Deshabilitada / 1=Personas Juridicas / 2=Personas F�sicas / 3=Ambas Personas / 4=Sin Personas
    v_mensaje varchar2(500);
    v_id_sexo varchar2(3);
    v_pai_cod_pais varchar2(10);
    v_id_numero number;
    v_cuil varchar2(20);
    v_Es_Expediente char(1);
    v_resp_PermAcc varchar2(2000);
    v_Tipo_PermAcc number;
    v_id_tipo_entidad ipj.t_tipos_entidades.id_tipo_entidad%TYPE;
  BEGIN
    --  busco los datos del tramite SUAC
    select * into v_Row_SUAC
    from NUEVOSUAC.VT_TRAMITES_IPJ_DESA s
    where
      S.ID_TRAMITE = p_id_tramite_suac;

    -- Busco la relaci�n entre SUAC y Gestion
    begin
      select sg.id_tipo_accion, ta.id_clasif_ipj, sg.id_ubicacion, Ta.Id_Tipo_Persona, Sg.Es_Expediente, nvl(ta.req_reemp, 'N')
        into v_id_tipo_accion, v_Id_Clasif_Ipj, v_id_ubicacion, v_id_tipo_persona, v_Es_Expediente, v_req_reemp
      from IPJ.T_RELACION_SUAC_GESTION sg left join IPJ.T_TIPOS_ACCIONESIPJ ta
          on sg.Id_Tipo_Accion = ta.Id_Tipo_Accion
        left join IPJ.T_TIPOS_CLASIF_IPJ tc
          on ta.Id_Clasif_Ipj = tc.Id_Clasif_Ipj
      where
        id_tipo_suac = v_Row_SUAC.id_tipo_tramite and
        id_subtipo_suac = v_Row_SUAC.id_subtipo_tramite;
    exception
       WHEN NO_DATA_FOUND THEN
         v_id_tipo_accion := 0;
         v_Id_Clasif_Ipj := null;
         v_id_ubicacion := 0;
         v_id_tipo_persona := 0;
         v_Es_Expediente := Null;
    end;

    if v_Es_Expediente is not null and v_Es_Expediente <> (case when v_Row_SUAC.nota_expediente = 'NOTA' then 'N' else 'S' end) then
      if v_Es_Expediente = 'S' then
        o_rdo  := 'Este tipo de Tr�mite SUAC debe ser un expediente o estar adjunto a uno.';
      else
        o_rdo  := 'Este tipo de Tr�mite SUAC debe ser una Nota.';
      end if;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      o_Id_Tramite_Ipj := 0;
      return;
    end if;

    -- Busco si el usuario tiene permiso al area de la accion, o si no existe a que area tiene permiso
    begin
      select t.id_ubicacion into v_id_ubicacion_usr
      from
        (select distinct u.id_ubicacion, to_number(u.orden)
         from IPJ.T_GRUPOS_TRAB_UBICACION tu join ipj.t_ubicaciones u
           on tu.id_ubicacion = u.id_ubicacion
         where
           tu.cuil = p_cuil_usuario and
           ( ( v_id_ubicacion = 0 and tu.id_ubicacion in (IPJ.TYPES.c_area_SRL, IPJ.TYPES.c_area_SxA, IPJ.TYPES.c_area_CyF))
             or v_id_ubicacion = tu.id_ubicacion)
         order by to_number(u.orden)) t
       where
         rownum = 1;
    exception
      WHEN NO_DATA_FOUND THEN
        v_id_ubicacion_usr := 0;
    end;

    -- Si SUAC tiene un area que el usuario no tiene permiso, no se avanza
    if v_id_ubicacion = 0 then
      o_rdo  := 'El expediente SUAC no corresponde a la tipificaci�n de IPJ.';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      o_Id_Tramite_Ipj := 0;
      return;
    else
      if v_id_ubicacion <> v_id_ubicacion_usr then
        o_rdo  := 'No tiene permiso para trabajar este expediente.';
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
        o_Id_Tramite_Ipj := 0;
        return;
      else
        -- Guardo el tramite para el area (del tramite o del usuario)
        IPJ.TRAMITES.SP_GUARDAR_TRAMITE(
          o_Id_Tramite_Ipj => o_Id_Tramite_Ipj,
          o_rdo => o_rdo,
          o_tipo_mensaje => o_tipo_mensaje,
          v_Id_Tramite_Ipj => 0, -- Solo se llama cuando se carga por primera vez un tramite
          v_id_tramite => p_id_tramite_suac,
          v_id_Clasif_IPJ => v_Id_Clasif_Ipj,
          v_id_Grupo => null,
          v_cuil_ult_estado => p_cuil_usuario,
          v_id_estado => 1, -- Estado Pendiente
          v_cuil_creador => p_cuil_usuario,
          v_observacion => null,
          v_id_Ubicacion => (case when v_id_ubicacion = 0 then v_id_ubicacion_usr else v_id_ubicacion end),
          v_urgente => 'N',
          p_sticker => v_Row_SUAC.NRO_STICKER,
          p_expediente => v_Row_SUAC.NRO_TRAMITE,
          p_simple_tramite => 'N');

        if o_rdo = IPJ.TYPES.C_RESP_OK then
          -- Si se carg� el tr�mite, veo de agregar la persona f�sica o jur�dica asociada
          if v_Row_SUAC.TIPO_INICIADOR = 'PERSONA JURIDICA' then
            if v_Row_SUAC.cuit_iniciador is not null then
              begin
                -- busco si ya tiene legajo, por CUIT
                select id_legajo into v_id_legajo
                from IPJ.t_legajos
                where
                  cuit = v_Row_SUAC.cuit_iniciador and
                  id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = (case when v_id_ubicacion = 0 then v_id_ubicacion_usr else v_id_ubicacion end)) and
                  nvl(eliminado, 0) <> 1 and
                  fecha_baja_entidad is null;
              exception
                -- si no tiene legajo y existe en AFIP, lo agrego
                when NO_DATA_FOUND then
                  v_id_legajo := null;
              end;
            end if;

            -- Si no tiene legajo, busco por NOMBRE
            if v_id_legajo is null and v_Row_SUAC.Razon_Social_Persona_juridica is not null then
              begin
                select id_legajo into v_id_legajo
                from IPJ.t_legajos
                where
                  IPJ.VARIOS.FC_QUITAR_ACENTOS(IPJ.VARIOS.FC_Quitar_Tipo_Empresa(denominacion_sia)) = IPJ.VARIOS.FC_QUITAR_ACENTOS(IPJ.VARIOS.FC_Quitar_Tipo_Empresa(v_Row_SUAC.Razon_Social_Persona_juridica)) and
                  cuit is null and
                  nvl(eliminado, '0') <> '1' and
                  fecha_baja_entidad is null and
                  id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = (case when v_id_ubicacion = 0 then v_id_ubicacion_usr else v_id_ubicacion end));
              exception
                -- si no tiene legajo y existe en AFIP, lo agrego
                when OTHERS then
                  v_id_legajo := null;
              end;
            end if;

            -- Si no tiene legajo, lo agrego
            if v_id_legajo is null then
              -- Controlo que el cuit sea valido
              t_comunes.pack_persona_juridica.VerificarCUIT (p_Cuil_Usuario, v_mensaje);
              if Upper(trim(v_mensaje)) <> IPJ.TYPES.C_RESP_OK then
                v_id_legajo := null;
              else
                --Si la acci�n es Alta de Fideicomisos se pasa la entidad 29-Fideicomisos
                IF v_id_tipo_accion = 109 THEN
                  v_id_tipo_entidad := 29;
                ELSE
                  v_id_tipo_entidad := 0;
                END IF;
                -- Agrego el nuevo legajo
                IPJ.TRAMITES.Sp_Guardar_Legajo(
                  o_id_legajo_Rdo => v_id_legajo,
                  o_rdo => o_rdo,
                  o_tipo_mensaje => o_tipo_mensaje,
                  p_nro_ficha => null,
                  p_matricula => null,
                  p_razon_social => v_Row_SUAC.Razon_Social_Persona_juridica,
                  p_cuit => v_Row_SUAC.cuit_iniciador,
                  p_id_tipo_entidad  => v_id_tipo_entidad,
                  p_usuario => p_cuil_usuario,
                  p_id_legajo => 0);
              end if;
            end if;

            if v_id_legajo is not null then
              IPJ.TRAMITES.SP_Guardar_TramiteIPJ_PersJur (
                o_rdo => o_rdo,
                o_tipo_mensaje => o_tipo_mensaje,
                p_Id_Tramite_Ipj => o_Id_Tramite_Ipj,
                p_id_legajo => v_id_legajo,
                p_id_sede => '00',
                p_error_dato => 'N',
                p_eliminar => 0);
            end if;
          else
            -- SI EL INICIADOR ES PERSONA FISICA
            if v_Row_SUAC.TIPO_INICIADOR = 'PERSONA FISICA' then
              begin
                select id_sexo, pai_cod_pais, id_numero, CUIL
                  into v_id_sexo, v_pai_cod_pais, v_id_numero, v_cuil
                from RCIVIL.VT_PERSONAS_CUIL
                where
                  nro_documento = v_Row_SUAC.documento_persona_fisica and
                  nov_nombre = v_Row_SUAC.Nombre and
                  nov_apellido = v_Row_SUAC.Apellido;
              exception
                when OTHERS then
                  v_id_sexo := null;
                  v_pai_cod_pais := null;
                  v_id_numero := null;
                  v_id_integrante := null;
              end;

              if v_id_sexo is not null and v_pai_cod_pais is not  null and v_id_numero is not null then
                IPJ.TRAMITES.SP_Guardar_TramiteIPJ_Int (
                  o_rdo => o_rdo,
                  o_tipo_mensaje => o_tipo_mensaje,
                  p_Id_Tramite_Ipj => o_Id_Tramite_Ipj,
                  p_id_integrante => null,
                  p_error_dato => 'N',
                  p_eliminar => 0,
                  p_id_sexo => v_id_sexo,
                  p_nro_documento => v_Row_SUAC.documento_persona_fisica,
                  p_pai_cod_pais => v_pai_cod_pais,
                  p_id_numero => v_id_numero,
                  p_cuil => v_cuil,
                  p_detalle => v_Row_SUAC.Nombre || ' ' || v_Row_SUAC.Apellido,
                  p_nombre => null,
                  p_apellido => null,
                  p_n_tipo_documento => null
                );
              end if;
            else
              v_id_legajo := null;
              v_id_sexo := null;
              v_pai_cod_pais := null;
              v_id_numero := null;
              v_id_integrante := null;
            end if;

          end if;

          -- Si las personas se cargaron bien, y tiene relacionada una accion la agrego
          -- Tipo Persona: 0=Deshabilitada / 1=Personas Juridicas / 2=Personas F�sicas / 3=Ambas Personas / 4=Sin Personas
          if (o_rdo = IPJ.TYPES.C_RESP_OK) and (nvl(v_id_tipo_accion, 0) <> 0) and
            ((nvl(v_id_integrante, 0) <> 0 and v_id_tipo_persona in (2, 3)) or (nvl(v_id_legajo, 0) <> 0 and v_id_tipo_persona in (1, 3)))  then
            IPJ.TRAMITES.SP_Guardar_TramiteIPJ_Acc (
              o_rdo => o_rdo,
              o_tipo_mensaje => o_tipo_mensaje,
              p_Id_Tramite_Ipj => o_Id_Tramite_Ipj,
              p_id_tramiteIpj_Accion => 0,
              p_Id_Tipo_Accion => v_id_tipo_accion,
              p_id_Integrante => v_id_integrante,
              p_DNI_Integrante => null,
              p_id_legajo => v_id_legajo,
              p_CUIT_persjur => null,
              p_id_estado => 1, -- Estado Pendiente
              p_Cuil_Usuario => p_cuil_usuario,
              p_Obs_Rubrica => null,
              p_Observacion => null,
              p_id_fondo_comercio => null,
              p_fec_veeduria => null,
              p_id_documento => null,
              p_n_documento => null,
              p_id_pagina => null
            );
          end if;
        end if;
      end if;

      -- Si los datos se pudieron cargar bien, hago commit, si algo fallo hago rollback.
      if o_rdo = IPJ.TYPES.C_RESP_OK then
        commit;
      else
        rollback;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
        o_Id_Tramite_Ipj := 0;
      end if;
    end if;

  EXCEPTION
    WHEN OTHERS THEN
      o_rdo  := To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END Sp_Pasar_SUAC_Gestion;


  PROCEDURE Sp_Crear_Accion_Automatica(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_cuil_usuario in varchar2,
    p_Id_Tramite_Ipj in number)
  IS
    v_Row_Tramite IPJ.t_tramitesipj%ROWTYPE;
    v_id_tipo_accion number(10);
    v_id_tipo_suac number(8);
    v_id_subtipo_suac number(8);
    v_Id_Clasif_Ipj number(6);
    v_req_reemp char;
    v_id_ubicacion number(10);
    v_id_ubicacion_usr number(10);
    v_id_legajo number(15);
    v_id_integrante number (10);
    v_id_tipo_persona number(5); -- 0=Deshabilitada / 1=Personas Juridicas / 2=Personas F�sicas / 3=Ambas Personas / 4=Sin Personas
    v_existe number(5);
    v_resp_PermAcc varchar2(2000);
    v_Tipo_PermAcc number;
  BEGIN
    --  busco los datos del tramite
    select * into v_Row_Tramite
    from IPJ.T_TramitesIPJ
    where
      Id_Tramite_Ipj = p_Id_Tramite_Ipj;

    -- Actualizo las acciones en las que tenga permiso con usuario Base de Datos, y le asigno el usuario actual
    update IPJ.t_tramitesipj_Acciones
    set cuil_usuario = p_cuil_usuario
    where
      Id_Tramite_Ipj = p_Id_Tramite_Ipj and
      cuil_usuario = '1' and
      id_tipo_accion in
        ( select id_tipo_accion
          from ipj.t_tipos_accionesipj ac join ipj.t_tipos_clasif_ipj cl
              on ac.id_clasif_ipj = cl.id_clasif_ipj
            join ipj.t_grupos_trab_ubicacion g
              on cl.id_ubicacion = g.id_ubicacion
          where
            G.CUIL = p_cuil_usuario
        );

    -- busco los datos del tramite SUAC
    begin
      select id_tipo_tramite, id_subtipo_tramite into v_id_tipo_suac, v_id_subtipo_suac
      from NUEVOSUAC.VT_TRAMITES_IPJ_DESA
      where
        id_tramite = v_Row_Tramite.id_tramite;
    exception
      when NO_DATA_FOUND then
        v_id_tipo_suac := 0;
        v_id_subtipo_suac := 0;
    end;

    -- Busco la relaci�n entre SUAC y Gestion
    begin
      select sg.id_tipo_accion, ta.id_clasif_ipj, sg.id_ubicacion, Ta.Id_Tipo_Persona, nvl(ta.req_reemp, 'N')
        into v_id_tipo_accion, v_Id_Clasif_Ipj, v_id_ubicacion, v_id_tipo_persona, v_req_reemp
      from IPJ.T_RELACION_SUAC_GESTION sg left join IPJ.T_TIPOS_ACCIONESIPJ ta
          on sg.Id_Tipo_Accion = ta.Id_Tipo_Accion
        left join IPJ.T_TIPOS_CLASIF_IPJ tc
          on ta.Id_Clasif_Ipj = tc.Id_Clasif_Ipj
      where
        id_tipo_suac =  v_id_tipo_suac and
        id_subtipo_suac = v_id_subtipo_suac;
    exception
       WHEN NO_DATA_FOUND THEN
         v_id_tipo_accion := 0;
         v_Id_Clasif_Ipj := null;
         v_id_ubicacion := 0;
         v_id_tipo_persona := 0;
    end;

    -- Veo si existe una accion preconfigurada en el tramite
    if nvl(v_id_tipo_accion, 0) <> 0 then
      select count(*) into v_existe
      from IPJ.t_tramitesipj_acciones
      where
        Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        id_tipo_accion = v_id_tipo_accion;

      o_rdo := IPJ.TYPES.C_RESP_OK;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;

      -- Si no existe, y se permite crear, creo una del tipo configurado para SUAC
      if v_existe = 0 then
        -- Busco la empresa o integrante
        if v_id_tipo_persona in (1, 3) then
          select sum(id_legajo) into v_id_legajo
          from IPJ.t_tramitesipj_persjur
          where
            Id_Tramite_Ipj = p_Id_Tramite_Ipj and
            rownum = 1;
        else
          select sum(id_integrante) into v_id_integrante
          from IPJ.T_Tramitesipj_Integrante
          where
            Id_Tramite_Ipj = p_Id_Tramite_Ipj and
            rownum = 1;
        end if;

          -- Tipo Persona: 0=Deshabilitada / 1=Personas Juridicas / 2=Personas F�sicas / 3=Ambas Personas / 4=Sin Personas
          if (nvl(v_id_tipo_accion, 0) <> 0) and
             ((nvl(v_id_integrante, 0) <> 0 and v_id_tipo_persona in (2, 3)) or (nvl(v_id_legajo, 0) <> 0 and v_id_tipo_persona in (1, 3)))  then
            IPJ.TRAMITES.SP_Guardar_TramiteIPJ_Acc (
              o_rdo => o_rdo,
              o_tipo_mensaje => o_tipo_mensaje,
              p_Id_Tramite_Ipj => p_Id_Tramite_Ipj,
              p_id_tramiteIpj_Accion => 0,
              p_Id_Tipo_Accion => v_id_tipo_accion,
              p_id_Integrante => v_id_integrante,
              p_DNI_Integrante => null,
              p_id_legajo => v_id_legajo,
              p_CUIT_persjur => null,
              p_id_estado => 1, -- Estado Pendiente
              p_Cuil_Usuario => p_cuil_usuario,
              p_Obs_Rubrica => null,
              p_Observacion => null,
              p_id_fondo_comercio => null,
              p_fec_veeduria => null,
              p_id_documento => null,
              p_n_documento => null,
              p_id_pagina => null
            );
          end if;
        --end if;
      end if;
    else
      -- Si no hay accion, retorno OK
      o_rdo := IPJ.TYPES.C_RESP_OK;
    end if;

    -- Si los datos se pudieron cargar bien, hago commit, si algo fallo hago rollback.
    if o_rdo = IPJ.TYPES.C_RESP_OK then
      commit;
    else
      rollback;
    end if;

  EXCEPTION
    WHEN OTHERS THEN
      o_rdo  := To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END Sp_Crear_Accion_Automatica;

  PROCEDURE SP_Traer_Datos_Expediente(
    o_Cursor OUT types.cursorType,
    p_sticker in varchar2,
    p_expediente in varchar2)
  IS
  /****************************************************
   Muestra el area, estado y responsable de un expediente, si existe.
   Se utiliza para cuando no se encuentra el expediente en la lista de pendientes de SUAC
  *****************************************************/
  BEGIN
       OPEN o_Cursor FOR
         select t.Id_Tramite_Ipj, t.expediente, t.sticker, t.Cuil_Ult_Estado, t.Id_Estado_Ult,
           ub.N_Ubicacion, e.N_Estado, t.expediente nro_expediente, t.observacion,
           (case
               when t.Cuil_Ult_Estado is not null then (select Descripcion from ipj.t_usuarios where cuil_usuario = t.Cuil_Ult_Estado)
               else (select n_grupo from IPJ.T_GRUPOS where id_grupo =  t.Id_Grupo_Ult_Estado)
           end) Descripcion,
           IPJ.TRAMITES_SUAC.FC_BUSCAR_ASUNTO_SUAC(t.id_tramite) Asunto
         from IPJ.T_TRAMITESIPJ t join IPJ.t_ubicaciones ub
             on T.ID_UBICACION = UB.ID_UBICACION
           join IPJ.T_ESTADOS e
             on T.ID_ESTADO_ULT = e.id_estado
         where
           --T.ID_PROCESO not in (4, 5) and  -- Elimino los historicos
           (p_sticker is not null and T.STICKER = p_sticker) or
           (p_expediente is not null and T.EXPEDIENTE = p_expediente);

  END SP_Traer_Datos_Expediente;

  PROCEDURE SP_Traer_Lista_Acc (
    o_Cursor out types.cursorType,
    p_Id_Tramite_Ipj in number,
    p_lista_acc in varchar2
    )
  IS
  /*************************************************
     Muestra los nombres de un listado de acciones
 *************************************************/
   vSQL varchar2(3000);
  BEGIN
    vSQL :=
      'select Id_Tramite_Ipj, id_tramiteipj_accion, Sticker, Expediente, N_Ubicacion, usuario, id_estado, ' ||
      '  n_estado, clase, observacion, tipo_ipj, Cuil_Creador, cuil_usuario, Id_Tipo_Accion, ' ||
      '  Id_Integrante, id_legajo, id_pagina ' ||
      'from IPJ.VT_TRAMITE_TECN_IPJ v ' ||
      'where ' ||
      '  v.Id_Tramite_Ipj = ' || to_char(p_Id_Tramite_Ipj) || ' and ' ||
      '  v.ID_TRAMITEIPJ_ACCION in (' || p_lista_acc || ') ';

    OPEN o_Cursor FOR vSQL;
  exception
    when OTHERS then
       o_Cursor := null;
  END SP_Traer_Lista_Acc;

  PROCEDURE SP_Act_Tramites_Relac(
    o_rdo out nvarchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number)
  IS
  /**********************************************************
    Actualiza los datos de los demas tramites abiertos del mismo legajo.
  **********************************************************/
    v_cursorEntidades  IPJ.types.cursorType;
    v_RowEntidad IPJ.t_entidades%ROWTYPE;
    v_RowEntidadOld IPJ.t_entidades%ROWTYPE;
    v_cant number;
    v_Exist_Entidad number;
    v_EsAnexo NUMBER;
  BEGIN
    -- Cuento si existen otros tramites abiertos para la misma entidad
    select count(1) into v_cant
    from ipj.t_entidades e join ipj.t_tramitesipj t
      on e.Id_Tramite_Ipj = t.Id_Tramite_Ipj
    where
      e.id_legajo = p_id_legajo and
      e.Id_Tramite_Ipj <> p_Id_Tramite_Ipj and
      T.ID_ESTADO_ULT < IPJ.TYPES.C_ESTADOS_COMPLETADO
      --and t.codigo_online IS NULL
      ;

    -- Veo que exista entidad para comparar (rubricas e informes no tienen)
    select count(1)into v_Exist_Entidad
    from ipj.t_entidades e
    where
      Id_Tramite_Ipj = p_Id_Tramite_Ipj and
      id_legajo = p_id_legajo;

    if v_cant > 0 and v_Exist_Entidad > 0 then
      DBMS_OUTPUT.PUT_LINE('1- SP_Act_Tramites_Relac - Arma cursor de relacionados');
      -- busco los datos del tramite actual
      select * into v_RowEntidad
      from ipj.t_entidades e
      where
        Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        id_legajo = p_id_legajo;

      -- Armo un cursor con los tramites abiertos con algun dato distinto (excluyendo la observacion).
      OPEN v_cursorEntidades FOR
        select
          -- Desarrollo
          /*
          e.Id_Tramite_Ipj, e.Cuit, e.Id_Sede, e.Id_Moneda, e.Id_Unidad_Med, e.Matricula, e.Fec_Inscripcion
          , e.Folio, e.Libro_Nro, e.Tomo, e.Anio, e.Monto, e.Acta_Contitutiva, e.Vigencia, e.Cierre_Ejercicio, e.Fec_Vig
          , e.Id_Vin_Comercial, e.Id_Vin_Real, e.Borrador, e.Fec_Acto, e.Denominacion_1, e.Denominacion_2
          , e.Alta_Temporal, e.Baja_Temporal, e.Tipo_Vigencia, e.Valor_Cuota, e.Matricula_Version
          , e.Cuotas, e.Id_Tipo_Entidad, e.Id_Legajo, e.Id_Tipo_Administracion, e.Id_Estado_Entidad, e.Obs_Cierre
          , e.Nro_Resolucion, e.Fec_Resolucion, e.Nro_Boletin, e.Fec_Boletin, e.Nro_Registro, e.Sede_Estatuto
          , e.Requiere_Sindico, e.Origen, e.Es_Sucursal, '' Observacion, e.Id_Tipo_Origen, e.Cierre_Fin_Mes
          , UTL_RAW.CAST_TO_VARCHAR2(dbms_sqlhash.gethash('select objeto_social from ipj.t_entidades where id_legajo = '|| to_char(e.id_legajo) || ' and Id_Tramite_Ipj = ' || to_char(e.Id_Tramite_Ipj), 1)) Objeto_Social,
          e.Fiscalizacion_Ejerc, e.Incluida_Lgs, e.Condicion_Fideic, e.Obs_Fiduciario,
          e.Obs_Fiduciante, e.Obs_Beneficiario, e.Obs_Fideicomisario, e.fecha_versionado,
          e.sin_denominacion, e.contrato_privado, e.fec_vig_hasta, e.cant_Fiduciario,
          e.cant_Fiduciante, e.cant_Beneficiario, e.cant_Fideicomisario, e.id_obj_social, e.acredita_grupo, e.fec_estatuto,
          e.cant_fiduciario_otros, e.cant_fiduciante_otros, e.cant_beneficiario_otros, e.cant_fideicomisario_otros,
          e.gubernamental
    */
          -- Produccion
          e.Cuit, e.Id_Sede, e.Id_Moneda, e.Id_Unidad_Med, e.Matricula, e.Fec_Inscripcion, e.Folio, e.Libro_Nro,
          e.Tomo, e.Anio, e.Monto, e.Acta_Contitutiva, e.Vigencia, e.Cierre_Ejercicio, e.Fec_Vig,
          e.Id_Vin_Comercial, e.Id_Vin_Real, e.Borrador, e.Fec_Acto, e.Denominacion_1, e.Denominacion_2,
          e.Alta_Temporal, e.Baja_Temporal, e.Tipo_Vigencia, e.Valor_Cuota, e.Matricula_Version, e.Cuotas,
          e.Id_Tipo_Entidad, e.Id_Tramite_Ipj, e.Id_Legajo, e.Id_Tipo_Administracion, e.Id_Estado_Entidad,
          e.Obs_Cierre, e.Nro_Resolucion, e.Fec_Resolucion, e.Nro_Boletin, e.Fec_Boletin, e.Nro_Registro,
          e.Sede_Estatuto, e.Requiere_Sindico, e.Origen, e.Es_Sucursal, '' Observacion, e.Id_Tipo_Origen,
          e.Cierre_Fin_Mes,
          dbms_sqlhash.gethash('select objeto_social from ipj.t_entidades where id_legajo = '|| to_char(e.id_legajo) || ' and Id_Tramite_Ipj = ' || to_char(e.Id_Tramite_Ipj), 1) Objeto_Social,
          e.Fiscalizacion_Ejerc, e.Incluida_Lgs, e.Condicion_Fideic, e.Obs_Fiduciario,
          e.Obs_Fiduciante, e.Obs_Beneficiario, e.Obs_Fideicomisario, e.fecha_versionado,
          e.sin_denominacion, e.contrato_privado, e.fec_vig_hasta, e.cant_Fiduciario,
          e.cant_Fiduciante, e.cant_Beneficiario, e.cant_Fideicomisario, e.id_obj_social, e.fec_estatuto, e.acredita_grupo,
          e.cant_fiduciario_otros, e.cant_fiduciante_otros, e.cant_beneficiario_otros, e.cant_fideicomisario_otros,
          e.gubernamental
        from ipj.t_entidades e join ipj.t_tramitesipj t
          on e.Id_Tramite_Ipj = t.Id_Tramite_Ipj
        where
          e.id_legajo = p_id_legajo and
          e.Id_Tramite_Ipj <> p_Id_Tramite_Ipj and
          t.Id_Estado_Ult < Ipj.Types.C_Estados_Completado AND
          t.codigo_online IS NULL

        MINUS
        select
          -- Desarrollo
          /*
          ee.Id_Tramite_Ipj, ee.Cuit, ee.Id_Sede, ee.Id_Moneda, ee.Id_Unidad_Med, ee.Matricula, ee.Fec_Inscripcion
          , ee.Folio, ee.Libro_Nro, ee.Tomo, ee.Anio, ee.Monto, ee.Acta_Contitutiva, ee.Vigencia, ee.Cierre_Ejercicio, ee.Fec_Vig
          , ee.Id_Vin_Comercial, ee.Id_Vin_Real, ee.Borrador, ee.Fec_Acto, ee.Denominacion_1, ee.Denominacion_2
          , ee.Alta_Temporal, ee.Baja_Temporal, ee.Tipo_Vigencia, ee.Valor_Cuota, ee.Matricula_Version
          , ee.Cuotas, ee.Id_Tipo_Entidad, ee.Id_Legajo, ee.Id_Tipo_Administracion, ee.Id_Estado_Entidad, ee.Obs_Cierre
          , ee.Nro_Resolucion, ee.Fec_Resolucion, ee.Nro_Boletin, ee.Fec_Boletin, ee.Nro_Registro, ee.Sede_Estatuto
          , ee.Requiere_Sindico, ee.Origen, ee.Es_Sucursal, '' Observacion, ee.Id_Tipo_Origen, ee.Cierre_Fin_Mes
          , UTL_RAW.CAST_TO_VARCHAR2(dbms_sqlhash.gethash('select objeto_social from ipj.t_entidades where id_legajo = '|| to_char(ee.id_legajo) || ' and Id_Tramite_Ipj = ' || to_char(ee.Id_Tramite_Ipj), 1)) Objeto_Social,
          ee.Fiscalizacion_Ejerc, ee.Incluida_Lgs, ee.Condicion_Fideic, ee.Obs_Fiduciario,
          ee.Obs_Fiduciante, ee.Obs_Beneficiario, ee.Obs_Fideicomisario, ee.fecha_versionado,
          ee.sin_denominacion, ee.contrato_privado, ee.fec_vig_hasta, ee.cant_Fiduciario,
          ee.cant_Fiduciante, ee.cant_Beneficiario, ee.cant_Fideicomisario, ee.id_obj_social, ee.acredita_grupo, ee.fec_estatuto,
          ee.cant_fiduciario_otros, ee.cant_fiduciante_otros, ee.cant_beneficiario_otros, ee.cant_fideicomisario_otros,
          ee.gubernamental
    */
          -- Produccion
          ee.Cuit, ee.Id_Sede, ee.Id_Moneda, ee.Id_Unidad_Med, ee.Matricula, ee.Fec_Inscripcion, ee.Folio, ee.Libro_Nro,
          ee.Tomo, ee.Anio, ee.Monto, ee.Acta_Contitutiva, ee.Vigencia, ee.Cierre_Ejercicio, ee.Fec_Vig,
          ee.Id_Vin_Comercial, ee.Id_Vin_Real, ee.Borrador, ee.Fec_Acto, ee.Denominacion_1, ee.Denominacion_2,
          ee.Alta_Temporal, ee.Baja_Temporal, ee.Tipo_Vigencia, ee.Valor_Cuota, ee.Matricula_Version, ee.Cuotas,
          ee.Id_Tipo_Entidad, ee.Id_Tramite_Ipj, ee.Id_Legajo, ee.Id_Tipo_Administracion, ee.Id_Estado_Entidad,
          ee.Obs_Cierre, ee.Nro_Resolucion, ee.Fec_Resolucion, ee.Nro_Boletin, ee.Fec_Boletin, ee.Nro_Registro,
          ee.Sede_Estatuto, ee.Requiere_Sindico, ee.Origen, ee.Es_Sucursal, '' Observacion, ee.Id_Tipo_Origen,
          ee.Cierre_Fin_Mes,
          dbms_sqlhash.gethash('select objeto_social from ipj.t_entidades where id_legajo = '|| to_char(ee.id_legajo) || ' and Id_Tramite_Ipj = ' || to_char(ee.Id_Tramite_Ipj), 1) Objeto_Social,
          ee.Fiscalizacion_Ejerc, ee.Incluida_Lgs, ee.Condicion_Fideic, ee.Obs_Fiduciario,
          ee.Obs_Fiduciante, ee.Obs_Beneficiario, ee.Obs_Fideicomisario, ee.fecha_versionado,
          ee.sin_denominacion, ee.contrato_privado, ee.fec_vig_hasta, ee.cant_Fiduciario,
          ee.cant_Fiduciante, ee.cant_Beneficiario, ee.cant_Fideicomisario, ee.id_obj_social, ee.fec_estatuto, ee.acredita_grupo,
          ee.cant_fiduciario_otros, ee.cant_fiduciante_otros, ee.cant_beneficiario_otros, ee.cant_fideicomisario_otros,
          ee.gubernamental
        from ipj.t_entidades ee
        where
          id_legajo = p_id_legajo and
          Id_Tramite_Ipj = p_Id_Tramite_Ipj;

      -- Actualiza cada entidad de los tramites abiertos y con alguna diferencia
      LOOP
        fetch v_cursorEntidades into v_RowEntidadOld;
        EXIT WHEN v_cursorEntidades%NOTFOUND or v_cursorEntidades%NOTFOUND is null;

        DBMS_OUTPUT.PUT_LINE('2- SP_Act_Tramites_Relac - Actualiza relacionados');
        update ipj.t_entidades
        set
          Cuit = v_RowEntidad.cuit,
          Id_Sede = v_RowEntidad.id_sede,
          Id_Moneda = v_RowEntidad.id_moneda,
          Id_Unidad_Med = v_RowEntidad.id_unidad_med,
          Matricula = v_RowEntidad.Matricula,
          Fec_Inscripcion = v_RowEntidad.Fec_Inscripcion,
          Folio = v_RowEntidad.Folio,
          Libro_Nro = v_RowEntidad.Libro_Nro,
          Tomo = v_RowEntidad.Tomo,
          Anio = v_RowEntidad.Anio,
          objeto_social = v_RowEntidad.objeto_social,
          Monto = v_RowEntidad.Monto,
          Acta_Contitutiva = v_RowEntidad.Acta_Contitutiva,
          Vigencia = v_RowEntidad.Vigencia,
          Cierre_Ejercicio = v_RowEntidad.Cierre_Ejercicio,
          Fec_Vig = v_RowEntidad.Fec_Vig,
          Id_Vin_Real = v_RowEntidad.Id_Vin_Real,
          Fec_Acto = v_RowEntidad.Fec_Acto,
          Denominacion_1 = v_RowEntidad.Denominacion_1,
          Alta_Temporal = v_RowEntidad.Alta_Temporal,
          Baja_Temporal = v_RowEntidad.Baja_Temporal,
          Tipo_Vigencia = v_RowEntidad.Tipo_Vigencia,
          Valor_Cuota = v_RowEntidad.Valor_Cuota,
          --Matricula_Version = (case when v_RowEntidadOld.Matricula_Version <= v_RowEntidad.Matricula_Version then v_RowEntidad.Matricula_Version +1 else Matricula_Version end),-- ver este punto
          Cuotas = v_RowEntidad.Cuotas,
          Id_Tipo_Entidad = v_RowEntidad.id_tipo_entidad,
          Id_Tipo_Administracion = v_RowEntidad.id_tipo_administracion,
          Id_Estado_Entidad = v_RowEntidad.id_estado_entidad,
          Obs_Cierre = v_RowEntidad.Obs_cierre,
          Nro_Resolucion = v_RowEntidad.Nro_Resolucion,
          Fec_Resolucion = v_RowEntidad.Fec_Resolucion,
          Nro_Boletin = v_RowEntidad.Nro_Boletin,
          Fec_Boletin = v_RowEntidad.Fec_Boletin,
          Nro_Registro = v_RowEntidad.Nro_Registro,
          Sede_Estatuto = v_RowEntidad.Sede_Estatuto,
          Requiere_Sindico = v_RowEntidad.Requiere_Sindico,
          Origen = v_RowEntidad.Origen,
          Es_Sucursal = v_RowEntidad.Es_Sucursal,
          Observacion = (case
                                    when observacion = v_RowEntidad.Observacion then Observacion
                                    when observacion <> v_RowEntidad.Observacion and length(observacion) + length(v_RowEntidad.Observacion) >= 4000 then Observacion
                                    else  v_RowEntidad.Observacion|| chr(13) || Observacion
                                 end),
          Id_Tipo_Origen = v_RowEntidad.id_Tipo_Origen,
          cierre_fin_mes = v_RowEntidad.cierre_fin_mes,
          sin_denominacion = v_RowEntidad.sin_denominacion,
          contrato_privado = v_RowEntidad.contrato_privado,
          cant_Fiduciario = v_RowEntidad.cant_Fiduciario,
          cant_Fiduciante = v_RowEntidad.cant_Fiduciante,
          cant_Beneficiario = v_RowEntidad.cant_Beneficiario,
          cant_Fideicomisario = v_RowEntidad.cant_Fideicomisario,
          acredita_grupo = v_RowEntidad.Acredita_Grupo,
          cant_fiduciario_otros = v_RowEntidad.Cant_Fiduciario_Otros,
          cant_fiduciante_otros = v_RowEntidad.Cant_Fiduciante_Otros,
          cant_beneficiario_otros = v_RowEntidad.Cant_Beneficiario_Otros,
          cant_fideicomisario_otros = v_RowEntidad.Cant_Fideicomisario_Otros,
          gubernamental = v_RowEntidad.gubernamental
        where
          Id_Tramite_Ipj = v_RowEntidadOld.Id_Tramite_Ipj and
          id_legajo = p_id_legajo;

      END LOOP;

      close v_cursorEntidades;
    end if;
    DBMS_OUTPUT.PUT_LINE('3- SP_Act_Tramites_Relac - Finaliza');
    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;

  EXCEPTION
    WHEN OTHERS THEN
      o_rdo := 'SP_Act_Tramites_Relac - ' || To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Act_Tramites_Relac;

  PROCEDURE SP_Permitir_Acciones (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_clasif_ipj in number,
    p_id_legajo in number
    )
  IS
    /********************************************************
      Dados una empresa y una clasificacion, verifica si puede iniciar el tamite deseado:
      - Informes se puede siempre (SRL = 2 / SA = 6 / ACyF = 8)
      - Reservas se puede siempre (SA = 11 / ACyF = 13)
      - Las demas se bloquean si la empresa no cuenta con un reempadronamiento o constitucion online
      - Legajo vencido, solo permite Informes y Certificados (SRL = 2 / SA = 6 / ACyF = 8)
    *********************************************************/
    v_Const_OL_Aprob number;
    v_Reemp_OL_Aprob number;
    v_Fecha_Reemp_OK date;
    v_Exp_Reemp_OK varchar2(50);
    v_Reemp_OL_Pend number;
    v_reemp number;
    v_const number;
    v_id_ubicacion number;
    v_baja_legajo date;
    v_id_tipo_entidad number;
  BEGIN
    select fecha_baja_entidad, id_tipo_entidad into v_baja_legajo, v_id_tipo_entidad
    from ipj.t_legajos
    where
      id_legajo = p_id_legajo;

    -- Si no es clasificacion JUDICIAL o FIDEICOMISOS de SRL, y no tiene tipo falla.
    if (p_id_clasif_ipj not in  (1, 18) and nvl(v_id_tipo_entidad, 0) = 0) then
      o_Rdo := 'La empresa no tiene tipo asignado, avise a SISTEMA. B�sque si esta duplicada con otra empresa sin CUIT.';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      Return;
    end if;

    -- Si esta dado de baja, solo permito informes
    if v_baja_legajo is not null and p_id_clasif_ipj not in (2, 6, 8)then
      o_Rdo := 'La empresa se encuentra dada de baja, no es posible modificarla.';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      Return;
    end if;

    -- Informes, Certificados y Reservas son v�lidos
    if p_id_clasif_ipj in (2, 6, 8, 11, 13) then
      o_Rdo := IPJ.TYPES.C_RESP_OK;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    else
      -- No se controlan todas las empresa para reempadronar
      if v_id_tipo_entidad in (2) then -- SA
        -- busco el area de la clasificacion
        select id_ubicacion into v_id_ubicacion
        from ipj.t_tipos_clasif_ipj
        where
          id_clasif_ipj = p_id_clasif_ipj;

        -- Veo que areas tienen tr�mites de reempadronamiento
        if v_id_ubicacion = IPJ.TYPES.C_AREA_SRL then
          v_reemp := to_number(nvl(IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG ('REEMP_RPC'), '0'));
          v_const := to_number(nvl(IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG ('CONST_OL_SXA_RPC'), '0'));
        end if;
        if v_id_ubicacion = IPJ.TYPES.C_AREA_SXA then
          v_reemp := to_number(nvl(IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG ('REEMP_SXA'), '0'));
          v_const := to_number(nvl(IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG ('CONST_OL_SXA'), '0'));
        end if;
        if v_id_ubicacion = IPJ.TYPES.C_AREA_CYF then
          v_reemp := to_number(nvl(IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG ('REEMP_ACYF'), '0'));
          v_const := to_number(nvl(IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG ('CONST_OL_ACYF'), '0'));
        end if;
        if v_id_ubicacion not in (IPJ.TYPES.C_AREA_CYF, IPJ.TYPES.C_AREA_SXA, IPJ.TYPES.C_AREA_SRL) then
          v_reemp := 0;
          v_const := 0;
        end if;

        -- Si no hay reempadronamiento, no se controla
        if v_reemp = 0 then
          o_Rdo := IPJ.TYPES.C_RESP_OK;
          o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
        else
          -- Verifico si tiene una constituci�n o reempadronamiento online
          select count(1) into v_Const_OL_Aprob
          from ipj.t_tramitesipj tr join ipj.t_ol_entidades ol
              on tr.codigo_online = ol.codigo_online
            join ipj.t_tramitesipj_persjur pj
              on pj.id_tramite_ipj = tr.id_tramite_ipj
          where
            OL.ID_TIPO_TRAMITE_OL in (v_const, v_Reemp) and
            pj.id_legajo =  p_id_legajo and
            tr.id_estado_ult < IPJ.TYPES.C_ESTADOS_RECHAZADO and
            tr.id_estado_ult >= IPJ.TYPES.C_ESTADOS_COMPLETADO ;

          -- Si encuentra algun tr�mite, pasa
          if nvl(v_Const_OL_Aprob, 0) > 0 then
            o_Rdo := IPJ.TYPES.C_RESP_OK;
            o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
          else
            o_Rdo := 'Esta empresa requiere una Actualizaci�n de Datos Online.';
            o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
          end if;
        end if;
      else
        o_Rdo := IPJ.TYPES.C_RESP_OK;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
      end if;
    end if;

  END SP_Permitir_Acciones;

  PROCEDURE SP_Traer_Adjuntos (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_Cursor OUT types.cursorType,
    p_Id_Tramite_Ipj in number)
  IS
    v_id_tramite_suac number;
  BEGIN
    begin
      select nvl(id_tramite, 0) into v_id_tramite_suac
      from ipj.t_tramitesipj
      where
        id_tramite_ipj = p_id_tramite_ipj;
    exception
      when OTHERS then
        v_id_tramite_suac := 0;
    end;

    -- Si tiene tramite suac, busco los pendientes
    --if v_id_tramite_suac > 0 then
      Open o_Cursor for
        Select v_sticker_12, v_nro_tramite, v_id, to_char(v_fecha_inicio, 'dd/mm/rrrr') v_fecha_inicio,
          v_asunto, v_iniciador, v_expediente, v_id_padre, v_tipo_relacion
        From Nuevosuac.Vt_Get_Tramites_Ipj_Anexos A
        Where
          A.V_Id_Padre = nvl(v_id_tramite_suac, 0);
    --end if;

    o_Rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
    WHEN OTHERS THEN
      o_rdo := 'SP_Traer_Adjuntos - ' || To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Traer_Adjuntos;

  PROCEDURE SP_Solicita_Certif_Gob(
    o_rdo out nvarchar2,
    o_tipo_mensaje out number,
    p_cuil_empleado in varchar2,
    p_id_legajo in number)
  IS
  /**********************************************************
    Registra los pedidos de certificado de los empleados de gobierno (Fuera de IPJ)
  **********************************************************/
  BEGIN

    insert into IPJ.T_SOLICITUD_CERTIFICADO (fecha, cuil_empleado, id_legajo)
    values (sysdate, p_cuil_empleado, p_id_legajo);

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;

  EXCEPTION
    WHEN OTHERS THEN
      o_rdo := 'SP_Solicita_Certif_Gob - ' || To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Solicita_Certif_Gob;

  PROCEDURE SP_Traer_Datos_OL (
    o_Cursor OUT types.cursorType,
    p_Id_Tramite_Ipj in number
    )
  IS
  /*
    Dado un tr�mite, devuelve los datos del tr�mite online relacionado
  */
  BEGIN

    OPEN o_Cursor FOR
      select
        tr.codigo_online, id_tipo_tramite_ol, id_sub_tramite_ol, tr.id_ubicacion_origen,
        cod_seguridad
      from ipj.t_tramitesipj tr join ipj.t_ol_entidades ol
        on tr.codigo_online = ol.codigo_online
      where
        tr.id_tramite_ipj = p_id_tramite_ipj;

  END SP_Traer_Datos_OL;

  PROCEDURE SP_Traer_Pendientes_Firma(
    o_Cursor OUT types.cursorType,
    p_cuil_usuario in varchar2,
    p_id_clasif_ipj in number
    )
  IS
  /****************************************************
   Este procedimiento trae todas las acciones pendientes de firma, para el �rea
   a la que pertenece el usuario
  *****************************************************/
  BEGIN

    OPEN o_Cursor FOR
      select vt.id_tramite_ipj, vt.codigo_online, vt.id_tramiteipj_accion,
        vt.expediente, vt.sticker, vt.fecha_asignacion, vt.id_tipo_accion,
        vt.usuario, vt.id_clasif_ipj, vt.tipo_ipj, vt.razon_social, vt.id_tramite Id_Tramite_Suac, vt.id_legajo,
        nvl(e.cuit, (select l.cuit from ipj.t_legajos l where l.id_legajo = vt.id_legajo)) cuit,
        to_char(e.fec_resolucion, 'dd/mm/rrrr') fec_resolucion, e.matricula_version,
        ipj.varios.fc_numero_matricula(nvl(e.matricula, (select l.nro_ficha from ipj.t_legajos l where l.id_legajo = vt.id_legajo))) matricula,
        ipj.varios.fc_letra_matricula(nvl(e.matricula, (select l.nro_ficha from ipj.t_legajos l where l.id_legajo = vt.id_legajo))) letra,
        ipj.tramites.fc_es_tram_digital(vt.id_tramite_ipj) Es_Digital,
        ( select count(1)
          from ipj.t_ol_entidades eo
          where eo.codigo_online = vt.codigo_online
            and (eo.id_tipo_tramite_ol = 20 and eo.id_sub_tramite_ol = 5) OR (eo.id_tipo_tramite_ol = 52 and eo.id_sub_tramite_ol = 11)
        ) Express, vt.fecha_ini_suac,
        vt.codigo_online,
        (select o.cod_seguridad from ipj.t_ol_entidades o where o.codigo_online = vt.codigo_online) cod_seguridad,
        IPJ.TRAMITES.FC_Datos_Adic_Firma(vt.id_tramite_ipj) Dato_Adicional,
        ipj.tramites.FC_Buscar_Fecha_Inf(vt.id_tramite_ipj, vt.id_tramiteipj_accion, vt.id_tipo_accion) Fecha_Emision,
        nvl(e.id_tipo_entidad, (select id_tipo_entidad from ipj.t_legajos l where l.id_legajo = vt.id_legajo)) id_tipo_entidad,
        ( select a.nro_resolucion_dig || '"' || a.id_prot_dig || '"' || '/' || SUBSTR(a.fec_resolucion_dig, 7)
          from ipj.t_tramitesipj_acciones a
          where
            a.id_tramite_ipj = vt.id_tramite_ipj and
            a.id_tramiteipj_accion = vt.id_tramiteipj_accion
        ) Resolucion
      from ipj.vt_tramite_tecn_ipj vt left join ipj.t_entidades e on e.id_tramite_ipj = vt.id_tramite_ipj and e.id_legajo = vt.id_legajo
      where
        vt.clase = 2 and -- solo acciones
        vt.id_estado_accion = ipj.types.c_Estados_Firma and -- Pendientes de Firma
        vt.n_ubicacion_origen in (select u.n_ubicacion from ipj.t_grupos_trab_ubicacion tu join ipj.t_ubicaciones u  on tu.id_ubicacion = u.id_ubicacion where tu.cuil = p_cuil_usuario) and
        (nvl(p_id_clasif_ipj, 0) = 0 or vt.id_clasif_ipj = p_id_clasif_ipj)
      order by Express DESC, vt.fecha_ini_suac ASC;

  END SP_Traer_Pendientes_Firma;

  PROCEDURE SP_Rechazar_Firma (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_id_tramiteIpj_Accion in number,
    p_Cuil in varchar2,
    p_Fecha_Rechazo in varchar2,
    p_Observacion IN VARCHAR2)
  IS
  /* Si se rechaza la firma de una acci�n, se devuelve al estado en PROCESO.
    Las acciones que se pasan a firmar, no cambian de resolutor, por lo tanto al
    volver a pedniente ya esta asignado correctamente.
  */
    v_row_accion IPJ.T_TRAMITESIPJ_ACCIONES%rowtype;
    v_id_pagina number;
    v_Usuario VARCHAR2(200);
    v_Observacion VARCHAR2(500);
    v_Id_Ubicacion ipj.t_tramitesipj.id_ubicacion%TYPE;
  BEGIN
    -- busco la acci�n firmada
    select * into v_row_accion
    from ipj.t_tramitesipj_acciones
    where
      id_tramite_ipj = p_id_tramite_ipj and
      id_tramiteipj_accion = p_id_tramiteipj_accion;

    -- busco el tipo de pagina de la accion
    select id_pagina into v_id_pagina
    from ipj.t_tipos_accionesipj
    where
      id_tipo_accion = v_row_accion.id_tipo_accion;

    --Item11396: [Firma Digital] - Incorporar motivo de Rechazo
    -- busco el usuario que rechaz� la firma
    SELECT u.descripcion
      INTO v_Usuario
      FROM ipj.t_usuarios u
     WHERE u.cuil_usuario = p_Cuil;

    -- busco la ubicaci�n del tr�mite para crear la alerta
    SELECT t.id_ubicacion
      INTO v_Id_Ubicacion
      FROM ipj.t_tramitesipj t
     WHERE t.id_tramite_ipj = p_Id_Tramite_Ipj;

    -- concateno fecha, usuario y observaci�n
    v_Observacion := p_Fecha_Rechazo || ' - ' || v_Usuario || ' - ' || p_Observacion;

    IPJ.TRAMITES.SP_Guardar_TramiteIPJ_Acc (
      o_rdo => o_rdo,
      o_tipo_mensaje => o_tipo_mensaje,
      p_Id_Tramite_Ipj => p_id_tramite_ipj,
      p_id_tramiteIpj_Accion => p_id_tramiteipj_accion,
      p_Id_Tipo_Accion => v_row_accion.id_tipo_Accion,
      p_id_Integrante => v_row_accion.id_integrante,
      p_DNI_Integrante => null,
      p_id_legajo => v_row_accion.id_legajo,
      p_CUIT_persjur => null,
      p_id_estado => IPJ.TYPES.C_ESTADOS_PROCESO ,
      p_Cuil_Usuario => v_row_accion.cuil_usuario ,
      p_Obs_Rubrica => v_row_accion.obs_rubrica,
      p_Observacion => v_Observacion,
      p_id_fondo_comercio => v_row_accion.id_fondo_comercio,
      p_fec_veeduria => v_row_accion.fec_veeduria,
      p_id_documento => v_row_accion.id_documento,
      p_n_documento => v_row_accion.n_documento,
      p_id_pagina => v_id_pagina
    );

    if o_tipo_mensaje = IPJ.TYPES.C_TIPO_MENS_OK then
      UPDATE ipj.t_tramitesipj t
         SET t.observacion = substr(v_Observacion || chr(13) || t.observacion, 1, 2000)
       WHERE t.id_tramite_ipj = p_id_tramite_ipj;
      commit;

      --11403: [Firma Digital] - [SG] - Destacar tr�mites con documentos rechazados desde la App Firma Digital
      IPJ.TRAMITES.SP_Crear_Alerta(
        o_rdo => o_rdo,
        o_tipo_mensaje => o_tipo_mensaje,
        p_Id_Tramite_Ipj => p_id_tramite_ipj,
        p_Id_Tramiteipj_Accion => p_id_tramiteipj_accion,
        p_Id_Tipo_Accion => v_row_accion.id_tipo_Accion,
        p_Fec_Alerta => SYSDATE,
        p_Id_Tipo_Alerta => 1,
        p_Id_Estado_Alerta => 1,
        p_Id_Ubicacion => v_Id_Ubicacion,
        p_Cuil_Usr_Origen => p_Cuil
      );

    else
      rollback;
    end if;
  EXCEPTION
     when OTHERS then
        o_rdo := 'SP_Rechazar_Firma: ' || To_Char(SQLCODE) || '-' || SQLERRM;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Rechazar_Firma;

  PROCEDURE SP_Finalizar_Firma (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_id_tramiteIpj_Accion in number)
  IS
  /* Si el tr�mite tiene m�s acciones pendientes, solo se cierra la acci�n de firma;
  si no tiene m�s pendientes se cierra la accion y se pasa a MESA SUAC el tramite.
  */
    v_row_accion IPJ.T_TRAMITESIPJ_ACCIONES%ROWTYPE;
    v_Row_Ubicacion IPJ.T_UBICACIONES%ROWTYPE;
    v_Row_Tramite IPJ.T_TRAMITESIPJ%ROWTYPE;
    v_rdo varchar2(2000);
    v_tipo_mensaje number;
    v_acciones_pendientes number;
    v_out_tramite number;
    v_id_clasif number;
    v_id_pagina number;
    v_id_estado_accion number;
    v_id_estado_tramite number;
    v_estado_entidad char(1);
    v_origen_entidad number;
    v_id_ult_tramite number;
    v_id_ult_accion number;
    v_id_tipo_ent_ult number;
    v_id_tipo_ent_legajo number;
    v_id_tramite_ipj_res number;
  BEGIN
    -- Como es un proceso de Firmas, verifico si se cargo una persona f�sica sin acciones desde SUAC, y la elimino
    -- Esto permite luego finalizar el tr�mite.
    DBMS_OUTPUT.PUT_LINE(' - SP_Finalizar_Firma (1): Elimino integrantes del tr�mite de empresas');
    delete ipj.t_tramitesipj_integrante
    where
      id_tramite_ipj = p_id_tramite_ipj and
      not exists ( select *
                       from ipj.t_tramitesipj_acciones  tacc join ipj.t_tramitesipj_integrante ti
                          on tacc.id_tramite_ipj = ti.id_tramite_ipj and tacc.id_integrante = ti.id_integrante
                       where
                         tacc.id_tramite_ipj = p_id_tramite_ipj);

    -- busco la acci�n firmada
    DBMS_OUTPUT.PUT_LINE(' - SP_Finalizar_Firma (2): Busco los datos de la acci�n');
    select * into v_row_accion
    from ipj.t_tramitesipj_acciones
    where
      id_tramite_ipj = p_id_tramite_ipj and
      id_tramiteipj_accion = p_id_tramiteipj_accion;

    -- busco el tipo de pagina de la accion
    DBMS_OUTPUT.PUT_LINE(' - SP_Finalizar_Firma (3): Busco las p�gina de la acci�n');
    select id_pagina into v_id_pagina
    from ipj.t_tipos_accionesipj
    where
      id_tipo_accion = v_row_accion.id_tipo_accion;

    -- Si con acciones de resoluciones irregulares, cierro IRREGULAR sino OK
    if v_row_accion.id_tipo_accion in (146, 147, 154, 157, 161, 176) then
      v_id_estado_accion := 203;
      v_id_estado_tramite := 203;
    else
      -- Si son acciones de Rechazo, los mando al estado TRAMITE RECHAZADO
      if v_row_accion.id_tipo_accion in (156, 155, 153, 159, 160, 175) then
        v_id_estado_accion := 204;
        v_id_estado_tramite := 204;
      else
        -- Si no es ni rechazo ni irregular, cierro OK
        v_id_estado_accion := IPJ.TYPES.c_Estados_Cerrado;
        v_id_estado_tramite := Ipj.Types.c_Estados_Completado;
      end if;
    end if;

    DBMS_OUTPUT.PUT_LINE(' - SP_Finalizar_Firma (4): Llamo a cerrar la acci�n');
    IPJ.TRAMITES.SP_Guardar_TramiteIPJ_Acc (
      o_rdo => v_rdo,
      o_tipo_mensaje => v_tipo_mensaje,
      p_Id_Tramite_Ipj => p_id_tramite_ipj,
      p_id_tramiteIpj_Accion => p_id_tramiteipj_accion,
      p_Id_Tipo_Accion => v_row_accion.id_tipo_Accion,
      p_id_Integrante => v_row_accion.id_integrante,
      p_DNI_Integrante => null,
      p_id_legajo => v_row_accion.id_legajo,
      p_CUIT_persjur => null,
      p_id_estado => v_id_estado_accion,
      p_Cuil_Usuario => v_row_accion.cuil_usuario ,
      p_Obs_Rubrica => v_row_accion.obs_rubrica,
      p_Observacion => null,
      p_id_fondo_comercio => v_row_accion.id_fondo_comercio,
      p_fec_veeduria => v_row_accion.fec_veeduria,
      p_id_documento => v_row_accion.id_documento,
      p_n_documento => v_row_accion.n_documento,
      p_id_pagina => v_id_pagina
    );
    DBMS_OUTPUT.PUT_LINE('          - v_rdo: ' || v_rdo);
    DBMS_OUTPUT.PUT_LINE('          - v_tipo_mensaje: ' || to_char(v_tipo_mensaje));

    if v_tipo_mensaje = IPJ.TYPES.C_TIPO_MENS_OK then
      -- Verifico cuantas acciones pendientes quedan
      select count(1) into v_acciones_pendientes
      from ipj.t_tramitesipj_acciones
      where
        id_tramite_ipj = p_id_tramite_ipj and
        id_estado < IPJ.TYPES.C_ESTADOS_COMPLETADO;
      DBMS_OUTPUT.PUT_LINE(' - SP_Finalizar_Firma (5): Acciones Pendinetes = ' || to_char(v_acciones_pendientes));

      -- Si no hay pendientes, derivo el tr�mite a MESA SUAC
      if v_acciones_pendientes = 0 then
        -- Busco el area de Mesa SUAC
        select * into v_Row_Ubicacion
        from IPJ.t_Ubicaciones
        where
          id_ubicacion = IPJ.TYPES.C_AREA_MESASUAC;

        -- Busco el tr�mite
        select * into v_Row_Tramite
        from IPJ.t_tramitesipj
        where
          Id_Tramite_Ipj = p_Id_Tramite_Ipj;

        -- busco la primer clasificacion del area MESA SUAC
        select id_clasif_ipj into v_id_clasif
        from IPJ.t_Tipos_Clasif_Ipj
        where
          id_ubicacion = IPJ.TYPES.C_AREA_MESASUAC and
          rownum = 1;

        DBMS_OUTPUT.PUT_LINE(' - SP_Finalizar_Firma (6): Llama a cerrar el tr�mite');
        IPJ.TRAMITES.SP_GUARDAR_TRAMITE(
          o_Id_Tramite_Ipj => v_out_tramite,
          o_rdo => v_rdo,
          o_tipo_mensaje => v_tipo_mensaje,
          v_Id_Tramite_Ipj => p_id_tramite_ipj,
          v_id_tramite => v_Row_Tramite.id_tramite,
          v_id_Clasif_IPJ => v_id_clasif,
          v_id_Grupo => (case when nvl(v_Row_Ubicacion.id_grupo, 0) > 0 then v_Row_Ubicacion.id_grupo else null end),
          v_cuil_ult_estado => (case when nvl(v_Row_Ubicacion.id_grupo, 0) = 0 then v_Row_Ubicacion.cuil_usuario else null end),
          v_id_estado => v_id_estado_tramite,
          v_cuil_creador => v_Row_Tramite.cuil_creador,
          v_observacion => null,
          v_id_Ubicacion => IPJ.TYPES.C_AREA_MESASUAC,
          v_urgente => v_Row_Tramite.urgente,
          p_sticker => v_Row_Tramite.sticker,
          p_expediente => v_Row_Tramite.expediente,
          p_simple_tramite => 'N'
        );
        DBMS_OUTPUT.PUT_LINE('          - v_rdo: ' || v_rdo);
        DBMS_OUTPUT.PUT_LINE('          - v_tipo_mensaje: ' || to_char(v_tipo_mensaje));

         if v_tipo_mensaje = IPJ.TYPES.C_TIPO_MENS_OK then
           commit;
           DBMS_OUTPUT.PUT_LINE(' ----- Commit 1');
         else
           rollback;
           DBMS_OUTPUT.PUT_LINE(' ----- Rollbak 1');
         end if;
      else
        -- Guarda el cierre de accion
        commit;
        DBMS_OUTPUT.PUT_LINE(' ----- Commit 2');
      end if;
    else
      rollback;
      DBMS_OUTPUT.PUT_LINE(' ----- Rollbak 1');
    end if;

    -- Si nada fallo, y es una acci�n de rechazo
    if v_tipo_mensaje = IPJ.TYPES.C_TIPO_MENS_OK and
      v_row_accion.id_tipo_accion in (157, 156, 155, 154, 153, 147, 146, 159, 160, 161, 175, 176) then
      DBMS_OUTPUT.PUT_LINE(' - SP_Finalizar_Firma (7): Si nada fallo, es contitucion y acci�n de Baja');

      -- Si es un tr�mite de CONSTITUCION, se da de baja el legajo
      if IPJ.TRAMITES.FC_Es_Tram_Const (p_id_tramite_ipj) > 0 then
        update ipj.t_legajos
        set fecha_baja_entidad = to_date(sysdate, 'dd/mm/rrrr')
        where
          id_legajo = v_row_accion.id_legajo;

        -- Configuro el estado y origen de la entidad
        if v_id_estado_accion in (203, 204) then
          v_estado_entidad := 'R'; -- Rechazada
          v_origen_entidad := 21; -- Rechazada
        else
          v_estado_entidad := 'S';-- Desistida
          v_origen_entidad := 20;-- Desistida
        end if;

        DBMS_OUTPUT.PUT_LINE(' - SP_Finalizar_Firma (8): Actualiza la baja en la entidad');
        update ipj.t_entidades
        set
          Baja_Temporal = to_date(sysdate, 'dd/mm/rrrr'),
          id_estado_entidad = v_estado_entidad,
          id_tipo_origen = v_origen_entidad,
          obs_cierre = 'Tr�mite rechazado o irregular.'
        where
          id_tramite_ipj = p_id_tramite_ipj and
          id_legajo = v_row_accion.id_legajo;

        -- Si es Const. rechazada o Desistida por irregular y la reserva no venci�
        --se extiende la fecha 1 mes m�s
        BEGIN
          SELECT id_tramite_ipj
            INTO v_id_tramite_ipj_res
            FROM ipj.t_tramitesipj t
           WHERE sticker = (SELECT codigo_reserva
                              FROM ipj.t_ol_entidades e
                             WHERE e.codigo_online = v_Row_Tramite.codigo_online);

          IF v_id_tramite_ipj_res IS NOT NULL THEN
            UPDATE ipj.t_tramitesipj_acc_reserva
               SET fec_vence = trunc(SYSDATE) + to_number(IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('DIAS_VENC_RES_RCH'))
             WHERE id_tramite_ipj = v_id_tramite_ipj_res;
          END IF;

        EXCEPTION
          WHEN no_data_found THEN
            NULL;
          WHEN too_many_rows THEN
            NULL;
          WHEN OTHERS THEN
            NULL;
        END;

        -- Actualizo los datos en Gobierno, si es Const. rechazada o Desistida
        IPJ.ENTIDAD_PERSJUR.SP_ACTUALIZAR_ENT_GOB(
          o_rdo => v_rdo,
          o_tipo_mensaje => v_tipo_mensaje,
          p_Id_Tramite_Ipj => p_id_tramite_ipj,
          p_id_legajo => v_row_accion.id_legajo
        );
      else
        -- Cualquier rechazo, pongo la versi�n en -1
        update ipj.t_entidades
        set
          matricula_version = -1
        where
          id_tramite_ipj = p_id_tramite_ipj and
          id_legajo = v_row_accion.id_legajo;
      end if;
    end if;

    -- Si nada fallo, y es un tr�mite de TRANSFORMACION y es una accion de rechazo se vuelve atras el cambio
    if v_tipo_mensaje = IPJ.TYPES.C_TIPO_MENS_OK and IPJ.TRAMITES.FC_Es_Tram_Transf (p_id_tramite_ipj) > 0 and
        v_row_accion.id_tipo_accion in (146, 147, 154, 157, 161, 156, 155, 153, 159, 160, 175, 176) then
      DBMS_OUTPUT.PUT_LINE(' - SP_Finalizar_Firma (8,5): Vuelve atras una transformaci�n.');
      -- Busco el �ltimo tr�mite, a ver su tipo de entidad
      IPJ.ENTIDAD_PERSJUR.SP_Buscar_Entidad_Tramite(
        o_Id_Tramite_Ipj => v_id_ult_tramite,
        o_id_tramiteipj_accion => v_id_ult_accion,
        p_id_legajo => v_row_accion.id_legajo,
        p_Abiertos => 'N');

      -- Busco el ultimo tipo de entidad actual del legajo
      select id_tipo_entidad into v_id_tipo_ent_legajo
      from ipj.t_legajos
      where
        id_legajo = v_row_accion.id_legajo;

     -- Busco el tipo de entidad actual del ultimo tramite
      begin
        select id_tipo_entidad into v_id_tipo_ent_ult
        from ipj.t_entidades
        where
          id_legajo = v_row_accion.id_legajo and
          id_tramite_ipj = v_id_ult_tramite;
      exception
        -- Si no hay datos, lo dejo como esta
        when NO_DATA_FOUND then
          v_id_tipo_ent_ult := v_id_tipo_ent_legajo;
      end;

      -- Si el ultimo tr�mites es ditinto al legajo, lo vulevo atras
      if nvl(v_id_tipo_ent_ult, 0) > 0 and v_id_tipo_ent_ult <> v_id_tipo_ent_legajo then
        update ipj.t_legajos
        set id_tipo_entidad = v_id_tipo_ent_ult
        where
          id_legajo = v_row_accion.id_legajo;
      end if;
    end if;

    DBMS_OUTPUT.PUT_LINE(' - SP_Finalizar_Firma (9): Finaliza SP');
     if v_tipo_mensaje = IPJ.TYPES.C_TIPO_MENS_OK then
       commit;
       DBMS_OUTPUT.PUT_LINE(' ----- Commit 3');
     else
       rollback;
       DBMS_OUTPUT.PUT_LINE(' ----- Rollbak 3');
     end if;

    o_rdo := v_rdo;
    o_tipo_mensaje := v_tipo_mensaje;
  EXCEPTION
     when OTHERS then
        o_rdo := 'SP_Finalizar_Firma: ' || To_Char(SQLCODE) || '-' || SQLERRM;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
        rollback;
  END SP_Finalizar_Firma;

  PROCEDURE SP_Traer_Pendientes_Boe (
    o_Cursor out types.cursorType,
    p_cuil_usuario in varchar2)
  IS
  CURSOR c_Edictos IS
    SELECT v.NRO_EDICTO, v.N_RESULT fecha_publica, v.ID_TIPO_LLAMADA, MAX(v.FEC_LLAMADA) fec_llamada
      FROM boletin_oficial.VT_LOGS_LLAMADAS_IPJ v
      JOIN ipj.t_entidades_edicto_boe boe ON v.NRO_EDICTO = boe.nro_edicto
      JOIN ipj.t_tramitesipj_acciones acc ON acc.id_tramite_ipj = boe.id_tramite_ipj
       AND acc.id_legajo = boe.id_legajo
       AND acc.id_tipo_accion IN (to_number(IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('ACC_EDICTO_BOE')),to_number(IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('ACC_EDICTO_BOE_RP')))
     WHERE
           BOE.ID_ESTADO_BOE < 3 and -- Edicto Pendiente
           v.ID_TIPO_LLAMADA IN (11,21) AND -- Respuesta de Boe OK o Cancel
           acc.id_estado < IPJ.TYPES.C_ESTADOS_COMPLETADO AND -- Tramite Pendiente
           acc.cuil_usuario = p_cuil_usuario
  GROUP BY v.NRO_EDICTO, v.N_RESULT, boe.fecha_publica, v.ID_TIPO_LLAMADA;

  v_rdo VARCHAR2(100);
  v_tipo_mensaje NUMBER;
  v_fecha_publica DATE;

  BEGIN
  /*************************************************
     Lista los ultimos edictos de BOE de cada tr�mite que corresponden al usuario y
     que tienen alguna respuesta de BOE (Cancelado o Finalizado)
 *************************************************/
    --Verifico si existen edictos con errores por el lado de BOE
    FOR r IN c_Edictos LOOP
      IF r.id_tipo_llamada = 21 THEN --Si estado en BOE es publicado
        PCK_TRAMITES_BOE.SP_BOE_CONTINGENCIA (v_rdo, v_tipo_mensaje, r.nro_edicto, to_date(r.fec_llamada,'dd/mm/rrrr'), r.id_tipo_llamada);
      ELSIF r.id_tipo_llamada = 11 THEN -- Si estado en BOE es cancelado

        BEGIN
          v_fecha_publica := to_date(SUBSTR(r.fecha_publica,19,10),'dd/mm/rrrr');
        EXCEPTION
          WHEN OTHERS THEN
            v_fecha_publica := NULL;
        END;

        PCK_TRAMITES_BOE.SP_BOE_CONTINGENCIA (v_rdo, v_tipo_mensaje, r.nro_edicto, v_fecha_publica, r.id_tipo_llamada);

      END IF;
    END LOOP;

    OPEN o_Cursor FOR
     select distinct boe.id_tramite_ipj, boe.nro_edicto, to_char(boe.fecha_cancela, 'dd/mm/rrrr') fecha_cancela,
        to_char(boe.fecha_publica, 'dd/mm/rrrr') fecha_publica, boe.enviar_mail ,boe.id_estado_boe, boe.id_legajo,
        to_char(boe.fecha_envio, 'dd/mm/rrrr') fecha_envio,
        (select eb.n_estado_boe from ipj.t_tipos_estado_boe eb where eb.id_estado_boe = boe.id_estado_boe) n_estado_boe,
        (select expediente from ipj.t_tramitesipj tr where tr.id_tramite_ipj = boe.id_tramite_ipj) Expediente
     from ipj.t_entidades_edicto_boe boe join
       --El ultimo edicto pendiente de BOE, si no existe ninguno pendiente en IPJ
       ( select acc.id_tramite_ipj, acc.id_legajo, acc.cuil_usuario, max(nro_edicto) nro_edicto
         from ipj.t_tramitesipj_acciones acc join ipj.t_entidades_edicto_boe boe
           on acc.id_tramite_ipj = boe.id_tramite_ipj and acc.id_legajo = boe.id_legajo
               and acc.id_tipo_accion IN (to_number(IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('ACC_EDICTO_BOE')),to_number(IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('ACC_EDICTO_BOE_RP')))
         where
           acc.id_estado < IPJ.TYPES.C_ESTADOS_COMPLETADO and
           acc.cuil_usuario = p_cuil_usuario and
           boe.id_estado_boe in (3, 4) and
          not exists
           ( select *
             from ipj.t_entidades_edicto_boe b
             where
               b.id_tramite_ipj = boe.id_tramite_ipj and
               b.id_legajo = boe.id_legajo and
               nvl(b.id_estado_boe,0) < 3 -- Pendiente IPJ
           )
       group by acc.id_tramite_ipj, acc.id_legajo, acc.cuil_usuario
       ) tmp
       on boe.id_tramite_ipj = tmp.id_tramite_ipj and boe.id_legajo = tmp.id_legajo and boe.nro_edicto = tmp.nro_edicto ;


  END SP_Traer_Pendientes_Boe;

  PROCEDURE SP_Buscar_Reemp(
    o_Cursor OUT types.cursorType,
    p_cuit in varchar2)
  IS
  /****************************************************
     Muestra si una empresa hizo un reempadronamiento en el Portal, y el estado del mismo
  *****************************************************/
  BEGIN
       OPEN o_Cursor FOR
         select o.cuit, o.denominacion, to_char(o.fecha_creacion, 'dd/mm/rrrr') fecha_creacion,
           o.id_estado,
           (case when o.id_tipo_tramite_ol = 18 then (select n_estado from IPJ.T_ESTADOS e where e.id_estado = o.id_estado)
             else
               (select t.n_tipo_tramite_ol from ipj.t_tipos_tramites_ol t where t.id_tipo_tramite_ol = o.id_tipo_tramite_ol) || ' - ' ||
               (select n_estado from IPJ.T_ESTADOS e where e.id_estado = o.id_estado)
           end) n_estado
         from ipj.t_ol_entidades o
         where
           o.id_tipo_tramite_ol in (1, 18, 20) and -- Const SA, Reempad, Const SAS
           o.cuit = replace(p_cuit, '-', '');

  END SP_Buscar_Reemp;

  PROCEDURE SP_Guardar_Accion_Resolucion (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_id_tramiteIpj_Accion in number,
    p_id_prot_jur in varchar2,
    p_nro_resolucion in number,
    p_fec_resolucion in varchar2 -- Es Fecha
  )
  IS
  /*
    Para una acci�n de Jur�dico, guarda la resoluci�n en la accion y ademas:
    - Actualiza los protocolos de jur�dico
    - Actualiza la resolucion en la entidad
    - Actualiza las fechas de las autoridades
  */
    v_id_legajo number;
    v_id_integrante number;
    v_hay_datos number;
    v_cursor_organismo ipj.types.cursortype;
    v_id_organismo number;
    v_duracion number;
    v_tipo_duracion number;
    v_cierre_ejerc varchar2(10);
    v_id_tipo_accion number;
    v_fin_febrero char(1);
  BEGIN
    if IPJ.Types.c_habilitar_log_SP = 'S' then
      IPJ.VARIOS.SP_GUARDAR_LOG(
        p_METODO => 'SP_Guardar_Accion_Resolucion',
        p_NIVEL => '',
        p_ORIGEN => 'DB',
        p_USR_LOG  => 'DB',
        p_USR_WINDOWS => 'Sistema',
        p_IP_PC  => null,
        p_EXCEPCION =>
           'Id Tramite IPJ = ' || to_char(p_Id_Tramite_Ipj)
           || ' / Id TramiteIPJ Accion = ' || to_char(p_id_tramiteIpj_Accion)
           || ' / Id Prot Jur = ' ||p_id_prot_jur
           || ' / Nro. resolucion = ' || to_char(p_nro_resolucion)
           || ' / Fec Resolucion = ' || p_fec_resolucion
       );
    end if;

    if p_fec_resolucion is null or IPJ.VARIOS.Valida_Fecha(p_fec_resolucion) = 'N' then
      o_rdo := IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT') ;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      return;
    end if;

    -- Busco el legajo asociado a la accion
    select id_legajo, id_integrante, id_tipo_accion into v_id_legajo, v_id_integrante, v_id_tipo_accion
    from ipj.t_tramitesipj_acciones
    where
      Id_Tramite_Ipj = p_Id_Tramite_Ipj and
      id_tramiteipj_accion = p_id_tramiteipj_accion;

    -- Para las inscripciones de ACyF, se actualizan varios datos desde la resolucion
    if v_id_tipo_accion = 42 then
      -- Verifico que existan datos cargados para la entidad
      select count(1) into v_hay_datos
      from ipj.t_entidades
      where
        id_tramite_ipj = p_id_tramite_ipj and
        id_legajo = v_id_legajo;

      -- Si no hay datos,
      if v_hay_datos = 0 then
        o_rdo := 'A la empresa no se le cargaron datos, verifique con el �rea correspondiente.';
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
        return;
      end if;

      -- Busco el cierre de ejercicio, si no viene no contin�a
      select to_char(cierre_ejercicio, 'dd/mm/rrrr'), cierre_fin_mes into v_cierre_ejerc, v_fin_febrero
      from ipj.t_entidades
      where
        id_tramite_ipj = p_id_tramite_ipj and
        id_legajo = v_id_legajo;

      if v_cierre_ejerc is null and nvl(v_fin_febrero, 'N') = 'N' then
        o_rdo := 'La empresa no cuenta con un cierre de ejercicio definido.';
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
        return;
      end if;

      -- Si es una empresa, corrijo sus datos
      if nvl(v_id_legajo, 0) > 0 then
        -- Actualizo los datos de la inscripci�n
        update ipj.t_entidades
        set
          fec_inscripcion = to_date(p_fec_resolucion, 'dd/mm/rrrr'),
          nro_resolucion = to_char(p_nro_resolucion) || ' "' || p_id_prot_jur || '"/' || to_char(to_date(p_fec_resolucion, 'dd/mm/rrrr'), 'yy'),
          fec_resolucion = to_date(p_fec_resolucion, 'dd/mm/rrrr'),
          tipo_vigencia = 1 -- Desde Fecha Resoluci�n
        where
          id_legajo = v_id_legajo and
          id_tramite_ipj = p_id_tramite_ipj;


        -- Si es el �ltimo d�a de febrero, tomo el 28 para el calculo de los plazos
        if nvl(v_fin_febrero, 'N') = 'S' then
          v_cierre_ejerc := '28/02/' || to_char(sysdate, 'rrrr');
        end if;

        -- Para cada organismo, actualizo las fechas segun su definicion
        OPEN v_cursor_organismo FOR
          select id_tipo_organismo,cant_meses, id_tipo_duracion
          from ipj.t_entidades_organismo
          where
            id_legajo = v_id_legajo;

        LOOP
          fetch v_cursor_organismo into v_id_organismo, v_duracion, v_tipo_duracion;
          EXIT WHEN v_cursor_organismo%NOTFOUND or v_cursor_organismo%NOTFOUND is null;

          -- Actualizo las autoridades definidas
          update ipj.t_entidades_admin
          set
            fecha_inicio = to_date(p_fec_resolucion, 'dd/mm/rrrr'),
            fecha_fin = (case
                                 -- Si es por ejercicios, y mayor al cierre de ejercicio
                                 when v_tipo_duracion = 1 and to_date(p_fec_resolucion, 'dd/mm/rrrr') >= to_date(substr(v_cierre_ejerc, 1, 6) || substr(p_fec_resolucion, 7, 4), 'dd/mm/rrrr') then
                                   to_date(substr(v_cierre_ejerc, 1, 6) || to_char(to_number(substr(p_fec_resolucion, 7, 4)) + v_duracion), 'dd/mm/rrrr')
                                 -- Si es por ejercicios, y menor al cierre de ejercicio
                                 when v_tipo_duracion = 1 and to_date(p_fec_resolucion, 'dd/mm/rrrr') < to_date(substr(v_cierre_ejerc, 1, 6) || substr(p_fec_resolucion, 7, 4), 'dd/mm/rrrr') then
                                   to_date(substr(v_cierre_ejerc, 1, 6) || to_char(to_number(substr(p_fec_resolucion, 7, 4)) + v_duracion-1), 'dd/mm/rrrr')
                                 -- Si es por A�os
                                 when v_tipo_duracion = 2 then
                                   to_date(substr(p_fec_resolucion, 1, 6) || to_char(to_number(substr(p_fec_resolucion, 7, 4)) + v_duracion), 'dd/mm/rrrr')
                              end)
          where
            id_tramite_ipj = p_id_tramite_ipj and
            id_legajo = v_id_legajo and
            id_tipo_organismo = v_id_organismo;

          -- Actualizo los fiscalizadores definidos
          update ipj.t_entidades_sindico
          set
            fecha_alta = to_date(p_fec_resolucion, 'dd/mm/rrrr'),
            fecha_baja = (case
                                 -- Si es por ejercicios, y mayor al cierre de ejercicio
                                 when v_tipo_duracion = 1 and to_date(p_fec_resolucion, 'dd/mm/rrrr') >= to_date(substr(v_cierre_ejerc, 1, 6) || substr(p_fec_resolucion, 7, 4), 'dd/mm/rrrr') then
                                   to_date(substr(v_cierre_ejerc, 1, 6) || to_char(to_number(substr(p_fec_resolucion, 7, 4)) + v_duracion), 'dd/mm/rrrr')
                                 -- Si es por ejercicios, y menor al cierre de ejercicio
                                 when v_tipo_duracion = 1 and to_date(p_fec_resolucion, 'dd/mm/rrrr') < to_date(substr(v_cierre_ejerc, 1, 6) || substr(p_fec_resolucion, 7, 4), 'dd/mm/rrrr') then
                                   to_date(substr(v_cierre_ejerc, 1, 6) || to_char(to_number(substr(p_fec_resolucion, 7, 4)) + v_duracion-1), 'dd/mm/rrrr')
                                 -- Si es por A�os
                                 when v_tipo_duracion = 2 then
                                   to_date(substr(p_fec_resolucion, 1, 6) || to_char(to_number(substr(p_fec_resolucion, 7, 4)) + v_duracion), 'dd/mm/rrrr')
                              end)
          where
            id_tramite_ipj = p_id_tramite_ipj and
            id_legajo = v_id_legajo and
            id_tipo_integrante in (select id_tipo_integrante from ipj.t_tipos_integrante where id_tipo_organismo =  v_id_organismo);

        END LOOP;
        CLOSE v_cursor_organismo;

         -- Actualizo Historial de Autoridades
         UPDATE
           ( SELECT
               -- Campos Admin
               ad.fecha_inicio, ad.fecha_fin,
               -- Campos Admin Hist
               adh.fecha_inicio fecha_inicio_hist, adh.fecha_fin fecha_fin_hist
             FROM Ipj.T_Entidades_admin ad join Ipj.T_Entidades_admin_hist adh
               on ad.id_tramite_ipj = adh.id_tramite_ipj and
                 ad.id_legajo = adh.id_legajo and
                 ad.id_admin = adh.id_admin
             WHERE
               ad.id_tramite_ipj = p_id_tramite_ipj and
               ad.id_legajo = v_id_legajo
           )
         SET
           fecha_inicio_hist = fecha_inicio,
           fecha_fin_hist = fecha_fin;

        -- Actualizo Historial de Fiscalizadores
         UPDATE
           ( SELECT
               -- Campos Admin
               ad.fecha_alta, ad.fecha_baja,
               -- Campos Admin Hist
               adh.fecha_alta fecha_alta_hist, adh.fecha_baja fecha_baja_hist
             FROM Ipj.T_Entidades_sindico ad join Ipj.T_Entidades_sindico_hist adh
               on ad.id_tramite_ipj = adh.id_tramite_ipj and
                 ad.id_legajo = adh.id_legajo and
                 ad.id_entidad_sindico = adh.id_entidad_sindico
             WHERE
               ad.id_tramite_ipj = p_id_tramite_ipj and
               ad.id_legajo = v_id_legajo
           )
         SET
           fecha_alta_hist = fecha_alta,
           fecha_baja_hist = fecha_baja;
      end if;
    end if;

    -- Actualiza los datos de la accion de Jur�dico
    update IPJ.t_TramitesIpj_Acciones
    set
      id_prot_jur = p_id_prot_jur,
      nro_resolucion = p_nro_resolucion,
      fec_resolucion = to_date(p_fec_resolucion, 'dd/mm/rrrr')
    where
      Id_Tramite_Ipj = p_Id_Tramite_Ipj and
      id_tramiteipj_accion = p_id_tramiteipj_accion;

    -- Actualizo el protocolo de Jur�dico
    update ipj.t_protocolos_juridico
    set numerador = (case when numerador >= p_nro_resolucion then numerador else p_nro_resolucion end)
    where
      id_prot_jur = p_id_prot_jur;

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    commit;
  EXCEPTION
     when OTHERS then
        o_rdo := 'SP_Guardar_Accion_Resolucion: ' || To_Char(SQLCODE) || '-' || SQLERRM;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
        rollback;
  END SP_Guardar_Accion_Resolucion;


  PROCEDURE SP_Act_Datos_Fideicomiso(
    o_rdo out nvarchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj IN number,
    p_Id_Legajo IN number,
    p_Codigo_Online IN number)
  IS
  /**********************************************************
    Asocia el tr�mite IPJ al c�digo online.
    Baja los datos del Tr�mite Online al tr�mite de Gesti�n.
    Marca el tr�mite Online como Finalizado (igual que si lo hubiese completado un Escribano).
  **********************************************************/
    v_Exist_Cod_OL number;
    v_Exist_TramiteIPJ NUMBER;
    v_Exist_TramiteSUAC NUMBER;
    v_sticker_suac VARCHAR2(50);
    v_id_tipo_tramite_ol ipj.t_ol_entidades.id_tipo_tramite_ol%TYPE;
  BEGIN

    --Verifico que el c�digo online exista, y sea un fideicomiso
    BEGIN
      SELECT 1, oe.id_tipo_tramite_ol
        INTO v_Exist_Cod_OL, v_id_tipo_tramite_ol
        FROM ipj.t_ol_entidades oe
       WHERE oe.codigo_online = p_Codigo_Online
         AND oe.id_tipo_tramite_ol = 19
         AND oe.id_estado BETWEEN IPJ.TYPES.c_Estados_Archivo_Inicial AND IPJ.TYPES.c_Estados_Completado;
    EXCEPTION
      WHEN no_data_found THEN
        o_rdo := 'El C�digo ingresado no existe o no corresponde a un Fideicomiso.';
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
        RETURN;
    END;

    --Verifica que el tr�mite no sea hist�rico
    SELECT COUNT(1)
      INTO v_Exist_TramiteSUAC
      FROM ipj.t_tramitesipj ti
     WHERE ti.id_tramite_ipj = p_Id_Tramite_Ipj
       AND nvl(ti.id_tramite,0) <> 0
       AND rownum < 2;


    --Verifica que no exista un tr�mite en IPJ asociado a dicho c�digo online
    SELECT COUNT(1)
      INTO v_Exist_TramiteIPJ
      FROM ipj.t_tramitesipj ti
     WHERE ti.codigo_online = p_Codigo_Online
       AND rownum < 2;

    IF v_Exist_TramiteIPJ = 0 AND v_Exist_TramiteSUAC <> 0 THEN
      --Baja los datos del Tr�mite Online al tr�mite de Gesti�n
      ipj.pk_tramites_online.SP_Pasar_Datos_Entidad(o_rdo,
                                                    o_tipo_mensaje,
                                                    p_Codigo_Online,
                                                    p_Id_Tramite_Ipj,
                                                    p_Id_Legajo);

      IF o_rdo <> 'OK' THEN
        o_rdo := 'Error al bajar los datos del Tr�mite OnLine al tr�mite de Gesti�n. Consulte con el Administrador de Sistema ('|| o_rdo || ').';
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
        ROLLBACK;
        RETURN;
      END IF;

      --Asocia el tr�mite IPJ al c�digo online
      UPDATE ipj.t_tramitesipj ti
         SET ti.codigo_online = p_Codigo_Online
       WHERE ti.id_tramite_ipj = p_Id_Tramite_Ipj;

      --Marca el tr�mite Online como Finalizado (igual que si lo hubiese completado un Escribano)
      UPDATE IPJ.T_OL_ENTIDADES
         SET id_estado = IPJ.TYPES.c_Estados_Completado,
             Fec_paso_procesado = to_date(sysdate, 'dd/mm/rrrr'),
             fecha_fin = to_date(sysdate, 'dd/mm/rrrr')
       WHERE codigo_online = p_codigo_online;

      COMMIT;
      o_rdo := IPJ.TYPES.C_RESP_OK;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    ELSE
      IF v_Exist_TramiteIPJ <> 0 THEN
        o_rdo := 'El C�digo ingresado ya est� asociado con un tr�mite IPJ.';
      ELSIF v_Exist_TramiteSUAC = 0 THEN
        o_rdo := 'El Tr�mite no existe en SUAC.';
      END IF;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      RETURN;
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      o_rdo := 'SP_Act_Datos_Fideicomiso - '|| o_rdo || '-' || To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Act_Datos_Fideicomiso;

  PROCEDURE SP_Tramites_Inactivos(
    o_Cursor OUT types.cursorType,
    p_id_ubicacion in number
    )
  IS
  /********************************************************************************
     Lista los expedientes inactivos por m�s de 20 d�as dentro de un �rea de IPJ
  *********************************************************************************/
    v_dias_inactividad number;
  BEGIN
    -- Busco la cantidad de d�as de inactividad permitidos
    v_dias_inactividad := to_number(IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('DIAS_PRE_AVISO_ARCHI'));

    OPEN o_Cursor FOR
      select tipo_inactivo, estado_nota, fecha_inicio, nro_expediente, sticker, razon_social, Asunto, id_tramite_ipj,
        id_tramite_suac, Ult_Nota, IPJ.VARIOS.FC_Dias_Laborables(ult_nota, to_date(sysdate, 'dd/mm/rrrr'),'S') Dias_Inactivos,
        (to_date(sysdate, 'dd/mm/rrrr') - ult_nota) Dias_Totales, nota_sistema, Hay_Mail,
        (case when fecha_inicio < to_date('01/01/2017', 'dd/mm/rrrr') then 'S' else 'N' end) Es_Viejo,
        cuil_creador, urgente, simple_tramite, cuil_ult_estado,
        (select vt.id_subtipo_tramite from NUEVOSUAC.VT_TRAMITES_IPJ_DESA vt where vt.id_tramite = tmp.id_tramite_suac) id_subtipo_tramite
      from
        ( select
            CASE WHEN (select max(nvl(fec_modif, fec_cumpl)) from ipj.t_entidades_notas n where n.id_tramite_ipj = tr.id_tramite_ipj and informar_suac = 'N' and nvl(n.id_nota_sistema, 0) <> 1) IS NULL AND (select max(to_date(v_fecha_anexado, 'dd/mm/rrrr')) from nuevosuac.vt_get_tramites_ipj_anexos n where n.v_id_padre = tr.id_tramite) IS NULL THEN 'SUAC'
                      WHEN NVL((select max(nvl(fec_modif, fec_cumpl)) from ipj.t_entidades_notas n where n.id_tramite_ipj = tr.id_tramite_ipj and informar_suac = 'N' and nvl(n.id_nota_sistema, 0) <> 1), tr.fecha_ini_suac) > nvl((select max(to_date(v_fecha_anexado, 'dd/mm/rrrr')) from nuevosuac.vt_get_tramites_ipj_anexos n where n.v_id_padre = tr.id_tramite), tr.fecha_ini_suac) THEN 'OBS'
                      WHEN nvl((select max(nvl(fec_modif, fec_cumpl)) from ipj.t_entidades_notas n where n.id_tramite_ipj = tr.id_tramite_ipj and informar_suac = 'N' and nvl(n.id_nota_sistema, 0) <> 1), tr.fecha_ini_suac) < nvl((select max(to_date(v_fecha_anexado, 'dd/mm/rrrr')) from nuevosuac.vt_get_tramites_ipj_anexos n where n.v_id_padre = tr.id_tramite), tr.fecha_ini_suac) THEN 'ANEX'
                      WHEN nvl((select max(nvl(fec_modif, fec_cumpl)) from ipj.t_entidades_notas n where n.id_tramite_ipj = tr.id_tramite_ipj and informar_suac = 'N' and nvl(n.id_nota_sistema, 0) <> 1), tr.fecha_ini_suac) = nvl((select max(to_date(v_fecha_anexado, 'dd/mm/rrrr')) from nuevosuac.vt_get_tramites_ipj_anexos n where n.v_id_padre = tr.id_tramite), tr.fecha_ini_suac) THEN 'IGUAL'
                 END tipo_inactivo,
            (select COUNT(1) from ipj.t_entidades_notas n where n.id_tramite_ipj = tr.id_tramite_ipj AND n.id_estado_nota IN (0,2)) estado_nota,
            to_date(tr.Fecha_Ini_Suac, 'dd/mm/rrrr') fecha_inicio,
            tr.expediente nro_expediente, tr.sticker sticker,
            nvl((select denominacion_sia from ipj.t_legajos l where l.id_legajo = pr.id_legajo), ipj.tramites_suac.FC_Buscar_Iniciador_SUAC(tr.id_tramite)) razon_social,
            IPJ.TRAMITES_SUAC.FC_Buscar_Asunto_SUAC(TR.ID_TRAMITE) Asunto,
            tr.id_tramite_ipj id_tramite_ipj,tr.id_tramite id_tramite_suac,
            Greatest(
                   nvl((select max(nvl(fec_modif, fec_cumpl)) from ipj.t_entidades_notas n where n.id_tramite_ipj = tr.id_tramite_ipj and informar_suac = 'N' and nvl(n.id_nota_sistema, 0) <> 1), tr.fecha_ini_suac),
                   nvl((select max(to_date(v_fecha_anexado, 'dd/mm/rrrr')) from nuevosuac.vt_get_tramites_ipj_anexos n where n.v_id_padre = tr.id_tramite), tr.fecha_ini_suac)
            ) Ult_Nota,
            (select max(nvl(fec_modif, fec_cumpl)) from ipj.t_entidades_notas n where n.id_tramite_ipj = tr.id_tramite_ipj and informar_suac = 'N' and nvl(n.id_nota_sistema, 0) = 1) nota_sistema,
            (select count(1) from ipj.t_entidades_autorizados a where a.id_tramite_ipj = tr.id_tramite_ipj and a.mail_autorizado = 'S') Hay_Mail,
            tr.cuil_creador, tr.urgente, tr.simple_tramite, tr.cuil_ult_estado
          from ipj.t_tramitesipj tr left join ipj.t_tramitesipj_persjur pr
              on pr.id_tramite_ipj = tr.id_tramite_ipj
          where
            tr.id_ubicacion_origen = p_id_ubicacion and
            tr.id_estado_ult between 2 and 99 and -- Estados de trabajo
            tr.id_estado_ult not in (50, 6) and -- Excluye anexados y subsanados
            nvl(tr.id_tramite, 0) > 0 and
            --nvl(tr.cuil_ult_estado, '1') <> '1' and
            ( --tr.fecha_ini_suac < to_date('01/01/2017', 'dd/mm/rrrr') or -- Ahora se trabajan todos los tr�mites iguales
              IPJ.VARIOS.FC_Dias_Laborables(
                  nvl( Greatest(
                           nvl((select max(nvl(fec_modif, fec_cumpl)) from ipj.t_entidades_notas n where n.id_tramite_ipj = tr.id_tramite_ipj and informar_suac = 'N' and nvl(n.id_nota_sistema, 0) <> 1), tr.fecha_ini_suac),
                           nvl((select max(to_date(v_fecha_anexado, 'dd/mm/rrrr')) from nuevosuac.vt_get_tramites_ipj_anexos n where n.v_id_padre = tr.id_tramite), tr.fecha_ini_suac)
                         ), sysdate
                  ), to_date(sysdate, 'dd/mm/rrrr'), 'S'
               ) >= v_dias_inactividad
            )
          ) tmp
      WHERE decode(tmp.tipo_inactivo,'OBS',tmp.estado_nota,'IGUAL',tmp.estado_nota,0) = 0
    order by Fecha_Inicio asc;

  END SP_Tramites_Inactivos;

  PROCEDURE SP_Tramites_Inactivos_Jur(
    o_Cursor OUT types.cursorType,
    p_id_ubicacion in number
    )
  IS
  /****************************************************
     Lista los expedientes inactivos por m�s de 90 d�as para cualquier �rea + archivado por inactividad
  *****************************************************/
    v_dias_inactividad number;
  BEGIN
    -- Busco la cantidade de d�as de inactividad permitidos
    v_dias_inactividad := to_number(IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('DIAS_PREVIO_PERENCIO'));

    OPEN o_Cursor FOR
      select
        fecha_inicio, nro_expediente, sticker, razon_social, Asunto, id_tramite_ipj,
        id_tramite_suac, Ult_Nota, (to_date(sysdate, 'dd/mm/rrrr') - ult_nota) Dias_Totales, Hay_Mail, cuil_creador, urgente, simple_tramite,
        cuil_ult_estado,
        (select vt.id_subtipo_tramite from NUEVOSUAC.VT_TRAMITES_IPJ_DESA vt where vt.id_tramite = tmp.id_tramite_suac) id_subtipo_tramite,
        id_ubicacion, n_ubicacion
      from
        ( select
            to_date(tr.Fecha_Ini_Suac, 'dd/mm/rrrr') fecha_inicio,
            tr.expediente nro_expediente, tr.sticker sticker,
            nvl((select denominacion_sia from ipj.t_legajos l where l.id_legajo = pr.id_legajo), ipj.tramites_suac.FC_Buscar_Iniciador_SUAC(tr.id_tramite)) razon_social,
            IPJ.TRAMITES_SUAC.FC_Buscar_Asunto_SUAC(TR.ID_TRAMITE) Asunto,
            tr.id_tramite_ipj id_tramite_ipj, tr.id_tramite id_tramite_suac,
            Greatest(
                   nvl((select max(nvl(fec_modif, fec_cumpl)) from ipj.t_entidades_notas n where n.id_tramite_ipj = tr.id_tramite_ipj and informar_suac = 'N' and nvl(n.id_nota_sistema, 0) <> 1), tr.fecha_ini_suac),
                   nvl((select max(to_date(v_fecha_anexado, 'dd/mm/rrrr')) from nuevosuac.vt_get_tramites_ipj_anexos n where n.v_id_padre = tr.id_tramite), tr.fecha_ini_suac)
            ) Ult_Nota,
            (select count(1) from ipj.t_entidades_autorizados a where a.id_tramite_ipj = tr.id_tramite_ipj and a.mail_autorizado = 'S') Hay_Mail,
            tr.cuil_creador,  tr.urgente, tr.simple_tramite, tr.cuil_ult_estado, u.id_ubicacion,
            u.n_ubicacion n_ubicacion,
            (select a.id_archivo_tramite from ipj.t_archivo_tramite a where a.id_tramite_ipj = tr.id_tramite_ipj) id_archivo_tramite
          from ipj.t_tramitesipj tr
            left join ipj.t_tramitesipj_persjur pr on pr.id_tramite_ipj = tr.id_tramite_ipj
            left join ipj.t_ubicaciones u on u.id_ubicacion = tr.id_ubicacion
          where
            (p_id_ubicacion = 0 or tr.id_ubicacion = p_id_ubicacion) and
            ((tr.id_estado_ult = 210 or tr.id_estado_ult between 2 and 99) and
            tr.id_estado_ult not in (50, 6) and
              (to_date(sysdate, 'dd/mm/rrrr') -
                  nvl( Greatest(
                         nvl((select max(nvl(fec_modif, fec_cumpl)) from ipj.t_entidades_notas n where n.id_tramite_ipj = tr.id_tramite_ipj and informar_suac = 'N' and nvl(n.id_nota_sistema, 0) <> 1), tr.fecha_ini_suac),
                         nvl((select max(to_date(v_fecha_anexado, 'dd/mm/rrrr')) from nuevosuac.vt_get_tramites_ipj_anexos n where n.v_id_padre = tr.id_tramite), tr.fecha_ini_suac)
                  ), tr.fecha_ini_suac)
               ) >= v_dias_inactividad
            ) and
            nvl(tr.id_tramite, 0) > 0
            --nvl(tr.cuil_ult_estado, '1') <> '1'
          ) tmp
    where tmp.Ult_Nota <> fecha_inicio
    order by Hay_Mail desc, Fecha_Inicio asc;

  END SP_Tramites_Inactivos_Jur;

  PROCEDURE SP_GUARDAR_NOTA_INACTIVO(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_observacion in varchar2
    )
  IS
    /*************************************************************
      Agrega una nota de Tiempo de Inactividad a un tr�mite, luego de los 20 d�as de inactividad
      - Se agrega al final
      - Se marca como NO NUEVA
      - Se marca como No Sincronizada
      - Se marca en estado OK
      - Se marca como NOTA SISTEMA.
  *************************************************************/
    v_nro number;
  BEGIN
    -- calculo la proxima nota para agregar
    select nvl(max(nro), 0) + 1 into v_nro
    from ipj.t_entidades_notas
    where
      Id_Tramite_Ipj = p_Id_Tramite_Ipj;

    insert into ipj.t_entidades_notas
      (Id_Tramite_Ipj, id_legajo, nro, fec_modif, ID_ESTADO_NOTA, fec_cumpl,
       observacion, Informar_Suac, es_nuevo, id_nota_sistema)
    values
      (p_Id_Tramite_Ipj, NULL, v_nro, to_date(sysdate, 'dd/mm/rrrr'),
       1, to_date(sysdate, 'dd/mm/rrrr'), p_observacion, 'S', 'N', 1);

    o_rdo := IPJ.TYPES.c_Resp_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     WHEN OTHERS THEN
       o_rdo := 'SP_GUARDAR_NOTA_INACTIVO: ' || To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_GUARDAR_NOTA_INACTIVO;

  PROCEDURE SP_Actualizar_Nota_Sistema(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_id_nota_sistema in number
  )
  IS
  BEGIN
    -- Actualiza la fecha de la observaci�n, y corrige la fecha dentro del mensaje
    update ipj.t_entidades_notas
    set
      fec_modif = to_date(sysdate, 'dd/mm/rrrr'),
      observacion = (
          case
            when ipj.varios.F_DATE(nvl(substr(OBSERVACION, instr(OBSERVACION, '-')+2, 10), '-')) = 1 then
                substr(observacion, 0, instr(OBSERVACION, '-')+1) || to_char(sysdate, 'dd/mm/rrrr') || substr(observacion, instr(OBSERVACION, '-')+12, length(observacion))
            else observacion
          end)
    where
      id_tramite_ipj = p_id_tramite_ipj and
      id_nota_sistema = p_id_nota_sistema and
      informar_suac = 'S';

    if sql%rowcount > 0 then
      o_rdo := IPJ.TYPES.c_Resp_OK;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    else
      o_rdo := 'No se encontr� ninguna notificaci�n de sistema.';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
    end if;
  EXCEPTION
     WHEN OTHERS THEN
       o_rdo := 'SP_Actualizar_Nota_Sistema: ' || To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Actualizar_Nota_Sistema;

  PROCEDURE SP_Archivar_Inactivos (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number)
  IS
  /* El tr�mite indicado se pasar a Archivo como perimido y se marca como Inactivo:
    - Se realiza el pase en SUAC.
    - Se cierran las acciones existentes
  */
    v_Row_Ubicacion IPJ.T_UBICACIONES%ROWTYPE;
    v_Row_Tramite IPJ.T_TRAMITESIPJ%ROWTYPE;
    v_existe_Acc number;
    v_hay_persona number;
    v_id_legajo number;
    v_id_integrante number;
    v_id_archivo_tramite number;
    v_cant_sincroniz number;
    v_sticker varchar2(20);
  BEGIN
    -- Busco los datos del area de Archivo
    select * into v_Row_Ubicacion
    from IPJ.T_UBICACIONES
    where
      id_ubicacion = IPJ.TYPES.C_AREA_ARCHIVO;

    -- Busco los datos del tr�mite
    select * into v_Row_Tramite
    from ipj.t_tramitesipj
    where
      id_tramite_ipj = p_id_tramite_ipj;

    -- Si no es un tr�mite SUAC no hago nada
    if nvl(v_Row_Tramite.id_tramite, 0) = 0 then
      o_rdo := 'No es un tr�mite de SUAC, no puede marcarse como inactivo.';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      return;
    end if;

    -- Si no es anterior a 01/01/2018, no deja usar la funcionalidad
    if v_Row_Tramite.fecha_ini_suac >= to_date('01/01/2018', 'dd/mm/rrrr') then
      o_rdo := 'Esta funcionalidad no esta habilitada para tr�mites posteriores a 01/01/2018.';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      return;
    end if;

    -- Verifico que tenga una persona f�sica o jur�dica asignada
    select count(1) into v_hay_persona
    from
      ( select id_legajo
        from ipj.t_tramitesipj_persjur
        where
          id_tramite_ipj = p_id_tramite_ipj
        UNION ALL
        select id_integrante id_legajo
        from ipj.t_tramitesipj_integrante
        where
          id_tramite_ipj = p_id_tramite_ipj
       ) tmp;

    if v_hay_persona = 0 then
      o_rdo := 'El tr�mite no cuenta con Empresa o Persona asociada.';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      return;
    end if;

    -- Realizo la transferencia en SUAC del area al archivo
    IPJ.TRAMITES_SUAC.SP_Realizar_Tranf_IPJ(
      o_rdo => o_rdo,
      o_tipo_mensaje => o_tipo_mensaje,
      p_id_tramite_suac => v_Row_Tramite.id_tramite,
      p_id_ubicacion_origen => v_Row_Tramite.id_ubicacion,
      p_id_ubicacion_destino => IPJ.TYPES.C_AREA_ARCHIVO,
      p_cuil_usuario_origen => v_Row_Tramite.cuil_ult_estado,
      p_cuil_usuario_destino => v_Row_Ubicacion.cuil_usuario
    );

    if o_rdo <> IPJ.TYPES.C_RESP_OK then
      o_rdo :=  'Error al realizar transferencia en SUAC: ' || o_rdo;
      rollback;
      return;
    end if;

    -- Actualizo los datos del tr�mite
    update ipj.t_tramitesipj
    set
      cuil_ult_estado = v_Row_Ubicacion.cuil_usuario,
      id_clasif_ipj = 9, -- Archivo
      id_ubicacion = IPJ.TYPES.C_AREA_ARCHIVO,
      id_estado_ult = 210 -- INACTIVO (archivo de oficina)
    where
      id_tramite_ipj = p_id_tramite_ipj;

    -- Actualizo el Historial del tr�mite
    IPJ.TRAMITES.SP_GUARDAR_TRAMITES_IPJ_ESTADO(
      o_rdo => o_rdo,
      o_tipo_mensaje => o_tipo_mensaje,
      v_fecha_pase => sysdate,
      v_id_estado => 210, -- INACTIVO (archivo de oficina)
      v_cuil_usuario => v_Row_Ubicacion.cuil_usuario,
      v_id_grupo => null,
      v_observacion => null,
      v_Id_Tramite_Ipj => p_Id_Tramite_Ipj
    );

    if o_rdo <> IPJ.TYPES.C_RESP_OK then
      o_rdo := 'No se pudo guardar el historial del tr�mite: ' || o_rdo;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      rollback;
      return;
    end if;

    -- Sincronizo Obvservaciones Pendientes
    IPJ.TRAMITES_SUAC.SP_Subir_Notas_SUAC(
      o_rdo => o_rdo,
      o_tipo_mensaje => o_tipo_mensaje,
      o_cant_sincroniz => v_cant_sincroniz,
      o_sticker => v_sticker,
      p_id_tramite_ipj => p_Id_Tramite_Ipj,
      p_transaccion => 'N'
    );

    if o_rdo <> IPJ.TYPES.C_RESP_OK then
      o_rdo := 'No se pudo sincronizar observaciones del tr�mite: ' || o_rdo;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      rollback;
      return;
    end if;

    -- Marco todas las acicones del tr�mite como finalizadas
    update ipj.t_tramitesipj_acciones
    set id_estado = 110
    where
      id_tramite_ipj = p_id_tramite_ipj and
      id_estado < 200;

    --Busco en el tramite si existe la accion de Archivo
    select count(1) into v_existe_Acc
    from IPJ.T_TRAMITESIPJ_ACCIONES
    where
      Id_Tramite_Ipj = p_Id_Tramite_Ipj and
      id_tipo_accion = IPJ.TYPES.c_Tipo_Acc_Archivar_Expediente;

    -- Si no existe una accion de archivo, la creo
    if v_existe_Acc = 0 then
      --Busco el legajo o integrante del tramite
      begin
        select tpj.id_legajo into v_id_legajo
        from  ipj.t_tramitesipj_persjur tpj
        where
          Id_Tramite_Ipj =  p_Id_Tramite_Ipj and
          rownum = 1;
      exception
        when NO_DATA_FOUND then
          select ti.id_integrante into v_id_integrante
          from  ipj.t_tramitesipj_integrante ti
          where
            Id_Tramite_Ipj =  p_Id_Tramite_Ipj and
            rownum = 1;

          v_id_legajo := null;
      end;

      -- Creo la accion para Archivo
      IPJ.TRAMITES.SP_GUARDAR_TRAMITEIPJ_ACC(
        o_rdo => o_rdo,
        o_tipo_mensaje => o_tipo_mensaje,
        p_Id_Tramite_Ipj => p_Id_Tramite_Ipj,
        p_id_tramiteipj_accion => 0,
        p_id_tipo_accion =>  IPJ.TYPES.c_Tipo_Acc_Archivar_Expediente,
        p_id_integrante => v_id_integrante,
        p_dni_integrante => null,
        p_id_legajo => v_id_legajo,
        p_cuit_persjur => null,
        p_id_estado => IPJ.TYPES.C_ESTADOS_COMPLETADO,
        p_cuil_usuario => v_Row_Ubicacion.cuil_usuario,
        p_obs_rubrica => null,
        p_observacion => null,
        p_id_fondo_comercio => null,
        p_fec_veeduria => null,
        p_id_documento => null,
        p_n_documento => null,
        p_id_pagina => 17 -- Pagina de Archivo
      );

      -- Creo la entrada vacia en archivo
      if o_rdo = IPJ.TYPES.C_RESP_OK then
        -- busco la accion creada
        select id_tramiteipj_accion into v_existe_Acc
        from IPJ.T_TRAMITESIPJ_ACCIONES
        where
          Id_Tramite_Ipj = p_Id_Tramite_Ipj and
          id_tipo_accion = IPJ.TYPES.c_Tipo_Acc_Archivar_Expediente;
      else
        o_rdo := 'No se pudo crear la acci�n de Archivo: ' || o_rdo;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
        rollback;
        return;
      end if;
    end if;

    -- Veo si el tr�mite ya esta asignado en Archivo
    begin
      select id_archivo_tramite into v_id_archivo_tramite
      from ipj.t_archivo_tramite
      where
        id_tramite_ipj = p_id_tramite_ipj;
    exception
      when NO_DATA_FOUND then
        v_id_archivo_tramite := null;
    end;

    -- Si el tr�mite no esta, lo busco por el expediente
    if v_id_archivo_tramite is null then
      begin
        select id_archivo_tramite into v_id_archivo_tramite
        from ipj.t_archivo_tramite
        where
          trim(expediente) = v_Row_Tramite.expediente;
      exception
        when NO_DATA_FOUND then
          v_id_archivo_tramite := null;
      end;
    end if;

    -- Si no existe hago un insert, sino actualizo el tramite y la accion en archivo
    if v_id_archivo_tramite is null then
      insert into ipj.t_archivo_tramite (id_archivo_tramite, id_tramite_ipj,
        expediente, id_tramiteipj_accion, id_ubicacion, id_legajo, id_integrante,
        id_tramite, id_accion_archivo)
      values ( IPJ.SEQ_ARCHIVO.nextval, p_Id_Tramite_Ipj,
        v_Row_Tramite.expediente, v_existe_Acc, v_Row_Tramite.id_ubicacion_origen,
        v_id_legajo, v_id_integrante, v_Row_Tramite.id_tramite, 6 /* Perimido*/);
    else
      update ipj.t_archivo_tramite
      set
        id_tramite_ipj = p_id_tramite_ipj,
        id_tramiteipj_accion = v_existe_Acc,
        id_integrante = v_id_integrante,
        id_legajo = v_id_legajo,
        id_tramite = v_Row_Tramite.id_tramite,
        id_accion_archivo = 6 -- Perimido
      where
        id_archivo_tramite = v_id_archivo_tramite;
    end if;

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    commit;
  EXCEPTION
     when OTHERS then
        o_rdo := 'SP_Archivar_Inactivos: ' || To_Char(SQLCODE) || '-' || SQLERRM;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
        rollback;
  END SP_Archivar_Inactivos;

  PROCEDURE SP_Guardar_Transferencia(
    o_Id_Tramite_Ipj out number,
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_id_Grupo IN number,
    p_cuil_ult_estado in varchar2,
    p_observacion IN varchar2,
    p_TransfiereAcc IN VARCHAR2
  )
  IS
   /****************************************************
   Guarda los datos de un tramite.
   Actualiza el historial de estados.
   Si se pasa a terminado, actualiza los datos en Personas Juridicas
   La transaccion la administra la aplicacion al guardar cabecera y detalles
   Si se requiere, al transferir el tr�mite, transfiere las acciones
   *****************************************************/
    v_Row_Tramite IPJ.T_TRAMITESIPJ%ROWTYPE;
    v_usrTecnico NUMBER;
    v_bloque VARCHAR2(50);
    v_tipo_accion ipj.t_tipos_accionesipj.n_tipo_accion%TYPE;
    v_rdo VARCHAR2(2000);
    v_cuil_usuario varchar2(20);
    v_estado number;
  BEGIN
    v_bloque := 'SP_Guardar_Transferencia';
    -- Busco los datos del tr�mite
    SELECT *
      INTO v_Row_Tramite
      FROM ipj.t_tramitesipj
     WHERE id_tramite_ipj = p_id_tramite_ipj;

    v_estado := (case when v_Row_Tramite.id_estado_ult = 1 then 2 else v_Row_Tramite.id_estado_ult end);

    -- Guardo el tramite para el area (del tramite o del usuario)
    IPJ.TRAMITES.SP_GUARDAR_TRAMITE(
      o_Id_Tramite_Ipj => o_Id_Tramite_Ipj,
      o_rdo => o_rdo,
      o_tipo_mensaje => o_tipo_mensaje,
      v_Id_Tramite_Ipj => v_Row_Tramite.Id_Tramite_Ipj,
      v_id_tramite => v_Row_Tramite.id_tramite,
      v_id_Clasif_IPJ => v_Row_Tramite.id_clasif_ipj,
      v_id_Grupo => p_id_Grupo,
      v_cuil_ult_estado => p_cuil_ult_estado,
      v_id_estado => v_estado,
      v_cuil_creador => v_Row_Tramite.cuil_creador,
      v_observacion => p_observacion,
      v_id_Ubicacion => v_Row_Tramite.id_ubicacion,
      v_urgente => 'N',
      p_sticker => v_Row_Tramite.sticker,
      p_expediente => v_Row_Tramite.expediente,
      p_simple_tramite => 'N');

      if o_rdo <> IPJ.TYPES.C_RESP_OK then
        o_rdo :=  v_bloque || ' - ' || o_rdo;
        o_Id_Tramite_Ipj := v_Row_Tramite.Id_Tramite_Ipj;
        o_tipo_mensaje := IPJ.TYPES.c_Tipo_Mens_ERROR;
        rollback;
        return;
      end if;

    -- (PBI 12978) Si se asigna a un grupo, las acciones se pasan al usuario DB, sino al usuario indicado
    if nvl(p_id_Grupo,0) <> 0 then
      v_usrTecnico := 1;
      v_cuil_usuario := '1';
    else
      v_cuil_usuario := p_cuil_ult_estado;
    end if;

    --Si el grupo viene vac�o, transfiero las acciones. Si no, solamente se guarda el tr�mite
    IF p_TransfiereAcc = 'S' THEN

      --Valido que el usuario al cual se van a transferir las acciones sea t�cnico
      SELECT count(1)
        INTO v_usrTecnico
        FROM ipj.t_grupos_trab_ubicacion gt
       WHERE gt.cuil = p_cuil_ult_estado
         AND gt.id_ubicacion = v_Row_Tramite.id_ubicacion
         AND gt.id_grupo_trabajo = 2;--TECNICO
    END IF;

    IF (p_TransfiereAcc = 'S' or nvl(p_id_Grupo, 0) <> 0 ) and v_usrTecnico <> 0 THEN
      --Busco las acciones que no est�n cerradas y pertenezcan al �rea del tr�mite
      FOR r IN (SELECT ta.id_tramiteipj_accion, ta.id_integrante, ta.id_legajo, ta.id_estado
                     , ti.id_pagina, ta.id_tipo_accion, ta.obs_rubrica, ta.id_documento, ta.n_documento
                     , fec_veeduria
                  FROM ipj.t_tramitesipj_acciones ta
                  JOIN ipj.t_tramitesipj t ON ta.id_tramite_ipj = t.id_tramite_ipj
                  JOIN ipj.t_tipos_accionesipj ti ON ta.id_tipo_accion = ti.id_tipo_accion
                  JOIN ipj.t_tipos_clasif_ipj tc ON ti.id_clasif_ipj = tc.id_clasif_ipj
                 WHERE ta.id_tramite_ipj = v_Row_Tramite.Id_Tramite_Ipj
                   AND ta.id_estado < IPJ.TYPES.c_Estados_Completado
                   AND t.id_ubicacion = tc.id_ubicacion) LOOP

        --Transfiere las acciones del tr�mite
        IPJ.TRAMITES.SP_GUARDAR_TRAMITEIPJ_ACC(
            o_rdo => o_rdo,
            o_tipo_mensaje => o_tipo_mensaje,
            p_Id_Tramite_Ipj => v_Row_Tramite.Id_Tramite_Ipj,
            p_id_tramiteipj_accion => r.id_tramiteipj_accion,
            p_id_tipo_accion =>  r.id_tipo_accion,
            p_id_integrante => r.id_integrante,
            p_dni_integrante => NULL,
            p_id_legajo => r.id_legajo,
            p_cuit_persjur => NULL,
            p_id_estado => r.id_estado,
            p_cuil_usuario => v_cuil_usuario,
            p_obs_rubrica => r.obs_rubrica,
            p_observacion => NULL,
            p_id_fondo_comercio => NULL,
            p_fec_veeduria => r.fec_veeduria,
            p_id_documento => r.id_documento,
            p_n_documento => r.n_documento,
            p_id_pagina => r.id_pagina );

        SELECT ta.n_tipo_accion
          INTO v_tipo_accion
          FROM ipj.t_tipos_accionesipj ta
         WHERE ta.id_tipo_accion = r.id_tipo_accion;

        if o_rdo <> IPJ.TYPES.C_RESP_OK then
          v_rdo := v_tipo_accion || ' - ' || v_rdo;
          o_rdo := 'Las siguientes acciones no pudieron transferirse: '|| ' - ' || v_rdo;
          o_Id_Tramite_Ipj := v_Row_Tramite.Id_Tramite_Ipj;
          o_tipo_mensaje := IPJ.TYPES.c_Tipo_Mens_WARNING;
        end if;

      END LOOP;
    END IF;

    COMMIT;

  EXCEPTION
    WHEN OTHERS THEN
      o_rdo  := To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      rollback;
  END SP_Guardar_Transferencia;

  PROCEDURE SP_Guardar_Accion_Act_Pers (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_id_tramiteIpj_Accion in number,
    p_nro_actuacion in varchar2,
    p_nro_dictamen in number,
    p_fecha_dictamen IN VARCHAR2 )
  IS
  BEGIN
    if IPJ.Types.c_habilitar_log_SP = 'S' then
      IPJ.VARIOS.SP_GUARDAR_LOG(
        p_METODO => 'SP_Guardar_Accion_Act_Pers',
        p_NIVEL => '',
        p_ORIGEN => 'DB',
        p_USR_LOG  => 'DB',
        p_USR_WINDOWS => 'Sistema',
        p_IP_PC  => null,
        p_EXCEPCION =>
          'Id Tramite Ipj = ' || to_char(p_Id_Tramite_Ipj)
          || ' / Id TramiteIpj Accion = ' || to_char(p_id_tramiteIpj_Accion)
          || ' / Nro. Actuacion = ' || p_nro_actuacion
          || ' / Nro. Dictamen = ' || to_char(p_nro_dictamen)
          || ' / Fecha Dictamen = ' || p_fecha_dictamen
       );
    end if;

    if p_fecha_dictamen is null or IPJ.VARIOS.Valida_Fecha(p_fecha_dictamen) = 'N' then
      o_rdo := IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT') ;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      return;
    end if;

    update IPJ.t_TramitesIpj_Acciones
    set
      nro_actuacion = p_nro_actuacion,
      nro_dictamen = p_nro_dictamen,
      fecha_dictamen = to_date(p_fecha_dictamen, 'dd/mm/rrrr')
    where
      Id_Tramite_Ipj = p_Id_Tramite_Ipj and
      id_tramiteipj_accion = p_id_tramiteipj_accion;

    -- Actualizo el protocolo que corresponda de Jur�dico
    if nvl(p_nro_dictamen, 0) > 0 then
      update ipj.t_protocolos_juridico
      set numerador = (case when numerador >= p_nro_dictamen then numerador else p_nro_dictamen end)
      where
        id_prot_jur = 'D';
    end if;

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    COMMIT;
  EXCEPTION
     when OTHERS then
        o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Guardar_Accion_Act_Pers;

PROCEDURE SP_Validar_Terminos(
    p_denominacion IN VARCHAR2,
    p_id_tipo_entidad IN NUMBER,
    p_codigo_online IN NUMBER,
    o_rdo OUT VARCHAR2,
    o_tipo_mensaje OUT NUMBER)
  IS
  /**************************************************************
  Este SP es llamado desde el Portal y maneja las validaciones
  de cada uno de los tipos de t�rminos
  ***************************************************************/
    v_id_ubicacion NUMBER;
    v_denominacion VARCHAR2(1000);
    v_espacios NUMBER;
    v_palabra VARCHAR2(1000);
    v_pos1 NUMBER;
    v_caracteres NUMBER;
    v_caracteres_frase NUMBER;
    v_frase VARCHAR2(1000);
    v_letras NUMBER;
    v_caracteres_especiales NUMBER;
    v_tipos_societarios NUMBER;
    v_tipo_societario varchar2(100);
    v_posicion_tipo_soc number;
    v_denominacion_final varchar2(250);
  BEGIN

    if IPJ.Types.c_habilitar_log_SP = 'S' then
      IPJ.VARIOS.SP_GUARDAR_LOG(
        p_METODO => 'SP_Validar_Terminos',
        p_NIVEL => '',
        p_ORIGEN => 'DB',
        p_USR_LOG  => 'DB',
        p_USR_WINDOWS => 'Sistema',
        p_IP_PC  => null,
        p_EXCEPCION =>
          'Denominacion = ' || p_denominacion
          || ' / Tipo Entidad = ' || to_char(p_id_tipo_entidad)
          || ' / Codigo Online = ' || to_char(p_codigo_online)
       );
    end if;

    -- Busco el tipo societario del c�digo online
    select n_tipo_sociedad into v_tipo_societario
    from ipj.t_ol_entidades
    where
      codigo_online = p_codigo_online;

    v_denominacion_final := p_denominacion;

    -- Si tiene un tipo societario, lo quito del nombre antes de validar
    if v_tipo_societario is not null then
      -- Busco la posici�n del tipo societario dentro del nombre
      v_posicion_tipo_soc := INSTRB(p_denominacion, v_tipo_societario, 1, 1);
      --Si se encuentra, quito la primer ocurrencia
      if v_posicion_tipo_soc > 0 then
        v_denominacion_final :=
          substr(p_denominacion, 1, v_posicion_tipo_soc - 1 ) ||
          substr(p_denominacion, v_posicion_tipo_soc + Length(v_tipo_societario), Length(p_denominacion));
      end if;
    end if;

    --Si no viene tipo de entidad no debe hacer nada
    IF p_id_tipo_entidad IS NOT NULL THEN
      SELECT t.id_ubicacion
        INTO v_id_ubicacion
        FROM ipj.t_tipos_entidades t
       WHERE t.id_tipo_entidad = p_id_tipo_entidad;

      --Antes de formatear valido caracteres bloqueados
      tramites.sp_validar_caracteres(v_denominacion_final, v_id_ubicacion, p_id_tipo_entidad, o_rdo, o_tipo_mensaje);

      IF o_tipo_mensaje = IPJ.TYPES.C_TIPO_MENS_ERROR THEN
        o_rdo := 'La denominaci�n '||p_denominacion|| o_rdo;-- final
        RETURN;
      END IF;

      v_denominacion := varios.FC_Formatear_Denominacion(v_denominacion_final);

      v_caracteres_frase := LENGTH(v_denominacion);--final

      --Valido que la denominaci�n contenga al menos tres caracteres
      IF v_caracteres_frase < 3 THEN
        o_rdo := 'La denominaci�n '||p_denominacion||' debe contener al menos tres caracteres, y como m�nimo dos de ellos deben ser letras.'; --final
        RETURN;
      END IF;

      --Valido que tenga al menos dos letras
      v_letras := LENGTH(REPLACE(v_denominacion,' ',''));
      IF v_letras < 2 THEN
        o_rdo := 'La denominaci�n '||p_denominacion||' debe contener al menos dos letras.';
        RETURN;
      END IF;

      --Calculo la cantidad de espacios de la frase
      v_espacios := length(v_denominacion) - length(replace(v_denominacion,' '));

      --Valido los tipos societarios con reemplazo de carcateres
      tramites.sp_validar_tipossoc(v_denominacion, v_id_ubicacion, p_id_tipo_entidad, o_rdo, o_tipo_mensaje);
      IF o_tipo_mensaje = IPJ.TYPES.C_TIPO_MENS_ERROR THEN
        o_rdo := 'La denominaci�n '||p_denominacion|| o_rdo;
        RETURN;
      END IF;

      --Valido los tipos societarios con el texto ingresado por el usuario
      tramites.sp_validar_tipossoc(v_denominacion_final, v_id_ubicacion, p_id_tipo_entidad, o_rdo, o_tipo_mensaje);
      IF o_tipo_mensaje = IPJ.TYPES.C_TIPO_MENS_ERROR THEN
        o_rdo := 'La denominaci�n '||p_denominacion|| o_rdo;
        RETURN;
      END IF;

      FOR r IN 1..(v_espacios + 1) LOOP

        --Busco la posici�n inicial de la palabra
        v_pos1 := REGEXP_INSTR(v_denominacion,'[^ ]+', 1, r);

        --Cuando es la �ltima palabra de la frase calculo el tama�o de la palabra
        IF r < v_espacios + 1 THEN
          --Busco la cantidad de caracteres de la palabra, obteniendo la posici�n del espacio y restando la posici�n 1
          v_caracteres := INSTR(v_denominacion, ' ',v_pos1) - v_pos1;

        ELSE
          --Busco la cantidad de caracteres de la �ltima palabra, restando la cantidad de caracteres de la frase menos
          --la posici�n de la �litma palabra menos 1, que es donde se ubica el �ltimo espacio
          v_caracteres := v_caracteres_frase - INSTR(v_denominacion, ' ',v_pos1-1);
        END IF;

        --Obtengo la palabra a partir de las posiciones
        v_palabra := SUBSTR(v_denominacion,v_pos1,v_caracteres);

        --Valido las palabras obtenidas contra los insultos cargados en la tabla de t�rminos exclu�dos
        ipj.tramites.sp_validar_insultos(v_palabra, v_denominacion, v_id_ubicacion, p_id_tipo_entidad, o_rdo, o_tipo_mensaje);

        IF o_tipo_mensaje = IPJ.TYPES.C_TIPO_MENS_ERROR THEN
          o_rdo := 'La denominaci�n '||p_denominacion|| o_rdo;--final
          RETURN;
        END IF;

        --Valido las palabras obtenidas contra los t�rminos prohibidos cargados en la tabla de t�rminos exclu�dos
        ipj.tramites.sp_validar_prohibidos(v_palabra, v_denominacion, v_id_ubicacion, p_id_tipo_entidad, o_rdo, o_tipo_mensaje);

        IF o_tipo_mensaje = IPJ.TYPES.C_TIPO_MENS_ERROR THEN
          o_rdo := 'La denominaci�n '||p_denominacion|| o_rdo;--final
          RETURN;
        END IF;

        --Valido las palabras obtenidas contra los t�rminos exclu�dos cargados en la tabla de t�rminos exclu�dos
        ipj.tramites.sp_validar_excluidos(v_palabra, v_denominacion, v_id_ubicacion, p_id_tipo_entidad, o_rdo, o_tipo_mensaje);

        IF o_tipo_mensaje = IPJ.TYPES.C_TIPO_MENS_ERROR THEN
          o_rdo := 'La denominaci�n '||p_denominacion|| o_rdo;--final
          RETURN;
        END IF;

      END LOOP;

      --Valido contra los nombres existentes en Gesti�n
      ipj.tramites.sp_validar_existentes(o_rdo, o_tipo_mensaje, v_denominacion, p_id_tipo_entidad, v_id_ubicacion);

      IF o_tipo_mensaje = IPJ.TYPES.C_TIPO_MENS_ERROR THEN
        o_rdo := 'La denominaci�n '||p_denominacion|| o_rdo;--final
        RETURN;
      END IF;

    ELSE
      o_rdo := 'Debe ingresar un tipo societario.';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      o_rdo := 'SP_VALIDAR_TERMINOS: '|| To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Validar_Terminos;

  PROCEDURE SP_Validar_Insultos(
    p_palabra IN VARCHAR2,
    p_frase IN VARCHAR2,
    p_ubicacion IN NUMBER,
    p_id_tipo_entidad IN NUMBER,
    o_rdo OUT VARCHAR2,
    o_tipo_mensaje OUT NUMBER)
  IS
  /************************************************************************
  Este SP es llamado desde el SP_Validar_Terminos y maneja las validaciones
  de los t�rminos tipo Insultos
  *************************************************************************/
  v_res NUMBER := 0;
  BEGIN
    SELECT count(1)
      INTO v_res
      FROM ipj.t_terminos_excluidos e
     WHERE e.id_tipo_termino = 1--Insultos
       AND (nvl(e.id_ubicacion,0) = nvl(p_ubicacion,0) OR
            nvl(e.id_tipo_entidad,0) = nvl(p_id_tipo_entidad,0) OR
            (e.id_ubicacion IS NULL AND e.id_tipo_entidad IS NULL))
       AND (e.n_termino = p_palabra OR e.n_termino = p_frase);

    IF v_res = 0 THEN
      o_rdo := 'OK';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    ELSE
      o_rdo := ' no es v�lida por contener uno o m�s t�rmino/s contrario/s a ley.';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      RETURN;
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      o_rdo := 'SP_VALIDAR_INSULTOS: ' ||To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Validar_Insultos;

  PROCEDURE SP_Validar_Prohibidos(
    p_palabra IN VARCHAR2,
    p_frase IN VARCHAR2,
    p_ubicacion IN NUMBER,
    p_id_tipo_entidad IN NUMBER,
    o_rdo OUT VARCHAR2,
    o_tipo_mensaje OUT NUMBER)
  IS
  /************************************************************************
  Este SP es llamado desde el SP_Validar_Terminos y maneja las validaciones
  de los t�rminos tipo Prohibidos
  *************************************************************************/
  v_res NUMBER := 0;
  BEGIN
    SELECT count(1)
      INTO v_res
      FROM ipj.t_terminos_excluidos e
     WHERE e.id_tipo_termino = 2--Prohibidos
       AND (nvl(e.id_ubicacion,0) = nvl(p_ubicacion,0) OR
            nvl(e.id_tipo_entidad,0) = nvl(p_id_tipo_entidad,0) OR
            (e.id_ubicacion IS NULL AND e.id_tipo_entidad IS NULL))
       AND (e.n_termino = p_palabra OR e.n_termino = p_frase);

    IF v_res = 0 THEN
      o_rdo := 'OK';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    ELSE
      o_rdo := ' no es v�lida por contener uno o m�s t�rmino/s contrario/s a ley.';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      o_rdo := 'SP_VALIDAR_PROHIBIDOS: ' ||To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Validar_Prohibidos;

  PROCEDURE SP_Validar_Excluidos(
    p_palabra IN VARCHAR2,
    p_frase IN VARCHAR2,
    p_ubicacion IN NUMBER,
    p_id_tipo_entidad IN NUMBER,
    o_rdo OUT VARCHAR2,
    o_tipo_mensaje OUT NUMBER)
  IS
  /************************************************************************
  Este SP es llamado desde el SP_Validar_Terminos y maneja las validaciones
  de los t�rminos tipo Exclu�dos
  *************************************************************************/
  v_res NUMBER := 0;
  BEGIN
    SELECT count(1)
      INTO v_res
      FROM ipj.t_terminos_excluidos e
     WHERE e.id_tipo_termino = 3--Exclu�dos
       AND (nvl(e.id_ubicacion,0) = nvl(p_ubicacion,0) OR
            nvl(e.id_tipo_entidad,0) = nvl(p_id_tipo_entidad,0) OR
            (e.id_ubicacion IS NULL AND e.id_tipo_entidad IS NULL))
       AND (e.n_termino = p_palabra OR e.n_termino = p_frase);

    IF v_res = 0 THEN
      o_rdo := 'OK';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    ELSE
      o_rdo := ' no es v�lida por contener uno o m�s t�rmino/s restringido/s.';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
    END IF;

  EXCEPTION
    WHEN OTHERS THEN
      o_rdo := 'SP_VALIDAR_EXCLUIDOS: ' ||To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Validar_Excluidos;

  PROCEDURE SP_Validar_Caracteres(
    p_denominacion IN VARCHAR2,
    p_ubicacion IN NUMBER,
    p_id_tipo_entidad IN NUMBER,
    o_rdo OUT VARCHAR2,
    o_tipo_mensaje OUT NUMBER)
  IS
  /************************************************************************
  Este SP es llamado desde el SP_Validar_Terminos y maneja las validaciones
  de los t�rminos tipo Caracteres
  *************************************************************************/
  v_caracteres_especiales NUMBER := 0;
  BEGIN

    SELECT SUM(INSTR(p_denominacion,t.n_termino))
      INTO v_caracteres_especiales
      FROM ipj.t_terminos_excluidos t
     WHERE t.id_tipo_termino = 4--Caracteres
       AND (nvl(t.id_ubicacion,0) = nvl(p_ubicacion,0) OR
           nvl(t.id_tipo_entidad,0) = nvl(p_id_tipo_entidad,0) OR
           (t.id_ubicacion IS NULL AND t.id_tipo_entidad IS NULL));

    IF v_caracteres_especiales = 0 THEN
      o_rdo := 'OK';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    ELSE
      o_rdo := ' contiene caracteres especiales no permitidos.';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      o_rdo := 'SP_VALIDAR_CARACTERES: ' ||To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Validar_Caracteres;

  PROCEDURE SP_Validar_TiposSoc(
    p_frase IN VARCHAR2,
    p_ubicacion IN NUMBER,
    p_id_tipo_entidad IN NUMBER,
    o_rdo OUT VARCHAR2,
    o_tipo_mensaje OUT NUMBER)
  IS
  /************************************************************************
  Este SP es llamado desde el SP_Validar_Terminos y maneja las validaciones
  de los t�rminos tipo Tipos Societarios
  *************************************************************************/
  v_tipos_societarios NUMBER := 0;
  BEGIN

    SELECT sum(INSTR(p_frase, t.n_termino))
      INTO v_tipos_societarios
      FROM ipj.t_terminos_excluidos t
     WHERE t.id_tipo_termino = 5--Tipos Societarios
       AND (nvl(t.id_ubicacion,0) = nvl(p_ubicacion,0) OR
           nvl(t.id_tipo_entidad,0) = nvl(p_id_tipo_entidad,0) OR
           (t.id_ubicacion IS NULL AND t.id_tipo_entidad IS NULL));

    /*IF p_frase LIKE '% SA'
      OR p_frase LIKE '% S A' OR p_frase LIKE '% SAS' OR p_frase LIKE '% S A S' OR p_frase LIKE '% SRL'
      OR p_frase LIKE '% S R L' OR p_frase LIKE '% SAC' OR p_frase LIKE '% SACIA' OR p_frase LIKE '% SAFAG'
      OR p_frase LIKE '% SAI' OR p_frase LIKE '% SAIC' OR p_frase LIKE '% SAIC Y A' OR p_frase LIKE '% SAICA'
      OR p_frase LIKE '% SAIF' OR p_frase LIKE '% SAIY C' OR p_frase LIKE '% SAMCI' OR p_frase LIKE '% SCA'
    THEN


      v_tipos_societarios := 1;
    end if;*/

    IF v_tipos_societarios = 0 THEN
      o_rdo := 'OK';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    ELSE
      o_rdo := ' contiene tipos societarios.';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      o_rdo := 'SP_VALIDAR_TIPOSSOC: ' ||To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Validar_TiposSoc;

  PROCEDURE SP_Buscar_Pendientes(
    o_Cursor out IPJ.TYPES.CURSORTYPE,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number)
  IS
  /**********************************************************
   (PBI 13257) Busca el listado de tr�mites pendientes de una empresa (ya no controla con respecto a que tr�mite)
  **********************************************************/
    v_fecha_suac_inf date;
  BEGIN
    -- Busco los tr�mites pendientes posteriores
    OPEN o_Cursor FOR
      select tr.expediente, tr.sticker,
        IPJ.TRAMITES_SUAC.FC_BUSCAR_SUBTIPO_SUAC(tr.id_tramite) SubTipo,
        IPJ.TRAMITES_SUAC.FC_BUSCAR_ASUNTO_SUAC(tr.id_tramite) Asunto
      from ipj.t_tramitesipj tr join ipj.t_tramitesipj_persjur trp
          on tr.id_tramite_ipj = trp.id_tramite_ipj
      where
        trp.id_legajo = p_id_legajo and
        tr.id_estado_ult < IPJ.TYPES.C_ESTADOS_COMPLETADO and
        nvl(tr.id_tramite, 0) > 0 -- Que sean tr�mites reales
      order by tr.fecha_ini_suac asc;

  END SP_Buscar_Pendientes;

  PROCEDURE SP_Validar_Existentes(
    o_rdo OUT VARCHAR2,
    o_tipo_mensaje OUT NUMBER,
    p_denominacion IN VARCHAR2,
    p_id_tipo_entidad IN NUMBER,
    p_id_ubicacion IN NUMBER)
  IS
    v_res NUMBER;
  BEGIN
  /*********************************************************
  Este SP es llamado desde el SP_Validar_Terminos y busca
  nombres de empresas similares segun su escritura
  en legajos y reservas OK no vencidas, restringidos a las
  areas del usuario logueado.
  **********************************************************/
    --Si es asociaci�n civil o fundaciones pueden tener nombres iguales
    IF p_id_ubicacion IN (5) THEN
      SELECT COUNT(1)
        INTO v_res
        FROM IPJ.T_Legajos L JOIN IPJ.t_tipos_entidades te
          ON L.id_tipo_entidad = te.id_tipo_entidad
       WHERE nvl(eliminado, 0) = 0
         AND te.id_ubicacion = p_id_ubicacion
         AND l.fecha_baja_entidad is null
         AND l.id_tipo_entidad = p_id_tipo_entidad
         AND p_denominacion = varios.FC_Quitar_Tipo_Empresa(varios.FC_Formatear_Denominacion(L.DENOMINACION_SIA));

      IF v_res = 0 THEN
        o_rdo := 'OK';
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
      ELSE
        o_rdo := ' corresponde a una empresa ya registrada en IPJ';
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
        RETURN;
      END IF;

      SELECT COUNT(1)
        INTO v_res
        FROM IPJ.T_tramitesipj_acc_reserva tr
        JOIN ipj.t_tramitesipj tra ON tr.Id_Tramite_Ipj = tra.Id_Tramite_Ipj
   LEFT JOIN ipj.t_ol_entidades e ON tra.codigo_online = e.codigo_online
       WHERE TR.ID_ESTADO_RESERVA = IPJ.Types.c_Reserva_OK
         AND TR.FEC_VENCE IS NOT NULL
         AND TR.FEC_VENCE >= to_date(SYSDATE, 'dd/mm/rrrr')
         AND tra.id_ubicacion_origen = p_id_ubicacion
         AND e.id_tipo_entidad = p_id_tipo_entidad
         AND p_denominacion = varios.FC_Quitar_Tipo_Empresa(varios.FC_Formatear_Denominacion(tr.N_Reserva));

      IF v_res = 0 THEN
        o_rdo := 'OK';
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
      ELSE
        o_rdo := ' corresponde a una reserva ya ingresada en IPJ';
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
        RETURN;
      END IF;

      SELECT COUNT(1)
        INTO v_res
        FROM ipj.t_tramitesipj tr JOIN ipj.t_entidades e
          ON tr.Id_Tramite_Ipj = e.Id_Tramite_Ipj
       WHERE TR.ID_ESTADO_ULT < IPJ.TYPES.C_ESTADOS_COMPLETADO
         AND tr.id_ubicacion_origen = p_id_ubicacion
         AND e.id_tipo_entidad = p_id_tipo_entidad
         AND p_denominacion = varios.FC_Quitar_Tipo_Empresa(varios.FC_Formatear_Denominacion(e.Denominacion_1));

      IF v_res = 0 THEN
        o_rdo := 'OK';
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
      ELSE
        o_rdo := ' corresponde a una empresa ya registrada en IPJ';
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
        RETURN;
      END IF;

    ELSE

      SELECT COUNT(1)
        INTO v_res
        FROM IPJ.T_Legajos L JOIN IPJ.t_tipos_entidades te
          ON L.id_tipo_entidad = te.id_tipo_entidad
       WHERE nvl(eliminado, 0) = 0
         AND te.id_ubicacion <> 5 --p_id_ubicacion
         AND l.fecha_baja_entidad is null
         AND p_denominacion = varios.FC_Quitar_Tipo_Empresa(varios.FC_Formatear_Denominacion(L.DENOMINACION_SIA));

      IF v_res = 0 THEN
        o_rdo := 'OK';
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
      ELSE
        o_rdo := ' corresponde a una empresa ya registrada en IPJ';
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
        RETURN;
      END IF;

      SELECT COUNT(1)
        INTO v_res
        FROM IPJ.T_tramitesipj_acc_reserva tr JOIN ipj.t_tramitesipj tra
          ON tr.Id_Tramite_Ipj = tra.Id_Tramite_Ipj
       WHERE TR.ID_ESTADO_RESERVA = IPJ.Types.c_Reserva_OK
         AND TR.FEC_VENCE IS NOT NULL
         AND TR.FEC_VENCE >= to_date(SYSDATE, 'dd/mm/rrrr')
         AND tra.id_ubicacion_origen <> 5 --p_id_ubicacion
         AND p_denominacion = varios.FC_Quitar_Tipo_Empresa(varios.FC_Formatear_Denominacion(tr.N_Reserva));

      IF v_res = 0 THEN
        o_rdo := 'OK';
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
      ELSE
        o_rdo := ' corresponde a una reserva ya ingresada en IPJ';
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
        RETURN;
      END IF;

      SELECT COUNT(1)
        INTO v_res
        FROM ipj.t_tramitesipj tr JOIN ipj.t_entidades e
          ON tr.Id_Tramite_Ipj = e.Id_Tramite_Ipj
       WHERE TR.ID_ESTADO_ULT < IPJ.TYPES.C_ESTADOS_COMPLETADO
         AND tr.id_ubicacion_origen <> 5 --p_id_ubicacion
         AND p_denominacion = varios.FC_Quitar_Tipo_Empresa(varios.FC_Formatear_Denominacion(e.Denominacion_1));

      IF v_res = 0 THEN
        o_rdo := 'OK';
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
      ELSE
        o_rdo := ' corresponde a una empresa ya registrada en IPJ';
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
        RETURN;
      END IF;

    END IF;

  END SP_Validar_Existentes;

  PROCEDURE SP_Hist_Inf_Subsistencia(
    o_Cursor out IPJ.TYPES.CURSORTYPE,
    p_Id_Accion in number,
    p_Id_Legajo in number)
  IS
  /**********************************************************
  Lista el historial de informes de subsistencia en un cursor
  **********************************************************/
  BEGIN

    OPEN o_Cursor FOR
      SELECT ta.id_documento, ta.n_documento
           , decode(t.id_proceso, 5, 'HISTORICO', t.expediente) expediente
           , decode(t.id_proceso, 5, 'HISTORICO', t.sticker) sticker
           , ta.id_legajo, t.id_tramite_ipj, ta.id_tramiteipj_accion
           , ta.id_tipo_accion
           , (SELECT to_char(MAX(tae.fecha_pase), 'dd/mm/rrrr') FROM ipj.t_tramitesipj_acciones_estado tae WHERE ta.id_tramiteipj_accion = tae.id_tramiteipj_accion) fecha
        FROM ipj.t_tramitesipj_acciones ta
        JOIN ipj.t_tramitesipj t ON ta.id_tramite_ipj = t.id_tramite_ipj
       WHERE ta.id_tipo_accion = p_id_accion
         AND ta.id_legajo = p_id_legajo
         AND t.id_estado_ult >= ipj.types.c_Estados_Completado
         AND t.id_estado_ult < ipj.types.c_Estados_Rechazado
         AND ta.id_estado = ipj.types.c_Estados_Cerrado;

  END SP_Hist_Inf_Subsistencia;

  PROCEDURE SP_Traer_Pendientes_SUAC(
    o_Cursor OUT types.cursorType,
    p_id_ubicacion in number
  )
  IS
  /****************************************************
   Este procedimiento muestra todos los tr�mites completos, finalizados en el Portal Web
   que se generan de forma 100% digital y no pasan por mesa SUAC, pero a�n estan pendientes.

   ITEM #11656 - Adjuntar documentaci�n Constituci�n SAS - Bandeja "Tr�mites Digitales" - Constituci�n SAS. Se agrega orden para const SAS
   BUG #15721 - Agrego tipo/subtipo tr�mite de ACyF modelo en calculo de valor de columna "Express"
   BUG #16013 - Agrego tipo/subtipo tr�mite de SA modelo en calculo de valor de columna "Express"
  *****************************************************/
  BEGIN

    OPEN o_Cursor FOR
      select tIPJ.id_tramite_ipj, tIPJ.expediente, tIPJ.sticker, tIPJ.codigo_online,
        tIPJ.Id_Ubicacion, tIPJ.Id_Clasif_Ipj, tIPJ.Id_Tramite, tIPJ.Urgente, tIPJ.Simple_Tramite, tIPJ.Cuil_Creador,
        to_char(vt_suac.fecha_inicio, 'dd/mm/rrrr') fecha_inicio,
        to_char(vt_suac.fecha_creacion , 'dd/mm/rrrr') fecha_creacion,
        vt_suac.nota_expediente, vt_suac.asunto,
        ( select count(1)
          from ipj.t_ol_entidades eo
          where eo.codigo_online = tIPJ.codigo_online
            and ((eo.id_tipo_tramite_ol = 1 and eo.id_sub_tramite_ol = 1) or
                 (eo.id_tipo_tramite_ol = 20 and eo.id_sub_tramite_ol = 5) or
                 (eo.id_tipo_tramite_ol = 17 and eo.id_sub_tramite_ol = 7) or
                 (eo.id_tipo_tramite_ol = 23 and eo.id_sub_tramite_ol = 9) or
                 (eo.id_tipo_tramite_ol = 52 and eo.id_sub_tramite_ol = 11))
        ) Express
      from
        ipj.t_tramitesIPJ tIPJ join NUEVOSUAC.VT_TRAMITES_IPJ_DESA vt_suac
          on VT_SUAC.ID_TRAMITE = tIPJ.id_tramite
      where
        tIPJ.id_ubicacion_origen = p_id_ubicacion
        and nvl(tIPJ.id_estado_ult, 1) = 1
        and IPJ.TRAMITES.FC_ES_TRAM_DIGITAL(tIPJ.id_tramite_ipj) > 0
      order by Express DESC, VT_SUAC.nro_sticker;

  END SP_Traer_Pendientes_SUAC;


  PROCEDURE SP_Traer_Observados_SUAC(
    o_Cursor OUT types.cursorType,
    p_id_ubicacion in number
  )
  IS
  /****************************************************
   Este procedimiento muestra todos los tr�mites generados en forma 100% digital y que
   se encuentran en estado Observado o ya Subsanado por el ciudadano
  *****************************************************/
  BEGIN

    OPEN o_Cursor FOR
      select tIPJ.id_tramite_ipj, tIPJ.expediente, tIPJ.sticker, tIPJ.codigo_online,
        tIPJ.Id_Ubicacion, tIPJ.Id_Clasif_Ipj, tIPJ.Id_Tramite, tIPJ.Urgente, tIPJ.Simple_Tramite, tIPJ.Cuil_Creador,
        to_char(vt_suac.fecha_inicio, 'dd/mm/rrrr') fecha_inicio,
        to_char(vt_suac.fecha_creacion , 'dd/mm/rrrr') fecha_creacion,
        vt_suac.nota_expediente, vt_suac.asunto, tIPJ.Id_Estado_Ult as id_estado,
        es.n_estado,
        ( select count(1)
          from ipj.t_ol_entidades eo
          where eo.codigo_online = tIPJ.codigo_online
            and (eo.id_tipo_tramite_ol = 20 and eo.id_sub_tramite_ol = 5) OR (eo.id_tipo_tramite_ol = 52 and eo.id_sub_tramite_ol = 11)
        ) Express
      from ipj.t_tramitesIPJ tIPJ
          join NUEVOSUAC.VT_TRAMITES_IPJ_DESA vt_suac on VT_SUAC.ID_TRAMITE = tIPJ.id_tramite
          join ipj.t_estados es on es.id_estado = tIPJ.Id_Estado_Ult
      where
        tIPJ.id_ubicacion_origen = p_id_ubicacion
        and nvl(tIPJ.id_estado_ult, 1) in (5, 6)
        and exists (select * from IPJ.T_ENTIDADES_DOC_ADJUNTOS da where da.id_tramite_ipj = tIPJ.id_tramite_ipj)
      order by tIPJ.id_estado_ult desc, Express DESC, VT_SUAC.nro_sticker;

  END SP_Traer_Observados_SUAC;

  PROCEDURE SP_Cancelar_Edicto_Boe(
    o_rdo OUT VARCHAR2,
    o_tipo_mensaje OUT NUMBER,
    p_Id_Tramite_Ipj IN NUMBER,
    p_Id_Legajo IN NUMBER,
    p_Nro_Edicto IN NUMBER,
    p_id_tramite_ipj_accion IN NUMBER)
  IS
  /*************************************************************************
    Cancela el edicto boe desde el sistema de Gesti�n. Estado Cancelado IPJ
  **************************************************************************/
  v_Row_TramiteIPJ_Accion  IPJ.T_TramitesIPJ_Acciones%ROWTYPE;
  BEGIN
    -- Actualizo el estado del edicto
    UPDATE ipj.t_entidades_edicto_boe e
       SET e.id_estado_boe = 5
         , e.fecha_cancela = to_date(SYSDATE, 'dd/mm/rrrr')
     WHERE e.id_tramite_ipj = p_Id_Tramite_Ipj
       AND e.id_legajo = p_Id_Legajo
       AND e.nro_edicto = p_Nro_Edicto;

    -- Rechazo la acci�n del Edicto
    update ipj.t_tramitesipj_acciones a
    set id_estado = IPJ.TYPES.C_ESTADOS_RECHAZADO
    where
      a.id_tramite_ipj = p_Id_Tramite_Ipj and
      a.id_tramiteipj_accion  = p_id_tramite_ipj_accion;

    -- Reactivo las acciones que estaban pendientes de BOE
    update ipj.t_tramitesipj_acciones a
    set id_estado = IPJ.TYPES.C_ESTADOS_PROCESO
    where
      a.id_tramite_ipj = p_Id_Tramite_Ipj and
      a.id_estado = IPJ.TYPES.C_ESTADOS_BOE;

    -- Guardo el hist�rico de estados de acciones
    select * into v_Row_TramiteIPJ_Accion
    from IPJ.T_TRAMITESIPJ_ACCIONES
    where
        Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        id_tramiteipj_accion = p_id_tramite_ipj_accion;

    IPJ.TRAMITES.SP_GUARDAR_TRAMITES_ACC_ESTADO(
          o_rdo => o_rdo,
          o_tipo_mensaje => o_tipo_mensaje,
          p_fecha_pase => sysdate,
          p_id_estado => v_Row_TramiteIPJ_Accion.Id_Estado,
          p_cuil_usuario => v_Row_TramiteIPJ_Accion.Cuil_Usuario,
          p_observacion => v_Row_TramiteIPJ_Accion.Observacion,
          p_Id_Tramite_Ipj => v_Row_TramiteIPJ_Accion.Id_Tramite_Ipj,
          p_id_tramite_Accion => v_Row_TramiteIPJ_Accion.id_tramiteIpj_Accion,
          p_id_tipo_accion => v_Row_TramiteIPJ_Accion.id_tipo_accion,
          p_id_documento => v_Row_TramiteIPJ_Accion.id_documento,
          p_n_documento => v_Row_TramiteIPJ_Accion.n_documento);

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;

  EXCEPTION
    WHEN OTHERS THEN
      o_rdo := 'SP_Cancelar_Edicto_Boe - ' || To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Cancelar_Edicto_Boe;

  PROCEDURE SP_Act_Estado_Accion(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj IN NUMBER,
    p_id_tramiteIpj_Accion in number,
    p_id_estado in NUMBER)
  IS
  /**********************************************************
    Actualiza el estado de las acciones y tramites anexados.
  **********************************************************/
    v_RowAcciones ipj.t_Tramitesipj_Acciones%ROWTYPE;
    v_id_pagina ipj.t_tipos_accionesipj.id_pagina%TYPE;
  BEGIN

    SELECT ta.*
      INTO v_RowAcciones
      FROM ipj.t_tramitesipj_acciones ta
     WHERE ta.id_tramiteipj_accion = p_id_tramiteIpj_Accion
       AND ta.id_tramite_ipj = p_Id_Tramite_Ipj;

    SELECT tac.id_pagina
      INTO v_id_pagina
      FROM ipj.t_tipos_accionesipj tac
     WHERE tac.id_tipo_accion = v_RowAcciones.Id_Tipo_Accion;

    DBMS_OUTPUT.PUT_LINE('SP_ACT_ESTADO_ACCION - Crea Accion');
        IPJ.TRAMITES.SP_GUARDAR_TRAMITEIPJ_ACC(
          o_rdo => o_rdo,
          o_tipo_mensaje => o_tipo_mensaje,
          p_Id_Tramite_Ipj => v_RowAcciones.Id_Tramite_Ipj,
          p_id_tramiteipj_accion => p_id_tramiteIpj_Accion,
          p_id_tipo_accion =>  v_RowAcciones.Id_Tipo_Accion,
          p_id_integrante => v_RowAcciones.Id_Integrante,
          p_dni_integrante => NULL,
          p_id_legajo => v_RowAcciones.Id_Legajo,
          p_cuit_persjur => NULL,
          p_id_estado => p_id_estado,
          p_cuil_usuario => v_RowAcciones.Cuil_Usuario,
          p_obs_rubrica => v_RowAcciones.Obs_Rubrica,
          p_observacion => v_RowAcciones.Observacion,
          p_id_fondo_comercio => v_RowAcciones.Id_Fondo_Comercio,
          p_fec_veeduria => v_RowAcciones.Fec_Veeduria,
          p_id_documento => v_RowAcciones.Id_Documento,
          p_n_documento => v_RowAcciones.n_Documento,
          p_id_pagina => v_id_pagina);

    IF o_tipo_mensaje <> IPJ.TYPES.C_TIPO_MENS_OK THEN
      o_rdo := o_rdo;
      o_tipo_mensaje := o_tipo_mensaje;
      RETURN;
    ELSE
      o_rdo := IPJ.TYPES.C_RESP_OK;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    END IF;

    COMMIT;

  EXCEPTION
    WHEN OTHERS THEN
      o_rdo := 'SP_Act_Estado_Accion - ' || To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Act_Estado_Accion;

  PROCEDURE SP_Traer_Pend_Asoc_CUIT(
    o_Cursor OUT types.cursorType,
    p_id_ubicacion in number
    )
  IS
  /****************************************************
   Este procedimiento trae todas las acciones pendientes de firma, para el �rea
   a la que pertenece el usuario
  *****************************************************/
    v_SQL varchar2(4000);
    v_Lista_Acciones varchar2(1000);
  BEGIN
    -- Busco el listado de acciones que se asocian al cuit
    select valor_variable into v_Lista_Acciones
    from ipj.t_config c
    where
      c.id_variable = 'ASOC_CUIT_ACCIONES';

    v_SQL :=
      'select ac.id_tramite_ipj, ac.id_tramiteipj_accion, ac.id_tipo_accion, ' ||
      '  ac.id_documento, ac.n_documento, to_char(t.fecha_ini_suac, ''dd/mm/rrrr'') fecha_ini_suac, ' ||
      '  IPJ.TRAMITES_SUAC.FC_BUSCAR_ASUNTO_SUAC (t.id_tramite) Asunto, ' ||
      '  t.expediente, t.sticker, ac.id_tipo_documento_cdd, ' ||
      '  (select td.n_tipo_documento from cdd.vt_tipos_documento_ipj td where td.id_tipo_documento = ac.id_tipo_documento_cdd and td.id_aplicacion = 1000) n_tipo_documento_cdd, ' ||
      '  (select cuit from ipj.t_legajos l where l.id_legajo = ac.id_legajo) cuit, ' ||
      '  (select ta.n_tipo_accion from ipj.t_tipos_accionesipj ta where ta.id_tipo_accion = ac.id_tipo_accion) n_tipo_accion, ' ||
      '  (select IPJ.VARIOS.FC_Letra_Matricula(nro_ficha) from ipj.t_legajos l where l.id_legajo = ac.id_legajo) Letra, ' ||
      '  (select IPJ.VARIOS.FC_Numero_Matricula(nro_ficha) from ipj.t_legajos l where l.id_legajo = ac.id_legajo) Matricula, ' ||
      '  (select denominacion_sia from ipj.t_legajos l where l.id_legajo = ac.id_legajo) denominacion, ' ||
      '  (SELECT e.matricula_version FROM ipj.t_entidades e WHERE e.id_tramite_ipj = t.id_tramite_ipj) Matricula_Version, ' ||
      '  ipj.tramites.FC_Buscar_Fecha_Inf(ac.id_tramite_ipj, ac.id_tramiteipj_accion, ac.id_tipo_accion) Fecha_Emision, ' ||
      '  ( select a.nro_resolucion_dig || ''"'' || a.id_prot_dig || ''"'' || ''/'' || SUBSTR(a.fec_resolucion_dig, 7)' ||
      '    from ipj.t_tramitesipj_acciones a' ||
      '    where a.id_tramite_ipj = ac.id_tramite_ipj and a.id_tramiteipj_accion = ac.id_tramiteipj_accion' ||
      '  ) Resolucion ' ||
      'from ipj.t_tramitesipj_acciones ac join ipj.t_tramitesipj t ' ||
      '  on ac.id_tramite_ipj = t.id_tramite_ipj ' ||
      'where ' ||
      '  ac.id_tipo_accion in (' || v_Lista_Acciones || ') and ' ||
      '  ac.id_documento is not null and ac.id_documento <> 0 and ' ||
      '  t.id_ubicacion_origen = ' || to_char(p_id_ubicacion) || ' and ' ||
      '  ac.id_estado in (100, 110) and ' || -- Acciones Completadas y Cerradas
      '  t.id_estado_ult between 100 and 199 and ' ||  -- Tr�mites completos o cerrados
      '  nvl(ac.asociado_cuit, ''S'') = ''N'' ' ||
      'order by t.fecha_ini_suac asc ';

    OPEN o_Cursor FOR v_SQL;

  END SP_Traer_Pend_Asoc_CUIT;

  PROCEDURE SP_Traer_Seccion_Inf(
    o_Cursor OUT types.cursorType,
    p_id_tramite_ipj in number)
  IS
   /**********************************************************
    Lista los tipos de secciones de informes, indicando cuales ya han sido seleccionados
  **********************************************************/
  BEGIN

    OPEN o_Cursor FOR
      select ts.id_secc_inf_rpc, ts.n_secc_inf_rpc, ts.orden, ts.orden_inf,
        ( select decode(id_tramite_ipj, null, 'N', 'S')
           from IPJ.T_ENTIDADES_SECC_INF es
           where
             es.id_tramite_ipj = p_id_tramite_ipj and
             ts.id_secc_inf_rpc = es.id_secc_inf_rpc
         ) Seleccionado
      from ipj.t_tipos_seccion_inf_rpc ts
      order by ts.orden asc;

  END SP_Traer_Seccion_Inf;

  PROCEDURE SP_Perimir_Tramite(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number)
  IS
    v_Row_Tramite IPJ.T_TRAMITESIPJ%rowtype;
    v_row_Acc_Perencion IPJ.T_TRAMITESIPJ_ACCIONES%rowtype;
    v_id_tramite_out number;
    v_id_clasif_arh number;
    v_responsable_arch varchar2(20);
    v_area_suac varchar2(500);
    v_error varchar2(500);
    v_Cursor_Tramite  types.cursorType;
    v_SUAC IPJ.TRAMITES_SUAC.Tipo_SUAC;
    v_id_ult_tramite number;
    v_id_ult_accion number;
    v_id_tipo_ent_legajo number;
    v_id_tipo_ent_ult number;
  BEGIN
    -- Busco los datos del tr�mite
    select * into v_Row_Tramite
    from IPJ.t_tramitesipj
    where
      Id_Tramite_Ipj = p_Id_Tramite_Ipj;

    -- Solo tiene permiso JURIDICO
    if v_Row_Tramite.id_ubicacion <> IPJ.TYPES.C_AREA_JURIDICO then
      o_rdo := 'Esta operaci�n solo se permite para el �rea de JURIDICO.';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      return;
    end if;

     -- Busco los datos del tr�mite SUAC
    NUEVOSUAC.PCK_INTERFAZ_VERTICALES_V2.P_GET_TRAMITE_BY_ID(v_Row_Tramite.id_tramite, v_Cursor_Tramite, v_error);
    if nvl(v_error, IPJ.TYPES.C_RESP_OK) <> IPJ.TYPES.C_RESP_OK then
      o_rdo :=  'No se encontr� el tr�mite en SUAC.';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      return;
    end if;

    loop
      fetch v_Cursor_Tramite into v_SUAC;

      EXIT WHEN v_Cursor_Tramite%NOTFOUND or v_Cursor_Tramite%NOTFOUND is null;
    end loop;
    close v_Cursor_Tramite;

    if v_SUAC.v_unidad_actual <> 'AREA JURIDICA DE LA DIR INSP PERS JUR' then
      o_rdo := 'El tr�mite no se encuentra en AREA JURIDICA DE LA DIR INSP PERS JUR en SUAC.';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      return;
    end if;

    -- Busco la acci�n de Perenci�n para tomar sus datos
    begin
      select * into v_row_Acc_Perencion
      from ipj.t_tramitesipj_acciones
      where
        id_tramite_ipj = p_id_tramite_ipj and
        id_tipo_accion = 118; -- Accion de Resolucion de Perencion
    exception
      when NO_DATA_FOUND then
        o_rdo := 'Este expediente no cuenta con una acci�n de Perenci�n.';
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
        return;
    end;

    -- Si no tiene Acc. Perenci�n cerrada, no continua
    if v_row_Acc_Perencion.id_estado not in (IPJ.TYPES.C_ESTADOS_CERRADO, IPJ.TYPES.C_ESTADOS_COMPLETADO) then
      o_rdo := 'La acci�n de Perenci�n no se encuentra cerrada.';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      return;
    end if;

    -- Busco los datos del Area Archivo
    select c.id_clasif_ipj into v_id_clasif_arh
    from ipj.t_tipos_clasif_ipj c
    where c.id_ubicacion = IPJ.TYPES.C_AREA_ARCHIVO;

    select u.cuil_usuario into v_responsable_arch
    from ipj.t_ubicaciones u
    where u.id_ubicacion = IPJ.TYPES.C_AREA_ARCHIVO;

    -- Realizo la transferencia en SUAC del area al archivo
    IPJ.TRAMITES_SUAC.SP_Realizar_Tranf_IPJ(
      o_rdo => o_rdo,
      o_tipo_mensaje => o_tipo_mensaje,
      p_id_tramite_suac => v_Row_Tramite.id_tramite,
      p_id_ubicacion_origen => v_Row_Tramite.id_ubicacion,
      p_id_ubicacion_destino => IPJ.TYPES.C_AREA_ARCHIVO,
      p_cuil_usuario_origen => v_Row_Tramite.cuil_ult_estado,
      p_cuil_usuario_destino => v_responsable_arch
    );

    if o_rdo <> IPJ.TYPES.C_RESP_OK then
      o_rdo :=  'Error al realizar transferencia en SUAC: ' || o_rdo;
      rollback;
      return;
    end if;

    -- Paso a Archivo el tr�mite
    IPJ.TRAMITES.SP_GUARDAR_TRAMITE(
      o_id_tramite_ipj => v_id_tramite_out,
      o_rdo => o_rdo,
      o_tipo_mensaje => o_tipo_mensaje,
      v_id_tramite_ipj => p_id_tramite_ipj,
      v_id_tramite => v_Row_Tramite.id_tramite,
      v_id_Clasif_IPJ => v_id_clasif_arh,
      v_id_Grupo  => 0,
      v_cuil_ult_estado => v_responsable_arch,
      v_id_estado => 201, -- Perimido
      v_cuil_creador => v_Row_Tramite.cuil_creador,
      v_observacion => '',
      v_id_Ubicacion => IPJ.TYPES.C_AREA_ARCHIVO,
      v_urgente => v_Row_Tramite.Urgente,
      p_sticker => v_Row_Tramite.Sticker,
      p_expediente => v_Row_Tramite.Expediente,
      p_simple_tramite => 'N'
    );

    -- Si no puedo cambiar el tr�mite a estado PERIMIDO, lo cierro
    if o_rdo <> IPJ.TYPES.C_RESP_OK then
      rollback;
      return;
    end if;

    -- Limpio la configuraci�n de Archivo, y asigno la resolucion como Obs.
    update ipj.t_archivo_tramite
    set
      observacion = 'Perimido por resoluci�n ' || to_char(v_row_Acc_Perencion.nro_resolucion) ||' / ' || v_row_Acc_Perencion.id_prot_jur || ' con fecha ' || to_char(v_row_Acc_Perencion.fec_resolucion, 'dd/mm/rrrr'),
      nro_tabla = null,
      nro_caja = null,
      id_proveedor = null,
      id_paquete = null,
      anio = null,
      id_accion_archivo = 10 -- Perimido
    where
      id_tramite_ipj = p_id_tramite_ipj;

    --  Si es Constituci�n y se perime, se da de baja el legajo, y se marca la baja en entidad si existe
    if IPJ.TRAMITES.FC_Es_Tram_Const (p_id_tramite_ipj) > 0 then
      update ipj.t_legajos
      set fecha_baja_entidad = v_row_Acc_Perencion.fec_resolucion
      where
        id_legajo = v_row_Acc_Perencion.id_legajo;

      update ipj.t_entidades
      set
        Baja_Temporal = v_row_Acc_Perencion.fec_resolucion,
        id_estado_entidad = 'P', -- Perimida
        id_tipo_origen = 19, -- Perimida
        obs_cierre = 'Perimido por resoluci�n ' || to_char(v_row_Acc_Perencion.nro_resolucion) ||' / ' || v_row_Acc_Perencion.id_prot_jur || ' con fecha ' || to_char(v_row_Acc_Perencion.fec_resolucion, 'dd/mm/rrrr')
      where
        id_tramite_ipj = p_id_tramite_ipj and
        id_legajo = v_row_Acc_Perencion.id_legajo;

      -- Actualizo los datos en Gobierno, porque se da de baja
      IPJ.ENTIDAD_PERSJUR.SP_ACTUALIZAR_ENT_GOB(
        o_rdo => o_rdo,
        o_tipo_mensaje => o_tipo_mensaje,
        p_Id_Tramite_Ipj => p_id_tramite_ipj,
        p_id_legajo => v_row_Acc_Perencion.id_legajo
      );
    else
     -- Si no es Cosnt. solo p�ngo matr�cula en -1
      update ipj.t_entidades
      set
        matricula_version = -1
      where
        id_tramite_ipj = p_id_tramite_ipj and
        id_legajo = v_row_Acc_Perencion.id_legajo;
    end if;

    -- Si perimo un tr�mite de TRANSFORMACION, se vuelve atras el cambio
    if IPJ.TRAMITES.FC_Es_Tram_Transf (p_id_tramite_ipj) > 0 then
      DBMS_OUTPUT.PUT_LINE(' - SP_Finalizar_Firma (8,5): Vuelve atras una transformaci�n.');
      -- Busco el �ltimo tr�mite, a ver su tipo de entidad
      IPJ.ENTIDAD_PERSJUR.SP_Buscar_Entidad_Tramite(
        o_Id_Tramite_Ipj => v_id_ult_tramite,
        o_id_tramiteipj_accion => v_id_ult_accion,
        p_id_legajo => v_row_Acc_Perencion.id_legajo,
        p_Abiertos => 'N');

      -- Busco el ultimo tipo de entidad actual del legajo
      select id_tipo_entidad into v_id_tipo_ent_legajo
      from ipj.t_legajos
      where
        id_legajo = v_row_Acc_Perencion.id_legajo;

     -- Busco el tipo de entidad actual del ultimo tramite
      begin
        select id_tipo_entidad into v_id_tipo_ent_ult
        from ipj.t_entidades
        where
          id_legajo = v_row_Acc_Perencion.id_legajo and
          id_tramite_ipj = v_id_ult_tramite;
      exception
        -- Si no hay datos, lo dejo como esta
        when NO_DATA_FOUND then
          v_id_tipo_ent_ult := v_id_tipo_ent_legajo;
      end;

      -- Si el ultimo tr�mites es ditinto al legajo, lo vulevo atras
      if nvl(v_id_tipo_ent_ult, 0) > 0 and v_id_tipo_ent_ult <> v_id_tipo_ent_legajo then
        update ipj.t_legajos
        set id_tipo_entidad = v_id_tipo_ent_ult
        where
          id_legajo = v_row_Acc_Perencion.id_legajo;
      end if;
    end if;

    if o_rdo <> IPJ.TYPES.C_RESP_OK then
      rollback;
      return;
    end if;

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    commit;
  EXCEPTION
    WHEN OTHERS THEN
      o_rdo := 'SP_Perimir_Tramite - ' || To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Perimir_Tramite;

  PROCEDURE SP_Crear_Alerta(
    o_rdo OUT VARCHAR2,
    o_tipo_mensaje OUT NUMBER,
    p_Id_Tramite_Ipj IN NUMBER,
    p_Id_Tramiteipj_Accion IN NUMBER,
    p_Id_Tipo_Accion IN NUMBER,
    p_Fec_Alerta IN DATE,
    p_Id_Tipo_Alerta IN NUMBER,
    p_Id_Estado_Alerta IN NUMBER,
    p_Id_Ubicacion IN NUMBER,
    p_Cuil_Usr_Origen IN VARCHAR2)
  IS

  BEGIN
    INSERT INTO ipj.t_alertas(id_alerta,id_tramite_ipj,id_tramiteipj_accion,id_tipo_accion,fec_alerta,id_tipo_alerta,id_estado_alerta,id_ubicacion,cuil_usr_origen)
    VALUES((SELECT nvl(MAX(a.id_alerta),0)+1 FROM ipj.t_alertas a),p_Id_Tramite_Ipj,p_Id_Tramiteipj_Accion,p_Id_Tipo_Accion,p_Fec_Alerta,p_Id_Tipo_Alerta,p_Id_Estado_Alerta,p_Id_Ubicacion,p_Cuil_Usr_Origen);

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    COMMIT;
  EXCEPTION
    WHEN OTHERS THEN
      o_rdo := 'SP_Crear_Alerta - ' || To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Crear_Alerta;

  PROCEDURE SP_Traer_Alertas (
    o_rdo OUT VARCHAR2,
    o_tipo_mensaje OUT NUMBER,
    o_Cursor OUT types.cursorType,
    p_Id_Tipo_Alerta IN NUMBER,
    p_Id_Ubicacion IN NUMBER,
    p_Cuil_Usr_Origen IN VARCHAR2)
  IS

  BEGIN

    OPEN o_Cursor FOR
      SELECT a.id_alerta, t.n_tipo_alerta, a.fec_alerta, a.cuil_usr_origen, e.n_estado_alerta, tr.expediente, tr.sticker, ta.n_tipo_accion, l.denominacion_sia
        FROM ipj.t_alertas a
        JOIN ipj.t_Tipos_Alerta t ON a.id_tipo_alerta = t.id_tipo_alerta
        JOIN ipj.t_estados_alerta e ON a.id_estado_alerta = e.id_estado_alerta
        JOIN ipj.t_tramitesipj tr ON a.id_tramite_ipj = tr.id_tramite_ipj
        JOIN ipj.t_tipos_accionesipj ta ON a.id_tipo_accion = ta.id_tipo_accion
        JOIN ipj.t_tramitesipj_acciones tac ON a.id_tramite_ipj = tac.id_tramite_ipj and a.id_tramiteipj_accion = tac.id_tramiteipj_accion
        JOIN ipj.t_tramitesipj_persjur tp ON tr.id_tramite_ipj = tp.id_tramite_ipj and tp.id_legajo = tac.id_legajo
        JOIN ipj.t_legajos l ON tp.id_legajo = l.id_legajo
       WHERE a.id_tipo_alerta = decode(nvl(p_Id_Tipo_Alerta,0),0,a.id_tipo_alerta,p_Id_Tipo_Alerta)
         AND a.id_ubicacion = p_Id_Ubicacion
         AND tac.cuil_usuario = REPLACE(p_Cuil_Usr_Origen,'-','')
         AND a.id_estado_alerta = 1;

    o_Rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
    WHEN OTHERS THEN
      o_rdo := 'SP_Traer_Alertas - ' || To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Traer_Alertas;

  PROCEDURE SP_Actualizar_Alerta(
    o_rdo OUT VARCHAR2,
    o_tipo_mensaje OUT NUMBER,
    p_Id_Alerta IN NUMBER,
    p_Id_Estado_Alerta IN NUMBER)
  IS
  BEGIN
    -- Actualiza el estado de la alerta
    UPDATE ipj.t_alertas a
       SET a.id_estado_alerta = p_Id_Estado_Alerta
     WHERE a.id_alerta = p_Id_Alerta;

    COMMIT;

    o_rdo := IPJ.TYPES.c_Resp_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;

  EXCEPTION
    WHEN OTHERS THEN
      o_rdo := 'SP_Actualizar_Alerta: ' || To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Actualizar_Alerta;

  PROCEDURE SP_Traer_TramitesIPJ_Bandeja(
    p_Cuil in varchar2,
    p_id_estado in number,
    p_id_clasif in number,
    p_id_ubicacion in number,
    p_Cursor OUT types.cursorType)
  IS
  /****************************************************
   Este procedimiento muestra los tramites y acciones asignadas a un t�cnico o
   a un grupo al cual pertenezca.
   Al unir los datos, indica en la primer columna si es TRAMITE o ACCION
  *****************************************************/
    v_grupo_admin_sa number;
    v_grupo_certif_rpc number;
  BEGIN
    -- Busco los grupos que manejan los tr�mites digitales recien ingresados
    v_grupo_admin_sa := to_number(IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('GRUPO_ADM_SA'));
    v_grupo_certif_rpc := to_number(IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('GRUPO_ADM_RP'));

    OPEN p_Cursor FOR
      select
        tmp.botones,
        tmp.Clase, tmp.id_tramite Id_Tramite_Suac, tmp.Sticker, tmp.fecha_ini_suac Fecha_Inicio_Suac,
        null Fecha_recepcion,
        (case tmp.id_proceso
           when 4 then 'GENERACION DE HISTORICOS - ' || tmp.N_UBICACION
           when 5 then 'CARGA DE HISTORICOS'
           else IPJ.TRAMITES_SUAC.FC_BUSCAR_SUBTIPO_SUAC(tmp.id_tramite)
        end) Tipo,
        (case tmp.id_proceso
            when 4 then 'GENERACION DE HISTORICOS - ' || tmp.N_UBICACION
            when 5 then tmp.observacion
            else IPJ.TRAMITES_SUAC.FC_BUSCAR_ASUNTO_SUAC(tmp.id_tramite)
        end) Asunto,
        tmp.Expediente Nro_Expediente, tmp.Id_Tramite_Ipj, tmp.id_clasif_ipj,
        tmp.id_tipo_accion, tmp.Observacion, tmp.id_estado, tmp.cuil_usuario, tmp.n_estado,
        tmp.Tipo_IPJ, tmp.Fecha_Asignacion, tmp.Acc_Abiertas, tmp.Acc_Cerradas, tmp.URGENTE,
        tmp.usuario, nvl(tmp.Error_Dato, 'N') Error_Dato, tmp.CUIT, tmp.razon_social, tmp.id_legajo,
        tmp.N_UBICACION, tmp.CUIL_CREADOR, tmp.id_ubicacion, tmp.id_tramiteipj_accion,
        tmp.id_protocolo, tmp.ID_INTEGRANTE, tmp.NRO_DOCUMENTO, tmp.Cuit_PersJur,
        tmp.Obs_Rubrica, tmp.identificacion, tmp.persona, tmp.ID_FONDO_COMERCIO,
        tmp.id_proceso, tmp.id_pagina, tmp.cuil_usuario cuil_usuario_old, tmp.ID_DOCUMENTO,
        tmp.N_DOCUMENTO,tmp.simple_tramite, tmp.Agrupable, tmp.codigo_online,
        ( select count(1)
          from ipj.t_ol_entidades eo
          where
            eo.codigo_online = tmp.codigo_online and
            ((eo.id_tipo_tramite_ol = 1 and eo.id_sub_tramite_ol = 1) or
             (eo.id_tipo_tramite_ol = 20 and eo.id_sub_tramite_ol = 5) or
             (eo.id_tipo_tramite_ol = 17 and eo.id_sub_tramite_ol = 7) or
             (eo.id_tipo_tramite_ol = 23 and eo.id_sub_tramite_ol = 9) or
             (eo.id_tipo_tramite_ol = 52 and eo.id_sub_tramite_ol = 11) or
             eo.id_tipo_tramite_ol = 19)
        ) Const_SA,
        (select eo.id_obj_social from ipj.t_ol_entidades eo where eo.codigo_online = tmp.codigo_online) id_Obj_Social,
        tmp.id_tramite_ipj_padre,
        --IPJ.TRAMITES_SUAC.FC_BUSCAR_SUBTIPO_SUAC(tmp.id_tramite) id_subtipo_tramite,
        ipj.tramites.FC_N_Subtipo_Suac(IPJ.TRAMITES_SUAC.FC_BUSCAR_SUBTIPO_SUAC(tmp.id_tramite), tmp.id_tramite) id_subtipo_tramite,
        (select count(1) from IPJ.T_ENTIDADES_AUTORIZADOS ea where ea.id_tramite_ipj = tmp.id_tramite_ipj and nvl(ea.mail_autorizado, 'N') = 'S') dom_electronico,
        ipj.tramites.FC_Es_Tram_Digital(tmp.id_tramite_ipj) Es_Digital,
        (select nvl(tr.enviar_mail, 0) from ipj.t_tramitesipj tr where tr.id_tramite_ipj = tmp.id_tramite_ipj) enviar_mail,
        (select o.cod_seguridad from ipj.t_ol_entidades o where o.codigo_online = tmp.codigo_online) cod_seguridad
      from
        (
          -- filtra que el usuario sea el asignado, o este en el grupo
          select *
          from ipj.vt_tramite_tecn_ipj_cab vtIPJ
          where
            VTIPJ.ID_ESTADO < IPJ.TYPES.C_ESTADOS_COMPLETADO and
            (VTIPJ.ID_GRUPO_ULT_ESTADO not in (v_grupo_admin_sa, v_grupo_certif_rpc) or VTIPJ.ID_ESTADO not in (1, 5, 6)) and
            ( vtIPJ.cuil_usuario = p_Cuil or vtIPJ.cuil_usuario_grupo = p_Cuil ) and
            (nvl(p_id_estado, 0) = 0 or vtIPJ.Id_Estado = p_id_estado) and
            (nvl(p_id_clasif, 0) = 0 or vtIPJ.id_clasif_ipj = p_id_clasif)

          UNION ALL
          -- Un tramite que tenga una accion asignada al usuario, pero no el tr�mite
          select *
          from IPJ.VT_TRAMITE_TECN_IPJ_cab vtIPJ
          where
            VTIPJ.ID_ESTADO < IPJ.TYPES.C_ESTADOS_COMPLETADO and
            vtIPJ.Clase = 1 and
            (VTIPJ.ID_GRUPO_ULT_ESTADO not in (v_grupo_admin_sa, v_grupo_certif_rpc) or VTIPJ.ID_ESTADO not in (1, 5, 6)) and
            vtIPJ.cuil_usuario <> p_Cuil and
            (vtIPJ.cuil_usuario_grupo is null or vtIPJ.cuil_usuario_grupo <> p_Cuil) and
            vtIPJ.Id_Tramite_Ipj in (select distinct Id_Tramite_Ipj
                                              from ipj.t_tramitesipj_acciones  acc
                                              where Acc.Cuil_Usuario = p_Cuil and acc.Id_Estado < IPJ.TYPES.C_ESTADOS_COMPLETADO) and
            (nvl(p_id_estado, 0) = 0 or vtIPJ.Id_Estado = p_id_estado) and
            (nvl(p_id_clasif, 0) = 0 or vtIPJ.id_clasif_ipj = p_id_clasif)

        UNION ALL
        -- Bug 10941:[SG] - [Transferencia de Acciones] - Visualizar tr�mites que no son del usuario, pero que tienen acciones del usuario
        select distinct 0 botones, clase, id_tramite, id_tramite_ipj, id_clasif_ipj, id_tipo_accion, observacion, id_estado, id_estado_tramite, id_estado_accion
             , cuil_usuario, n_estado, tipo_ipj, fecha_asignacion, acc_abiertas, acc_cerradas, urgente, usuario, error_dato, cuit, razon_social, id_legajo
             , n_ubicacion, cuil_creador, id_ubicacion, id_tramiteipj_accion, id_protocolo, id_integrante, nro_documento, cuit_persjur, obs_rubrica
             , identificacion, persona, id_fondo_comercio, id_proceso, id_pagina, agrupable, null cuil_usuario_grupo, id_grupo_ult_estado, sticker, expediente
             , id_documento, n_documento, simple_tramite, n_ubicacion_origen, codigo_online, id_tramite_ipj_padre, fecha_ini_suac
          from IPJ.VT_TRAMITE_TECN_IPJ_cab vtIPJ
         WHERE
           VTIPJ.ID_ESTADO < IPJ.TYPES.C_ESTADOS_COMPLETADO
           AND vtIPJ.Clase = 1
           and (VTIPJ.ID_GRUPO_ULT_ESTADO not in (v_grupo_admin_sa, v_grupo_certif_rpc) or VTIPJ.ID_ESTADO not in (1, 5, 6))
           AND (nvl(vtIPJ.id_Grupo_Ult_Estado,0) <> 0 AND vtIPJ.CUIL_USUARIO IS NULL)
           AND vtIPJ.Id_Tramite_Ipj in (select distinct Id_Tramite_Ipj
                                          from ipj.t_tramitesipj_acciones  acc
                                         where Acc.Cuil_Usuario = p_Cuil
                                           and acc.Id_Estado < IPJ.TYPES.C_ESTADOS_COMPLETADO)
           AND vtIPJ.Id_Tramite_Ipj NOT IN (select vtIPJ.id_tramite_ipj
                                              from IPJ.VT_TRAMITE_TECN_IPJ vtIPJ
                                             WHERE VTIPJ.ID_ESTADO < IPJ.TYPES.C_ESTADOS_COMPLETADO
                                               -- filtra que el usuario sea el asignado, o este en el grupo
                                               AND (vtIPJ.cuil_usuario = p_Cuil or vtIPJ.cuil_usuario_grupo = p_Cuil)
                                               AND (nvl(p_id_estado, 0) = 0 or vtIPJ.Id_Estado = p_id_estado)
                                               AND (nvl(p_id_clasif, 0) = 0 or vtIPJ.id_clasif_ipj = p_id_clasif)
                                               AND vtIPJ.Clase = 1)

        ) tmp
      WHERE tmp.id_ubicacion = p_id_ubicacion
      order by Const_SA desc , id_tramite_ipj asc, clase asc;

  END SP_Traer_TramitesIPJ_Bandeja;


  PROCEDURE SP_Traer_TramitesIPJ_Band_Det(
    p_Id_Tramite_Ipj IN NUMBER,
    p_Cursor OUT types.cursorType)
  IS
  /****************************************************
   Este procedimiento muestra las acciones de un tr�mite.
  *****************************************************/
  BEGIN
    OPEN p_Cursor FOR
      SELECT IPJ.varios.FC_Habilitar_Botones (TIPJ.id_ubicacion,
                                               tIPJ.id_clasif_ipj,
                                               AccIPJ.id_tipo_Accion,
                                               0,
                                               -1,
                                               AccIPJ.id_estado,
                                               2,
                                               tIPJ.id_proceso,
                                               tIPJ.simple_tramite,
                                               AccIPJ.cuil_usuario,
                                               AccIPJ.cuil_usuario,
                                               tIPJ.id_tramite_ipj,
                                               AccIPJ.id_tramiteipj_accion)
                 botones,
              2 Clase,
              tIPJ.id_tramite,
              AccIPJ.id_tramite_ipj,
              (select ta.id_clasif_ipj from ipj.t_tipos_AccionesIpj ta where ACCIPJ.ID_TIPO_ACCION = ta.id_tipo_accion) id_clasif_ipj,
              AccIPJ.id_tipo_accion,
              AccIPJ.Observacion,
              AccIPJ.id_estado,
              0 id_estado_tramite,
              AccIPJ.id_estado id_estado_accion,
              AccIPJ.cuil_usuario,
              (select n_estado from IPJ.t_estados e where AccIPJ.id_estado = e.id_estado) n_estado,
              (select ta.N_Tipo_Accion from ipj.t_tipos_AccionesIpj ta where ACCIPJ.ID_TIPO_ACCION = ta.id_tipo_accion) Tipo_IPJ,
              IPJ.TRAMITES.FC_Ultima_Fecha_Tramite (AccIPJ.id_tramite_ipj) Fecha_Asignacion,
              0 Acc_Abiertas,
              0 Acc_Cerradas,
              tIPJ.URGENTE,
              (select u.descripcion from IPJ.t_usuarios u where AccIPJ.Cuil_Usuario = u.cuil_usuario) usuario,
              NVL (TRPJ.ERROR_DATO, trInt.Error_Dato) Error_Dato,
              (select L.CUIT from IPJ.T_Legajos L where ACCIPJ.id_legajo = L.id_legajo) cuit,
              (select L.DENOMINACION_SIA from IPJ.T_Legajos L where ACCIPJ.id_legajo = L.id_legajo) razon_social,
              ACCIPJ.id_legajo,
              (select n_ubicacion from IPJ.T_Ubicaciones ub where  TIPJ.ID_UBICACION = ub.id_ubicacion) N_UBICACION,
              '' CUIL_CREADOR,
              TIPJ.id_ubicacion,
              AccIPJ.id_tramiteipj_accion,
              (select NVL (ta.id_protocolo, 0) from ipj.t_tipos_AccionesIpj ta where ACCIPJ.ID_TIPO_ACCION = ta.id_tipo_accion) id_protocolo,
              ACCIPJ.ID_INTEGRANTE,
              (select I.NRO_DOCUMENTO from IPJ.T_INTEGRANTES i where AccIPJ.ID_INTEGRANTE = i.ID_INTEGRANTE) nro_documento,
              (select L.CUIT from IPJ.T_Legajos L where ACCIPJ.id_legajo = L.id_legajo) Cuit_PersJur,
              AccIPJ.Obs_Rubrica,
              NVL ((select I.NRO_DOCUMENTO from IPJ.T_INTEGRANTES i where AccIPJ.ID_INTEGRANTE = i.ID_INTEGRANTE), (select L.CUIT from IPJ.T_Legajos L where ACCIPJ.id_legajo = L.id_legajo)) identificacion,
              NVL ((select fc.n_fondo_comercio from IPJ.t_Fondos_comercio fc where AccIPJ.ID_FONDO_COMERCIO = FC.ID_FONDO_COMERCIO), NVL ((select I.DETALLE from IPJ.T_INTEGRANTES i where AccIPJ.ID_INTEGRANTE = i.ID_INTEGRANTE), (select L.DENOMINACION_SIA from IPJ.T_Legajos L where ACCIPJ.id_legajo = L.id_legajo))) persona,
              AccIPJ.ID_FONDO_COMERCIO,
              tIPJ.id_proceso,
              (select ta.id_pagina from ipj.t_tipos_AccionesIpj ta where ACCIPJ.ID_TIPO_ACCION = ta.id_tipo_accion) id_pagina,
              (select agrupable from IPJ.t_paginas p where p.id_pagina in (select ta.id_pagina from ipj.t_tipos_AccionesIpj ta where ACCIPJ.ID_TIPO_ACCION = ta.id_tipo_accion)) AGRUPABLE,
              '' CUIL_USUARIO_grupo,
              0 id_Grupo_Ult_Estado,
              TIPJ.STICKER,
              TIPJ.EXPEDIENTE,
              AccIPJ.id_documento,
              AccIPJ.n_documento,
              TIPJ.SIMPLE_TRAMITE,
              (SELECT n_ubicacion FROM IPJ.T_Ubicaciones WHERE id_ubicacion = TIPJ.ID_UBICACION_ORIGEN) n_ubicacion_origen,
              tipj.codigo_online,
              tipj.id_tramite_ipj_padre,
              to_char(tipj.fecha_ini_suac, 'dd/mm/rrrr') fecha_ini_suac
         FROM ipj.t_tramitesIPJ tIPJ join ipj.t_tramitesipj_acciones AccIPJ
                 ON AccIPJ.Id_Tramite_IPJ = tIPJ.id_tramite_ipj
              LEFT JOIN ipj.T_TramitesIpj_PersJur TRPJ
                 ON TIPJ.ID_TRAMITE_IPJ = TRPJ.ID_TRAMITE_IPJ
                    AND ACCIPJ.id_legajo = TRPJ.id_legajo
              LEFT JOIN IPJ.T_TRAMITESIPJ_INTEGRANTE trInt
                 ON ACCIPJ.ID_INTEGRANTE = TRINT.ID_INTEGRANTE AND AccIPJ.Id_tramite_ipj = trInt.id_tramite_ipj
        WHERE
          NVL (AccIPJ.Id_Estado, 0) <> 110
          AND tipj.id_tramite_ipj = p_Id_Tramite_Ipj;

  END SP_Traer_TramitesIPJ_Band_Det;

  PROCEDURE SP_Desistir_Tramite(
    o_rdo OUT VARCHAR2,
    o_tipo_mensaje OUT NUMBER,
    p_id_tramite_ipj IN NUMBER,
    p_cuil_usuario in varchar2)
  IS
  /*
    Este procedimiento desiste un tr�mite, realizando las siguientes operaciones:
    - Si es Constituci�n, libera el legajo.
    - Si no es digital, lo registra desitido en Archivo
    - Lo mueve en SUAC
    - Valdia que el usuario pueda desistir
  */
    v_row_tramite ipj.t_tramitesipj%rowtype;
    v_SUAC IPJ.TRAMITES_SUAC.Tipo_SUAC;
    v_id_tramite_out number;
    v_id_clasif_arh number;
    v_responsable_arch varchar2(20);
    v_id_legajo number;
    v_Usr_Hab number;
    v_Notas_Pend number;
    v_ubic_Gestion varchar2(500);
     v_id_ult_tramite number;
     v_id_ult_accion number;
     v_id_tipo_ent_legajo number;
     v_id_tipo_ent_ult number;
  BEGIN
    -- Logueo los par�metros
    if IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('HAB_LOG_TRAMITES') = 'S' then
      IPJ.VARIOS.SP_GUARDAR_LOG(
        p_METODO => 'SP_Desistir_Tramite',
        p_NIVEL => '',
        p_ORIGEN => 'DB',
        p_USR_LOG  => 'DB',
        p_USR_WINDOWS => 'Sistema',
        p_IP_PC  => null,
        p_EXCEPCION =>
          'Id Tramite IPJ = ' || to_char(p_id_tramite_ipj)
          || ' / Cuil Usuario = ' || p_cuil_usuario
       );
       COMMIT;
    end if;

    -- Busco los datos del tr�mite
    select * into v_row_tramite
    from ipj.t_tramitesipj
    where
      id_tramite_ipj = p_id_tramite_ipj;

    -- Si el tr�mite no esta asociado a SUAC, no hace nada
    if nvl(v_row_tramite.id_tramite, 0) = 0 then
      o_rdo := 'No es un tr�mite real, no se puede desistir.';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      Return;
    end if;

    -- Si el tr�mite no lo tiene el usuario logueado, no se puede desistir
    if nvl(v_row_tramite.cuil_ult_estado, '0') <> p_cuil_usuario then
      o_rdo := 'Debe asignarse el tr�mite para realizar esta acci�n.';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      Return;
    end if;

    -- Verifico que el usuario tenga permisos para Desistir.
    select count(1) into v_Usr_Hab
    from ipj.t_usuarios u
    where
      u.cuil_usuario = p_cuil_usuario
      and upper(u.hab_desistir) = 'S';

    if v_Usr_Hab = 0 then
      o_rdo := 'No cuenta con permisos para realizar esta operaci�n.';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      Return;
    end if;

    -- Busco los datos en SUAC
    IPJ.TRAMITES_SUAC.SP_Buscar_Suac (
      o_rdo => o_rdo,
      o_tipo_mensaje => o_tipo_mensaje,
      o_SUAC => v_SUAC,
      p_id_tramite => v_row_tramite.id_tramite,
      p_loguear => 'N'
    );

    if o_rdo <> IPJ.TYPES.c_Resp_OK then
      return;
    end if;

    -- Busco el area del tr�mite en Gesti�n.
    begin
      select us.n_unidad into v_ubic_Gestion
      from ipj.t_relacion_ubic_suac u join NUEVOSUAC.VT_UNIDADES us
        on u.codigo_suac = us.codigo and us.fecha_hasta is null and nvl(us.desactivado, 0) = 0 and nvl(us.externa, 0) = 0 and nvl(us.eliminado, 0) = 0
      where
        u.id_ubicacion = v_row_tramite.id_ubicacion and
        us.n_unidad = v_SUAC.v_unidad_actual;
    exception
      when NO_DATA_FOUND then
        v_ubic_Gestion := '-';
    end;

    -- Verifico que este en el �rea del tr�mite, y no tenga un pase.
    if v_SUAC.v_estado <> 'A ENVIAR' or v_ubic_gestion <> v_SUAC.v_unidad_actual then
      o_rdo := 'El tr�mite en SUAC no se encuenta en la misma Area que Gesti�n.';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      Return;
    end if;

    --**************************************
    --      CUERPO DEL PROCEDIMIENTO
    --**************************************
    -- Busco los datos del Area Archivo
    select c.id_clasif_ipj into v_id_clasif_arh
    from ipj.t_tipos_clasif_ipj c
    where c.id_ubicacion = IPJ.TYPES.C_AREA_ARCHIVO;

    select u.cuil_usuario into v_responsable_arch
    from ipj.t_ubicaciones u
    where u.id_ubicacion = IPJ.TYPES.C_AREA_ARCHIVO;

    select p.id_legajo into v_id_legajo
    from ipj.t_tramitesipj_persjur p
    where
      p.id_tramite_ipj = p_id_tramite_ipj
      and rownum = 1;

    IF FC_Es_Tram_Digital(p_id_tramite_ipj) = 0 THEN
      IPJ.TRAMITES_SUAC.SP_Realizar_Tranf_IPJ(
        o_rdo => o_rdo,
        o_tipo_mensaje => o_tipo_mensaje,
        p_id_tramite_suac => v_Row_Tramite.id_tramite,
        p_id_ubicacion_origen => v_Row_Tramite.id_ubicacion,
        p_id_ubicacion_destino => IPJ.TYPES.C_AREA_ARCHIVO,
        p_cuil_usuario_origen => v_Row_Tramite.cuil_ult_estado,
        p_cuil_usuario_destino => v_responsable_arch
      );
    END IF;

    if o_rdo <> IPJ.TYPES.C_RESP_OK then
      o_rdo :=  'Error al realizar transferencia en SUAC: ' || o_rdo;
      rollback;
      return;
    end if;

    -- Paso a Archivo el tr�mite
    IPJ.TRAMITES.SP_GUARDAR_TRAMITE(
      o_id_tramite_ipj => v_id_tramite_out,
      o_rdo => o_rdo,
      o_tipo_mensaje => o_tipo_mensaje,
      v_id_tramite_ipj => p_id_tramite_ipj,
      v_id_tramite => v_Row_Tramite.id_tramite,
      v_id_Clasif_IPJ => v_id_clasif_arh,
      v_id_Grupo  => 0,
      v_cuil_ult_estado => v_responsable_arch,
      v_id_estado => 202, -- Desistido por Usuario
      v_cuil_creador => v_Row_Tramite.cuil_creador,
      v_observacion => '',
      v_id_Ubicacion => IPJ.TYPES.C_AREA_ARCHIVO,
      v_urgente => v_Row_Tramite.Urgente,
      p_sticker => v_Row_Tramite.Sticker,
      p_expediente => v_Row_Tramite.Expediente,
      p_simple_tramite => 'N'
    );

    -- Si no puedo cambiar el tr�mite a estado PERIMIDO, lo cierro
    if o_rdo <> IPJ.TYPES.C_RESP_OK then
      rollback;
      return;
    end if;

    -- Limpio la configuraci�n de Archivo, y asigno la resolucion como Obs.
    update ipj.t_archivo_tramite
    set
      observacion = 'Desistido por Usuario',
      id_accion_archivo = 7 -- VARIOS
    where
      id_tramite_ipj = p_id_tramite_ipj;

    -- Si se perime, se da de baja el legajo, y se marca la baja en entidad si existe
    if IPJ.TRAMITES.FC_Es_Tram_Const (p_id_tramite_ipj) > 0 then
      update ipj.t_legajos
      set fecha_baja_entidad = to_date(sysdate, 'dd/mm/rrrr')
      where
        id_legajo = v_id_legajo;

      update ipj.t_entidades
      set
        Baja_Temporal = sysdate,
        id_estado_entidad = 'S', -- Desistida
        id_tipo_origen = 20, -- Desistido por Usuario
        obs_cierre = 'Desistido por Usuario'
      where
        id_tramite_ipj = p_id_tramite_ipj and
        id_legajo = v_id_legajo;

      -- Actualizo los datos en Gobierno porque se da de baja.
      IPJ.ENTIDAD_PERSJUR.SP_ACTUALIZAR_ENT_GOB(
        o_rdo => o_rdo,
        o_tipo_mensaje => o_tipo_mensaje,
        p_Id_Tramite_Ipj => p_id_tramite_ipj,
        p_id_legajo => v_id_legajo
      );
    else
      -- Si no es Const. solo pongo versi�n -1
      update ipj.t_entidades
      set
        matricula_version = -1
      where
        id_tramite_ipj = p_id_tramite_ipj and
        id_legajo = v_id_legajo;
    end if;

    -- Si se perime un tr�mite de TRANSFORMACION ,se vuelve atras el cambio
    if IPJ.TRAMITES.FC_Es_Tram_Transf (p_id_tramite_ipj) > 0 then
      -- Busco el �ltimo tr�mite, a ver su tipo de entidad
      IPJ.ENTIDAD_PERSJUR.SP_Buscar_Entidad_Tramite(
        o_Id_Tramite_Ipj => v_id_ult_tramite,
        o_id_tramiteipj_accion => v_id_ult_accion,
        p_id_legajo => v_id_legajo,
        p_Abiertos => 'N');

      -- Busco el ultimo tipo de entidad actual del legajo
      select id_tipo_entidad into v_id_tipo_ent_legajo
      from ipj.t_legajos
      where
        id_legajo = v_id_legajo;

     -- Busco el tipo de entidad actual del ultimo tramite
      begin
        select id_tipo_entidad into v_id_tipo_ent_ult
        from ipj.t_entidades
        where
          id_legajo = v_id_legajo and
          id_tramite_ipj = v_id_ult_tramite;
      exception
        -- Si no hay datos, lo dejo como esta
        when NO_DATA_FOUND then
          v_id_tipo_ent_ult := v_id_tipo_ent_legajo;
      end;

      -- Si el ultimo tr�mites es ditinto al legajo, lo vulevo atras
      if nvl(v_id_tipo_ent_ult, 0) > 0 and v_id_tipo_ent_ult <> v_id_tipo_ent_legajo then
        update ipj.t_legajos
        set id_tipo_entidad = v_id_tipo_ent_ult
        where
          id_legajo = v_id_legajo;
      end if;
    end if;

    if o_rdo <> IPJ.TYPES.C_RESP_OK then
      rollback;
      return;
    end if;

    o_rdo := IPJ.TYPES.c_Resp_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    commit;
  EXCEPTION
    WHEN OTHERS THEN
      o_rdo := 'SP_Desistir_Tramite: ' || To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      rollback;
  END SP_Desistir_Tramite;

  FUNCTION FC_Es_Tram_Const (p_id_tramite_ipj number) return number is
    -- Si es un tr�mite que Constituye Entidad, devuelvo 1, sino 0
    v_id_tramite number;
    v_es_constitucion number;
  BEGIN
    select tr.id_tramite into v_id_tramite
    from ipj.t_tramitesipj tr
    where
      tr.id_tramite_ipj = p_id_tramite_ipj;

    if nvl(v_id_tramite, 0) > 0 then
      -- si es del subtipo listado como constituciones, cuenta
      select count(1) into v_es_constitucion
      from NUEVOSUAC.VT_SUBTIPOS_TRAMITE st
      where
          -- #####  EN PRODUCCION VA ESTO  ######
          st.id_subtipo_tramite in (39451, 39452, 39453, 39480, 39547, 39549, 39587, 39586, 48344, 39592, 39593, 39598, 39582, 39575, 51060)
          -- #####  DESARROLLO VA ESTO  ######
          --n_subtipo_tramite like 'CONSTITUC%'
          and n_subtipo_tramite = ipj.tramites_suac.FC_Buscar_SubTipo_SUAC(v_id_tramite)
      ;
    else
      v_Es_constitucion := 0;
    end if;

    Return v_Es_constitucion;
  Exception
    When Others Then
      Return 0;
  End FC_Es_Tram_Const;

  FUNCTION FC_Es_Tram_Transf (p_id_tramite_ipj number) return number is
    -- Si es un tr�mite de Transformaci�n de Entidad, devuelvo 1, sino 0
    v_id_tramite number;
    v_es_Transf number;
  BEGIN
    select tr.id_tramite into v_id_tramite
    from ipj.t_tramitesipj tr
    where
      tr.id_tramite_ipj = p_id_tramite_ipj;

    if nvl(v_id_tramite, 0) > 0 then
      -- si es del subtipo listado como constituciones, cuenta
      select count(1) into v_es_Transf
      from NUEVOSUAC.VT_SUBTIPOS_TRAMITE st
      where
          st.id_subtipo_tramite in (39571, 39605)
          and n_subtipo_tramite = ipj.tramites_suac.FC_Buscar_SubTipo_SUAC(v_id_tramite)
      ;
    else
      v_es_Transf := 0;
    end if;

    Return v_es_Transf;
  Exception
    When Others Then
      Return 0;
  End FC_Es_Tram_Transf;

  PROCEDURE SP_Validar_Usuario_Desistir(
    o_Res OUT NUMBER, --0 sin permiso 1 con permiso
    p_Cuil_Usuario IN VARCHAR2)
  IS
  /*********************************************************
    Valida si el usuario tiene permiso de desistir tr�mite
  *********************************************************/
  BEGIN
    SELECT COUNT(1)
      INTO o_Res
      FROM ipj.t_usuarios u
     WHERE u.cuil_usuario = p_Cuil_Usuario
       AND nvl(u.hab_desistir, 'N') = 'S';

  EXCEPTION
    WHEN OTHERS THEN
      o_Res := 0;

  END SP_Validar_Usuario_Desistir;

  PROCEDURE SP_Guardar_Resol_Digital (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_id_tramiteIpj_Accion in number,
    p_id_prot_dig in varchar2,
    p_nro_resolucion in number,
    p_fec_resolucion in varchar2 -- Es Fecha
  )
  IS
  /*
    Para una acci�n de Protocolos Digitales, guarda:
    - Actualiza los protocolos de digital
    - Actualiza la resolucion en la entidad
    - Actualiza las fechas de las autoridades
  */
    v_id_legajo number;
    v_id_integrante number;
    v_hay_datos number;
    v_cursor_organismo ipj.types.cursortype;
    v_id_organismo number;
    v_duracion number;
    v_tipo_duracion number;
    v_cierre_ejerc varchar2(10);
    v_id_tipo_accion number;
    v_fin_febrero char(1);
    v_prot_digital varchar(2);
  BEGIN
    if IPJ.Types.c_habilitar_log_SP = 'S' then
      IPJ.VARIOS.SP_GUARDAR_LOG(
        p_METODO => 'SP_Guardar_Resol_Digital',
        p_NIVEL => '',
        p_ORIGEN => 'DB',
        p_USR_LOG  => 'DB',
        p_USR_WINDOWS => 'Sistema',
        p_IP_PC  => null,
        p_EXCEPCION =>
           'Id Tramite IPJ = ' || to_char(p_Id_Tramite_Ipj)
           || ' / Id TramiteIPJ Accion = ' || to_char(p_id_tramiteIpj_Accion)
           || ' / Id Prot Dig = ' ||p_id_prot_dig
           || ' / Nro. resolucion = ' || to_char(p_nro_resolucion)
           || ' / Fec Resolucion = ' || p_fec_resolucion
       );
    end if;

    if p_fec_resolucion is null or IPJ.VARIOS.Valida_Fecha(p_fec_resolucion) = 'N' then
      o_rdo := IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT') ;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      return;
    end if;

    -- Busco el legajo asociado a la accion
    select id_legajo, id_integrante, a.id_tipo_accion, ta.id_prot_dig
      into v_id_legajo, v_id_integrante, v_id_tipo_accion, v_prot_digital
    from ipj.t_tramitesipj_acciones a join ipj.t_tipos_accionesipj ta
      on a.id_tipo_accion = ta.id_tipo_accion
    where
      Id_Tramite_Ipj = p_Id_Tramite_Ipj and
      id_tramiteipj_accion = p_id_tramiteipj_accion;

    --Actualizo la fecha de resoluci�n en la accion, si es un protocolo digital
    if v_prot_digital is not null then
      update ipj.t_tramitesipj_acciones
      set fec_resolucion_dig = to_date(p_fec_resolucion, 'dd/mm/rrrr')
      where
        Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        id_tramiteipj_accion = p_id_tramiteipj_accion;
    end if;

    -- Para las inscripciones de ACyF, se actualizan varios datos desde la resolucion
    if v_id_tipo_accion = 135 then
      -- Verifico que existan datos cargados para la entidad
      select count(1) into v_hay_datos
      from ipj.t_entidades
      where
        id_tramite_ipj = p_id_tramite_ipj and
        id_legajo = v_id_legajo;

      -- Si no hay datos,
      if v_hay_datos = 0 then
        o_rdo := 'A la empresa no se le cargaron datos.';
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
        return;
      end if;

      -- Busco el cierre de ejercicio, si no viene no contin�a
      select to_char(cierre_ejercicio, 'dd/mm/rrrr'), cierre_fin_mes into v_cierre_ejerc, v_fin_febrero
      from ipj.t_entidades
      where
        id_tramite_ipj = p_id_tramite_ipj and
        id_legajo = v_id_legajo;

      if v_cierre_ejerc is null and nvl(v_fin_febrero, 'N') = 'N' then
        o_rdo := 'La empresa no cuenta con un cierre de ejercicio definido.';
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
        return;
      end if;

      -- Si es una empresa, corrijo sus datos
      -- BUG15665:[SG] - [Adjuntar Documentaci�n - Constituci�n Asociaci�n Civil] - Resoluci�n Digital- El numero de resoluci�n no es incremental, siempre sugiere "1".
      if nvl(v_id_legajo, 0) > 0 then
        -- Actualizo los datos de la inscripci�n
        update ipj.t_entidades
        set
          fec_inscripcion = to_date(p_fec_resolucion, 'dd/mm/rrrr'),
          nro_resolucion = decode(p_nro_resolucion,0,nro_resolucion,to_char(p_nro_resolucion) || ' "' || p_id_prot_dig || '"/' || to_char(to_date(p_fec_resolucion, 'dd/mm/rrrr'), 'yy')),
          fec_resolucion = to_date(p_fec_resolucion, 'dd/mm/rrrr'),
          tipo_vigencia = 1 -- Desde Fecha Resoluci�n
        where
          id_legajo = v_id_legajo and
          id_tramite_ipj = p_id_tramite_ipj;

        -- Si es el �ltimo d�a de febrero, tomo el 28 para el calculo de los plazos
        if nvl(v_fin_febrero, 'N') = 'S' then
          v_cierre_ejerc := '28/02/' || to_char(sysdate, 'rrrr');
        end if;

        -- Para cada organismo, actualizo las fechas segun su definicion
        OPEN v_cursor_organismo FOR
          select id_tipo_organismo,cant_meses, id_tipo_duracion
          from ipj.t_entidades_organismo
          where
            id_legajo = v_id_legajo;

        LOOP
          fetch v_cursor_organismo into v_id_organismo, v_duracion, v_tipo_duracion;
          EXIT WHEN v_cursor_organismo%NOTFOUND or v_cursor_organismo%NOTFOUND is null;

          -- Actualizo las autoridades definidas
          update ipj.t_entidades_admin
          set
            fecha_inicio = to_date(p_fec_resolucion, 'dd/mm/rrrr'),
            fecha_fin = (case
                                 -- Si es por ejercicios, y mayor al cierre de ejercicio
                                 when v_tipo_duracion = 1 and to_date(p_fec_resolucion, 'dd/mm/rrrr') >= to_date(substr(v_cierre_ejerc, 1, 6) || substr(p_fec_resolucion, 7, 4), 'dd/mm/rrrr') then
                                   to_date(substr(v_cierre_ejerc, 1, 6) || to_char(to_number(substr(p_fec_resolucion, 7, 4)) + v_duracion), 'dd/mm/rrrr')
                                 -- Si es por ejercicios, y menor al cierre de ejercicio
                                 when v_tipo_duracion = 1 and to_date(p_fec_resolucion, 'dd/mm/rrrr') < to_date(substr(v_cierre_ejerc, 1, 6) || substr(p_fec_resolucion, 7, 4), 'dd/mm/rrrr') then
                                   to_date(substr(v_cierre_ejerc, 1, 6) || to_char(to_number(substr(p_fec_resolucion, 7, 4)) + v_duracion-1), 'dd/mm/rrrr')
                                 -- Si es por A�os
                                 when v_tipo_duracion = 2 then
                                   to_date(substr(p_fec_resolucion, 1, 6) || to_char(to_number(substr(p_fec_resolucion, 7, 4)) + v_duracion), 'dd/mm/rrrr')
                              end)
          where
            id_tramite_ipj = p_id_tramite_ipj and
            id_legajo = v_id_legajo and
            id_tipo_organismo = v_id_organismo;

          -- Actualizo los fiscalizadores definidos
          update ipj.t_entidades_sindico
          set
            fecha_alta = to_date(p_fec_resolucion, 'dd/mm/rrrr'),
            fecha_baja = (case
                                 -- Si es por ejercicios, y mayor al cierre de ejercicio
                                 when v_tipo_duracion = 1 and to_date(p_fec_resolucion, 'dd/mm/rrrr') >= to_date(substr(v_cierre_ejerc, 1, 6) || substr(p_fec_resolucion, 7, 4), 'dd/mm/rrrr') then
                                   to_date(substr(v_cierre_ejerc, 1, 6) || to_char(to_number(substr(p_fec_resolucion, 7, 4)) + v_duracion), 'dd/mm/rrrr')
                                 -- Si es por ejercicios, y menor al cierre de ejercicio
                                 when v_tipo_duracion = 1 and to_date(p_fec_resolucion, 'dd/mm/rrrr') < to_date(substr(v_cierre_ejerc, 1, 6) || substr(p_fec_resolucion, 7, 4), 'dd/mm/rrrr') then
                                   to_date(substr(v_cierre_ejerc, 1, 6) || to_char(to_number(substr(p_fec_resolucion, 7, 4)) + v_duracion-1), 'dd/mm/rrrr')
                                 -- Si es por A�os
                                 when v_tipo_duracion = 2 then
                                   to_date(substr(p_fec_resolucion, 1, 6) || to_char(to_number(substr(p_fec_resolucion, 7, 4)) + v_duracion), 'dd/mm/rrrr')
                              end)
          where
            id_tramite_ipj = p_id_tramite_ipj and
            id_legajo = v_id_legajo and
            id_tipo_integrante in (select id_tipo_integrante from ipj.t_tipos_integrante where id_tipo_organismo =  v_id_organismo);

        END LOOP;
        CLOSE v_cursor_organismo;

         -- Actualizo Historial de Autoridades
         UPDATE
           ( SELECT
               -- Campos Admin
               ad.fecha_inicio, ad.fecha_fin,
               -- Campos Admin Hist
               adh.fecha_inicio fecha_inicio_hist, adh.fecha_fin fecha_fin_hist
             FROM Ipj.T_Entidades_admin ad join Ipj.T_Entidades_admin_hist adh
               on ad.id_tramite_ipj = adh.id_tramite_ipj and
                 ad.id_legajo = adh.id_legajo and
                 ad.id_admin = adh.id_admin
             WHERE
               ad.id_tramite_ipj = p_id_tramite_ipj and
               ad.id_legajo = v_id_legajo
           )
         SET
           fecha_inicio_hist = fecha_inicio,
           fecha_fin_hist = fecha_fin;

        -- Actualizo Historial de Fiscalizadores
         UPDATE
           ( SELECT
               -- Campos Admin
               ad.fecha_alta, ad.fecha_baja,
               -- Campos Admin Hist
               adh.fecha_alta fecha_alta_hist, adh.fecha_baja fecha_baja_hist
             FROM Ipj.T_Entidades_sindico ad join Ipj.T_Entidades_sindico_hist adh
               on ad.id_tramite_ipj = adh.id_tramite_ipj and
                 ad.id_legajo = adh.id_legajo and
                 ad.id_entidad_sindico = adh.id_entidad_sindico
             WHERE
               ad.id_tramite_ipj = p_id_tramite_ipj and
               ad.id_legajo = v_id_legajo
           )
         SET
           fecha_alta_hist = fecha_alta,
           fecha_baja_hist = fecha_baja;
      end if;
    end if;

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    commit;
  EXCEPTION
     when OTHERS then
        o_rdo := 'SP_Guardar_Resol_Digital: ' || To_Char(SQLCODE) || '-' || SQLERRM;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
        rollback;
  END SP_Guardar_Resol_Digital;

  PROCEDURE SP_Tramites_Inactivos_Ciud(
    o_Cursor OUT types.cursorType,
    p_id_ubicacion in number
    )
  IS
  /********************************************************************************
     Lista los expedientes inactivos por m�s de 20 d�as por parte del ciudadano
  *********************************************************************************/
    v_dias_inactividad number;
  BEGIN
    -- Busco la cantidad de d�as de inactividad permitidos
    v_dias_inactividad := to_number(IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('DIAS_PRE_AVISO_ARCHI'));

    OPEN o_Cursor FOR
      select tipo_inactivo, estado_nota, fecha_inicio, nro_expediente, sticker, razon_social, Asunto, id_tramite_ipj,
        id_tramite_suac, Ult_Nota, IPJ.VARIOS.FC_Dias_Laborables(ult_nota, to_date(sysdate, 'dd/mm/rrrr'),'S') Dias_Inactivos,
        (to_date(sysdate, 'dd/mm/rrrr') - ult_nota) Dias_Totales, nota_sistema, Hay_Mail,
        (case when fecha_inicio < to_date('01/01/2017', 'dd/mm/rrrr') then 'S' else 'N' end) Es_Viejo,
        cuil_creador, urgente, simple_tramite, cuil_ult_estado,
        (select vt.id_subtipo_tramite from NUEVOSUAC.VT_TRAMITES_IPJ_DESA vt where vt.id_tramite = tmp.id_tramite_suac) id_subtipo_tramite
      from
        ( select
            CASE WHEN (select max(nvl(fec_modif, fec_cumpl)) from ipj.t_entidades_notas n where n.id_tramite_ipj = tr.id_tramite_ipj and informar_suac = 'N' and nvl(n.id_nota_sistema, 0) <> 1) IS NULL AND (select max(to_date(v_fecha_anexado, 'dd/mm/rrrr')) from nuevosuac.vt_get_tramites_ipj_anexos n where n.v_id_padre = tr.id_tramite) IS NULL THEN 'SUAC'
                      WHEN NVL((select max(nvl(fec_modif, fec_cumpl)) from ipj.t_entidades_notas n where n.id_tramite_ipj = tr.id_tramite_ipj and informar_suac = 'N' and nvl(n.id_nota_sistema, 0) <> 1), tr.fecha_ini_suac) > nvl((select max(to_date(v_fecha_anexado, 'dd/mm/rrrr')) from nuevosuac.vt_get_tramites_ipj_anexos n where n.v_id_padre = tr.id_tramite), tr.fecha_ini_suac) THEN 'OBS'
                      WHEN nvl((select max(nvl(fec_modif, fec_cumpl)) from ipj.t_entidades_notas n where n.id_tramite_ipj = tr.id_tramite_ipj and informar_suac = 'N' and nvl(n.id_nota_sistema, 0) <> 1), tr.fecha_ini_suac) < nvl((select max(to_date(v_fecha_anexado, 'dd/mm/rrrr')) from nuevosuac.vt_get_tramites_ipj_anexos n where n.v_id_padre = tr.id_tramite), tr.fecha_ini_suac) THEN 'ANEX'
                      WHEN nvl((select max(nvl(fec_modif, fec_cumpl)) from ipj.t_entidades_notas n where n.id_tramite_ipj = tr.id_tramite_ipj and informar_suac = 'N' and nvl(n.id_nota_sistema, 0) <> 1), tr.fecha_ini_suac) = nvl((select max(to_date(v_fecha_anexado, 'dd/mm/rrrr')) from nuevosuac.vt_get_tramites_ipj_anexos n where n.v_id_padre = tr.id_tramite), tr.fecha_ini_suac) THEN 'IGUAL'
                 END tipo_inactivo,
            (select COUNT(1) from ipj.t_entidades_notas n where n.id_tramite_ipj = tr.id_tramite_ipj AND n.id_estado_nota IN (0,2)) estado_nota,
            to_date(tr.Fecha_Ini_Suac, 'dd/mm/rrrr') fecha_inicio,
            tr.expediente nro_expediente, tr.sticker sticker,
            nvl((select denominacion_sia from ipj.t_legajos l where l.id_legajo = pr.id_legajo), ipj.tramites_suac.FC_Buscar_Iniciador_SUAC(tr.id_tramite)) razon_social,
            IPJ.TRAMITES_SUAC.FC_Buscar_Asunto_SUAC(TR.ID_TRAMITE) Asunto,
            tr.id_tramite_ipj id_tramite_ipj,tr.id_tramite id_tramite_suac,
            Greatest(
                   nvl((select max(nvl(fec_modif, fec_cumpl)) from ipj.t_entidades_notas n where n.id_tramite_ipj = tr.id_tramite_ipj and informar_suac = 'N' and nvl(n.id_nota_sistema, 0) <> 1), tr.fecha_ini_suac),
                   nvl((select max(to_date(v_fecha_anexado, 'dd/mm/rrrr')) from nuevosuac.vt_get_tramites_ipj_anexos n where n.v_id_padre = tr.id_tramite), tr.fecha_ini_suac)
            ) Ult_Nota,
            (select max(nvl(fec_modif, fec_cumpl)) from ipj.t_entidades_notas n where n.id_tramite_ipj = tr.id_tramite_ipj and informar_suac = 'N' and nvl(n.id_nota_sistema, 0) = 1) nota_sistema,
            (select count(1) from ipj.t_entidades_autorizados a where a.id_tramite_ipj = tr.id_tramite_ipj and a.mail_autorizado = 'S') Hay_Mail,
            tr.cuil_creador, tr.urgente, tr.simple_tramite, tr.cuil_ult_estado
          from ipj.t_tramitesipj tr left join ipj.t_tramitesipj_persjur pr
              on pr.id_tramite_ipj = tr.id_tramite_ipj
          where
            tr.id_ubicacion_origen = p_id_ubicacion and
            tr.id_estado_ult between 2 and 99 and -- Estados de trabajo
            tr.id_estado_ult not in (50, 6) and -- Excluye anexados y subsanados
            nvl(tr.id_tramite, 0) > 0 and
            --nvl(tr.cuil_ult_estado, '1') <> '1' and
            ( --tr.fecha_ini_suac < to_date('01/01/2017', 'dd/mm/rrrr') or -- Ahora se trabajan todos los tr�mites iguales
              IPJ.VARIOS.FC_Dias_Laborables(
                  nvl( Greatest(
                           nvl((select max(nvl(fec_modif, fec_cumpl)) from ipj.t_entidades_notas n where n.id_tramite_ipj = tr.id_tramite_ipj and informar_suac = 'N' and nvl(n.id_nota_sistema, 0) <> 1), tr.fecha_ini_suac),
                           nvl((select max(to_date(v_fecha_anexado, 'dd/mm/rrrr')) from nuevosuac.vt_get_tramites_ipj_anexos n where n.v_id_padre = tr.id_tramite), tr.fecha_ini_suac)
                         ), sysdate
                  ), to_date(sysdate, 'dd/mm/rrrr'), 'S'
               ) >= v_dias_inactividad
            )
          ) tmp
      WHERE decode(tmp.tipo_inactivo,'OBS',tmp.estado_nota,'IGUAL',tmp.estado_nota,0) > 0
    order by Fecha_Inicio asc;

  END SP_Tramites_Inactivos_Ciud;

  PROCEDURE SP_Rechazar_Tramite(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_observacion in varchar2)
  IS
    v_Row_Tramite IPJ.T_TRAMITESIPJ%rowtype;
    v_id_legajo number;
    v_id_integrante number;
    v_hay_observaciones number;
    v_cant_sincroniz number;
    v_sticker varchar2(20);
  BEGIN
    -- Busco los datos del tr�mite
    select * into v_Row_Tramite
    from IPJ.t_tramitesipj
    where
      Id_Tramite_Ipj = p_Id_Tramite_Ipj;

    -- Busco la Empresa del Tr�mite
    begin
      select id_legajo into v_id_legajo
      from IPJ.t_tramitesipj_persjur
      where
        Id_Tramite_Ipj = p_Id_Tramite_Ipj;
    exception
      when NO_DATA_FOUND then
        v_id_legajo := 0;
    end;

    -- Busco la Persona del Tr�mite
    begin
      select id_integrante into v_id_integrante
      from ipj.t_tramitesipj_integrante
      where
        Id_Tramite_Ipj = p_Id_Tramite_Ipj;
    exception
      when NO_DATA_FOUND then
        v_id_integrante := 0;
    end;

    -- Si no hay empresa ni persona, no se puede rechazar
    if v_id_legajo = 0  and v_id_integrante = 0 then
      o_rdo := 'No existe una Entidad o Persona F�sica asociada al tr�mite.';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      return;
    end if;

    -- Si es Digital, no permite utilizarlo
    if IPJ.TRAMITES.FC_ES_TRAM_DIGITAL(p_id_tramite_ipj) > 0 then
      o_rdo := 'Es un expediente digital, por favor utilice las acciones correspondientes.';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      return;
    end if;

    -- Cuento la cantidad de Notificaciones en SUAC
    select count(1) into v_hay_observaciones
    from IPJ.T_ENTIDADES_NOTAS
    where
      id_tramite_ipj = p_id_tramite_ipj;

      -- Si no tiene notificaciones en SUAC, no avanza
    if v_hay_observaciones = 0 then
      o_rdo := 'Necesita al menos una notificaci�n en SUAC.';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      return;
    end if;

    -- RPC sigue como antes, las otras usan las neuvas validaciones
    if v_Row_Tramite.id_ubicacion <> IPJ.TYPES.c_area_SRL then
      -- Sincronizo las observaciones en SUAC
      IPJ.TRAMITES_SUAC.SP_SUBIR_NOTAS_SUAC(
        o_rdo => o_rdo,
        o_tipo_mensaje => o_tipo_mensaje,
        o_cant_sincroniz => v_cant_sincroniz,
        o_sticker => v_sticker,
        p_id_tramite_ipj => p_id_tramite_ipj,
        p_transaccion => 'S'
      ) ;

      -- Si no pudo sincronizar las notas en SUAC, cancelo la operaci�n.
      if nvl(o_rdo, IPJ.TYPES.C_RESP_OK) <> IPJ.TYPES.C_RESP_OK then
        o_rdo := 'Rechazar - Sincronizar Notas SUAC: ' || o_rdo;
        return;
      end if;

      -- Paso a rechazado a archivo
      update ipj.t_tramitesipj
      set id_estado_ult = 204 -- Tramite Rechazado
      where
        id_tramite_ipj = p_id_tramite_ipj;

      --Rechazo todas las acciones pendintes
      update ipj.t_tramitesipj_acciones
      set id_estado = 204 -- Tramite Rechazado
      where
        id_tramite_ipj = p_id_tramite_ipj and
        id_estado < 100;

      --  Si es Constituci�n y se perime, se da de baja el legajo, y se marca la baja en entidad si existe
      if IPJ.TRAMITES.FC_Es_Tram_Const (p_id_tramite_ipj) > 0 then
        update ipj.t_legajos
        set fecha_baja_entidad = to_date(sysdate, 'dd/mm/rrrr')
        where
          id_legajo = v_id_legajo;

        update ipj.t_entidades
        set
          Baja_Temporal = to_date(sysdate, 'dd/mm/rrrr'),
          id_estado_entidad = 'R', -- Rechazado
          id_tipo_origen = 21, -- Rechazado
          obs_cierre = 'Tr�mite Rechazado por IPJ.'
        where
          id_tramite_ipj = p_id_tramite_ipj and
          id_legajo = v_id_legajo;
      end if;
    else
      -- Para Registro Publico, se rechaza sin valdiar como antes
      update ipj.t_tramitesipj
      set id_estado_ult = 200 -- Rechazado (viejo)
      where
        id_tramite_ipj = p_id_tramite_ipj;

      --Rechazo todas las acciones pendintes
      update ipj.t_tramitesipj_acciones
      set id_estado = 200 -- Rechazado (viejo)
      where
        id_tramite_ipj = p_id_tramite_ipj and
        id_estado < 200;
    end if;

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    commit;
  EXCEPTION
    WHEN OTHERS THEN
      o_rdo := 'SP_Rechazar_Tramite - ' || To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Rechazar_Tramite;

  PROCEDURE SP_Guardar_Fec_Veed(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_id_tramiteipj_accion IN number,
    p_id_legajo in varchar2,
    p_fec_veeduria IN varchar2
  )
  IS
   /****************************************************
   Guarda la fecha de Veedur�a para acciones de Veeduria, Intervenciones y Comisiones
   *****************************************************/
  BEGIN
    -- Actualizo la fecha con la mascara de Fecha y Hora
    update ipj.t_tramitesipj_acciones
    set fec_veeduria = to_date(p_fec_veeduria, 'dd/mm/rrrr HH:MI:SS AM')
    where
      id_tramite_ipj = p_id_tramite_ipj and
      id_tramiteipj_accion = p_id_tramiteipj_accion and
      id_legajo = p_id_legajo;

    o_rdo  := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    COMMIT;

  EXCEPTION
    WHEN OTHERS THEN
      o_rdo  := To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      rollback;
  END SP_Guardar_Fec_Veed;

  PROCEDURE SP_Buscar_Res_Prot_Dig(
    o_Cursor OUT types.cursorType,
    p_id_legajo in number,
    p_id_prot_dig in varchar2,
    p_nro_resolucion in number,
    p_anio in number,
    p_nro_expediente in varchar2,
    p_fecha_desde in varchar2, -- es fecha
    p_fecha_hasta in varchar2, -- es fecha
    p_id_ubicacion in number
    )
  IS
  /****************************************************
     lista las resoluciones digitales generadas seg�n los datos ingresados
  *****************************************************/
  BEGIN
    -- Logueo los par�metros
    if IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('HAB_LOG_TRAMITES') = 'S' then
      IPJ.VARIOS.SP_GUARDAR_LOG(
        p_METODO => 'SP_Buscar_Res_Prot_Dig',
        p_NIVEL => '',
        p_ORIGEN => 'DB',
        p_USR_LOG  => 'DB',
        p_USR_WINDOWS => 'Sistema',
        p_IP_PC  => null,
        p_EXCEPCION =>
           'Id Legajo = ' || to_char(p_id_legajo)
           || ' / Id Prot Dig = ' || p_id_prot_dig
           || ' / Nro Resolucion = ' || to_char(p_nro_resolucion)
           || ' / Anio = ' || to_char(p_anio)
           || ' / Nro Expedinete = ' || p_nro_expediente
           || ' / Fecha Desde = ' || p_fecha_desde
           || ' / Fecha Hasta = ' || p_fecha_hasta
           || ' / Ubicacion = ' || to_char(p_id_ubicacion)
       );
       COMMIT;
    end if;

    OPEN o_Cursor FOR
      select to_char(a.fec_resolucion_dig, 'dd/mm/rrrr') fec_resolucion_dig, tr.expediente,
        to_char(a.nro_resolucion_dig) || ' ' || a.id_prot_dig Protocolo, ta.n_tipo_accion,
        (select decode(cuit, null, 'No', 'Si') from ipj.t_legajos l where l.id_legajo = a.id_legajo) cuit,
        ( case
            when a.id_tipo_accion in (146, 147, 154, 157, 161, 176) and a.id_estado = 203 then
              (select n_estado from ipj.t_estados e where e.id_estado = IPJ.TYPES.c_Estados_Cerrado)
            when a.id_tipo_accion in (156, 155, 153, 159, 160, 175) and a.id_estado = 204 then
              (select n_estado from ipj.t_estados e where e.id_estado = IPJ.TYPES.c_Estados_Cerrado)
           else
             (select n_estado from ipj.t_estados e where e.id_estado = a.id_estado)
         end) Estado_Accion,
        (select n_estado from ipj.t_estados e where e.id_estado = tr.id_estado_ult) Estado_Tramite,
        (Select decode(count(1), 0, 'No', 'Si') from ipj.t_entidades_acta ac join ipj.t_entidades_acta_orden o on ac.id_entidad_acta = o.id_entidad_acta where ac.id_tramite_ipj = tr.id_tramite_ipj and o.id_tipo_orden_dia = 7) Reforma,
        a.id_documento, a.n_documento, a.id_tipo_documento_cdd,
        a.id_tipo_accion, ta.id_pagina, tr.id_tramite_ipj, a.id_legajo,
        a.id_prot_dig, tr.sticker, a.id_tramiteipj_accion,
        to_char(tr.fecha_ini_suac, 'dd/mm/rrrr') fecha_ini_suac,
        IPJ.TRAMITES_SUAC.FC_BUSCAR_ASUNTO_SUAC(tr.id_tramite) Asunto,
        tr.id_tramite id_tramite_suac
      from ipj.t_tramitesipj tr join ipj.t_tramitesipj_acciones a
          on tr.id_tramite_ipj = a.id_tramite_ipj
        join ipj.t_tipos_accionesipj ta
          on ta.id_tipo_accion = a.id_tipo_accion
      where
        (tr.id_estado_ult >= 100 and tr.id_estado_ult <> 210) and -- Tramites completos, no inactivos.
        (ta.id_prot_dig is not null or a.id_prot_dig is not null) -- Acciones con Protocolos Digitales
        and (nvl(p_id_legajo, 0) = 0 or a.id_legajo = p_id_legajo) -- Para cualquier entidad o de la indicada
        and (p_fecha_desde is null or a.fec_resolucion_dig between to_date(p_fecha_desde, 'dd/mm/rrrr') and to_date(p_fecha_hasta, 'dd/mm/rrrr')) -- Vi viene rango de fecha lo uso
        and (nvl(p_nro_resolucion, 0) = 0 or (a.nro_resolucion_dig = p_nro_resolucion and to_char(a.fec_resolucion_dig, 'rrrr') = to_char(p_anio))) -- Si viene resolucion, busco por numero y a�o
        and (p_nro_expediente is null or tr.expediente = p_nro_expediente) --Si viene el expedinete lo uso
        and (p_id_ubicacion = IPJ.TYPES.C_AREA_JURIDICO or  ta.id_clasif_ipj in (select id_clasif_ipj from ipj.t_tipos_clasif_ipj cl where cl.id_ubicacion = p_id_ubicacion)) --Juridico busca todo, las areas solo sus acciones.
        and (p_id_prot_dig is null or ta.id_prot_dig = p_id_prot_dig or a.id_prot_dig = p_id_prot_dig) -- Si viene un protocolo, lo busco.

      UNION ALL

      select
        to_char(e.fec_resolucion, 'dd/mm/rrrr') fec_resolucion_dig, tr.expediente,
        'A' Protocolo, ta.n_tipo_accion,
        (select decode(cuit, null, 'No', 'Si') from ipj.t_legajos l where l.id_legajo = a.id_legajo) cuit,
        ( case
            when a.id_tipo_accion in (146, 147, 154, 157, 161, 176) and a.id_estado = 203 then
              (select n_estado from ipj.t_estados e where e.id_estado = IPJ.TYPES.c_Estados_Cerrado)
            when a.id_tipo_accion in (156, 155, 153, 159, 160, 175) and a.id_estado = 204 then
              (select n_estado from ipj.t_estados e where e.id_estado = IPJ.TYPES.c_Estados_Cerrado)
           else
             (select n_estado from ipj.t_estados e where e.id_estado = a.id_estado)
         end) Estado_Accion,
        (select n_estado from ipj.t_estados e where e.id_estado = tr.id_estado_ult) Estado_Tramite,
        (Select decode(count(1), 0, 'No', 'Si') from ipj.t_entidades_acta ac join ipj.t_entidades_acta_orden o on ac.id_entidad_acta = o.id_entidad_acta where ac.id_tramite_ipj = tr.id_tramite_ipj and o.id_tipo_orden_dia = 7) Reforma,
        a.id_documento, a.n_documento, a.id_tipo_documento_cdd,
        a.id_tipo_accion, ta.id_pagina, tr.id_tramite_ipj, a.id_legajo,
        a.id_prot_dig, tr.sticker, a.id_tramiteipj_accion,
        to_char(tr.fecha_ini_suac, 'dd/mm/rrrr') fecha_ini_suac,
        IPJ.TRAMITES_SUAC.FC_BUSCAR_ASUNTO_SUAC(tr.id_tramite) Asunto,
        tr.id_tramite id_tramite_suac
      from ipj.t_tramitesipj tr join ipj.t_tramitesipj_acciones a
          on tr.id_tramite_ipj = a.id_tramite_ipj
        join ipj.t_tipos_accionesipj ta
          on ta.id_tipo_accion = a.id_tipo_accion
        join ipj.t_entidades e
          on tr.id_tramite_ipj = e.id_tramite_ipj and a.id_legajo = e.id_legajo
      where
        (tr.id_estado_ult >= 100 and tr.id_estado_ult <> 210) and -- Tramites completos, no inactivos.
        a.id_tipo_accion in (133, 128, 134, 131, 132) -- Acciones con Protocolos A
        and (nvl(p_id_legajo, 0) = 0 or a.id_legajo = p_id_legajo) -- Para cualquier entidad o de la indicada
        and (p_fecha_desde is null or e.fec_resolucion between to_date(p_fecha_desde, 'dd/mm/rrrr') and to_date(p_fecha_hasta, 'dd/mm/rrrr')) -- Vi viene rango de fecha lo uso
        and (p_nro_expediente is null or tr.expediente = p_nro_expediente) --Si viene el expedinete lo uso
        and p_id_ubicacion in (IPJ.TYPES.C_AREA_JURIDICO, IPJ.TYPES.C_AREA_SXA, IPJ.TYPES.C_AREA_SRL ) --Juridico o SxA
        and (p_id_prot_dig is null or p_id_prot_dig = 'A') -- Si se listan todos o el A
      ;

  END SP_Buscar_Res_Prot_Dig;

  PROCEDURE SP_Reabrir_Tramite(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_cuil_usuario in varchar2,
    p_id_ubicacion in number,
    p_observacion varchar2,
    p_cuil_solicitante in varchar2)
  IS
    /* Si cumple todos los requisitos, se reabre un tr�mite cerrado, solo para correcci�n de datos*/
    v_cantidad number;
    v_row_tramite ipj.t_tramitesipj%rowtype;
    v_row_accion ipj.t_tramitesipj_acciones%rowtype;
    v_id_clasif_ipj number;
    v_cursor_acc ipj.types.cursortype;
    v_usuario_row ipj.t_usuarios%rowtype;
    v_rdo_accion varchar2(2000);
    v_tipo_mensaje_accion number;
  BEGIN
    -- Verifico que el usuario posea permisos para Reabrir tr�mites
    select * into v_usuario_row
    from ipj.t_usuarios u
    where
      cuil_usuario = p_cuil_solicitante;

    if nvl(v_usuario_row.hab_reabrir, 'N') <> 'S' then
      o_rdo := 'Usted no posee permisos para Re-Abrir Tr�mites.';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      return;
    end if;

    -- Busco el tr�mite para validaciones
    select * into v_row_tramite
    from ipj.t_tramitesipj
    where
      id_tramite_ipj = p_id_tramite_ipj;

    -- Si el expediente esta duplicado, no se abre
    select count(1) into v_cantidad
    from ipj.t_tramitesipj
    where
      upper(trim(expediente)) = upper(trim(v_row_tramite.expediente));

    if v_cantidad > 1 then
      o_rdo := 'El expediente que intenta reabrir se encuentra duplicado, comun�quese con el �rea de sistemas.';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      return;
    end if;

    -- Si el usuario indicado no posee permisos en el �rea, no se abre.
    select count(1) into v_cantidad
    from IPJ.T_GRUPOS_TRAB_UBICACION g
    where
      g.cuil = p_cuil_usuario and
      g.id_ubicacion = p_id_ubicacion;

    if v_cantidad = 0 then
      o_rdo := 'El Usuario indicado no posee permisos en esta �rea.';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      return;
    end if;

    -- Si el tramite no esta cerrado OK, no avanza
    if v_row_tramite.id_Estado_ult < 100 or v_row_tramite.id_Estado_ult >= 200 then
      o_rdo := 'El tr�mite no esta cerrado OK, no se puede reabrir; comun�quese con el �rea de sistemas.';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      return;
    end if;

    --***************************************************
    -- PROCESO OK
    --***************************************************
    -- Busco la clasificaci�n de la primer accion del area de origen
      begin
        select id_clasif_ipj into v_id_clasif_ipj
        from
          ( select ac.id_tramiteipj_accion, ta.id_clasif_ipj
            from ipj.t_tramitesipj_acciones ac join ipj.t_tipos_accionesipj ta
                on ac.id_tipo_accion = ta.id_tipo_accion
              join ipj.t_tipos_clasif_ipj tc
                on tc.id_clasif_ipj = ta.id_clasif_ipj
            where
              tc.id_ubicacion =  p_id_ubicacion and
              id_tramite_ipj = p_id_tramite_ipj
            order by ac.id_tramiteipj_accion asc
          ) tmp
          where
            rownum = 1;
      exception
        -- Sino, tomo la primer clasificacion del area
        when NO_DATA_FOUND then
          select id_clasif_ipj into v_id_clasif_ipj
          from ipj.t_tipos_clasif_ipj
          where
            id_ubicacion = p_id_ubicacion and
           rownum = 1;
      end;

    -- Reabro el tr�mite en el area original, al usuario indicado y en la primer clasifcacion del area.
    update ipj.t_tramitesipj
    set
      id_estado_ult = IPJ.TYPES.c_Estados_Asignado,
      cuil_ult_estado = p_cuil_usuario,
      id_grupo_ult_estado = null,
      id_ubicacion = p_id_ubicacion,
      id_clasif_ipj = v_id_clasif_ipj,
      observacion = (case
                               when p_observacion is null then 'Reabierto por ' || v_usuario_row.descripcion || ' (' || to_char(sysdate, 'dd/mm/rrrr') || ') ' || chr(13) || observacion
                               else substr(p_observacion || chr(13) || observacion, 1, 2000)
                             end)
    where
      Id_Tramite_Ipj = p_Id_Tramite_Ipj;

    -- Actualizo el historial
    IPJ.TRAMITES.SP_GUARDAR_TRAMITES_IPJ_ESTADO(
      o_rdo => o_rdo,
      o_tipo_mensaje => o_tipo_mensaje,
      v_fecha_pase => sysdate,
      v_id_estado => IPJ.TYPES.c_Estados_Asignado,
      v_cuil_usuario => p_Cuil_Usuario,
      v_id_grupo => null,
      v_observacion => p_observacion,
      v_Id_Tramite_Ipj => p_Id_Tramite_Ipj
    );

    --***************************************
    -- Busco todas las acciones que se deben reabrir
    --***************************************
    OPEN v_cursor_acc FOR
      select acc.*
      from ipj.t_tramitesipj tr join ipj.t_tramitesipj_acciones acc
          on tr.id_tramite_ipj = acc.id_tramite_ipj
        join ipj.t_tipos_accionesipj ta
          on acc.id_tipo_accion = ta.id_tipo_accion
        join ipj.t_tipos_clasif_ipj c
          on ta.id_clasif_ipj = c.id_clasif_ipj
      where
        tr.id_tramite_ipj = p_Id_Tramite_Ipj and
        c.id_ubicacion = tr.id_ubicacion_origen and -- Acciones del �rea de origen
        ta.id_prot_dig is null and -- No acciones de portocolos digitales
        acc.id_tipo_accion not in (34, 128, 134); -- Que no sean resoluciones

    LOOP
      fetch v_cursor_acc into v_row_accion;
      EXIT WHEN v_cursor_acc%NOTFOUND or v_cursor_acc%NOTFOUND is null;

      IPJ.ARCHIVO.SP_Reabrir_Accion(
        o_rdo => v_rdo_accion,
        o_tipo_mensaje => v_tipo_mensaje_Accion,
        p_Id_Tramite_Ipj => p_Id_Tramite_Ipj,
        p_id_tramiteipj_accion => v_row_accion.id_tramiteipj_accion,
        p_cuil_usuario => p_cuil_usuario,
        p_observacion => null
      );
    END LOOP;
    close v_cursor_acc;

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    commit;
  EXCEPTION
    WHEN OTHERS THEN
      o_rdo := 'SP_Reabrir_Tramite - ' || To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Reabrir_Tramite;

  /**********************************************************
  **********************************************************
        SOBRECARGA DE PARAMETROS
   **********************************************************
  **********************************************************/
  PROCEDURE SP_Buscar_Razon_Social(
    o_Cursor out IPJ.TYPES.CURSORTYPE,
    p_Cuit in varchar2,
    p_id_legajo in number,
    p_razon_social in varchar2,
    p_matricula in varchar2,
    p_ficha in varchar2,
    p_registro in number,
    p_id_ubicacion in number
    )
  IS
  BEGIN
    SP_Buscar_Razon_Social(
      o_Cursor => o_Cursor,
      p_Cuit => p_cuit,
      p_id_legajo => p_id_legajo,
      p_razon_social => p_razon_social,
      p_matricula => p_matricula,
      p_ficha => p_ficha,
      p_registro => p_registro,
      p_id_ubicacion => p_id_ubicacion,
      p_folio => null,
      p_anio => 0
      );
  END SP_Buscar_Razon_Social;

  PROCEDURE SP_GUARDAR_ANEXADOS (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj_Padre in number
    )
  IS
    /*********************************************************
    Este procedimeinto maneja el estado de los anexados, dependiendo del estado
    del padre. Al cerrar el padre, se cierran los anexos y se copia la version de la entidad
  **********************************************************/
    v_row_tram_padre IPJ.t_tramitesipj%rowtype;
    v_row_PersJur_padre IPJ.t_tramitesipj_persjur%rowtype;
    v_anexo_tramite number;
    v_anexo_legajo number;
    v_cursor_anexos ipj.types.cursortype;
    v_Cursor_PersJur_Padre ipj.types.cursortype;
    v_matricula_padre varchar2(20);
    v_version_padre number;
    v_cuit_padre varchar2(20);
    v_id_tramite_anexo number;
    v_metodo varchar2(200);
    v_origen varchar2(200);
  BEGIN
    -- Busco los datos del Padre
    select * into v_row_tram_padre
    from ipj.t_tramitesipj
    where
      id_tramite_ipj = p_Id_Tramite_Ipj_Padre;

    -- Si el padre esta completo, cerrado o rechazado, se replica en sus anexos.
    if v_row_tram_padre.id_estado_ult >= IPJ.TYPES.C_ESTADOS_COMPLETADO THEN
      -- Busco las empresas del tr�mite padre
      OPEN v_Cursor_PersJur_Padre FOR
        select tpj.*
        from IPJ.T_TRAMITESIPJ_PERSJUR tpj
        where
          tpj.Id_Tramite_Ipj = p_Id_Tramite_Ipj_Padre;

      LOOP
        fetch v_Cursor_PersJur_Padre into v_row_PersJur_padre;
        EXIT WHEN v_Cursor_PersJur_Padre%NOTFOUND or v_Cursor_PersJur_Padre%NOTFOUND is null;

        -- busco la version del padre
        begin
          select matricula, matricula_version, cuit
            into v_matricula_padre, v_version_padre, v_cuit_padre
          from ipj.t_entidades
          where
            id_tramite_ipj = p_Id_Tramite_Ipj_Padre and
            id_legajo = v_row_PersJur_padre.id_legajo;
        exception
           when NO_DATA_FOUND then
             v_matricula_padre := null;
             v_version_padre := null;
        end;

        -- Busco los anexos del padre y la empresa en curso. Bug14416:[Anexados] - Al cerrar un Sticker padre, no se cierra el sticker del anexado, el mismo queda abierto
        open v_cursor_anexos for
          select distinct ac.id_tramite_ipj, ac.id_legajo
          from ipj.t_tramitesipj_persjur ac join ipj.t_tramitesipj tr
            on ac.id_tramite_ipj = tr.id_tramite_ipj
          where
            tr.id_tramite_ipj_padre = v_row_tram_padre.id_tramite_ipj and
            ac.id_legajo = v_row_PersJur_padre.id_legajo;

        v_id_tramite_anexo := 0;
        LOOP
          fetch v_cursor_anexos into v_anexo_tramite, v_anexo_legajo;
          EXIT WHEN v_cursor_anexos%NOTFOUND or v_cursor_anexos%NOTFOUND is null;

          -- Si el padre tiene matr�cula y versi�n, se copia en todos los anexos
          if v_matricula_padre is not null then
            update ipj.t_entidades
            set
              matricula = v_matricula_padre,
              matricula_version = v_version_padre
            where
              id_tramite_ipj = v_Anexo_tramite and
              id_legajo = v_anexo_legajo;
          end if;

          -- Si el padre se completo o cerro, se marca cerradas, si se rechazo se rechazan
          update ipj.t_tramitesipj_acciones
          set id_estado = /*IPJ.TYPES.C_ESTADOS_CERRADO*/(case
                                    when v_row_tram_padre.id_estado_ult = IPJ.TYPES.C_ESTADOS_COMPLETADO then IPJ.TYPES.C_ESTADOS_CERRADO
                                    else v_row_tram_padre.id_estado_ult
                                  end)-- PBI6281:[SG] - [Anexados]
          where
            id_tramite_ipj = v_anexo_tramite and
            id_legajo = v_anexo_legajo and
            id_estado < IPJ.TYPES.C_ESTADOS_COMPLETADO;

          -- Para cada tr�mite, lo cierro, corrijo el borrador
          if v_id_tramite_anexo <> v_anexo_tramite then
            -- Finalizo el tr�mite
            update ipj.t_tramitesipj
            set id_estado_ult = IPJ.TYPES.C_ESTADOS_CERRADO--v_row_tram_padre.id_estado_ult
            where
              id_tramite_ipj = v_anexo_tramite;

            -- Sincroniza los tramites abiertos de la misma entidad
            /*IPJ.TRAMITES.SP_Act_Tramites_Relac(
              o_rdo => o_rdo,
              o_tipo_mensaje => o_tipo_mensaje,
              p_Id_Tramite_Ipj => v_anexo_tramite,
              p_id_legajo => v_anexo_legajo);

            if o_rdo != TYPES.C_RESP_OK then
              o_rdo :=  'Cerrar Anexos - Sincronizar Tr�mites: '|| o_rdo;
              return;
            end if;*/ --PV. No vamos a sincronizar los tr�mites abiertos

            -- Actualizo los datos en T_COMUNES solo si tiene CUIT
            /*if v_cuit_padre is not null  then
              IPJ.ENTIDAD_PERSJUR.sp_actualizar_pers_juridica(
                o_rdo =>  o_rdo,
                o_metodo => v_metodo,
                o_origen => v_origen,
                o_tipo_mensaje => o_tipo_mensaje,
                p_Id_Tramite_Ipj => v_anexo_tramite,
                p_id_legajo => v_anexo_legajo );

              if o_rdo != TYPES.C_RESP_OK then
                o_rdo := 'Cerrar Anexos - Actualizar Borrado / ' || v_metodo || ' ' ||v_origen  || ' ' ||o_rdo;
                return;
              end if;
            end if;*/--No vamos a actualizar los datos en esta instancia - PBI6281:[SG] - [Anexados]

            IPJ.ENTIDAD_PERSJUR.SP_Actualizar_Borrador(
              o_rdo => o_rdo,
              o_tipo_mensaje => o_tipo_mensaje,
              p_Id_Tramite_Ipj => v_anexo_tramite,
              p_id_legajo => v_anexo_legajo,
              p_borrador => 'N'
            );
            if o_rdo != TYPES.C_RESP_OK then
              o_rdo :=  'Cerrar Anexos - Actualizar Borrador: ' ||o_rdo;
              return;
            end if;
          end if;
        end loop;

        CLOSE v_cursor_anexos;
      end loop;

      CLOSE v_Cursor_PersJur_Padre;
    end if;

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     when OTHERS then
        o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_GUARDAR_ANEXADOS;

  PROCEDURE SP_Traer_TramitesIPJ_Tecnico_N(
    v_Cuil in varchar2,
    v_id_estado in number,
    p_id_clasif in number,
    p_cuil_logueado in varchar2,
    p_id_ubicacion in number,
    p_Cursor OUT types.cursorType)
  IS
    v_EsAdmin number;
  /****************************************************
   Este procedimiento muestra los tramites y acciones asignadas a un t�cnico o
   a un grupo al cual pertenezca.
   Al unir los datos, indica en la primer columna si es TRAMITE o ACCION
  *****************************************************/
  BEGIN
    -- Verifico si el usuario posee permisos de Administrador
    select count(*) into v_EsAdmin
    from IPJ.t_grupos_trab_ubicacion gru
    where
      gru.Cuil = p_cuil_logueado and
      gru.id_ubicacion = p_id_ubicacion and
      gru.Id_Grupo_Trabajo = IPJ.Types.C_Rol_Administrador;

    OPEN p_Cursor FOR
      select
        tmp.botones,
        tmp.Clase, tmp.id_tramite Id_Tramite_Suac, tmp.Sticker, tmp.fecha_ini_suac Fecha_Inicio_Suac,
        null Fecha_recepcion,
        (case tmp.id_proceso
           when 4 then 'GENERACION DE HISTORICOS - ' || tmp.N_UBICACION
           when 5 then 'CARGA DE HISTORICOS'
           else IPJ.TRAMITES_SUAC.FC_BUSCAR_SUBTIPO_SUAC(tmp.id_tramite)
        end) Tipo,
        (case tmp.id_proceso
            when 4 then 'GENERACION DE HISTORICOS - ' || tmp.N_UBICACION
            when 5 then tmp.observacion
            else IPJ.TRAMITES_SUAC.FC_BUSCAR_ASUNTO_SUAC(tmp.id_tramite)
        end) Asunto,
        tmp.Expediente Nro_Expediente, tmp.Id_Tramite_Ipj, tmp.id_clasif_ipj,
        tmp.id_tipo_accion, tmp.Observacion, tmp.id_estado, tmp.cuil_usuario, tmp.n_estado,
        tmp.Tipo_IPJ, tmp.Fecha_Asignacion, tmp.Acc_Abiertas, tmp.Acc_Cerradas, tmp.URGENTE,
        tmp.usuario, nvl(tmp.Error_Dato, 'N') Error_Dato, tmp.CUIT, tmp.razon_social, tmp.id_legajo,
        tmp.N_UBICACION, tmp.CUIL_CREADOR, tmp.id_ubicacion, tmp.id_tramiteipj_accion,
        tmp.id_protocolo, tmp.ID_INTEGRANTE, tmp.NRO_DOCUMENTO, tmp.Cuit_PersJur,
        tmp.Obs_Rubrica, tmp.identificacion, tmp.persona, tmp.ID_FONDO_COMERCIO,
        tmp.id_proceso, tmp.id_pagina, tmp.cuil_usuario cuil_usuario_old, tmp.ID_DOCUMENTO,
        tmp.N_DOCUMENTO,tmp.simple_tramite, tmp.Agrupable, tmp.codigo_online,
        ( select count(1)
          from ipj.t_ol_entidades eo
          where
            eo.codigo_online = tmp.codigo_online and
            ((eo.id_tipo_tramite_ol = 1 and eo.id_sub_tramite_ol = 1) or
             (eo.id_tipo_tramite_ol = 20 and eo.id_sub_tramite_ol = 5) or
             (eo.id_tipo_tramite_ol = 17 and eo.id_sub_tramite_ol = 7) or
             (eo.id_tipo_tramite_ol = 23 and eo.id_sub_tramite_ol = 9) or
             (eo.id_tipo_tramite_ol = 52 and eo.id_sub_tramite_ol = 11) or
             eo.id_tipo_tramite_ol = 19)
        ) Const_SA,
        (select eo.id_obj_social from ipj.t_ol_entidades eo where eo.codigo_online = tmp.codigo_online) id_Obj_Social,
        tmp.id_tramite_ipj_padre,
        --IPJ.TRAMITES_SUAC.FC_BUSCAR_SUBTIPO_SUAC(tmp.id_tramite) id_subtipo_tramite,
        IPJ.TRAMITES.FC_N_Subtipo_Suac(IPJ.TRAMITES_SUAC.FC_BUSCAR_SUBTIPO_SUAC(tmp.id_tramite), tmp.id_tramite) id_subtipo_tramite,
        (select count(1) from IPJ.T_ENTIDADES_AUTORIZADOS ea where ea.id_tramite_ipj = tmp.id_tramite_ipj and nvl(ea.mail_autorizado, 'N') = 'S') dom_electronico,
        IPJ.TRAMITES.FC_Es_Tram_Digital(tmp.id_tramite_ipj) Es_Digital,
        (select nvl(tr.enviar_mail, 0) from ipj.t_tramitesipj tr where tr.id_tramite_ipj = tmp.id_tramite_ipj) enviar_mail,
        (select o.cod_seguridad from ipj.t_ol_entidades o where o.codigo_online = tmp.codigo_online) cod_seguridad
      from
        (SELECT IPJ.varios.FC_Habilitar_Botones (
             tIPJ.id_ubicacion,
             tIPJ.id_clasif_ipj,
             0,
             tIPJ.id_estado_ult,
             IPJ.TRAMITES.FC_Acciones_Abiertas (tIPJ.id_tramite_ipj),
             0,
             2,
             tIPJ.id_proceso,
             tIPJ.simple_tramite,
             NVL (p_cuil_logueado, tIpj.cuil_ult_estado),
             NVL (p_cuil_logueado, tIpj.cuil_ult_estado),
             tIPJ.id_tramite_ipj,
             0)
             botones,
          1 Clase,
          tIPJ.id_tramite,
          tIPJ.id_tramite_ipj,
          tIPJ.id_clasif_ipj,
          0 id_tipo_accion,
          tIPJ.Observacion,
          tIPJ.id_estado_ult id_estado,
          tIPJ.id_estado_ult id_estado_tramite,
          0 id_estado_accion,
          tIpj.cuil_ult_estado CUIL_USUARIO,
          (SELECT n_estado
             FROM IPJ.t_estados e
            WHERE tIPJ.id_estado_ult = e.id_estado)
             n_estado,
          (SELECT N_Clasif_Ipj
             FROM IPJ.t_tipos_Clasif_ipj tc
            WHERE TIPJ.Id_Clasif_Ipj = tc.Id_Clasif_Ipj)
             Tipo_IPJ,
          IPJ.TRAMITES.FC_Ultima_Fecha_Tramite (tIPJ.id_tramite_ipj)
             Fecha_Asignacion,
          IPJ.TRAMITES.FC_Acciones_Abiertas (tIPJ.id_tramite_ipj)
             Acc_Abiertas,
          IPJ.TRAMITES.FC_Acciones_Cerradas (tIPJ.id_tramite_ipj)
             Acc_Cerradas,
          TIPJ.URGENTE,
          NVL ( (SELECT descripcion
                   FROM ipj.t_usuarios u
                  WHERE u.cuil_usuario = tIpj.cuil_ult_estado),
               (select n_grupo from ipj.t_grupos gr where gr.id_grupo = tIPJ.ID_GRUPO_ULT_ESTADO))
             usuario,
          'N' Error_Dato,
          '' cuit,
          '' razon_social,
          0 id_legajo,
          (SELECT n_ubicacion
             FROM ipj.t_ubicaciones u
            WHERE u.id_ubicacion = tIPJ.id_ubicacion)
             N_UBICACION,
          TIPJ.CUIL_CREADOR,
          tIPJ.id_ubicacion,
          0 id_tramiteipj_accion,
          0 id_protocolo,
          0 ID_INTEGRANTE,
          NULL NRO_DOCUMENTO,
          NULL Cuit_PersJur,
          '' Obs_Rubrica,
          '' identificacion,
          '' persona,
          NULL ID_FONDO_COMERCIO,
          tIPJ.id_proceso,
          0 id_pagina,
          0 Agrupable,
          p_cuil_logueado CUIL_USUARIO_grupo,
          tIPJ.id_Grupo_Ult_Estado,
          TIPJ.STICKER,
          TIPJ.EXPEDIENTE,
          NULL id_documento,
          NULL n_documento,
          TIPJ.SIMPLE_TRAMITE,
          (SELECT n_ubicacion
             FROM IPJ.T_Ubicaciones
            WHERE id_ubicacion = TIPJ.ID_UBICACION_ORIGEN)
             n_ubicacion_origen,
          tipj.codigo_online,
          tipj.id_tramite_ipj_padre,
          TO_CHAR (tipj.fecha_ini_suac, 'dd/mm/rrrr') fecha_ini_suac
     FROM IPJ.t_tramitesIPJ tIPJ
    WHERE
      NVL (tIPJ.id_estado_ult, 0) <> 110     --IPJ.TYPES.C_ESTADOS_CERRADO
      and tipj.id_ubicacion = p_id_ubicacion
        ) tmp
      order by Const_SA desc , id_tramite_ipj asc, clase asc;

  END SP_Traer_TramitesIPJ_Tecnico_N;

  PROCEDURE SP_Buscar_Expte_Referencia(
    o_Cursor OUT types.cursorType,
    p_sticker in varchar2,
    p_expediente in varchar2)
  IS
  /****************************************************
   Muestra el area, estado y cuil responsable de un expediente/stiker rechazado de una entidad para eximir
  *****************************************************/

  v_row_tramite  ipj.t_tramitesipj%ROWTYPE;
  BEGIN

    BEGIN
      SELECT *
        INTO v_row_tramite
        FROM ipj.t_tramitesipj t
       WHERE (p_sticker IS NOT NULL AND t.sticker = p_sticker)
          OR (p_expediente IS NOT NULL AND t.expediente = p_expediente);
    EXCEPTION
      WHEN OTHERS THEN
        RETURN;
    END;

    IF ipj.tramites.fc_es_tram_const(v_row_tramite.id_tramite_ipj) > 0 THEN

      --Si es tr�mite de Constituci�n Rechazado
      OPEN o_cursor FOR
        SELECT t.id_tramite_ipj, ent.id_legajo, t.expediente, t.sticker, t.cuil_ult_estado,
               t.id_estado_ult, ub.n_ubicacion, e.n_estado, t.observacion,
               ipj.tramites_suac.fc_buscar_fec_ini_suac(t.id_tramite) fecha_inicio_suac,
               ipj.tramites_suac.fc_buscar_asunto_suac(t.id_tramite) asunto, eol.codigo_reserva,
               decode((SELECT COUNT(1)
                         FROM ipj.t_expte_referenciado
                        WHERE id_tramite_ipj_referencia = t.id_tramite_ipj)
                      ,1
                      ,'S'
                      ,'N') asignado_sn
          FROM ipj.t_tramitesipj t, ipj.t_ubicaciones ub, ipj.t_estados e, ipj.t_entidades ent,
               ipj.t_ol_entidades eol
         WHERE t.id_ubicacion = ub.id_ubicacion
           AND t.id_estado_ult = e.id_estado
           AND ent.id_tramite_ipj = t.id_tramite_ipj
           AND t.codigo_online = eol.codigo_online
           AND t.id_estado_ult >= ipj.types.c_estados_rechazado
           AND t.id_tramite_ipj NOT IN
               (SELECT er.id_tramite_ipj
                  FROM ipj.t_expte_referenciado er
                 WHERE er.id_legajo = ent.id_legajo)
           AND t.id_tramite_ipj = v_row_tramite.id_tramite_ipj;

    ELSE
      --Si es tr�mite de Asamblea Rechazado (b�sca el �ltimo rechazado)
      OPEN o_cursor FOR
        SELECT t.id_tramite_ipj, ea.id_legajo, t.expediente, t.sticker, ea.id_entidad_acta,
               ea.fec_acta, t.cuil_ult_estado, t.id_estado_ult, ub.n_ubicacion, e.n_estado,
               t.observacion,
               ipj.tramites_suac.fc_buscar_fec_ini_suac(t.id_tramite) fecha_inicio_suac,
               ipj.tramites_suac.fc_buscar_asunto_suac(t.id_tramite) asunto, ea.id_tipo_acta,
               ta.n_tipo_acta, eol.id_tipo_tramite_ol id_tipo_tramite_ol_ref,
               decode((SELECT COUNT(1)
                         FROM ipj.t_expte_referenciado
                        WHERE id_tramite_ipj_referencia = t.id_tramite_ipj)
                      ,1
                      ,'S'
                      ,'N') asignado_sn
          FROM ipj.t_tramitesipj t, ipj.t_ubicaciones ub, ipj.t_estados e, ipj.t_entidades_acta ea,
               ipj.t_tipos_acta ta, ipj.t_ol_entidades eol
         WHERE t.id_ubicacion = ub.id_ubicacion
           AND t.id_estado_ult = e.id_estado
           AND ea.id_tramite_ipj = t.id_tramite_ipj
           AND ea.id_tipo_acta = ta.id_tipo_acta
           AND t.codigo_online = eol.codigo_online(+)
           AND t.id_estado_ult >= ipj.types.c_estados_rechazado
           AND ea.fec_acta = (SELECT MAX(ea2.fec_acta)
                                FROM ipj.t_entidades_acta ea2
                               WHERE ea2.id_tramite_ipj = ea.id_tramite_ipj)
           AND t.id_tramite_ipj NOT IN (SELECT er.id_tramite_ipj
                                          FROM ipj.t_expte_referenciado er
                                         WHERE er.id_legajo = ea.id_legajo)
              --AND T.ID_PROCESO not in (4, 5) and  -- Elimino los historicos
              /*AND ((p_sticker IS NOT NULL AND t.sticker = p_sticker)
                        OR (p_expediente IS NOT NULL AND t.expediente = p_expediente))*/
           AND t.id_tramite_ipj = v_row_tramite.id_tramite_ipj;

    END IF;

  END SP_Buscar_Expte_Referencia;

  PROCEDURE SP_Guardar_Expte_Referencia(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_codigo_online in number,
    p_tipo_tramite_referencia number,
    p_id_tramite_ipj_referencia in number,
    p_id_legajo_referencia in number,
    p_expte_referencia in varchar2,
    p_obervacion in varchar2)
  IS
    v_valid_parametros varchar2(2000);
    v_n_tipo_tramite_ol varchar2(200);
    v_row_ol_entidad  ipj.t_ol_entidades%ROWTYPE;

  /***************************************************************
   Guarda el expediente de referencia y referenciado
  *****************************************************************/
  BEGIN

    if nvl(p_codigo_online, 0) = 0 then --Debe existir un C�digo Online
      v_valid_parametros := v_valid_parametros || 'C�dgo Online Inv�lido';
    end if;
    /*if nvl(p_tipo_tramite_referencia, 0) = 0 then --Debe existir un tipo de tr�mite de referencia
      v_valid_parametros := v_valid_parametros || 'Tipo de Tr�mite Inv�lido';
    end if;*/
    if nvl(p_id_tramite_ipj_referencia, 0) = 0 then --Debe existir un tramite de referencia
      v_valid_parametros := v_valid_parametros || 'Tr�mite de Referencia Inv�lido';
    end if;
    if nvl(p_id_legajo_referencia, 0) = 0 then --Debe existir un legajo de refeencia
      v_valid_parametros := v_valid_parametros || 'Legajo de Referencia Inv�lido';
    end if;
    if p_expte_referencia is null then --Debe existir el expediente de referencia
      v_valid_parametros := v_valid_parametros || 'Expediente de Referencia Inv�lido';
    end if;

    -- Si los paramentros no estan OK, no continuo
    if v_valid_parametros is not null then
      o_rdo := 'Parametros : ' || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    -- Busco los datos para comenzar los contoles
    SELECT *
      INTO v_row_ol_entidad
      FROM ipj.t_ol_entidades e
     WHERE e.codigo_online = p_codigo_online;

    SELECT t.n_tipo_tramite_ol
      INTO v_n_tipo_tramite_ol
      FROM ipj.t_tipos_tramites_ol t
     WHERE t.id_tipo_tramite_ol = v_row_ol_entidad.id_tipo_tramite_ol;

     IF v_row_ol_entidad.id_tipo_tramite_ol in (1, 17, 20, 23, 52) THEN

        IF v_row_ol_entidad.id_sub_tramite_ol not in (2, 8, 6, 10, 12) THEN
          o_rdo := 'El Tr�mite/Expediente de Constituci�n no es Instrumento Redactado.';
          o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
          return;
        END IF;

        BEGIN
          INSERT INTO ipj.t_expte_referenciado
            (id_tramite_ipj_referencia, expte_referencia, fecha_alta_exim, observacion, codigo_online, id_tipo_tramite_ol, n_tipo_tramite_ol)
          VALUES
            (p_id_tramite_ipj_referencia, p_expte_referencia, SYSDATE, p_obervacion, p_codigo_online, v_row_ol_entidad.id_tipo_tramite_ol, v_n_tipo_tramite_ol);
          COMMIT;
        EXCEPTION
          WHEN OTHERS THEN
            o_rdo := 'El Sticker/Expediente de Referencia, ha sido asociado en un tr�mite anterior.';
            o_tipo_mensaje := ipj.types.c_tipo_mens_warning;
            RETURN;
        END;

     ELSIF v_row_ol_entidad.id_tipo_tramite_ol in (21, 22, 26, 30, 31) THEN

        IF v_row_ol_entidad.id_legajo <> p_id_legajo_referencia THEN
          o_rdo := 'El Tr�mite/Expediente de referencia no corresponde a la misma Entidad';
          o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
          return;
        END IF;
        IF v_row_ol_entidad.id_tipo_tramite_ol <> p_tipo_tramite_referencia THEN
          o_rdo := 'El Tipo de Tr�mite de referencia no corresponde al tipo de tramite actual';
          o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
          return;
        END IF;

        BEGIN
          INSERT INTO ipj.t_expte_referenciado
            (id_tramite_ipj_referencia, expte_referencia, fecha_alta_exim, observacion, codigo_online, id_tipo_tramite_ol, n_tipo_tramite_ol)
          VALUES
            (p_id_tramite_ipj_referencia, p_expte_referencia, SYSDATE, p_obervacion, p_codigo_online, v_row_ol_entidad.id_tipo_tramite_ol, v_n_tipo_tramite_ol);
          COMMIT;
        EXCEPTION
          WHEN OTHERS THEN
            o_rdo := 'El Sticker/Expediente de Referencia, ha sido asociado en un tr�mite anterior.';
            o_tipo_mensaje := ipj.types.c_tipo_mens_warning;
            RETURN;
        END;

     ELSE
        o_rdo := 'El Tr�mite/Expediente no Corresponde a un Tramite Referenciado.';
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
        return;
     END IF;

    o_rdo := TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
    WHEN OTHERS THEN
      o_rdo := 'SP_GUARDAR_EXPTE_REFERENCIA: ' || To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;

  END SP_Guardar_Expte_Referencia;


  PROCEDURE SP_Eiminar_Expte_Referencia(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in number,--Pasar p_id_tramite_ipj_referencia
    p_expediente in varchar2) --Pasar p_expte_referencia

  IS

  BEGIN
    IF (nvl(p_id_tramite_ipj, 0) <> 0) THEN

      DELETE ipj.t_expte_referenciado
       WHERE id_tramite_ipj_referencia = p_id_tramite_ipj
         AND expte_referencia = p_expediente;

    END IF;
    o_rdo := ipj.types.c_resp_ok;
    o_tipo_mensaje := ipj.types.c_tipo_mens_ok;
    COMMIT;
  EXCEPTION
    WHEN OTHERS THEN
      o_rdo := 'SP_EIMINAR_EXPTE_REFERENCIA: ' || to_char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := ipj.types.c_tipo_mens_error;
      ROLLBACK;
  END SP_Eiminar_Expte_Referencia;


  PROCEDURE SP_Traer_Expte_Referencia (
    o_Cursor OUT types.cursorType,
    p_codigo_online in NUMBER)

  IS
  BEGIN

    OPEN o_Cursor FOR
      SELECT id_legajo, id_tramite_ipj, id_tramite_ipj_referencia, expte_referencia, fecha_alta_exim,
             observacion, codigo_online, id_tipo_tramite_ol, n_tipo_tramite_ol
        FROM ipj.t_expte_referenciado
       WHERE codigo_online = p_codigo_online;

  END SP_Traer_Expte_Referencia;

  PROCEDURE SP_Orden_Dia_Expte (
    o_Cursor OUT types.cursorType,
    p_id_entidad_Acta in number)
  IS
  /**************************************************
    Lista las Ordenes del d�a seleccionadas para un Expte/Tramite Determinado
  ***************************************************/
  BEGIN

   --Filtra por id_entidad_acta
    OPEN o_Cursor FOR
      SELECT t.id_tipo_orden_dia, t.n_tipo_orden_dia, t.siglas
        FROM ipj.t_tipos_orden_dia t
       WHERE id_tipo_orden_dia IN
             (SELECT id_tipo_orden_dia
                FROM ipj.t_entidades_acta_orden a
               WHERE a.id_entidad_acta = p_id_entidad_acta);

  END SP_Orden_Dia_Expte;

end Tramites;
/

