CREATE OR REPLACE PACKAGE IPJ.PKG_UIF_OLD AS
  /*
   ORA-02292 integrity constraint (string.string) violated - child record found
   Cause: An attempt was made to delete a row that is referenced by a foreign key.
  */

  /* invalid number*/
  V_ERROR_01722 EXCEPTION;
  PRAGMA EXCEPTION_INIT(V_ERROR_01722, -01722);

  V_ERROR_2292 EXCEPTION;
  PRAGMA EXCEPTION_INIT(V_ERROR_2292, -2292);

  /*
  ORA-00001 unique constraint (string.string) violated
  Cause: An UPDATE or INSERT statement attempted to insert a duplicate key.
  */
  V_ERROR_00001 EXCEPTION;
  PRAGMA EXCEPTION_INIT(V_ERROR_00001, -00001);

  /*
  ORA-01407 cannot update (string) to NULL
  Cause: An attempt was made to update a table column USER.TABLE.COLUMN with a NULL val
  */
  V_ERROR_01407 EXCEPTION;
  PRAGMA EXCEPTION_INIT(V_ERROR_01407, -01407);

  TYPE TY_CURSOR IS REF CURSOR;

  PROCEDURE BUSCAR_CONSTITUCION_SOCIEDADES(p_T_CONSULTA  OUT TY_CURSOR,
                                           P_FECHA_DESDE IN IPJ.t_Entidades.FEC_INSCRIPCION%TYPE DEFAULT NULL,
                                           P_FECHA_HASTA IN IPJ.t_Entidades.FEC_INSCRIPCION%TYPE DEFAULT NULL);

  PROCEDURE SOCIO_PERSONA_FISICA(p_T_CONSULTA  OUT TY_CURSOR,
                                 P_FECHA_DESDE IN IPJ.t_Entidades.FEC_INSCRIPCION%TYPE DEFAULT NULL,
                                 P_FECHA_HASTA IN IPJ.t_Entidades.FEC_INSCRIPCION%TYPE DEFAULT NULL,
                                 P_CUIL        IN IPJ.t_Entidades.CUIT%TYPE DEFAULT NULL);

  PROCEDURE SOCIO_PERSONA_JURIDICA(p_T_CONSULTA  OUT TY_CURSOR,
                                   P_FECHA_DESDE IN IPJ.t_Entidades.FEC_INSCRIPCION%TYPE DEFAULT NULL,
                                   P_FECHA_HASTA IN IPJ.t_Entidades.FEC_INSCRIPCION%TYPE DEFAULT NULL,
                                   P_CUIL        IN IPJ.t_Entidades.CUIT%TYPE DEFAULT NULL);

  PROCEDURE ENTIDADES_SIN_FINES_DE_LUCRO(p_T_CONSULTA  OUT TY_CURSOR,
                                         P_FECHA_DESDE IN IPJ.t_Entidades.FEC_INSCRIPCION%TYPE DEFAULT NULL,
                                         P_FECHA_HASTA IN IPJ.t_Entidades.FEC_INSCRIPCION%TYPE DEFAULT NULL);
  FUNCTION OBTENER_LISTA_PALABRAS(p_frase_Clave    IN VARCHAR2,
                                  cantidad_palabra out number,
                                  LARGO_PALABRA    IN NUMBER)
    RETURN dbms_sql.varchar2_table;

  FUNCTION COMPARAR_PALABRAS(p_frase_Clave    IN VARCHAR2,
                             p_frase_comparar in VARCHAR2) RETURN number;

  FUNCTION COMPARAR_PALABRAS_PROMEDIO(p_frase_Clave    IN VARCHAR2,
                                      p_frase_comparar in VARCHAR2)
    RETURN number;
  FUNCTION COMPARAR_PALABRAS_PROMEDIO_1(p_frase_Clave    IN VARCHAR2,
                                        p_frase_comparar in VARCHAR2)
    RETURN number;

  FUNCTION COMPARAR_PALABRAS_PROMEDIO3(p_frase_Clave    IN VARCHAR2,
                                       p_frase_comparar in VARCHAR2)
    RETURN number;

  FUNCTION COMPARAR_PALABRAS_COMBINADAS(p_frase_Clave    IN VARCHAR2,
                                        p_frase_comparar in VARCHAR2)
    RETURN number;
  FUNCTION COMPARAR_PALABRAS_COMBINADAS_1(p_frase_Clave    IN VARCHAR2,
                                          p_frase_comparar in VARCHAR2)
    RETURN number;
  FUNCTION COMPARAR_PALABRAS_COMBINADAS_PRO(p_frase_Clave    IN VARCHAR2,
                                            p_frase_comparar in VARCHAR2)
    RETURN number;

  FUNCTION PALABRAS_COMBINADAS_PRO_1(p_frase_Clave    IN VARCHAR2,
                                     p_frase_comparar in VARCHAR2)
    RETURN number;

  FUNCTION PALABRAS_PROMEDIO_BASE(p_frase_Clave    IN VARCHAR2,
                                  p_frase_comparar in VARCHAR2) RETURN number;

  FUNCTION COMPARAR_PALABRAS_ENCONTRADAS(p_frase_Clave    IN VARCHAR2,
                                         p_frase_comparar in VARCHAR2)
    RETURN number;

END PKG_UIF_OLD;
/

CREATE OR REPLACE PACKAGE BODY IPJ.PKG_UIF_OLD AS

  PROCEDURE BUSCAR_CONSTITUCION_SOCIEDADES(p_T_CONSULTA  OUT TY_CURSOR,
                                           P_FECHA_DESDE IN IPJ.t_Entidades.FEC_INSCRIPCION%TYPE DEFAULT NULL,
                                           P_FECHA_HASTA IN IPJ.t_Entidades.FEC_INSCRIPCION%TYPE DEFAULT NULL) IS
    V_MSG         VARCHAR2(300);
    V_FECHA_DESDE DATE;
    V_FECHA_HASTA DATE;
    V_SENTENCIA   VARCHAR2(50);
  
  BEGIN
  
    V_FECHA_DESDE := P_FECHA_DESDE;
    V_FECHA_HASTA := P_FECHA_HASTA;
  
    V_SENTENCIA := '  ALTER SESSION SET NLS_DATE_FORMAT=' || '''' ||
                   'DD/MM/YYYY' || '''';
  
    EXECUTE IMMEDIATE V_SENTENCIA;
  
    OPEN p_T_CONSULTA FOR
    
    ------CONSTITUCIONES TODAS DESDE OCTUBRE 2020 y socios BEA
    
      select distinct REPLACE((upper(substr(e.denominacion_1, 1, 1)) ||
                              lower(SUBSTR(e.denominacion_1, 2))),
                              '\&',
                              '\&amp;') AS "Denominaci93n",
                      
                      CASE translate(UPPER(te.tipo_entidad),
                                 '����������',
                                 'aeiouAEIOU')
                      
                        WHEN UPPER('Sociedad de bolsa') THEN
                         'Sociedad de bolsa' --
                      
                        WHEN UPPER('Sociedad colectiva') THEN
                         'Sociedad colectiva' --
                      
                        WHEN UPPER('Sociedades en comandita simple') THEN
                         'Sociedad en comandita simple' --
                      
                        WHEN UPPER('Sociedad de capital e industria') THEN
                         'Sociedad de capital e industria' --
                      
                        WHEN UPPER('Sociedad de responsabilidad limitada') THEN
                         'Sociedad de responsabilidad limitada' --
                      
                        WHEN UPPER('Sociedad anonima') THEN
                         'Sociedad an�nima' --
                      
                        WHEN UPPER('Sociedades del estado') THEN
                         'Sociedad del estado' --
                      
                        WHEN UPPER('Sociedad en comandita por acciones') THEN
                         'Sociedad en comandita por acciones' --
                      
                        WHEN UPPER('Sociedad de hecho') THEN
                         'Sociedad de hecho' --
                      
                        WHEN UPPER('Sociedad constituida en el extranjero') THEN
                         'Sociedad constituida en el extranjero' --
                      
                        WHEN
                         UPPER('Sociedad binacional fuera de jurisdiccion') THEN
                         'Sociedad binacional fuera de jurisdicci�n' --
                      
                        WHEN UPPER('Asociacion civil') THEN
                         'Asociaci�n civil' --
                      
                        WHEN UPPER('Entidad extranjera sin fines de lucro') THEN
                         'Entidad extranjera sin fines de lucro' --
                      
                        WHEN UPPER('Fundacion') THEN
                         'Fundacion' --
                      
                        WHEN UPPER('Simples asociaciones') THEN
                         'Simples asociaciones' --        
                      
                        WHEN UPPER('Federacion') THEN
                         'Federaci�n' --      
                      
                        WHEN UPPER('Confederacion') THEN
                         'Confederaci�n' --      
                      
                        WHEN UPPER('C�mara') THEN
                         'C�mara' --      
                      
                        WHEN UPPER('Contrato de colaboracion empresaria') THEN
                         'Contrato de colaboraci�n empresaria' --    
                      
                        WHEN UPPER('Union transitoria') THEN
                         'Uni�n transitoria de empresas' --    
                      
                        WHEN UPPER('Sociedad de garantia reciproca') THEN
                         'Sociedad de garant�a rec�iproca' --    
                      
                        WHEN UPPER('Soc. capitalizacion y ahorro') THEN
                         'Soc. capitalizacion y ahorro"' --    
                      
                        WHEN UPPER('Cooperativas') THEN
                         'Cooperativa' --    
                      
                        WHEN UPPER('Mutuales') THEN
                         'Mutual' --    
                      
                        WHEN UPPER('Fideicomisos') THEN
                         'Fideicomiso' --
                      
                        ELSE
                         'Otros'
                      END AS Tipo_Sociedad,
                      
                      e.cuit "CUIT_CUIL",
                      
                      --  TO_NUMBER ( trim(REPLACE (translate (upper (e.nro_resolucion),'AB / " - ','*') ,'*','')))"Nro_de_denominaci93n" ,--JP
                      
                      to_char(to_date(e.acta_contitutiva, 'dd/mm/yyyy'),
                              'yyyy-MM-dd') || 'T00:00:00-03:00' /*E.ACTA_CONTITUTIVA*/ "Fecha_de_Constituci93n",
                      
                      DBMS_LOB.substr(e.objeto_social,
                                      DBMS_LOB.getlength(e.objeto_social),
                                      1) Objeto_Social,
                      
                      (select decode((upper(substr(m.n_moneda, 1, 1)) ||
                                     lower(SUBSTR(m.n_moneda, 2))),
                                     'Peso',
                                     'Peso Argentino',
                                     (upper(substr(m.n_moneda, 1, 1)) ||
                                     lower(SUBSTR(m.n_moneda, 2))))
                         from t_comunes.t_monedas m
                        where m.id_moneda = e.id_moneda) "Tipo_de_Moneda",
                      
                      e.monto "Valor_del_Capital_Social",
                      
                      e.monto "Valor_del_Capital_Social_en_Pesos",
                      
                      e.monto "Monto_en_Efectivo",
                      
                      0 Valor_Estimado_de_Bienes,
                      /*e.cuotas,
                      e.valor_cuota,*/
                      
                      (select (upper(substr(d.n_calle, 1, 1)) ||
                              lower(SUBSTR(d.n_calle, 2)))
                         from dom_manager.vt_domicilios_cond d
                        where d.id_vin =
                              IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(l.id_legajo,
                                                                    'S')) "Calle",
                      
                      (select d.altura
                         from dom_manager.vt_domicilios_cond d
                        where d.id_vin =
                              IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(l.id_legajo,
                                                                    'S')) "Nro",
                      
                      (select d.piso
                         from dom_manager.vt_domicilios_cond d
                        where d.id_vin =
                              IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(l.id_legajo,
                                                                    'S')) "Piso",
                      
                      (select d.depto
                         from dom_manager.vt_domicilios_cond d
                        where d.id_vin =
                              IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(l.id_legajo,
                                                                    'S')) "Departamento",
                      
                      (select (upper(substr(d.N_Localidad, 1, 1)) ||
                              lower(SUBSTR(d.N_Localidad, 2)))
                         from dom_manager.vt_domicilios_cond d
                        where d.id_vin =
                              IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(l.id_legajo,
                                                                    'S')) "Localidad",
                      
                      (select d.cpa
                         from dom_manager.vt_domicilios_cond d
                        where d.id_vin =
                              IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(l.id_legajo,
                                                                    'S')) "C93digo_Postal",
                      
                      (select (upper(substr(d.n_provincia, 1, 1)) ||
                              lower(SUBSTR(d.n_provincia, 2)))
                         from dom_manager.vt_domicilios_cond d
                        where d.id_vin =
                              IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(l.id_legajo,
                                                                    'S')) "Provincia",
                      
                      (select (upper(substr(pa.n_pais, 1, 1)) ||
                              lower(SUBSTR(pa.n_pais, 2)))
                         from dom_manager.vt_paises pa
                        where pa.ID_PAIS = 'ARG') "Pa92s",
                      
                      '' as Prefijo,
                      '' as Tel91fono,
                      '' as Email
      
        from ipj.t_tipos_entidades         te,
             ipj.t_tramitesipj             tr,
             nuevosuac.vt_tramites_id      suactr,
             nuevosuac.vt_subtipos_tramite subtip,
             ipj.t_legajos                 l
        join ipj.t_entidades e
          on l.id_legajo = e.id_legajo
        join ipj.t_entidades_admin ad
          on e.id_tramite_ipj = ad.id_tramite_ipj
         and e.id_legajo = ad.id_legajo
        join ipj.t_integrantes i
          on i.id_integrante = ad.id_integrante
        join ipj.t_tipos_integrante ti
          on ti.id_tipo_integrante = ad.id_tipo_integrante
        join ipj.t_tipos_organismo o
          on o.id_tipo_organismo = ad.id_tipo_organismo
      
       where te.id_tipo_entidad = e.id_tipo_entidad
         and tr.id_tramite_ipj = e.id_tramite_ipj
         and suactr.id_tramite = tr.id_tramite
         and subtip.id_subtipo_tramite = suactr.id_subtipo_tramite
            
         and te.id_ubicacion = 4
            -- excluir las entidades que tienen persona juridica
         and not exists
       (select 1
                from t_entidades_socios es1
               where (es1.cuit_empresa is not null or
                     es1.n_empresa is not null)
                 and es1.borrador = 'N'
                 and e.id_legajo = es1.id_legajo
                 and e.id_tramite_ipj = es1.id_tramite_ipj
                 and es1.fecha_fin is null)
            
            --INICIO sector valores null
         and e.denominacion_1 is not null
         and te.tipo_entidad is not null
         and length(e.acta_contitutiva) = 10 --el requerimiento dice que no puede estar vacio, y el formato es de 10 digitos (10/01/2012)
            -- por lo tanto debe cumplir eso para su correcta transformacion.
            
         and e.monto is not null
         and (select d.n_calle
                from dom_manager.vt_domicilios_cond d
               where d.id_vin =
                     IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(l.id_legajo, 'S')) is not null
         and (select d.altura
                from dom_manager.vt_domicilios_cond d
               where d.id_vin =
                     IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(l.id_legajo, 'S')) is not null
            
         and (select (upper(substr(d.N_Localidad, 1, 1)) ||
                     lower(SUBSTR(d.N_Localidad, 2)))
                from dom_manager.vt_domicilios_cond d
               where d.id_vin =
                     IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(l.id_legajo, 'S')) is not null
            
         and (select (upper(substr(d.n_provincia, 1, 1)) ||
                     lower(SUBSTR(d.n_provincia, 2)))
                from dom_manager.vt_domicilios_cond d
               where d.id_vin =
                     IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(l.id_legajo, 'S')) is not null
         and (select (upper(substr(pa.n_pais, 1, 1)) ||
                     lower(SUBSTR(pa.n_pais, 2)))
                from dom_manager.vt_paises pa
               where pa.ID_PAIS = 'ARG') is not null
            
         and (select (upper(substr(vp.APELLIDO, 1, 1)) ||
                     lower(SUBSTR(vp.APELLIDO, 2)))
                from rcivil.vt_pk_persona vp
               where vp.id_sexo = i.id_sexo
                 and vp.nro_documento = i.nro_documento
                 and vp.pai_cod_pais = i.pai_cod_pais
                 and vp.id_numero = i.id_numero) is not null
            
         and (select initcap(vp.nombre)
                from rcivil.vt_pk_persona vp
               where vp.id_sexo = i.id_sexo
                 and vp.nro_documento = i.nro_documento
                 and vp.pai_cod_pais = i.pai_cod_pais
                 and vp.id_numero = i.id_numero) is not null
            
         and decode(ad.persona_expuesta, 'S', 'true', 'false') is not null
            -- FIN sector valores null
            
         and subtip.id_subtipo_tramite in
             ('39451',
              '39452',
              '39453',
              '39480',
              '39547',
              '39549',
              '39586',
              '39587',
              '189',
              '1396',
              '1397',
              '1398',
              '1399',
              '1486',
              '48344',
              '39582',
              '51060')
         and tr.id_estado_ult between 100 and 199
            
         and E.BORRADOR = 'N'
            
         and ti.id_tipo_integrante IN ('5',
                                       '3',
                                       '24',
                                       '2',
                                       '9',
                                       '11',
                                       '38',
                                       '51',
                                       '52',
                                       '28',
                                       '30',
                                       '34',
                                       '1',
                                       '14',
                                       '42',
                                       '49',
                                       '39',
                                       '20',
                                       '23',
                                       '10')
            
         and te.id_tipo_entidad NOT IN ('5',
                                        '9',
                                        '11',
                                        '12',
                                        '16',
                                        '20',
                                        '21',
                                        '22',
                                        '31',
                                        '32',
                                        '33',
                                        '34',
                                        '35',
                                        '36',
                                        '37',
                                        '38',
                                        '39',
                                        '40',
                                        '41',
                                        '42',
                                        '43',
                                        '44')
            
         and (ad.fecha_fin >= to_date(sysdate, 'dd/mm/rrrr') or
             ad.fecha_fin =
             (select max(adm.fecha_fin)
                 from ipj.t_tramitesipj tr
                 join ipj.t_entidades_admin adm
                   on tr.id_tramite_ipj = adm.id_tramite_ipj
                where adm.id_legajo = l.id_legajo
                  and tr.id_estado_ult between 100 and 199))
            
         and nvl(e.fec_inscripcion, e.fec_resolucion) >= V_FECHA_DESDE
         and nvl(e.fec_inscripcion, e.fec_resolucion) <= V_FECHA_HASTA
      --order by te.tipo_entidad
      ;
  
  EXCEPTION
    WHEN V_ERROR_00001 THEN
      V_MSG := 'Mal.';
  END BUSCAR_CONSTITUCION_SOCIEDADES;

  PROCEDURE SOCIO_PERSONA_FISICA(p_T_CONSULTA  OUT TY_CURSOR,
                                 P_FECHA_DESDE IN IPJ.t_Entidades.FEC_INSCRIPCION%TYPE DEFAULT NULL,
                                 P_FECHA_HASTA IN IPJ.t_Entidades.FEC_INSCRIPCION%TYPE DEFAULT NULL,
                                 P_CUIL        IN IPJ.t_Entidades.CUIT%TYPE DEFAULT NULL) IS
    V_MSG         VARCHAR2(300);
    V_FECHA_DESDE DATE;
    V_FECHA_HASTA DATE;
    V_SENTENCIA   VARCHAR2(50);
    v_cuil        number;
  
  BEGIN
  
    V_FECHA_DESDE := P_FECHA_DESDE;
    V_FECHA_HASTA := P_FECHA_HASTA;
    v_cuil        := P_CUIL;
  
    V_SENTENCIA := '  ALTER SESSION SET NLS_DATE_FORMAT=' || '''' ||
                   'DD/MM/YYYY' || ''''; -- modificado por E.J 2010-04-22 para que tenga en cuenta hh:mm_ss
    --dd/mm/yyyy hh24:mi:ss
  
    EXECUTE IMMEDIATE V_SENTENCIA;
  
    OPEN p_T_CONSULTA FOR
    
    ------CONSTITUCIONES TODAS DESDE OCTUBRE 2020 y socios BEA
    
      select
      --)Socio_Persona_F92sica 
       (select (upper(substr(vp.APELLIDO, 1, 1)) ||
               lower(SUBSTR(vp.APELLIDO, 2)))
          from rcivil.vt_pk_persona vp
         where vp.id_sexo = i.id_sexo
           and vp.nro_documento = i.nro_documento
           and vp.pai_cod_pais = i.pai_cod_pais
           and vp.id_numero = i.id_numero) "Apellido88PerFisica",
       
       '' as Segundo_Apellido88PerFisica,
       
       (select initcap(vp.nombre)
          from rcivil.vt_pk_persona vp
         where vp.id_sexo = i.id_sexo
           and vp.nro_documento = i.nro_documento
           and vp.pai_cod_pais = i.pai_cod_pais
           and vp.id_numero = i.id_numero) "Nombre88PerFisica",
       
       '' as Segundo_Nombre88PerFisica,
       
       (select vp.n_tipo_documento
          from rcivil.vt_pk_persona vp
         where vp.id_sexo = i.id_sexo
           and vp.nro_documento = i.nro_documento
           and vp.pai_cod_pais = i.pai_cod_pais
           and vp.id_numero = i.id_numero) "Tipo_Documento88PerFisica",
       
       i.nro_documento "N94mero_documento88PerFisica",
       
       (select vp.cuil
          from rcivil.vt_pk_persona vp
         where vp.id_sexo = i.id_sexo
           and vp.nro_documento = i.nro_documento
           and vp.pai_cod_pais = i.pai_cod_pais
           and vp.id_numero = i.id_numero) "Nro_de_CUIT_CUIL88PerFisica",
       
       'false' as Radicada_en_el_Exterior88PerFisica,
       'false' as Radicada_en_Paraiso_Fiscal88PerFisica,
       
       decode(ad.persona_expuesta, 'S', 'true', 'false') "PEP88PerFisica",
       
       (select (upper(substr(d.n_calle, 1, 1)) ||
               lower(SUBSTR(d.n_calle, 2)))
          from DOM_MANAGER.VT_DOMICILIOS_COND d
         where d.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo,
                                                           i.nro_documento,
                                                           i.pai_cod_pais,
                                                           to_char(i.id_numero),
                                                           3)) "Calle88PerFisica",
       (select d.altura
          from DOM_MANAGER.VT_DOMICILIOS_COND d
         where d.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo,
                                                           i.nro_documento,
                                                           i.pai_cod_pais,
                                                           to_char(i.id_numero),
                                                           3)) "Nro88PerFisica",
       (select d.piso
          from DOM_MANAGER.VT_DOMICILIOS_COND d
         where d.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo,
                                                           i.nro_documento,
                                                           i.pai_cod_pais,
                                                           to_char(i.id_numero),
                                                           3)) "Piso88PerFisica",
       (select d.depto
          from DOM_MANAGER.VT_DOMICILIOS_COND d
         where d.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo,
                                                           i.nro_documento,
                                                           i.pai_cod_pais,
                                                           to_char(i.id_numero),
                                                           3)) "Departamento88PerFisica",
       (select (upper(substr(d.n_localidad, 1, 1)) ||
               lower(SUBSTR(d.n_localidad, 2)))
          from DOM_MANAGER.VT_DOMICILIOS_COND d
         where d.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo,
                                                           i.nro_documento,
                                                           i.pai_cod_pais,
                                                           to_char(i.id_numero),
                                                           3)) "Localidad88PerFisica",
       (select d.cpa
          from DOM_MANAGER.VT_DOMICILIOS_COND d
         where d.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo,
                                                           i.nro_documento,
                                                           i.pai_cod_pais,
                                                           to_char(i.id_numero),
                                                           3)) "C93digo_Postal88PerFisica",
       (select (upper(substr(d.n_provincia, 1, 1)) ||
               lower(SUBSTR(d.n_provincia, 2)))
          from dom_manager.vt_domicilios_cond d
         where d.id_vin =
               IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(l.id_legajo, 'S')) "Provincia88PerFisica",
       
       (select INITCAP(pa.n_pais)
          from dom_manager.vt_paises pa
         where pa.ID_PAIS = i.pai_cod_pais) "Pa92s88PerFisica",
       
       /*IPJ.VARIOS.FC_Buscar_Te_Caract(i.id_sexo || i.nro_documento ||
       i.pai_cod_pais ||
       to_char(i.id_numero),
       126)*/
       
       '' "Prefijo88PerFisica",
       
       /*IPJ.VARIOS.FC_Buscar_Telefono(i.id_sexo || i.nro_documento ||
       i.pai_cod_pais || to_char(i.id_numero),
       126) */
       
       '' "Tel91fono88PerFisica",
       
       IPJ.VARIOS.FC_BUSCAR_MAIL(i.id_sexo || i.nro_documento ||
                                 i.pai_cod_pais || to_char(i.id_numero),
                                 126) "Email88PerFisica",
       
       (select decode(vp.FECHA_NACIMIENTO,
                      null,
                      '',
                      to_char(vp.FECHA_NACIMIENTO, 'yyyy-MM-dd') ||
                      'T00:00:00-03:00')
          from rcivil.vt_pk_persona vp
         where vp.id_sexo = i.id_sexo
           and vp.nro_documento = i.nro_documento
           and vp.pai_cod_pais = i.pai_cod_pais
           and vp.id_numero = i.id_numero) "Fecha_de_Nacimiento88PerFisica",
       
       CASE UPPER(ti.n_tipo_integrante)
       
         WHEN UPPER('Socio/a') THEN
          'Socio' --
       
         WHEN UPPER('Gerente/a') THEN
          'Gerente' --
       
         WHEN UPPER('S�ndico/a Titular') THEN
          'S�ndico' --
       
         WHEN UPPER('Tesorero/a') THEN
          'Tesorero' --
       
         WHEN UPPER('Presidente/a') THEN
          'Presidente' --
       
         WHEN UPPER('Director/a') THEN
          'Director' -- 
       
         WHEN UPPER('Director/a Titular') THEN
          'Director' --
       
         WHEN UPPER('Vice-Presidente/a') THEN
          'Vicepresidente' --
       
         WHEN UPPER('Vicepresidente/a') THEN
          'Vicepresidente' --  
       
         WHEN UPPER('Titular') THEN
          'Titular' --       
       
         WHEN UPPER('Representante') THEN
          'Representante Legal' --     
       
         ELSE
          'Otros'
       END AS "Caracter_Invocado88PerFisica"
      
        from ipj.t_tipos_entidades         te,
             ipj.t_tramitesipj             tr,
             nuevosuac.vt_tramites_id      suactr,
             nuevosuac.vt_subtipos_tramite subtip,
             ipj.t_legajos                 l
        join ipj.t_entidades e
          on l.id_legajo = e.id_legajo
      -- join ipj.t_entidades_admin ad
      
        join (select distinct i.id_integrante,
                              i.id_legajo,
                              i.id_tramite_ipj,
                              i.id_tipo_integrante,
                              i.fecha_fin,
                              i.persona_expuesta
              
                from ipj.t_entidades_admin i
                join ipj.t_tramitesipj tr
                  on i.id_tramite_ipj = tr.id_tramite_ipj
                join ipj.t_legajos l
                  on l.id_legajo = i.id_legajo
               where nvl(i.id_integrante, 0) > 0
                 and -- Personas F�sicas
                     nvl(i.fecha_fin, sysdate) >= trunc(sysdate)
                 and -- Vigentes
                     nvl(l.eliminado, 0) <> 1
                 and -- Entidades no eliminadas
                     l.fecha_baja_entidad is null
                 and -- Entidades no dadas de baja
                     tr.id_estado_ult between 100 and 199 -- En tr�mites inscriptos
              UNION
              select distinct i.id_integrante,
                              i.id_legajo,
                              i.id_tramite_ipj,
                              i.id_tipo_integrante,
                              i.fecha_baja,
                              i.persona_expuesta
                from ipj.t_entidades_sindico i
                join ipj.t_tramitesipj tr
                  on i.id_tramite_ipj = tr.id_tramite_ipj
                join ipj.t_legajos l
                  on l.id_legajo = i.id_legajo
               where nvl(i.id_integrante, 0) > 0
                 and -- Personas F�sicas
                     nvl(i.fecha_baja, sysdate) >= trunc(sysdate)
                 and -- Vigentes
                     nvl(l.eliminado, 0) <> 1
                 and -- Entidades no eliminadas
                     l.fecha_baja_entidad is null
                 and -- Entidades no dadas de baja
                     tr.id_estado_ult between 100 and 199 -- En tr�mites inscriptos
              UNION
              select distinct i.id_integrante,
                              i.id_legajo,
                              i.id_tramite_ipj,
                              51,
                              i.fecha_baja,
                              i.persona_expuesta
                from ipj.t_entidades_representante i
                join ipj.t_tramitesipj tr
                  on i.id_tramite_ipj = tr.id_tramite_ipj
                join ipj.t_legajos l
                  on l.id_legajo = i.id_legajo
               where nvl(i.id_integrante, 0) > 0
                 and -- Personas F�sicas
                     nvl(i.fecha_baja, sysdate) >= trunc(sysdate)
                 and -- Vigentes
                     nvl(l.eliminado, 0) <> 1
                 and -- Entidades no eliminadas
                     l.fecha_baja_entidad is null
                 and -- Entidades no dadas de baja
                     tr.id_estado_ult between 100 and 199 -- En tr�mites inscriptos
              UNION
              select distinct i.id_integrante,
                              i.id_legajo,
                              i.id_tramite_ipj,
                              46,
                              i.fecha_fin,
                              i.persona_expuesta
                from ipj.t_entidades_benef i
                join ipj.t_tramitesipj tr
                  on i.id_tramite_ipj = tr.id_tramite_ipj
                join ipj.t_legajos l
                  on l.id_legajo = i.id_legajo
               where nvl(i.id_integrante, 0) > 0
                 and -- Personas F�sicas
                     nvl(i.fecha_fin, sysdate) >= trunc(sysdate)
                 and -- Vigentes
                     nvl(l.eliminado, 0) <> 1
                 and -- Entidades no eliminadas
                     l.fecha_baja_entidad is null
                 and -- Entidades no dadas de baja
                     tr.id_estado_ult between 100 and 199 -- En tr�mites inscriptos
              UNION
              select distinct i.id_integrante,
                              i.id_legajo,
                              i.id_tramite_ipj,
                              46,
                              i.fecha_fin,
                              i.persona_expuesta
                from ipj.t_entidades_fideicom i
                join ipj.t_tramitesipj tr
                  on i.id_tramite_ipj = tr.id_tramite_ipj
                join ipj.t_legajos l
                  on l.id_legajo = i.id_legajo
               where nvl(i.id_integrante, 0) > 0
                 and -- Personas F�sicas
                     nvl(i.fecha_fin, sysdate) >= trunc(sysdate)
                 and -- Vigentes
                     nvl(l.eliminado, 0) <> 1
                 and -- Entidades no eliminadas
                     l.fecha_baja_entidad is null
                 and -- Entidades no dadas de baja
                     tr.id_estado_ult between 100 and 199 -- En tr�mites inscriptos
              UNION
              select distinct i.id_integrante,
                              i.id_legajo,
                              i.id_tramite_ipj,
                              46,
                              i.fecha_fin,
                              i.persona_expuesta
                from ipj.t_entidades_fiduciantes i
                join ipj.t_tramitesipj tr
                  on i.id_tramite_ipj = tr.id_tramite_ipj
                join ipj.t_legajos l
                  on l.id_legajo = i.id_legajo
               where nvl(i.id_integrante, 0) > 0
                 and -- Personas F�sicas
                     nvl(i.fecha_fin, sysdate) >= trunc(sysdate)
                 and -- Vigentes
                     nvl(l.eliminado, 0) <> 1
                 and -- Entidades no eliminadas
                     l.fecha_baja_entidad is null
                 and -- Entidades no dadas de baja
                     tr.id_estado_ult between 100 and 199 -- En tr�mites inscriptos
              UNION
              select distinct i.id_integrante,
                              i.id_legajo,
                              i.id_tramite_ipj,
                              46,
                              i.fecha_fin,
                              i.persona_expuesta
                from ipj.t_entidades_fiduciarios i
                join ipj.t_tramitesipj tr
                  on i.id_tramite_ipj = tr.id_tramite_ipj
                join ipj.t_legajos l
                  on l.id_legajo = i.id_legajo
               where nvl(i.id_integrante, 0) > 0
                 and -- Personas F�sicas
                     nvl(i.fecha_fin, sysdate) >= trunc(sysdate)
                 and -- Vigentes
                     nvl(l.eliminado, 0) <> 1
                 and -- Entidades no eliminadas
                     l.fecha_baja_entidad is null
                 and -- Entidades no dadas de baja
                     tr.id_estado_ult between 100 and 199 -- En tr�mites inscriptos
              UNION
              select distinct i.id_integrante,
                              i.id_legajo,
                              i.id_tramite_ipj,
                              i.id_tipo_integrante,
                              i.fecha_fin,
                              i.persona_expuesta
                from ipj.t_entidades_socios i
                join ipj.t_tramitesipj tr
                  on i.id_tramite_ipj = tr.id_tramite_ipj
                join ipj.t_legajos l
                  on l.id_legajo = i.id_legajo
               where nvl(i.id_integrante, 0) > 0
                 and -- Personas F�sicas
                     nvl(i.fecha_fin, sysdate) >= trunc(sysdate)
                 and -- Vigentes
                     nvl(l.eliminado, 0) <> 1
                 and -- Entidades no eliminadas
                     l.fecha_baja_entidad is null
                 and -- Entidades no dadas de baja
                     tr.id_estado_ult between 100 and 199 -- En tr�mites inscriptos
              UNION
              select distinct s.id_integrante,
                              s.id_legajo,
                              s.id_tramite_ipj,
                              s.id_tipo_integrante,
                              s.fecha_fin,
                              s.persona_expuesta
                from ipj.t_entidades_socios_copro i
                join ipj.t_entidades_socios s
                  on i.id_entidad_socio = s.id_entidad_socio
                join ipj.t_tramitesipj tr
                  on s.id_tramite_ipj = tr.id_tramite_ipj
                join ipj.t_legajos l
                  on l.id_legajo = s.id_legajo
               where nvl(i.id_integrante, 0) > 0
                 and -- Personas F�sicas
                     nvl(i.fec_baja, sysdate) >= trunc(sysdate)
                 and -- Vigentes
                     nvl(l.eliminado, 0) <> 1
                 and -- Entidades no eliminadas
                     l.fecha_baja_entidad is null
                 and -- Entidades no dadas de baja
                     tr.id_estado_ult between 100 and 199 -- En tr�mites inscriptos
              UNION
              select distinct s.id_integrante,
                              s.id_legajo,
                              s.id_tramite_ipj,
                              s.id_tipo_integrante,
                              s.fecha_fin,
                              s.persona_expuesta
                from ipj.t_entidades_socios_usuf i
                join ipj.t_entidades_socios s
                  on i.id_entidad_socio = s.id_entidad_socio
                join ipj.t_tramitesipj tr
                  on s.id_tramite_ipj = tr.id_tramite_ipj
                join ipj.t_legajos l
                  on l.id_legajo = s.id_legajo
               where nvl(i.id_integrante, 0) > 0
                 and -- Personas F�sicas
                     nvl(i.fec_baja, sysdate) >= trunc(sysdate)
                 and -- Vigentes
                     nvl(l.eliminado, 0) <> 1
                 and -- Entidades no eliminadas
                     l.fecha_baja_entidad is null
                 and -- Entidades no dadas de baja
                     tr.id_estado_ult between 100 and 199 -- En tr�mites inscriptos
              ) ad
      
          on e.id_tramite_ipj = ad.id_tramite_ipj
         and e.id_legajo = ad.id_legajo
        join ipj.t_integrantes i
          on i.id_integrante = ad.id_integrante
        join ipj.t_tipos_integrante ti
          on ti.id_tipo_integrante = ad.id_tipo_integrante
      /*  join ipj.t_tipos_organismo o
      on o.id_tipo_organismo = ad.id_tipo_organismo*/
      
       where te.id_tipo_entidad = e.id_tipo_entidad
         and tr.id_tramite_ipj = e.id_tramite_ipj
         and suactr.id_tramite = tr.id_tramite
         and subtip.id_subtipo_tramite = suactr.id_subtipo_tramite
            
            --INICIO sector valores null
         and e.denominacion_1 is not null
         and te.tipo_entidad is not null
         and length(e.acta_contitutiva) = 10 --el requerimiento dice que no puede estar vacio, y el formato es de 10 digitos (10/01/2012)
            -- por lo tanto debe cumplir eso para su correcta transformacion.
            
         and e.monto is not null
         and (select d.n_calle
                from dom_manager.vt_domicilios_cond d
               where d.id_vin =
                     IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(l.id_legajo, 'S')) is not null
         and (select d.altura
                from dom_manager.vt_domicilios_cond d
               where d.id_vin =
                     IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(l.id_legajo, 'S')) is not null
            
         and (select (upper(substr(d.N_Localidad, 1, 1)) ||
                     lower(SUBSTR(d.N_Localidad, 2)))
                from dom_manager.vt_domicilios_cond d
               where d.id_vin =
                     IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(l.id_legajo, 'S')) is not null
            
         and (select (upper(substr(d.n_provincia, 1, 1)) ||
                     lower(SUBSTR(d.n_provincia, 2)))
                from dom_manager.vt_domicilios_cond d
               where d.id_vin =
                     IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(l.id_legajo, 'S')) is not null
         and (select (upper(substr(pa.n_pais, 1, 1)) ||
                     lower(SUBSTR(pa.n_pais, 2)))
                from dom_manager.vt_paises pa
               where pa.ID_PAIS = 'ARG') is not null
            
         and (select (upper(substr(vp.APELLIDO, 1, 1)) ||
                     lower(SUBSTR(vp.APELLIDO, 2)))
                from rcivil.vt_pk_persona vp
               where vp.id_sexo = i.id_sexo
                 and vp.nro_documento = i.nro_documento
                 and vp.pai_cod_pais = i.pai_cod_pais
                 and vp.id_numero = i.id_numero) is not null
            
         and (select initcap(vp.nombre)
                from rcivil.vt_pk_persona vp
               where vp.id_sexo = i.id_sexo
                 and vp.nro_documento = i.nro_documento
                 and vp.pai_cod_pais = i.pai_cod_pais
                 and vp.id_numero = i.id_numero) is not null
            
         and decode(ad.persona_expuesta, 'S', 'true', 'false') is not null
            -- FIN sector valores null
            
         and subtip.id_subtipo_tramite in
             ('39451',
              '39452',
              '39453',
              '39480',
              '39547',
              '39549',
              '39586',
              '39587',
              '189',
              '1396',
              '1397',
              '1398',
              '1399',
              '1486',
              '48344',
              '39582',
              '51060')
         and tr.id_estado_ult between 100 and 199
            
         and E.BORRADOR = 'N'
            
         and ti.id_tipo_integrante IN ('5',
                                       '3',
                                       '24',
                                       '2',
                                       '9',
                                       '11',
                                       '38',
                                       '51',
                                       '52',
                                       '28',
                                       '30',
                                       '34',
                                       '1',
                                       '14',
                                       '42',
                                       '49',
                                       '39',
                                       '20',
                                       '23',
                                       '10')
            
         and te.id_tipo_entidad NOT IN ('5',
                                        '9',
                                        '11',
                                        '12',
                                        '16',
                                        '20',
                                        '21',
                                        '22',
                                        '31',
                                        '32',
                                        '33',
                                        '34',
                                        '35',
                                        '36',
                                        '37',
                                        '38',
                                        '39',
                                        '40',
                                        '41',
                                        '42',
                                        '43',
                                        '44')
            
         and (ad.fecha_fin >= to_date(sysdate, 'dd/mm/rrrr') or
             ad.fecha_fin =
             (select max(adm.fecha_fin)
                 from ipj.t_tramitesipj tr
                 join ipj.t_entidades_admin adm
                   on tr.id_tramite_ipj = adm.id_tramite_ipj
                where adm.id_legajo = l.id_legajo
                  and tr.id_estado_ult between 100 and 199))
            
         and CASE UPPER(ti.n_tipo_integrante)
             
               WHEN UPPER('Socio/a') THEN
                'Socio' --
             
               WHEN UPPER('Gerente/a') THEN
                'Gerente' --
             
               WHEN UPPER('S�ndico/a Titular') THEN
                'S�ndico' --
             
               WHEN UPPER('Tesorero/a') THEN
                'Tesorero' --
             
               WHEN UPPER('Presidente/a') THEN
                'Presidente' --
             
               WHEN UPPER('Director/a') THEN
                'Director' -- 
             
               WHEN UPPER('Director/a Titular') THEN
                'Director' --
             
               WHEN UPPER('Vice-Presidente/a') THEN
                'Vicepresidente' --
             
               WHEN UPPER('Vicepresidente/a') THEN
                'Vicepresidente' --  
             
               WHEN UPPER('Titular') THEN
                'Titular' --       
             
               WHEN UPPER('Representante') THEN
                'Representante Legal' --     
             
               ELSE
                'Otros'
             END <> 'Otros'
            
         and nvl(e.fec_inscripcion, e.fec_resolucion) >= V_FECHA_DESDE
         and nvl(e.fec_inscripcion, e.fec_resolucion) <= V_FECHA_HASTA
            
         and e.cuit = v_cuil
      
       order by te.tipo_entidad;
  
  EXCEPTION
    WHEN V_ERROR_00001 THEN
      V_MSG := 'Mal.';
    
  END SOCIO_PERSONA_FISICA;

  PROCEDURE SOCIO_PERSONA_JURIDICA(p_T_CONSULTA  OUT TY_CURSOR,
                                   P_FECHA_DESDE IN IPJ.t_Entidades.FEC_INSCRIPCION%TYPE DEFAULT NULL,
                                   P_FECHA_HASTA IN IPJ.t_Entidades.FEC_INSCRIPCION%TYPE DEFAULT NULL,
                                   P_CUIL        IN IPJ.t_Entidades.CUIT%TYPE DEFAULT NULL) IS
    V_MSG         VARCHAR2(300);
    V_FECHA_DESDE DATE;
    V_FECHA_HASTA DATE;
    V_SENTENCIA   VARCHAR2(50);
    v_cuil        number;
  
  BEGIN
  
    V_FECHA_DESDE := P_FECHA_DESDE;
    V_FECHA_HASTA := P_FECHA_HASTA;
    v_cuil        := P_CUIL;
  
    V_SENTENCIA := '  ALTER SESSION SET NLS_DATE_FORMAT=' || '''' ||
                   'DD/MM/YYYY' || ''''; -- modificado por E.J 2010-04-22 para que tenga en cuenta hh:mm_ss
    --dd/mm/yyyy hh24:mi:ss
  
    EXECUTE IMMEDIATE V_SENTENCIA;
  
    OPEN p_T_CONSULTA FOR
      select ---Persona Jur�dica
      
       (select PJU.RAZON_SOCIAL
          from t_comunes.vt_pers_juridicas_unica PJU
         where PJU.CUIT = jur.cuit) "Denominaci93n88PerJuridica",
       
       CASE
        translate(UPPER(te_jur.tipo_entidad), '����������', 'aeiouAEIOU')
       
         WHEN UPPER('Sociedad de bolsa') THEN
          'Sociedad de bolsa' --
       
         WHEN UPPER('Sociedad colectiva') THEN
          'Sociedad colectiva' --
       
         WHEN UPPER('Sociedades en comandita simple') THEN
          'Sociedad en comandita simple' --
       
         WHEN UPPER('Sociedad de capital e industria') THEN
          'Sociedad de capital e industria' --
       
         WHEN UPPER('Sociedad de responsabilidad limitada') THEN
          'Sociedad de responsabilidad limitada' --
       
         WHEN UPPER('Sociedad anonima') THEN
          'Sociedad an�nima' --
       
         WHEN UPPER('Sociedades del estado') THEN
          'Sociedad del estado' --
       
         WHEN UPPER('Sociedad en comandita por acciones') THEN
          'Sociedad en comandita por acciones' --
       
         WHEN UPPER('Sociedad de hecho') THEN
          'Sociedad de hecho' --
       
         WHEN UPPER('Sociedad constituida en el extranjero') THEN
          'Sociedad constituida en el extranjero' --
       
         WHEN UPPER('Sociedad binacional fuera de jurisdiccion') THEN
          'Sociedad binacional fuera de jurisdicci�n' --
       
         WHEN UPPER('Asociacion civil') THEN
          'Asociaci�n civil' --
       
         WHEN UPPER('Entidad extranjera sin fines de lucro') THEN
          'Entidad extranjera sin fines de lucro' --
       
         WHEN UPPER('Fundacion') THEN
          'Fundacion' --
       
         WHEN UPPER('Simples asociaciones') THEN
          'Simples asociaciones' --        
       
         WHEN UPPER('Federacion') THEN
          'Federaci�n' --      
       
         WHEN UPPER('Confederacion') THEN
          'Confederaci�n' --      
       
         WHEN UPPER('C�mara') THEN
          'C�mara' --      
       
         WHEN UPPER('Contrato de colaboracion empresaria') THEN
          'Contrato de colaboraci�n empresaria' --    
       
         WHEN UPPER('Union transitoria') THEN
          'Uni�n transitoria de empresas' --    
       
         WHEN UPPER('Sociedad de garantia reciproca') THEN
          'Sociedad de garant�a rec�iproca' --    
       
         WHEN UPPER('Soc. capitalizacion y ahorro') THEN
          'Soc. capitalizacion y ahorro"' --    
       
         WHEN UPPER('Cooperativas') THEN
          'Cooperativa' --    
       
         WHEN UPPER('Mutuales') THEN
          'Mutual' --    
       
         WHEN UPPER('Fideicomisos') THEN
          'Fideicomiso' --
       
         ELSE
          'Otros'
       END as "Forma_Jur92dica88PerJuridica",
       --   (select FJ.N_FORMA_JURIDICA from t_comunes.vt_formas_juridica FJ where FJ.ID_FORMA_JURIDICA=PJu.ID_FORMA_JURIDICA)"Forma_Jur92dica88Per",
       (select PJ.CUIT
          from t_comunes.vt_pers_juridicas_unica pj
         where pj.CUIT = jur.CUIT) "Nro_CUIT88PerJuridica",
       
       (select (upper(substr(d.n_calle, 1, 1)) ||
               lower(SUBSTR(d.n_calle, 2)))
          from dom_manager.vt_domicilios_cond d
         where d.id_vin =
               IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(l.id_legajo, 'S')) as "Calle88PerJuridica",
       (select d.altura
          from dom_manager.vt_domicilios_cond d
         where d.id_vin =
               IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(l.id_legajo, 'S')) as "Nro88PerJuridica",
       (select d.piso
          from dom_manager.vt_domicilios_cond d
         where d.id_vin =
               IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(l.id_legajo, 'S')) as "Piso88PerJuridica",
       (select d.depto
          from dom_manager.vt_domicilios_cond d
         where d.id_vin =
               IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(l.id_legajo, 'S')) as "Departamento88PerJuridica",
       (select (upper(substr(d.n_localidad, 1, 1)) ||
               lower(SUBSTR(d.n_localidad, 2)))
          from dom_manager.vt_domicilios_cond d
         where d.id_vin =
               IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(l.id_legajo, 'S')) as "Localidad88PerJuridica",
       (select d.cpa
          from dom_manager.vt_domicilios_cond d
         where d.id_vin =
               IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(l.id_legajo, 'S')) as "C93digo_Postal88PerJuridica",
       (select (upper(substr(d.n_provincia, 1, 1)) ||
               lower(SUBSTR(d.n_provincia, 2)))
          from dom_manager.vt_domicilios_cond d
         where d.id_vin =
               IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(l.id_legajo, 'S')) as "Provincia88PerJuridica",
       (select pa.n_pais
          from dom_manager.vt_domicilios_cond d
          join DOM_MANAGER.VT_PROVINCIAS pr
            on pr.id_provincia = d.id_provincia
          join DOM_MANAGER.VT_PAISES pa
            on pr.id_pais = pa.id_pais
         where d.id_vin =
               IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(l.id_legajo, 'S')) as "Pa92s88PerJuridica",
       IPJ.VARIOS.FC_Buscar_Mail(L.cuit || '00', 126) as "Email88PerJuridica",
       IPJ.VARIOS.FC_Buscar_Te_Caract(L.cuit || '00', 126) as "Prefijo88PerJuridica",
       IPJ.VARIOS.FC_Buscar_Telefono(L.cuit || '00', 126) as "Tel91fono88PerJuridica",
       -- '' as "Actividad_que_Desarrolla88PerJuridica",
       
       (select max(apju.n_actividad_uif)
          from t_actividad_persona_juri_uif    apju, /* t_comunes.t_actividades a,*/
               t_comunes.vt_actividades_perjur ap
         where apju.id_actividad_uif = ap.id_actividad
              /*a.id_actividad=ap.id_actividad and*/
           and ap.cuit = e.cuit) "Actividad_que_Desarrolla88PerJuridica"
      
        from ipj.t_tipos_entidades         te,
             ipj.t_tramitesipj             tr,
             nuevosuac.vt_tramites_id      suactr,
             nuevosuac.vt_subtipos_tramite subtip,
             ipj.t_legajos                 l
        join ipj.t_entidades e
          on l.id_legajo = e.id_legajo
        join ipj.t_entidades_SOCIOS ad
          on e.id_tramite_ipj = ad.id_tramite_ipj
         and e.id_legajo = ad.id_legajo
        join ipj.t_Entidades JUR
          on JUR.ID_LEGAJO = AD.ID_LEGAJO_SOCIO
        join ipj.t_tipos_entidades te_jur
          on te_jur.id_tipo_entidad = jur.id_tipo_entidad
      
       where te.id_tipo_entidad = e.id_tipo_entidad
         and tr.id_tramite_ipj = e.id_tramite_ipj
         and suactr.id_tramite = tr.id_tramite
         and subtip.id_subtipo_tramite = suactr.id_subtipo_tramite
            
            --INICIO sector valores null
         and e.denominacion_1 is not null
         and te.tipo_entidad is not null
         and length(e.acta_contitutiva) = 10 --el requerimiento dice que no puede estar vacio, y el formato es de 10 digitos (10/01/2012)
            -- por lo tanto debe cumplir eso para su correcta transformacion.
            
         and decode(ad.persona_expuesta, 'S', 'true', 'false') is not null
            -- FIN sector valores null
            
         and subtip.id_subtipo_tramite in
             ('39451',
              '39452',
              '39453',
              '39480',
              '39547',
              '39549',
              '39586',
              '39587',
              '189',
              '1396',
              '1397',
              '1398',
              '1399',
              '1486',
              '48344',
              '39582',
              '51060')
            
         and tr.id_estado_ult between 100 and 199
            
         and E.BORRADOR = 'N'
            
         and te.id_tipo_entidad NOT IN ('5',
                                        '9',
                                        '11',
                                        '12',
                                        '16',
                                        '20',
                                        '21',
                                        '22',
                                        '31',
                                        '32',
                                        '33',
                                        '34',
                                        '35',
                                        '36',
                                        '37',
                                        '38',
                                        '39',
                                        '40',
                                        '41',
                                        '42',
                                        '43',
                                        '44')
            
         and nvl(e.fec_inscripcion, e.fec_resolucion) >= V_FECHA_DESDE
         and nvl(e.fec_inscripcion, e.fec_resolucion) <= V_FECHA_HASTA
            
         and e.cuit = v_cuil
      
       order by te.tipo_entidad;
  
  EXCEPTION
    WHEN V_ERROR_00001 THEN
      V_MSG := 'Mal.';
    
  END SOCIO_PERSONA_JURIDICA;

  PROCEDURE ENTIDADES_SIN_FINES_DE_LUCRO(p_T_CONSULTA  OUT TY_CURSOR,
                                         P_FECHA_DESDE IN IPJ.t_Entidades.FEC_INSCRIPCION%TYPE DEFAULT NULL,
                                         P_FECHA_HASTA IN IPJ.t_Entidades.FEC_INSCRIPCION%TYPE DEFAULT NULL) IS
    V_MSG         VARCHAR2(300);
    V_FECHA_DESDE DATE;
    V_FECHA_HASTA DATE;
    V_SENTENCIA   VARCHAR2(50);
    v_cuil        number;
  
  BEGIN
  
    V_FECHA_DESDE := P_FECHA_DESDE;
    V_FECHA_HASTA := P_FECHA_HASTA;
  
    V_SENTENCIA := '  ALTER SESSION SET NLS_DATE_FORMAT=' || '''' ||
                   'DD/MM/YYYY' || ''''; -- modificado por E.J 2010-04-22 para que tenga en cuenta hh:mm_ss
    --dd/mm/yyyy hh24:mi:ss
  
    EXECUTE IMMEDIATE V_SENTENCIA;
  
    OPEN p_T_CONSULTA FOR
    
      select distinct (upper(substr(e.denominacion_1, 1, 1)) ||
                      lower(SUBSTR(e.denominacion_1, 2))) AS "Denominaci93n",
                      
                      e.cuit "Nro_de_CUIT",
                      
                      to_char(to_date(e.acta_contitutiva, 'dd/mm/yyyy'),
                              'yyyy-MM-dd') || 'T00:00:00-03:00' /*E.ACTA_CONTITUTIVA*/ "Fecha_de_Constituci93n",
                      
                      (select (upper(substr(d.n_calle, 1, 1)) ||
                              lower(SUBSTR(d.n_calle, 2)))
                         from dom_manager.vt_domicilios_cond d
                        where d.id_vin =
                              IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(l.id_legajo,
                                                                    'S')) "Calle",
                      
                      (select d.altura
                         from dom_manager.vt_domicilios_cond d
                        where d.id_vin =
                              IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(l.id_legajo,
                                                                    'S')) "Nro",
                      
                      (select d.piso
                         from dom_manager.vt_domicilios_cond d
                        where d.id_vin =
                              IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(l.id_legajo,
                                                                    'S')) "Piso",
                      
                      (select d.depto
                         from dom_manager.vt_domicilios_cond d
                        where d.id_vin =
                              IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(l.id_legajo,
                                                                    'S')) "Departamento",
                      
                      (select (upper(substr(d.N_Localidad, 1, 1)) ||
                              lower(SUBSTR(d.N_Localidad, 2)))
                         from dom_manager.vt_domicilios_cond d
                        where d.id_vin =
                              IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(l.id_legajo,
                                                                    'S')) "Localidad",
                      
                      (select (upper(substr(d.n_provincia, 1, 1)) ||
                              lower(SUBSTR(d.n_provincia, 2)))
                         from dom_manager.vt_domicilios_cond d
                        where d.id_vin =
                              IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(l.id_legajo,
                                                                    'S')) "Provincia",
                      
                      (select initcap(upper(substr(pa.n_pais, 1, 1)) ||
                                      lower(SUBSTR(pa.n_pais, 2)))
                         from dom_manager.vt_paises pa
                        where pa.ID_PAIS = 'ARG') "Pa92s",
                      
                      (select d.cpa
                         from dom_manager.vt_domicilios_cond d
                        where d.id_vin =
                              IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(l.id_legajo,
                                                                    'S')) "C93digo_Postal",
                      
                      '' as Prefijo_Tel,
                      '' as Nro_Tel,
                      '' as Email,
                      
                      --)REPRESENTANTE
                      
                      (select (upper(substr(vp.APELLIDO, 1, 1)) ||
                              lower(SUBSTR(vp.APELLIDO, 2)))
                         from rcivil.vt_pk_persona vp
                        where vp.id_sexo = i.id_sexo
                          and vp.nro_documento = i.nro_documento
                          and vp.pai_cod_pais = i.pai_cod_pais
                          and vp.id_numero = i.id_numero) "Apellido88Representante",
                      
                      '' as Segundo_Apellido88Representante,
                      
                      (select initcap(vp.nombre)
                         from rcivil.vt_pk_persona vp
                        where vp.id_sexo = i.id_sexo
                          and vp.nro_documento = i.nro_documento
                          and vp.pai_cod_pais = i.pai_cod_pais
                          and vp.id_numero = i.id_numero) "Nombre88Representante",
                      
                      '' as Segundo_Nombre88Representante,
                      
                      (select vp.n_tipo_documento
                         from rcivil.vt_pk_persona vp
                        where vp.id_sexo = i.id_sexo
                          and vp.nro_documento = i.nro_documento
                          and vp.pai_cod_pais = i.pai_cod_pais
                          and vp.id_numero = i.id_numero) "Tipo_Documento88Representante",
                      
                      i.nro_documento "N94mero_Documento",
                      
                      (select vp.cuil
                         from rcivil.vt_pk_persona vp
                        where vp.id_sexo = i.id_sexo
                          and vp.nro_documento = i.nro_documento
                          and vp.pai_cod_pais = i.pai_cod_pais
                          and vp.id_numero = i.id_numero) "Nro_de_CUIT_CUIL88Representante",
                      
                      'false' as Radicada_en_el_Exterior,
                      'false' as Radicada_en_Para92so_Fiscal,
                      
                      decode(ad.persona_expuesta, 'S', 'true', 'false') "PEP",
                      
                      (select (upper(substr(d.n_calle, 1, 1)) ||
                              lower(SUBSTR(d.n_calle, 2)))
                         from DOM_MANAGER.VT_DOMICILIOS_COND d
                        where d.id_vin =
                              IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo,
                                                               i.nro_documento,
                                                               i.pai_cod_pais,
                                                               to_char(i.id_numero),
                                                               3)) "Calle88Representante",
                      
                      (select d.altura
                         from DOM_MANAGER.VT_DOMICILIOS_COND d
                        where d.id_vin =
                              IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo,
                                                               i.nro_documento,
                                                               i.pai_cod_pais,
                                                               to_char(i.id_numero),
                                                               3)) "Nro88Representante",
                      
                      (select d.piso
                         from DOM_MANAGER.VT_DOMICILIOS_COND d
                        where d.id_vin =
                              IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo,
                                                               i.nro_documento,
                                                               i.pai_cod_pais,
                                                               to_char(i.id_numero),
                                                               3)) "Piso88Representante",
                      
                      (select d.depto
                         from DOM_MANAGER.VT_DOMICILIOS_COND d
                        where d.id_vin =
                              IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo,
                                                               i.nro_documento,
                                                               i.pai_cod_pais,
                                                               to_char(i.id_numero),
                                                               3)) "Departamento88Representante",
                      
                      (select (upper(substr(d.n_localidad, 1, 1)) ||
                              lower(SUBSTR(d.n_localidad, 2)))
                         from DOM_MANAGER.VT_DOMICILIOS_COND d
                        where d.id_vin =
                              IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo,
                                                               i.nro_documento,
                                                               i.pai_cod_pais,
                                                               to_char(i.id_numero),
                                                               3)) "Localidad88Representante",
                      
                      (select d.cpa
                         from DOM_MANAGER.VT_DOMICILIOS_COND d
                        where d.id_vin =
                              IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo,
                                                               i.nro_documento,
                                                               i.pai_cod_pais,
                                                               to_char(i.id_numero),
                                                               3)) "C93digo_Postal88Representante",
                      
                      (select (upper(substr(d.n_provincia, 1, 1)) ||
                              lower(SUBSTR(d.n_provincia, 2)))
                         from dom_manager.vt_domicilios_cond d
                        where d.id_vin =
                              IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(l.id_legajo,
                                                                    'S')) "Provincia88Representante",
                      
                      (select INITCAP(pa.n_pais)
                         from dom_manager.vt_paises pa
                        where pa.ID_PAIS = i.pai_cod_pais) "Pa92s88Representante",
                      
                      IPJ.VARIOS.FC_Buscar_Te_Caract(i.id_sexo ||
                                                     i.nro_documento ||
                                                     i.pai_cod_pais ||
                                                     to_char(i.id_numero),
                                                     126) "Prefijo_Tel88Representante",
                      
                      IPJ.VARIOS.FC_Buscar_Telefono(i.id_sexo ||
                                                    i.nro_documento ||
                                                    i.pai_cod_pais ||
                                                    to_char(i.id_numero),
                                                    126) "Nro_Tel88Representante",
                      
                      IPJ.VARIOS.FC_BUSCAR_MAIL(i.id_sexo ||
                                                i.nro_documento ||
                                                i.pai_cod_pais ||
                                                to_char(i.id_numero),
                                                126) "Email88Representante",
                      
                      CASE UPPER(ti.n_tipo_integrante)
                      
                        WHEN UPPER('Presidente/a') THEN
                         'Presidente' --
                      
                      /*WHEN UPPER('Representante') THEN
                         'Representante Legal' --     
                      */
                        ELSE
                         'otros'
                      END AS "Car90cter"
      
        from ipj.t_tipos_entidades         te,
             ipj.t_tramitesipj             tr,
             nuevosuac.vt_tramites_id      suactr,
             nuevosuac.vt_subtipos_tramite subtip,
             ipj.t_legajos                 l
        join ipj.t_entidades e
          on l.id_legajo = e.id_legajo
      -- join ipj.t_entidades_admin ad
      
        join (select distinct i.id_integrante,
                              i.id_legajo,
                              i.id_tramite_ipj,
                              i.id_tipo_integrante,
                              i.fecha_fin,
                              i.persona_expuesta
              
                from ipj.t_entidades_admin i
                join ipj.t_tramitesipj tr
                  on i.id_tramite_ipj = tr.id_tramite_ipj
                join ipj.t_legajos l
                  on l.id_legajo = i.id_legajo
               where nvl(i.id_integrante, 0) > 0
                 and -- Personas F�sicas
                     nvl(i.fecha_fin, sysdate) >= trunc(sysdate)
                 and -- Vigentes
                     nvl(l.eliminado, 0) <> 1
                 and -- Entidades no eliminadas
                     l.fecha_baja_entidad is null
                 and -- Entidades no dadas de baja
                     tr.id_estado_ult between 100 and 199 -- En tr�mites inscriptos   
              ) ad
      
          on e.id_tramite_ipj = ad.id_tramite_ipj
         and e.id_legajo = ad.id_legajo
        join ipj.t_integrantes i
          on i.id_integrante = ad.id_integrante
        join ipj.t_tipos_integrante ti
          on ti.id_tipo_integrante = ad.id_tipo_integrante
      /*  join ipj.t_tipos_organismo o
      on o.id_tipo_organismo = ad.id_tipo_organismo*/
      
       where te.id_tipo_entidad = e.id_tipo_entidad
         and tr.id_tramite_ipj = e.id_tramite_ipj
         and suactr.id_tramite = tr.id_tramite
         and subtip.id_subtipo_tramite = suactr.id_subtipo_tramite
         and subtip.id_subtipo_tramite in
             ('39451',
              '39452',
              '39453',
              '39480',
              '39547',
              '39549',
              '39586',
              '39587',
              '189',
              '1396',
              '1397',
              '1398',
              '1399',
              '1486',
              '48344',
              '39582',
              '51060')
            
         and te.id_ubicacion = 5 --CIVILES Y FUNDACIONES
            -- excluir las entidades que tienen persona juridica
         and not exists
       (select 1
                from ipj.t_entidades_socios es1
               where (es1.cuit_empresa is not null or
                     es1.n_empresa is not null)
                 and es1.borrador = 'N'
                 and e.id_legajo = es1.id_legajo
                 and e.id_tramite_ipj = es1.id_tramite_ipj
                 and es1.fecha_fin is null)
            
         and tr.id_estado_ult between 100 and 199
            
         and E.BORRADOR = 'N'
            
         and ti.id_tipo_integrante IN ('11', '38')
            
         and (ad.fecha_fin >= to_date(sysdate, 'dd/mm/rrrr') or
             ad.fecha_fin =
             (select max(adm.fecha_fin)
                 from ipj.t_tramitesipj tr
                 join ipj.t_entidades_admin adm
                   on tr.id_tramite_ipj = adm.id_tramite_ipj
                where adm.id_legajo = l.id_legajo
                  and tr.id_estado_ult between 100 and 199))
            
         AND ((nvl(e.fec_inscripcion, e.fec_resolucion) >= V_FECHA_DESDE) AND
             (nvl(e.fec_inscripcion, e.fec_resolucion) <= V_FECHA_HASTA));
  
  EXCEPTION
    WHEN V_ERROR_00001 THEN
      V_MSG := 'Mal.';
    
  END ENTIDADES_SIN_FINES_DE_LUCRO;

  FUNCTION OBTENER_LISTA_PALABRAS(p_frase_Clave    IN VARCHAR2,
                                  cantidad_palabra out number,
                                  LARGO_PALABRA    IN NUMBER)
    RETURN dbms_sql.varchar2_table AS
    ret              number;
    v_ret            number;
    str              varchar2(2000);
    str_aux          varchar2(200);
    i                number;
    indice           number;
    name_array_clave dbms_sql.varchar2_table;
  BEGIN
  
    /**** ARMO UN CONJUNTO CON LAS PALABRAS CLAVES ********/
    --reemplazo los 2 espacios por seguidos por 1
    str := REGEXP_REPLACE(trim(p_frase_Clave), '  *', ' ');
    --reemplazo los espacios por guiones para parsear las palabras
    str := replace(trim(str), ' ', '_');
  
    -- Separo la primer palabra y comienzo a armar la lista de palabas
    if instr(str, '_') > 0 then
      i := instr(str, '_');
    else
      i := length(str) + 1;
    end if;
    str_aux := substr(str, 1, i - 1);
    str     := substr(str, i + 1);
    indice  := 1;
    WHILE length(str_aux) > 0 LOOP
      if (str_aux <> 'AL' AND STR_AUX <> 'DEL' AND STR_AUX <> 'EL' AND
         STR_AUX <> 'LA' AND STR_AUX <> 'LO' AND STR_AUX <> 'LOS' AND
         STR_AUX <> 'LAS' AND STR_AUX <> 'UN' AND STR_AUX <> 'UNO' AND
         STR_AUX <> 'UNA' AND STR_AUX <> 'UNOS' AND STR_AUX <> 'UNAS' AND
         STR_AUX <> 'A' AND STR_AUX <> 'ANTE' AND STR_AUX <> 'BAJO' AND
         STR_AUX <> 'CABE' AND STR_AUX <> 'CON' AND STR_AUX <> 'CONTRA' AND
         STR_AUX <> 'DE' AND STR_AUX <> 'DESDE' AND STR_AUX <> 'EN' AND
         STR_AUX <> 'ENTRE' AND STR_AUX <> 'HACIA' AND STR_AUX <> 'HASTA' AND
         STR_AUX <> 'PARA' AND STR_AUX <> 'POR' AND STR_AUX <> 'SEGUN' AND
         STR_AUX <> 'SIN' AND STR_AUX <> 'SO' AND STR_AUX <> 'SOBRE' AND
         STR_AUX <> 'TRAS' AND STR_AUX <> 'DURANTE' AND
         STR_AUX <> 'MEDIANTE' AND STR_AUX <> 'VERSUS' AND
         STR_AUX <> 'VIA' AND STR_AUX <> 'CIA') then
        if length(str_aux) >= LARGO_PALABRA then
          --cargo la palabra
          name_array_clave(indice) := upper(str_aux);
          indice := indice + 1;
        end if;
      END IF;
      -- Separo la siguiente palabra o me quedo con la ultima
      if instr(str, '_') > 0 then
        i := instr(str, '_');
      else
        i := length(str) + 1;
      end if;
      str_aux := substr(str, 1, i - 1);
      str     := substr(str, i + 1);
    END LOOP;
  
    cantidad_palabra := indice;
  
    RETURN name_array_clave;
  END OBTENER_LISTA_PALABRAS;

  FUNCTION COMPARAR_PALABRAS(p_frase_Clave    IN VARCHAR2,
                             p_frase_comparar in VARCHAR2) RETURN number AS
    ret                       number;
    v_ret                     number;
    str                       varchar2(2000);
    str_aux                   varchar2(200);
    i                         number;
    j                         number;
    indice                    number;
    name_array_clave          dbms_sql.varchar2_table;
    name_array_comparar       dbms_sql.varchar2_table;
    resultado                 number;
    resultado_final           number;
    cantidad_palabra_clave    number;
    cantidad_palabra_comparar number;
  
  BEGIN
    resultado       := 0;
    resultado_final := 0;
  
    name_array_clave    := OBTENER_LISTA_PALABRAS(p_frase_Clave,
                                                  cantidad_palabra_clave,
                                                  3);
    name_array_comparar := OBTENER_LISTA_PALABRAS(p_frase_comparar,
                                                  cantidad_palabra_comparar,
                                                  3);
  
    /**** COMPARO LAS PALABRAS ********/
    ret := 0;
    FOR i IN 1 .. cantidad_palabra_clave - 1
    
     LOOP
    
      FOR j IN 1 .. cantidad_palabra_comparar - 1 LOOP
      
        -- resultado := utl_match.edit_distance_similarity (name_array_clave(i),name_array_comparar(j));
        resultado := utl_match.jaro_winkler_similarity(name_array_clave(i),
                                                       name_array_comparar(j));
      
        if (resultado >= resultado_final) then
          resultado_final := resultado;
        end if;
      
      END LOOP;
    
    END LOOP;
  
    RETURN resultado_final;
  END COMPARAR_PALABRAS;

  FUNCTION COMPARAR_PALABRAS_PROMEDIO(p_frase_Clave    IN VARCHAR2,
                                      p_frase_comparar in VARCHAR2)
    RETURN number AS
    ret                       number;
    v_ret                     number;
    str                       varchar2(2000);
    str_aux                   varchar2(200);
    i                         number;
    j                         number;
    indice                    number;
    name_array_clave          dbms_sql.varchar2_table;
    name_array_comparar       dbms_sql.varchar2_table;
    resultado                 number;
    promedio                  number;
    DIVISOR                   number;
    resultado_total           number;
    resultado_final           number;
    resultado_subtotal        number;
    cantidad_palabra_clave    number;
    cantidad_palabra_comparar number;
  
  BEGIN
    resultado           := 0;
    resultado_total     := 0;
    promedio            := 0;
    resultado_subtotal  := 0;
    resultado_final     := 0;
    name_array_clave    := OBTENER_LISTA_PALABRAS(p_frase_Clave,
                                                  cantidad_palabra_clave,
                                                  3);
    name_array_comparar := OBTENER_LISTA_PALABRAS(p_frase_comparar,
                                                  cantidad_palabra_comparar,
                                                  3);
  
    /**** COMPARO LAS PALABRAS ********/
    ret := 0;
  
    FOR i IN 1 .. cantidad_palabra_clave - 1
    
     LOOP
      IF (cantidad_palabra_clave - 1) >= 2 THEN
      
        FOR j IN 1 .. cantidad_palabra_comparar - 1 LOOP
        
          -- resultado := utl_match.edit_distance_similarity (name_array_clave(i),name_array_comparar(j));
          resultado := utl_match.jaro_winkler_similarity(name_array_clave(i),
                                                         name_array_comparar(j));
        
          --resultado_total :=  resultado + resultado_total;
        
          if (resultado >= resultado_subtotal) then
            resultado_subtotal := resultado;
          end if;
        
        END LOOP;
        resultado_total    := resultado_total + resultado_subtotal;
        resultado_subtotal := 0;
        DIVISOR            := i;
      
      ELSE
      
        FOR j IN 1 .. cantidad_palabra_comparar - 1 LOOP
        
          -- resultado := utl_match.edit_distance_similarity (name_array_clave(i),name_array_comparar(j));
          resultado := utl_match.jaro_winkler_similarity(name_array_clave(i),
                                                         name_array_comparar(j));
        
          resultado_total := resultado_total + resultado;
        
          DIVISOR := j;
        END LOOP;
      
        -- resultado_final :=resultado_total/DIVISOR;
      END IF;
    
    END LOOP;
  
    resultado_final := resultado_total / (DIVISOR);
  
    RETURN resultado_final;
  END COMPARAR_PALABRAS_PROMEDIO;

  FUNCTION COMPARAR_PALABRAS_PROMEDIO_1(p_frase_Clave    IN VARCHAR2,
                                        p_frase_comparar in VARCHAR2)
    RETURN number AS
    ret                       number;
    v_ret                     number;
    str                       varchar2(2000);
    str_aux                   varchar2(200);
    i                         number;
    j                         number;
    indice                    number;
    name_array_clave          dbms_sql.varchar2_table;
    name_array_comparar       dbms_sql.varchar2_table;
    resultado                 number;
    promedio                  number;
    DIVISOR                   number;
    resultado_total           number;
    resultado_final           number;
    resultado_subtotal        number;
    TOTAL_X_PALABRA           number;
    cantidad_palabra_clave    number;
    cantidad_palabra_comparar number;
    dividir                   number;
  
  BEGIN
    resultado           := 0;
    resultado_total     := 0;
    promedio            := 0;
    resultado_subtotal  := 0;
    resultado_final     := 0;
    TOTAL_X_PALABRA     := 0;
    dividir             := 0;
    DIVISOR             := 0;
    name_array_clave    := OBTENER_LISTA_PALABRAS(p_frase_Clave,
                                                  cantidad_palabra_clave,
                                                  3);
    name_array_comparar := OBTENER_LISTA_PALABRAS(p_frase_comparar,
                                                  cantidad_palabra_comparar,
                                                  3);
  
    /**** COMPARO LAS PALABRAS ********/
    ret := 0;
  
    FOR i IN 1 .. cantidad_palabra_clave - 1
    
     LOOP
      IF (cantidad_palabra_clave - 1) >= 2 THEN
      
        FOR j IN 1 .. cantidad_palabra_comparar - 1 LOOP
        
          -- resultado := utl_match.edit_distance_similarity (name_array_clave(i),name_array_comparar(j));
          resultado := utl_match.jaro_winkler_similarity(name_array_clave(i),
                                                         name_array_comparar(j));
        
          resultado_total := resultado + resultado_total;
        
          DIVISOR := DIVISOR + 1;
        
        END LOOP;
      
      ELSE
      
        FOR j IN 1 .. cantidad_palabra_comparar - 1 LOOP
        
          resultado := utl_match.jaro_winkler_similarity(name_array_clave(i),
                                                         name_array_comparar(j));
        
          resultado_total := resultado_total + resultado;
        
          DIVISOR := j;
        END LOOP;
      
      END IF;
    
    END LOOP;
  
    if (DIVISOR = 0) then
      DIVISOR := 1;
    end if;
  
    resultado_final := resultado_total / DIVISOR;
  
    RETURN resultado_final;
  END COMPARAR_PALABRAS_PROMEDIO_1;

  FUNCTION COMPARAR_PALABRAS_PROMEDIO3(p_frase_Clave    IN VARCHAR2,
                                       p_frase_comparar in VARCHAR2)
    RETURN number AS
    ret                       number;
    v_ret                     number;
    str                       varchar2(2000);
    str_aux                   varchar2(200);
    i                         number;
    j                         number;
    indice                    number;
    name_array_clave          dbms_sql.varchar2_table;
    name_array_comparar       dbms_sql.varchar2_table;
    resultado                 number;
    promedio                  number;
    DIVISOR                   number;
    resultado_total           number;
    resultado_final           number;
    resultado_subtotal        number;
    cantidad_palabra_clave    number;
    cantidad_palabra_comparar number;
  
  BEGIN
    resultado           := 0;
    resultado_total     := 0;
    promedio            := 0;
    resultado_subtotal  := 0;
    resultado_final     := 0;
    name_array_clave    := OBTENER_LISTA_PALABRAS(p_frase_Clave,
                                                  cantidad_palabra_clave,
                                                  3);
    name_array_comparar := OBTENER_LISTA_PALABRAS(p_frase_comparar,
                                                  cantidad_palabra_comparar,
                                                  3);
  
    /**** COMPARO LAS PALABRAS ********/
    ret := 0;
  
    FOR i IN 1 .. cantidad_palabra_clave - 1
    
     LOOP
      IF (cantidad_palabra_clave - 1) >= 2 THEN
      
        FOR j IN 1 .. cantidad_palabra_comparar - 1 LOOP
        
          -- resultado := utl_match.edit_distance_similarity (name_array_clave(i),name_array_comparar(j));
          resultado := utl_match.jaro_winkler_similarity(name_array_clave(i),
                                                         name_array_comparar(j));
        
          --resultado_total :=  resultado + resultado_total;
        
          if (resultado >= resultado_subtotal) then
            resultado_subtotal := resultado;
          end if;
        
        END LOOP;
        resultado_total    := resultado_total + resultado_subtotal;
        resultado_subtotal := 0;
        DIVISOR            := i;
      
      ELSE
      
        FOR j IN 1 .. cantidad_palabra_comparar - 1 LOOP
        
          -- resultado := utl_match.edit_distance_similarity (name_array_clave(i),name_array_comparar(j));
          resultado := utl_match.jaro_winkler_similarity(name_array_clave(i),
                                                         name_array_comparar(j));
        
          if (resultado >= resultado_subtotal) then
            resultado_subtotal := resultado;
          end if;
        
        END LOOP;
      
        resultado_total := resultado_total + resultado_subtotal;
      
        -- resultado_final :=resultado_total/DIVISOR;
      END IF;
    
    END LOOP;
  
    resultado_final := resultado_total / (cantidad_palabra_clave - 1);
  
    RETURN resultado_final;
  END COMPARAR_PALABRAS_PROMEDIO3;

  FUNCTION COMPARAR_PALABRAS_COMBINADAS(p_frase_Clave    IN VARCHAR2,
                                        p_frase_comparar in VARCHAR2)
    RETURN number AS
    ret                       number;
    v_ret                     number;
    str                       varchar2(2000);
    str_aux                   varchar2(200);
    i                         number;
    j                         number;
    indice                    number;
    name_array_clave          dbms_sql.varchar2_table;
    name_array_comparar       dbms_sql.varchar2_table;
    resultado                 number;
    promedio                  number;
    DIVISOR                   number;
    resultado_fonetico        number;
    resultado_total           number;
    resultado_final           number;
    resultado_subtotal        number;
    cantidad_palabra_clave    number;
    cantidad_palabra_comparar number;
  
  BEGIN
    resultado           := 0;
    resultado_total     := 0;
    promedio            := 0;
    resultado_subtotal  := 0;
    resultado_final     := 0;
    name_array_clave    := OBTENER_LISTA_PALABRAS(p_frase_Clave,
                                                  cantidad_palabra_clave,
                                                  3);
    name_array_comparar := OBTENER_LISTA_PALABRAS(p_frase_comparar,
                                                  cantidad_palabra_comparar,
                                                  3);
  
    /**** COMPARO LAS PALABRAS ********/
    ret := 0;
  
    FOR i IN 1 .. cantidad_palabra_clave - 1
    
     LOOP
      IF (cantidad_palabra_clave - 1) >= 2 THEN
      
        FOR j IN 1 .. cantidad_palabra_comparar - 1 LOOP
        
          resultado_fonetico := pkg_snd.FC_Comparar_Fonetica(name_array_clave(i),
                                                             name_array_comparar(j));
          -- resultado := utl_match.edit_distance_similarity (name_array_clave(i),name_array_comparar(j));
        
          if resultado_fonetico >= 1 then
          
            resultado := utl_match.jaro_winkler_similarity(name_array_clave(i),
                                                           name_array_comparar(j));
          else
            --resultado := 0;
            resultado := utl_match.jaro_winkler_similarity(name_array_clave(i),
                                                           name_array_comparar(j));
          end if;
        
          --resultado_total :=  resultado + resultado_total;
        
          if (resultado >= resultado_subtotal) then
            resultado_subtotal := resultado;
          end if;
        
        END LOOP;
        resultado_total    := resultado_total + resultado_subtotal;
        resultado_subtotal := 0;
        DIVISOR            := i;
      
      ELSE
      
        FOR j IN 1 .. cantidad_palabra_comparar - 1 LOOP
          resultado_fonetico := pkg_snd.FC_Comparar_Fonetica(name_array_clave(i),
                                                             name_array_comparar(j));
          if resultado_fonetico >= 1 then
            -- resultado := utl_match.edit_distance_similarity (name_array_clave(i),name_array_comparar(j));
            resultado := utl_match.jaro_winkler_similarity(name_array_clave(i),
                                                           name_array_comparar(j));
          else
            resultado := utl_match.jaro_winkler_similarity(name_array_clave(i),
                                                           name_array_comparar(j));
          end if;
          resultado_total := resultado_total + resultado;
        
          DIVISOR := j;
        END LOOP;
      
        -- resultado_final :=resultado_total/DIVISOR;
      END IF;
    
    END LOOP;
  
    resultado_final := resultado_total / (DIVISOR);
  
    RETURN resultado_final;
  END COMPARAR_PALABRAS_COMBINADAS;

  FUNCTION COMPARAR_PALABRAS_COMBINADAS_1(p_frase_Clave    IN VARCHAR2,
                                          p_frase_comparar in VARCHAR2)
    RETURN number AS
    ret                       number;
    v_ret                     number;
    str                       varchar2(2000);
    str_aux                   varchar2(200);
    i                         number;
    j                         number;
    indice                    number;
    name_array_clave          dbms_sql.varchar2_table;
    name_array_comparar       dbms_sql.varchar2_table;
    resultado                 number;
    resultado_fonetico        number;
    promedio                  number;
    DIVISOR                   number;
    resultado_total           number;
    resultado_final           number;
    resultado_subtotal        number;
    TOTAL_X_PALABRA           number;
    cantidad_palabra_clave    number;
    cantidad_palabra_comparar number;
  
  BEGIN
    resultado           := 0;
    resultado_total     := 0;
    promedio            := 0;
    resultado_subtotal  := 0;
    resultado_final     := 0;
    TOTAL_X_PALABRA     := 0;
    name_array_clave    := OBTENER_LISTA_PALABRAS(p_frase_Clave,
                                                  cantidad_palabra_clave,
                                                  3);
    name_array_comparar := OBTENER_LISTA_PALABRAS(p_frase_comparar,
                                                  cantidad_palabra_comparar,
                                                  3);
  
    /**** COMPARO LAS PALABRAS ********/
    ret := 0;
  
    FOR i IN 1 .. cantidad_palabra_clave - 1
    
     LOOP
      IF (cantidad_palabra_clave - 1) >= 2 THEN
      
        FOR j IN 1 .. cantidad_palabra_comparar - 1 LOOP
          resultado_fonetico := pkg_snd.FC_Comparar_Fonetica(name_array_clave(i),
                                                             name_array_comparar(j));
          -- resultado := utl_match.edit_distance_similarity (name_array_clave(i),name_array_comparar(j));
        
          if resultado_fonetico >= 1 then
            -- resultado := utl_match.edit_distance_similarity (name_array_clave(i),name_array_comparar(j));
            resultado := utl_match.jaro_winkler_similarity(name_array_clave(i),
                                                           name_array_comparar(j));
          else
            resultado := utl_match.jaro_winkler_similarity(name_array_clave(i),
                                                           name_array_comparar(j));
          end if;
        
          resultado_subtotal := resultado + resultado_subtotal;
        
          DIVISOR := j;
        END LOOP;
        /*resultado_total := resultado_subtotal / DIVISOR ;
        TOTAL_X_PALABRA :=  resultado_total + TOTAL_X_PALABRA;*/
      
        TOTAL_X_PALABRA := resultado_subtotal / DIVISOR;
        resultado_total := TOTAL_X_PALABRA + resultado_total;
      
        resultado_subtotal := 0;
        TOTAL_X_PALABRA    := 0;
        --resultado_total:=0;
      
        -- END IF;
      
        -- END IF;      
        DIVISOR := i;
      
      ELSE
      
        FOR j IN 1 .. cantidad_palabra_comparar - 1 LOOP
        
          resultado_fonetico := pkg_snd.FC_Comparar_Fonetica(name_array_clave(i),
                                                             name_array_comparar(j));
          if resultado_fonetico >= 1 then
            -- resultado := utl_match.edit_distance_similarity (name_array_clave(i),name_array_comparar(j));
            resultado := utl_match.jaro_winkler_similarity(name_array_clave(i),
                                                           name_array_comparar(j));
          else
            resultado := utl_match.jaro_winkler_similarity(name_array_clave(i),
                                                           name_array_comparar(j));
          end if;
        
          resultado_total := resultado_total + resultado;
        
          DIVISOR := j;
        END LOOP;
        -- resultado_final :=resultado_total/DIVISOR;
      
      END IF;
    
    END LOOP;
  
    resultado_final := resultado_total / (DIVISOR);
  
    -- resultado_final := TOTAL_X_PALABRA/(DIVISOR); 
  
    RETURN resultado_final;
  END COMPARAR_PALABRAS_COMBINADAS_1;

  FUNCTION COMPARAR_PALABRAS_COMBINADAS_PRO(p_frase_Clave    IN VARCHAR2,
                                            p_frase_comparar in VARCHAR2)
    RETURN number AS
    ret                       number;
    v_ret                     number;
    str                       varchar2(2000);
    str_aux                   varchar2(200);
    i                         number;
    j                         number;
    indice                    number;
    name_array_clave          dbms_sql.varchar2_table;
    name_array_comparar       dbms_sql.varchar2_table;
    resultado                 number;
    resultado_fonetico        number;
    promedio                  number;
    DIVISOR                   number;
    resultado_total           number;
    resultado_final           number;
    resultado_subtotal        number;
    TOTAL_X_PALABRA           number;
    cantidad_palabra_clave    number;
    cantidad_palabra_comparar number;
  
  BEGIN
    resultado           := 0;
    resultado_total     := 0;
    promedio            := 0;
    resultado_subtotal  := 0;
    resultado_final     := 0;
    TOTAL_X_PALABRA     := 0;
    name_array_clave    := OBTENER_LISTA_PALABRAS(p_frase_Clave,
                                                  cantidad_palabra_clave,
                                                  3);
    name_array_comparar := OBTENER_LISTA_PALABRAS(p_frase_comparar,
                                                  cantidad_palabra_comparar,
                                                  3);
  
    /**** COMPARO LAS PALABRAS ********/
    ret := 0;
  
    FOR i IN 1 .. cantidad_palabra_clave - 1
    
     LOOP
      IF (cantidad_palabra_clave - 1) >= 2 THEN
      
        FOR j IN 1 .. cantidad_palabra_comparar - 1 LOOP
          resultado_fonetico := pkg_snd.FC_Comparar_Fonetica(name_array_clave(i),
                                                             name_array_comparar(j));
          -- resultado := utl_match.edit_distance_similarity (name_array_clave(i),name_array_comparar(j));
        
          if resultado_fonetico >= 1 then
            -- resultado := utl_match.edit_distance_similarity (name_array_clave(i),name_array_comparar(j));
            resultado := utl_match.jaro_winkler_similarity(name_array_clave(i),
                                                           name_array_comparar(j));
          else
            resultado := 0;
          end if;
        
          if resultado_subtotal < resultado then
            resultado_subtotal := resultado;
          end if;
        END LOOP;
      
        resultado_total    := resultado_subtotal + resultado_total;
        resultado_subtotal := 0;
      
      ELSE
      
        FOR j IN 1 .. cantidad_palabra_comparar - 1 LOOP
        
          resultado_fonetico := pkg_snd.FC_Comparar_Fonetica(name_array_clave(i),
                                                             name_array_comparar(j));
          if resultado_fonetico >= 1 then
            -- resultado := utl_match.edit_distance_similarity (name_array_clave(i),name_array_comparar(j));
            resultado := utl_match.jaro_winkler_similarity(name_array_clave(i),
                                                           name_array_comparar(j));
          else
            resultado := 0;
          end if;
        
          if resultado_subtotal < resultado then
            resultado_subtotal := resultado;
          end if;
        
        END LOOP;
        resultado_total    := resultado_subtotal + resultado_total;
        resultado_subtotal := 0;
      END IF;
    END LOOP;
  
    resultado_final := resultado_total / (cantidad_palabra_clave - 1);
  
    RETURN resultado_final;
  END COMPARAR_PALABRAS_COMBINADAS_PRO;

  FUNCTION PALABRAS_COMBINADAS_PRO_1(p_frase_Clave    IN VARCHAR2,
                                     p_frase_comparar in VARCHAR2)
    RETURN number AS
    ret                         number;
    v_ret                       number;
    str                         varchar2(2000);
    str_aux                     varchar2(200);
    i                           number;
    j                           number;
    indice                      number;
    name_array_clave            dbms_sql.varchar2_table;
    name_array_comparar         dbms_sql.varchar2_table;
    resultado                   number;
    resultado_fonetico          number;
    promedio                    number;
    DIVISOR                     number;
    resultado_total             number;
    resultado_final             number;
    resultado_subtotal          number;
    TOTAL_X_PALABRA             number;
    cantidad_palabra_clave      number;
    cantidad_palabra_comparar   number;
    resultado_fonetico_clave    varchar2(10);
    resultado_fonetico_comparar varchar2(10);
  
  BEGIN
    resultado          := 0;
    resultado_total    := 0;
    promedio           := 0;
    resultado_subtotal := 0;
    resultado_final    := 0;
    TOTAL_X_PALABRA    := 0;
    --resultado_fonetico_clave := 0;
    --resultado_fonetico_comparar  := 0;
  
    name_array_clave    := OBTENER_LISTA_PALABRAS(p_frase_Clave,
                                                  cantidad_palabra_clave,
                                                  3);
    name_array_comparar := OBTENER_LISTA_PALABRAS(p_frase_comparar,
                                                  cantidad_palabra_comparar,
                                                  3);
  
    /**** COMPARO LAS PALABRAS ********/
    ret := 0;
  
    FOR i IN 1 .. cantidad_palabra_clave - 1
    
     LOOP
      IF (cantidad_palabra_clave - 1) >= 2 THEN
      
        FOR j IN 1 .. cantidad_palabra_comparar - 1 LOOP
        
          resultado_fonetico_clave    := pkg_snd.soundesp(name_array_clave(i));
          resultado_fonetico_comparar := pkg_snd.soundesp(name_array_comparar(j));
        
          if resultado_fonetico_clave = resultado_fonetico_comparar then
          
            resultado := utl_match.jaro_winkler_similarity(name_array_clave(i),
                                                           name_array_comparar(j));
            resultado := resultado * 1;
          
          else
            --resultado := 0;
          
            resultado := utl_match.jaro_winkler_similarity(name_array_clave(i),
                                                           name_array_comparar(j));
            resultado := resultado * 0.9;
          
          end if;
        
          if resultado_subtotal < resultado then
            resultado_subtotal := resultado;
          end if;
        END LOOP;
      
        resultado_total    := resultado_subtotal + resultado_total;
        resultado_subtotal := 0;
      
      ELSE
      
        FOR j IN 1 .. cantidad_palabra_comparar - 1 LOOP
        
          resultado_fonetico_clave    := pkg_snd.soundesp(name_array_clave(i));
          resultado_fonetico_comparar := pkg_snd.soundesp(name_array_comparar(j));
        
          if resultado_fonetico_clave = resultado_fonetico_comparar then
          
            resultado := utl_match.jaro_winkler_similarity(name_array_clave(i),
                                                           name_array_comparar(j));
            resultado := resultado * 1;
          
          else
            --resultado := 0;
          
            resultado := utl_match.jaro_winkler_similarity(name_array_clave(i),
                                                           name_array_comparar(j));
            resultado := resultado * 0.9;
          
          end if;
        
          if resultado_subtotal < resultado then
            resultado_subtotal := resultado;
          end if;
        
        END LOOP;
        resultado_total    := resultado_subtotal + resultado_total;
        resultado_subtotal := 0;
      END IF;
    END LOOP;
  
    resultado_final := resultado_total / (cantidad_palabra_clave - 1);
  
    RETURN resultado_final;
  END PALABRAS_COMBINADAS_PRO_1;

  FUNCTION PALABRAS_PROMEDIO_BASE(p_frase_Clave    IN VARCHAR2,
                                  p_frase_comparar in VARCHAR2) RETURN number AS
    ret                       number;
    v_ret                     number;
    str                       varchar2(2000);
    str_aux                   varchar2(200);
    i                         number;
    j                         number;
    indice                    number;
    name_array_clave          dbms_sql.varchar2_table;
    name_array_comparar       dbms_sql.varchar2_table;
    resultado                 number;
    promedio                  number;
    DIVISOR                   number;
    resultado_total           number;
    resultado_final           number;
    resultado_subtotal        number;
    cantidad_palabra_clave    number;
    cantidad_palabra_comparar number;
  
  BEGIN
    resultado           := 0;
    resultado_total     := 0;
    promedio            := 0;
    resultado_subtotal  := 0;
    resultado_final     := 0;
    name_array_clave    := OBTENER_LISTA_PALABRAS(p_frase_Clave,
                                                  cantidad_palabra_clave,
                                                  3);
    name_array_comparar := OBTENER_LISTA_PALABRAS(p_frase_comparar,
                                                  cantidad_palabra_comparar,
                                                  3);
  
    /**** COMPARO LAS PALABRAS ********/
    ret := 0;
  
    FOR i IN 1 .. cantidad_palabra_comparar - 1
    
     LOOP
      IF (cantidad_palabra_comparar - 1) >= 2 THEN
      
        FOR j IN 1 .. cantidad_palabra_clave - 1 LOOP
        
          -- resultado := utl_match.edit_distance_similarity (name_array_clave(i),name_array_comparar(j));
          resultado := utl_match.jaro_winkler_similarity(name_array_clave(j),
                                                         name_array_comparar(i));
        
          --resultado_total :=  resultado + resultado_total;
        
          if (resultado >= resultado_subtotal) then
            resultado_subtotal := resultado;
          end if;
        
        END LOOP;
        resultado_total    := resultado_total + resultado_subtotal;
        resultado_subtotal := 0;
        DIVISOR            := i;
      
      ELSE
      
        FOR j IN 1 .. cantidad_palabra_clave - 1 LOOP
        
          -- resultado := utl_match.edit_distance_similarity (name_array_clave(i),name_array_comparar(j));
          resultado := utl_match.jaro_winkler_similarity(name_array_clave(j),
                                                         name_array_comparar(i));
        
          resultado_total := resultado_total + resultado;
        
          DIVISOR := j;
        END LOOP;
      
        -- resultado_final :=resultado_total/DIVISOR;
      END IF;
    
    END LOOP;
  
    resultado_final := resultado_total / (DIVISOR);
  
    RETURN resultado_final;
  END PALABRAS_PROMEDIO_BASE;

  FUNCTION COMPARAR_PALABRAS_ENCONTRADAS(p_frase_Clave    IN VARCHAR2,
                                         p_frase_comparar in VARCHAR2)
    RETURN number AS
    ret                         number;
    v_ret                       number;
    str                         varchar2(2000);
    str_aux                     varchar2(200);
    i                           number;
    j                           number;
    indice                      number;
    name_array_clave            dbms_sql.varchar2_table;
    name_array_comparar         dbms_sql.varchar2_table;
    resultado                   number;
    promedio                    number;
    DIVISOR                     number;
    resultado_total             number;
    resultado_final             number;
    resultado_subtotal          number;
    cantidad_palabra_clave      number;
    cantidad_palabra_comparar   number;
    palabra_encontrada          varchar2(2000);
    name_array_clave_coin       dbms_sql.varchar2_table;
    name_array_clave_no_coin    dbms_sql.varchar2_table;
    name_array_comparar_coin    dbms_sql.varchar2_table;
    name_array_comparar_no_coin dbms_sql.varchar2_table;
    resultado_fonetico          number;
    ind_coinc_clave             number;
    ind_coinc_compara           number;
    ind_coinc_min               number;
    ind_no_coinc_clave          number;
    ind_no_coinc_compara        number;
    ban                         number;
    ban_ind_coinc_min           number;
    resultado_fonetico_clave    varchar2(10);
    resultado_fonetico_comparar varchar2(10);
  
  BEGIN
    resultado           := 0;
    resultado_total     := 0;
    promedio            := 0;
    resultado_subtotal  := 0;
    resultado_final     := 0;
    palabra_encontrada  := 'xz';
    resultado_fonetico  := 0;
    ind_coinc_min       := 0;
    ban_ind_coinc_min   := 0;
    name_array_clave    := OBTENER_LISTA_PALABRAS(p_frase_Clave,
                                                  cantidad_palabra_clave,
                                                  3);
    name_array_comparar := OBTENER_LISTA_PALABRAS(p_frase_comparar,
                                                  cantidad_palabra_comparar,
                                                  3);
  
    /**** COMPARO LAS PALABRAS ********/
    ban                  := 0;
    ind_coinc_compara    := 0;
    ind_no_coinc_compara := 0;
    ind_no_coinc_clave   := 0;
    ind_coinc_clave      := 0;
  
    FOR i IN 1 .. cantidad_palabra_clave - 1 LOOP
      FOR j IN 1 .. cantidad_palabra_comparar - 1 LOOP
      
        resultado := utl_match.jaro_winkler_similarity(name_array_clave(i),
                                                       name_array_comparar(j));
      
        if (pkg_snd.soundesp(name_array_clave(i)) =
           pkg_snd.soundesp(name_array_comparar(j))) then
          resultado_fonetico := 1;
        else
          resultado_fonetico := 0;
        end if;
      
        IF (resultado >= 93 or
           (resultado_fonetico >= 1 and resultado >= 90)) THEN
          ind_coinc_compara := ind_coinc_compara + 1;
          name_array_comparar_coin(ind_coinc_compara) := name_array_comparar(j);
          ban_ind_coinc_min := 1;
        END IF;
      END LOOP;
      if ban_ind_coinc_min = 1 then
        ind_coinc_min     := ind_coinc_min + 1;
        ban_ind_coinc_min := 0;
      end if;
    END LOOP;
  
    FOR i IN 1 .. cantidad_palabra_comparar - 1 LOOP
      FOR j IN 1 .. cantidad_palabra_clave - 1 LOOP
      
        resultado := utl_match.jaro_winkler_similarity(name_array_clave(j),
                                                       name_array_comparar(i));
      
        if (pkg_snd.soundesp(name_array_clave(j)) =
           pkg_snd.soundesp(name_array_comparar(i))) then
          resultado_fonetico := 1;
        else
          resultado_fonetico := 0;
        end if;
      
        IF (resultado >= 93 or
           (resultado_fonetico >= 1 and resultado >= 90)) THEN
          ind_coinc_clave := ind_coinc_clave + 1;
          name_array_clave_coin(ind_coinc_clave) := name_array_clave(J);
        
        END IF;
      END LOOP;
    END LOOP;
  
    --------------------palabra de la base no coincidentes
    FOR i IN 1 .. cantidad_palabra_comparar - 1 LOOP
      FOR j IN 1 .. ind_coinc_compara LOOP
      
        IF (name_array_comparar(i) = name_array_comparar_coin(j)) THEN
          ban := 1;
        END IF;
      
      END LOOP;
    
      IF (ban = 0) THEN
        ind_no_coinc_compara := ind_no_coinc_compara + 1;
        name_array_comparar_no_coin(ind_no_coinc_compara) := name_array_comparar(i);
      
      END IF;
      ban := 0;
    END LOOP;
  
    --------------------palabras ingresadas no coincidentes
    ban := 0;
  
    FOR i IN 1 .. cantidad_palabra_clave - 1 LOOP
      FOR j IN 1 .. ind_coinc_clave LOOP
      
        IF (name_array_clave(i) = name_array_clave_coin(j)) THEN
          ban := 1;
        END IF;
      
      END LOOP;
    
      IF (ban = 0) THEN
        ind_no_coinc_clave := ind_no_coinc_clave + 1;
        name_array_clave_no_coin(ind_no_coinc_clave) := name_array_clave(i);
      
      END IF;
      ban := 0;
    END LOOP;
  
    ---------------BUSCAR PROMEDIO-------
  
    FOR i IN 1 .. ind_no_coinc_clave LOOP
    
      FOR j IN 1 .. ind_no_coinc_compara LOOP
      
       /* resultado_fonetico_clave    := pkg_snd.soundesp(name_array_clave_no_coin(i));
        resultado_fonetico_comparar := pkg_snd.soundesp(name_array_comparar_no_coin(j));
      
        if resultado_fonetico_clave = resultado_fonetico_comparar then*/
        
          resultado := utl_match.jaro_winkler_similarity(name_array_clave_no_coin(i),
                                                         name_array_comparar_no_coin(j));
        /*  resultado := resultado * 1;
        
        else
        
          resultado := utl_match.jaro_winkler_similarity(name_array_clave_no_coin(i),
                                                         name_array_comparar_no_coin(j));
          resultado := resultado * 0.9;
        
        end if;*/
      
        if resultado_subtotal < resultado then
          resultado_subtotal := resultado;
        end if;
      END LOOP;
    
      resultado_total    := resultado_subtotal + resultado_total;
      resultado_subtotal := 0;
    
    END LOOP;
    resultado_total := resultado_subtotal + resultado_total;
    --resultado_subtotal := 0;
  
    IF (ind_no_coinc_clave = 0 or ind_no_coinc_compara = 0) THEN
    
      IF (ind_coinc_min = 0) THEN
        resultado_final := 0;
      ELSE
        if ind_coinc_min > 1 then
          resultado_final := 100;
        else
        
          FOR i IN 1 .. cantidad_palabra_clave - 1 LOOP
          
            FOR j IN 1 .. cantidad_palabra_comparar - 1 LOOP
            
              resultado := utl_match.jaro_winkler_similarity(name_array_clave(i),
                                                             name_array_comparar(j));
            
              if resultado_subtotal < resultado then
                resultado_subtotal := resultado;
              end if;
            END LOOP;
          
            resultado_total    := resultado_subtotal + resultado_total;
            resultado_subtotal := 0;
          
          END LOOP;
          resultado_final := resultado_total / (cantidad_palabra_clave - 1);
          
        resultado_subtotal := 0;
        resultado_total := 0;
        
          FOR i IN 1 .. cantidad_palabra_clave - 1 LOOP
          
            FOR j IN 1 .. cantidad_palabra_comparar - 1 LOOP
            
              resultado := utl_match.jaro_winkler_similarity(name_array_clave(i),
                                                             name_array_comparar(j));
            
              resultado_subtotal := resultado + resultado_subtotal;
            
            END LOOP;
            resultado_total    := resultado_subtotal + resultado_total;
            resultado_subtotal := 0;
          END LOOP;
          resultado_total := resultado_total / ((cantidad_palabra_clave -1) *( cantidad_palabra_comparar - 1));
          resultado_final := (resultado_final + (((100 - resultado_total) * 4 / 5) +
                             resultado_total)) / 2;
        end if;
      END IF;
    
    ELSE
      resultado_final := resultado_total / ind_no_coinc_clave;
    
      resultado_final := (resultado_final + (100 * ind_coinc_min)) /
                         ((ind_coinc_min) + 1);
    END IF;
  
    RETURN resultado_final;
  
  END COMPARAR_PALABRAS_ENCONTRADAS;

END PKG_UIF_OLD;
/

