CREATE OR REPLACE PACKAGE IPJ.FONDO_COMERCIO AS
    
  PROCEDURE SP_Traer_Lista_Fondo_Comercio(
    p_Cursor OUT types.cursorType
    );
    
  PROCEDURE SP_Traer_Fondo_Comercio(
    p_Cursor OUT types.cursorType,
    p_id_tramite_ipj in number,
    p_id_tramiteipj_accion in number,
    p_id_tipo_accion in number,
    p_id_fondo_comercio in number
    );
    
  PROCEDURE SP_Traer_Fondo_Domicilio(
      p_Cursor OUT TYPES.cursorType,
      p_id_tramite_ipj in number,
      p_id_tramiteipj_accion in number,
      p_id_tipo_accion in number,
      p_id_fondo_comercio in number,      
      p_id_vin in number
      );
      
  PROCEDURE SP_Traer_Comp_Vend_Domicilio(
      p_Cursor OUT TYPES.cursorType,
      p_id_vin in number,
      p_id_integrante in number,
      p_id_legajo in number);
      
  PROCEDURE SP_Traer_Fondo_Rubros(
    p_Cursor OUT types.cursorType,
    p_id_tramite_ipj in number,
    p_id_tramiteipj_accion in number,
    p_id_tipo_accion in number,
    p_id_fondo_comercio in number
    );
    
  PROCEDURE SP_Traer_Fondo_Vendedores(
    p_Cursor OUT types.cursorType,
    p_id_tramite_ipj in number,
    p_id_tramiteipj_accion in number,
    p_id_tipo_accion in number,
    p_id_fondo_comercio in number
    );
    
  PROCEDURE SP_Traer_Fondo_Compradores(
    p_Cursor OUT types.cursorType,
    p_id_tramite_ipj in number,
    p_id_tramiteipj_accion in number,
    p_id_tipo_accion in number,
    p_id_fondo_comercio in number
    );
    
  PROCEDURE SP_Traer_Fondo_InsLegal(
    p_Cursor OUT types.cursorType,
    p_id_tramite_ipj in number,
    p_id_tramiteipj_accion in number,
    p_id_tipo_accion in number,
    p_id_fondo_comercio in number
    );
    
  PROCEDURE SP_Hist_Fondo_Compradores(
    p_Cursor OUT types.cursorType,
    p_id_tramite_ipj in number,
    p_id_tramiteipj_accion in number,
    p_id_tipo_accion in number,
    p_id_fondo_comercio in number
    );
    
  PROCEDURE SP_Hist_Fondo_Vendedores(
    p_Cursor OUT types.cursorType,
    p_id_tramite_ipj in number,
    p_id_tramiteipj_accion in number,
    p_id_tipo_accion in number,
    p_id_fondo_comercio in number
    );
    
  PROCEDURE SP_GUARDAR_FONDO_COMERCIO(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_fondo_comercio out number,
    p_id_tramite_ipj in number,
    p_id_tramiteipj_accion in number,
    p_ID_FONDO_COMERCIO in number,
    p_N_FONDO_COMERCIO in varchar2,
    p_FEC_INSCRIPCION in varchar2,
    p_FOLIO in varchar2,
    p_TOMO in varchar2,
    p_ANO in number,
    p_PROTOCOLO in varchar2,
    p_MATRICULA in varchar2,
    p_MATRICULA_VERSION in number,
    p_ID_VIN in number,
    p_OBJETO_SOCIAL in varchar2,
    p_letra in varchar2
  );
  
   PROCEDURE SP_GUARDAR_FONDO_RUBROS(
     o_rdo out varchar2,
     o_tipo_mensaje out number,
     p_id_tramite_ipj in number,
     p_id_tramiteipj_accion in number,
     p_id_tipo_accion in number,
     p_id_fondo_comercio in number,
     p_id_rubro in varchar2,
     p_id_tipo_actividad in varchar2,
     p_id_actividad in varchar2,
     p_fec_inicio in varchar2,
     p_fec_vencimiento in varchar2,
     p_eliminar in number  -- 1 elimina
     );  
     
  PROCEDURE SP_GUARDAR_FONDO_IL(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_ID_TRAMITE_IPJ in NUMBER,
    p_ID_TRAMITEIPJ_ACCION in NUMBER,
    p_ID_TIPO_ACCION in NUMBER,
    p_ID_FONDO_COMERCIO in NUMBER,
    p_ID_JUZGADO in NUMBER,
    p_IL_TIPO in NUMBER,
    p_IL_NUMERO in NUMBER,
    p_IL_FECHA in varchar2,
    p_TITULO in VARCHAR2,
    p_DESCRIPCION in VARCHAR2,
    p_FECHA in varchar2,
    p_eliminar in number
    );
    
  PROCEDURE SP_GUARDAR_FONDO_VENDEDOR(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_ID_FONDO_COMERCIO_VENDEDOR out number,
    p_ID_FONDO_COMERCIO in number,
    p_ID_TRAMITE_IPJ in number,
    p_ID_TRAMITEIPJ_ACCION in number,
    p_TIPO_PERSONA  in number,
    p_ID_INTEGRANTE in number,
    p_id_sexo in varchar2,
    p_nro_documento in varchar2,
    p_pai_cod_pais in varchar2,
    p_id_numero in number,
    p_detalle in varchar2,
    p_nombre in varchar2,
    p_apellido in varchar2,
    p_error_dato in varchar2,
    p_id_legajo in number,
    p_FEC_ALTA in varchar2,
    p_FEC_BAJA in varchar2,
    p_BORRADOR in varchar2,
    p_id_vin in number,
    p_ID_FONDO_COMERCIO_VENDEDOR in number,
    p_n_tipo_documento in varchar2,
    p_eliminar in number  -- 1 elimina
    ); 
    
  PROCEDURE SP_GUARDAR_FONDO_COMPRADOR(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_ID_FONDO_COMERCIO_COMPRADOR out number,
    p_ID_FONDO_COMERCIO in number,
    p_ID_TRAMITE_IPJ in number,
    p_ID_TRAMITEIPJ_ACCION in number,
    p_TIPO_PERSONA  in number,
    p_ID_INTEGRANTE in number,
    p_id_sexo in varchar2,
    p_nro_documento in varchar2,
    p_pai_cod_pais in varchar2,
    p_id_numero in number,
    p_detalle in varchar2,
    p_nombre in varchar2,
    p_apellido in varchar2,
    p_error_dato in varchar2,
    p_id_legajo in number,
    p_FEC_ALTA in varchar2,
    p_FEC_BAJA in varchar2,
    p_BORRADOR in varchar2,
    p_id_vin in number,
    p_ID_FONDO_COMERCIO_COMPRADOR in number,
    p_n_tipo_documento in varchar2,
    p_eliminar in number  -- 1 elimina
    );
    
  PROCEDURE SP_GUARDAR_FONDO_DOMICILIO(
    o_id_vin out number, 
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_ID_FONDO_COMERCIO in number, 
    p_id_vin in number,
    P_ID_PROVINCIA in varchar2,
    P_ID_DEPARTAMENTO in number,
    P_ID_LOCALIDAD in number,
    P_BARRIO in varchar2,
    P_CALLE in varchar2,
    P_ALTURA in number,
    P_DEPTO in varchar2,
    P_PISO in varchar2,
    p_torre in varchar2,
    p_manzana in varchar2,
    p_lote in varchar2); 
 
 PROCEDURE SP_GUARDAR_FONDO_COMPVEND_DOM(
    o_id_vin out number, 
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_ID_FONDO_COMERCIO in number, 
    p_ID_FONDO_COMERCIO_COMPRADOR in number,
    p_ID_FONDO_COMERCIO_VENDEDOR in number,
    p_id_vin in number,
    P_ID_PROVINCIA in varchar2,
    P_ID_DEPARTAMENTO in number,
    P_ID_LOCALIDAD in number,
    P_BARRIO in varchar2,
    P_CALLE in varchar2,
    P_ALTURA in number,
    P_DEPTO in varchar2,
    P_PISO in varchar2,
    p_torre in varchar2,
    p_manzana in varchar2,
    p_lote in varchar2);   
  
END FONDO_COMERCIO;
/

create or replace package body ipj.FONDO_COMERCIO is

  PROCEDURE SP_Traer_Lista_Fondo_Comercio(
    p_Cursor OUT types.cursorType)
  IS
  BEGIN
    OPEN p_Cursor FOR
      select ID_FONDO_COMERCIO, N_FONDO_COMERCIO
      from IPJ.T_FONDOS_COMERCIO fc;

  END SP_Traer_Lista_Fondo_Comercio;


  PROCEDURE SP_Traer_Fondo_Comercio(
    p_Cursor OUT types.cursorType,
    p_id_tramite_ipj in number,
    p_id_tramiteipj_accion in number,
    p_id_tipo_accion in number,
    p_id_fondo_comercio in number
    )
  is
    v_existe number;
    v_id_tramite_ipj number;
    v_id_tramiteipj_accion number;
    v_id_tipo_accion number;
    v_id_integrante number;
  BEGIN
    if nvl(p_id_fondo_comercio, 0) <= 0 then
      OPEN p_Cursor FOR
       select
          p_id_tramite_ipj id_tramite_ipj, p_id_tramiteipj_accion id_tramiteipj_accion, p_id_tipo_accion id_tipo_accion,
          null ID_FONDO_COMERCIO, '' N_FONDO_COMERCIO, null FEC_INSCRIPCION,
          null FOLIO, null TOMO, null ano, null PROTOCOLO, null MATRICULA, '' letra, null MATRICULA_VERSION,
          null ID_VIN, null OBJETO_SOCIAL
       from dual;
    else
      OPEN p_Cursor FOR
       select
          p_id_tramite_ipj id_tramite_ipj, p_id_tramiteipj_accion id_tramiteipj_accion, p_id_tipo_accion id_tipo_accion,
          ID_FONDO_COMERCIO, N_FONDO_COMERCIO, to_char(FEC_INSCRIPCION, 'dd/mm/rrrr') FEC_INSCRIPCION,
          FOLIO, TOMO, A�O ano, PROTOCOLO,
          trim(TRANSLATE(MATRICULA, 'ABCDEF','      ')) MATRICULA,
          trim(TRANSLATE(MATRICULA, '0123456789','          ')) Letra,
          MATRICULA_VERSION, ID_VIN, OBJETO_SOCIAL
       from IPJ.T_FONDOS_COMERCIO
       where
         id_fondo_comercio = p_id_fondo_comercio;
    end if;

  END SP_Traer_Fondo_Comercio;


   PROCEDURE SP_Traer_Fondo_Domicilio(
      p_Cursor OUT TYPES.cursorType,
      p_id_tramite_ipj in number,
      p_id_tramiteipj_accion in number,
      p_id_tipo_accion in number,
      p_id_fondo_comercio in number,
      p_id_vin in number)
  IS
    v_existe_domicilio number;
  BEGIN
    /********************************************************
      Este procemiento devuelve la direccion de la sede una entidad de un tramite
    *********************************************************/
     IPJ.VARIOS.SP_Traer_Domicilio(
        p_id_vin => p_id_vin,
        p_id_integrante => 0,
        p_id_legajo  => 0,
        p_cuit_empresa => null,
        p_id_fondo_comercio => p_id_fondo_comercio,
        p_id_entidad_acta => 0,
        p_es_comerciante => 'N',
        p_es_admin_entidad => 'N',
        p_Cursor => p_cursor
      );

  END SP_Traer_Fondo_Domicilio;

  PROCEDURE SP_Traer_Comp_Vend_Domicilio(
      p_Cursor OUT TYPES.cursorType,
      p_id_vin in number,
      p_id_integrante in number,
      p_id_legajo in number)
  IS
  BEGIN

     IPJ.VARIOS.SP_Traer_Domicilio(
        p_id_vin => p_id_vin,
        p_id_integrante => p_id_integrante,
        p_id_legajo  => p_id_legajo,
        p_cuit_empresa => null,
        p_id_fondo_comercio => 0,
        p_id_entidad_acta => 0,
        p_es_comerciante => 'N',
        p_es_admin_entidad => 'N',
        p_Cursor => p_cursor
      );

  END SP_Traer_Comp_Vend_Domicilio;


  PROCEDURE SP_Traer_Fondo_Rubros(
    p_Cursor OUT types.cursorType,
    p_id_tramite_ipj in number,
    p_id_tramiteipj_accion in number,
    p_id_tipo_accion in number,
    p_id_fondo_comercio in number)
  IS
  BEGIN
    OPEN p_Cursor FOR
     select R.ID_TRAMITE_IPJ, r.id_tramiteipj_accion,
       to_char(r.fec_desde, 'dd/mm/rrrr') fecha_desde, to_char(r.fec_hasta, 'dd/mm/rrrr') fecha_hasta,
       r.id_rubro, ACT.N_RUBRO,  R.ID_TIPO_ACTIVIDAD, '' n_tipo_actividad,
       R.ID_ACTIVIDAD, ACT.N_ACTIVIDAD, r.borrador, R.ID_FONDO_COMERCIO
    from ipj.T_FONDOS_COMERCIO_RUBRO r join  T_COMUNES.VT_ACTIVIDADES act
          on R.ID_ACTIVIDAD = ACT.ID_ACTIVIDAD
    where
      r.id_fondo_comercio = p_id_fondo_comercio and
      (r.borrador = 'N' or r.ID_TRAMITE_IPJ = p_id_tramite_ipj) and
      (r.fec_hasta is null or r.fec_hasta > sysdate or r.borrador = 'S') ;

  END SP_Traer_Fondo_Rubros;


  PROCEDURE SP_Traer_Fondo_Vendedores(
    p_Cursor OUT types.cursorType,
    p_id_tramite_ipj in number,
    p_id_tramiteipj_accion in number,
    p_id_tipo_accion in number,
    p_id_fondo_comercio in number)
  IS
  BEGIN

    OPEN p_Cursor FOR
     select
        fcv.ID_FONDO_COMERCIO, fcv.ID_TRAMITE_IPJ, fcv.ID_TRAMITEIPJ_ACCION,
        fcv.TIPO_PERSONA, fcv.ID_INTEGRANTE, L.CUIT, to_char(fcv.FEC_ALTA, 'dd/mm/rrrr') FEC_ALTA,
        to_char(fcv.FEC_BAJA, 'dd/mm/rrrr') FEC_BAJA, fcv.BORRADOR, FCV.ID_FONDO_COMERCIO_VENDEDOR,
        I.DETALLE, I.ID_NUMERO, I.ID_SEXO, I.ID_VIN, I.NRO_DOCUMENTO, I.PAI_COD_PAIS,
        L.Denominacion_Sia razon_social,
        (case fcv.TIPO_PERSONA when 1 then 'Empresa' else 'Persona' end) N_TIPO_PERSONA,
        (case fcv.TIPO_PERSONA when 1 then L.CUIT else I.NRO_DOCUMENTO end) Identificacion,
        (case fcv.TIPO_PERSONA when 1 then L.Denominacion_Sia else I.DETALLE end) Nombre_Detalle,
        d.id_tipodom, d.n_tipodom, d.id_tipocalle, d.n_tipocalle, d.id_calle, initcap(d.n_calle) n_calle,
        d.altura, d.depto, d.piso, d.torre, d.id_barrio, initcap(d.n_barrio) n_barrio, /*d.nro_mail, d.cod_area,*/
        d.id_localidad, initcap(d.n_localidad) n_loocalidad, d.id_departamento,
        initcap(d.n_departamento) n_departamento, d.id_provincia,
        initcap(d.n_provincia) n_provincia, d.cpa, l.id_legajo,
        1 imprimir
     from ipj.T_FONDOS_COMERCIO_VENDEDOR fcv  left join IPJ.T_INTEGRANTES i
         on fcv.id_integrante = i.id_integrante
       left join IPJ.T_Legajos L
         on fcv.id_legajo = L.id_legajo
       left join dom_manager.VT_DOMICILIOS_COND d
         on id_app = IPJ.TYPES.C_ID_APLICACION and
           nvl(i.id_vin, IPJ.ENTIDAD_PERSJUR.FC_DIRECCION_ENTIDAD(L.Cuit)) = d.id_vin
           --and d.id_entidad = IPJ.VARIOS.FC_Generar_Entidad_Dom (nvl(i.id_integrante, 0), nvl(L.id_legajo, 0), 0, 0, 'N', 'N')
     where
       fcv.id_fondo_comercio = p_id_fondo_comercio and
       (fcv.borrador = 'N' or fcv.ID_TRAMITE_IPJ = p_id_tramite_ipj) and
       (fcv.fec_baja is null or (fcv.fec_baja is not null and fcv.borrador = 'S')) ;

  END SP_Traer_Fondo_Vendedores;


  PROCEDURE SP_Traer_Fondo_Compradores(
    p_Cursor OUT types.cursorType,
    p_id_tramite_ipj in number,
    p_id_tramiteipj_accion in number,
    p_id_tipo_accion in number,
    p_id_fondo_comercio in number)
  IS
    v_id_vin_empresa number;
  BEGIN

    OPEN p_Cursor FOR
     select
        fcc.ID_FONDO_COMERCIO, fcc.ID_TRAMITE_IPJ, fcc.ID_TRAMITEIPJ_ACCION,
        fcc.TIPO_PERSONA, fcc.ID_INTEGRANTE, L.CUIT, to_char(fcc.FEC_ALTA, 'dd/mm/rrrr') FEC_ALTA,
        to_char(fcc.FEC_BAJA, 'dd/mm/rrrr') FEC_BAJA, fcc.BORRADOR, FCC.ID_FONDO_COMERCIO_COMPRADOR,
        I.DETALLE, I.ID_NUMERO, I.ID_SEXO, I.ID_VIN, I.NRO_DOCUMENTO, I.PAI_COD_PAIS,
        L.Denominacion_Sia razon_social,
        (case fcc.TIPO_PERSONA when 1 then 'Empresa' else 'Persona' end) N_TIPO_PERSONA,
        (case fcc.TIPO_PERSONA when 1 then L.CUIT else I.NRO_DOCUMENTO end) Identificacion,
        (case fcc.TIPO_PERSONA when 1 then L.Denominacion_Sia else I.DETALLE end) Nombre_Detalle,
        d.id_tipodom, d.n_tipodom, d.id_tipocalle, d.n_tipocalle, d.id_calle, d.n_calle,
        d.altura, d.depto, d.piso, d.torre, d.id_barrio, d.n_barrio, /*d.nro_mail, d.cod_area,*/
        d.id_localidad, d.n_localidad, d.id_departamento, d.n_departamento, d.id_provincia,
        d.n_provincia, d.cpa, l.id_legajo,
        1 imprimir
     from ipj.T_FONDOS_COMERCIO_COMPRADOR fcc  left join IPJ.T_INTEGRANTES i
          on fcc.id_integrante = i.id_integrante
        left join IPJ.T_Legajos L
          on fcc.id_legajo = L.id_legajo
        left join dom_manager.VT_DOMICILIOS_COND d
         on id_app = IPJ.TYPES.C_ID_APLICACION and
           nvl(i.id_vin, IPJ.ENTIDAD_PERSJUR.FC_DIRECCION_ENTIDAD(L.Cuit)) = d.id_vin
           --and d.id_entidad = IPJ.VARIOS.FC_Generar_Entidad_Dom (nvl(i.id_integrante, 0), nvl(L.id_legajo, 0), 0, 0, 'N', 'N')
     where
       fcc.id_fondo_comercio = p_id_fondo_comercio and
       (fcc.borrador = 'N' or fcc.ID_TRAMITE_IPJ = p_id_tramite_ipj) and
       (fcc.fec_baja is null or (fcc.fec_baja is not null and fcc.borrador = 'S')) ;

  END SP_Traer_Fondo_Compradores;


  PROCEDURE SP_Traer_Fondo_InsLegal(
    p_Cursor OUT types.cursorType,
    p_id_tramite_ipj in number,
    p_id_tramiteipj_accion in number,
    p_id_tipo_accion in number,
    p_id_fondo_comercio in number)
  IS
  BEGIN
    OPEN p_Cursor FOR
      select
         fcil.ID_TRAMITE_IPJ, fcil.ID_TRAMITEIPJ_ACCION,
         fcil.id_fondo_comercio, fcil.ID_JUZGADO,
         fcil.IL_TIPO, fcil.IL_NUMERO, To_Char(fcil.IL_FECHA, 'dd/mm/rrrr') IL_FECHA,
         fcil.TITULO, fcil.DESCRIPCION, fcil.BORRADOR, To_Char(fcil.FECHA, 'dd/mm/rrrr') FECHA,
         J.N_JUZGADO, L.N_IL_TIPO
      from ipj.T_FONDOS_COMERCIO_INS_LEGAL fcil join ipj.t_juzgados j
          on fcil.ID_JUZGADO = J.ID_JUZGADO
        join IPJ.T_TIPOS_INS_LEGAL L
          on fcil.IL_TIPO = L.IL_TIPO
      where
        fcil.id_fondo_comercio = p_id_fondo_comercio and
        (fcil.borrador = 'N' or fcil.ID_TRAMITE_IPJ = p_id_tramite_ipj)
      order by fcil.fecha desc, fcil.IL_FECHA desc;

  END SP_Traer_Fondo_InsLegal;


  PROCEDURE SP_Hist_Fondo_Compradores(
    p_Cursor OUT types.cursorType,
    p_id_tramite_ipj in number,
    p_id_tramiteipj_accion in number,
    p_id_tipo_accion in number,
    p_id_fondo_comercio in number)
  IS
  BEGIN
    OPEN p_Cursor FOR
     select
        fcc.ID_FONDO_COMERCIO, fcc.ID_TRAMITE_IPJ, fcc.ID_TRAMITEIPJ_ACCION,
        fcc.TIPO_PERSONA, fcc.ID_INTEGRANTE, L.CUIT, to_char(fcc.FEC_ALTA, 'dd/mm/rrrr') FEC_ALTA,
        to_char(fcc.FEC_BAJA, 'dd/mm/rrrr') FEC_BAJA, fcc.BORRADOR,
        I.DETALLE, IPJ.TRAMITES.FC_RAZON_SOCIAL(L.cuit) razon_social,
        (case fcc.TIPO_PERSONA when 1 then 'Empresa' else 'Persona' end) N_TIPO_PERSONA
     from ipj.T_FONDOS_COMERCIO_COMPRADOR fcc  left join IPJ.T_INTEGRANTES i
         on fcc.id_integrante = i.id_integrante
       left join IPJ.T_Legajos L
         on FCC.ID_LEGAJO = L.id_legajo
     where
       fcc.id_fondo_comercio = p_id_fondo_comercio and
       fcc.fec_baja is not null and
       fcc.borrador = 'N'
     order by fcc.fec_baja desc;

  END SP_Hist_Fondo_Compradores;


  PROCEDURE SP_Hist_Fondo_Vendedores(
    p_Cursor OUT types.cursorType,
    p_id_tramite_ipj in number,
    p_id_tramiteipj_accion in number,
    p_id_tipo_accion in number,
    p_id_fondo_comercio in number)
  IS
  BEGIN
    OPEN p_Cursor FOR
     select
        fcv.ID_FONDO_COMERCIO, fcv.ID_TRAMITE_IPJ, fcv.ID_TRAMITEIPJ_ACCION,
        fcv.TIPO_PERSONA, fcv.ID_INTEGRANTE, L.CUIT, to_char(fcv.FEC_ALTA, 'dd/mm/rrrr') FEC_ALTA,
        to_char(fcv.FEC_BAJA, 'dd/mm/rrrr') FEC_BAJA, fcv.BORRADOR,
        I.DETALLE, IPJ.TRAMITES.FC_RAZON_SOCIAL(L.cuit) razon_social,
        (case fcv.TIPO_PERSONA when 1 then 'Empresa' else 'Persona' end) N_TIPO_PERSONA
     from ipj.T_FONDOS_COMERCIO_VENDEDOR fcv  left join IPJ.T_INTEGRANTES i
         on fcv.id_integrante = i.id_integrante
       left join IPJ.T_Legajos L
         on fcv.id_legajo = L.id_legajo
     where
       fcv.id_fondo_comercio = p_id_fondo_comercio and
       fcv.fec_baja is not null and
       fcv.borrador = 'N'
     order by fcv.fec_baja desc;

  END SP_Hist_Fondo_Vendedores;


  PROCEDURE SP_GUARDAR_FONDO_COMERCIO(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_fondo_comercio out number,
    p_id_tramite_ipj in number,
    p_id_tramiteipj_accion in number,
    p_ID_FONDO_COMERCIO in number,
    p_N_FONDO_COMERCIO in varchar2,
    p_FEC_INSCRIPCION in varchar2,
    p_FOLIO in varchar2,
    p_TOMO in varchar2,
    p_ANO in number,
    p_PROTOCOLO in varchar2,
    p_MATRICULA in varchar2,
    p_MATRICULA_VERSION in number,
    p_ID_VIN in number,
    p_OBJETO_SOCIAL in varchar2,
    p_letra in varchar2
  )
  IS
    v_mensaje varchar2(200);
    v_valid_parametros varchar2(500);
    v_matricula_exists varchar2(500);
    v_id_tramiteipj_accion number;
    v_ID_FONDO_COMERCIO number;
    v_id_vin number;
    v_TIPO_DOM  NUMBER;
    v_N_TIPO_DOM  VARCHAR2(150);
  BEGIN
  /********************************************************
    Guarda los datos de un Fondo de Comercio.
    Si el fondo ya existe, los actualiza, en caso contrario los ingresa
    No maneja transaccion, ya que se usa en cascada desde la aplicacion y lo administra el codigo.
  *********************************************************/
    --  VALIDACION DE PARAMETROS
    if p_FEC_INSCRIPCION is not null and IPJ.VARIOS.Valida_Fecha(p_FEC_INSCRIPCION) = 'N' or To_Date(p_FEC_INSCRIPCION, 'dd/mm/rrrr') > sysdate then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT')  || ' (Fecha Inscripci�n)';
    end if;
    if IPJ.VARIOS.TONUMBER(p_ano) is null then
      v_valid_parametros := v_valid_parametros || 'A�o Invalido.';
    end if;
    if p_matricula is not null then
        IPJ.VARIOS.SP_Validar_Matricula(
         o_rdo => v_matricula_exists,
         o_tipo_mensaje => o_tipo_mensaje,
         p_id_tramite_ipj => p_id_tramite_ipj,
         p_id_legajo => null,
         p_id_tramiteipj_accion => p_id_tramiteipj_accion,
         p_cuit => null,
         p_matricula => p_letra || p_matricula);

       if v_matricula_exists != IPJ.TYPES.C_RESP_OK then
         v_valid_parametros :=  v_valid_parametros || v_matricula_exists;
       end if;
    end if;

    -- Si los paramentros no estan OK, no continuo
    if v_valid_parametros is not null then
      o_rdo := 'Parametros : ' || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    --  CUERPO DEL PROCEDIMIETNO
    -- Actualizo el registro (si existe)
    v_id_fondo_comercio := p_id_fondo_comercio;

    update ipj.t_fondos_comercio
    set
      N_FONDO_COMERCIO = p_N_FONDO_COMERCIO,
      fec_inscripcion = To_Date(p_FEC_INSCRIPCION, 'dd/mm/rrrr'),
      matricula = p_letra || p_MATRICULA,
      folio = p_FOLIO,
      tomo = p_TOMO,
      a�o = p_ANO,
      protocolo = p_PROTOCOLO,
      id_vin = p_id_vin,
      objeto_social = p_objeto_social
    where
      id_fondo_comercio = v_id_fondo_comercio;

    -- Si no se actualizo nada, lo inserto.
    -- NO DEBERIA OCURRIR, VER SI INSERTO O MUESTRO ERROR
    if sql%rowcount = 0 then
      SELECT IPJ.SEQ_FONDO_COMERCIO.nextval INTO v_ID_FONDO_COMERCIO FROM dual;

      insert into ipj.t_fondos_comercio
         (ID_FONDO_COMERCIO, N_FONDO_COMERCIO, FEC_INSCRIPCION ,
         FOLIO, TOMO, A�O, PROTOCOLO, MATRICULA, MATRICULA_VERSION,
         ID_VIN, OBJETO_SOCIAL)
      values
        ( v_ID_FONDO_COMERCIO, p_N_FONDO_COMERCIO, to_date(p_FEC_INSCRIPCION, 'dd/mm/rrrr'),
         p_FOLIO, p_TOMO, p_ANO, p_PROTOCOLO, p_letra || p_MATRICULA, 0,
         p_ID_VIN, p_OBJETO_SOCIAL);

      --si es un nuevo fondo de comercio, actualizo su ID en la accion
      --Una vez creada la entidad, marco la accion como EN PROCESO, y le asigno el id fondo
      -- por las dudas sea uno nuevo
      update ipj.t_tramitesipj_acciones
      set id_estado =3, id_fondo_comercio = v_ID_FONDO_COMERCIO
      where
        id_tramite_ipj = p_id_tramite_ipj and
        id_tramiteipj_accion = p_id_tramiteipj_accion;
    else
      --Si ya existia el fondo, marco la accion como EN PROCESO
      update ipj.t_tramitesipj_acciones
      set id_estado =3
      where
        id_tramite_ipj = p_id_tramite_ipj and
        id_tramiteipj_accion = p_id_tramiteipj_accion;
    end if;

    IPJ.VARIOS.SP_Actualizar_Protocolo(
      o_rdo => o_rdo,
      o_tipo_mensaje => o_tipo_mensaje,
      p_id_tramite_ipj => p_id_tramite_ipj,
      p_id_legajo => null,
      p_id_tramiteipj_accion => p_id_tramiteipj_accion ,
      p_matricula => p_letra || p_matricula);

    if o_rdo != IPJ.TYPES.C_RESP_OK then
      o_rdo := 'SP_Actualizar_Protocolo ' || o_rdo;
      return;
    end if;

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    o_id_fondo_comercio := v_id_fondo_comercio;
  EXCEPTION
     WHEN OTHERS THEN
       o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_GUARDAR_FONDO_COMERCIO;


  PROCEDURE SP_GUARDAR_FONDO_RUBROS(
     o_rdo out varchar2,
     o_tipo_mensaje out number,
     p_id_tramite_ipj in number,
     p_id_tramiteipj_accion in number,
     p_id_tipo_accion in number,
     p_id_fondo_comercio in number,
     p_id_rubro in varchar2,
     p_id_tipo_actividad in varchar2,
     p_id_actividad in varchar2,
     p_fec_inicio in varchar2,
     p_fec_vencimiento in varchar2,
     p_eliminar in number) -- 1 elimina
  IS
    v_actividad_existente varchar2(20) := null;
    v_id_actividad varchar2(20) := null;
    v_id_rubro varchar2(3) := null;
    v_id_tipo_actividad varchar2(3) := null;
    v_valid_parametros varchar2(200);
    v_bloque varchar2(100);
    v_cuit varchar2(11);
  BEGIN
  /***********************************************************
    Guarda los rubros y actividades asociados a un fondo de comercio de un tramite
    Si se utiliza solo Rubro y Tipo_Actividades, se guarda la primera actividad de la vista.
    Luego el programa se supone que no muestra ese nivel.
    Si lo utilizan, siempre viene por parametro y se utiliza normalmente.
    No maneja transaccion, ya que se usa en cascada desde la aplicacion y lo administra el codigo.
  ************************************************************/
    -- VALIDACION DE PARAMETROS
    v_bloque := 'GUARDAR RUBROS - Par�metros: ';
    if p_id_actividad is null then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('RUBRO_NOT');
    end if;
    if IPJ.VARIOS.Valida_Fecha(p_fec_inicio) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT');
    end if;
    if p_fec_vencimiento is not null and IPJ.VARIOS.Valida_Fecha(p_fec_vencimiento) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT');
    end if;

    -- si los parametros no estan OK, no continuo
    if v_valid_parametros is not null then
      o_rdo := v_bloque || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    -- CUERPO DEL PROCEDIMIENTO
    if p_eliminar = 1 then
      delete ipj.t_fondos_comercio_rubro
      where
        id_tramite_ipj = p_id_tramite_ipj and
        id_tramiteipj_Accion = p_id_tramiteipj_accion and
        id_fondo_comercio = p_id_fondo_comercio and
        id_rubro = p_id_rubro and
        id_tipo_actividad = p_id_tipo_actividad and
        id_actividad = p_id_actividad and
        borrador = 'S';

    else
      v_bloque := '(Rubros/Actividades) ';
      -- Verifico si ya esta cargado o no.
      select nvl(max(id_actividad), '-1') into v_actividad_existente
      from ipj.t_fondos_comercio_rubro cr
      where
        cr.id_fondo_comercio = p_id_fondo_comercio and
        cr.id_rubro = p_id_rubro and
        cr.id_tipo_actividad = p_id_tipo_actividad and
        cr.id_actividad = p_id_actividad;

      -- Si solo viene e codigo de Actividad, busco el rubto y tipo asociados
      if p_id_actividad is not null  and p_id_rubro is null and p_id_tipo_actividad is null then
        select id_rubro into v_id_rubro
        from t_comunes.vt_actividades a
        where
          a.id_actividad = p_id_actividad;

        select id_tipo_actividad into v_id_tipo_actividad
        from t_comunes.vt_actividades a
        where
          a.id_actividad = p_id_actividad;
      else
        v_id_rubro := p_id_rubro;
        v_id_tipo_actividad :=   p_id_tipo_actividad;
      end if;

      -- si no viene actividad, busco la primera del tipo dado
      if p_id_actividad is null then
        select id_actividad into v_id_actividad
        from t_comunes.vt_actividades a
        where
          a.ID_RUBRO = p_id_rubro and
          a.ID_TIPO_ACTIVIDAD = p_id_tipo_actividad and
          rownum = 1;
      else
        v_id_actividad := p_id_actividad;
      end if;

      -- Actualizo la fecha de baja
      update ipj.t_fondos_comercio_rubro
      set
        fec_hasta = to_date(p_fec_vencimiento, 'dd/mm/rrrr')
      where
        id_tramite_ipj = p_id_tramite_ipj and
        id_tramiteipj_accion = p_id_tramiteipj_accion and
        id_fondo_comercio = p_id_fondo_comercio and
        id_rubro = v_id_rubro and
        id_tipo_actividad = v_id_tipo_actividad and
        id_actividad = nvl(p_id_actividad, v_actividad_existente);

      -- si no existe, lo agrego
      if sql%rowcount = 0 then
        insert into ipj.t_fondos_comercio_rubro
          (fec_desde, fec_hasta, id_rubro, id_tipo_actividad, id_actividad, ID_TRAMITE_IPJ, id_tramiteipj_accion,
          id_fondo_comercio, borrador)
        values
          (To_Date(p_fec_inicio, 'dd/mm/rrrr'), to_date(p_fec_vencimiento, 'dd/mm/rrrr'),
           v_id_rubro, v_id_tipo_actividad, nvl(p_id_actividad, v_id_actividad), p_id_tramite_ipj,
           p_id_tramiteipj_accion, p_id_fondo_comercio, 'S');
      end if;
    end if;

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
    WHEN OTHERS THEN
      o_rdo := v_bloque || To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_GUARDAR_FONDO_RUBROS;


  PROCEDURE SP_GUARDAR_FONDO_IL(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_ID_TRAMITE_IPJ in NUMBER,
    p_ID_TRAMITEIPJ_ACCION in NUMBER,
    p_ID_TIPO_ACCION in NUMBER,
    p_ID_FONDO_COMERCIO in NUMBER,
    p_ID_JUZGADO in NUMBER,
    p_IL_TIPO in NUMBER,
    p_IL_NUMERO in NUMBER,
    p_IL_FECHA in varchar2,
    p_TITULO in VARCHAR2,
    p_DESCRIPCION in VARCHAR2,
    p_FECHA in varchar2,
    p_eliminar in number)
  IS
    v_valid_parametros varchar2(500);
  BEGIN
    /*************************************************************
      Guarda los instrumentos legales asociados a un fondo de comercio de un tramite.
      No maneja transaccion, ya que se usa en cascada desde la aplicacion y lo administra el codigo.
  *************************************************************/
  -- VALIDACION DE PARAMETROS
    if IPJ.VARIOS.Valida_Fecha(p_il_fecha) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT' || '(Fecha Ins. Legal)');
    end if;
    if IPJ.VARIOS.Valida_Fecha(p_fecha) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT' || '(Fecha Insc.)');
    end if;
    if IPJ.VARIOS.Valida_Tipo_Ins_Legal(nvl(p_il_tipo, 0)) = 0 then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('INS_LEGAL_NOT');
    end if;

    if v_valid_parametros is not null then
      o_rdo := 'Parametros: '  || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    -- CUERPO PROCEDIMIENTO
    if p_eliminar = 1 then
      delete ipj.t_fondos_comercio_ins_legal
      where
        id_tramite_ipj = p_id_tramite_ipj and
        id_tramiteipj_accion = p_id_tramiteipj_accion and
        id_fondo_comercio = p_id_fondo_comercio and
        id_juzgado = p_id_juzgado and
        il_tipo = p_il_tipo and
        il_numero = p_il_numero and
        borrador = 'S';

    else
      -- Si existe un registro igual no hago nada
      update ipj.t_fondos_comercio_ins_legal
      set
         titulo = p_TITULO,
         descripcion = p_DESCRIPCION,
         fecha = to_date(p_FECHA, 'dd/mm/rrrr')
      where
        id_tramite_ipj = p_id_tramite_ipj and
        id_tramiteipj_accion = p_id_tramiteipj_accion and
        id_fondo_comercio = p_id_fondo_comercio and
        id_juzgado = p_id_juzgado and
        il_tipo = p_il_tipo and
        il_numero = p_il_numero;

      if sql%rowcount = 0 then
        insert into ipj.t_fondos_comercio_ins_legal
          ( ID_TRAMITE_IPJ, ID_TRAMITEIPJ_ACCION, ID_FONDO_COMERCIO, ID_JUZGADO,
            IL_TIPO, IL_NUMERO, IL_FECHA, TITULO, DESCRIPCION, BORRADOR, FECHA)
        values
          (p_ID_TRAMITE_IPJ, p_ID_TRAMITEIPJ_ACCION, p_ID_FONDO_COMERCIO, p_ID_JUZGADO,
            p_IL_TIPO, p_IL_NUMERO, to_date(p_IL_FECHA, 'dd/mm/rrrr'),
            p_TITULO, p_DESCRIPCION, 'S', to_date(p_FECHA, 'dd/mm/rrrr'));
      end if;
    end if;

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     WHEN OTHERS THEN
       o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_GUARDAR_FONDO_IL;


  PROCEDURE SP_GUARDAR_FONDO_VENDEDOR(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_ID_FONDO_COMERCIO_VENDEDOR out number,
    p_ID_FONDO_COMERCIO in number,
    p_ID_TRAMITE_IPJ in number,
    p_ID_TRAMITEIPJ_ACCION in number,
    p_TIPO_PERSONA  in number,
    p_ID_INTEGRANTE in number,
    p_id_sexo in varchar2,
    p_nro_documento in varchar2,
    p_pai_cod_pais in varchar2,
    p_id_numero in number,
    p_detalle in varchar2,
    p_nombre in varchar2,
    p_apellido in varchar2,
    p_error_Dato in varchar2,
    p_id_legajo in number,
    p_FEC_ALTA in varchar2,
    p_FEC_BAJA in varchar2,
    p_BORRADOR in varchar2,
    p_id_vin in number,
    p_ID_FONDO_COMERCIO_VENDEDOR in number,
    p_n_tipo_documento in varchar2,
    p_eliminar in number) -- 1 elimina
  IS
    v_valid_parametros varchar2(500);
    v_id_integrante number;
    v_id_legajo number;
    v_id_vin number;
    v_ID_FONDO_COMERCIO_VENDEDOR number;
  BEGIN
    /*************************************************************
      Guarda los vendedores asociados a un fondo de comercio de un tramite.
      No maneja transaccion, ya que se usa en cascada desde la aplicacion y lo administra el codigo.
  *************************************************************/
  -- VALIDACION DE PARAMETROS
    if IPJ.VARIOS.Valida_Fecha(p_FEC_ALTA) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT' || 'Fecha Inscipci�n)');
    end if;

    if p_FEC_BAJA is not null and IPJ.VARIOS.Valida_Fecha(p_FEC_BAJA) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT' || 'Fecha Baja)');
    end if;

    if v_valid_parametros is not null then
      o_rdo := 'Parametros: '  || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    -- CUERPO PROCEDIMIENTO
    if p_eliminar = 1 then
      -- SI ESA EN BORRADOR, LO ELIMINO; SINO LE ASIGNO FECHA DE BAJA
      if p_borrador = 'N' then
        update ipj.t_fondos_comercio_vendedor
        set fec_baja = to_date(sysdate, 'dd/mm/rrrr')
        where
          ID_FONDO_COMERCIO_VENDEDOR = p_ID_FONDO_COMERCIO_VENDEDOR;
      else
        delete ipj.t_fondos_comercio_vendedor
        where
          ID_FONDO_COMERCIO_VENDEDOR = p_ID_FONDO_COMERCIO_VENDEDOR;
      end if;
    else
      -- Si es una persona f�sica y no existe, la creo
      if p_tipo_persona = 2 and nvl(p_id_integrante, 0) = 0 then
        IPJ.ENTIDAD_PERSJUR.SP_GUARDAR_INTEGRANTES(
           o_rdo => o_rdo,
           o_tipo_mensaje => o_tipo_mensaje,
           o_id_integrante => v_id_integrante,
           p_id_sexo => p_id_sexo,
           p_nro_documento => p_nro_documento,
           p_pai_cod_pais => p_pai_cod_pais,
           p_id_numero => p_id_numero,
           p_cuil => null,
           p_detalle => p_detalle,
           p_Nombre => p_nombre,
           p_Apellido => p_apellido,
           p_error_dato => p_error_dato,
           p_n_tipo_documento => p_n_tipo_documento);

          if Upper(o_rdo) <> IPJ.TYPES.C_RESP_OK then
            return;
          end if;
      else
         if p_error_dato = 'S' then
           update ipj.t_integrantes
           set
             error_dato = p_error_dato,
             detalle = p_detalle
           where
             id_integrante = p_id_integrante;
         end if;
        v_id_integrante := p_id_integrante;
      end if;

       -- Si existe un registro igual , solo actualizo la fecha
      update ipj.t_fondos_comercio_vendedor
      set
         fec_alta = to_date(p_fec_alta, 'dd/mm/rrrr')
      where
        ID_FONDO_COMERCIO_VENDEDOR = p_ID_FONDO_COMERCIO_VENDEDOR;

      o_ID_FONDO_COMERCIO_VENDEDOR := p_ID_FONDO_COMERCIO_VENDEDOR;

      if sql%rowcount = 0 then
        SELECT IPJ.SEQ_FONDOS_VENDEDOR.nextval INTO v_ID_FONDO_COMERCIO_VENDEDOR FROM dual;

        -- si viene 0 como parametro, paso null por la FK.
        if v_id_integrante = 0 then
          v_id_integrante := null;
        end if;

        if nvl(p_id_legajo, 0) = 0 then
          v_id_legajo := null;
        else
          v_id_legajo := p_id_legajo;
        end if;

        insert into ipj.t_fondos_comercio_vendedor
          (ID_FONDO_COMERCIO, ID_TRAMITE_IPJ, ID_TRAMITEIPJ_ACCION, TIPO_PERSONA,
           ID_INTEGRANTE, FEC_ALTA, FEC_BAJA, BORRADOR, ID_FONDO_COMERCIO_VENDEDOR, id_legajo)
        values
          (p_ID_FONDO_COMERCIO, p_ID_TRAMITE_IPJ, p_ID_TRAMITEIPJ_ACCION, p_TIPO_PERSONA,
           v_ID_INTEGRANTE, to_date(p_FEC_ALTA, 'dd/mm/rrrr'), null, 'S', v_ID_FONDO_COMERCIO_VENDEDOR, v_id_legajo);

         o_ID_FONDO_COMERCIO_VENDEDOR := v_ID_FONDO_COMERCIO_VENDEDOR;
      end if;
    end if;

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     WHEN OTHERS THEN
       o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_GUARDAR_FONDO_VENDEDOR;


PROCEDURE SP_GUARDAR_FONDO_COMPRADOR(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_ID_FONDO_COMERCIO_COMPRADOR out number,
    p_ID_FONDO_COMERCIO in number,
    p_ID_TRAMITE_IPJ in number,
    p_ID_TRAMITEIPJ_ACCION in number,
    p_TIPO_PERSONA  in number,
    p_ID_INTEGRANTE in number,
    p_id_sexo in varchar2,
    p_nro_documento in varchar2,
    p_pai_cod_pais in varchar2,
    p_id_numero in number,
    p_detalle in varchar2,
    p_nombre in varchar2,
    p_apellido in varchar2,
    p_error_dato in varchar2,
    p_Id_Legajo in Number,
    p_FEC_ALTA in varchar2,
    p_FEC_BAJA in varchar2,
    p_BORRADOR in varchar2,
    p_id_vin in number,
    p_ID_FONDO_COMERCIO_COMPRADOR in number,
    p_n_tipo_documento in varchar2,
    p_eliminar in number) -- 1 elimina
  IS
    v_valid_parametros varchar2(500);
    v_id_integrante number;
    v_id_legajo number;
    v_id_vin number;
    v_ID_FONDO_COMERCIO_COMPRADOR number;
  BEGIN
    /*************************************************************
      Guarda los compradores asociados a un fondo de comercio de un tramite.
      Si es una persona fisica sin ID_INTEGRANTE, se crea y luego se asigna.
      Si es una persona fisica con direccion, se guarda.
      No maneja transaccion, ya que se usa en cascada desde la aplicacion y lo administra el codigo.
  *************************************************************/
  -- VALIDACION DE PARAMETROS
    if IPJ.VARIOS.Valida_Fecha(p_FEC_ALTA) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT' || 'Fecha Inscipci�n)');
    end if;

    if p_FEC_BAJA is not null and IPJ.VARIOS.Valida_Fecha(p_FEC_BAJA) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT' || 'Fecha Baja)');
    end if;

    if v_valid_parametros is not null then
      o_rdo := 'Parametros: '  || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    -- CUERPO PROCEDIMIENTO
    if p_eliminar = 1 then
      -- SI ESA EN BORRADOR, LO ELIMINO; SINO LE ASIGNO FECHA DE BAJA
      if p_borrador = 'N' then
        update ipj.t_fondos_comercio_comprador
        set fec_baja = to_date(sysdate, 'dd/mm/rrrr')
        where
          ID_FONDO_COMERCIO_COMPRADOR = p_ID_FONDO_COMERCIO_COMPRADOR;
      else
        delete ipj.t_fondos_comercio_comprador
        where
          ID_FONDO_COMERCIO_COMPRADOR = p_ID_FONDO_COMERCIO_COMPRADOR;
      end if;
    else
      -- Si es una persona f�sica y no existe, la creo
      if p_tipo_persona = 2 and nvl(p_id_integrante, 0) = 0 then
        IPJ.ENTIDAD_PERSJUR.SP_GUARDAR_INTEGRANTES(
           o_rdo => o_rdo,
           o_tipo_mensaje => o_tipo_mensaje,
           o_id_integrante => v_id_integrante,
           p_id_sexo => p_id_sexo,
           p_nro_documento => p_nro_documento,
           p_pai_cod_pais => p_pai_cod_pais,
           p_id_numero => p_id_numero,
           p_cuil => null,
           p_detalle => p_detalle,
           p_Nombre => p_nombre,
           p_Apellido => p_apellido,
           p_error_dato => p_error_dato,
           p_n_tipo_documento => p_n_tipo_documento);

          if Upper(o_rdo) <> IPJ.TYPES.C_RESP_OK then
            return;
          end if;
      else
        if p_error_dato = 'S' then
          update ipj.t_integrantes
          set
            error_dato = p_error_dato,
            detalle = p_detalle
          where
            id_integrante = p_id_integrante;
        end if;
        v_id_integrante := p_id_integrante;
      end if;

      -- Si existe un registro igual , solo actualizo la fecha
      update ipj.t_fondos_comercio_comprador
      set
         fec_alta = to_date(p_fec_alta, 'dd/mm/rrrr')
      where
        ID_FONDO_COMERCIO_COMPRADOR = p_ID_FONDO_COMERCIO_COMPRADOR;

      o_ID_FONDO_COMERCIO_COMPRADOR := p_ID_FONDO_COMERCIO_COMPRADOR;
      if sql%rowcount = 0 then
        SELECT IPJ.SEQ_FONDOS_COMPRADOR.nextval INTO v_ID_FONDO_COMERCIO_COMPRADOR FROM dual;

         -- si viene 0 como parametro, paso null por la FK.
        if v_id_integrante = 0 then
          v_id_integrante := null;
        end if;

        if p_id_legajo = 0 then
          v_id_legajo := null;
        else
          v_id_legajo := p_id_legajo;
        end if;

        insert into ipj.t_fondos_comercio_comprador
          (ID_FONDO_COMERCIO, ID_TRAMITE_IPJ, ID_TRAMITEIPJ_ACCION, TIPO_PERSONA,
           ID_INTEGRANTE, FEC_ALTA, FEC_BAJA, BORRADOR, ID_FONDO_COMERCIO_COMPRADOR, id_legajo)
        values
          (p_ID_FONDO_COMERCIO, p_ID_TRAMITE_IPJ, p_ID_TRAMITEIPJ_ACCION, p_TIPO_PERSONA,
           v_ID_INTEGRANTE, to_date(p_FEC_ALTA, 'dd/mm/rrrr'), null, 'S', v_ID_FONDO_COMERCIO_COMPRADOR, v_id_legajo);

        o_ID_FONDO_COMERCIO_COMPRADOR := v_ID_FONDO_COMERCIO_COMPRADOR;
      end if;
    end if;

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     WHEN OTHERS THEN
       o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_GUARDAR_FONDO_COMPRADOR;


  PROCEDURE SP_GUARDAR_FONDO_DOMICILIO(
    o_id_vin out number,
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_ID_FONDO_COMERCIO in number,
    p_id_vin in number,
    P_ID_PROVINCIA in varchar2,
    P_ID_DEPARTAMENTO in number,
    P_ID_LOCALIDAD in number,
    P_BARRIO in varchar2,
    P_CALLE in varchar2,
    P_ALTURA in number,
    P_DEPTO in varchar2,
    P_PISO in varchar2,
    p_torre in varchar2,
    p_manzana in varchar2,
    p_lote in varchar2)
  IS
  BEGIN
  /*************************************************************
    Inserta o Actualiza un domicilio para un Fondo de Comercio en DOM_MANAGER
    No maneja transaccion, ya lo hace DOM_MANAGER
  *************************************************************/
    -- si viene algun dato, intento guardar la direccion, sino le asigno NULL y devuelvo 0
    if P_ID_DEPARTAMENTO <> 0 then
      IPJ.VARIOS.SP_GUARDAR_DOMICILIO(
        o_id_vin => o_id_vin,
        o_rdo => o_rdo,
        o_tipo_mensaje => o_tipo_mensaje,
        p_id_vin => p_id_vin,
        p_usuario => 'USR_IPJ',
        p_Tipo_Domicilio => 0, -- 3 Real //  0 Sin Asignar
        p_id_integrante => 0,
        p_id_legajo => 0,
        p_cuit_empresa => null,
        p_id_fondo_comercio => p_ID_FONDO_COMERCIO,
        p_id_entidad_acta => 0,
        p_es_comerciante => 'N',
        p_es_admin_entidad => 'N',
        P_ID_PROVINCIA => p_id_provincia,
        P_ID_DEPARTAMENTO => p_id_departamento,
        P_ID_LOCALIDAD => p_id_localidad,
        P_BARRIO => p_barrio,
        P_CALLE => p_calle,
        P_ALTURA => p_altura,
        P_DEPTO => p_depto,
        P_PISO => p_piso,
        p_torre => p_torre,
        p_manzana => p_manzana,
        p_lote => p_lote,
        p_id_barrio => null,
        p_id_calle => null,
        p_id_tipocalle => 9,
        p_km => null
      );

      if o_rdo <> IPJ.TYPES.C_RESP_OK then
          return;
      end if;

    else
      o_id_vin := null;
      o_rdo := IPJ.TYPES.C_RESP_OK;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;

    end if;

    update IPJ.t_fondos_comercio
    set id_vin = o_id_vin
    where id_fondo_comercio = p_ID_FONDO_COMERCIO;

  EXCEPTION
     WHEN OTHERS THEN
        o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;

  END SP_GUARDAR_FONDO_DOMICILIO;

  PROCEDURE SP_GUARDAR_FONDO_COMPVEND_DOM(
    o_id_vin out number,
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_ID_FONDO_COMERCIO in number,
    p_ID_FONDO_COMERCIO_COMPRADOR in number,
    p_ID_FONDO_COMERCIO_VENDEDOR in number,
    p_id_vin in number,
    P_ID_PROVINCIA in varchar2,
    P_ID_DEPARTAMENTO in number,
    P_ID_LOCALIDAD in number,
    P_BARRIO in varchar2,
    P_CALLE in varchar2,
    P_ALTURA in number,
    P_DEPTO in varchar2,
    P_PISO in varchar2,
    p_torre in varchar2,
    p_manzana in varchar2,
    p_lote in varchar2)
  IS
    v_id_integrante number(10);
  BEGIN
  /*************************************************************
    Inserta o Actualiza un domicilio para un Comprador o Vendedor de un Fondo de Comercio en DOM_MANAGER
    No maneja transaccion, ya lo hace DOM_MANAGER
  *************************************************************/
    if nvl(p_ID_FONDO_COMERCIO_COMPRADOR, 0) > 0 then
      select id_integrante into v_id_integrante
      from IPJ.t_fondos_comercio_comprador
      where
        id_fondo_comercio = p_ID_FONDO_COMERCIO and
        ID_FONDO_COMERCIO_COMPRADOR = p_ID_FONDO_COMERCIO_COMPRADOR;
    else
      select id_integrante into v_id_integrante
      from IPJ.t_fondos_comercio_vendedor
      where
        id_fondo_comercio = p_ID_FONDO_COMERCIO and
        ID_FONDO_COMERCIO_VENDEDOR = p_ID_FONDO_COMERCIO_VENDEDOR;
    end if;

    -- si viene algun dato, intento guardar la direccion, sino le asigno NULL y devuelvo 0
    if v_id_integrante > 0 then
      if P_ID_DEPARTAMENTO <> 0 then
        IPJ.VARIOS.SP_GUARDAR_DOMICILIO(
          o_id_vin => o_id_vin,
          o_rdo => o_rdo,
          o_tipo_mensaje => o_tipo_mensaje,
          p_id_vin => p_id_vin,
          p_usuario => 'USR_IPJ',
          p_Tipo_Domicilio => 0, -- 3 Real //  0 Sin Asignar
          p_id_integrante => v_id_integrante,
          p_id_legajo => 0,
          p_cuit_empresa => null,
          p_id_fondo_comercio => 0,
          p_id_entidad_acta => 0,
          p_es_comerciante => 'N',
          p_es_admin_entidad => 'N',
          P_ID_PROVINCIA => p_id_provincia,
          P_ID_DEPARTAMENTO => p_id_departamento,
          P_ID_LOCALIDAD => p_id_localidad,
          P_BARRIO => p_barrio,
          P_CALLE => p_calle,
          P_ALTURA => p_altura,
          P_DEPTO => p_depto,
          P_PISO => p_piso,
          p_torre => p_torre,
          p_manzana => p_manzana,
          p_lote => p_lote,
          p_id_barrio => null,
          p_id_calle => null,
          p_id_tipocalle => 9,
          p_km => null
        );

        if o_rdo <> IPJ.TYPES.C_RESP_OK then
          return;
        end if;

      else
        o_id_vin := null;
        o_rdo := IPJ.TYPES.C_RESP_OK;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
      end if;

      update IPJ.t_integrantes
      set id_vin = o_id_vin
      where
        id_integrante = v_id_integrante;

    else
      o_id_vin := null;
      o_rdo := IPJ.TYPES.C_RESP_OK;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    end if;
  EXCEPTION
     WHEN OTHERS THEN
        o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;

  END SP_GUARDAR_FONDO_COMPVEND_DOM;

END FONDO_COMERCIO;
/

