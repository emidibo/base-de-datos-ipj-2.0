create or replace package ipj.Entidad_PersJur is

  -- Author  : D24385600
  -- Created : 07/08/2014 11:56:28 a.m.
  -- Purpose : Aministrar Entidades de Personas Juridicas
  FUNCTION FC_Cuit_AFip(p_id_legajo in number) return number;
  FUNCTION FC_Fecha_Orig_Admin(p_id_legajo IN number, p_id_admin in number) return varchar2;
  FUNCTION FC_Fecha_Orig_Sindico(p_id_legajo IN number, p_id_entidad_sindico in number) return varchar2;
  FUNCTION FC_Buscar_Ult_Dom(p_id_legajo in number, p_Tram_Abierto in char) return number;
  FUNCTION FC_Buscar_Ult_Tram(p_id_legajo in number, p_Tram_Abierto in char) return number;
  FUNCTION FC_FORMATEAR_CADENA_COMP(v_cadena IN varchar2) return varchar2;
  FUNCTION FC_Balances_Pendientes(p_id_legajo IN number, p_id_tramite_ipj in number) return varchar2;
  FUNCTION FC_ESTADO_GRAVAMEN_PERSJUR(p_id_legajo IN number) return varchar2;
  FUNCTION FC_ESTADO_GRAVAMEN_INTERV(p_id_legajo IN number) return varchar2;
  FUNCTION FC_DIRECCION_ENTIDAD (p_cuit IN varchar2) return number;
  FUNCTION FC_LISTAR_ACTA_ARCH_PEND(p_id_entidad_acta in number, p_id_tipo_acta in number) return varchar2;
  FUNCTION FC_LISTAR_ACTA_ORDEN_DIA(p_id_entidad_acta in number) return varchar2;
  FUNCTION FC_LISTAR_SOCIOS_COPROP(p_id_entidad_socio in number, p_id_tramite_ipj in number) return varchar2;
  FUNCTION FC_LISTAR_SOCIOS_USUF(p_id_entidad_socio in number, p_Id_Tramite_Ipj in number) return varchar2;
  FUNCTION FC_Fecha_Vigencia_Entidad(p_id_legajo number, p_matricula_version number, p_tipo_vigencia number) return varchar2;
  FUNCTION FC_Ult_Tram_Admin(p_id_legajo IN number, p_id_ubicacion_ent in number,
    p_matricula_version in number, p_Id_Tramite_Ipj in number) return number;
  FUNCTION FC_Ult_Tram_Sindico(p_id_legajo IN number, p_id_ubicacion_ent in number,
    p_matricula_version in number, p_Id_Tramite_Ipj in number) return number;
  FUNCTION FC_Ult_Tram_Repres(p_id_legajo IN number, p_id_ubicacion_ent in number,
    p_matricula_version in number, p_Id_Tramite_Ipj in number) return number;
  FUNCTION FC_Buscar_Prim_Acta(p_id_tramite_ipj in number) return date;
  FUNCTION FC_Buscar_Socio_Matr(p_id_tramite_ipj in number, p_id_entidad_socio in number) return varchar2;
  FUNCTION FC_Buscar_Socio_Matr_Vers(p_id_tramite_ipj in number, p_id_entidad_socio in number) return Number;
  FUNCTION FC_Buscar_Socio_Folio(p_id_tramite_ipj in number, p_id_entidad_socio in number) return varchar2;
  FUNCTION FC_Buscar_Socio_Anio(p_id_tramite_ipj in number, p_id_entidad_socio in number) return Number;
  FUNCTION FC_Validar_Equidad_Genero(p_legajo IN NUMBER, p_tramite_ipj IN NUMBER) RETURN VARCHAR2;

  PROCEDURE SP_Validar_Equidad_Genero(o_rdo            OUT VARCHAR2
                                     ,o_tipo_mensaje   OUT NUMBER
                                     ,p_id_legajo      IN NUMBER);

  PROCEDURE SP_Actualizar_Borrador(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in number,
    p_id_legajo in number,
    p_borrador in char
  );

  PROCEDURE SP_Buscar_CUIT_Tramite(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_cuit out varchar2,
    o_denominacion out varchar2,
    p_id_tramite_ipj in number,
    p_id_tramiteipj_accion in number);

  PROCEDURE SP_Buscar_Ult_Tramite(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_tramite_base out number,
    o_id_legajo_base out number,
    o_ubicacion_ent out number,
    p_id_legajo in number,
    p_id_ubicacion in number);

  PROCEDURE SP_Traer_Pers_Juridica(
      p_id_tramite_ipj in number,
      p_id_tramiteipj_accion in number,
      p_id_legajo in number,
      p_id_sede in varchar2 := '00',
      p_Cursor OUT TYPES.cursorType,
      o_rdo out varchar2);

  PROCEDURE SP_Traer_PersJur_Actas(
    p_id_tramite_ipj in number,
    p_id_legajo in number,
    p_Cursor OUT TYPES.cursorType);

  PROCEDURE SP_Traer_PersJur_InstLegal(
    p_id_tramite_ipj in number,
    p_id_legajo in number,
    p_Cursor OUT TYPES.cursorType);

  PROCEDURE SP_Traer_PersJur_Int_Admin(
    p_id_tramite_ipj in number,
    p_id_legajo in number,
    p_Cursor OUT TYPES.cursorType);

  PROCEDURE SP_Traer_PersJur_Socios_Inf(
    o_Cursor OUT TYPES.cursorType,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number);

  PROCEDURE SP_Traer_PersJur_Admin_Inf(
    p_id_tramite_ipj in number,
    p_id_legajo in number,
    p_Cursor OUT TYPES.cursorType);

  PROCEDURE SP_Traer_PersJur_Int_Socios(
    p_id_tramite_ipj in number,
    p_id_legajo in number,
    p_Cursor OUT TYPES.cursorType);

  PROCEDURE SP_Traer_PersJur_Rubros(
      p_id_tramite_ipj in number,
      p_id_legajo in number,
      p_Cursor OUT TYPES.cursorType);

  PROCEDURE SP_Traer_PersJur_Domicilio(
      p_id_tramite_ipj in number,
      p_id_legajo in number,
      p_id_sede varchar2 := '00',
      p_Cursor OUT TYPES.cursorType);

  PROCEDURE SP_TRAER_SEDES(
    p_cuil in varchar2,
    p_Cursor OUT TYPES.cursorType);

  PROCEDURE SP_GUARDAR_ENTIDAD_ACTAS(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_entidad_acta out number,
    p_id_entidad_acta in number,
    p_id_tramite_ipj in number,
    p_id_legajo in number,
    p_id_tipo_acta in number,
    p_fec_acta in varchar2,
    p_observacion in varchar2,
    p_id_vin in number,
    p_en_sede in number, --1= en la sede (graba id_vin en 0), 0 = domicilio particular
    p_art_reformados in varchar2,
    p_eliminar in number,
    p_es_unanime in char,
    p_es_autoconvocada in char,
    p_es_en_sede in char,
    p_fecha_convocatoria in varchar2, -- Es fecha
    p_fecha_acta_directorio in varchar2, -- Es fecha
    p_fecha_libro_asamblea in varchar2, -- Es fecha
    p_publicado_boe in char,
    p_fecha_primera_boe in varchar2, -- Es fecha
    p_fecha_ultima_boe in varchar2, -- Es fecha
    p_publicado_diario in char,
    p_fecha_primera_diario in varchar2, -- Es fecha
    p_fecha_ultima_diario in varchar2, -- Es fecha
    p_porc_capital_social in varchar,
    p_quorum_convocatoria1 in varchar,
    p_quorum_convocatoria2 in varchar,
    p_fecha_cuarto_inter in varchar2, -- Es fecha
    p_hora_Acta in varchar2 -- Hora formato HH24:Mi
  );

  PROCEDURE SP_GUARDAR_ENTIDAD_INS_LEGAL(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_entidad_ins_legal out number,
    p_id_tramite_ipj in number,
    p_id_legajo in number,
    p_id_juzgado in number,
    p_il_numero in number,
    p_il_fecha in varchar2,
    p_il_tipo in number,
    p_titulo in varchar2,
    p_descripcion in varchar2,
    p_fecha_acto in varchar2,
    p_eliminar in number,-- 1 elimina
    p_id_entidad_ins_legal in number
  );

  PROCEDURE SP_GUARDAR_ENTIDADES(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in number,
    p_id_legajo in number,
    p_cuit in varchar2,
    p_desc_obj_social in clob,
    p_monto in varchar2,
    p_acta_constitutiva in varchar2,
    p_vigencia in number,
    p_id_moneda in varchar2,
    p_cierre_ejercicio in varchar2,
    p_id_unidad_med in varchar2,
    p_fec_vig in varchar2,
    p_id_vin_comercial in number,
    p_id_vin_real in number,
    p_matricula in varchar2,
    p_fecha_inscripcion in varchar2,
    p_folio in varchar2,
    p_libro_nro in number,
    p_tomo in varchar2,
    p_anio in number,
    p_borrador in varchar2,
    p_denominacion_1 in varchar2,
    p_n_Sede in varchar2,
    p_id_sede in varchar2,
    p_alta_temporal in varchar2,
    p_tipo_vigencia in number,
    p_valor_cuota in varchar2,
    p_cuotas in number,
    p_matricula_version in number,
    p_id_tipo_admin in number,
    p_id_tipo_entidad in number,
    p_baja_temporal in varchar2,
    p_id_estado_entidad in varchar2,
    p_obs_cierre in varchar2,
    p_Nro_Resolucion in varchar2,
    p_Fec_Resolucion in varchar2,
    p_Nro_Boletin in Number,
    p_Fec_Boletin in varchar2,
    p_Nro_Registro in varchar2,
    p_Sede_Estatuto in Varchar2,
    p_Requiere_Sindico in Varchar2,
    p_Origen in Number,
    p_Es_Sucursal in Number,
    p_Observacion in Varchar2,
    p_letra in varchar2,
    p_Id_Tipo_Origen in number,
    p_cierre_fin_mes in char,
    p_fiscalizacion_ejerc in char,
    p_incluida_lgs in char,
    p_condicion_fideic in varchar2,
    p_obs_fiduciario in varchar2,
    p_obs_fiduciante in varchar2,
    p_obs_beneficiario in varchar2,
    p_obs_fideicomisario in varchar2,
    p_fecha_versionado in varchar2,
    p_sin_denominacion in char,
    p_contrato_privado in char,
    p_cant_Fiduciario in number,
    p_cant_Fiduciante in number,
    p_cant_Beneficiario in number,
    p_cant_Fideicomisario in number,
    p_fec_vig_hasta in varchar2, -- Es Fecha
    p_acredita_grupo IN VARCHAR2,
    p_fec_estatuto in varchar2,
    p_gubernamental in char
  );

  PROCEDURE SP_GUARDAR_ENTIDAD_SOCIOS(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_entidad_socio out number,
    p_id_tramite_ipj in number,
    p_id_legajo in number,
    p_id_tipo_integrante in number,
    p_cuota in number,
    p_fecha_inicio in varchar2, -- es fecha
    p_fecha_fin in varchar2,  -- es fecha
    p_id_integrante in number,
    p_id_sexo in varchar2,
    p_nro_documento in varchar2,
    p_pai_cod_pais in varchar2,
    p_id_numero in number,
    p_cuil in varchar2,
    p_detalle in varchar2,
    p_nombre in varchar2,
    p_apellido in varchar2,
    p_error_dato in varchar2,
    p_eliminar in number, -- 1 Elimina
    p_n_tipo_documento in varchar2,
    p_id_entidad_socio in number,
    p_id_legajo_socio in number,
    p_cuit_empresa in varchar2,
    p_n_empresa in varchar2,
    p_cuota_compartida in varchar2,
    p_id_tramite_ipj_entidad in number,
    p_porc_capital in varchar2,
    p_id_tipo_socio in number,
    p_en_formacion in char,
    p_matricula in varchar2,
    p_fec_acta in varchar2, --es fecha
    p_id_tipo_entidad in number,
    p_certificacion_contable in char,
    p_matricula_certificante in varchar2,
    p_folio in varchar2,
    p_anio in varchar2,
    p_persona_expuesta in char
  );

  PROCEDURE SP_GUARDAR_ENTIDAD_ADMIN(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_integrante out number,
    o_id_admin out number,
    p_id_tramite_ipj in number,
    p_id_legajo in number,
    p_id_tipo_integrante in number,
    p_fecha_inicio in varchar2,
    p_fecha_fin in varchar2,
    p_id_integrante in number,
    p_id_sexo in varchar2,
    p_nro_documento in varchar2,
    p_pai_cod_pais in varchar2,
    p_id_numero in number,
    p_cuil in varchar2,
    p_detalle in varchar2,
    p_nombre in varchar2,
    p_apellido in varchar2,
    p_Id_Vin in number,
    p_Id_Tipo_Organismo in number,
    p_error_dato in varchar2,
    p_eliminar in number, -- 1 eliminar
    p_n_tipo_documento in varchar2,
    p_id_tramite_ipj_entidad in number,
    p_id_entidades_accion in number,
    p_id_motivo_baja in number,
    p_porc_garantia in varchar2,
    p_monto_garantia in varchar2,
    p_habilitado_present in char,
    p_persona_expuesta in char,
    p_id_moneda in varchar2,
    p_id_admin in number,
    p_cuit_empresa in varchar2,
    p_n_empresa in varchar2,
    p_matricula in varchar2,
    p_fec_acta in varchar2, -- es fecha
    p_id_legajo_empresa in number,
    p_folio in varchar2,
    p_anio in number,
    p_Id_Tipo_Entidad in NUMBER,
    p_es_admin_afip in varchar2
    );

  PROCEDURE SP_GUARDAR_ENTIDAD_RUBROS(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in number,
    p_id_legajo in number,
    p_id_rubro in varchar2,
    p_id_tipo_actividad in varchar2,
    p_id_actividad in varchar2,
    p_fecha_inicio in varchar2,
    p_fecha_vencimiento in varchar2,
    p_eliminar in number);

  PROCEDURE SP_GUARDAR_INTEGRANTES(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_integrante out number,
    o_pai_cod_pais out varchar2,
    p_id_sexo in varchar2,
    p_nro_documento in varchar2,
    p_pai_cod_pais in varchar2,
    p_id_numero in number,
    p_cuil in varchar2,
    p_detalle in varchar2,
    p_Nombre in varchar2,
    p_Apellido in varchar2,
    p_error_dato in varchar2,
    p_n_tipo_documento in varchar2,
    p_fec_nac in varchar2,
    p_id_sintys in number
  );

  PROCEDURE SP_CREAR_ENTIDAD_TRAMITE(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_legajo in number,
    p_id_sede in varchar2,
    p_id_tramite_ipj in number,
    p_id_tramiteipj_accion in number);

  PROCEDURE SP_ACTUALIZAR_PERS_JURIDICA(
    o_rdo out varchar2,
    o_metodo out varchar2,
    o_origen out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in number,
    p_id_legajo in number);


  PROCEDURE SP_Buscar_Personas_RCivil(
    p_id_sexo in varchar2,
    p_nro_documento in varchar2,
     p_Cursor OUT TYPES.cursorType);


  PROCEDURE SP_GUARDAR_ENTIDAD_DOMICILIO(
    o_id_vin out number,
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in number,
    p_id_legajo in number,
    p_id_sede in varchar2,
    p_id_vin_real in number,
    P_ID_PROVINCIA in varchar2,
    P_ID_DEPARTAMENTO in number,
    P_ID_LOCALIDAD in number,
    P_BARRIO in varchar2,
    P_CALLE in varchar2,
    P_ALTURA in number,
    P_DEPTO in varchar2,
    P_PISO in varchar2,
    p_torre in varchar2,
    p_manzana in varchar2,
    p_lote in varchar2,
    p_id_tipocalle in number,
    p_km in varchar,
    p_id_calle in number,
    p_id_barrio in number
  );

  PROCEDURE SP_GUARDAR_ENTIDAD_ADMIN_DOM(
    o_id_vin out number,
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in number,
    p_id_legajo in number,
    p_id_tipo_integrante in number,
    p_id_integrante in number,
    p_fecha_inicio in varchar2,
    p_id_vin in number,
    P_ID_PROVINCIA in varchar2,
    P_ID_DEPARTAMENTO in number,
    P_ID_LOCALIDAD in number,
    P_BARRIO in varchar2,
    P_CALLE in varchar2,
    P_ALTURA in number,
    P_DEPTO in varchar2,
    P_PISO in varchar2,
    p_torre in varchar2,
    p_manzana in varchar2,
    p_lote in varchar2,
    p_id_admin in number,
    p_cuil_empresa in varchar2,
    p_id_tipocalle in number,
    p_km in varchar2,
    p_id_calle in number,
    p_id_barrio in number);

 PROCEDURE SP_Traer_PersJur_Rubricas(
      p_id_tramite_ipj in number,
      p_id_legajo in number,
      p_Cursor OUT TYPES.cursorType);

  PROCEDURE SP_Traer_PersJur_Rub_Mecan(
      p_id_tramite_ipj in number,
      p_id_legajo in number,
      p_Cursor OUT TYPES.cursorType);

  PROCEDURE SP_Traer_PersJur_Aut_Mecan(
      p_Id_Tramite_Ipj in number,
      p_id_legajo in number,
      p_Cursor OUT TYPES.cursorType);

  PROCEDURE SP_Traer_PersJur_Rubricas_NO(
      p_id_tramite_ipj in number,
      p_id_legajo in number,
      p_Cursor OUT TYPES.cursorType);

   PROCEDURE SP_GUARDAR_ENTIDAD_RUBRICAS(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in number,
    p_id_legajo in number,
    p_id_tipo_libro in number,
    p_nro_libro in number,
    p_titulo in varchar2,
    p_fecha_tramite in varchar2, --es DATE
    p_Fs_Ps in varchar2,
    p_id_juzgado in number,
    p_sentencia in varchar2,
    p_fecha_sent in varchar2, --es DATE
    p_observacion in varchar2,
    p_eliminar in number, -- 1 eliminar
    p_nro_desde in number,
    p_nro_hasta in number,
    p_hojas_desde in number,
    p_hojas_hasta in number,
    p_es_autorizacion in char
  );

  PROCEDURE SP_Validar_PersJur_InstLegal(
    p_id_tramite_ipj in number,
    p_id_legajo in number,
    p_id_juzgado in number,
    p_il_numero in number,
    p_rdo out number);

   PROCEDURE SP_Validar_PersJur_Actas(
    p_id_tramite_ipj in number,
    p_id_legajo in number,
    p_id_Tipo_Acta in number,
    p_fec_acta in varchar2,
    p_rdo out number);

  PROCEDURE SP_Validar_PersJur_Admin(
    p_id_tramite_ipj in number,
    p_id_legajo in number,
    p_id_Tipo_Integrante in number,
    p_fecha_inicio in varchar2,
    p_id_sexo in varchar2,
    p_nro_documento in varchar2,
    p_pai_cod_pais in varchar2,
    p_rdo out number);

  PROCEDURE SP_Validar_PersJur_Socios(
    p_id_tramite_ipj in number,
    p_id_legajo in number,
    p_id_Tipo_Integrante in number,
    p_id_sexo in varchar2,
    p_nro_documento in varchar2,
    p_pai_cod_pais in varchar2,
    p_rdo out number);

 PROCEDURE SP_Validar_PersJur_Rubrica(
    p_id_tramite_ipj in number,
    p_id_legajo in number,
    p_id_Tipo_Libro in number,
    p_Nro_Libro in number,
    p_rdo out number);

 PROCEDURE SP_Validar_PersJur_Rubros(
    p_id_tramite_ipj in number,
    p_id_legajo in number,
    p_id_rubro in varchar2,
    p_id_tipo_actividad in varchar2,
    p_id_actividad in varchar2,
    p_rdo out number);

 PROCEDURE SP_Hist_PersJur_Rubros(
      o_Cursor OUT TYPES.cursorType,
      p_id_tramite_ipj in number,
      p_id_legajo in number );

  PROCEDURE SP_Hist_PersJur_Int_Socios(
    o_Cursor OUT TYPES.cursorType,
    p_id_tramite_ipj in number,
    p_id_legajo in number);

  PROCEDURE SP_Hist_PersJur_Int_Socios_SxA(
    o_Cursor OUT TYPES.cursorType,
    p_id_tramite_ipj in number,
    p_id_legajo in number);

  PROCEDURE SP_Hist_PersJur_Int_Admin(
    o_Cursor OUT TYPES.cursorType,
    p_id_tramite_ipj in number,
    p_id_legajo in number);

  PROCEDURE SP_Hist_PersJur_Domicilio(
    o_Cursor OUT TYPES.cursorType,
    p_id_tramite_ipj in number,
    p_id_legajo in number);

  PROCEDURE SP_Hist_PersJur_Acciones(
      o_Cursor OUT TYPES.cursorType,
      p_Id_Tramite_Ipj in number,
      p_id_legajo in number);

  PROCEDURE SP_Hist_PersJur_Razon_Social(
    o_Cursor OUT TYPES.cursorType,
    p_id_tramite_ipj in number,
    p_id_legajo in number);

  PROCEDURE SP_Hist_PersJur_Vigencia(
    o_Cursor OUT TYPES.cursorType,
    p_id_tramite_ipj in number,
    p_id_legajo in number);

  PROCEDURE SP_GUARDAR_GRAVAMENES(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_ID_TRAMITE_IPJ in NUMBER,
    p_ID_TRAMITEIPJ_ACCION  in NUMBER,
    p_ID_TIPO_ACCION  in NUMBER,
    p_TIPO_PERSONA  in NUMBER,
    p_ID_INTEGRANTE  in NUMBER,
    p_id_legajo in NUMBER,
    p_FEC_INSCRIPCION in varchar2,
    p_MATRICULA  in VARCHAR2,
    p_MATRICULA_VERSION  in NUMBER,
    p_FOLIO in VARCHAR2,
    p_TOMO in VARCHAR2,
    p_ANIO in NUMBER,
    p_PROTOCOLO in VARCHAR2,
    p_letra in varchar2);

  PROCEDURE SP_Traer_Gravamenes(
      o_Cursor OUT TYPES.cursorType,
      o_rdo out varchar,
      p_id_tramite_ipj in number,
      p_id_tramiteipj_accion in number,
      p_id_tipo_accion in number );

  PROCEDURE SP_GUARDAR_GRAVAMEN_INS_LEGAL(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_ID_TRAMITE_IPJ in NUMBER,
    p_ID_TRAMITEIPJ_ACCION in NUMBER,
    p_ID_TIPO_ACCION in NUMBER,
    p_ID_JUZGADO in NUMBER,
    p_IL_TIPO in NUMBER,
    p_IL_NUMERO in NUMBER,
    p_ID_TIPO_GRAVAMEN in NUMBER,
    p_IL_FECHA in varchar2,
    p_TITULO in VARCHAR2,
    p_DESCRIPCION in VARCHAR2,
    p_FECHA in varchar2,
    p_eliminar in number);

  PROCEDURE SP_Traer_Gravamen_Ins_Legal(
    o_Cursor OUT TYPES.cursorType,
    p_id_tramite_ipj in number,
    p_Id_TramiteIPJ_Accion in number,
    p_id_tipo_accion in number);

   PROCEDURE SP_GUARDAR_INTEG_DOMICILIO(
     o_id_vin out number,
     o_rdo out varchar2,
     o_tipo_mensaje out number,
     p_id_tramite_ipj in number,
     p_id_tramiteipj_accion in number,
     p_id_tipo_Accion in number,
     p_id_integrante in number,
     p_id_vin_integrante in number,
     P_ID_PROVINCIA in varchar2,
     P_ID_DEPARTAMENTO in number,
     P_ID_LOCALIDAD in number,
     P_BARRIO in varchar2,
     P_CALLE in varchar2,
     P_ALTURA in number,
     P_DEPTO in varchar2,
     P_PISO in varchar2,
     p_torre in varchar2,
     p_manzana in varchar2,
     p_lote in varchar2
   );

   PROCEDURE SP_GUARDAR_ENTIDAD_RELACIONADA(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_entidad_relacion out number,
    p_id_tramite_ipj in number,
    p_id_legajo in number,
    p_id_tipo_relacion_empresa in number,
    p_id_legajo_relacionado in number,
    p_fecha_relacion in varchar2,
    p_eliminar in number, -- 1 elimina
    p_id_entidad_relacion in number,
    p_cuit_empresa in varchar2,
    p_n_empresa in varchar2,
    p_id_integrante in number,
    p_id_sexo in varchar2,
    p_nro_documento in varchar2,
    p_pai_cod_pais in varchar2,
    p_id_numero in number,
    p_cuil in varchar2,
    p_detalle in varchar2,
    p_nombre in varchar2,
    p_apellido in varchar2,
    p_error_dato in varchar2,
    p_n_tipo_documento in varchar2
    );

   PROCEDURE SP_Traer_PersJur_Relacionada(
     o_Cursor OUT TYPES.cursorType,
     p_id_tramite_ipj in number,
     p_id_legajo in number);

   PROCEDURE SP_Validar_PersJur_Relacion(
     o_rdo out number,
     p_id_tramite_ipj in number,
     p_id_legajo in number,
     p_id_tipo_relacion_empresa in number,
     p_id_legajo_relacionado in number);

   PROCEDURE SP_Traer_Consulta_Fonetica(
     o_cursor OUT TYPES.cursorType,
     p_Clave in varchar2,
     p_cuil_usuario in varchar2);

   PROCEDURE SP_Traer_Consulta_Textual(
    o_cursor OUT TYPES.cursorType,
    p_Clave in varchar2,
    p_cuil_usuario in varchar2);

  PROCEDURE SP_Traer_Consulta_Nombre(
    o_cursor OUT TYPES.cursorType,
    p_Clave in varchar2,
    p_cuil_usuario in varchar2);

  PROCEDURE SP_Traer_Consulta_Nombre_Comp(
    o_cursor OUT TYPES.cursorType,
    p_Clave in varchar2,
    p_id_ubicacion in number);

  PROCEDURE SP_Traer_PersJur_Actas_Orden(
    o_Cursor OUT TYPES.cursorType,
    p_ID_ENTIDAD_ACTA in number);

  PROCEDURE SP_Traer_Lista_Actas_Orden(
    o_Cursor OUT TYPES.cursorType,
    p_Lista_entidad_acta in varchar2);

  PROCEDURE SP_Traer_PersJur_Actas_Arch(
    o_Cursor OUT TYPES.cursorType,
    p_ID_ENTIDAD_ACTA in number,
    p_id_tipo_acta in number);

  PROCEDURE SP_Traer_PersJur_Balances(
    o_Cursor OUT TYPES.cursorType,
    p_id_tramite_ipj in number,
    p_id_legajo in number);

  PROCEDURE SP_Traer_PersJur_Notif(
    o_Cursor OUT TYPES.cursorType,
    p_id_tramite_ipj in number,
    p_id_legajo in number);

  PROCEDURE SP_Traer_PersJur_Sindicos(
    o_Cursor OUT TYPES.cursorType,
    p_id_tramite_ipj in number,
    p_id_legajo in number);

  PROCEDURE SP_Traer_PersJur_Represent(
    o_Cursor OUT TYPES.cursorType,
    p_id_tramite_ipj in number,
    p_id_legajo in number);

  PROCEDURE SP_GUARDAR_ENTIDAD_BALANCES(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in number,
    p_id_legajo in number,
    p_fecha in varchar2, -- es fecha
    p_monto in varchar2,
    p_reserva_legal in varchar2,
    p_borrador in varchar2,
    p_Activos in varchar2,
    p_Pasivos in varchar2,
    p_Neto in varchar2,
    p_Ingreso_Ejerc in varchar2,
    p_Costo_Ejerc in varchar2,
    p_Result_Ejerc in varchar2,
    p_Fecha_Certif in varchar2, -- es fecha
    p_matricula in varchar2,
    p_id_integrante in number,
    p_DETALLE in varchar2,
    p_Nombre in varchar2,
    p_Apellido in varchar2,
    p_NRO_DOCUMENTO in varchar2,
    p_ID_NUMERO in number,
    p_ID_SEXO in varchar2,
    p_ID_VIN in number,
    p_PAI_COD_PAIS in varchar2,
    p_CUIL in varchar2,
    p_error_dato in varchar2,
    p_n_tipo_documento in varchar2,
    p_eliminar in number, -- 1 elimina
    p_observacion in varchar2,
    p_id_tipo_dictamen in number,
    p_irregular in char);

  PROCEDURE SP_GUARDAR_ENTIDAD_NOTAS(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_nro out number,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number,
    p_nro in number,
    p_fec_modif in varchar2,
    p_id_estado_nota in number,
    p_observacion in varchar2,
    p_eliminar in number,
    p_es_nueva in char,
    p_id_nota_sistema in number
    );

  PROCEDURE SP_GUARDAR_ENTIDAD_SINDICO(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_entidad_sindico out number,
    p_id_entidad_sindico in number,
    p_id_tramite_ipj in number,
    p_id_legajo in number,
    p_fec_alta in varchar2,
    p_fec_baja in varchar2,
    p_matricula in varchar2,
    p_id_tipo_integrante in number,
    p_borrador in varchar2,
    p_id_integrante in number,
    p_DETALLE in varchar2,
    p_Nombre in varchar2,
    p_Apellido in varchar2,
    p_NRO_DOCUMENTO in varchar2,
    p_ID_NUMERO in number,
    p_ID_SEXO in varchar2,
    p_ID_VIN in number,
    p_PAI_COD_PAIS in varchar2,
    p_CUIL in varchar2,
    p_error_dato in varchar2,
    p_n_tipo_documento in varchar2,
    p_id_legajo_sindico in number,
    p_matricula_empresa in varchar2,
    p_id_tipo_entidad in number,
    p_cuit_empresa in varchar2,
    p_n_empresa in varchar2,
    p_id_entidades_accion in number,
    p_eliminar in number, -- 1 elimina
    p_id_tramite_ipj_entidad in number,
    p_id_motivo_baja in number,
    p_folio in varchar2,
    p_anio in varchar2,
    p_en_formacion in char,
    p_fec_acta in varchar2, -- Es Fecha
    p_persona_expuesta in char
    );

  PROCEDURE SP_GUARDAR_ENTIDAD_REPRES(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in number,
    p_id_legajo in number,
    p_fec_alta in varchar2,
    p_fec_baja in varchar2,
    p_borrador in varchar2,
    p_id_integrante in number,
    p_DETALLE in varchar2,
    p_Nombre in varchar2,
    p_Apellido in varchar2,
    p_NRO_DOCUMENTO in varchar2,
    p_ID_NUMERO in number,
    p_ID_SEXO in varchar2,
    p_ID_VIN in number,
    p_PAI_COD_PAIS in varchar2,
    p_CUIL in varchar2,
    p_error_dato in varchar2,
    p_n_tipo_documento in varchar2,
    p_eliminar in number, -- 1 elimina
    p_id_motivo_baja in number,
    p_id_tramite_ipj_entidad in number
  );

  PROCEDURE SP_Traer_PersJur_Veedores(
    o_Cursor OUT TYPES.cursorType,
    p_id_tramite_ipj in number,
    p_id_legajo in number);

  PROCEDURE SP_GUARDAR_ENTIDAD_VEEDOR(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in number,
    p_id_tramiteipj_accion in number,
    p_id_legajo in number,
    p_fecha_veed in varchar2,
    p_fec_alta in varchar2,
    p_fec_baja in varchar2,
    p_id_rol_veedor in number,
    p_id_integrante in number,
    p_id_sexo in varchar2,
    p_nro_documento in varchar2,
    p_pai_cod_pais in varchar2,
    p_id_numero in number,
    p_cuil in varchar2,
    p_detalle in varchar2,
    p_nombre in varchar2,
    p_apellido in varchar2,
    p_error_dato in varchar2,
    p_n_tipo_documento in varchar2,
    p_eliminar in number); -- 1 elimina

  PROCEDURE SP_Hist_PersJur_Represent(
    o_Cursor OUT TYPES.cursorType,
    p_id_tramite_ipj in number,
    p_id_legajo in number);

  PROCEDURE SP_Hist_PersJur_Sindicos(
    o_Cursor OUT TYPES.cursorType,
    p_id_tramite_ipj in number,
    p_id_legajo in number);

  PROCEDURE SP_Informar_Pers_Juridica(
      p_id_tramite_ipj in number,
      p_id_legajo in number,
      p_id_sede in varchar2 := '00',
      p_Cursor OUT TYPES.cursorType,
      o_rdo out varchar);

  PROCEDURE SP_Informar_Entidad_Soc_Usuf(
    o_Cursor OUT TYPES.cursorType,
    p_id_tramite_ipj in number,
    p_id_legajo in number);

  PROCEDURE SP_GUARDAR_ENTIDAD_ACTAS_ORDEN(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_entidad_acta in number,
    p_id_tipo_orden_dia in number,
    p_eliminar in number); -- 1 Elimina

  PROCEDURE SP_GUARDAR_ENTIDAD_ACTAS_ARCH(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_entidad_acta in number,
    p_id_archivo in number,
    p_id_tipo_acta in number,
    p_fecha in varchar2,
    p_id_integrante in number,
    p_id_sexo in varchar2,
    p_nro_documento in varchar2,
    p_pai_cod_pais in varchar2,
    p_id_numero in number,
    p_detalle in varchar2,
    p_nombre in varchar2,
    p_apellido in varchar2,
    p_error_Dato in varchar2,
    p_n_tipo_documento in varchar2,
    p_eliminar in number);-- 1 Elimina

  PROCEDURE SP_GUARDAR_ENTIDAD_ACTA_DOM(
    o_id_vin out number,
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in number,
    p_id_legajo in number,
    p_id_entidad_Acta in number,
    p_id_vin in number,
    P_ID_PROVINCIA in varchar2,
    P_ID_DEPARTAMENTO in number,
    P_ID_LOCALIDAD in number,
    P_BARRIO in varchar2,
    P_CALLE in varchar2,
    P_ALTURA in number,
    P_DEPTO in varchar2,
    P_PISO in varchar2,
    p_torre in varchar2,
    p_manzana in varchar2,
    p_lote in varchar2,
    p_id_tipocalle in number,
    p_km in varchar,
    p_id_calle in number,
    p_id_barrio in number);

  PROCEDURE SP_Traer_PersJur_Acta_Dom(
      p_id_tramite_ipj in number,
      p_id_legajo in number,
      p_id_entidad_Acta in number,
      p_Cursor OUT TYPES.cursorType);

  PROCEDURE SP_Traer_PersJur_Int_Admin_Dom(
      p_id_tramite_ipj in number,
      p_id_legajo in number,
      p_id_tipo_integrante in number,
      p_id_integrante in number,
      p_fecha_inicio in varchar2,
      p_Cursor OUT TYPES.cursorType);

  PROCEDURE SP_GENERAR_MATRICULA_FUNDACION(
    o_matricula out varchar2,
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_razon_social in varchar2);

  PROCEDURE SP_Traer_Entidad_Soc_Usuf(
    o_Cursor OUT TYPES.cursorType,
    p_id_entidad_socio in number);

  PROCEDURE SP_Traer_Entidad_Soc_Copro(
    o_Cursor OUT TYPES.cursorType,
    p_id_entidad_socio in number);


  PROCEDURE SP_Guardar_Entidad_Soc_Usuf(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_entidad_socio in number,
    p_fec_alta in varchar2,
    p_fec_baja in varchar2,
    p_id_integrante in number,
    p_id_sexo in varchar2,
    p_nro_documento in varchar2,
    p_pai_cod_pais in varchar2,
    p_id_numero in number,
    p_cuil in varchar2,
    p_detalle in varchar2,
    p_nombre in varchar2,
    p_apellido in varchar2,
    p_error_dato in varchar2,
    p_n_tipo_documento in varchar2,
    p_eliminar in number,  -- 1 Elimina
    p_cuota in number);

    PROCEDURE SP_Guardar_Entidad_Soc_Copro(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_entidad_socio in number,
    p_fec_alta in varchar2,
    p_fec_baja in varchar2,
    p_id_integrante in number,
    p_id_sexo in varchar2,
    p_nro_documento in varchar2,
    p_pai_cod_pais in varchar2,
    p_id_numero in number,
    p_cuil in varchar2,
    p_detalle in varchar2,
    p_nombre in varchar2,
    p_apellido in varchar2,
    p_error_dato in varchar2,
    p_n_tipo_documento in varchar2,
    p_eliminar in number); -- 1 Elimina

  PROCEDURE SP_Listar_Comis_Venc(
    o_Cursor OUT TYPES.cursorType,
    p_cuil_usuario in varchar2);

  PROCEDURE SP_Listar_Interv_Venc(
    o_Cursor OUT TYPES.cursorType,
    p_cuil_usuario in varchar2);

  PROCEDURE SP_Listar_Pers_Venc(
    o_Cursor OUT TYPES.cursorType,
    p_cuil_usuario in varchar2);

  PROCEDURE SP_Listar_Entidad_Irregular(
    o_Cursor OUT TYPES.cursorType,
    p_cuil_usuario in varchar2);

  PROCEDURE SP_GUARDAR_ENTIDAD_SOCIOS_HIST(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in number,
    p_id_legajo in number,
    p_id_tipo_integrante in number,
    p_cuota in number,
    p_fecha_inicio in varchar2,
    p_fecha_fin in varchar2,
    p_id_integrante in number,
    p_id_entidad_socio in number,
    p_id_legajo_socio in number,
    p_cuit_empresa in varchar2,
    p_n_empresa in varchar2,
    p_cuota_compartida in varchar2,
    p_porc_capital in varchar2);

  PROCEDURE SP_GUARDAR_ENTIDAD_ADMIN_HIST(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in number,
    p_id_legajo in number,
    p_id_admin in number,
    p_id_tipo_integrante in number,
    p_fecha_inicio in varchar2,
    p_fecha_fin in varchar2,
    p_id_integrante in number,
    p_Id_Vin in number,
    p_Id_Tipo_Organismo in number,
    p_id_motivo_baja in number,
    p_cuit_empresa in varchar2,
    p_n_empresa in varchar2,
    p_id_legajo_empresa in varchar2);

  PROCEDURE SP_GUARDAR_ENTIDAD_SIND_HIST(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_entidad_sindico in number,
    p_id_tramite_ipj in number,
    p_id_legajo in number,
    p_fec_alta in varchar2,
    p_fec_baja in varchar2,
    p_matricula in varchar2,
    p_id_tipo_integrante in number,
    p_borrador in varchar2,
    p_id_integrante in number,
    p_id_legajo_sindico in number,
    p_matricula_empresa in varchar2,
    p_id_tipo_entidad in number,
    p_cuit_empresa in varchar2,
    p_n_empresa in varchar2,
    p_id_entidades_accion in number,
    p_id_motivo_baja in number);

  PROCEDURE SP_Buscar_Entidad_Tramite(
    o_id_tramite_ipj out number,
    o_id_tramiteipj_accion out number,
    p_id_legajo in number );

  PROCEDURE SP_Buscar_Entidad_Tramite(
      o_id_tramite_ipj out number,
      o_id_tramiteipj_accion out number,
      p_id_legajo in number,
      p_Abiertos in char );

  PROCEDURE SP_Buscar_Entidad_Tramite_PROV(
      o_id_tramite_ipj out number,
      o_id_tramiteipj_accion out number,
      p_id_legajo in number,
      p_Abiertos in char );

  PROCEDURE SP_Buscar_Ent_Tramites(
    o_Cursor OUT TYPES.cursorType,
    p_id_legajo in number);

  PROCEDURE SP_Buscar_Ent_Tramites(
    o_Cursor OUT TYPES.cursorType,
    p_id_legajo in number,
    p_id_estados in varchar2);

  PROCEDURE SP_Buscar_Ent_Tramites_Pend(
    o_Cursor OUT TYPES.cursorType,
    p_id_legajo in number);

  PROCEDURE SP_Traer_PersJur_Organos(
    o_Cursor OUT TYPES.cursorType,
    p_id_tramite_ipj in number,
    p_id_legajo in number);

  PROCEDURE Sp_Guardar_Entidad_Organos(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in number,
    p_id_legajo in number,
    p_id_tipo_organismo in number,
    p_cant_meses in varchar2,
    p_id_tipo_duracion in number,
    p_min_titular in number,
    p_max_titular in number,
    p_suplente_igual in char,
    p_suplente_menor in char,
    p_suplente_mayor in char,
    p_Max_Suplente in number,
    p_Min_Suplente in number,
    p_observacion in varchar2,
    p_duracion_indefinida in char,
    p_id_tipo_administracion in number,
    p_periodos_reeleccion IN number
    );

  PROCEDURE SP_Traer_Entidad_Acciones(
    o_Cursor OUT types.cursorType,
    p_id_legajo in number,
    p_id_tramite_ipj in number);

  PROCEDURE SP_Guardar_Entidad_Acciones(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_entidades_accion out number,
    p_id_legajo in number,
    p_id_tramite_ipj in number,
    p_id_entidades_accion in number,
    p_Clase in varchar2,
    p_Cant_Votos in number,
    p_Id_Accion in number,
    p_Id_Forma_Accion in number,
    p_eliminar in number,
    p_borrador in varchar2, -- 1 elimina
    p_sin_clase in varchar2,
    p_fecha_baja in varchar2 -- Es Fecha
  );

  PROCEDURE SP_Traer_Entidad_Integracion(
    o_Cursor OUT types.cursorType,
    p_id_legajo in number,
    p_id_tramite_ipj in number,
    p_Id_Entidad_Socio in number);

  PROCEDURE SP_Traer_Entidad_Soc_Acc(
    o_Cursor OUT types.cursorType,
    p_id_legajo in number,
    p_id_tramite_ipj in number,
    p_Id_Entidad_Socio in number);

  PROCEDURE SP_Guardar_Entidad_Soc_Acc(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_legajo in number,
    p_id_tramite_ipj in number,
    p_id_entidad_socio in number,
    p_id_entidades_accion in number,
    p_cantidad in number,
    p_borrador in varchar2,
    p_eliminar in number); -- 1 elimina

  PROCEDURE SP_Guardar_Entidad_Integ(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_entidad_socio_capital out number,
    p_id_legajo in number,
    p_id_tramite_ipj in number,
    p_id_entidad_socio_capital in number,
    p_id_entidad_socio in number,
    p_id_capital in number,
    p_dominio in varchar2,
    p_monto in varchar2,
    p_borrador in varchar2,
    p_eliminar in number, -- 1 elimina
    p_id_tramite_ipj_entidad in number,
    p_obs in varchar2,
    p_id_moneda in varchar2
  );

  PROCEDURE Sp_Traer_Datos_Empresa_IPJ(
    o_Cursor out IPJ.TYPES.CURSORTYPE,
    p_id_legajo in number
    );

  PROCEDURE SP_Traer_Entidad_Autorizados(
    o_Cursor OUT types.cursorType,
    p_id_tramite_ipj in number);

  PROCEDURE SP_Guardar_Entidad_Autorizados(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in number,
    p_id_legajo in number,
    p_id_integrante in number,
    p_id_sexo in varchar2,
    p_nro_documento in varchar2,
    p_pai_cod_pais in varchar2,
    p_id_numero in number,
    p_nombre in varchar2,
    p_eliminar in number -- 1 Elimina
    );

  PROCEDURE SP_Traer_Entidad_ClasifIPJ(
    o_Cursor OUT types.cursorType,
    p_id_tramite_ipj in number,
    p_id_legajo in number);

  PROCEDURE SP_Guardar_Entidad_ClasifIPJ(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in number,
    p_id_legajo in number,
    p_id_rubro_ipj in number,
    p_eliminar in number);

  PROCEDURE SP_Traer_PersJur_Fiduciario(
    o_Cursor OUT TYPES.cursorType,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number);

  PROCEDURE SP_Traer_PersJur_Fiduciante(
    o_Cursor OUT TYPES.cursorType,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number);

  PROCEDURE SP_Traer_PersJur_Benef(
    o_Cursor OUT TYPES.cursorType,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number);

  PROCEDURE SP_Traer_PersJur_Fideicom(
    o_Cursor OUT TYPES.cursorType,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number);

  PROCEDURE SP_GUARDAR_ENTIDAD_FIDUCIARIO(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_entidad_fiduciario out number,
    p_id_entidad_fiduciario in number,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number,
    -- Datos particulares
    p_fecha_inicio in varchar2, -- es fecha
    p_fecha_fin in varchar2,  -- es fecha
    p_tipo_participacion in varchar2,
    p_persona_expuesta in char,
    -- Persona Fisica
    p_id_integrante in number,
    -- Empresa
    p_id_legajo_emp in number,
    p_cuit_empresa in varchar2,
    p_n_empresa in varchar2,
    p_id_tipo_entidad in number,
    p_matricula in varchar2,
    p_folio in varchar2,
    p_anio in varchar2,
    p_en_formacion in char,
    p_fec_acta in varchar2, --es fecha
    p_certificacion_contable in char,
    p_matricula_certificante in varchar2,
    -- Varios
    p_eliminar in number -- 1 Elimina
    );

  PROCEDURE SP_GUARDAR_ENTIDAD_FIDUCIANTE(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_entidad_fiduciante out number,
    p_id_entidad_fiduciante in number,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number,
    -- Datos particulares
    p_fecha_inicio in varchar2, -- es fecha
    p_fecha_fin in varchar2,  -- es fecha
    p_tipo_participacion in varchar2,
    p_persona_expuesta in char,
    -- Persona Fisica
    p_id_integrante in number,
    -- Empresa
    p_id_legajo_emp in number,
    p_cuit_empresa in varchar2,
    p_n_empresa in varchar2,
    p_id_tipo_entidad in number,
    p_matricula in varchar2,
    p_folio in varchar2,
    p_anio in varchar2,
    p_en_formacion in char,
    p_fec_acta in varchar2, --es fecha
    p_certificacion_contable in char,
    p_matricula_certificante in varchar2,
    -- Varios
    p_eliminar in number -- 1 Elimina
    );

    PROCEDURE SP_GUARDAR_ENTIDAD_BENEF(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_entidad_benef out number,
    p_id_entidad_benef in number,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number,
    -- Datos particulares
    p_fecha_inicio in varchar2, -- es fecha
    p_fecha_fin in varchar2,  -- es fecha
    p_tipo_participacion in varchar2,
    p_persona_expuesta in char,
    -- Persona Fisica
    p_id_integrante in number,
    -- Empresa
    p_id_legajo_emp in number,
    p_cuit_empresa in varchar2,
    p_n_empresa in varchar2,
    p_id_tipo_entidad in number,
    p_matricula in varchar2,
    p_folio in varchar2,
    p_anio in varchar2,
    p_en_formacion in char,
    p_fec_acta in varchar2, --es fecha
    p_certificacion_contable in char,
    p_matricula_certificante in varchar2,
    -- Varios
    p_eliminar in number -- 1 Elimina
    );

  PROCEDURE SP_GUARDAR_ENTIDAD_FIDEICOM(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_entidad_fideicom out number,
    p_id_entidad_fideicom in number,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number,
    -- Datos particulares
    p_fecha_inicio in varchar2, -- es fecha
    p_fecha_fin in varchar2,  -- es fecha
    p_tipo_participacion in varchar2,
    p_persona_expuesta in char,
    -- Persona Fisica
    p_id_integrante in number,
    -- Empresa
    p_id_legajo_emp in number,
    p_cuit_empresa in varchar2,
    p_n_empresa in varchar2,
    p_id_tipo_entidad in number,
    p_matricula in varchar2,
    p_folio in varchar2,
    p_anio in varchar2,
    p_en_formacion in char,
    p_fec_acta in varchar2, --es fecha
    p_certificacion_contable in char,
    p_matricula_certificante in varchar2,
    -- Varios
    p_eliminar in number -- 1 Elimina
    );

  PROCEDURE SP_Traer_PersJur_Docs(
    o_Cursor OUT TYPES.cursorType,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number
  );

  PROCEDURE SP_Guardar_Entidad_Docs(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number,
    p_id_documento in number,
    p_n_documento in varchar2,
    p_observacion in varchar2,
    p_cuil_usr in varchar2,
    p_fecha_alta in varchar2, -- es fecha
    p_id_tipo_documento_cdd in number,
    p_eliminar in number -- 1 Elimina
  );

  PROCEDURE SP_Traer_Entidad_Autoriz_Boe(
    o_Cursor OUT types.cursorType,
    p_id_tramite_ipj in number
  );

  PROCEDURE SP_Guardar_Entidad_Autoriz_Boe(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in number,
    p_id_integrante in number,
    p_eliminar in number -- 1 Elimina
  );

  PROCEDURE SP_Traer_Entidad_Edicto(
    o_Cursor OUT types.cursorType,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number
  );

  PROCEDURE SP_Guardar_Envio_Edicto(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_cant_edictos out number,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number,
    p_nro_edicto in number,
    p_fecha_envio in varchar2, -- es fecha
    p_enviar_mail in char
  );

  PROCEDURE SP_Act_Autorizado_Dom_Elec(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_integrante IN ipj.t_integrantes.id_integrante%TYPE,
    p_id_tramite_ipj IN ipj.t_entidades_autorizados.id_tramite_ipj%TYPE
  );

  PROCEDURE SP_Act_Entidad_Anexados(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_sede IN ipj.t_entidades.id_sede%TYPE,
    p_cuit IN ipj.t_entidades.cuit%TYPE,
    p_id_moneda IN ipj.t_entidades.id_moneda%TYPE,
    p_id_unidad_med IN ipj.t_entidades.id_unidad_med%TYPE,
    p_matricula IN ipj.t_entidades.matricula%TYPE,
    p_fecha_inscripcion IN ipj.t_entidades.fec_inscripcion%TYPE,
    p_folio IN ipj.t_entidades.folio%TYPE,
    p_libro_nro IN ipj.t_entidades.libro_nro%TYPE,
    p_tomo IN ipj.t_entidades.tomo%TYPE,
    p_anio IN ipj.t_entidades.anio%TYPE,
    p_desc_obj_social IN ipj.t_entidades.objeto_social%TYPE,
    p_monto IN ipj.t_entidades.monto%TYPE,
    p_acta_constitutiva IN ipj.t_entidades.acta_contitutiva%TYPE,
    p_vigencia IN ipj.t_entidades.vigencia%TYPE,
    p_cierre_fin_mes IN ipj.t_entidades.cierre_fin_mes%TYPE,
    p_fec_vig IN ipj.t_entidades.fec_vig%TYPE,
    p_id_vin_comercial IN ipj.t_entidades.id_vin_comercial%TYPE,
    p_id_vin_real IN ipj.t_entidades.id_vin_real%TYPE,
    p_borrador IN ipj.t_entidades.borrador%TYPE,
    p_fecha_acto IN ipj.t_entidades.fec_acto%TYPE,
    p_denominacion_1 IN ipj.t_entidades.denominacion_1%TYPE,
    p_n_Sede IN ipj.t_entidades.denominacion_2%TYPE,
    p_alta_temporal IN ipj.t_entidades.alta_temporal%TYPE,
    p_tipo_vigencia IN ipj.t_entidades.tipo_vigencia%TYPE,
    p_valor_cuota IN ipj.t_entidades.valor_cuota%TYPE,
    p_matricula_version IN ipj.t_entidades.matricula_version%TYPE,
    p_cuotas IN ipj.t_entidades.cuotas%TYPE,
    p_id_tipo_admin IN ipj.t_entidades.id_tipo_administracion%TYPE,
    p_id_tipo_entidad IN ipj.t_entidades.id_tipo_entidad%TYPE,
    p_baja_temporal IN ipj.t_entidades.baja_temporal%TYPE,
    p_id_estado_entidad IN ipj.t_entidades.id_estado_entidad%TYPE,
    p_obs_cierre IN ipj.t_entidades.obs_cierre%TYPE,
    p_Nro_Resolucion IN ipj.t_entidades.nro_resolucion%TYPE,
    p_Fec_Resolucion IN ipj.t_entidades.fec_resolucion%TYPE,
    p_Nro_Boletin IN ipj.t_entidades.nro_boletin%TYPE,
    p_Fec_Boletin IN ipj.t_entidades.fec_boletin%TYPE,
    p_Nro_Registro IN ipj.t_entidades.nro_registro%TYPE,
    p_Sede_Estatuto IN ipj.t_entidades.sede_estatuto%TYPE,
    p_Requiere_Sindico IN ipj.t_entidades.requiere_sindico%TYPE,
    p_Origen IN ipj.t_entidades.origen%TYPE,
    p_Es_Sucursal IN ipj.t_entidades.es_sucursal%TYPE,
    p_Observacion IN ipj.t_entidades.observacion%TYPE,
    p_Id_Tipo_Origen IN ipj.t_entidades.id_tipo_origen%TYPE,
    p_cierre_ejercicio IN ipj.t_entidades.cierre_ejercicio%TYPE,
    p_fiscalizacion_ejerc IN ipj.t_entidades.fiscalizacion_ejerc%TYPE,
    p_incluida_lgs IN ipj.t_entidades.incluida_lgs%TYPE,
    p_condicion_fideic IN ipj.t_entidades.condicion_fideic%TYPE,
    p_obs_fiduciario IN ipj.t_entidades.obs_fiduciario%TYPE,
    p_obs_fiduciante IN ipj.t_entidades.obs_fiduciante%TYPE,
    p_obs_beneficiario IN ipj.t_entidades.obs_beneficiario%TYPE,
    p_obs_fideicomisario IN ipj.t_entidades.obs_fideicomisario%TYPE,
    p_fecha_versionado IN ipj.t_entidades.fecha_versionado%TYPE,
    p_sin_denominacion IN ipj.t_entidades.sin_denominacion%TYPE,
    p_contrato_privado IN ipj.t_entidades.contrato_privado%TYPE,
    p_cant_Fiduciario IN ipj.t_entidades.cant_fiduciario%TYPE,
    p_cant_Fiduciante IN ipj.t_entidades.cant_fiduciante%TYPE,
    p_cant_Beneficiario IN ipj.t_entidades.cant_beneficiario%TYPE,
    p_cant_Fideicomisario IN ipj.t_entidades.cant_fideicomisario%TYPE,
    p_fec_vig_hasta IN ipj.t_entidades.fec_vig_hasta%TYPE,
    p_id_tramite_ipj IN ipj.t_entidades.id_tramite_ipj%TYPE,
    p_id_legajo IN ipj.t_entidades.id_legajo%TYPE,
    p_acredita_grupo IN ipj.t_entidades.acredita_grupo%TYPE
    );

  PROCEDURE SP_Buscar_Entidad_Informe(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_Id_Tramite_Ipj out number,
    o_id_tramiteipj_accion out number,
    o_id_documento out number,
    o_firmado out number, -- 0 Sin firma / >0 Con Firma
    p_id_legajo in number
  );

  PROCEDURE SP_GUARDAR_ENTIDAD_CUIT(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number,
    p_cuit in varchar2
  );

  PROCEDURE SP_Traer_PersJur_Docs_Adj(
    o_Cursor OUT TYPES.cursorType,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number
  );

  PROCEDURE SP_Guardar_Entidad_Docs_Adj(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in number,
    p_id_legajo in number,
    p_id_documento in number,
    p_n_documento in varchar2,
    p_fecha_verif in varchar2, -- Es Fecha
    p_id_integrante in number,
    p_id_tipo_documento_cdd in number,
    p_id_estado_doc in number,
    p_fecha_modif in varchar2, -- Es Fecha
    p_observacion in varchar2,
    p_eliminar in number, -- 1 Elimina
    p_utiliza_firma in number
  );

  PROCEDURE SP_Act_Datos_Resolucion(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj IN ipj.t_entidades.id_tramite_ipj%TYPE,
    p_Id_Legajo IN ipj.t_entidades.id_legajo%TYPE,
    p_Matricula IN ipj.t_entidades.matricula%TYPE,
    p_Letra IN VARCHAR2,
    p_Matricula_Version IN ipj.t_entidades.matricula_version%TYPE,
    p_Fecha_Versionado IN VARCHAR2,
    p_Nro_resolucion IN ipj.t_entidades.nro_resolucion%TYPE,
    p_Fecha_Resolucion IN VARCHAR2,
    p_Cuit IN VARCHAR2
  );

  PROCEDURE SP_Buscar_CUIT_Gobierno(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_denominacion out varchar2,
    p_cuit in varchar2,
    p_id_tramite_ipj in number
  );

  PROCEDURE SP_Traer_PersJur_Libros_Dig(
    o_Cursor OUT IPJ.TYPES.cursorType,
    p_id_legajo in number
  );

  PROCEDURE SP_Traer_Intervencion(
    o_Cursor OUT TYPES.cursorType,
    o_rdo out varchar,
    p_Id_Tramite_Ipj in number,
    p_id_tramiteipj_accion in number,
    p_id_tipo_accion in number
  );

  PROCEDURE SP_Traer_Interv_Ins_Legal(
    o_Cursor OUT TYPES.cursorType,
    p_Id_Tramite_Ipj in number,
    p_Id_TramiteIPJ_Accion in number,
    p_id_tipo_accion in number
  );

  PROCEDURE SP_GUARDAR_INTERV_INS_LEGAL(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in number,
    p_id_tramiteipj_accion in number,
    p_id_tipo_accion in number,
    p_id_legajo in number,
    p_id_integrante in number,
    p_id_tipo_intervencion in number,
    p_id_juzgado in number,
    p_il_tipo in number,
    p_il_numero in number,
    p_il_fecha in varchar2, -- Es Fecha
    p_titulo in varchar2,
    p_descripcion in varchar2,
    p_fecha in varchar2, -- es fecha
    p_eliminar in number-- 1 elimina
  );

  PROCEDURE SP_GUARDAR_ENTIDAD_REPR_HIST(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in number,
    p_id_legajo in number,
    p_id_integrante in number,
    p_fecha_alta in varchar2,
    p_borrador in varchar2,
    p_fecha_baja in varchar2,
    p_persona_expuesta char,
    p_id_vin_especial number,
    p_id_motivo_baja number
  );

  PROCEDURE SP_Validar_Libros_Dig(
    o_rdo out number,
    p_id_legajo in number
  );

  PROCEDURE SP_Traer_Autoriz_Mail(
    o_Cursor OUT types.cursorType,
    p_Id_Tramite_Ipj in number,
    p_hab_autoriz in char,
    p_hab_admin in char,
    p_hab_repres in char,
    p_hab_socios in char,
    p_hab_fiduciario in char,
    p_hab_fiduciante in char,
    p_hab_denunciante in char,
    p_hab_normalizador in char
  );

  PROCEDURE SP_Traer_PersJur_Admin_Borr(
    o_Cursor OUT TYPES.cursorType,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number
  );

  PROCEDURE SP_Traer_PersJur_Sind_Borr(
    o_Cursor OUT TYPES.cursorType,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number
  );

  PROCEDURE SP_Traer_Actas_Inscr(
    o_Cursor OUT TYPES.cursorType,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number
  );

  PROCEDURE SP_Bajar_Docs_Tramite(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number
  );

  PROCEDURE SP_Traer_Otorgantes_Fideo_Inf(
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number,
    p_Cursor OUT TYPES.cursorType
  );

  PROCEDURE SP_Informar_Gravamenes(
    o_Cursor OUT TYPES.cursorType,
    p_id_legajo in number
  );

  PROCEDURE SP_Informar_Gravamenes_Pers(
    o_Cursor OUT TYPES.cursorType,
    p_id_integrante in number
  );

  PROCEDURE SP_GUARDAR_GRAVAMEN_FIN(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    -- Datos del instrumento del Gravamen Original
    p_id_tramite_ipj in number,
    p_id_tramiteipj_accion in number,
    p_id_tipo_accion in number,
    p_id_juzgado in number,
    p_il_tipo in number,
    p_il_numero in number,
    p_id_tipo_gravamen in number,
    -- Datos del Instrumento de Fin
    p_fin_id_juzgado in number,
    p_fin_il_tipo in number,
    p_fin_il_numero in number,
    p_fin_il_fecha in varchar2,
    p_fin_fecha in varchar2,
    p_fin_id_tramite_ipj in number
  );

  PROCEDURE SP_Inf_Hist_Razon_Social(
    o_Cursor OUT TYPES.cursorType,
    p_id_legajo in number,
    p_anio_desde in number,
    p_anio_hasta in number,
    p_desde_const in char
  );

  PROCEDURE SP_Inf_Hist_Objeto(
    o_Cursor OUT TYPES.cursorType,
    p_id_legajo in number,
    p_anio_desde in number,
    p_anio_hasta in number,
    p_desde_const in char
  );

  PROCEDURE SP_Inf_Hist_Cierre_Ejerc(
    o_Cursor OUT TYPES.cursorType,
    p_id_legajo in number,
    p_anio_desde in number,
    p_anio_hasta in number,
    p_desde_const in char
  );

  PROCEDURE SP_Inf_Hist_Sede(
    o_Cursor OUT TYPES.cursorType,
    p_id_legajo in number,
    p_anio_desde in number,
    p_anio_hasta in number,
    p_desde_const in char
  );

  PROCEDURE SP_Inf_Hist_Vigencia(
    o_Cursor OUT TYPES.cursorType,
    p_id_legajo in number,
    p_anio_desde in number,
    p_anio_hasta in number,
    p_desde_const in char
  );

  PROCEDURE SP_Inf_Hist_Capital(
    o_Cursor OUT TYPES.cursorType,
    p_id_legajo in number,
    p_anio_desde in number,
    p_anio_hasta in number,
    p_desde_const in char
  );

  PROCEDURE SP_Inf_Hist_Tipo_Admin(
    o_Cursor OUT TYPES.cursorType,
    p_id_legajo in number,
    p_anio_desde in number,
    p_anio_hasta in number,
    p_desde_const in char
  );

  PROCEDURE SP_Inf_Hist_Admin(
    o_Cursor OUT TYPES.cursorType,
    p_id_legajo in number,
    p_anio_desde in number,
    p_anio_hasta in number,
    p_desde_const in char
  );

  PROCEDURE SP_Inf_Hist_Repres(
    o_Cursor OUT TYPES.cursorType,
    p_id_legajo in number,
    p_anio_desde in number,
    p_anio_hasta in number,
    p_desde_const in char
  );

  PROCEDURE SP_Inf_Hist_Socios(
    o_Cursor OUT TYPES.cursorType,
    p_id_legajo in number,
    p_anio_desde in number,
    p_anio_hasta in number,
    p_desde_const in char
  );

  PROCEDURE SP_Inf_Hist_Fiduciario(
    o_Cursor OUT TYPES.cursorType,
    p_id_legajo in number,
    p_anio_desde in number,
    p_anio_hasta in number,
    p_desde_const in char
  );

  PROCEDURE SP_Inf_Hist_Fiduciante(
    o_Cursor OUT TYPES.cursorType,
    p_id_legajo in number,
    p_anio_desde in number,
    p_anio_hasta in number,
    p_desde_const in char
  );

  PROCEDURE SP_Inf_Hist_Beneficiario(
    o_Cursor OUT TYPES.cursorType,
    p_id_legajo in number,
    p_anio_desde in number,
    p_anio_hasta in number,
    p_desde_const in char
  );

  PROCEDURE SP_Inf_Hist_Fideicomisario(
    o_Cursor OUT TYPES.cursorType,
    p_id_legajo in number,
    p_anio_desde in number,
    p_anio_hasta in number,
    p_desde_const in char
  );

  PROCEDURE SP_Armar_Rango_Inf_Hist(
    o_fecha_Desde out date,
    o_fecha_hasta out date,
    p_id_legajo in number,
    p_anio_desde in number,
    p_anio_hasta in number,
    p_desde_Const char,
    p_buscar_anterio char,
    p_datos_entidad char
  );

  PROCEDURE SP_Traer_Fiduciario_Repres(
    o_Cursor OUT types.cursorType,
    p_id_tramite_ipj in number,
    p_id_entidad_fiduciario in number
  );

  PROCEDURE SP_Guardar_Fiduciario_Repres(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_entidad_fiduciario in number,
    p_id_tramite_ipj in number,
    -- Datos Persona
    p_id_integrante in number,
    p_id_sexo in varchar2,
    p_nro_documento in varchar2,
    p_pai_cod_pais in varchar2,
    p_id_numero in number,
    p_detalle in varchar2,
    p_nombre in varchar2,
    p_apellido in varchar2,
    p_n_tipo_documento in varchar2,
    -- Datos Particulaes
    p_cuil in varchar2,
    p_id_tipo_repres in number,
    p_repres_persona_expuesta in char,
    p_fecha_poder in varchar -- es fecha
  );

  PROCEDURE SP_Buscar_Versiones_Ent(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    -- Vigencia
    o_matricula_Vigencia out varchar2,
    o_matricula_version_Vigencia out number,
    o_folio_Vigencia out varchar2,
    o_anio_Vigencia out number,
    -- Estado
    o_matricula_Estado out varchar2,
    o_matricula_version_Estado out number,
    o_folio_Estado out varchar2,
    o_anio_Estado out number,
    -- Cierre Ejercicio
    o_matricula_Cierre out varchar2,
    o_matricula_version_Cierre out number,
    o_folio_Cierre out varchar2,
    o_anio_Cierre out number,
    -- Domicilio
    o_matricula_Dom out varchar2,
    o_matricula_version_Dom out number,
    o_folio_Dom out varchar2,
    o_anio_Dom out number,
    -- Denominacion
    o_matricula_Denom out varchar2,
    o_matricula_version_Denom out number,
    o_folio_Denom out varchar2,
    o_anio_Denom out number,
    -- Tipo Administracion
    o_matricula_TipoAd out varchar2,
    o_matricula_version_TipoAd out number,
    o_folio_TipoAd out varchar2,
    o_anio_TipoAd out number,
    -- Objeto Social
    o_matricula_Obj out varchar2,
    o_matricula_version_Obj out number,
    o_folio_Obj out varchar2,
    o_anio_Obj out number,
    -- Capital
    o_matricula_Capital out varchar2,
    o_matricula_version_Capital out number,
    o_folio_Capital out varchar2,
    o_anio_Capital out number,

    p_Id_Tramite_Ipj in number,
    p_id_legajo in number
  );

  PROCEDURE SP_ACTUALIZAR_ENT_GOB(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number
  );

  PROCEDURE SP_Traer_PersJur_Resol(
    o_Cursor OUT TYPES.cursorType,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number
  );

 /*********************************************************
  **********************************************************
                   PROCEDIMINETO CON SOBRECARGA DE PARAMETROS
  **********************************************************
  **********************************************************/
  PROCEDURE SP_TRAER_CONSULTA_NOMBRE_COMP(O_CURSOR       OUT TYPES.CURSORTYPE,
                                          P_CLAVE        IN VARCHAR2,
                                          P_CUIL_USUARIO IN VARCHAR2);

  PROCEDURE SP_GUARDAR_INTEGRANTES(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_integrante out number,
    p_id_sexo in varchar2,
    p_nro_documento in varchar2,
    p_pai_cod_pais in varchar2,
    p_id_numero in number,
    p_cuil in varchar2,
    p_detalle in varchar2,
    p_Nombre in varchar2,
    p_Apellido in varchar2,
    p_error_dato in varchar2,
    p_n_tipo_documento in varchar2
  );


  PROCEDURE SP_GUARDAR_ENTIDAD_ADMIN(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_integrante out number,
    o_id_admin out number,
    p_id_tramite_ipj in number,
    p_id_legajo in number,
    p_id_tipo_integrante in number,
    p_fecha_inicio in varchar2,
    p_fecha_fin in varchar2,
    p_id_integrante in number,
    p_id_sexo in varchar2,
    p_nro_documento in varchar2,
    p_pai_cod_pais in varchar2,
    p_id_numero in number,
    p_cuil in varchar2,
    p_detalle in varchar2,
    p_nombre in varchar2,
    p_apellido in varchar2,
    p_Id_Vin in number,
    p_Id_Tipo_Organismo in number,
    p_error_dato in varchar2,
    p_eliminar in number, -- 1 eliminar
    p_n_tipo_documento in varchar2,
    p_id_tramite_ipj_entidad in number,
    p_id_entidades_accion in number,
    p_id_motivo_baja in number,
    p_porc_garantia in varchar2,
    p_monto_garantia in varchar2,
    p_habilitado_present in char,
    p_persona_expuesta in char,
    p_id_moneda in varchar2,
    p_id_admin in number,
    p_cuit_empresa in varchar2,
    p_n_empresa in varchar2,
    p_matricula in varchar2,
    p_fec_acta in varchar2, -- es fecha
    p_id_legajo_empresa in number,
    p_folio in varchar2,
    p_anio in number,
    p_Id_Tipo_Entidad in NUMBER
    );



end Entidad_PersJur;
/

create or replace package body ipj.Entidad_PersJur is

  FUNCTION FC_Cuit_AFip(p_id_legajo in number) return number is
  /**********************************************************
    Busca si el cuit de la entidad lo asign� Gestion integrado a AFIP
  **********************************************************/
    v_result number;
  BEGIN
    select count(1) into v_result
    from ipj.t_tramitesipj tr join ipj.t_entidades e
      on tr.id_tramite_ipj = e.id_tramite_ipj
    where
      e.id_legajo = p_id_legajo and
      tr.asigna_cuit_afip is not null and -- El tramite se marco pro AFIP
      tr.id_estado_ult between 100 and 199; -- no es un tr�mite rechazado

    Return v_result;
  Exception
    When Others Then
      Return 0;
  End FC_Cuit_AFip;

  FUNCTION FC_Buscar_Ult_Dom(p_id_legajo in number, p_Tram_Abierto in char) return number is
  /**********************************************************
    Busca el �ltimo domicilio asignado a una entidad
    - p_Tram_Abierto: indica si utiliza tramitres pendientes o cerrados
  **********************************************************/
    v_id_vin number(20);
    v_Id_Tramite_Ipj number;
    v_id_tramiteipj_accion number;
  BEGIN
    -- Busco si ya se registr� el ultimo domicilio en el legajo.
    select id_vin into v_id_vin
    from ipj.t_legajos l
    where
      id_legajo = p_id_legajo;

    -- Si no hay domicilio registrado o se busca en tr�mites abiertos, busco el �ltimo tr�mite
    if nvl(v_id_vin, 0) = 0 or p_Tram_Abierto = 'S' then
      IPJ.ENTIDAD_PERSJUR.SP_Buscar_Entidad_Tramite(
        o_Id_Tramite_Ipj => v_Id_Tramite_Ipj,
        o_id_tramiteipj_accion => v_id_tramiteipj_accion,
        p_id_legajo => p_id_legajo,
        p_Abiertos => p_Tram_Abierto);

      select id_vin_real into v_id_vin
      from ipj.t_entidades
      where
        id_tramite_ipj = v_id_tramite_ipj;
    end if;

    Return v_id_vin;
  Exception
    When Others Then
      Return null;
  End FC_Buscar_Ult_Dom;

  FUNCTION FC_FORMATEAR_CADENA_COMP(v_cadena IN varchar2) return varchar2 is
    v_res varchar2(250);
  /**********************************************************
    Elimina caracteres predefinido y concatena las palabras
  **********************************************************/
    v_inavlid_char varchar2(50);
  BEGIN
    v_inavlid_char := '+-;._�?�!$()�';
    v_res := TRANSLATE (v_cadena, v_inavlid_char, lpad('@', length(v_inavlid_char), '@'));

    Return replace(v_res, '@', '');
  Exception
    When Others Then
      Return '';
  End FC_FORMATEAR_CADENA_COMP;

  FUNCTION FC_Balances_Pendientes(p_id_legajo IN number, p_Id_Tramite_Ipj in number) return varchar2 is
  /**********************************************************
    Lista los ablances adeudados por una empresa SxA o ACyF
  **********************************************************/
    v_res varchar2(1000);
    v_id_ubicacion number;
    v_ultimo_balance date;
    v_cierre_ejerc varchar2(10);
    v_Cierre_Fin_Mes char;
    v_ultimo_balance_present date;
    v_fec_inscripcion date;
  BEGIN
    -- Busco el area y el cierre de ejercicio
    select to_char(e.Cierre_Ejercicio, 'dd/mm'), E.FEC_INSCRIPCION, te.id_ubicacion, nvl(Cierre_Fin_Mes, 'N')
      into v_cierre_ejerc, v_fec_inscripcion, v_id_ubicacion, v_Cierre_Fin_Mes
    from ipj.t_entidades e join ipj.t_tipos_entidades te
      on e.id_tipo_entidad = te.id_tipo_entidad
    where
      id_legajo = p_id_legajo and
      Id_Tramite_Ipj = p_Id_Tramite_Ipj;

    if v_id_ubicacion <> IPJ.TYPES.C_AREA_SRL then
      select max(fecha) into v_ultimo_balance_present
      from ipj.t_entidades_balance
      where
        id_legajo = p_id_legajo and
        (id_tramite_ipj = p_Id_Tramite_Ipj or borrador = 'N');

      -- Controlo cual deberia ser el �ltimo balance, viendo si usa Ultimo de Febrero o no
      if v_Cierre_Fin_Mes = 'N' then
        if to_date(v_cierre_ejerc ||'/'|| to_char(sysdate, 'rrrr'), 'dd/mm/rrrr') > sysdate then
          v_ultimo_balance := to_date(v_cierre_ejerc ||'/'|| to_char(to_number(to_char(sysdate, 'rrrr')) -1), 'dd/mm/rrrr');
        else
          v_ultimo_balance := to_date(v_cierre_ejerc ||'/'|| to_char(sysdate, 'rrrr'), 'dd/mm/rrrr');
        end if;
      else
        if to_date('01/03/'|| to_char(sysdate, 'rrrr'), 'dd/mm/rrrr')-1 > sysdate then
          v_ultimo_balance := to_date('01/03/'|| to_char(to_number(to_char(sysdate, 'rrrr')) -1), 'dd/mm/rrrr')-1;
        else
          v_ultimo_balance := to_date('01/03/'|| to_char(sysdate, 'rrrr'), 'dd/mm/rrrr')-1;
        end if;
      end if;

      -- Para el ultimo balance, tiene 120 d�as para presentarlo, luego de esto se marca como pendiente
      if sysdate - v_ultimo_balance  < 120 then
        if to_char(v_ultimo_balance, 'dd/mm') <> '29/02' then
          v_ultimo_balance := to_date(to_char(v_ultimo_balance, 'dd/mm') ||'/'|| to_char(to_number(to_char(v_ultimo_balance, 'rrrr')) -1), 'dd/mm/rrrr');
        else
          v_ultimo_balance := to_date('28/02' ||'/'|| to_char(to_number(to_char(v_ultimo_balance, 'rrrr')) -1), 'dd/mm/rrrr');
        end if;
      end if;

      -- Para ACyF, si el primer balance es irregluar, se toma como presentado, y se pide a partir de segundo
      if v_id_ubicacion = IPJ.TYPES.C_AREA_CYF and v_ultimo_balance_present is null then
        if MONTHS_BETWEEN (to_date(v_ultimo_balance, 'dd/mm/rrrr'), to_date(v_fec_inscripcion, 'dd/mm/rrrr')) < 12 then
          v_ultimo_balance_present := v_ultimo_balance;
        end if;
      end if;

      -- Si no tiene balances, controlo cual debe ser el primero
      if v_ultimo_balance_present is null then
        if v_Cierre_Fin_Mes = 'N' then
          if to_date(v_fec_inscripcion, 'dd/mm/rrrr') < to_date(v_cierre_ejerc || '/' || to_char(v_fec_inscripcion, 'rrrr'), 'dd/mm/rrrr') then
            v_ultimo_balance_present := to_date(to_char(v_fec_inscripcion, 'dd/mm') || '/' || to_char(to_number(to_char(v_fec_inscripcion, 'rrrr'))-1), 'dd/mm/rrrr');
          else
            v_ultimo_balance_present := v_fec_inscripcion;
          end if;
        else
          if to_date(v_fec_inscripcion, 'dd/mm/rrrr') < to_date('01/03/' || to_char(v_fec_inscripcion, 'rrrr'), 'dd/mm/rrrr')-1 then
            v_ultimo_balance_present := to_date(to_char(v_fec_inscripcion, 'dd/mm') || '/' || to_char(to_number(to_char(v_fec_inscripcion, 'rrrr'))-1), 'dd/mm/rrrr');
          else
            v_ultimo_balance_present := v_fec_inscripcion;
          end if;
        end if;
      end if;

      -- Armo un listado de los a�os adeudados
      FOR i in to_number(to_char(v_ultimo_balance_present, 'rrrr'))+1 .. to_number(to_char(v_ultimo_balance, 'rrrr')) loop
        if v_res is null then
          v_res := to_char(i);
        else
          v_res := v_res || ', ' || to_char(i);
        end if;
      END LOOP;

    else
      v_res := '';
    end if;

    Return v_res;
  Exception
    When Others Then
      Return '(Sin Fecha de Cierre o Inscripci�n)';
  End FC_Balances_Pendientes;

  FUNCTION FC_ESTADO_GRAVAMEN_PERSJUR(p_id_legajo IN number) return varchar2 is
    v_res varchar2(2500);
  /**********************************************************
    Devuelve el ultimo estado de un Gravamen de una Empresa
  **********************************************************/
  BEGIN
    select
      ( select distinct listagg ('- ' || tg.n_tipo_gravamen || ' (desde ' || to_char(GL.FECHA, 'dd/mm/rrrr') || ') : ' || replace(replace(GL.DESCRIPCION, chr(10), ' '), chr(13), ' ') ||chr(10), '')
                       within group (order by g.id_legajo)
                       over (partition by g.id_legajo)
                       lista_nombres
        from ipj.t_gravamenes g join IPJ.T_GRAVAMENES_INS_LEGAL gl
          on g.id_tramite_ipj = gl.id_tramite_ipj and g.id_tramiteipj_accion = gl.id_tramiteipj_accion and g.id_tipo_accion = gl.id_tipo_accion
        join IPJ.T_TIPOS_GRAVAMENES tg
          on GL.ID_TIPO_GRAVAMEN = TG.ID_TIPO_GRAVAMEN
      where
        g.id_legajo = p_id_legajo and
        gl.fin_id_tramite_ipj is null
      ) into v_res
    from dual;

    if v_res is null then
      Return '--';
    else
      Return nvl(v_res, 'Sin Gravamenes');
    end if;
  Exception
    When Others Then
      Return '--';
  End FC_ESTADO_GRAVAMEN_PERSJUR;

  FUNCTION FC_ESTADO_GRAVAMEN_INTERV(p_id_legajo IN number) return varchar2 is
    v_res varchar2(1000);
    v_n_tipo_accion varchar2(500);
    v_descripcion varchar(200);
    v_Fecha_Inicio varchar2(10);
    v_Sticker varchar2(50);
    v_Expediente varchar2(50);
  /**********************************************************
    Devuelve el nombre de la accion abierta que tenga: Intervencion, Veeduria, Comision Normalizadora
  **********************************************************/
  BEGIN
    select n_tipo_accion, descripcion, to_char(Fecha_Inicio, 'dd/mm/rrrr'), Sticker, Expediente
      into v_n_tipo_accion, v_descripcion, v_Fecha_Inicio, v_Sticker, v_Expediente
    from
      ( select acc.n_tipo_accion, u.descripcion, tr.fecha_inicio, tr.sticker, tr.expediente
        from IPJ.t_tramitesipj_acciones tacc join ipj.t_tipos_Accionesipj acc
            on tacc.id_tipo_accion = acc.id_tipo_accion
          join ipj.t_tramitesipj tr
            on tr.Id_Tramite_Ipj = tacc.Id_Tramite_Ipj
          left join ipj.t_usuarios u
            on tacc.cuil_usuario = u.cuil_usuario
        where
          id_legajo = p_id_legajo and
          tacc.id_tipo_accion in (
             ipj.types.c_Tipo_Acc_Interv_ACyF,
             ipj.types.c_Tipo_Acc_Norm_ACyF,
             ipj.types.c_Tipo_Acc_Interv_SxA,
             ipj.types.c_Tipo_Acc_Norm_SxA,
             152) and --    Intervenci�n ACyF
          tr.id_estado_ult < IPJ.TYPES.c_Estados_Completado and
          (tacc.id_tipo_accion <> ipj.types.c_Tipo_Acc_Norm_ACyF or (select d.fec_resol_cn from ipj.t_denuncias d where d.id_tramite_ipj = tr.id_tramite_ipj) is not null)
        order by tacc.Id_Tramite_Ipj desc, tacc.id_tramiteipj_accion desc
      ) t
    where
      rownum = 1;

    v_res := v_n_tipo_accion || ' pendiente, a cargo de ' || v_descripcion || ' desde ' || v_Fecha_Inicio || ' (Sticker: ' || v_Sticker || ', Exp: ' || v_Expediente || ').';

    Return nvl(v_res, '--');
  Exception
    When Others Then
      Return '--';
  End FC_ESTADO_GRAVAMEN_INTERV;

  FUNCTION FC_DIRECCION_ENTIDAD (p_cuit IN varchar2) return number is
    v_res number;
  /**********************************************************
     Devuelve el maximo ID_VIN guardado para una empresa determinada
 **********************************************************/
  BEGIN

    select max(id_vin) into v_res
    from dom_manager.VT_DOMICILIOS_COND
    where
      id_app = IPJ.TYPES.C_ID_APLICACION and
      id_entidad =  p_cuit || '00' and
      ID_TIPODOM = 3;

    Return nvl(v_res, 0);
  Exception
    When Others Then
      Return 0;
  End FC_DIRECCION_ENTIDAD;


  FUNCTION FC_LISTAR_ACTA_ARCH_PEND(p_id_entidad_acta in number, p_id_tipo_acta in number) return varchar2 is
    v_res varchar2(200);
    v_Cursor_Archivos IPJ.TYPES.CURSORTYPE;
    v_Row_Archivos IPJ.t_archivos_acta%ROWTYPE;
  BEGIN
    OPEN v_Cursor_Archivos FOR
      select aa.*
      from IPJ.t_archivos_acta aa
      where
        id_tipo_acta = p_id_tipo_acta and
        id_archivo not in
          ( select id_archivo
            from IPJ.t_entidades_acta_arch eaa
            where
              EAA.ID_ENTIDAD_ACTA = p_id_entidad_acta and
              NVL(EAA.ID_INTEGRANTE, 0) <>0
         );

    v_res := null;
    LOOP
       fetch v_Cursor_Archivos into v_Row_Archivos;
       EXIT WHEN v_Cursor_Archivos%NOTFOUND;
       if v_res is null then
          v_res := v_Row_Archivos.ABREVIACION ;
       else
          v_res := v_res || ', ' || v_Row_Archivos.ABREVIACION;
       end if;
    END LOOP;

   CLOSE v_Cursor_Archivos;

   Return nvl(v_res, '');
  Exception
    When Others Then
      Return '--';
  End FC_LISTAR_ACTA_ARCH_PEND;


  FUNCTION FC_LISTAR_ACTA_ORDEN_DIA(p_id_entidad_acta in number) return varchar2 is
    v_res varchar2(200);
    v_Cursor_OrdenDia IPJ.TYPES.CURSORTYPE;
    v_Row_OrdenDia IPJ.t_tipos_orden_dia%ROWTYPE;
  BEGIN
    OPEN v_Cursor_OrdenDia FOR
      select od.*
      from IPJ.t_tipos_orden_dia od join IPJ.t_entidades_acta_orden eao
        on OD.ID_TIPO_ORDEN_DIA = EAO.ID_TIPO_ORDEN_DIA
      where
        EAo.ID_ENTIDAD_ACTA = p_id_entidad_acta;

    v_res := null;
    LOOP
       fetch v_Cursor_OrdenDia into v_Row_OrdenDia;
       EXIT WHEN v_Cursor_OrdenDia%NOTFOUND;
       if v_res is null then
          v_res := v_Row_OrdenDia.SIGLAS;
       else
          v_res := v_res || ', ' || nvl(v_Row_OrdenDia.SIGLAS, '-');
       end if;
    END LOOP;

   CLOSE v_Cursor_OrdenDia;

   Return nvl(v_res, '');
  Exception
    When Others Then
      Return '--';
  End FC_LISTAR_ACTA_ORDEN_DIA;


  FUNCTION FC_LISTAR_SOCIOS_COPROP(p_id_entidad_socio in number, p_Id_Tramite_Ipj in number) return varchar2 is
    v_res varchar2(2000);
    v_Cursor_Copro IPJ.TYPES.CURSORTYPE;
    v_Row_Copro IPJ.t_integrantes%ROWTYPE;
  BEGIN
    OPEN v_Cursor_Copro FOR
      select i.id_integrante, i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, i.cuil,
             (select p.nombre||' '||p.apellido from rcivil.vt_pk_persona p where p.id_sexo = i.id_sexo and p.nro_documento = i.nro_documento and p.pai_cod_pais = i.pai_cod_pais and p.id_numero = i.id_numero) detalle,
             i.id_vin, i.error_dato, i.id_estado_civil
      from IPJ.t_entidades_socios_copro esc join ipj.t_entidades_socios es
          on esc.id_entidad_socio = es.id_entidad_socio
        join t_integrantes i
          on esc.id_integrante = i.id_integrante
      where
        esc.id_entidad_socio = p_id_entidad_socio and
        (es.Id_Tramite_Ipj = p_Id_Tramite_Ipj or esc.borrador = 'N') and
        (esc.fec_baja is null or esc.fec_baja > sysdate);

    v_res := null;
    LOOP
       fetch v_Cursor_Copro into v_Row_Copro;
       EXIT WHEN v_Cursor_Copro%NOTFOUND;
       if v_res is null then
          v_res := v_Row_Copro.Detalle || ' DNI Nro. ' || v_Row_Copro.nro_documento;
       else
          v_res := v_res || ', ' || v_Row_Copro.Detalle || ' DNI Nro. ' || v_Row_Copro.nro_documento;
       end if;
    END LOOP;

   CLOSE v_Cursor_Copro;

   Return nvl(v_res, '');
  Exception
    When Others Then
      Return '--';
  End FC_LISTAR_SOCIOS_COPROP;

  FUNCTION FC_LISTAR_SOCIOS_USUF(p_id_entidad_socio in number, p_Id_Tramite_Ipj in number) return varchar2 is
    v_res varchar2(2000);
    v_Cursor_Copro IPJ.TYPES.CURSORTYPE;
    v_Row_Copro IPJ.t_integrantes%ROWTYPE;
  BEGIN
    OPEN v_Cursor_Copro FOR
      select i.id_integrante, i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, i.cuil,
             (select p.nombre||' '||p.apellido from rcivil.vt_pk_persona p where p.id_sexo = i.id_sexo and p.nro_documento = i.nro_documento and p.pai_cod_pais = i.pai_cod_pais and p.id_numero = i.id_numero) detalle,
             i.id_vin, i.error_dato, i.id_estado_civil
      from IPJ.t_entidades_socios_Usuf esc join ipj.t_entidades_socios es
          on esc.id_entidad_socio = es.id_entidad_socio
        join t_integrantes i
          on esc.id_integrante = i.id_integrante
      where
        esc.id_entidad_socio = p_id_entidad_socio and
        (es.Id_Tramite_Ipj = p_Id_Tramite_Ipj or esc.borrador = 'N') and
        (esc.fec_baja is null or esc.fec_baja > sysdate);

    v_res := null;
    LOOP
       fetch v_Cursor_Copro into v_Row_Copro;
       EXIT WHEN v_Cursor_Copro%NOTFOUND;
       if v_res is null then
          v_res := v_Row_Copro.Detalle || ' DNI Nro. ' || v_Row_Copro.nro_documento;
       else
          v_res := v_res || ', ' || v_Row_Copro.Detalle || ' DNI Nro. ' || v_Row_Copro.nro_documento;
       end if;
    END LOOP;

   CLOSE v_Cursor_Copro;

   Return nvl(v_res, '');
  Exception
    When Others Then
      Return '--';
  End FC_LISTAR_SOCIOS_USUF;


  FUNCTION FC_Fecha_Vigencia_Entidad(p_id_legajo number, p_matricula_version number, p_tipo_vigencia number) return varchar2 is
    v_result date;
  BEGIN
    select
      (case p_TIPO_VIGENCIA
         when 0 then Ent.FEC_INSCRIPCION
         when 1 then Ent.FEC_ACTO
         when 2 then Ent.FEC_VIG
        else null
      end) into v_result
    from ipj.t_entidades ent join IPJ.t_tramitesipj tipj
      on ent.Id_Tramite_Ipj = tipj.Id_Tramite_Ipj
    where
      ent.id_legajo = p_id_legajo and
      ent.matricula_version = p_matricula_version and
      TIPJ.ID_ESTADO_ULT < IPJ.TYPES.C_ESTADOS_RECHAZADO and
      rownum = 1;

    return to_char(v_result, 'dd/mm/rrrr');

  END FC_Fecha_Vigencia_Entidad;

  FUNCTION FC_Ult_Tram_Admin(p_id_legajo IN number, p_id_ubicacion_ent in number,
    p_matricula_version in number, p_Id_Tramite_Ipj in number) return number is
  /**********************************************************
    Busca el ultimo tr�mite que cargo autoridades, ordenando de distinta manera
    dependiendo de la ubicacion
  **********************************************************/
    v_id_admin number;
    v_id_Ult_autor number;
  BEGIN
    -- BUSCO el ultimo tr�mite de administradores, ordenando dependiendo del area
    if p_id_ubicacion_ent = IPJ.TYPES.C_AREA_CYF then
      -- Busco alg�n administrador de la maxima fecha desde
      select sum(id_admin) into v_id_admin
      from
        ( select ad.id_admin
          from ipj.t_entidades_admin ad join ipj.t_tramitesipj tr
            on ad.Id_Tramite_Ipj = tr.Id_Tramite_Ipj
          where
            ad.id_legajo = p_id_legajo and
            ( tr.id_tramite_ipj = p_id_tramite_ipj or
              tr.id_tramite_ipj_padre = p_id_tramite_ipj or
              (tr.id_estado_ult >= IPJ.TYPES.C_ESTADOS_COMPLETADO and tr.id_estado_ult < IPJ.TYPES.C_ESTADOS_RECHAZADO)
            )
          order by ad.fecha_inicio desc, ad.fecha_fin desc
          ) t
      where rownum = 1;

      -- Busco el ultimo tr�mite que tiene a ese administrador vigente
      select nvl(sum(Id_Tramite_Ipj), p_Id_Tramite_Ipj) into v_id_Ult_autor
      from
        ( select h.Id_Tramite_Ipj
          from ipj.t_entidades_admin_hist h  join ipj.t_tramitesipj tr
            on h.id_tramite_ipj = tr.id_tramite_ipj
          where
            h.id_legajo = p_id_legajo and
            h.id_admin = v_id_admin and
            ( h.id_tramite_ipj = p_id_tramite_ipj or
              tr.id_tramite_ipj_padre = p_id_tramite_ipj or
              (tr.id_estado_ult >= IPJ.TYPES.C_ESTADOS_COMPLETADO and tr.id_estado_ult < IPJ.TYPES.C_ESTADOS_RECHAZADO)
            )
          order by h.id_tramite_ipj desc
          ) t
      where rownum = 1;

    else -- ES SRL O SXA
      -- Busco el ultimo tramite que guardo Administradores
      select nvl(sum(Id_Tramite_Ipj), p_Id_Tramite_Ipj) into v_id_Ult_autor
      from
        ( select h.Id_Tramite_Ipj
          from ipj.t_entidades_admin_hist h join ipj.t_entidades e
              on h.Id_Tramite_Ipj = e.Id_Tramite_Ipj and h.id_legajo = e.id_legajo
            join ipj.t_tramitesipj tr
              on h.id_tramite_ipj = tr.id_tramite_ipj
          where
            h.id_legajo = p_id_legajo and
            e.Matricula_Version <= p_matricula_version and
            e.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = p_id_ubicacion_ent) and
            ( h.id_tramite_ipj = p_id_tramite_ipj or
              tr.id_tramite_ipj_padre = p_id_tramite_ipj or
              (tr.id_estado_ult >= IPJ.TYPES.C_ESTADOS_COMPLETADO and tr.id_estado_ult < IPJ.TYPES.C_ESTADOS_RECHAZADO)
            )
          order by e.matricula asc , matricula_version desc, nvl(e.anio, 0) desc, e.folio desc, h.Id_Tramite_Ipj desc
          ) t
      where rownum = 1;

    end if;

    return v_id_Ult_autor;
  Exception
    When Others Then
      Return 0;
  End FC_Ult_Tram_Admin;

  FUNCTION FC_Ult_Tram_Sindico(p_id_legajo IN number, p_id_ubicacion_ent in number,
    p_matricula_version in number, p_Id_Tramite_Ipj in number) return number is
  /**********************************************************
    Busca el ultimo tr�mite que cargo autoridades, ordenando de distinta manera
    dependiendo de la ubicacion
  **********************************************************/
    v_id_entidad_sindico number;
    v_id_Ult_autor_sindico number;
  BEGIN
    -- BUSCO el ultimo tr�mite de administradores, ordenando dependiendo del area
    if p_id_ubicacion_ent = IPJ.TYPES.C_AREA_CYF then
      -- Busco alguna fiscalizador de la maxima fecha desde
      select sum(id_entidad_sindico) into v_id_entidad_sindico
      from
        ( select s.id_entidad_sindico
          from ipj.t_entidades_sindico s join ipj.t_tramitesipj tr
            on s.Id_Tramite_Ipj = tr.Id_Tramite_Ipj
          where
            s.id_legajo = p_id_legajo and
            ( tr.id_tramite_ipj = p_id_tramite_ipj or
              tr.id_tramite_ipj_padre = p_id_tramite_ipj or
              ( tr.id_estado_ult >= IPJ.TYPES.C_ESTADOS_COMPLETADO and tr.id_estado_ult < IPJ.TYPES.C_ESTADOS_RECHAZADO)
            )
          order by s.fecha_alta desc, s.fecha_baja desc
          ) t
      where rownum = 1;

      select nvl(sum(Id_Tramite_Ipj), p_Id_Tramite_Ipj) into v_id_Ult_autor_sindico
      from
        ( select h.Id_Tramite_Ipj
          from ipj.t_entidades_sindico_hist h join ipj.t_tramitesipj tr
            on h.Id_Tramite_Ipj = tr.Id_Tramite_Ipj
          where
            h.id_legajo = p_id_legajo and
            h.id_entidad_sindico = v_id_entidad_sindico and
            ( h.id_tramite_ipj = p_id_tramite_ipj or
              tr.id_tramite_ipj_padre = p_id_tramite_ipj or
              ( tr.id_estado_ult >= IPJ.TYPES.C_ESTADOS_COMPLETADO and tr.id_estado_ult < IPJ.TYPES.C_ESTADOS_RECHAZADO)
            )
          order by h.Id_Tramite_Ipj desc
          ) t
      where rownum = 1;

    else

      -- Busco el ultimo tramite que guardo Sindicos
      select nvl(sum(Id_Tramite_Ipj), p_Id_Tramite_Ipj) into v_id_Ult_autor_sindico
      from
        ( select h.Id_Tramite_Ipj
          from ipj.t_entidades_sindico_hist h join ipj.t_entidades e
              on h.Id_Tramite_Ipj = e.Id_Tramite_Ipj and h.id_legajo = e.id_legajo
            join ipj.t_tramitesipj tr
              on h.id_tramite_ipj = tr.id_tramite_ipj
          where
            h.id_legajo = p_id_legajo and
            e.Matricula_Version <= p_matricula_version and
            e.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = p_id_ubicacion_ent) and
            ( h.id_tramite_ipj = p_id_tramite_ipj or
              tr.id_tramite_ipj_padre = p_id_tramite_ipj or
              (tr.id_estado_ult >= IPJ.TYPES.C_ESTADOS_COMPLETADO and tr.id_estado_ult < IPJ.TYPES.C_ESTADOS_RECHAZADO)
            )-- Se corrige para que traiga los del  tramite o lo cerrado
            --tr.id_estado_ult >= IPJ.TYPES.C_ESTADOS_COMPLETADO and
            --tr.id_estado_ult < IPJ.TYPES.C_ESTADOS_RECHAZADO
          order by e.matricula asc , matricula_version desc, nvl(anio, 0) desc, folio desc, h.Id_Tramite_Ipj desc
          ) t
      where rownum = 1;
    end if;

    return v_id_Ult_autor_sindico;
  Exception
    When Others Then
      Return 0;
  End FC_Ult_Tram_Sindico;

  FUNCTION FC_Ult_Tram_Repres(p_id_legajo IN number, p_id_ubicacion_ent in number,
    p_matricula_version in number, p_Id_Tramite_Ipj in number) return number is
  /**********************************************************
    Busca el ultimo tr�mite que cargo el organo de Presentaci�n de las SAS
  **********************************************************/
    v_id_Ult_autor_repres number;
    v_tipo_entidad number;
  BEGIN

    v_id_Ult_autor_repres := 0;

    -- Busco el tipo de la entidad
    select id_tipo_entidad into v_tipo_entidad
    from ipj.t_legajos l
    where
      l.id_legajo = p_id_legajo;

    -- BUSCO el ultimo tr�mite de representantes solo para SAS
    if p_id_ubicacion_ent in (IPJ.TYPES.C_AREA_SXA) and v_tipo_entidad = 40 then
      -- Busco el ultimo tramite que guardo representantes
      select nvl(sum(Id_Tramite_Ipj), p_Id_Tramite_Ipj) into v_id_Ult_autor_repres
      from
        ( select r.Id_Tramite_Ipj
          from ipj.t_entidades_repres_hist r join ipj.t_entidades e
              on r.Id_Tramite_Ipj = e.Id_Tramite_Ipj and r.id_legajo = e.id_legajo
            join ipj.t_tramitesipj tr
              on r.id_tramite_ipj = tr.id_tramite_ipj
          where
            r.id_legajo = p_id_legajo and
            e.Matricula_Version <= p_matricula_version and
            e.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = p_id_ubicacion_ent) and
            ( r.id_tramite_ipj = p_id_tramite_ipj or
              tr.id_tramite_ipj_padre = p_id_tramite_ipj or
              (tr.id_estado_ult >= IPJ.TYPES.C_ESTADOS_COMPLETADO and tr.id_estado_ult < IPJ.TYPES.C_ESTADOS_RECHAZADO)
            )
          order by e.matricula asc , matricula_version desc, nvl(anio, 0) desc, folio desc, r.Id_Tramite_Ipj desc
          ) t
      where rownum = 1;
    end if;

    return v_id_Ult_autor_repres;
  Exception
    When Others Then
      Return 0;
  End FC_Ult_Tram_Repres;

  FUNCTION FC_Fecha_Orig_Admin(p_id_legajo IN number, p_id_admin in number) return varchar2 is
  /**********************************************************
    Busca la fecha de fin original, del primer tr�mite que carg� la autoridad
  **********************************************************/
    v_id_tipo_entidad number;
    v_fecha_orig date;
    v_id_ubicacion NUMBER;
  BEGIN
    -- Busco el tipo de entidad, para poder ordenar los tr�mites
    select e.id_tipo_entidad, e.id_ubicacion into v_id_tipo_entidad, v_id_ubicacion
    from ipj.t_legajos l
    JOIN ipj.t_tipos_entidades e ON l.id_tipo_entidad = e.id_tipo_entidad
    where
      id_legajo = p_id_legajo;

    if v_id_ubicacion = IPJ.TYPES.C_AREA_CYF then
      -- Busco la primer fecha de fin cargado para la autoridad
      select fecha_fin into v_fecha_orig
      from
        ( select ad.*, FC_Buscar_Prim_Acta(ad.id_tramite_ipj) Fecha_Acta
          from ipj.t_entidades_admin_hist ad join ipj.t_tramitesipj tr
            on ad.Id_Tramite_Ipj = tr.Id_Tramite_Ipj
          where
            ad.id_legajo = p_id_legajo and
            ad.id_admin = p_id_admin and
            tr.id_estado_ult < IPJ.TYPES.C_ESTADOS_RECHAZADO
          order by Fecha_Acta ASC
          ) t
      where rownum = 1;

    else -- ES SRL O SXA
      -- Busco el ultimo tramite que guardo Administradores
      select fecha_fin into v_fecha_orig
      from
        ( select h.*
          from ipj.t_entidades_admin_hist h join ipj.t_entidades e
              on h.Id_Tramite_Ipj = e.Id_Tramite_Ipj and h.id_legajo = e.id_legajo
            join ipj.t_tramitesipj tr
              on h.id_tramite_ipj = tr.id_tramite_ipj
          where
            h.id_legajo = p_id_legajo and
            e.id_tipo_entidad = v_id_tipo_entidad and
            h.id_admin = p_id_admin and
            (tr.id_estado_ult >= IPJ.TYPES.C_ESTADOS_COMPLETADO and tr.id_estado_ult < IPJ.TYPES.C_ESTADOS_RECHAZADO)
          order by e.matricula asc , matricula_version desc, nvl(e.anio, 0) desc, e.folio desc, h.Id_Tramite_Ipj desc
          ) t
      where rownum = 1;
    end if;

    return to_char(v_fecha_orig, 'dd/mm/rrrr');
  Exception
    When Others Then
      Return '';
  End FC_Fecha_Orig_Admin;

  FUNCTION FC_Fecha_Orig_Sindico(p_id_legajo IN number, p_id_entidad_sindico in number) return varchar2 is
  /**********************************************************
    Busca la fecha de fin original, del primer tr�mite que carg� la autoridad
  **********************************************************/
    v_id_tipo_entidad number;
    v_fecha_orig date;
    v_id_ubicacion NUMBER;
  BEGIN
    -- Busco el tipo de entidad, para poder ordenar los tr�mites
    select e.id_tipo_entidad, e.id_ubicacion into v_id_tipo_entidad, v_id_ubicacion
    from ipj.t_legajos l
    JOIN ipj.t_tipos_entidades e ON l.id_tipo_entidad = e.id_tipo_entidad
    where
      id_legajo = p_id_legajo;

    if v_id_ubicacion = IPJ.TYPES.C_AREA_CYF then
      -- Busco la primer fecha fin del sindico
      select fecha_baja into v_fecha_orig
      from
        ( select ad.*, FC_Buscar_Prim_Acta(ad.id_tramite_ipj) Fecha_Acta
          from ipj.t_entidades_sindico_hist ad join ipj.t_tramitesipj tr
            on ad.Id_Tramite_Ipj = tr.Id_Tramite_Ipj
          where
            ad.id_legajo = p_id_legajo and
            ad.id_entidad_sindico = p_id_entidad_sindico and
            tr.id_estado_ult < IPJ.TYPES.C_ESTADOS_RECHAZADO
          order by Fecha_Acta ASC
          ) t
      where rownum = 1;

    else -- ES SRL O SXA
      -- Busco el ultimo tramite que guardo Administradores
      select fecha_baja into v_fecha_orig
      from
        ( select h.*
          from ipj.t_entidades_sindico_hist h join ipj.t_entidades e
              on h.Id_Tramite_Ipj = e.Id_Tramite_Ipj and h.id_legajo = e.id_legajo
            join ipj.t_tramitesipj tr
              on h.id_tramite_ipj = tr.id_tramite_ipj
          where
            h.id_legajo = p_id_legajo and
            e.id_tipo_entidad = v_id_tipo_entidad and
            h.id_entidad_sindico = p_id_entidad_sindico and
            (tr.id_estado_ult >= IPJ.TYPES.C_ESTADOS_COMPLETADO and tr.id_estado_ult < IPJ.TYPES.C_ESTADOS_RECHAZADO)
          order by e.matricula asc , matricula_version desc, nvl(e.anio, 0) desc, e.folio desc, h.Id_Tramite_Ipj desc
          ) t
      where rownum = 1;
    end if;

    return to_char(v_fecha_orig, 'dd/mm/rrrr');
  Exception
    When Others Then
      Return '';
  End FC_Fecha_Orig_Sindico;

  FUNCTION FC_Validar_Equidad_Genero(p_legajo IN NUMBER, p_tramite_ipj IN NUMBER) RETURN VARCHAR2 IS
    /**********************************************************
      Busca que la cantidad de organismos de sexo femenino sea mayor igual al de sexo masculino
    **********************************************************/
    v_result  VARCHAR2(1);
    v_row_ent ipj.t_entidades%ROWTYPE;

    v_cant_adm_h NUMBER;
    v_cant_adm_m NUMBER;

    v_cant_fis_h NUMBER;
    v_cant_fis_m NUMBER;

    v_cant_rep_h NUMBER;
    v_cant_rep_m NUMBER;

    v_cant_fid_h NUMBER;
    v_cant_fid_m NUMBER;

    v_adm_sn VARCHAR2(1);
    v_fis_sn VARCHAR2(1);
    v_rep_sn VARCHAR2(1);

    --Cursor Org. Administraci�n
    CURSOR cur_adm IS
       SELECT id_sexo, COUNT(id_sexo) cant_hm
         FROM ipj.t_integrantes i
        WHERE i.id_integrante IN
              (SELECT ie.id_integrante
                 FROM ipj.t_entidades_admin_hist h, ipj.t_entidades_admin ie, ipj.t_tramitesipj tr, ipj.t_tipos_integrante ti
                WHERE h.id_legajo = ie.id_legajo
                  AND h.id_admin = ie.id_admin
                  AND tr.id_tramite_ipj = ie.id_tramite_ipj
                  AND ie.id_tipo_integrante = ti.id_tipo_integrante
                  AND ti.es_suplente = 'N'
                  AND ie.id_integrante IS NOT NULL
                  AND tr.id_estado_ult BETWEEN 100 AND 199
                  AND ie.id_legajo = p_legajo
                  --AND ie.id_tramite_ipj = p_tramite_ipj
                  AND h.id_tramite_ipj =
                      ipj.entidad_persjur.fc_ult_tram_admin(v_row_ent.id_legajo
                                                           ,nvl((SELECT te.id_ubicacion
                                                                  FROM ipj.t_tipos_entidades te
                                                                 WHERE te.id_tipo_entidad = v_row_ent.id_tipo_entidad)
                                                               ,(SELECT te.id_ubicacion
                                                                  FROM ipj.t_legajos l2,
                                                                       ipj.t_tipos_entidades te
                                                                 WHERE l2.id_tipo_entidad =
                                                                       te.id_tipo_entidad
                                                                   AND l2.id_legajo = v_row_ent.id_legajo))
                                                           ,nvl(v_row_ent.matricula_version, 0)
                                                           ,p_tramite_ipj)
                  AND (h.fecha_fin >= trunc(SYSDATE) OR h.id_motivo_baja IS NULL))
        GROUP BY id_sexo
        ORDER BY id_sexo ASC;

    --Cursor Org. Fiscalizaci�n
    CURSOR cur_fisc IS
      SELECT id_sexo, COUNT(id_sexo) cant_hm
        FROM ipj.t_integrantes i
       WHERE i.id_integrante IN
             (SELECT ie.id_integrante
                 FROM ipj.t_entidades_sindico_hist h, ipj.t_entidades_sindico ie, ipj.t_tramitesipj tr, ipj.t_tipos_integrante ti
                WHERE h.id_legajo = ie.id_legajo
                  AND h.id_entidad_sindico = ie.id_entidad_sindico
                  AND tr.id_tramite_ipj = ie.id_tramite_ipj
                  AND ie.id_tipo_integrante = ti.id_tipo_integrante
                  AND ti.es_suplente = 'N'
                  AND ie.id_integrante IS NOT NULL
                  AND tr.id_estado_ult BETWEEN 100 AND 199
                  AND ie.id_legajo = p_legajo
                  --AND ie.id_tramite_ipj = p_tramite_ipj
                  AND h.id_tramite_ipj =
                      ipj.entidad_persjur.fc_ult_tram_sindico(v_row_ent.id_legajo
                                                           ,nvl((SELECT te.id_ubicacion
                                                                  FROM ipj.t_tipos_entidades te
                                                                 WHERE te.id_tipo_entidad = v_row_ent.id_tipo_entidad)
                                                               ,(SELECT te.id_ubicacion
                                                                  FROM ipj.t_legajos l2,
                                                                       ipj.t_tipos_entidades te
                                                                 WHERE l2.id_tipo_entidad = te.id_tipo_entidad
                                                                   AND l2.id_legajo = v_row_ent.id_legajo))
                                                           ,nvl(v_row_ent.matricula_version, 0)
                                                           ,p_tramite_ipj)
                  AND (h.fecha_baja >= trunc(SYSDATE) OR h.id_motivo_baja is null))
        GROUP BY id_sexo
        ORDER BY id_sexo ASC;

    --Cursor Org. Representaci�n
      CURSOR cur_repres IS
        SELECT id_sexo, COUNT(id_sexo) cant_hm
          FROM ipj.t_integrantes i
         WHERE i.id_integrante IN
               (SELECT er.id_integrante
                  FROM ipj.t_entidades_representante er, ipj.t_tramitesipj tr
                 WHERE er.id_integrante IS NOT NULL
                   AND tr.id_tramite_ipj = er.id_tramite_ipj
                   AND tr.id_estado_ult BETWEEN 100 AND 199
                   AND er.id_legajo = p_legajo
                   --AND er.id_tramite_ipj = p_tramite_ipj
                   AND er.id_tramite_ipj =
                       ipj.entidad_persjur.fc_ult_tram_repres(v_row_ent.id_legajo
                                                             ,nvl((SELECT te.id_ubicacion
                                                                    FROM ipj.t_tipos_entidades te
                                                                   WHERE te.id_tipo_entidad = v_row_ent.id_tipo_entidad)
                                                                 ,(SELECT te.id_ubicacion
                                                                    FROM ipj.t_legajos l2,
                                                                         ipj.t_tipos_entidades te
                                                                   WHERE l2.id_tipo_entidad = te.id_tipo_entidad
                                                                     AND l2.id_legajo = v_row_ent.id_legajo))
                                                             ,nvl(v_row_ent.matricula_version, 0)
                                                             ,p_tramite_ipj)
                   AND (er.fecha_baja >= trunc(SYSDATE) OR er.id_motivo_baja IS NULL))
         GROUP BY id_sexo
         ORDER BY id_sexo ASC;

    --Cursor Fideicomiso
    CURSOR cur_fid IS
      SELECT id_sexo, COUNT(id_sexo) cant_hm
        FROM ipj.t_integrantes i
       WHERE i.id_integrante IN (SELECT o.id_integrante
                                   FROM ipj.t_entidades_fideicom o, ipj.t_tramitesipj tr
                                  WHERE o.id_legajo = p_legajo
                                    AND o.id_tramite_ipj = p_tramite_ipj
                                    AND o.id_tramite_ipj = tr.id_tramite_ipj
                                    AND tr.id_estado_ult between 100 and 199
                                    AND (o.fecha_fin is null or o.fecha_fin >= to_date(sysdate, 'DD/MM/RRRR'))
                                 UNION
                                 SELECT o.id_integrante
                                   FROM ipj.t_entidades_benef o, ipj.t_tramitesipj tr
                                  WHERE o.id_legajo = p_legajo
                                    AND o.id_tramite_ipj = p_tramite_ipj
                                    AND o.id_tramite_ipj = tr.id_tramite_ipj
                                    AND tr.id_estado_ult between 100 and 199
                                    AND (o.fecha_fin is null or o.fecha_fin >= to_date(sysdate, 'DD/MM/RRRR'))
                                 UNION
                                 SELECT id_integrante
                                   FROM ipj.t_entidades_fiduciantes o, ipj.t_tramitesipj tr
                                  WHERE o.id_legajo = p_legajo
                                    AND o.id_tramite_ipj = p_tramite_ipj
                                    AND o.id_tramite_ipj = tr.id_tramite_ipj
                                    AND tr.id_estado_ult between 100 and 199
                                    AND (o.fecha_fin is null or o.fecha_fin >= to_date(sysdate, 'DD/MM/RRRR'))
                                 UNION
                                 SELECT id_integrante
                                   FROM ipj.t_entidades_fiduciarios o, ipj.t_tramitesipj tr
                                  WHERE o.id_legajo = p_legajo
                                    AND o.id_tramite_ipj = p_tramite_ipj
                                    AND o.id_tramite_ipj = tr.id_tramite_ipj
                                    AND tr.id_estado_ult between 100 and 199
                                    AND (o.fecha_fin is null or o.fecha_fin >= to_date(sysdate, 'DD/MM/RRRR')))
       GROUP BY id_sexo
       ORDER BY id_sexo ASC;

  BEGIN

    -- Corroboro Organo de Administraci�n y Fiscalizaci�n
    SELECT *
      INTO v_row_ent
      FROM ipj.t_entidades e
     WHERE e.id_legajo = p_legajo
       AND e.id_tramite_ipj = p_tramite_ipj;

    IF v_row_ent.id_tramite_ipj IS NOT NULL THEN

      --Si el tipo de Entidad es Fideicomiso
      IF v_row_ent.id_tipo_entidad = 29 THEN

        FOR c_fid IN cur_fid
        LOOP

          CASE
            WHEN c_fid.id_sexo = '01' THEN
              v_cant_fid_h := c_fid.cant_hm;
            WHEN c_fid.id_sexo = '02' THEN
              v_cant_fid_m := c_fid.cant_hm;
          END CASE;

        END LOOP;

        IF nvl(v_cant_fid_h,0) = 0 AND nvl(v_cant_fid_m,0) = 0 THEN
          v_result := 'N';
        ELSIF nvl(v_cant_fid_m,0) >= nvl(v_cant_fid_h,0)THEN
          v_result := 'S';
        ELSE
          v_result := 'N';
        END IF;

      ELSE

        IF nvl(v_row_ent.requiere_sindico, 'S') = 'S' THEN

          --ADM
          FOR c_adm IN cur_adm
          LOOP

            CASE
              WHEN c_adm.id_sexo = '01' THEN
                v_cant_adm_h := c_adm.cant_hm;
              WHEN c_adm.id_sexo = '02' THEN
                v_cant_adm_m := c_adm.cant_hm;
            END CASE;

          END LOOP;

          IF nvl(v_cant_adm_m,0) = 0 AND nvl(v_cant_adm_h,0) = 0 THEN
            v_adm_sn := 'N';
          ELSIF nvl(v_cant_adm_m,0) >= nvl(v_cant_adm_h,0) THEN
            v_adm_sn := 'S';
          ELSE
            v_adm_sn := 'N';
          END IF;

          --FISC
          FOR c_fisc IN cur_fisc
          LOOP

            CASE
              WHEN c_fisc.id_sexo = '01' THEN
                v_cant_fis_h := c_fisc.cant_hm;
              WHEN c_fisc.id_sexo = '02' THEN
                v_cant_fis_m := c_fisc.cant_hm;
            END CASE;

          END LOOP;

          IF nvl(v_cant_fis_m,0) = 0 AND nvl(v_cant_fis_h,0) = 0 THEN
            v_fis_sn := 'N';
          ELSIF nvl(v_cant_fis_m,0) >= nvl(v_cant_fis_h,0) THEN
            v_fis_sn := 'S';
          ELSE
            v_fis_sn := 'N';
          END IF;

          --Si el Tipo Entidad SAS Busco Org. Representaci�n
          IF v_row_ent.id_tipo_entidad = 40 THEN
             --REPRES
              FOR c_rep IN cur_repres
              LOOP

                CASE
                  WHEN c_rep.id_sexo = '01' THEN
                    v_cant_rep_h := c_rep.cant_hm;
                  WHEN c_rep.id_sexo = '02' THEN
                    v_cant_rep_m := c_rep.cant_hm;
                END CASE;

              END LOOP;

              IF nvl(v_cant_rep_m,0) = 0 AND nvl(v_cant_rep_h,0) = 0 THEN
                v_rep_sn := 'N';
              ELSIF nvl(v_cant_rep_m,0) >= nvl(v_cant_rep_h,0) THEN
                v_rep_sn := 'S';
              ELSE
                v_rep_sn := 'N';
              END IF;

              IF v_rep_sn = 'S' AND v_fis_sn = 'S' THEN
                v_result := 'S';
              ELSIF v_rep_sn = 'S' AND v_adm_sn = 'S' THEN
                v_result := 'S';
              ELSIF v_adm_sn = 'S' AND v_fis_sn = 'S' THEN
                v_result := 'S';
              ELSE
                v_result := 'N';
              END IF;

          ELSE

              IF v_adm_sn = 'S' AND v_fis_sn = 'S' THEN
                v_result := 'S';
              ELSE
                v_result := 'N';
              END IF;

          END IF;

        ELSE

          --ADM
          FOR c_adm IN cur_adm
          LOOP

            CASE
              WHEN c_adm.id_sexo = '01' THEN
                v_cant_adm_h := c_adm.cant_hm;
              WHEN c_adm.id_sexo = '02' THEN
                v_cant_adm_m := c_adm.cant_hm;
            END CASE;

          END LOOP;

          IF nvl(v_cant_adm_m,0) = 0 AND nvl(v_cant_adm_h,0) = 0 THEN
            v_adm_sn := 'N';
          ELSIF nvl(v_cant_adm_m,0) >= nvl(v_cant_adm_h,0) THEN
            v_adm_sn := 'S';
          ELSE
            v_adm_sn := 'N';
          END IF;

          --Si el Tipo Entidad SAS Busco Org. Representaci�n
          IF v_row_ent.id_tipo_entidad = 40 THEN
             --REPRES
              FOR c_rep IN cur_repres
              LOOP

                CASE
                  WHEN c_rep.id_sexo = '01' THEN
                    v_cant_rep_h := c_rep.cant_hm;
                  WHEN c_rep.id_sexo = '02' THEN
                    v_cant_rep_m := c_rep.cant_hm;
                END CASE;

              END LOOP;

              IF nvl(v_cant_rep_m,0) = 0 AND nvl(v_cant_rep_h,0) = 0 THEN
                v_rep_sn := 'N';
              ELSIF nvl(v_cant_rep_m,0) >= nvl(v_cant_rep_h,0) THEN
                v_rep_sn := 'S';
              ELSE
                v_rep_sn := 'N';
              END IF;

              IF v_rep_sn = 'S' AND v_adm_sn = 'S' THEN
                v_result := 'S';
              ELSE
                v_result := 'N';
              END IF;

          ELSE

             IF v_adm_sn = 'S' THEN
              v_result := 'S';
             ELSE
              v_result := 'N';
             END IF;

          END IF;

        END IF;

      END IF;
    ELSE
      v_result := NULL;
    END IF;

    RETURN v_result;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN '';
  END FC_Validar_Equidad_Genero;

  PROCEDURE SP_Validar_Equidad_Genero(o_rdo            OUT VARCHAR2
                                     ,o_tipo_mensaje   OUT NUMBER
                                     ,p_id_legajo      IN NUMBER) IS

  v_eg_sn VARCHAR2(1);
  v_Id_Tramite_Ipj NUMBER;
  v_id_tramiteipj_accion NUMBER;

  BEGIN
    /*****************************************************
      Valida que la cantidad de organismos de sexo femenino sea mayor igual al de sexo masculino
      para no aplicar la tasa.
    ******************************************************/
    IF ipj.varios.fc_buscar_variable_config('HAB_LOG_GESTION') = '1' THEN
      ipj.varios.sp_guardar_log_ol(p_metodo      => 'SP_Validar_Equidad_Genero'
                                  ,p_nivel       => 'Validar Equidad Genero'
                                  ,p_origen      => 'DB'
                                  ,p_usr_log     => 'DB'
                                  ,p_usr_windows => 'Sistema'
                                  ,p_ip_pc       => NULL
                                  ,p_excepcion   => 'Id Tramite Ipj = ' || to_char(p_id_legajo));

    END IF;

    -- CUERPO DEL PROCEDIMEINTO
    -- Busco el ultimo tr�mite de la entidad
    IPJ.Entidad_PersJur.SP_Buscar_Entidad_Tramite(
        o_Id_Tramite_Ipj => v_Id_Tramite_Ipj,
        o_id_tramiteipj_accion => v_id_tramiteipj_accion,
        p_id_legajo => p_id_legajo,
        p_abiertos => 'N');

    v_eg_sn := FC_Validar_Equidad_Genero(p_id_legajo,v_Id_Tramite_Ipj);

    o_rdo := v_eg_sn;
    o_tipo_mensaje := ipj.types.c_tipo_mens_ok;

  EXCEPTION
    WHEN OTHERS THEN
       o_rdo := '';
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      ROLLBACK;
  END SP_Validar_Equidad_Genero;

  PROCEDURE SP_Bajar_Docs_Tramite(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number)
  IS
  /**********************************************************
    BUG 8367
    Si el tr�mite viene del Portal y no cuenta con documentos asociados,
    intenta bajarlos desde el portal.
  **********************************************************/
    v_codigo_online number;
    v_existen_docs number;
  BEGIN
    -- Busco el codigo onlin asociado al tr�mite
    select nvl(codigo_online, 0) into v_codigo_online
    from ipj.t_tramitesipj
    where
      id_tramite_ipj = p_id_tramite_ipj;

    -- Veo si falta pasar alg�n documento
    select count(1) into v_existen_docs
    from ipj.t_ol_entidad_documentos od
    where
      codigo_online = v_codigo_online and
      nvl(id_documento, 0) > 0 and
      not exists ( select *
                       from ipj.t_entidades_documentos d
                       where
                         d.id_tramite_ipj = p_id_tramite_ipj and
                         d.id_legajo = p_id_legajo and
                         d.id_tipo_documento_cdd = od.id_tipo_documento_cdd
                     );

    -- Si es de un t�mite online, y no tiene documentos, inserto los del portal
    if v_codigo_online > 0 and v_existen_docs > 0 then
      INSERT INTO IPJ.T_ENTIDADES_DOCUMENTOS (Id_Tramite_Ipj, Id_Legajo, id_documento,
        n_documento, observacion, cuil_usr, fecha_alta, id_tipo_documento_cdd,
        fecha_notarial, cuil_escrib_orig, nro_escribania_orig, id_cargo_escrib_orig,
        n_cargo_escrib_orig, detalle_escribania_orig )
      select
        p_Id_Tramite_Ipj, p_Id_Legajo, id_documento, n_documento, observacion,
        cuil_usr, fecha_alta, id_tipo_documento_cdd, fecha_notarial, cuil_escrib_orig,
        nro_escribania_orig, id_cargo_escrib_orig, n_cargo_escrib_orig, detalle_escribania_orig
      from IPJ.t_ol_entidad_documentos od
      where
        codigo_online = v_codigo_online and
        nvl(id_documento, 0) > 0 and
        not exists ( select *
                         from ipj.t_entidades_documentos d
                         where
                           d.id_tramite_ipj = p_id_tramite_ipj and
                           d.id_legajo = p_id_legajo and
                           d.id_tipo_documento_cdd = od.id_tipo_documento_cdd
                       );
    end if;

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
    WHEN OTHERS THEN
      o_rdo := trunc('SP_Bajar_Docs_Tramite - ' || To_Char(SQLCODE) || '-' || SQLERRM, 4000);
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Bajar_Docs_Tramite;

  PROCEDURE SP_Actualizar_Borrador(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number,
    p_borrador in char)
  IS
  BEGIN
    /***********************************************************
      Dado un tramite y legajo, marco todos los detalles como No Borrador
    ************************************************************/
    update IPJ.t_entidades set borrador = p_borrador
    where
      Id_Tramite_Ipj = p_Id_Tramite_Ipj and
      (p_id_legajo is null or id_legajo = p_id_legajo);

    update IPJ.t_entidades_rubrica set borrador = p_borrador
    where
      Id_Tramite_Ipj = p_Id_Tramite_Ipj and
      (p_id_legajo is null or id_legajo = p_id_legajo);

    update IPJ.t_entidades_acta set borrador = p_borrador
    where
      Id_Tramite_Ipj = p_Id_Tramite_Ipj and
      (p_id_legajo is null or id_legajo = p_id_legajo);

    update IPJ.t_entidades_rubros set borrador = p_borrador
    where
      Id_Tramite_Ipj = p_Id_Tramite_Ipj and
      (p_id_legajo is null or id_legajo = p_id_legajo);

    update IPJ.t_entidades_admin set borrador = p_borrador
    where
      Id_Tramite_Ipj = p_Id_Tramite_Ipj and
      (p_id_legajo is null or id_legajo = p_id_legajo);

    update IPJ.t_entidades_socios set borrador = p_borrador
    where
      Id_Tramite_Ipj = p_Id_Tramite_Ipj and
      (p_id_legajo is null or id_legajo = p_id_legajo);

    update Ipj.T_Entidades_Socios_Copro
    set borrador = p_borrador
    where
      id_entidad_socio in (select id_entidad_socio
                                    from IPJ.t_entidades_socios
                                    where
                                      Id_Tramite_Ipj = p_Id_Tramite_Ipj and
                                      (p_id_legajo is null or id_legajo = p_id_legajo)
                                    );

    update Ipj.T_Entidades_Socios_Usuf
    set borrador = p_borrador
    where
      id_entidad_socio in (select id_entidad_socio
                                    from IPJ.t_entidades_socios
                                    where
                                      Id_Tramite_Ipj = p_Id_Tramite_Ipj and
                                      (p_id_legajo is null or id_legajo = p_id_legajo)
                                    );

    update IPJ.t_entidades_socio_capital set borrador = p_borrador
    where
      Id_Tramite_Ipj = p_Id_Tramite_Ipj and
      (p_id_legajo is null or id_legajo = p_id_legajo);

    update IPJ.t_entidades_socios_acciones set borrador = p_borrador
    where
      Id_Tramite_Ipj = p_Id_Tramite_Ipj and
      (p_id_legajo is null or id_legajo = p_id_legajo);

    update IPJ.t_entidades_acciones set borrador = p_borrador
    where
      Id_Tramite_Ipj = p_Id_Tramite_Ipj and
      (p_id_legajo is null or id_legajo = p_id_legajo);

    update IPJ.t_entidades_ins_legal set borrador = p_borrador
    where
      Id_Tramite_Ipj = p_Id_Tramite_Ipj and
      (p_id_legajo is null or id_legajo = p_id_legajo);

    update IPJ.T_ENTIDADES_BALANCE set borrador = p_borrador
    where
      Id_Tramite_Ipj = p_Id_Tramite_Ipj and
      (p_id_legajo is null or id_legajo = p_id_legajo);

    update IPJ.T_ENTIDADES_REPRESENTANTE set borrador = p_borrador
    where
      Id_Tramite_Ipj = p_Id_Tramite_Ipj and
      (p_id_legajo is null or id_legajo = p_id_legajo);

    update IPJ.T_ENTIDADES_RELACIONADAS set borrador = p_borrador
    where
      Id_Tramite_Ipj = p_Id_Tramite_Ipj and
      (p_id_legajo is null or id_legajo = p_id_legajo);

    update IPJ.T_ENTIDADES_SINDICO set borrador = p_borrador
    where
      Id_Tramite_Ipj = p_Id_Tramite_Ipj and
      (p_id_legajo is null or id_legajo = p_id_legajo);

    update IPJ.t_entidades_fiduciarios set borrador = p_borrador
    where
      Id_Tramite_Ipj = p_Id_Tramite_Ipj and
      (p_id_legajo is null or id_legajo = p_id_legajo);

    update IPJ.t_entidades_fiduciantes set borrador = p_borrador
    where
      Id_Tramite_Ipj = p_Id_Tramite_Ipj and
      (p_id_legajo is null or id_legajo = p_id_legajo);

    update IPJ.t_entidades_fideicom set borrador = p_borrador
    where
      Id_Tramite_Ipj = p_Id_Tramite_Ipj and
      (p_id_legajo is null or id_legajo = p_id_legajo);

    update IPJ.t_entidades_benef set borrador = p_borrador
    where
      Id_Tramite_Ipj = p_Id_Tramite_Ipj and
      (p_id_legajo is null or id_legajo = p_id_legajo);

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     WHEN OTHERS THEN
       o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Actualizar_Borrador;

  PROCEDURE SP_Buscar_CUIT_Tramite(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_cuit out varchar2,
    o_denominacion out varchar2,
    p_Id_Tramite_Ipj in number,
    p_id_tramiteipj_accion in number)
  IS
    /***********************************************************
      Dado un tr�mite y una acci�n, devuelve el cuit y nombre de la empresa asociada
    ************************************************************/
    v_cuit_ent varchar2(20);
    v_cuit_leg varchar2(20);
    v_denominacion_ent varchar2(250);
    v_denominacion_leg varchar2(250);
    v_id_legajo number(15);
  BEGIN
    -- Busco el legajo asociado a la accion
    select id_legajo into v_id_legajo
    from ipj.t_tramitesipj_acciones
    where
      Id_Tramite_Ipj = p_Id_Tramite_Ipj and
      Id_Tramiteipj_Accion = p_id_tramiteipj_accion;

    if v_id_legajo is not null then
      -- Busco CUIT y Denominacion en Entidad
      begin
        select e.cuit, e.denominacion_1 into v_cuit_ent, v_denominacion_ent
        from ipj.t_entidades e
        where
          e.Id_Tramite_Ipj = p_Id_Tramite_Ipj and
          e.id_legajo = v_id_legajo;
      exception
        when NO_DATA_FOUND then
          v_cuit_ent := null;
      end;

      -- Busco el CUIT y Denominacion en legajo
      begin
        select e.cuit, e.denominacion_sia into v_cuit_leg, v_denominacion_leg
        from ipj.t_legajos e
        where
          e.id_legajo = v_id_legajo;
      exception
        when NO_DATA_FOUND then
          v_cuit_leg := null;
      end;

      o_cuit := nvl(v_cuit_ent, v_cuit_leg);
      o_denominacion := nvl(v_denominacion_ent, v_denominacion_leg);
    else
      o_cuit := null;
      o_denominacion := null;
    end if;

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     WHEN OTHERS THEN
       o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Buscar_CUIT_Tramite;

  PROCEDURE SP_Buscar_Ult_Tramite(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_tramite_base out number,
    o_id_legajo_base out number,
    o_ubicacion_ent out number,
    p_id_legajo in number,
    p_id_ubicacion in number)
  IS
    /***********************************************************
      Busca el ultimo tramite de una empresa, excluyendo informes y acciones de archivos.
      Solo tiene en cuenta los tr�mites no rechazados.
      Si no encuentra la empresa en el �rea actual, la busca para cualquier �rea
    ************************************************************/
    v_ubicacion_legajo number;
  BEGIN
    -- Busco la ubicacion de la entidad, para definir el orden de los tr�mites.
    select e.id_ubicacion into v_ubicacion_legajo
    from ipj.t_legajos l join ipj.t_tipos_entidades e
        on l.id_tipo_entidad = e.id_tipo_entidad
    where
      l.id_legajo = p_id_legajo;

    begin
      -- Para SRL y SA ordeno por versi�n, matricula, folio, tomoi, a�o y tr�mite
      if v_ubicacion_legajo in (IPJ.TYPES.C_AREA_SRL, IPJ.TYPES.C_AREA_SXA) then
        select Id_Tramite_Ipj, id_legajo, ubicacion_ent
          into o_id_tramite_base, o_id_legajo_base, o_ubicacion_ent
        from
          ( select e.Id_Tramite_Ipj, e.id_legajo,
              (select id_ubicacion from ipj.t_tipos_entidades te where te.id_tipo_entidad = e.id_tipo_entidad) ubicacion_ent
            from ipj.t_entidades e join ipj.t_tramitesipj tr
                on e.Id_Tramite_Ipj = tr.Id_Tramite_Ipj
            where
              e.id_legajo = p_id_legajo and
              e.borrador = 'N' and
              e.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = p_id_ubicacion) and
              TR.ID_ESTADO_ULT < IPJ.Types.c_Estados_Rechazado
              and exists
                ( select * from ipj.t_tramitesipj_acciones ac
                  where
                    ac.Id_Tramite_Ipj = tr.Id_Tramite_Ipj and
                    ac.id_legajo = e.id_legajo and
                    id_tipo_accion not in (IPJ.Types.c_Tipo_Acc_Retiro,IPJ.Types.c_Tipo_Acc_Archivar_Expediente) and
                    id_tipo_accion not in (select id_tipo_accion from ipj.t_informes_tramite where id_informe <> 18) -- Informe Caratula
                )
            order by matricula asc , matricula_version desc, nvl(anio, 0) desc, folio desc, Id_Tramite_Ipj desc
          ) t
        where
          rownum = 1;
      end if;

      -- Para ACyF ordeno por fecha de acta y tr�mite
      if v_ubicacion_legajo in (IPJ.TYPES.C_AREA_CYF) then
        select Id_Tramite_Ipj, id_legajo, ubicacion_ent
          into o_id_tramite_base, o_id_legajo_base, o_ubicacion_ent
        from
          ( select e.Id_Tramite_Ipj, e.id_legajo,
              (select id_ubicacion from ipj.t_tipos_entidades te where te.id_tipo_entidad = e.id_tipo_entidad) ubicacion_ent,
              IPJ.ENTIDAD_PERSJUR.FC_Buscar_Prim_Acta (tr.id_tramite_ipj) Fecha_Acta
            from ipj.t_entidades e join ipj.t_tramitesipj tr
                on e.Id_Tramite_Ipj = tr.Id_Tramite_Ipj
            where
              e.id_legajo = p_id_legajo and
              e.borrador = 'N' and
              e.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = p_id_ubicacion) and
              TR.ID_ESTADO_ULT < IPJ.Types.c_Estados_Rechazado
              and exists
                ( select * from ipj.t_tramitesipj_acciones ac
                  where
                    ac.Id_Tramite_Ipj = tr.Id_Tramite_Ipj and
                    ac.id_legajo = e.id_legajo and
                    id_tipo_accion not in (IPJ.Types.c_Tipo_Acc_Retiro,IPJ.Types.c_Tipo_Acc_Archivar_Expediente) and
                    id_tipo_accion not in (select id_tipo_accion from ipj.t_informes_tramite where id_informe <> 18) -- Informe Caratula
                )
            order by Fecha_Acta desc , Id_Tramite_Ipj desc
          ) t
        where
          rownum = 1;
      end if;
    exception
      when NO_DATA_FOUND then
        o_id_tramite_base := 0;
        o_id_legajo_base := 0;
        o_ubicacion_ent := 0;
    end;

    -- Sino la encontr�, buso con un cambio de �rea
    if o_id_tramite_base = 0 then
      begin
        -- Para SRL y SA ordeno por versi�n, matricula, folio, tomoi, a�o y tr�mite
        if p_id_ubicacion in (IPJ.TYPES.C_AREA_SRL, IPJ.TYPES.C_AREA_SXA) then
          select Id_Tramite_Ipj, id_legajo, ubicacion_ent
            into o_id_tramite_base, o_id_legajo_base, o_ubicacion_ent
          from
            ( select e.Id_Tramite_Ipj, e.id_legajo,
                (select id_ubicacion from ipj.t_tipos_entidades te where te.id_tipo_entidad = e.id_tipo_entidad) ubicacion_ent
              from ipj.t_entidades e join ipj.t_tramitesipj tr
                  on e.Id_Tramite_Ipj = tr.Id_Tramite_Ipj
              where
                e.id_legajo = p_id_legajo and
                e.borrador = 'N' and
                TR.ID_ESTADO_ULT < IPJ.Types.c_Estados_Rechazado
                and exists
                  ( select * from ipj.t_tramitesipj_acciones ac
                    where
                      ac.Id_Tramite_Ipj = tr.Id_Tramite_Ipj and
                      ac.id_legajo = e.id_legajo and
                      id_tipo_accion not in (IPJ.Types.c_Tipo_Acc_Retiro,IPJ.Types.c_Tipo_Acc_Archivar_Expediente) and
                      id_tipo_accion not in (select id_tipo_accion from ipj.t_informes_tramite where id_informe <> 18) -- Informe Caratula
                  )
              order by matricula asc , matricula_version desc, nvl(anio, 0) desc, folio desc, Id_Tramite_Ipj desc
            ) t
          where
            rownum = 1;
        end if;

        -- Para SRL y SA ordeno por versi�n, matricula, folio, tomoi, a�o y tr�mite
        if p_id_ubicacion in (IPJ.TYPES.C_AREA_CYF) then
          select Id_Tramite_Ipj, id_legajo, ubicacion_ent
          into o_id_tramite_base, o_id_legajo_base, o_ubicacion_ent
          from
            ( select e.Id_Tramite_Ipj, e.id_legajo,
                (select id_ubicacion from ipj.t_tipos_entidades te where te.id_tipo_entidad = e.id_tipo_entidad) ubicacion_ent,
                IPJ.ENTIDAD_PERSJUR.FC_Buscar_Prim_Acta (tr.id_tramite_ipj) Fecha_Acta
              from ipj.t_entidades e join ipj.t_tramitesipj tr
                  on e.Id_Tramite_Ipj = tr.Id_Tramite_Ipj
              where
                e.id_legajo = p_id_legajo and
                e.borrador = 'N' and
                TR.ID_ESTADO_ULT < IPJ.Types.c_Estados_Rechazado
                and exists
                  ( select * from ipj.t_tramitesipj_acciones ac
                    where
                      ac.Id_Tramite_Ipj = tr.Id_Tramite_Ipj and
                      ac.id_legajo = e.id_legajo and
                      id_tipo_accion not in (IPJ.Types.c_Tipo_Acc_Retiro,IPJ.Types.c_Tipo_Acc_Archivar_Expediente) and
                      id_tipo_accion not in (select id_tipo_accion from ipj.t_informes_tramite where id_informe <> 18) -- Informe Caratula
                  )
              order by Fecha_Acta desc , Id_Tramite_Ipj desc
            ) t
          where
            rownum = 1;
        end if;
      exception
        when NO_DATA_FOUND then
          o_id_tramite_base := 0;
          o_id_legajo_base := 0;
          o_ubicacion_ent := 0;
      end;
    end if;

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     WHEN OTHERS THEN
       o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Buscar_Ult_Tramite;

  PROCEDURE SP_Traer_Pers_Juridica(
      p_Id_Tramite_Ipj in number,
      p_id_tramiteipj_accion in number,
      p_id_legajo in number,
      p_id_sede in varchar2 := '00',
      p_Cursor OUT TYPES.cursorType,
      o_rdo out varchar2)
  IS
    v_id_sede varchar2(3);
    v_existe_entidad number;
    v_nro_habilitacion number;
    v_expediente varchar2(30);
    v_fec_veeduria varchar2(30);
    v_n_tipo_accion varchar2(1000);
    v_Id_Tramite_Ipj number;
    v_id_tramiteipj_accion number;
    v_id_tipo_organ_fiscal number;
    v_N_tipo_organ_fiscal varchar2(200);
    v_cuil_usuario_origen varchar2(20);
    v_id_estado_origen number(5);
    v_id_pagina_origen number(6);
    v_id_legajo_origen number(15);
    v_id_integrante_origen number(10);
    v_Nombre_Accion varchar2(100);
    v_habilitacion_accion number;
    v_CursorAcciones  IPJ.TYPES.cursorType;
    v_tipo_mensaje number;
    v_codigo_reserva ipj.t_ol_entidades.codigo_reserva%TYPE;
    v_tipo_accion number;
    v_resolucion_dig ipj.t_entidades.nro_resolucion%TYPE;
    v_fec_resolucion_dig ipj.t_tramitesipj_acciones.fec_resolucion_dig%TYPE;
  BEGIN
  /**************************************************
   Este procedimiento busca una Persona Juridica, uniendo SUAC y entidades.
   Si no existe una entidad asociada al tramite, devuelve un registro vacio con solo el cuit,
   e informa el error
  ****************************************************/

    /* Detalles de campos de entidad:
      - Denominacion_1: es la Razon Social
      - Denominacion_2: es el Nombre de la Sede
      - Alta_Temporal: es la fecha de inicio de actividades
      - Baja_Temporal: es la fecha de fin de actividades
    */

    --Si la sede viene de 1 digito, le agrego un 0 adelante
    if length(p_id_sede) < 2 then
      v_id_sede := lpad(p_id_sede, 2, '0');
    else
      v_id_sede := p_id_sede;
    end if;

    -- Busco las notas de SUAC, si existen
    IPJ.TRAMITES_SUAC.SP_Bajar_Notas_SUAC(
      o_rdo => o_rdo,
      o_tipo_mensaje => v_tipo_mensaje,
      p_Id_Tramite_Ipj => p_Id_Tramite_Ipj
    );
    commit;

    -- Trae documentos faltantes desde el Portal Web
    SP_Bajar_Docs_Tramite(
      o_rdo => o_rdo,
      o_tipo_mensaje => v_tipo_mensaje,
      p_Id_Tramite_Ipj => p_id_tramite_ipj,
      p_id_legajo => p_id_legajo
    );
    commit;

    -- Busco los datos de la accion
    begin
      select A.ID_TIPO_ACCION, to_char(a.nro_resolucion_dig) || ' "' || a.id_prot_dig || '"/' || to_char(to_date(a.fec_resolucion_dig, 'dd/mm/rrrr'), 'yyyy'), fec_resolucion_dig
        into v_tipo_accion, v_resolucion_dig, v_fec_resolucion_dig
      from IPJ.T_TRAMITESIPJ_ACCIONES a
      where
        id_tramite_ipj = p_id_tramite_ipj and
        id_tramiteipj_accion = p_id_tramiteipj_accion;
    exception
      when NO_DATA_FOUND then
        v_tipo_accion := 0;
        v_resolucion_dig := '';
    end;

    -- Busco que habilitan las acciones agrupadas y sus ombres
    begin
      -- Busco los datos de la accion solicitada
      select cuil_usuario, id_estado, id_integrante, id_legajo, id_pagina
        into v_cuil_usuario_origen, v_id_estado_origen, v_id_integrante_origen, v_id_legajo_origen, v_id_pagina_origen
      from IPJ.T_TRAMITESIPJ_ACCIONES acc join IPJ.T_TIPOS_ACCIONESIPJ ta
        on acc.id_tipo_accion = ta.id_tipo_accion
      where
        acc.Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        ACC.ID_TRAMITEIPJ_ACCION = p_id_tramiteipj_accion;

      -- Busco todos los nombres y habilitaciones de las acciones
      v_nro_habilitacion :=  0;
      v_n_tipo_accion := null;
      OPEN v_CursorAcciones FOR
        select ta.N_Tipo_Accion, ta.Codigo_Habilitacion
        from IPJ.T_TRAMITESIPJ_ACCIONES acc join IPJ.T_TIPOS_ACCIONESIPJ ta
          on acc.id_tipo_accion = ta.id_tipo_accion
        where
          acc.Id_Tramite_Ipj = p_Id_Tramite_Ipj and
          acc.cuil_usuario = v_cuil_usuario_origen and
          nvl(acc.id_legajo, 0) = nvl(v_id_legajo_origen, 0) and
          nvl(acc.id_integrante, 0) = nvl(v_id_integrante_origen, 0) and
          acc.id_estado = v_id_estado_origen and
          ta.id_pagina = v_id_pagina_origen;

      LOOP
        fetch v_CursorAcciones into v_Nombre_Accion, v_habilitacion_accion;
        EXIT WHEN v_CursorAcciones%NOTFOUND;

         v_nro_habilitacion :=  bitand(v_nro_habilitacion, v_habilitacion_accion);
         v_n_tipo_accion := v_n_tipo_accion || (case when v_n_tipo_accion is null then null else  ' - ' end)|| v_Nombre_Accion;
      END LOOP;

      CLOSE v_CursorAcciones;
    exception
      when NO_DATA_FOUND then
        v_nro_habilitacion := 0;
    end;

    -- Busco si ya cargo algun tipo de organismo de fiscalizacion
    begin
      select eo.id_tipo_organismo, o.n_tipo_organismo into v_id_tipo_organ_fiscal, v_N_tipo_organ_fiscal
      from ipj.t_entidades_organismo eo join IPJ.t_tipos_organismo o
          on eo.id_tipo_organismo = o.id_tipo_organismo
        join ipj.t_tramitesipj tr
          on eo.id_tramite_ipj = tr.id_tramite_ipj
      where
        id_legajo = p_id_legajo and
        (tr.id_estado_ult < 200 or tr.id_estado_ult = 210) and -- Evita estados rechazados
        O.ES_ADMIN = 'N' and
        eo.fecha_baja is null;
    exception
      when NO_DATA_FOUND then
        v_id_tipo_organ_fiscal := 0;
        v_N_tipo_organ_fiscal := null;
    end;
    -- Si no cargo organismo, busco si cargo algun s�ndico
    if nvl(v_id_tipo_organ_fiscal, 0) = 0 then
      begin
        select distinct o.id_tipo_organismo, O.N_Tipo_Organismo into v_id_tipo_organ_fiscal, v_N_tipo_organ_fiscal
        from IPJ.T_ENTIDADES_SINDICO es join IPJ.t_tipos_integrante i
            on es.Id_Tipo_Integrante = I.Id_Tipo_Integrante
          join IPJ.t_tipos_organismo o
            on i.id_tipo_organismo = o.id_tipo_organismo
          join ipj.t_tramitesipj tr
            on es.Id_Tramite_Ipj = tr.Id_Tramite_Ipj
          jOIN ipj.t_entidades_organismo eo
            ON es.id_tramite_ipj = eo.id_tramite_ipj
        where
          es.id_legajo = p_id_legajo and
          O.ES_ADMIN = 'N' and
          o.id_ubicacion = tr.id_ubicacion_origen AND
          eo.fecha_baja is null;
      exception
        when NO_DATA_FOUND then
          v_id_tipo_organ_fiscal := 0;
          v_N_tipo_organ_fiscal := null;
      end;
    end if;

    --Valido que exista alguna entidad, o busco su ultimo tr�mite
    select count(*) into v_existe_entidad
    from ipj.t_entidades e
    where
      e.Id_Tramite_Ipj = p_Id_Tramite_Ipj and
      e.id_legajo = p_id_legajo;

    --PBI11088 - Busco el c�digo de reserva del tr�mite on-line
    BEGIN
      SELECT e.codigo_reserva
        INTO v_codigo_reserva
        FROM ipj.t_tramitesipj t
        JOIN ipj.t_ol_entidades e ON t.codigo_online = e.codigo_online
       WHERE t.id_tramite_ipj = p_Id_Tramite_Ipj;
    EXCEPTION
      WHEN no_data_found THEN
        v_codigo_reserva := 0;
    END;

    if v_existe_entidad = 0 then
      -- busco el ultimo tr�mite de la entidad
      IPJ.Entidad_PersJur.SP_Buscar_Entidad_Tramite(
        o_Id_Tramite_Ipj => v_Id_Tramite_Ipj,
        o_id_tramiteipj_accion => v_id_tramiteipj_accion,
        p_id_legajo => p_id_legajo);

     -- busco el expediente y la veeduria
      begin
        select expediente, to_char(acc.FEC_VEEDURIA, 'dd/mm/rrrr HH:MI:SS AM') into v_expediente, v_fec_veeduria
        from ipj.t_tramitesipj e join ipj.t_tramitesipj_acciones acc
          on e.Id_Tramite_Ipj = acc.Id_Tramite_Ipj
        where
          e.Id_Tramite_Ipj = p_Id_Tramite_Ipj and
          acc.id_tramiteipj_accion = p_id_tramiteipj_accion and
          acc.id_legajo = p_id_legajo;
      exception
        when NO_DATA_FOUND then
          v_expediente := '';
          v_fec_veeduria := null;
      end;

      if v_Id_Tramite_Ipj = 0 and v_id_tramiteipj_accion = 0 then

        OPEN p_Cursor FOR
          SELECT '' desc_obj_social, '0' monto, null acta_contitutiva, null vigencia, null id_unidad_med,
                 null id_moneda, null cierre_ejercicio, null fec_vig, L.CUIT, L.DENOMINACION_SIA denominacion_1,
                 null alta_temporal, null baja_temporal, 'N' Existe, l.nro_ficha Matricula, 'S' Borrador,
                 nvl(v_nro_habilitacion, 2097151) NRO_HABILITACION, --2097151  es completo
                 null id_tipo_organo_fiscal, l.id_tipo_entidad, te.tipo_entidad, v_fec_veeduria fec_veeduria
            FROM IPJ.t_legajos l
            JOIN ipj.t_tipos_entidades te ON l.id_tipo_entidad = te.id_tipo_entidad
           WHERE id_legajo = p_id_legajo;

      else

        OPEN p_Cursor FOR
          SELECT e.objeto_social desc_obj_social, trim(To_Char(nvl(e.monto, '0'), '99999999999999999990.99')) monto , e.acta_contitutiva,
                 e.vigencia, e.id_unidad_med, e.id_moneda, To_Char(e.cierre_ejercicio, 'DD/MM') cierre_ejercicio,
                 to_char(e.fec_vig, 'dd/mm/rrrr') fec_vig, e.cuit, e.denominacion_1, to_char(e.alta_temporal, 'dd/mm/rrrr') alta_temporal,
                 to_char(e.baja_temporal, 'dd/mm/rrrr') baja_temporal, nvl(e.borrador, 'S') Borrador,
                 case nvl(e.denominacion_1, '') when '' then 'N' else 'S' end Existe,
                 e.Id_Tramite_Ipj,  e.ID_SEDE,  --(case nvl(e.MATRICULA, '') when '' then null else e.matricula end) MATRICULA,
                 to_char(e.FEC_INSCRIPCION, 'dd/mm/rrrr') FEC_INSCRIPCION, e.FOLIO, e.LIBRO_NRO,  e.TOMO,  e.anio,
                 e.ID_VIN_COMERCIAL, e.ID_VIN_REAL, e.FEC_ACTO,  e.DENOMINACION_2, e.tipo_vigencia,
                 trim(To_Char(nvl(e.valor_cuota, '0'), '99999999999999999990.9999')) valor_cuota,
                 e.cuotas, e.MATRICULA_VERSION,  e.id_tipo_administracion, initcap(M.N_MONEDA) N_MONEDA,
                 TA.N_TIPO_ADMINISTRACION, E.ID_TIPO_ENTIDAD, e.Id_Tipo_Origen, tor.n_tipo_origen,
                 E.ID_ESTADO_ENTIDAD, TE.ESTADO_ENTIDAD, E.OBS_CIERRE,
                 IPJ.ENTIDAD_PERSJUR.FC_ESTADO_GRAVAMEN_PERSJUR(e.id_legajo) Estado_Gravamen,
                 IPJ.ENTIDAD_PERSJUR.FC_ESTADO_GRAVAMEN_INTERV(e.id_legajo) Estado_Interv,
                 e.NRO_RESOLUCION, to_char(e.FEC_RESOLUCION, 'dd/mm/rrrr') FEC_RESOLUCION,
                 e.NRO_BOLETIN, to_char(e.FEC_BOLETIN, 'dd/mm/rrrr') FEC_BOLETIN,
                 e.NRO_REGISTRO, e.SEDE_ESTATUTO, e.REQUIERE_SINDICO, e.ORIGEN, e.ES_SUCURSAL, e.OBSERVACION,
                 IPJ.VARIOS.FC_Numero_Matricula(e.MATRICULA) MATRICULA, IPJ.VARIOS.FC_Letra_Matricula(e.MATRICULA) Letra,
                 nvl(v_nro_habilitacion, 2097151) NRO_HABILITACION, --2097151  es completo
                 v_expediente expediente, v_fec_veeduria fec_veeduria, v_n_tipo_accion n_tipo_accion,
                 v_id_tipo_organ_fiscal id_tipo_organo_fiscal, v_N_tipo_organ_fiscal N_tipo_organo_fiscal,
                 e.cierre_fin_mes, e.Fiscalizacion_Ejerc, e.incluida_lgs, e.Condicion_Fideic,
                 e.Obs_Fiduciario, e.Obs_Fiduciante, e.Obs_Beneficiario, e.Obs_Fideicomisario,
                 IPJ.TRAMITES_SUAC.FC_BUSCAR_AUTORIZ_SUAC(tr.id_tramite) Autoridades_SUAC,
                 to_char(e.fecha_versionado, 'dd/mm/rrrr') fecha_versionado, nvl(e.sin_denominacion, 'N') sin_denominacion,
                 e.contrato_privado, tr.id_tramite_ipj_padre,
                 e.cant_Fiduciario, e.cant_Fiduciante, e.cant_Beneficiario, e.cant_Fideicomisario,
                 tr.id_ubicacion_origen, to_char(fec_vig_hasta, 'dd/mm/rrrr') fec_vig_hasta,
                 tr.codigo_online, ten.tipo_entidad, e.acredita_grupo, v_codigo_reserva codigo_reserva,
                 (select o.id_sub_tramite_ol from ipj.t_ol_entidades o where o.codigo_online = tr.codigo_online) id_sub_tramite_ol, -- (PBI 11389)
                 to_char(tr.asigna_cuit_afip, 'dd/mm/rrrr') asigna_cuit_afip, -- (PBI 11389)
                 to_char(e.fec_estatuto, 'dd/mm/rrrr') fec_estatuto, --(PBI 12185)
                 IPJ.TRAMITES.FC_Es_Tram_Digital(tr.id_tramite_ipj) Es_Digital,
                 decode(v_tipo_accion, 135, E.NRO_RESOLUCION, v_resolucion_dig) protocolo_dig, -- Resolucion Const. AC Dig, o Asambelas Dig
                 (select expediente from ipj.t_tramitesipj tt where tt.id_tramite_ipj = (select id_tramite_ipj_ref from ipj.t_ol_entidades o where o.codigo_online = tr.codigo_online)) exp_referente,
                 e.cant_fiduciario_otros, e.cant_fiduciante_otros, e.cant_beneficiario_otros, e.cant_fideicomisario_otros,
                 IPJ.VARIOS.FC_Buscar_Te_Nro(nvl(e.cuit, 'TrOL-' || to_char(nvl(tr.codigo_online, 0))) || '00', IPJ.Types.c_id_Aplicacion) te_nro_sociedad,
                 IPJ.VARIOS.FC_Buscar_Te_Caract(nvl(e.cuit, 'TrOL-' || to_char(nvl(tr.codigo_online, 0))) || '00', IPJ.Types.c_id_Aplicacion) te_caract_sociedad,
                 IPJ.ENTIDAD_PERSJUR.FC_Cuit_AFip(e.id_legajo) Es_Cuit_AFIP,
                 to_char(v_fec_resolucion_dig, 'dd/mm/rrrr') fec_resolucion_dig,
                 ( select count(1)
                   from ipj.t_ol_entidades eo
                   where
                     eo.codigo_online = tr.codigo_online and
                     eo.id_tipo_tramite_ol in(1, 20, 17, 23, 52) -- Const. SA, SAS, AC y F, SRL
                 ) Const_OL,
                 nvl(e.gubernamental, 'N') gubernamental
            FROM ipj.t_entidades e JOIN ipj.t_tramitesipj tr
              on e.id_tramite_ipj = tr.id_tramite_ipj
              left join T_COMUNES.T_MONEDAS m
              on E.ID_MONEDA = M.ID_MONEDA
              left join IPJ.T_TIPOS_ADMINISTRACION ta
              on  E.ID_TIPO_ADMINISTRACION = TA.ID_TIPO_ADMINISTRACION
              left join IPJ.T_ESTADOS_ENTIDADES te
              on E.ID_ESTADO_ENTIDAD = TE.ID_ESTADO_ENTIDAD
              left join IPJ.t_tipos_origen tor
              on e.id_tipo_origen = tor.id_tipo_origen
              LEFT JOIN ipj.t_tipos_entidades ten
              ON e.id_tipo_entidad = ten.id_tipo_entidad
             WHERE e.Id_Tramite_Ipj = v_Id_Tramite_Ipj
               AND e.id_legajo = p_id_legajo;
      end if;

    else
      -- busco el expediente y la veeduria
      begin
        select expediente, to_char(acc.FEC_VEEDURIA, 'dd/mm/rrrr HH:MI:SS AM') into v_expediente, v_fec_veeduria
        from ipj.t_tramitesipj e join ipj.t_tramitesipj_acciones acc
          on e.Id_Tramite_Ipj = acc.Id_Tramite_Ipj
        where
          e.Id_Tramite_Ipj = p_Id_Tramite_Ipj and
          acc.id_tramiteipj_accion = p_id_tramiteipj_accion and
          acc.id_legajo = p_id_legajo;
      exception
        when NO_DATA_FOUND then
          v_expediente := '';
      end;

      OPEN p_Cursor FOR
        select e.objeto_social desc_obj_social, trim(To_Char(nvl(e.monto, '0'), '99999999999999999990.99'))  monto , e.acta_contitutiva,
               e.vigencia, e.id_unidad_med, e.id_moneda, To_Char(e.cierre_ejercicio, 'DD/MM') cierre_ejercicio,
               to_char(e.fec_vig, 'dd/mm/rrrr') fec_vig, e.cuit, e.denominacion_1, to_char(e.alta_temporal, 'dd/mm/rrrr') alta_temporal,
               to_char(e.baja_temporal, 'dd/mm/rrrr') baja_temporal, nvl(e.borrador, 'S') borrador,
               case nvl(e.denominacion_1, '') when '' then 'N' else 'S' end Existe,
               e.Id_Tramite_Ipj,  e.ID_SEDE,  --(case nvl(e.MATRICULA, '') when '' then null else e.matricula end) MATRICULA,
               to_char(e.FEC_INSCRIPCION, 'dd/mm/rrrr') FEC_INSCRIPCION, e.FOLIO, e.LIBRO_NRO,  e.TOMO,  e.anio,
               e.ID_VIN_COMERCIAL, e.ID_VIN_REAL, e.FEC_ACTO,  e.DENOMINACION_2, e.tipo_vigencia,
               trim(To_Char(nvl(e.valor_cuota, '0'), '99999999999999999990.9999')) valor_cuota,
               e.cuotas, e.MATRICULA_VERSION,  e.id_tipo_administracion, initcap(M.N_MONEDA) N_MONEDA,
               TA.N_TIPO_ADMINISTRACION, E.ID_TIPO_ENTIDAD, e.Id_Tipo_Origen, tor.n_tipo_origen,
               E.ID_ESTADO_ENTIDAD, TE.ESTADO_ENTIDAD, E.OBS_CIERRE,
               IPJ.ENTIDAD_PERSJUR.FC_ESTADO_GRAVAMEN_PERSJUR(e.id_legajo) Estado_Gravamen,
               IPJ.ENTIDAD_PERSJUR.FC_ESTADO_GRAVAMEN_INTERV(e.id_legajo) Estado_Interv,
               e.NRO_RESOLUCION, to_char(e.FEC_RESOLUCION, 'dd/mm/rrrr') FEC_RESOLUCION,
               e.NRO_BOLETIN, to_char(e.FEC_BOLETIN, 'dd/mm/rrrr') FEC_BOLETIN,
               e.NRO_REGISTRO, e.SEDE_ESTATUTO, e.REQUIERE_SINDICO, e.ORIGEN, e.ES_SUCURSAL, e.OBSERVACION,
               IPJ.VARIOS.FC_Numero_Matricula(e.MATRICULA) MATRICULA, IPJ.VARIOS.FC_Letra_Matricula(e.MATRICULA) Letra,
               nvl(v_nro_habilitacion, 2097151) NRO_HABILITACION, --2097151  es completo
               v_expediente expediente, v_fec_veeduria fec_veeduria, v_n_tipo_accion n_tipo_accion,
               v_id_tipo_organ_fiscal id_tipo_organo_fiscal, v_N_tipo_organ_fiscal N_tipo_organo_fiscal,
               e.cierre_fin_mes, e.Fiscalizacion_Ejerc, e.incluida_lgs, e.Condicion_Fideic,
               e.Obs_Fiduciario, e.Obs_Fiduciante, e.Obs_Beneficiario, e.Obs_Fideicomisario,
               IPJ.TRAMITES_SUAC.FC_BUSCAR_AUTORIZ_SUAC(tr.id_tramite) Autoridades_SUAC,
               to_char(e.fecha_versionado, 'dd/mm/rrrr') fecha_versionado, nvl(e.sin_denominacion, 'N') sin_denominacion,
               e.contrato_privado, tr.id_tramite_ipj_padre,
               e.cant_Fiduciario, e.cant_Fiduciante, e.cant_Beneficiario, e.cant_Fideicomisario,
               tr.id_ubicacion_origen, to_char(fec_vig_hasta, 'dd/mm/rrrr') fec_vig_hasta,
               tr.codigo_online, ten.tipo_entidad, e.acredita_grupo, v_codigo_reserva codigo_reserva,
               (select o.id_sub_tramite_ol from ipj.t_ol_entidades o where o.codigo_online = tr.codigo_online) id_sub_tramite_ol, -- (PBI 11389)
               to_char(tr.asigna_cuit_afip, 'dd/mm/rrrr') asigna_cuit_afip,-- (PBI 11389)
               to_char(e.fec_estatuto, 'dd/mm/rrrr') fec_estatuto, --(PBI 12185)
               IPJ.TRAMITES.FC_Es_Tram_Digital(tr.id_tramite_ipj) Es_Digital,
               decode(v_tipo_accion, 135, E.NRO_RESOLUCION, v_resolucion_dig) protocolo_dig, -- Resolucion Const. AC Dig, o Asambelas Dig
               (select expediente from ipj.t_tramitesipj tt where tt.id_tramite_ipj = (select id_tramite_ipj_ref from ipj.t_ol_entidades o where o.codigo_online = tr.codigo_online)) exp_referente,
               e.cant_fiduciario_otros, e.cant_fiduciante_otros, e.cant_beneficiario_otros, e.cant_fideicomisario_otros,
               IPJ.VARIOS.FC_Buscar_Te_Nro(nvl(e.cuit, 'TrOL-' || to_char(nvl(tr.codigo_online, 0))) || '00', IPJ.Types.c_id_Aplicacion) te_nro_sociedad,
               IPJ.VARIOS.FC_Buscar_Te_Caract(nvl(e.cuit, 'TrOL-' || to_char(nvl(tr.codigo_online, 0))) || '00', IPJ.Types.c_id_Aplicacion) te_caract_sociedad,
               IPJ.ENTIDAD_PERSJUR.FC_Cuit_AFip(e.id_legajo) Es_Cuit_AFIP,
               to_char(v_fec_resolucion_dig, 'dd/mm/rrrr') fec_resolucion_dig,
               ( select count(1)
                   from ipj.t_ol_entidades eo
                   where
                     eo.codigo_online = tr.codigo_online and
                     eo.id_tipo_tramite_ol in(1, 20, 17, 23, 52) -- Const. SA, SAS, AC y F, SRL
                 ) Const_OL,
                 nvl(e.gubernamental, 'N') gubernamental
          FROM ipj.t_entidades e join ipj.t_tramitesipj tr
            on e.id_tramite_ipj = tr.id_tramite_ipj
            left join T_COMUNES.T_MONEDAS m
            on E.ID_MONEDA = M.ID_MONEDA
            left join IPJ.T_TIPOS_ADMINISTRACION ta
            on  E.ID_TIPO_ADMINISTRACION = TA.ID_TIPO_ADMINISTRACION
            left join IPJ.T_ESTADOS_ENTIDADES te
            on E.ID_ESTADO_ENTIDAD = TE.ID_ESTADO_ENTIDAD
            left join IPJ.t_tipos_origen tor
            on e.id_tipo_origen = tor.id_tipo_origen
            LEFT JOIN ipj.t_tipos_entidades ten
            ON e.id_tipo_entidad = ten.id_tipo_entidad
         WHERE e.Id_Tramite_Ipj = p_Id_Tramite_Ipj
           AND e.id_legajo = p_id_legajo;
    end if;

    if o_rdo is null then
      o_rdo := 'OK';
    end if;
  END SP_Traer_Pers_Juridica;

  PROCEDURE SP_Traer_PersJur_Actas(
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number,
    p_Cursor OUT TYPES.cursorType)
  IS
    v_matricula_version number(6,0);
    v_ubicacion_ent number(6);
    v_ubicacion_leg number(6);
  BEGIN
  /*******************************************************
    Este procedimento devuelve las actas asociadas a una entidad de un tramite
  ********************************************************/
    begin
      select nvl(matricula_version, 0),
        (select id_ubicacion from ipj.t_tipos_entidades te where te.id_tipo_entidad = e.id_tipo_entidad) ubicacion_ent,
        (select id_ubicacion from ipj.t_tipos_entidades te where te.id_tipo_entidad = l.id_tipo_entidad) ubicacion_leg
        into v_matricula_version, v_ubicacion_ent, v_ubicacion_leg
      from IPJ.t_entidades e join ipj.t_legajos l
        on e.id_legajo = l.id_legajo
      where
        e.Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        e.id_legajo = p_id_legajo;
    exception
      when no_data_found then
        v_matricula_version := 0;
    end;

    -- Traigo las actas de a misma empresa, que no esten en borrador o que sean
    -- del mismo tramite
    OPEN p_Cursor FOR
      select ta.n_tipo_acta, ec.id_legajo, ec.id_tipo_acta, to_char(ec.fec_acta, 'dd/mm/rrrr') fec_acta,
        EC.OBSERVACION, ec.borrador, EC.ID_ENTIDAD_ACTA,
        EC.Id_Tramite_Ipj, EC.ART_REFORMADOS, EC.ID_VIN,
        nvl(tr.sticker,  IPJ.TRAMITES_SUAC.FC_BUSCAR_STIKER_PUBLICO(tr.id_tramite)) Sticker,
        tr.id_tramite Id_Tramite_Suac,
        ec.es_unanime, ec.es_autoconvocada, ec.es_en_sede,
        to_char(ec.fecha_convocatoria, 'dd/mm/rrrr') fecha_convocatoria,
        to_char(ec.fecha_acta_directorio, 'dd/mm/rrrr') fecha_acta_directorio,
        to_char(ec.fecha_libro_asamblea, 'dd/mm/rrrr') fecha_libro_asamblea,
        ec.publicado_boe, to_char(ec.fecha_primera_boe, 'dd/mm/rrrr') fecha_primera_boe,
        to_char(ec.fecha_ultima_boe, 'dd/mm/rrrr') fecha_ultima_boe,
        ec.publicado_diario, to_char(ec.fecha_primera_diario, 'dd/mm/rrrr') fecha_primera_diario,
        to_char(ec.fecha_ultima_diario, 'dd/mm/rrrr') fecha_ultima_diario,
        to_char(ec.porc_capital_social,  '999990.99') porc_capital_social,
        to_char(ec.quorum_convocatoria1, '999990.99') quorum_convocatoria1,
        to_char(ec.quorum_convocatoria2, '999990.99') quorum_convocatoria2,
        to_char(ec.fecha_cuarto_inter, 'dd/mm/rrrr') fecha_cuarto_inter,
        IPJ.VARIOS.FC_ARMAR_CALLE_DOM(d.n_calle, d.altura, d.piso, d.depto, d.torre, d.mzna, d.lote, d.n_barrio, d.km, d.n_tipocalle) calle_inf,
        d.id_tipodom, d.n_tipodom, d.id_tipocalle, d.n_tipocalle, d.id_calle,
        initcap(d.n_calle) n_calle, d.altura, d.depto, d.piso, d.torre, d.id_barrio,
        initcap(d.n_barrio) n_barrio,
        d.id_localidad, initcap(d.n_localidad) n_localidad, d.id_departamento,
        D.mzna Manzana, D.Lote, d.km,
        initcap(d.n_departamento) n_departamento, d.id_provincia,
        initcap(d.n_provincia) n_provincia, d.cpa,
        (case when nvl(ec.id_vin, 0) = 0 then 1 else 0 end) en_sede,
        (case when ec.Id_Tramite_Ipj = p_Id_Tramite_Ipj then 1 else 0 end) imprimir,
        nvl(tr.expediente, IPJ.TRAMITES_SUAC.FC_BUSCAR_EXPEDIENTE(tr.id_tramite)) Nro_Expediente,
        p_Id_Tramite_Ipj Id_Tramite_Ipj_actual,
        (select N_estado from Ipj.T_Estados es where es.id_estado = Tr.Id_Estado_Ult) N_Estado,
        Tr.Id_Estado_Ult id_estado, tr.id_tramite_ipj_padre,
        decode(to_char(ec.fec_acta,'HH24:Mi'),'00:00', NULL, to_char(ec.fec_acta,'HH24:Mi')) hora_acta
      from ipj.t_entidades_acta ec join ipj.t_tipos_acta ta
          on ta.id_tipo_acta = ec.id_tipo_acta
        join ipj.t_Entidades tpj
          on ec.Id_Tramite_Ipj = tpj.Id_Tramite_Ipj and ec.id_legajo = tpj.id_legajo
        join ipj.t_tramitesipj tr
          on TR.Id_Tramite_Ipj = TPJ.Id_Tramite_Ipj
        left join dom_manager.VT_DOMICILIOS_COND d
          on d.id_vin = ec.id_vin
      where
        tpj.id_legajo = p_id_legajo and
        tpj.matricula_version <= v_matricula_version and
        tpj.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_ubicacion_leg)
      order by FEC_ACTA desc;

  END SP_Traer_PersJur_Actas;


  PROCEDURE SP_Traer_PersJur_InstLegal(
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number,
    p_Cursor OUT TYPES.cursorType)
  IS
    v_matricula_version number(6,0);
    v_ubicacion_ent number(6);
    v_ubicacion_leg number(6);
  BEGIN
  /*******************************************************
    Este procedimento devuelve los instrumentos legales que conforman la Entidad
  ********************************************************/
    begin
      select nvl(matricula_version, 0),
        (select id_ubicacion from ipj.t_tipos_entidades te where te.id_tipo_entidad = e.id_tipo_entidad) ubicacion_ent,
        (select id_ubicacion from ipj.t_tipos_entidades te where te.id_tipo_entidad = l.id_tipo_entidad) ubicacion_leg
        into v_matricula_version, v_ubicacion_ent, v_ubicacion_leg
      from IPJ.t_entidades e join ipj.t_legajos l
        on e.id_legajo = l.id_legajo
      where
        e.Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        e.id_legajo = p_id_legajo;
    exception
      when NO_DATA_FOUND then
        v_matricula_version := 0;
    end;

    OPEN p_Cursor FOR
      select EIL.ID_JUZGADO, EIL.Id_Tramite_Ipj, EIL.id_legajo,
        to_char(EIL.IL_FECHA , 'dd/mm/rrrr') il_fecha, EIL.IL_NUMERO, EIL.IL_TIPO,
        J.N_JUZGADO, eil.borrador, J.N_JUZGADO, L.N_IL_TIPO, eil.titulo, eil.descripcion,
        to_char(eil.fecha, 'dd/mm/rrrr') fecha, eil.Id_Entidad_Ins_Legal,
        (case when EIL.Id_Tramite_Ipj = p_Id_Tramite_Ipj then 1 else 0 end) imprimir
      from ipj.t_entidades_ins_legal eil left join ipj.t_juzgados j
          on EIL.ID_JUZGADO = J.ID_JUZGADO
        join IPJ.T_Entidades e
          on eil.Id_Tramite_Ipj = e.Id_Tramite_Ipj and eil.id_legajo = e.id_legajo
        join IPJ.T_TIPOS_INS_LEGAL L
          on EIL.IL_TIPO = L.IL_TIPO
      where
        e.id_legajo = p_id_legajo and
        e.matricula_version <= v_matricula_version and
        e.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_ubicacion_leg) and
        (eil.borrador = 'N' or EIL.Id_Tramite_Ipj = p_Id_Tramite_Ipj)
      order by
        EIL.IL_FECHA desc;
  END SP_Traer_PersJur_InstLegal;

  PROCEDURE SP_Traer_PersJur_Int_Admin(
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number,
    --o_rdo out number,
    p_Cursor OUT TYPES.cursorType)
  IS
  /***************************************************************
  Este procemiento devuelve el listado de integrantes asociados a una entidad de un tramite,
  cuyo tipo sea distinto de SOCIO
  ***************************************************************/
    v_matricula_version number(6,0);
    v_ubicacion_ent number(6);
    v_ubicacion_leg number(6);
    v_now date;
    v_dias_vigencia number;
  BEGIN

    if IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('HAB_LOG_GESTION') = '1' then
      IPJ.VARIOS.SP_GUARDAR_LOG(
        p_METODO => 'SP_Traer_PersJur_Int_Admin',
        p_NIVEL => 'Gestion',
        p_ORIGEN => 'DB',
        p_USR_LOG  => 'DB',
        p_USR_WINDOWS => 'Sistema',
        p_IP_PC  => null,
        p_EXCEPCION =>
          'Id Tramite IPJ = ' || to_char(p_Id_Tramite_Ipj)
          || ' / Id Legajo = ' || to_char(p_id_legajo)
      );
    end if;

    -- (PBI 9917) Busco los d�as de vigencia configurados
    v_dias_vigencia := to_number(IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('DIAS_ADMIN_LEY'));

    begin
      select nvl(matricula_version, 0),
        (select id_ubicacion from ipj.t_tipos_entidades te where te.id_tipo_entidad = e.id_tipo_entidad) ubicacion_ent,
        (select id_ubicacion from ipj.t_tipos_entidades te where te.id_tipo_entidad = l.id_tipo_entidad) ubicacion_leg
        into v_matricula_version, v_ubicacion_ent, v_ubicacion_leg
      from IPJ.t_entidades e join ipj.t_legajos l
        on e.id_legajo = l.id_legajo
      where
        e.Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        e.id_legajo = p_id_legajo;
    exception
      when NO_DATA_FOUND then
        v_matricula_version := 0;

        select
          (select id_ubicacion from ipj.t_tipos_entidades te where te.id_tipo_entidad = l.id_tipo_entidad) ubicacion_leg
           into v_ubicacion_leg
        from ipj.t_legajos l
        where
          l.id_legajo = p_id_legajo;
    end;

    OPEN p_Cursor FOR
      select ie.id_tipo_integrante, ie.id_legajo, to_char(ie.fecha_inicio, 'dd/mm/rrrr') fecha_inicio,
        to_char(ie.fecha_fin, 'dd/mm/rrrr') fecha_fin,
        ie.id_integrante, ie.Id_Tramite_Ipj, ti.n_tipo_integrante,
        nvl(n_empresa, p.NOMBRE || ' ' ||p.APELLIDO) detalle,
        I.ID_NUMERO, I.ID_SEXO, I.NRO_DOCUMENTO, I.PAI_COD_PAIS, ie.borrador,
        IE.ID_VIN, IE.ID_TIPO_ORGANISMO, org.n_tipo_organismo, ie.id_motivo_baja,
        trim(To_Char(Ie.Porc_Garantia, '9999999999999990.99')) Porc_Garantia,
        trim(To_Char(Ie.Monto_Garantia, '99999999999999999990.99')) Monto_Garantia, Ie.Habilitado_Present,
        I.ERROR_DATO,
        IPJ.VARIOS.FC_ARMAR_CALLE_DOM(d.n_calle, d.altura, d.piso, d.depto, d.torre, d.mzna, d.lote, d.n_barrio, d.km, d.n_tipocalle) calle_inf,
        d.id_tipodom, d.n_tipodom, d.id_tipocalle, d.n_tipocalle, d.id_calle,
        initcap(d.n_calle) n_calle, d.altura, d.depto, d.piso, d.torre, d.id_barrio,
        initcap(d.n_barrio) n_barrio,
        D.mzna Manzana, D.Lote, d.km,
        d.id_localidad, initcap(d.n_localidad) n_localidad, d.id_departamento,
        initcap(d.n_departamento) n_departamento, d.id_provincia,
        initcap(d.n_provincia) n_provincia, d.cpa, p_Id_Tramite_Ipj IdTramiteIpj_Entidad,
        Ie.Id_Entidades_Accion, Acc.Clase, mb.n_motivo_baja, ie.persona_expuesta,
        ie.id_moneda, ie.id_admin,
        ie.cuit_empresa, ie.n_empresa, ie.matricula,
        to_char(ie.fec_acta, 'dd/mm/rrrr') fec_acta, ie.id_legajo_empresa, ie.folio,
        ie.anio, Ie.Id_Tipo_Entidad, te.tipo_entidad,
        (select expediente from ipj.t_tramitesipj tr where tr.id_tramite_ipj = ie.id_tramite_ipj) Expediente,
        org.nro_orden org_nro_orden, ti.nro_orden ti_nro_orden,
        to_char(ie.fecha_fin + v_dias_vigencia, 'dd/mm/rrrr') fin_vigencia, -- (PBI 9917)
        ti.es_suplente, --(PBI 11389)
        IPJ.VARIOS.FC_Buscar_Mail(I.Id_Sexo || I.Nro_Documento || I.Pai_Cod_Pais || to_char(I.Id_Numero), IPJ.Types.c_id_Aplicacion) EMail, --(PBI 11389)
        nvl(p.cuil, i.cuil) Cuil, --(PBI 11389)
        ie.es_admin_afip,
        IPJ.ENTIDAD_PERSJUR.FC_Fecha_Orig_Admin(ie.id_legajo, ie.id_admin) Fecha_Fin_Orig,
        ie.id_integrante_repres,NVL(i2.Cuil,p2.cuil) Cuil_Repres
      from ipj.t_entidades_admin ie left join ipj.t_integrantes i
          on ie.id_integrante = i.id_integrante
        join ipj.t_tipos_integrante ti
          on ie.id_tipo_integrante = ti.id_tipo_integrante
        left join IPJ.T_TIPOS_ORGANISMO org
          on org.id_tipo_organismo = ie.id_tipo_organismo
        join ipj.t_Entidades tpj
          on ie.Id_Tramite_Ipj = tpj.Id_Tramite_Ipj and ie.id_legajo = tpj.id_legajo
        left join ipj.t_entidades_acciones acc
          on ie.id_legajo = acc.id_legajo and Ie.Id_Entidades_Accion = Acc.Id_Entidades_Accion
        left join ipj.t_tipos_motivos_baja mb
          on ie.id_motivo_baja = mb.id_motivo_baja
        left join rcivil.vt_pk_persona p
          on i.id_sexo = p.ID_SEXO and
            i.nro_documento = p.NRO_DOCUMENTO and
            i.pai_cod_pais = p.PAI_COD_PAIS and
            i.id_numero = p.ID_NUMERO
        left join dom_manager.VT_DOMICILIOS_COND d
          on d.id_vin = ie.id_vin
        left join ipj.t_tipos_entidades te
          on ie.id_tipo_entidad = te.id_tipo_entidad
        --se agrega representante
        left join ipj.t_integrantes i2
            on ie.id_integrante_repres = i2.id_integrante
        left join RCIVIL.VT_PERSONAS_CUIL p2
          on p2.id_sexo = i2.id_sexo and p2.nro_documento = i2.nro_documento and p2.pai_cod_pais = i2.pai_cod_pais and p2.id_numero = i2.id_numero
    where
      tpj.id_legajo = p_id_legajo and
      tpj.matricula_version <= v_matricula_version and
      tpj.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_ubicacion_leg) and
      (ie.borrador = 'N' or ie.Id_Tramite_Ipj = p_Id_Tramite_Ipj) and
      (ie.fecha_fin is null or (ie.fecha_fin + v_dias_vigencia) >= to_date(sysdate, 'dd/mm/rrrr') or  ie.Id_Tramite_Ipj = p_Id_Tramite_Ipj)
    UNION ALL
    select ie.id_tipo_integrante, ie.id_legajo, to_char(ie.fecha_inicio, 'dd/mm/rrrr') fecha_inicio,
        to_char(ie.fecha_fin, 'dd/mm/rrrr') fecha_fin,
        ie.id_integrante, ie.Id_Tramite_Ipj, ti.n_tipo_integrante,
        nvl(n_empresa, p.NOMBRE || ' ' ||p.APELLIDO) detalle,
        I.ID_NUMERO, I.ID_SEXO, I.NRO_DOCUMENTO, I.PAI_COD_PAIS, ie.borrador,
        IE.ID_VIN, IE.ID_TIPO_ORGANISMO, org.n_tipo_organismo, ie.id_motivo_baja,
        trim(To_Char(Ie.Porc_Garantia, '9999999999999990.99')) Porc_Garantia,
        trim(To_Char(Ie.Monto_Garantia, '99999999999999999990.99')) Monto_Garantia, Ie.Habilitado_Present,
        I.ERROR_DATO,
        IPJ.VARIOS.FC_ARMAR_CALLE_DOM(d.n_calle, d.altura, d.piso, d.depto, d.torre, d.mzna, d.lote, d.n_barrio, d.km, d.n_tipocalle) calle_inf,
        d.id_tipodom, d.n_tipodom, d.id_tipocalle, d.n_tipocalle, d.id_calle,
        initcap(d.n_calle) n_calle, d.altura, d.depto, d.piso, d.torre, d.id_barrio,
        initcap(d.n_barrio) n_barrio,
        D.mzna Manzana, D.Lote, d.km,
        d.id_localidad, initcap(d.n_localidad) n_localidad, d.id_departamento,
        initcap(d.n_departamento) n_departamento, d.id_provincia,
        initcap(d.n_provincia) n_provincia, d.cpa, p_Id_Tramite_Ipj IdTramiteIpj_Entidad,
        Ie.Id_Entidades_Accion, Acc.Clase, mb.n_motivo_baja, ie.persona_expuesta,
        ie.id_moneda, ie.id_admin,
        ie.cuit_empresa, ie.n_empresa, ie.matricula,
        to_char(ie.fec_acta, 'dd/mm/rrrr') fec_acta, ie.id_legajo_empresa, ie.folio,
        ie.anio, Ie.Id_Tipo_Entidad, te.tipo_entidad,
        (select expediente from ipj.t_tramitesipj tr where tr.id_tramite_ipj = ie.id_tramite_ipj) Expediente,
        org.nro_orden org_nro_orden, ti.nro_orden ti_nro_orden,
        to_char(ie.fecha_fin + v_dias_vigencia, 'dd/mm/rrrr') fin_vigencia, -- (PBI 9917)
        ti.es_suplente, --(PBI 11389)
        IPJ.VARIOS.FC_Buscar_Mail(I.Id_Sexo || I.Nro_Documento || I.Pai_Cod_Pais || to_char(I.Id_Numero), IPJ.Types.c_id_Aplicacion) EMail, --(PBI 11389)
        nvl(p.cuil, i.cuil) Cuil, --(PBI 11389)
        ie.es_admin_afip,
        ipj.entidad_persjur.FC_Fecha_Orig_Admin(ie.id_legajo, ie.id_admin) Fecha_Fin_Orig,
        ie.id_integrante_repres,NVL(i2.Cuil,p2.cuil) Cuil_Repres
      from ipj.t_entidades_admin ie left join ipj.t_integrantes i
          on ie.id_integrante = i.id_integrante
        join ipj.t_tipos_integrante ti
          on ie.id_tipo_integrante = ti.id_tipo_integrante
        left join IPJ.T_TIPOS_ORGANISMO org
          on org.id_tipo_organismo = ie.id_tipo_organismo
        join ipj.t_Entidades tpj
          on ie.Id_Tramite_Ipj = tpj.Id_Tramite_Ipj and ie.id_legajo = tpj.id_legajo
        left join ipj.t_entidades_acciones acc
          on ie.id_legajo = acc.id_legajo and Ie.Id_Entidades_Accion = Acc.Id_Entidades_Accion
        left join ipj.t_tipos_motivos_baja mb
          on ie.id_motivo_baja = mb.id_motivo_baja
        left join rcivil.vt_pk_persona p
          on i.id_sexo = p.ID_SEXO and
            i.nro_documento = p.NRO_DOCUMENTO and
            i.pai_cod_pais = p.PAI_COD_PAIS and
            i.id_numero = p.ID_NUMERO
        left join dom_manager.VT_DOMICILIOS_COND d
          on d.id_vin = ie.id_vin
        left join ipj.t_tipos_entidades te
          on ie.id_tipo_entidad = te.id_tipo_entidad
        JOIN ipj.t_tramitesipj tra
          ON ie.id_tramite_ipj = tra.id_tramite_ipj
        --se agrega representante
        left join ipj.t_integrantes i2
            on ie.id_integrante_repres = i2.id_integrante
        left join RCIVIL.VT_PERSONAS_CUIL p2
          on p2.id_sexo = i2.id_sexo and p2.nro_documento = i2.nro_documento and p2.pai_cod_pais = i2.pai_cod_pais and p2.id_numero = i2.id_numero
    where
      tpj.id_legajo = (SELECT max(e.id_legajo) FROM ipj.t_entidades e WHERE e.id_tramite_ipj = tra.id_tramite_ipj) and
      tpj.matricula_version <= v_matricula_version and
      tpj.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_ubicacion_leg) and
      tra.id_tramite_ipj_padre = p_Id_Tramite_Ipj and
      (ie.fecha_fin is null or (ie.fecha_fin + v_dias_vigencia) >= to_date(sysdate, 'dd/mm/rrrr') or  tra.id_tramite_ipj_padre = p_Id_Tramite_Ipj)
    order by id_tramite_ipj, org_nro_orden asc, ti_nro_orden asc, detalle ASC;

  END SP_Traer_PersJur_Int_Admin;

  PROCEDURE SP_Traer_PersJur_Socios_Inf(
    o_Cursor OUT TYPES.cursorType,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number)
  IS
    v_matricula_version number(6, 0);
    v_ubicacion_ent number(6);
    v_ubicacion_leg number(6);
  BEGIN
    /*********************************************************
      Este procemiento devuelve el listado de integrantes asociados a una entidad de un tramite,
     cuyo tipo sea igual a SOCIO
    **********************************************************/
    begin
      select nvl(matricula_version, 0),
        (select id_ubicacion from ipj.t_tipos_entidades te where te.id_tipo_entidad = e.id_tipo_entidad) ubicacion_ent,
        (select id_ubicacion from ipj.t_tipos_entidades te where te.id_tipo_entidad = l.id_tipo_entidad) ubicacion_leg
        into v_matricula_version, v_ubicacion_ent, v_ubicacion_leg
      from IPJ.t_entidades e join ipj.t_legajos l
        on e.id_legajo = l.id_legajo
      where
        e.Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        e.id_legajo = p_id_legajo;
    exception
      when NO_DATA_FOUND then
        v_matricula_version := 0;
    end;

    --DomicilioCompleto
    OPEN o_Cursor FOR
      select s.id_tramite_ipj, s.id_legajo, s.Id_entidad_Socio, s.Id_Tipo_Integrante, s.Cuota, to_char(s.Fecha_Inicio, 'dd/mm/rrrr') fecha_inicio,
        to_char(s.Fecha_Fin, 'dd/mm/rrrr') fecha_fin, s.Id_Legajo_Socio, s.Cuit_Empresa,
        upper(s.N_Empresa) N_Empresa, s.En_Formacion, s.Matricula, to_char(s.Fec_Acta, 'dd/mm/rrrr') fec_acta,
        s.Id_Tipo_Entidad, s.Certificacion_Contable, s.Matricula_Certificante, s.Id_Integrante,
        s.Id_Modo_Part, s.Id_Integrante_Representante, s.Id_Tipo_Repres,
        to_char(s.Fecha_Repres, 'dd/mm/rrrr') fecha_repres ,s.Id_Vin_Empresa,
        i1.Nro_Documento, upper(p1.Nov_Nombre || ' ' || p1.Nov_Apellido) Detalle, p1.Cuil,
        to_char(p1.Fecha_Nacimiento, 'dd/mm/rrrr') Fecha_Nacimiento,
        (select lower(n_estado_civil) from RCIVIL.VT_ESTADOS_CIVIL where id_estado_civil = (case when i1.id_estado_civil is not null then i1.id_estado_civil else p1.Id_Estado_Civil end)) N_Estado_Civil,
        Initcap(Pais1.Pai_Nacionalidad) Pai_Nacionalidad,
        IPJ.VARIOS.FC_ARMAR_CALLE_DOM(d1.n_calle, d1.altura, d1.piso, d1.depto, d1.torre, d1.mzna, d1.lote, d1.n_barrio, d1.km, d1.n_tipocalle) calle_inf,
        initcap(D1.N_Calle) Calle, D1.Altura, D1.Piso, D1.Depto, D1.Torre, initcap(D1.N_Barrio) Barrio,
        initcap(D1.N_Localidad) Localidad, D1.CPA Cp, initcap(D1.N_Departamento) Departamento,
        initcap(D1.N_Provincia) Provincia, d1.mzna Manzana, d1.Lote, d1.km,
        ( select initcap(p.n_pais) from DOM_MANAGER.VT_PROVINCIAS pr join DOM_MANAGER.VT_PAISES p  on pr.id_pais = p.id_pais where id_provincia = d1.id_provincia) PaisDomicilio,
        initcap(IPJ.VARIOS.FC_Buscar_Profesion(i1.id_sexo, i1.nro_documento, i1.pai_cod_pais, i1.id_numero)) N_Profesion,
        IPJ.TYPES.fc_numero_a_letras(s.cuota ) CuotaLetras,
        sex.tipo_sexo sexo,
        IPJ.VARIOS.FC_Numero_Matricula(IPJ.ENTIDAD_PERSJUR.FC_Buscar_Socio_Matr(p_Id_Tramite_Ipj, s.id_entidad_socio)) Matricula_Entidad,
        replace(IPJ.VARIOS.FC_Letra_Matricula(IPJ.ENTIDAD_PERSJUR.FC_Buscar_Socio_Matr(p_Id_Tramite_Ipj, s.id_entidad_socio)), '#', '') Letra_Matr_Entidad,
        IPJ.ENTIDAD_PERSJUR.FC_Buscar_Socio_Matr_Vers(p_Id_Tramite_Ipj, s.id_entidad_socio) version_entidad,
        IPJ.ENTIDAD_PERSJUR.FC_Buscar_Socio_Folio(p_Id_Tramite_Ipj, s.id_entidad_socio) folio,
        IPJ.ENTIDAD_PERSJUR.FC_Buscar_Socio_Anio(p_Id_Tramite_Ipj, s.id_entidad_socio) anio,
        (case
          when nvl(s.fecha_fin, sysdate) >= trunc(sysdate) then 'S'
          else 'N'
        end ) Vigente,
        ( select count(1) from ipj.t_entidades_socios_usuf us where us.id_entidad_socio = s.id_entidad_socio ) hay_usufructo
      from ipj.t_entidades_socios s join ipj.t_entidades e
          on s.id_tramite_ipj = e.id_tramite_ipj and s.id_legajo = e.id_legajo
        join ipj.t_integrantes i1
          on s.id_integrante = i1.id_integrante
        join RCIVIL.VT_PERSONAS_CUIL p1
          on p1.id_sexo = i1.id_sexo and p1.nro_documento = i1.nro_documento and p1.pai_cod_pais = i1.pai_cod_pais and p1.id_numero = i1.id_numero
        left join RCIVIL.VT_PAISES pais1
          on PAIS1.ID_PAIS = P1.PAI_COD_PAIS_NACIONALIDAD
        left join -- Lista de id_vin en rcivil de los socios del tramite
          ( select i.id_integrante, IPJ.VARIOS.FC_Buscar_Dom_Persona(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3) id_vin
              from IPJ.t_entidades_socios s join ipj.t_integrantes i
                  on s.id_integrante = i.id_integrante
              where
                id_tramite_ipj = p_id_tramite_ipj
            ) tmp
          on
            tmp.id_integrante = i1.id_integrante
        left join DOM_MANAGER.VT_DOMICILIOS_COND d1
          on tmp.id_vin = d1.id_vin
        join T_COMUNES.VT_SEXOS sex
          on sex.id_sexo = i1.id_sexo
      where
        nvl(s.id_integrante, 0) <> 0 and
        s.id_legajo = p_id_legajo and
        e.matricula_version <= v_matricula_version and
        e.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_ubicacion_leg) and
        (s.borrador = 'N' or s.Id_Tramite_Ipj = p_Id_Tramite_Ipj) and
        (s.fecha_fin is null or s.fecha_fin >= to_date(sysdate, 'dd/mm/rrrr') or  s.Id_Tramite_Ipj = p_Id_Tramite_Ipj)

      UNION -- LISTAMOS EMPRESA PERSONAS ARRIBA Y EMPRESAS ABAJO

      select s.id_tramite_ipj, s.id_legajo, s.Id_entidad_Socio, s.Id_Tipo_Integrante, s.Cuota, to_char(s.Fecha_Inicio, 'dd/mm/rrrr') fecha_inicio,
        to_char(s.Fecha_Fin, 'dd/mm/rrrr') fecha_fin, s.Id_Legajo_Socio, s.Cuit_Empresa,
        decode(s.cuota_compartida, 'S', 'Co-Propiedad', upper(s.N_Empresa)) N_Empresa,
        s.En_Formacion, s.Matricula, to_char(s.Fec_Acta, 'dd/mm/rrrr') fec_acta,
        s.Id_Tipo_Entidad, s.Certificacion_Contable, s.Matricula_Certificante, s.Id_Integrante,
        s.Id_Modo_Part, s.Id_Integrante_Representante, s.Id_Tipo_Repres,
        to_char(s.Fecha_Repres, 'dd/mm/rrrr') fecha_repres ,s.Id_Vin_Empresa,
        null Nro_Documento, null Detalle, null Cuil,
        null Fecha_Nacimiento,
        null N_Estado_Civil, null Pai_Nacionalidad,
        IPJ.VARIOS.FC_ARMAR_CALLE_DOM(d1.n_calle, d1.altura, d1.piso, d1.depto, d1.torre, d1.mzna, d1.lote, d1.n_barrio, d1.km, d1.n_tipocalle) calle_inf,
        initcap(D1.n_Calle) Calle, D1.Altura, D1.Piso, D1.Depto, D1.Torre, initcap(D1.n_Barrio) Barrio, initcap(D1.n_Localidad) Localidad, D1.cpa Cp,
        initcap(D1.n_Departamento) Departamento, initcap(D1.n_Provincia) Provincia,
        D1.mzna Manzana, D1.Lote, d1.km,
        initcap((select n_pais
                    from Dom_Manager.Vt_Provincias Pr Join Dom_Manager.vt_Paises Pa
                       on pr.id_pais = pa.id_pais
                    where
                       Pr.Id_Provincia = d1.id_provincia)) PaisDomicilio,
        null N_Profesion,
        IPJ.TYPES.fc_numero_a_letras(s.cuota) CuotaLetras,
        null sexo,
        IPJ.VARIOS.FC_Numero_Matricula(IPJ.ENTIDAD_PERSJUR.FC_Buscar_Socio_Matr(p_Id_Tramite_Ipj, s.id_entidad_socio)) Matricula_Entidad,
        replace(IPJ.VARIOS.FC_Letra_Matricula(IPJ.ENTIDAD_PERSJUR.FC_Buscar_Socio_Matr(p_Id_Tramite_Ipj, s.id_entidad_socio)), '#', '') Letra_Matr_Entidad,
        IPJ.ENTIDAD_PERSJUR.FC_Buscar_Socio_Matr_Vers(p_Id_Tramite_Ipj, s.id_entidad_socio) version_entidad,
        IPJ.ENTIDAD_PERSJUR.FC_Buscar_Socio_Folio(p_Id_Tramite_Ipj, s.id_entidad_socio) folio,
        IPJ.ENTIDAD_PERSJUR.FC_Buscar_Socio_Anio(p_Id_Tramite_Ipj, s.id_entidad_socio) anio,
        (case
          when nvl(s.fecha_fin, sysdate) >= trunc(sysdate) then 'S'
          else 'N'
        end ) Vigente,
        ( select count(1) from ipj.t_entidades_socios_usuf us where us.id_entidad_socio = s.id_entidad_socio ) hay_usufructo
      from t_entidades_socios s join ipj.t_entidades e
          on s.id_tramite_ipj = e.id_tramite_ipj and s.id_legajo = e.id_legajo
        left join DOM_MANAGER.VT_DOMICILIOS_COND d1
          on D1.Id_Vin = s.id_vin_empresa
      where
        nvl(s.id_integrante, 0) = 0 and
        s.id_legajo = p_id_legajo and
        e.matricula_version <= v_matricula_version and
        e.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_ubicacion_leg) and
        (s.borrador = 'N' or s.Id_Tramite_Ipj = p_Id_Tramite_Ipj) and
        (s.fecha_fin is null or s.fecha_fin >= to_date(sysdate, 'dd/mm/rrrr') or  s.Id_Tramite_Ipj = p_Id_Tramite_Ipj)
    ;

  END SP_Traer_PersJur_Socios_Inf;

  PROCEDURE SP_Traer_PersJur_Admin_Inf(
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number,
    p_Cursor OUT TYPES.cursorType)
  IS
  /***************************************************************
    ADVERTENCIA: ESTE SP carga la lista de autoridades de CIDI, si se agregan campos avisar
    Este procedimiento lista las autoridades distintas de SOCIO de los
    organos de administracion y fiscalizacion de una empresa
  ***************************************************************/
    v_matricula_version number(6,0);
    v_now date;
    v_id_Ult_autor number;
    v_id_Ult_autor_sindico number;
    v_id_Ult_autor_repres number;
    v_id_ubicacion_ent number;
    v_id_admin number;
    v_requiere_sindico varchar2(1);
    v_fiscalizacion_ejerc varchar2(1);
    v_hab_sindico number;
  BEGIN

    if IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('HAB_LOG_GESTION') = '1' then
      IPJ.VARIOS.SP_GUARDAR_LOG(
        p_METODO => 'SP_Traer_PersJur_Admin_Inf',
        p_NIVEL => 'Gestion',
        p_ORIGEN => 'DB',
        p_USR_LOG  => 'DB',
        p_USR_WINDOWS => 'Sistema',
        p_IP_PC  => null,
        p_EXCEPCION =>
          'Id Tramite IPJ = ' || to_char(p_Id_Tramite_Ipj)
          || ' / Id Legajo = ' || to_char(p_id_legajo)
      );
    end if;

    -- Busco la version de la matricula, asociada al tramite
    begin
      select nvl(e.matricula_version, 0), te.id_ubicacion, requiere_sindico, fiscalizacion_ejerc
          into v_matricula_version, v_id_ubicacion_ent, v_requiere_sindico, v_fiscalizacion_ejerc
      from IPJ.t_entidades e  join ipj.t_tipos_entidades te
        on e.id_tipo_entidad = te.id_tipo_entidad
      where
        e.Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        e.id_legajo = p_id_legajo;
    exception
      when NO_DATA_FOUND then
        v_matricula_version := 0;
        v_requiere_sindico := 'N';
        v_fiscalizacion_ejerc := 'N';

        select id_ubicacion into v_id_ubicacion_ent
        from ipj.t_legajos l join ipj.t_tipos_entidades te
          on l.id_tipo_entidad = te.id_tipo_entidad
        where
          l.id_legajo = p_id_legajo;
    end;

    -- Veo si se habilita o no los sindicos, seg�n el �ltimo tr�mite
    if v_requiere_sindico = 'S' or (v_requiere_sindico = 'O' and v_fiscalizacion_ejerc = 'S') then
      v_hab_sindico := 1;
    else
      v_hab_sindico := 0;
    end if;

    -- BUSCO el ultimo tr�mite de administradores, ordenando dependiendo del area
    v_id_Ult_autor := FC_Ult_Tram_Admin(p_id_legajo, v_id_ubicacion_ent,  v_matricula_version, p_Id_Tramite_Ipj);
    v_id_Ult_autor_sindico := FC_Ult_Tram_Sindico(p_id_legajo, v_id_ubicacion_ent,  v_matricula_version, p_Id_Tramite_Ipj);
    v_id_Ult_autor_repres := FC_Ult_Tram_Repres(p_id_legajo, v_id_ubicacion_ent,  v_matricula_version, p_Id_Tramite_Ipj);
    DBMS_OUTPUT.PUT_LINE('SP_Traer_PersJur_Admin_Inf: ');
    DBMS_OUTPUT.PUT_LINE('  - Ult. Tram. Admin.: ' || to_char(v_id_Ult_autor));
    DBMS_OUTPUT.PUT_LINE('  - Ult. Tram. Sindico: ' || to_char(v_id_Ult_autor_sindico));
    DBMS_OUTPUT.PUT_LINE('  - Ult. Tram. Sindico: ' || to_char(v_id_Ult_autor_repres));
    DBMS_OUTPUT.PUT_LINE('  - Versi�n Matr�cula: ' || to_char(v_matricula_version));
    DBMS_OUTPUT.PUT_LINE('  - Ubic. Entidad: ' || to_char(v_id_ubicacion_ent));

    OPEN p_Cursor FOR
      select id_tipo_integrante, id_legajo, fecha_inicio, fecha_fin, id_integrante,
        Id_Tramite_Ipj, n_tipo_integrante, detalle, ID_NUMERO, ID_SEXO,
        NRO_DOCUMENTO, PAI_COD_PAIS, borrador, ID_VIN, ID_TIPO_ORGANISMO,
        n_tipo_organismo, ERROR_DATO, id_tipodom, n_tipodom, id_tipocalle,
        IPJ.VARIOS.FC_ARMAR_CALLE_DOM(n_calle, altura, piso, depto, torre, Manzana, lote, n_barrio, km, n_tipocalle) calle_inf,
        n_tipocalle, id_calle, n_calle, altura, depto, piso, torre, id_barrio, Manzana, Lote,
        n_barrio, id_localidad, n_localidad, id_departamento, n_departamento,
        id_provincia, n_provincia, cpa, IdTramiteIpj_Entidad, Id_Entidades_Accion,
        Clase, Orden_Org, Orden_Cargo, Estado, sexo, Cuil, N_Estado_Civil,
        Fecha_Nacimiento, Pai_Nacionalidad, N_Profesion,
        n_empresa, cuit_empresa, matricula, nvl(es_admin, 'S') es_admin, Habilitado_Present,
        (case when upper(Estado) like 'VIGENTE%' then 'S' else 'N' end) Habilitado,
        Matricula_Entidad,  Letra_Matr_Entidad, version_entidad, folio, anio
        --ADVERTENCIA: ESTE SP carga la lista de autoridades de CIDI, si se agregan campos avisar
        -- TAMBIEN AGREGAR LOS CAMPOS EN : IPJ.PK_TRAMITES_ONLINE.FC_Normaliz_Autorizado
      from
       (--Organo de Administracion PERSONAS FISICAS
        select ie.id_tipo_integrante, ie.id_legajo, to_char(ie.fecha_inicio, 'dd/mm/rrrr') fecha_inicio,
          to_char(ie.fecha_fin, 'dd/mm/rrrr') fecha_fin,
          ie.id_integrante, ie.Id_Tramite_Ipj, ti.n_tipo_integrante,
          nvl(ie.n_empresa, p.NOMBRE || ' ' ||p.APELLIDO) detalle,
          I.ID_NUMERO, I.ID_SEXO, I.NRO_DOCUMENTO, I.PAI_COD_PAIS, ie.borrador,
          IE.ID_VIN, IE.ID_TIPO_ORGANISMO, org.n_tipo_organismo,
          I.ERROR_DATO,
          IPJ.VARIOS.FC_ARMAR_CALLE_DOM(d.n_calle, d.altura, d.piso, d.depto, d.torre, d.mzna, d.lote, d.n_barrio, d.km, d.n_tipocalle) calle_inf,
          d.id_tipodom, d.n_tipodom, d.id_tipocalle, d.n_tipocalle, d.id_calle,
          initcap(d.n_calle) n_calle, d.altura, d.depto, d.piso, d.torre, d.id_barrio,
          initcap(d.n_barrio) n_barrio, /*d.nro_mail, d.cod_area,*/
          D.mzna Manzana, D.Lote, d.km,
          d.id_localidad, initcap(d.n_localidad) n_localidad, d.id_departamento,
          initcap(d.n_departamento) n_departamento, d.id_provincia,
          initcap(d.n_provincia) n_provincia, d.cpa, p_Id_Tramite_Ipj IdTramiteIpj_Entidad,
          Ie.Id_Entidades_Accion, Acc.Clase, Org.Nro_Orden Orden_Org, Ti.Nro_Orden Orden_Cargo,
          (Case
             when p.fec_defuncion is not null then 'Fallecido en la fecha ' || to_char(p.fec_defuncion, 'dd/mm/rrrr')
             when p.fec_defuncion is null and nvl(IPJ.ENTIDAD_PERSJUR.FC_ESTADO_GRAVAMEN_INTERV(p_id_legajo), '--') <> '--' then 'Desplazado por Com. Norm.'
             when p.fec_defuncion is null and ie.fecha_fin < sysdate and to_date(ie.fecha_fin +120, 'dd/mm/rrrr') > sysdate then 'Vigente, con extensi�n de 120 d�as'
             when p.fec_defuncion is null and ie.fecha_fin > sysdate then 'Vigente'
             when p.fec_defuncion is null and ie.fecha_fin IS NULL then 'Vigente'
             else 'Vencido'
          end) Estado,
          sex.tipo_sexo sexo, p.Cuil,
          (select lower(n_estado_civil) from RCIVIL.VT_ESTADOS_CIVIL where id_estado_civil = (case when i.id_estado_civil is not null then i.id_estado_civil else p.Id_Estado_Civil end)) N_Estado_Civil,
          to_char(p.Fecha_Nacimiento, 'dd/mm/rrrr') Fecha_Nacimiento,
          Initcap(Pais.Pai_Nacionalidad) Pai_Nacionalidad,
          initcap(IPJ.VARIOS.FC_Buscar_Profesion(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero)) N_Profesion,
          null n_empresa, null cuit_empresa, null matricula, org.es_admin,
          ie.Habilitado_Present,
          IPJ.VARIOS.FC_Numero_Matricula(tpj.matricula) Matricula_Entidad,
          replace(IPJ.VARIOS.FC_Letra_Matricula(tpj.matricula), '#', '') Letra_Matr_Entidad,
          tpj.matricula_version version_entidad, tpj.folio, tpj.anio
        from ipj.t_entidades_admin_hist h join ipj.t_entidades_admin ie
            on h.id_legajo = ie.id_legajo and h.id_admin = ie.id_admin
          join ipj.t_integrantes i
            on ie.id_integrante = i.id_integrante
          join ipj.t_tipos_integrante ti
            on ie.id_tipo_integrante = ti.id_tipo_integrante
          left join IPJ.T_TIPOS_ORGANISMO org
            on org.id_tipo_organismo = ie.id_tipo_organismo
          join ipj.t_Entidades tpj
            on ie.Id_Tramite_Ipj = tpj.Id_Tramite_Ipj and ie.id_legajo = tpj.id_legajo
          join ipj.t_tramitesipj tr
            on tr.id_tramite_ipj = ie.id_tramite_ipj
          left join ipj.t_entidades_acciones acc
            on ie.id_legajo = acc.id_legajo and Ie.Id_Entidades_Accion = Acc.Id_Entidades_Accion
          left join rcivil.vt_pk_persona p
            on i.id_sexo = p.ID_SEXO and
              i.nro_documento = p.NRO_DOCUMENTO and
              i.pai_cod_pais = p.PAI_COD_PAIS and
              i.id_numero = p.ID_NUMERO
          left join dom_manager.VT_DOMICILIOS_COND d
            on d.id_vin = ie.id_vin
          join T_COMUNES.VT_SEXOS sex
            on sex.id_sexo = i.id_sexo
          left join RCIVIL.VT_PAISES pais
            on pais.id_pais = p.pai_cod_pais_nacionalidad
        where
          ie.id_integrante is not null and
          tr.id_estado_ult < 200 and -- Tramites no rechazados
          tpj.id_legajo = p_id_legajo and
          h.Id_Tramite_Ipj = nvl(v_id_Ult_autor, v_id_Ult_autor_sindico) and
          (h.fecha_fin >= trunc(SYSDATE) OR h.Id_Motivo_Baja is null)

        UNION ALL

        --Organo de Administracion PERSONAS JURIDICAS
        select ie.id_tipo_integrante, ie.id_legajo, to_char(ie.fecha_inicio, 'dd/mm/rrrr') fecha_inicio,
          to_char(ie.fecha_fin, 'dd/mm/rrrr') fecha_fin,
          ie.id_integrante, ie.Id_Tramite_Ipj, ti.n_tipo_integrante,
          ie.n_empresa detalle,
          null ID_NUMERO, null ID_SEXO, null NRO_DOCUMENTO, null PAI_COD_PAIS, ie.borrador,
          IE.ID_VIN, IE.ID_TIPO_ORGANISMO, org.n_tipo_organismo,
          null ERROR_DATO,
          IPJ.VARIOS.FC_ARMAR_CALLE_DOM(d.n_calle, d.altura, d.piso, d.depto, d.torre, d.mzna, d.lote, d.n_barrio, d.km, d.n_tipocalle) calle_inf,
          d.id_tipodom, d.n_tipodom, d.id_tipocalle, d.n_tipocalle, d.id_calle,
          initcap(d.n_calle) n_calle, d.altura, d.depto, d.piso, d.torre, d.id_barrio,
          initcap(d.n_barrio) n_barrio, /*d.nro_mail, d.cod_area,*/
          D.mzna Manzana, D.Lote, d.km,
          d.id_localidad, initcap(d.n_localidad) n_localidad, d.id_departamento,
          initcap(d.n_departamento) n_departamento, d.id_provincia,
          initcap(d.n_provincia) n_provincia, d.cpa, p_Id_Tramite_Ipj IdTramiteIpj_Entidad,
          Ie.Id_Entidades_Accion, Acc.Clase, Org.Nro_Orden Orden_Org, Ti.Nro_Orden Orden_Cargo,
          (Case
             when nvl(IPJ.ENTIDAD_PERSJUR.FC_ESTADO_GRAVAMEN_INTERV(p_id_legajo), '--') <> '--' then 'Desplazado por Com. Norm.'
             when ie.fecha_fin < sysdate and to_date(ie.fecha_fin +120, 'dd/mm/rrrr') > sysdate then 'Vigente, con extensi�n de 120 d�as'
             when ie.fecha_fin > sysdate then 'Vigente'
             when ie.fecha_fin IS NULL then 'Vigente'
             else 'Vencido'
          end) Estado,
          null sexo, null Cuil,
          null N_Estado_Civil,
          null Fecha_Nacimiento,
          null Pai_Nacionalidad,
          null N_Profesion,
           ie.n_empresa, ie.cuit_empresa, ie.matricula, org.es_admin,
           ie.Habilitado_Present,
          IPJ.VARIOS.FC_Numero_Matricula(tpj.matricula) Matricula_Entidad,
          replace(IPJ.VARIOS.FC_Letra_Matricula(tpj.matricula), '#', '') Letra_Matr_Entidad,
          tpj.matricula_version version_entidad, tpj.folio, tpj.anio
        from ipj.t_entidades_admin_hist h join ipj.t_entidades_admin ie
            on h.id_legajo = ie.id_legajo and h.id_admin = ie.id_admin
          join ipj.t_tipos_integrante ti
            on ie.id_tipo_integrante = ti.id_tipo_integrante
          left join IPJ.T_TIPOS_ORGANISMO org
            on org.id_tipo_organismo = ie.id_tipo_organismo
          join ipj.t_Entidades tpj
            on ie.Id_Tramite_Ipj = tpj.Id_Tramite_Ipj and ie.id_legajo = tpj.id_legajo
          join ipj.t_tramitesipj tr
            on tr.id_tramite_ipj = ie.id_tramite_ipj
          left join ipj.t_entidades_acciones acc
            on ie.id_legajo = acc.id_legajo and Ie.Id_Entidades_Accion = Acc.Id_Entidades_Accion
          left join dom_manager.VT_DOMICILIOS_COND d
            on d.id_vin = ie.id_vin
        where
          ie.id_integrante is null and
          tr.id_estado_ult < 200 and -- Tramites no rechazados
          tpj.id_legajo = p_id_legajo and
          h.Id_Tramite_Ipj = nvl(v_id_Ult_autor, v_id_Ult_autor_sindico) and
          (h.fecha_fin >= trunc(SYSDATE) OR h.Id_Motivo_Baja is null)

        UNION ALL

        --Organo de Fiscalizacion PERSONAS FISICAS
        select ie.id_tipo_integrante, ie.id_legajo, to_char(ie.fecha_alta, 'dd/mm/rrrr') fecha_inicio,
          to_char(ie.fecha_baja, 'dd/mm/rrrr') fecha_fin,
          ie.id_integrante, ie.Id_Tramite_Ipj, ti.n_tipo_integrante,
          p.NOMBRE || ' ' ||p.APELLIDO detalle,
          I.ID_NUMERO, I.ID_SEXO, I.NRO_DOCUMENTO, I.PAI_COD_PAIS, ie.borrador,
          null ID_VIN, ti.ID_TIPO_ORGANISMO, org.n_tipo_organismo,
          I.ERROR_DATO,
          null calle_inf,
          null id_tipodom, null n_tipodom, null id_tipocalle, null n_tipocalle, null id_calle,
          null n_calle, null altura, null depto, null piso, null torre, null id_barrio,
          null n_barrio, /*d.nro_mail, d.cod_area,*/
          null Manzana,  null Lote, null km,
          null id_localidad, null n_localidad, null id_departamento,
          null n_departamento, null id_provincia,
          null n_provincia, null cpa, p_Id_Tramite_Ipj IdTramiteIpj_Entidad,
          Ie.Id_Entidades_Accion, Acc.Clase, Org.Nro_Orden Orden_Org, Ti.Nro_Orden Orden_Cargo,
          (Case
              when p.fec_defuncion is not null then 'Fallecido en la fecha ' || to_char(p.fec_defuncion, 'dd/mm/rrrr')
              when p.fec_defuncion is null and nvl(IPJ.ENTIDAD_PERSJUR.FC_ESTADO_GRAVAMEN_INTERV(p_id_legajo), '--') <> '--' then 'Desplazado por Com. Norm.'
              when p.fec_defuncion is null and ie.fecha_baja < sysdate and to_date(ie.fecha_baja +120, 'dd/mm/rrrr') > sysdate then 'Vigente, con extensi�n de 120 d�as'
              when p.fec_defuncion is null and ie.fecha_baja > sysdate then 'Vigente'
              when p.fec_defuncion is null and ie.fecha_baja IS NULL then 'Vigente'
              else 'Vencido'
          end) Estado,
          sex.tipo_sexo sexo, p.Cuil,
          (select lower(n_estado_civil) from RCIVIL.VT_ESTADOS_CIVIL where id_estado_civil = (case when i.id_estado_civil is not null then i.id_estado_civil else p.Id_Estado_Civil end)) N_Estado_Civil,
          to_char(p.Fecha_Nacimiento, 'dd/mm/rrrr') Fecha_Nacimiento,
          Initcap(Pais.Pai_Nacionalidad) Pai_Nacionalidad,
          initcap(IPJ.VARIOS.FC_Buscar_Profesion(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero)) N_Profesion,
          null n_empresa, null cuit_empresa, null matricula, org.es_admin,
          null Habilitado_Present,
          IPJ.VARIOS.FC_Numero_Matricula(tpj.matricula) Matricula_Entidad,
          replace(IPJ.VARIOS.FC_Letra_Matricula(tpj.matricula), '#', '') Letra_Matr_Entidad,
          tpj.matricula_version version_entidad, tpj.folio, tpj.anio
        from ipj.T_Entidades_Sindico_Hist h join ipj.T_Entidades_Sindico ie
            on h.id_legajo = ie.id_legajo and h.id_entidad_sindico = ie.id_entidad_sindico
          join ipj.t_integrantes i
            on ie.id_integrante = i.id_integrante
          join ipj.t_tipos_integrante ti
            on ie.id_tipo_integrante = ti.id_tipo_integrante
          left join IPJ.T_TIPOS_ORGANISMO org
            on org.id_tipo_organismo = ti.id_tipo_organismo
          join ipj.t_Entidades tpj
            on ie.Id_Tramite_Ipj = tpj.Id_Tramite_Ipj and ie.id_legajo = tpj.id_legajo
          join ipj.t_tramitesipj tr
            on tr.id_tramite_ipj = ie.id_tramite_ipj
          left join ipj.t_entidades_acciones acc
            on ie.id_legajo = acc.id_legajo and Ie.Id_Entidades_Accion = Acc.Id_Entidades_Accion
          left join rcivil.vt_pk_persona p
            on i.id_sexo = p.ID_SEXO and
              i.nro_documento = p.NRO_DOCUMENTO and
              i.pai_cod_pais = p.PAI_COD_PAIS and
              i.id_numero = p.ID_NUMERO
          join T_COMUNES.VT_SEXOS sex
            on sex.id_sexo = i.id_sexo
          left join RCIVIL.VT_PAISES pais
            on pais.id_pais = p.pai_cod_pais_nacionalidad
        where
          v_hab_sindico = 1 and -- Solo si el tr�mite tiene habilitado Sindicatura
          tr.id_estado_ult < 200 and -- Tramites no rechazados
          ie.id_integrante is not null and
          tpj.id_legajo = p_id_legajo and
          h.Id_Tramite_Ipj = nvl(v_id_Ult_autor_sindico, v_id_Ult_autor) and
          (h.fecha_baja >= trunc(SYSDATE) OR h.Id_Motivo_Baja is null)

        UNION ALL

        --Organo de Fiscalizacion PERSONAS JURIDICA
        select ie.id_tipo_integrante, ie.id_legajo, to_char(ie.fecha_alta, 'dd/mm/rrrr') fecha_inicio,
          to_char(ie.fecha_baja, 'dd/mm/rrrr') fecha_fin,
          ie.id_integrante, ie.Id_Tramite_Ipj, ti.n_tipo_integrante,
          ie.n_empresa detalle,
          null ID_NUMERO, null ID_SEXO, null NRO_DOCUMENTO, null PAI_COD_PAIS, ie.borrador,
          null ID_VIN, ti.ID_TIPO_ORGANISMO, org.n_tipo_organismo,
          null ERROR_DATO,
          null calle_inf,
          null id_tipodom, null n_tipodom, null id_tipocalle, null n_tipocalle, null id_calle,
          null n_calle, null altura, null depto, null piso, null torre, null id_barrio,
          null n_barrio, /*d.nro_mail, d.cod_area,*/
          null Manzana,  null Lote, null km,
          null id_localidad, null n_localidad, null id_departamento,
          null n_departamento, null id_provincia,
          null n_provincia, null cpa, p_Id_Tramite_Ipj IdTramiteIpj_Entidad,
          Ie.Id_Entidades_Accion, Acc.Clase, Org.Nro_Orden Orden_Org, Ti.Nro_Orden Orden_Cargo,
          (Case
              when nvl(IPJ.ENTIDAD_PERSJUR.FC_ESTADO_GRAVAMEN_INTERV(p_id_legajo), '--') <> '--' then 'Desplazado por Com. Norm.'
              when ie.fecha_baja < sysdate and to_date(ie.fecha_baja +120, 'dd/mm/rrrr') > sysdate then 'Vigente, con extensi�n de 120 d�as'
              when ie.fecha_baja > sysdate then 'Vigente'
              when ie.fecha_baja IS NULL then 'Vigente'
              else 'Vencido'
          end) Estado,
          null sexo, null Cuil,
          null N_Estado_Civil,
          null Fecha_Nacimiento,
          null Pai_Nacionalidad,
          null N_Profesion,
          ie.n_empresa, ie.cuit_empresa, ie.matricula, org.es_admin,
          null Habilitado_Present,
          IPJ.VARIOS.FC_Numero_Matricula(tpj.matricula) Matricula_Entidad,
          replace(IPJ.VARIOS.FC_Letra_Matricula(tpj.matricula), '#', '') Letra_Matr_Entidad,
          tpj.matricula_version version_entidad, tpj.folio, tpj.anio
        from ipj.T_Entidades_Sindico_Hist h join ipj.T_Entidades_Sindico ie
            on h.id_legajo = ie.id_legajo and h.id_entidad_sindico = ie.id_entidad_sindico
          join ipj.t_tipos_integrante ti
            on ie.id_tipo_integrante = ti.id_tipo_integrante
          left join IPJ.T_TIPOS_ORGANISMO org
            on org.id_tipo_organismo = ti.id_tipo_organismo
          join ipj.t_Entidades tpj
            on ie.Id_Tramite_Ipj = tpj.Id_Tramite_Ipj and ie.id_legajo = tpj.id_legajo
          join ipj.t_tramitesipj tr
            on tr.id_tramite_ipj = ie.id_tramite_ipj
          left join ipj.t_entidades_acciones acc
            on ie.id_legajo = acc.id_legajo and Ie.Id_Entidades_Accion = Acc.Id_Entidades_Accion
        where
          v_hab_sindico = 1 and -- Solo si el tr�mite tiene habilitado Sindicatura
          ie.id_integrante is null and
          tr.id_estado_ult < 200 and -- Tramites no rechazados
          tpj.id_legajo = p_id_legajo and
          h.Id_Tramite_Ipj = nvl(v_id_Ult_autor_sindico, v_id_Ult_autor) and
          (h.fecha_baja >= trunc(SYSDATE) OR h.Id_Motivo_Baja is null)

        UNION ALL
        --Organo de REPRESENTACION
        select null id_tipo_integrante, er.id_legajo, to_char(er.fecha_alta, 'dd/mm/rrrr') fecha_inicio,
          to_char(er.fecha_baja, 'dd/mm/rrrr') fecha_fin,
          er.id_integrante, er.Id_Tramite_Ipj, 'Representante' n_tipo_integrante,
          p.NOMBRE || ' ' ||p.APELLIDO detalle,
          I.ID_NUMERO, I.ID_SEXO, I.NRO_DOCUMENTO, I.PAI_COD_PAIS, er.borrador,
          null ID_VIN, null ID_TIPO_ORGANISMO, '' n_tipo_organismo,
          I.ERROR_DATO,
          null calle_inf,
          null id_tipodom, null n_tipodom, null id_tipocalle, null n_tipocalle, null id_calle,
          null n_calle, null altura, null depto, null piso, null torre, null id_barrio,
          null n_barrio, /*d.nro_mail, d.cod_area,*/
          null Manzana,  null Lote, null km,
          null id_localidad, null n_localidad, null id_departamento,
          null n_departamento, null id_provincia,
          null n_provincia, null cpa, p_Id_Tramite_Ipj IdTramiteIpj_Entidad,
          null Id_Entidades_Accion, null Clase, 1 Orden_Org, 1 Orden_Cargo,
          (Case
              when p.fec_defuncion is not null then 'Fallecido en la fecha ' || to_char(p.fec_defuncion, 'dd/mm/rrrr')
              when p.fec_defuncion is null and nvl(IPJ.ENTIDAD_PERSJUR.FC_ESTADO_GRAVAMEN_INTERV(p_id_legajo), '--') <> '--' then 'Desplazado por Com. Norm.'
              when p.fec_defuncion is null and er.fecha_baja < sysdate and to_date(er.fecha_baja +120, 'dd/mm/rrrr') > sysdate then 'Vigente, con extensi�n de 120 d�as'
              when p.fec_defuncion is null and er.fecha_baja > sysdate then 'Vigente'
              when p.fec_defuncion is null and er.fecha_baja IS NULL then 'Vigente'
              else 'Vencido'
          end) Estado,
          sex.tipo_sexo sexo, p.Cuil,
          (select lower(n_estado_civil) from RCIVIL.VT_ESTADOS_CIVIL where id_estado_civil = (case when i.id_estado_civil is not null then i.id_estado_civil else p.Id_Estado_Civil end)) N_Estado_Civil,
          to_char(p.Fecha_Nacimiento, 'dd/mm/rrrr') Fecha_Nacimiento,
          Initcap(Pais.Pai_Nacionalidad) Pai_Nacionalidad,
          initcap(IPJ.VARIOS.FC_Buscar_Profesion(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero)) N_Profesion,
          null n_empresa, null cuit_empresa, null matricula, 'R' es_admin,
          'S' Habilitado_Present,
          IPJ.VARIOS.FC_Numero_Matricula(tpj.matricula) Matricula_Entidad,
          replace(IPJ.VARIOS.FC_Letra_Matricula(tpj.matricula), '#', '') Letra_Matr_Entidad,
          tpj.matricula_version version_entidad, tpj.folio, tpj.anio
        from ipj.T_Entidades_Representante er join ipj.t_integrantes i
            on er.id_integrante = i.id_integrante
          join ipj.t_Entidades tpj
            on er.Id_Tramite_Ipj = tpj.Id_Tramite_Ipj and er.id_legajo = tpj.id_legajo
          join ipj.t_tramitesipj tr
            on tr.id_tramite_ipj = er.id_tramite_ipj
          left join rcivil.vt_pk_persona p
            on i.id_sexo = p.ID_SEXO and
              i.nro_documento = p.NRO_DOCUMENTO and
              i.pai_cod_pais = p.PAI_COD_PAIS and
              i.id_numero = p.ID_NUMERO
          join T_COMUNES.VT_SEXOS sex
            on sex.id_sexo = i.id_sexo
          left join RCIVIL.VT_PAISES pais
            on pais.id_pais = p.pai_cod_pais_nacionalidad
        where
          tpj.id_legajo = p_id_legajo and
          tr.id_estado_ult < 200 and -- Tramites no rechazados
          er.Id_Tramite_Ipj = v_id_Ult_autor_repres and
          nvl(er.fecha_baja, sysdate) >= to_date(sysdate, 'dd/mm/rrrr')

        UNION ALL
        -- Comisiones Normalizadoras
        select tn.id_normalizador id_tipo_integrante, cn.id_legajo,
          to_char((select d.fecha_cargos from ipj.t_denuncias d where d.id_tramite_ipj = cn.id_tramite_ipj) , 'dd/mm/rrrr') fecha_inicio,
          null fecha_fin,
          cn.id_integrante, cn.Id_Tramite_Ipj, tn.n_normalizador n_tipo_integrante,
          (p.NOMBRE || ' ' ||p.APELLIDO) detalle, i.id_numero, i.id_sexo,
          i.nro_documento, i.pai_cod_pais, null borrador, null id_vin,
          null id_tipo_organismo, null n_tipo_organismo, i.error_dato, null calle_inf,
          null id_tipodom, null n_tipodom, null id_tipocalle, null n_tipocalle, null id_calle,
          null n_calle, null altura, null depto, null piso, null torre, null id_barrio,
          null n_barrio,  null Manzana, null Lote, null km, null id_localidad, null n_localidad,
          null id_departamento, null n_departamento, null id_provincia,
          null n_provincia, null cpa, cn.id_tramite_ipj IdTramiteIpj_Entidad,
          null Id_Entidades_Accion, null Clase, 50 Orden_Org, 500 Orden_Cargo,
          (Case
             when p.fec_defuncion is not null then 'Fallecido en la fecha ' || to_char(p.fec_defuncion, 'dd/mm/rrrr')
             when p.fec_defuncion is null then 'Vigente'
             else 'Vencido'
          end) Estado,
          sex.tipo_sexo sexo, p.Cuil,
          (select lower(n_estado_civil) from RCIVIL.VT_ESTADOS_CIVIL where id_estado_civil = (case when i.id_estado_civil is not null then i.id_estado_civil else p.Id_Estado_Civil end)) N_Estado_Civil,
          to_char(p.Fecha_Nacimiento, 'dd/mm/rrrr') Fecha_Nacimiento,
          Initcap(Pais.Pai_Nacionalidad) Pai_Nacionalidad,
          initcap(IPJ.VARIOS.FC_Buscar_Profesion(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero)) N_Profesion,
          null n_empresa, null cuit_empresa, null matricula, 'S' es_admin,
          null Habilitado_Present,
          null Matricula_Entidad,  null Letra_Matr_Entidad, null version_entidad, null folio, null anio
        from ipj.t_cn_normalizador cn  join ipj.t_integrantes i
            on cn.id_integrante = i.id_integrante
          join ipj.t_tipos_normalizador tn
            on tn.id_normalizador = cn.id_normalizador
          join ipj.t_tramitesipj tpj
            on cn.Id_Tramite_Ipj = tpj.Id_Tramite_Ipj
          join rcivil.vt_pk_persona p
            on i.id_sexo = p.ID_SEXO and
              i.nro_documento = p.NRO_DOCUMENTO and
              i.pai_cod_pais = p.PAI_COD_PAIS and
              i.id_numero = p.ID_NUMERO
          join T_COMUNES.VT_SEXOS sex
            on sex.id_sexo = i.id_sexo
          left join RCIVIL.VT_PAISES pais
            on pais.id_pais = p.pai_cod_pais_nacionalidad
        where
          cn.id_legajo = p_id_legajo and
          nvl(IPJ.ENTIDAD_PERSJUR.FC_ESTADO_GRAVAMEN_INTERV(p_id_legajo), '--') <> '--' and
          cn.id_estado_normalizador = 2 and -- Aceptado
          cn.id_motivo_baja is null and
          tpj.id_estado_ult < 100
      ) tmp
    where
      to_date(tmp.fecha_inicio, 'dd/mm/rrrr') <= to_date(sysdate, 'dd/mm/rrrr')
    order by orden_Org asc, orden_Cargo asc, detalle asc;

  END SP_Traer_PersJur_Admin_Inf;


  PROCEDURE SP_Traer_PersJur_Int_Socios(
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number,
    p_Cursor OUT TYPES.cursorType)
  IS
    v_matricula_version number(6,0);
    v_ubicacion_ent number(6);
    v_ubicacion_leg number(6);
  BEGIN
  /*********************************************************
  Este procemiento devuelve el listado de integrantes asociados a una entidad de un tramite,
  cuyo tipo sea igual a SOCIO
  **********************************************************/
    -- busco la ultima area y su version, para visualizar los datos correctos
    begin
      select nvl(matricula_version, 0),
        (select id_ubicacion from ipj.t_tipos_entidades te where te.id_tipo_entidad = e.id_tipo_entidad) ubicacion_ent,
        (select id_ubicacion from ipj.t_tipos_entidades te where te.id_tipo_entidad = l.id_tipo_entidad) ubicacion_leg
        into v_matricula_version, v_ubicacion_ent, v_ubicacion_leg
      from IPJ.t_entidades e join ipj.t_legajos l
        on e.id_legajo = l.id_legajo
      where
        e.Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        e.id_legajo = p_id_legajo;
    exception
      when NO_DATA_FOUND then
        v_matricula_version := 0;
    end;

    OPEN p_Cursor FOR
      select -- (PBI 12835)
        id_entidad_socio, id_tipo_integrante, cuota, id_legajo, fecha_inicio, fecha_fin,
        id_integrante, id_tramite_ipj, id_legajo_socio, cuit_empresa, n_empresa,
        cuota_compartida, folio, anio, n_tipo_integrante,  porc_capital, detalle,
        en_formacion, matricula, fec_acta, id_tipo_entidad, certificacion_contable,
        matricula_certificante, id_modo_part, id_integrante_representante, id_tipo_repres,
        fecha_repres, tipo_entidad, id_numero, id_sexo, nro_documento, pai_cod_pais,
        borrador, error_dato,persona, identificacion, identificacion_socio, n_copropiedad,
        idtramiteipj_entidad, id_tipo_socio, n_tipo_socio, persona_expuesta,
        (case
             when id_integrante is null and n_empresa is not null then n_empresa
             when id_integrante is not null then detalle
             when Cuota_Compartida = 'S' then n_copropiedad
        end) Nombre_Socio,
        (case
             when id_integrante is null and n_empresa is not null then 'CUIT Nro.'
             when id_integrante is not null then 'DNI Nro.'
             when Cuota_Compartida = 'S' then null
        end) Tipo_Identificacion,
        cuil, vigente
      from
      (
        -- PERSONAS FISICAS
        select ie.id_entidad_socio, ie.id_tipo_integrante, ie.cuota, ie.id_legajo,
          to_char(ie.fecha_inicio, 'dd/mm/rrrr') fecha_inicio,
          to_char(ie.fecha_fin, 'dd/mm/rrrr') fecha_fin, ie.id_integrante, ie.Id_Tramite_Ipj,
          ie.id_legajo_socio, ie.cuit_empresa, ie.n_empresa, nvl(ie.cuota_compartida, 'N') cuota_compartida,
          ie.folio, ie.anio,
          ti.n_tipo_integrante,  trim(To_Char(nvl(ie.Porc_Capital, '0'), '9990.99'))Porc_Capital,
          nvl(ie.n_empresa, p.NOMBRE || ' ' ||p.APELLIDO)  detalle,
          ie.En_Formacion, ie.Matricula, to_char(ie.Fec_Acta, 'dd/mm/rrrr') Fec_Acta,-- desde aca
          ie.Id_Tipo_Entidad, ie.Certificacion_Contable, ie.Matricula_Certificante,
          ie.Id_Modo_Part, ie.Id_Integrante_Representante, ie.Id_Tipo_Repres,
          to_char(ie.Fecha_Repres, 'dd/mm/rrrr') Fecha_Repres,                              -- hasta aca
          TE.TIPO_ENTIDAD,
          I.ID_NUMERO, I.ID_SEXO, I.NRO_DOCUMENTO, I.PAI_COD_PAIS, ie.borrador,
          I.ERROR_DATO,
          (case
             when ie.id_integrante is null and ie.n_empresa is not null then
                ie.n_empresa
             when ie.id_integrante is not null then
                (p.NOMBRE || ' ' ||p.APELLIDO)
             when Ie.Cuota_Compartida = 'S' then 'Cuota Compartida'
           end) Persona,
          (case
             when ie.id_integrante is null and ie.n_empresa is not null then ie.cuit_empresa
             when ie.id_integrante is not null then p.CUIL
             when Ie.Cuota_Compartida = 'S' then ''
           end) Identificacion,
           (case
             when ie.id_integrante is null and ie.n_empresa is not null then ie.cuit_empresa
             when ie.id_integrante is not null then I.nro_documento
             when Ie.Cuota_Compartida = 'S' then ''
           end) Identificacion_socio,
          IPJ.ENTIDAD_PERSJUR.FC_LISTAR_SOCIOS_COPROP (ie.id_entidad_socio, tpj.Id_Tramite_Ipj) N_COPROPIEDAD,
          p_Id_Tramite_Ipj IdTramiteIpj_Entidad, ie.id_tipo_socio, ts.n_tipo_socio,
          ie.persona_expuesta, i.cuil,
          (case
             when nvl(ie.fecha_fin, sysdate) >= trunc(sysdate) then 'S'
             else 'N'
           end ) Vigente
        from ipj.t_entidades_socios ie join ipj.t_tipos_integrante ti
            on ie.id_tipo_integrante = ti.id_tipo_integrante
          join ipj.t_Entidades tpj
            on ie.Id_Tramite_Ipj = tpj.Id_Tramite_Ipj and ie.id_legajo = tpj.id_legajo
          left join IPJ.t_tipos_entidades te
            on te.Id_Tipo_Entidad = ie.Id_Tipo_Entidad
          left join ipj.t_tipos_socio ts
            on ts.id_tipo_socio = ie.id_tipo_socio
          left join ipj.t_integrantes i
            on ie.id_integrante = i.id_integrante
          left join rcivil.vt_pk_persona p
           on i.id_sexo = p.ID_SEXO and
              i.nro_documento = p.NRO_DOCUMENTO and
              i.pai_cod_pais = p.PAI_COD_PAIS and
              i.id_numero = p.ID_NUMERO
        where
          tpj.id_legajo = p_id_legajo and
          tpj.matricula_version <= v_matricula_version and
          tpj.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_ubicacion_leg) and
          (ie.borrador = 'N' or ie.Id_Tramite_Ipj = p_Id_Tramite_Ipj) and
          (ie.fecha_fin is null or ie.fecha_fin >= to_date(sysdate, 'dd/mm/rrrr') or  ie.Id_Tramite_Ipj = p_Id_Tramite_Ipj)
        UNION ALL
        -- PERSONAS JURIDICAS
        select ie.id_entidad_socio, ie.id_tipo_integrante, ie.cuota, ie.id_legajo,
          to_char(ie.fecha_inicio, 'dd/mm/rrrr') fecha_inicio,
          to_char(ie.fecha_fin, 'dd/mm/rrrr') fecha_fin, ie.id_integrante, ie.Id_Tramite_Ipj,
          ie.id_legajo_socio, ie.cuit_empresa, ie.n_empresa, nvl(ie.cuota_compartida, 'N') cuota_compartida,
          ie.folio, ie.anio,
          ti.n_tipo_integrante,  trim(To_Char(nvl(ie.Porc_Capital, '0'), '9990.99'))Porc_Capital,
          nvl(ie.n_empresa, p.NOMBRE || ' ' ||p.APELLIDO)  detalle,
          ie.En_Formacion, ie.Matricula, to_char(ie.Fec_Acta, 'dd/mm/rrrr') Fec_Acta,-- desde aca
          ie.Id_Tipo_Entidad, ie.Certificacion_Contable, ie.Matricula_Certificante,
          ie.Id_Modo_Part, ie.Id_Integrante_Representante, ie.Id_Tipo_Repres,
          to_char(ie.Fecha_Repres, 'dd/mm/rrrr') Fecha_Repres,                              -- hasta aca
          TE.TIPO_ENTIDAD,
          I.ID_NUMERO, I.ID_SEXO, I.NRO_DOCUMENTO, I.PAI_COD_PAIS, ie.borrador,
          I.ERROR_DATO,
          (case
             when ie.id_integrante is null and ie.n_empresa is not null then
                ie.n_empresa
             when ie.id_integrante is not null then
                (p.NOMBRE || ' ' ||p.APELLIDO)
             when Ie.Cuota_Compartida = 'S' then 'Cuota Compartida'
           end) Persona,
          (case
             when ie.id_integrante is null and ie.n_empresa is not null then ie.cuit_empresa
             when ie.id_integrante is not null then I.CUIL
             when Ie.Cuota_Compartida = 'S' then ''
           end) Identificacion,
           (case
             when ie.id_integrante is null and ie.n_empresa is not null then ie.cuit_empresa
             when ie.id_integrante is not null then I.nro_documento
             when Ie.Cuota_Compartida = 'S' then ''
           end) Identificacion_Socio,
          IPJ.ENTIDAD_PERSJUR.FC_LISTAR_SOCIOS_COPROP (ie.id_entidad_socio, tpj.Id_Tramite_Ipj) N_COPROPIEDAD,
          p_Id_Tramite_Ipj IdTramiteIpj_Entidad, ie.id_tipo_socio, ts.n_tipo_socio,
          ie.persona_expuesta, i.cuil,
          (case
             when nvl(ie.fecha_fin, sysdate) >= trunc(sysdate) then 'S'
             else 'N'
           end ) Vigente
        from ipj.t_entidades_socios ie join ipj.t_tipos_integrante ti
            on ie.id_tipo_integrante = ti.id_tipo_integrante
          join ipj.t_Entidades tpj
            on ie.Id_Tramite_Ipj = tpj.Id_Tramite_Ipj and ie.id_legajo = tpj.id_legajo
          left join IPJ.t_tipos_entidades te
            on te.Id_Tipo_Entidad = ie.Id_Tipo_Entidad
          left join ipj.t_tipos_socio ts
            on ts.id_tipo_socio = ie.id_tipo_socio
          left join ipj.t_integrantes i
            on ie.id_integrante = i.id_integrante
          left join rcivil.vt_pk_persona p
            on i.id_sexo = p.ID_SEXO and
              i.nro_documento = p.NRO_DOCUMENTO and
              i.pai_cod_pais = p.PAI_COD_PAIS and
              i.id_numero = p.ID_NUMERO
          JOIN ipj.t_tramitesipj tra
            ON ie.id_tramite_ipj = tra.id_tramite_ipj
        where
          tpj.id_legajo = (SELECT max(e.id_legajo) FROM ipj.t_entidades e WHERE e.id_tramite_ipj = tra.id_tramite_ipj) and
          tpj.matricula_version <= v_matricula_version and
          tpj.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_ubicacion_leg) and
          tra.id_tramite_ipj_padre = p_Id_Tramite_Ipj and
          (ie.fecha_fin is null or ie.fecha_fin >= to_date(sysdate, 'dd/mm/rrrr') or  tra.id_tramite_ipj_padre = p_Id_Tramite_Ipj)
      ) tmp;

  END SP_Traer_PersJur_Int_Socios;


  PROCEDURE SP_Traer_PersJur_Rubros(
      p_Id_Tramite_Ipj in number,
      p_id_legajo in number,
      p_Cursor OUT TYPES.cursorType)
  IS
    v_matricula_version number(6,0);
  BEGIN
  /*****************************************************************
  Este procedimiento devuelve un listado de rubros y actividades asociados a una entidad de un tramite
  ******************************************************************/
    begin
      select nvl(matricula_version, 0) into v_matricula_version
      from IPJ.t_entidades
      where
        Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        id_legajo = p_id_legajo;
    exception
      when NO_DATA_FOUND then
        v_matricula_version := 0;
    end;

    OPEN p_Cursor FOR
    select r.id_tramite_ipj, r.id_legajo, to_char(r.fecha_desde, 'dd/mm/rrrr') fecha_desde, to_char(r.fecha_hasta, 'dd/mm/rrrr') fecha_hasta,
      r.id_rubro, act.n_rubro,  r.id_tipo_actividad, '' n_tipo_actividad,
      r.id_actividad, act.n_actividad, r.borrador, tact.n_tipo_actividad,
      ru.n_rubro
    from ipj.t_entidades_rubros r join  t_comunes.vt_actividades act
          on r.id_actividad = act.id_actividad and r.id_rubro = act.id_rubro and r.id_tipo_actividad = act.id_tipo_actividad
        join t_comunes.vt_tipos_actividad tact
          on tact.id_tipo_actividad = r.id_tipo_actividad and act.id_rubro = tact.id_rubro
        join t_comunes.vt_rubros ru
          on ru.id_rubro = r.id_rubro
        join ipj.t_entidades e
          on r.id_tramite_ipj = e.id_tramite_ipj and r.id_legajo = e.id_legajo
    where
      e.id_legajo = p_id_legajo and
      e.matricula_version <= v_matricula_version and
      (r.borrador = 'N' or r.Id_Tramite_Ipj = p_Id_Tramite_Ipj) and
      (r.fecha_hasta is null or r.fecha_hasta > sysdate or r.Id_Tramite_Ipj = p_Id_Tramite_Ipj)
    order by r.nro_orden asc, R.FECHA_DESDE;

  END SP_Traer_PersJur_Rubros;


  PROCEDURE SP_Traer_PersJur_Domicilio(
      p_Id_Tramite_Ipj in number,
      p_id_legajo in number,
      p_id_sede varchar2 := '00',
      p_Cursor OUT TYPES.cursorType)
  IS
    v_existe_domicilio number;
    v_cuit_empresa varchar2(20);
  BEGIN
    /********************************************************
      Este procemiento devuelve la direccion de la sede una entidad de un tramite
    *********************************************************/
    -- controlo si la entidad ya tiene una direccion
    begin
      select id_vin_real, cuit into v_existe_domicilio, v_cuit_empresa
      from IPJ.T_ENTIDADES
      where
      Id_Tramite_Ipj = p_Id_Tramite_Ipj and
      id_legajo = p_id_legajo;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN v_existe_domicilio := 0;
    end;

    ipj.varios.SP_Traer_Domicilio(
      p_id_vin => v_existe_domicilio,
      p_id_integrante => 0,
      p_id_legajo  => p_id_legajo,
      p_cuit_empresa => v_cuit_empresa,
      p_id_fondo_comercio => 0,
      p_id_entidad_acta => 0,
      p_es_comerciante => 'N',
      p_es_admin_entidad => 'N',
      p_Cursor => p_cursor
    );
  END SP_Traer_PersJur_Domicilio;


  PROCEDURE SP_TRAER_SEDES(
    p_cuil in varchar2,
    p_Cursor OUT TYPES.cursorType)
  IS
  BEGIN
  /********************************************************
  Lista la sedes correspondientes a una entidad
  *********************************************************/
    OPEN p_Cursor FOR
    select distinct id_sede, max(denominacion_2)
    from
      ( select '-1' id_sede, 'Nueva Sede' denominacion_2 , 1 Tipo
        from dual
        UNION
        select id_sede, n_sede denominacion_2, 2 tipo
        from t_comunes.vt_pers_juridicas pj
        where
          pj.cuit = p_cuil
        UNION
        select distinct id_sede, denominacion_2, 3 tipo
        from t_entidades
        where
          cuit = p_cuil and borrador = 'N'
      )
    group by id_sede;
  END SP_TRAER_SEDES;


  PROCEDURE SP_GUARDAR_ENTIDAD_ACTAS(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_entidad_acta out number,
    p_id_entidad_acta in number,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number,
    p_id_tipo_acta in number,
    p_fec_acta in varchar2,
    p_observacion in varchar2,
    p_id_vin in number,
    p_en_sede in number, --1= en la sede (graba id_vin en 0), 0 = domicilio particular
    p_art_reformados in varchar2,
    p_eliminar in number, -- 1 Elimina
    p_es_unanime in char,
    p_es_autoconvocada in char,
    p_es_en_sede in char,
    p_fecha_convocatoria in varchar2, -- Es fecha
    p_fecha_acta_directorio in varchar2, -- Es fecha
    p_fecha_libro_asamblea in varchar2, -- Es fecha
    p_publicado_boe in char,
    p_fecha_primera_boe in varchar2, -- Es fecha
    p_fecha_ultima_boe in varchar2, -- Es fecha
    p_publicado_diario in char,
    p_fecha_primera_diario in varchar2, -- Es fecha
    p_fecha_ultima_diario in varchar2, -- Es fecha
    p_porc_capital_social in varchar,
    p_quorum_convocatoria1 in varchar,
    p_quorum_convocatoria2 in varchar,
    p_fecha_cuarto_inter in varchar2, -- Es fecha
    p_hora_Acta in varchar2 -- Hora formato HH24:Mi
    )
    /*************************************************************
      Guarda las actas asociadas a una entidad de un tramite.
      No maneja transaccion, ya que se usa en cascada desde la aplicacion y lo administra el codigo.
  *************************************************************/
  IS
    v_existe number;
    v_valid_parametros varchar2(500);
    v_id_entidad_acta number;
    v_fec_acta date;
  BEGIN

    if IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('HAB_LOG_GESTION') = '1' then
      IPJ.VARIOS.SP_GUARDAR_LOG(
        p_METODO => 'sp_guardar_entidad_actas',
        p_NIVEL => 'Gestion',
        p_ORIGEN => 'DB',
        p_USR_LOG  => 'DB',
        p_USR_WINDOWS => 'Sistema',
        p_IP_PC  => null,
        p_EXCEPCION =>
           'Id Entidad Acta = ' || to_char(p_id_entidad_acta)
           || ' / Id Tramite Ipj = ' || to_char(p_Id_Tramite_Ipj)
           || ' / Id Legajo = ' || to_char(p_id_legajo)
           || ' / Id Tipo Acta = ' || to_char(p_id_tipo_acta)
           || ' / Fec Acta = ' || p_fec_acta
           || ' / Observacion = ' || p_observacion
           || ' / Id Vin = ' || to_char(p_id_vin)
           || ' / En Sede = ' || to_char(p_en_sede)
           || ' / Art Reformado = ' || p_art_reformados
           || ' / Eliminar = ' ||  to_char(p_eliminar)
           || ' / Es Unanume = ' || p_es_unanime
           || ' / Es Autoconvocada = ' || p_es_autoconvocada
           || ' / Es en Sede = ' || p_es_en_sede
           || ' / Fecha Convocatoria = ' || p_fecha_convocatoria
           || ' / Fecha Acta Dir = ' || p_fecha_acta_directorio
           || ' / Fecha Libro Asamb = ' || p_fecha_libro_asamblea
           || ' / Publicado Boe = ' || p_publicado_boe
           || ' / Fecha Primera Boe = ' || p_fecha_primera_boe
           || ' / Fecha Ultima Boe = ' || p_fecha_ultima_boe
           || ' / Publicado Diario = ' || p_publicado_diario
           || ' / Fecha Primera Diario = ' || p_fecha_primera_diario
           || ' / Fecha Ultima Diario = ' || p_fecha_ultima_diario
           || ' / Porc Capital Social = ' || p_porc_capital_social
           || ' / Quorum Convocat 1 = ' || p_quorum_convocatoria1
           || ' / Quorum Convoct 2 = ' || p_quorum_convocatoria2
           || ' / Fecha Cuarto Inter = ' || p_fecha_cuarto_inter
      );
    end if;

  -- VALIDACION DE PARAMETROS
    if IPJ.VARIOS.Valida_Fecha(p_fec_acta) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT');
    end if;
    if IPJ.VARIOS.Valida_Entidad(nvl(p_Id_Tramite_Ipj, 0), p_id_legajo) = 0 then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('ENT_NOT')|| '(' || to_char(p_Id_Tramite_Ipj) ||' - ' || to_char(p_id_legajo)||')';
    end if;
    if IPJ.VARIOS.Valida_Tipo_Acta(nvl(p_id_tipo_acta, 0)) = 0 then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('ACTA_NOT');
    end if;
    if p_porc_capital_social is not null and nvl(IPJ.VARIOS.ToNumber(p_porc_capital_social), -1) not between 0 and 100 then
      v_valid_parametros := v_valid_parametros || 'Porcentaje Inv�lido (Porc. Capital Social)';
    end if;
    if p_quorum_convocatoria1 is not null and nvl(IPJ.VARIOS.ToNumber(p_quorum_convocatoria1), -1) not between 0 and 100 then
      v_valid_parametros := v_valid_parametros || 'Porcentaje Inv�lido (Quorum Convocatoria 1)';
    end if;
    if p_quorum_convocatoria2 is not null and nvl(IPJ.VARIOS.ToNumber(p_quorum_convocatoria2), -1) not between 0 and 100 then
      v_valid_parametros := v_valid_parametros || 'Porcentaje Inv�lido (Quorum Convocatoria 2)';
    end if;


    if v_valid_parametros is not null then
      o_rdo := 'GUARDAR ACTAS - Parametros: '  || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      o_id_entidad_acta := 0;
      return;
    end if;

    -- CUERPO PROCEDIMIENTO
    if p_eliminar  = 1 then
      delete IPJ.T_Entidades_Acta_Orden
      where
        id_entidad_acta = p_id_entidad_acta;

      delete IPJ.T_ENTIDADES_ACTA_ARCH
      where
        id_entidad_acta = p_id_entidad_acta;

      delete IPJ.T_Entidades_Acta
      where
        id_entidad_acta = nvl(p_id_entidad_acta, 0) and
        borrador = 'S';


        o_id_entidad_acta := p_id_entidad_acta;
    else
      -- Armo la fecha de acta con la fecha y hora (si viene)
      if p_hora_acta is not null then
        v_fec_acta := to_date(p_fec_acta||' '||p_hora_acta,'dd/mm/yyyy HH24:Mi');
      else
         v_fec_acta := to_date(p_fec_acta, 'dd/mm/rrrr');
      end if;

      -- Si existe un registro, actualizo los datos
      if nvl(p_id_entidad_acta, 0) > 0 then
        update ipj.t_entidades_acta set
          fec_acta = v_fec_acta,
          observacion = p_observacion,
          id_vin = (case when p_en_sede = 1 then 0 else p_id_vin end),
          art_reformados = p_art_reformados,
          es_unanime = p_es_unanime,
          es_autoconvocada = p_es_autoconvocada,
          es_en_sede = p_es_en_sede,
          fecha_convocatoria = to_date(p_fecha_convocatoria, 'dd/mm/rrrr'),
          fecha_acta_directorio = to_date(p_fecha_acta_directorio, 'dd/mm/rrrr'),
          fecha_libro_asamblea = to_date(p_fecha_libro_asamblea, 'dd/mm/rrrr'),
          publicado_boe = p_publicado_boe,
          fecha_primera_boe = to_date(p_fecha_primera_boe, 'dd/mm/rrrr'),
          fecha_ultima_boe = to_date(p_fecha_ultima_boe, 'dd/mm/rrrr'),
          publicado_diario = p_publicado_diario,
          fecha_primera_diario = to_date(p_fecha_primera_diario, 'dd/mm/rrrr'),
          fecha_ultima_diario = to_date(p_fecha_ultima_diario, 'dd/mm/rrrr'),
          porc_capital_social = IPJ.VARIOS.ToNumber(p_porc_capital_social) ,
          quorum_convocatoria1 = IPJ.VARIOS.ToNumber(p_quorum_convocatoria1),
          quorum_convocatoria2 = IPJ.VARIOS.ToNumber(p_quorum_convocatoria2),
          fecha_cuarto_inter = to_date(p_fecha_cuarto_inter, 'dd/mm/rrrr')
        where
          id_entidad_acta = p_id_entidad_acta and
          borrador = 'S';

          o_id_entidad_acta := p_id_entidad_acta;
      else
        SELECT IPJ.SEQ_T_ENTIDADES_ACTA.nextval INTO v_id_entidad_acta FROM dual;

        insert into ipj.t_entidades_acta
          (id_entidad_acta, Id_Tramite_Ipj, id_tipo_acta, fec_acta, observacion, borrador, id_legajo,
          id_vin, art_reformados, es_unanime, es_autoconvocada, es_en_sede,
          fecha_convocatoria, fecha_acta_directorio, fecha_libro_asamblea, publicado_boe,
          fecha_primera_boe, fecha_ultima_boe, publicado_diario, fecha_primera_diario,
          fecha_ultima_diario, porc_capital_social, quorum_convocatoria1, quorum_convocatoria2,
          fecha_cuarto_inter)
        values (
          v_id_entidad_acta, p_Id_Tramite_Ipj, p_id_tipo_acta, v_fec_acta,
          p_observacion, 'S', p_id_legajo, (case when p_en_sede = 1 then 0 else p_id_vin end), p_art_reformados,
          p_es_unanime, p_es_autoconvocada, p_es_en_sede, to_date(p_fecha_convocatoria, 'dd/mm/rrrr'),
          to_date(p_fecha_acta_directorio, 'dd/mm/rrrr'), to_date(p_fecha_libro_asamblea, 'dd/mm/rrrr'),
          p_publicado_boe, to_date(p_fecha_primera_boe, 'dd/mm/rrrr'),
          to_date(p_fecha_ultima_boe, 'dd/mm/rrrr'), p_publicado_diario,
          to_date(p_fecha_primera_diario, 'dd/mm/rrrr'), to_date(p_fecha_ultima_diario, 'dd/mm/rrrr'),
          IPJ.VARIOS.ToNumber(p_porc_capital_social), IPJ.VARIOS.ToNumber(p_quorum_convocatoria1),
          IPJ.VARIOS.ToNumber(p_quorum_convocatoria2), to_date(p_fecha_cuarto_inter, 'dd/mm/rrrr')
         );

        o_id_entidad_acta := v_id_entidad_acta;
      end if;
    end if;

    o_rdo := 'OK';
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     WHEN OTHERS THEN
       o_rdo := 'sp_guardar_entidad_actas: ' || To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
       o_id_entidad_acta := 0;
  END SP_GUARDAR_ENTIDAD_ACTAS;


  PROCEDURE SP_GUARDAR_ENTIDAD_INS_LEGAL(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_entidad_ins_legal out number,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number,
    p_id_juzgado in number,
    p_il_numero in number,
    p_il_fecha in varchar2,
    p_il_tipo in number,
    p_titulo in varchar2,
    p_descripcion in varchar2,
    p_fecha_acto in varchar2,
    p_eliminar in number, -- 1 elimina
    p_id_entidad_ins_legal in number
  )
  IS
    v_existe number;
    v_id_juzgado number;
    v_valid_parametros varchar2(500);
    v_il_tipo NUMBER;
  BEGIN
    /*************************************************************
      Guarda los instrumentos legales asociados a una entidad de un tramite.
      No maneja transaccion, ya que se usa en cascada desde la aplicacion y lo administra el codigo.
  *************************************************************/
  -- VALIDACION DE PARAMETROS
    /*if IPJ.VARIOS.Valida_Fecha(p_il_fecha) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT');
    end if;
    if p_fecha_acto is not null and IPJ.VARIOS.Valida_Fecha(p_fecha_acto) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT');
    end if;*/
    if IPJ.VARIOS.Valida_Entidad(nvl(p_Id_Tramite_Ipj, 0), p_id_legajo) = 0 then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('ENT_NOT')|| '(' || to_char(p_Id_Tramite_Ipj) ||' - ' || to_char(p_id_legajo)||')';
    end if;
    if IPJ.VARIOS.Valida_Tipo_Ins_Legal(nvl(p_il_tipo, 0)) = 0 then
      --v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('INS_LEGAL_NOT');
      v_il_tipo := 4;
    ELSE
      v_il_tipo := p_il_tipo;
    end if;

    if v_valid_parametros is not null then
      o_rdo := 'GUARDAR INS LEG - Parametros: '  || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    -- CUERPO PROCEDIMIENTO
    if p_eliminar = 1 then
      delete ipj.t_entidades_ins_legal
      where
        Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        id_legajo = p_id_legajo and
        id_entidad_ins_legal = p_id_entidad_ins_legal and
        borrador = 'S';

    else
      v_id_juzgado := (case when p_id_juzgado = 0 then null else p_id_juzgado end);

      -- Si existe un registro igual no hago nada
      update ipj.t_entidades_ins_legal
      set
        titulo = p_titulo,
        descripcion = p_descripcion ,
        fecha = to_date(p_fecha_acto, 'dd/mm/rrrr'),
        id_juzgado = v_id_juzgado,
        il_numero = p_il_numero,
        il_tipo = v_il_tipo
      where
        id_legajo = p_id_legajo and
        id_entidad_ins_legal = p_id_entidad_ins_legal;

      o_id_entidad_ins_legal := p_id_entidad_ins_legal;

      if sql%rowcount = 0 then
        SELECT IPJ.SEQ_ENTIDAD_INS_LEGAL.nextval INTO o_id_entidad_ins_legal FROM dual;

        insert into ipj.t_entidades_ins_legal
          (Id_Tramite_Ipj, id_legajo, id_juzgado, il_numero, il_fecha, il_tipo, borrador,
          titulo, descripcion, fecha, id_entidad_ins_legal)
        values
          (p_Id_Tramite_Ipj, p_id_legajo, v_id_juzgado, p_il_numero,
           to_date(p_il_fecha, 'dd/mm/rrrr'), v_il_tipo, 'S', p_titulo, p_descripcion,
           to_date(p_fecha_acto, 'dd/mm/rrrr'), o_id_entidad_ins_legal);
      end if;
    end if;

    o_rdo := 'OK';
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     WHEN OTHERS THEN
       o_rdo := 'Sp_Guardar_Entidad_Ins_Legal : ' || To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_GUARDAR_ENTIDAD_INS_LEGAL;

  PROCEDURE SP_GUARDAR_ENTIDADES(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number,
    p_cuit in varchar2,
    p_desc_obj_social in clob,
    p_monto in varchar2,
    p_acta_constitutiva in varchar2, -- Es Fecha
    p_vigencia in number,
    p_id_moneda in varchar2,
    p_cierre_ejercicio in varchar2,
    p_id_unidad_med in varchar2,
    p_fec_vig in varchar2,  -- Es Fecha
    p_id_vin_comercial in number,
    p_id_vin_real in number,
    p_matricula in varchar2,
    p_fecha_inscripcion in varchar2, -- Es Fecha
    p_folio in varchar2,
    p_libro_nro in number,
    p_tomo in varchar2,
    p_anio in number,
    p_borrador in varchar2,
    p_denominacion_1 in varchar2,
    p_n_Sede in varchar2,
    p_id_sede in varchar2,
    p_alta_temporal in varchar2, -- Es Fecha
    p_tipo_vigencia in number,
    p_valor_cuota in varchar2,
    p_cuotas in number,
    p_matricula_version in number,
    p_id_tipo_admin in number,
    p_id_tipo_entidad in number,
    p_baja_temporal in varchar2, -- Es Fecha
    p_id_estado_entidad in varchar2,
    p_obs_cierre in varchar2,
    p_Nro_Resolucion in varchar2,
    p_Fec_Resolucion in varchar2, -- Es Fecha
    p_Nro_Boletin in Number,
    p_Fec_Boletin in varchar2, -- Es Fecha
    p_Nro_Registro in varchar2,
    p_Sede_Estatuto in Varchar2,
    p_Requiere_Sindico in Varchar2,
    p_Origen in Number,
    p_Es_Sucursal in Number,
    p_Observacion in Varchar2,
    p_letra in varchar2,
    p_Id_Tipo_Origen in number,
    p_cierre_fin_mes in char,
    p_fiscalizacion_ejerc in char,
    p_incluida_lgs in char,
    p_condicion_fideic in varchar2,
    p_obs_fiduciario in varchar2,
    p_obs_fiduciante in varchar2,
    p_obs_beneficiario in varchar2,
    p_obs_fideicomisario in varchar2,
    p_fecha_versionado in varchar2, -- Es Fecha
    p_sin_denominacion in char,
    p_contrato_privado in char,
    p_cant_Fiduciario in number,
    p_cant_Fiduciante in number,
    p_cant_Beneficiario in number,
    p_cant_Fideicomisario in number,
    p_fec_vig_hasta in VARCHAR2, -- Es Fecha
    p_acredita_grupo IN VARCHAR2,
    p_fec_estatuto in varchar2,
    p_gubernamental in char
  )
  IS
  /********************************************************
  Guarda los datos de una entidad relacionada a un tramite.
  Si la entidad ya existe, los actualiza, en caso contrario los ingresa
  No maneja transaccion, ya que se usa en cascada desde la aplicacion y lo administra el codigo.
  *********************************************************/
    v_mensaje varchar2(2000);
    v_valid_parametros varchar2(2000);
    v_matricula_exists varchar2(2000);
    v_cierre_ejercicio varchar2(10);
    v_id_tramiteipj_accion number;
    v_id_tipo_admin number;
    v_id_vin_real number;
    v_id_vin_comercial number;
    v_Id_Tipo_Origen number;
    v_cuit varchar2(20);
    v_id_tipo_entidad_leg number(5);
    v_tramite_padre ipj.t_tramitesipj.id_tramite_ipj_padre%TYPE;
    v_Row_Entidades_Old IPJ.t_Entidades%ROWTYPE;
    v_Row_Entidades_Padre IPJ.t_Entidades%ROWTYPE;
    v_cierre_fin_mes ipj.t_entidades.cierre_fin_mes%TYPE;
    v_Hay_Libros_Dig number;
  BEGIN

    if IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('HAB_LOG_GESTION') = '1' then
      IPJ.VARIOS.SP_GUARDAR_LOG(
        p_METODO => 'SP_GUARDAR_ENTIDADES',
        p_NIVEL => 'Parametros',
        p_ORIGEN => 'DB',
        p_USR_LOG  => 'DB',
        p_USR_WINDOWS => 'Sistema',
        p_IP_PC  => null,
        p_EXCEPCION =>
          'Id Tramite IPJ = ' || to_char(p_Id_Tramite_Ipj)
          || ' / Id Legajo = ' || to_char(p_id_legajo)
          || ' / Cuit = ' || p_cuit
          --|| ' / Obj Social = ' || p_desc_obj_social
          || ' / Monto = ' || p_monto
          || ' / Acta Const = ' || p_acta_constitutiva
          || ' / Vigencia = ' || to_char(p_vigencia )
          || ' / Id Moneda = ' || p_id_moneda
          || ' / Cierre Ejerc = ' || p_cierre_ejercicio
          || ' / Id Unid Med = ' || p_id_unidad_med
          || ' / Fec Vigencia = ' || p_fec_vig
          || ' / Id Vin Comercial = ' || to_char(p_id_vin_comercial)
          || ' / Id Vin Real = ' || to_char(p_id_vin_real)
          || ' / Matricula = ' || p_matricula
          || ' / Fec Inscripcion = ' || p_fecha_inscripcion
          || ' / Folio = ' || p_folio
          || ' / Libro = ' || to_char(p_libro_nro )
          || ' / Tomo = ' || p_tomo
          || ' / A�o = ' || to_char(p_anio )
          || ' / Borrador = ' || p_borrador
          || ' / Denominacion = ' || p_denominacion_1
          || ' / N Sede = ' || p_n_Sede
          || ' / Id Sede = ' || p_id_sede
          || ' / Alta Temp = ' || p_alta_temporal
          || ' / Tipo Vigencia = ' || to_char(p_tipo_vigencia )
          || ' / Valor Cuota = ' || p_valor_cuota
          || ' / Cuotas = ' || to_char(p_cuotas )
          || ' / Matricula Version = ' || to_char(p_matricula_version)
          || ' / Id Tipo Admin = ' || to_char(p_id_tipo_admin)
          || ' / Id Tipo Entidad = ' || to_char(p_id_tipo_entidad )
          || ' / Baja Temp = ' ||p_baja_temporal
          || ' / Id Estado Ent = ' || p_id_estado_entidad
          || ' / Obs Cierre = ' || p_obs_cierre
          || ' / Nro Resolucion = ' || p_Nro_Resolucion
          || ' / Fec Resolucion = ' || p_Fec_Resolucion
          || ' / Nro Boletin = ' || p_Nro_Boletin
          || ' / Fec Boletin = ' || p_Fec_Boletin
          || ' / Nro Registro = ' || p_Nro_Registro
          || ' / Sede Estatuto = ' || p_Sede_Estatuto
          || ' / Requiere Sindico = ' || p_Requiere_Sindico
          || ' / Origen = ' || to_char(p_Origen)
          || ' / Es Suc = ' || to_char(p_Es_Sucursal)
          || ' / Observacion = ' || p_Observacion
          || ' / Letra = ' || p_letra
          || ' / Id Tipo Origen = ' || to_char(p_Id_Tipo_Origen)
          || ' / Fin de Mes = ' || p_cierre_fin_mes
          || ' / Fiscaliz Ejerc = ' || p_fiscalizacion_ejerc
          || ' / Incluida LGS = ' || p_incluida_LGS
          || ' / Condic Fideic = ' || p_condicion_fideic
          || ' / Obs Fiduciario = ' || p_obs_fiduciario
          || ' / Obs Fiduciante = ' || p_obs_fiduciante
          || ' / Obs Benef = ' || p_obs_beneficiario
          || ' / Obs Fideicom = ' || p_obs_fideicomisario
          || ' / Fecha Vers = ' || p_fecha_versionado
          || ' / Sin Denominacion = ' || p_sin_denominacion
          || ' / Contrato Privado = ' || p_contrato_privado
          || ' / Cant Fiduciario = ' || to_char(p_cant_Fiduciario)
          || ' / Cant Fiduciante = ' || to_char(p_cant_Fiduciante)
          || ' / Cant Benef = ' || to_char(p_cant_Beneficiario)
          || ' / Cant Fiduciante = ' || to_char(p_cant_Fideicomisario)
          || ' / Fec Vigencia Hasta = ' || p_fec_vig_hasta
          || ' / Acredita Grupo = ' || p_acredita_grupo
          || ' / Fec Estatuto = ' || p_fec_estatuto
          || ' / Gubernamental = ' ||  p_gubernamental
      );
    end if;

    -- Traigo el valor antes que se actualice el registro
    DBMS_OUTPUT.PUT_LINE('1- SP_GUARDAR_ENTIDADES - Busca datos de Entidad paa VALIDACIONES');
    SELECT e.*
      INTO v_Row_Entidades_Old
      FROM ipj.t_entidades e
     WHERE e.id_tramite_ipj = p_Id_Tramite_Ipj
       AND e.id_legajo = p_id_legajo;

    --  VALIDACION DE PARAMETROS
    if p_cuit is not null then
      v_cuit := replace(p_cuit, '-', '');
      t_comunes.pack_persona_juridica.VerificarCUIT (v_cuit, v_mensaje);
      if Upper(trim(v_mensaje)) <> 'OK' then --La validacion de CUIT responde siempre CUIL
        v_valid_parametros := v_valid_parametros || replace(v_mensaje, 'CUIL', 'CUIT') || '. ';
      end if;
    else
      v_cuit := null;
    end if;
    if p_cierre_ejercicio is not null then
      -- SI la fecha vene sin dias, le pongo 01
      if length(p_cierre_ejercicio) <10 then
        v_cierre_ejercicio := p_cierre_ejercicio || '/' ||  to_Char(sysdate, 'rrrr');
        if to_date(v_cierre_ejercicio, 'dd/mm/rrrr') > sysdate then
          v_cierre_ejercicio := p_cierre_ejercicio || '/' ||  to_char(to_Char(sysdate, 'rrrr')-1);
        end if;
      else
        v_cierre_ejercicio := p_cierre_ejercicio;
      end if;
      if IPJ.VARIOS.Valida_Fecha(v_cierre_ejercicio) = 'N' or To_Date(v_cierre_ejercicio, 'dd/mm/rrrr') > sysdate then
        v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT') || ' (Cierre Ejercicio)';
      end if;
    end if;
    if p_fecha_inscripcion is not null and IPJ.VARIOS.Valida_Fecha(p_fecha_inscripcion) = 'N' or To_Date(p_fecha_inscripcion, 'dd/mm/rrrr') > sysdate then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT')  || ' (Fecha Inscripcio)';
    end if;
    if p_fec_vig is not null and IPJ.VARIOS.Valida_Fecha(p_fec_vig) = 'N'  then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT') || ' (Fecha Vigencia)';
    end if;
    if p_fec_vig_hasta is not null and IPJ.VARIOS.Valida_Fecha(p_fec_vig_hasta) = 'N'  then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT') || ' (Fecha Vigencia Hasta)';
    end if;
    if p_acta_constitutiva is not null and IPJ.VARIOS.Valida_Fecha(p_acta_constitutiva) = 'N' or To_Date(p_acta_constitutiva, 'dd/mm/rrrr') > sysdate then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT')  || ' (Acta Constitutiva)';
    end if;
    if p_fecha_versionado is not null and IPJ.VARIOS.Valida_Fecha(p_fecha_versionado) = 'N' or To_Date(p_fecha_versionado, 'dd/mm/rrrr') > sysdate then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT')  || ' (Fecha Versionado)';
    end if;
    if p_alta_temporal is not null and IPJ.VARIOS.Valida_Fecha(p_alta_temporal) = 'N' or To_Date(p_alta_temporal, 'dd/mm/rrrr') > sysdate then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT') || ' (Fecha Alta)';
    end if;
    if p_Fec_Resolucion is not null and IPJ.VARIOS.Valida_Fecha(p_Fec_Resolucion) = 'N' or To_Date(p_Fec_Resolucion, 'dd/mm/rrrr') > sysdate then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT') || ' (Fecha Resoluci�n)';
    end if;
    if p_Fec_Boletin is not null and IPJ.VARIOS.Valida_Fecha(p_Fec_Boletin) = 'N' or To_Date(p_Fec_Boletin, 'dd/mm/rrrr') > sysdate then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT') || ' (Fecha Bolet�n)';
    end if;
    if p_Fec_Estatuto is not null and IPJ.VARIOS.Valida_Fecha(p_Fec_Estatuto) = 'N' or To_Date(p_Fec_Boletin, 'dd/mm/rrrr') > sysdate then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT') || ' (Fecha Estatuto)';
    end if;
    if p_monto is not null and nvl(IPJ.VARIOS.ToNumber(p_monto), -1) < 0 then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('MONTO_NOT');
    end if;
    if p_valor_cuota is not null and nvl(IPJ.VARIOS.ToNumber(p_valor_cuota), -1) < 0 then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('MONTO_NOT');
    end if;
    if IPJ.VARIOS.TONUMBER(p_libro_nro) is null then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('LIBRO_NOT');
    end if;
    if IPJ.VARIOS.TONUMBER(p_anio) is null then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('A�O_NOT');
    end if;
    if IPJ.VARIOS.TONUMBER(p_vigencia) is null then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('VIGEN_NOT');
    end if;
    if IPJ.VARIOS.TONUMBER(p_cuotas) is null then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('CUOTA_NOT');
    end if;
     if p_matricula is not null and IPJ.VARIOS.FV_Valida_Entero(p_matricula) = 0 then
      v_valid_parametros := v_valid_parametros || ' La Matr�cula solo acepta n�meros.';
    end if;
    if nvl(p_id_legajo, 0) = 0 then
       v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('ENT_NOT');
    end if;

    -- Si los paramentros no estan OK, no continuo
    DBMS_OUTPUT.PUT_LINE('2- SP_GUARDAR_ENTIDADES - Finaliza Validaciones con resoltado = ' || v_valid_parametros);

    if v_valid_parametros is not null then
      o_rdo := 'GUARDAR ENTIDAD - Parametros : ' || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    -- Valido que la matricula sea vallida y no duplicada
    if p_matricula is not null and p_letra is not null then
      DBMS_OUTPUT.PUT_LINE('3- SP_GUARDAR_ENTIDADES - Verifica la Matr�cula');
      IPJ.VARIOS.SP_Validar_Matricula(
        o_rdo => v_matricula_exists,
        o_tipo_mensaje => o_tipo_mensaje,
        p_Id_Tramite_Ipj => p_Id_Tramite_Ipj,
        p_id_legajo => p_id_legajo,
        p_id_tramiteipj_accion => null,
        p_cuit => v_cuit,
        p_matricula => Upper(p_letra) || p_matricula);

      DBMS_OUTPUT.PUT_LINE('          Resultado = ' || v_matricula_exists);
       if v_matricula_exists != IPJ.TYPES.C_RESP_OK then
         o_rdo := 'GUARDAR ENTIDAD - Parametros : ' || v_matricula_exists;
         o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
         return;
       end if;
    end if;

    --  CUERPO DEL PROCEDIMIETNO
    -- Los valores en 0 los pongo NULL para no violar las FK.
    v_id_tipo_admin := (case when p_id_tipo_admin = 0 then null else p_id_tipo_admin end);
    v_id_vin_real := (case when p_id_vin_real = 0 then null else p_id_vin_real end);
    v_id_vin_comercial := (case when p_id_vin_comercial = 0 then null else p_id_vin_comercial end);
    v_id_tipo_origen := (case when p_id_tipo_origen = 0 then null else p_id_tipo_origen end);

    -- Actualizo el registro (si existe)
    DBMS_OUTPUT.PUT_LINE('4- SP_GUARDAR_ENTIDADES - Actualiza la entidad con los valores');
    update ipj.t_entidades
    set
      id_sede = p_id_sede,
      cuit = v_cuit,
      id_moneda  = p_id_moneda ,
      id_unidad_med = p_id_unidad_med,
      matricula = (case when p_letra is null or p_matricula is null then null else upper(p_letra) || p_matricula end),
      fec_inscripcion = To_date(p_fecha_inscripcion, 'dd/mm/rrrr'),
      folio = p_folio,
      libro_nro = p_libro_nro,
      tomo = p_tomo,
      anio  = p_anio ,
      objeto_social = p_desc_obj_social,
      monto = IPJ.VARIOS.ToNumber(p_monto),
      acta_contitutiva = p_acta_constitutiva,
      vigencia = p_vigencia,
      cierre_ejercicio = (case when nvl(p_cierre_fin_mes, 'N') = 'S' then null else To_Date(v_cierre_ejercicio, 'dd/mm/rrrr') end),
      fec_vig = To_Date(p_fec_vig, 'dd/mm/rrrr'),
      id_vin_comercial = v_id_vin_comercial,
      id_vin_real = v_id_vin_real,
      borrador  = p_borrador ,
      fec_acto = to_date(p_acta_constitutiva, 'dd/mm/rrrr'),
      denominacion_1 = p_denominacion_1,
      denominacion_2 = p_n_Sede,
      alta_temporal = To_Date(p_alta_temporal, 'dd/mm/rrrr'),
      tipo_vigencia = p_tipo_vigencia,
      valor_cuota = IPJ.VARIOS.ToNumber(p_valor_cuota),
      matricula_version = p_matricula_version,
      cuotas = p_cuotas,
      id_tipo_administracion = v_id_tipo_admin,
      id_tipo_entidad = p_id_tipo_entidad,
      baja_temporal = To_Date(p_baja_temporal, 'dd/mm/rrrr'),
      id_estado_entidad = p_id_estado_entidad,
      obs_cierre = p_obs_cierre,
      Nro_Resolucion = p_Nro_Resolucion,
      Fec_Resolucion = to_date(p_Fec_Resolucion, 'dd/mm/rrrr'),
      Nro_Boletin = p_Nro_Boletin,
      Fec_Boletin= to_date(p_Fec_Boletin, 'dd/mm/rrrr'),
      Nro_Registro = p_Nro_Registro,
      Sede_Estatuto = p_Sede_Estatuto,
      Requiere_Sindico = p_Requiere_Sindico,
      Origen = p_Origen,
      Es_Sucursal = p_Es_Sucursal,
      Observacion = p_Observacion,
      Id_Tipo_Origen = v_Id_Tipo_Origen,
      cierre_fin_mes = p_cierre_fin_mes,
      fiscalizacion_ejerc = p_fiscalizacion_ejerc,
      incluida_lgs = p_incluida_lgs,
      condicion_fideic = p_condicion_fideic,
      obs_fiduciario = p_obs_fiduciario,
      obs_fiduciante = p_obs_fiduciante,
      obs_beneficiario = p_obs_beneficiario,
      obs_fideicomisario = p_obs_fideicomisario,
      fecha_versionado = to_date(p_fecha_versionado, 'dd/mm/rrrr'),
      sin_denominacion = p_sin_denominacion,
      contrato_privado = p_contrato_privado,
      cant_Fiduciario = p_cant_Fiduciario,
      cant_Fiduciante = p_cant_Fiduciante,
      cant_Beneficiario = p_cant_Beneficiario,
      cant_Fideicomisario = p_cant_Fideicomisario,
      fec_vig_hasta = to_date(p_fec_vig_hasta, 'dd/mm/rrrr'),
      acredita_grupo = p_acredita_grupo,
      fec_estatuto = to_date(p_Fec_Estatuto, 'dd/mm/rrrr'),
      gubernamental = p_gubernamental
    where
      Id_Tramite_Ipj = p_Id_Tramite_Ipj and
      id_legajo = p_id_legajo;

    -- Si no se actualizo nada, lo inserto.
    -- NO DEBERIA OCURRIR, VER SI INSERTO O MUESTRO ERROR
    if sql%rowcount = 0 then
      DBMS_OUTPUT.PUT_LINE('5- SP_GUARDAR_ENTIDADES - No actualizo nada, INSERTA los valores');
      insert into ipj.t_entidades
        (Id_Tramite_Ipj, cuit, objeto_social, monto, acta_contitutiva, vigencia, id_moneda,
         cierre_ejercicio, id_unidad_med, fec_vig, id_vin_comercial, id_vin_real,
         matricula, fec_inscripcion, folio, libro_nro, tomo, anio, borrador,
         denominacion_1, denominacion_2, id_sede, alta_temporal, tipo_vigencia,
         valor_cuota, fec_acto, cuotas, MATRICULA_VERSION,
         id_tipo_administracion, id_tipo_entidad, baja_temporal, id_estado_entidad, obs_cierre,
         id_legajo, Nro_Resolucion, Fec_Resolucion, Nro_Boletin, Fec_Boletin, Nro_Registro,
         Sede_Estatuto, Requiere_Sindico, Origen, Es_Sucursal, Observacion, Id_Tipo_Origen,
         cierre_fin_mes, fiscalizacion_ejerc, incluida_lgs, condicion_fideic, obs_fiduciario,
         obs_fiduciante, obs_beneficiario, obs_fideicomisario, fecha_versionado,
         sin_denominacion, contrato_privado, cant_Fiduciario, cant_Fiduciante,
         cant_Beneficiario, cant_Fideicomisario, fec_vig_hasta, acredita_grupo,
         fec_estatuto, gubernamental)
      values
        (p_Id_Tramite_Ipj, v_cuit, p_desc_obj_social, IPJ.VARIOS.ToNumber(p_monto), p_acta_constitutiva, p_vigencia, p_id_moneda,
         (case when nvl(p_cierre_fin_mes, 'N') = 'S' then null else To_Date(v_cierre_ejercicio, 'dd/mm/rrrr') end),
         p_id_unidad_med, To_Date(p_fec_vig, 'dd/mm/rrrr'),
         v_id_vin_comercial, v_id_vin_real, upper(p_letra) || p_matricula, To_Date(p_fecha_inscripcion, 'dd/mm/rrrr'),
         p_folio, p_libro_nro, p_tomo, p_anio, 'S',
         p_denominacion_1, p_n_Sede, p_id_sede, To_Date(p_alta_temporal, 'dd/mm/rrrr'), p_tipo_vigencia,
         IPJ.VARIOS.ToNumber(p_valor_cuota), to_date(p_acta_constitutiva, 'dd/mm/rrrr'), p_cuotas, p_matricula_version,
         v_id_tipo_admin, p_id_tipo_entidad, To_Date(p_baja_temporal, 'dd/mm/rrrr'), p_id_estado_entidad, p_obs_cierre,
         p_id_legajo, p_Nro_Resolucion, to_date(p_Fec_Resolucion, 'dd/mm/rrrr'), p_Nro_Boletin,
         to_date(p_Fec_Boletin, 'dd/mm/rrrr'), p_Nro_Registro,
         p_Sede_Estatuto, p_Requiere_Sindico, p_Origen, p_Es_Sucursal, p_Observacion, v_Id_Tipo_Origen,
         p_cierre_fin_mes, p_fiscalizacion_ejerc, p_incluida_lgs, p_condicion_fideic, p_obs_fiduciario,
         p_obs_fiduciante, p_obs_beneficiario, p_obs_fideicomisario, to_date(p_fecha_versionado, 'dd/mm/rrrr'),
         p_sin_denominacion, p_contrato_privado, p_cant_Fiduciario, p_cant_Fiduciante,
         p_cant_Beneficiario, p_cant_Fideicomisario, to_date(p_fec_vig_hasta, 'dd/mm/rrrr'),
         p_acredita_grupo, to_date(p_Fec_Estatuto, 'dd/mm/rrrr'), p_gubernamental);
    end if;

    -- Si el legajo no tiene tipo de entidad, lo actualiza
    DBMS_OUTPUT.PUT_LINE('6- SP_GUARDAR_ENTIDADES - Actualiza el tipo de entidad en Legajo');
    select id_tipo_entidad into v_id_tipo_entidad_leg
    from ipj.t_legajos l
    where
      l.id_legajo = p_id_legajo;

    if nvl(v_id_tipo_entidad_leg, 0) = 0 then
      update ipj.t_legajos
      set id_tipo_entidad = p_id_tipo_entidad
      where
        id_legajo = p_id_legajo;
    end if;

    -- Busca los datos para actualizar el protocolo asociado
    begin
      select max(id_tramiteipj_accion) into v_id_tramiteipj_accion
      from IPJ.T_TramitesIPJ_Acciones t join IPJ.T_TIPOS_ACCIONESIPJ ta
          on T.ID_TIPO_ACCION = TA.ID_TIPO_ACCION
        left join IPJ.T_CONFIG_PROTOCOLO cp
          on CP.ID_PROTOCOLO = TA.ID_PROTOCOLO
      where
        Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        id_legajo = p_id_legajo and
        TA.ID_PROTOCOLO between 1 and 7 and
        CP.SIGLA =  upper(p_letra);
    exception
      when NO_DATA_FOUND then
        v_id_tramiteipj_accion := 0;
    end;

    DBMS_OUTPUT.PUT_LINE('7- SP_GUARDAR_ENTIDADES - Actualiza los protocolos');
    IPJ.VARIOS.SP_Actualizar_Protocolo(
      o_rdo => o_rdo,
      o_tipo_mensaje => o_tipo_mensaje,
      p_Id_Tramite_Ipj => p_Id_Tramite_Ipj,
      p_id_legajo => p_id_legajo,
      p_id_tramiteipj_accion => v_id_tramiteipj_accion ,
      p_matricula => upper(p_letra) || p_matricula);

    DBMS_OUTPUT.PUT_LINE('           Resultado = ' || o_rdo);
    if o_rdo != IPJ.TYPES.C_RESP_OK then
      o_rdo := 'SP_Actualizar_Protocolo ' || o_rdo;
    end if;

    -- Busco si la entidad tiene libros digitales cargados
    DBMS_OUTPUT.PUT_LINE('8- SP_GUARDAR_ENTIDADES - Verifica Libros Digitales y Avisos');
    SP_Validar_Libros_Dig (
      o_rdo => v_Hay_Libros_Dig,
      p_id_legajo => p_id_legajo );

    -- Si tiene libros digitales, y se pasa a estado CANCELADA, se marca para envio de mail
    if v_Hay_Libros_Dig > 0 and p_id_estado_entidad = 'B' then
      update IPJ.t_tramitesipj tr
      set tr.enviar_mail = 1
      where
        tr.id_tramite_ipj = p_Id_Tramite_Ipj;
    end if;

    --Si es un tr�mite hijo, verifico qu� dato se actualiz� y lo copio en el padre - pv
    BEGIN
      SELECT e.*
        INTO v_Row_Entidades_Padre
        FROM ipj.t_entidades e
        JOIN ipj.t_tramitesipj t ON e.id_tramite_ipj = t.id_tramite_ipj_padre
       WHERE t.id_tramite_ipj = p_Id_Tramite_Ipj;

      v_cierre_fin_mes := (case when nvl(p_cierre_fin_mes, 'N') = 'S' then null else p_cierre_fin_mes end);

      --Llamo a la actualizaci�n del tr�mite Padre - PV
      DBMS_OUTPUT.PUT_LINE('9- SP_GUARDAR_ENTIDADES - Actualiza los Anexos');
      SP_Act_Entidad_Anexados(o_rdo,
          o_tipo_mensaje,
          (case when p_id_sede = v_Row_Entidades_Old.Id_Sede then v_Row_Entidades_Padre.Id_Sede else p_id_sede end),--
          (case when v_cuit = v_Row_Entidades_Old.Cuit then v_Row_Entidades_Padre.Cuit else v_cuit end),
          (case when p_id_moneda = v_Row_Entidades_Old.Id_Moneda then v_Row_Entidades_Padre.Id_Moneda else p_id_moneda end),
          (case when p_id_unidad_med = v_Row_Entidades_Old.Id_Unidad_Med then v_Row_Entidades_Padre.Id_Unidad_Med else p_id_unidad_med end),
          (case when p_letra is null or p_matricula is null then null else upper(p_letra) || p_matricula end),
          (case when To_date(p_fecha_inscripcion, 'dd/mm/rrrr') = to_date(v_Row_Entidades_Old.Fec_Inscripcion, 'dd/mm/rrrr') then to_date(v_Row_Entidades_Padre.Fec_Inscripcion, 'dd/mm/rrrr') else To_date(p_fecha_inscripcion, 'dd/mm/rrrr') end),
          (case when p_folio = v_Row_Entidades_Old.Folio then v_Row_Entidades_Padre.Folio else p_folio end),
          (case when p_libro_nro = v_Row_Entidades_Old.Libro_Nro then v_Row_Entidades_Padre.Libro_Nro else p_libro_nro end),
          (case when p_tomo = v_Row_Entidades_Old.Tomo then v_Row_Entidades_Padre.Tomo else p_tomo end),
          (case when p_anio = v_Row_Entidades_Old.Anio then v_Row_Entidades_Padre.Anio else p_anio end),
          (case when p_desc_obj_social = v_Row_Entidades_Old.Objeto_Social then v_Row_Entidades_Padre.Objeto_Social else p_desc_obj_social end),
          (case when IPJ.VARIOS.ToNumber(p_monto) = v_Row_Entidades_Old.Monto then v_Row_Entidades_Padre.Monto else IPJ.VARIOS.ToNumber(p_monto) end),
          (case when p_acta_constitutiva = v_Row_Entidades_Old.Acta_Contitutiva then v_Row_Entidades_Padre.Acta_Contitutiva else p_acta_constitutiva end),--acta_constitutiva
          (case when p_vigencia = v_Row_Entidades_Old.Vigencia then v_Row_Entidades_Padre.Vigencia else p_vigencia end),
          (case when v_cierre_fin_mes = v_Row_Entidades_Old.Cierre_Fin_Mes then v_Row_Entidades_Padre.Cierre_Fin_Mes else v_cierre_fin_mes end),
          (case when To_Date(p_fec_vig, 'dd/mm/rrrr') = to_date(v_Row_Entidades_Old.Fec_Vig, 'dd/mm/rrrr') then to_date(v_Row_Entidades_Padre.Fec_Vig, 'dd/mm/rrrr') else To_Date(p_fec_vig, 'dd/mm/rrrr') end),
          (case when v_id_vin_comercial = v_Row_Entidades_Old.Id_Vin_Comercial then v_Row_Entidades_Padre.Id_Vin_Comercial else v_id_vin_comercial end),
          (case when v_id_vin_real = v_Row_Entidades_Old.Id_Vin_Real then v_Row_Entidades_Padre.Id_Vin_Real else v_id_vin_real end),
          (case when p_borrador = v_Row_Entidades_Old.Borrador then v_Row_Entidades_Padre.Borrador else p_borrador end),
          (case when to_date(p_acta_constitutiva, 'dd/mm/rrrr') = to_date(v_Row_Entidades_Old.Fec_Acto, 'dd/mm/rrrr') then to_date(v_Row_Entidades_Padre.Fec_Acto, 'dd/mm/rrrr') else to_date(p_acta_constitutiva, 'dd/mm/rrrr') end),--fecha_acto
          (case when p_denominacion_1 = v_Row_Entidades_Old.Denominacion_1 then v_Row_Entidades_Padre.Denominacion_1 else p_denominacion_1 end),
          (case when p_n_Sede = v_Row_Entidades_Old.Denominacion_2 then v_Row_Entidades_Padre.Denominacion_2 else p_n_Sede end),
          (case when To_Date(p_alta_temporal, 'dd/mm/rrrr') = to_date(v_Row_Entidades_Old.Alta_Temporal, 'dd/mm/rrrr') then to_date(v_Row_Entidades_Padre.Alta_Temporal, 'dd/mm/rrrr') else To_Date(p_alta_temporal, 'dd/mm/rrrr') end),
          (case when p_tipo_vigencia = v_Row_Entidades_Old.Tipo_Vigencia then v_Row_Entidades_Padre.Tipo_Vigencia else p_tipo_vigencia end),
          (case when IPJ.VARIOS.ToNumber(p_valor_cuota) = v_Row_Entidades_Old.Valor_Cuota then v_Row_Entidades_Padre.Valor_Cuota else IPJ.VARIOS.ToNumber(p_valor_cuota) end),
          (case when p_matricula_version = v_Row_Entidades_Old.Matricula_Version then v_Row_Entidades_Padre.Matricula_Version else p_matricula_version end),
          (case when p_cuotas = v_Row_Entidades_Old.Cuotas then v_Row_Entidades_Padre.Cuotas else p_cuotas end),
          (case when v_id_tipo_admin = v_Row_Entidades_Old.Id_Tipo_Administracion then v_Row_Entidades_Padre.Id_Tipo_Administracion else v_id_tipo_admin end),
          (case when p_id_tipo_entidad = v_Row_Entidades_Old.Id_Tipo_Entidad then v_Row_Entidades_Padre.Id_Tipo_Entidad else p_id_tipo_entidad end),
          (case when To_Date(p_baja_temporal, 'dd/mm/rrrr') = to_date(v_Row_Entidades_Old.Baja_Temporal, 'dd/mm/rrrr') then to_date(v_Row_Entidades_Padre.Baja_Temporal, 'dd/mm/rrrr') else To_Date(p_baja_temporal, 'dd/mm/rrrr') end),
          (case when p_id_estado_entidad = v_Row_Entidades_Old.Id_Estado_Entidad then v_Row_Entidades_Padre.Id_Estado_Entidad else p_id_estado_entidad end),
          (case when p_obs_cierre = v_Row_Entidades_Old.Obs_Cierre then v_Row_Entidades_Padre.Obs_Cierre else p_obs_cierre end),
          (case when p_Nro_Resolucion = v_Row_Entidades_Old.Nro_Resolucion then v_Row_Entidades_Padre.Nro_Resolucion else p_Nro_Resolucion end),
          (case when to_date(p_Fec_Resolucion, 'dd/mm/rrrr') = to_date(v_Row_Entidades_Old.Fec_Resolucion, 'dd/mm/rrrr') then to_date(v_Row_Entidades_Padre.Fec_Resolucion, 'dd/mm/rrrr') else to_date(p_Fec_Resolucion, 'dd/mm/rrrr') end),
          (case when p_Nro_Boletin = v_Row_Entidades_Old.Nro_Boletin then v_Row_Entidades_Padre.Nro_Boletin else p_Nro_Boletin end),
          (case when to_date(p_Fec_Boletin, 'dd/mm/rrrr') = to_date(v_Row_Entidades_Old.Fec_Boletin, 'dd/mm/rrrr') then to_date(v_Row_Entidades_Padre.Fec_Boletin, 'dd/mm/rrrr') else to_date(p_Fec_Boletin, 'dd/mm/rrrr') end),
          (case when p_Nro_Registro = v_Row_Entidades_Old.Nro_Registro then v_Row_Entidades_Padre.Nro_Registro else p_Nro_Registro end),
          (case when p_Sede_Estatuto = v_Row_Entidades_Old.Sede_Estatuto then v_Row_Entidades_Padre.Sede_Estatuto else p_Sede_Estatuto end),
          (case when p_Requiere_Sindico = v_Row_Entidades_Old.Requiere_Sindico then v_Row_Entidades_Padre.Requiere_Sindico else p_Requiere_Sindico end),
          (case when p_Origen = v_Row_Entidades_Old.Origen then v_Row_Entidades_Padre.Origen else p_Origen end),
          (case when p_Es_Sucursal = v_Row_Entidades_Old.Es_Sucursal then v_Row_Entidades_Padre.Es_Sucursal else p_Es_Sucursal end),
          (case when p_Observacion = v_Row_Entidades_Old.Observacion then v_Row_Entidades_Padre.Observacion else v_Row_Entidades_Padre.Observacion||'-'||p_Observacion end),
          (case when v_Id_Tipo_Origen = v_Row_Entidades_Old.Id_Tipo_Origen then v_Row_Entidades_Padre.Id_Tipo_Origen else v_Id_Tipo_Origen end),
          (case when to_date(v_cierre_ejercicio, 'dd/mm/rrrr') = to_date(v_Row_Entidades_Old.Cierre_Ejercicio, 'dd/mm/rrrr') then to_date(v_Row_Entidades_Padre.Cierre_Ejercicio, 'dd/mm/rrrr') else to_date(v_cierre_ejercicio, 'dd/mm/rrrr') end),
          (case when p_fiscalizacion_ejerc = v_Row_Entidades_Old.Fiscalizacion_Ejerc then v_Row_Entidades_Padre.Fiscalizacion_Ejerc else p_fiscalizacion_ejerc end),
          (case when p_incluida_lgs = v_Row_Entidades_Old.Incluida_Lgs then v_Row_Entidades_Padre.Incluida_Lgs else p_incluida_lgs end),
          (case when p_condicion_fideic = v_Row_Entidades_Old.Condicion_Fideic then v_Row_Entidades_Padre.Condicion_Fideic else p_condicion_fideic end),
          (case when p_obs_fiduciario = v_Row_Entidades_Old.Obs_Fiduciario then v_Row_Entidades_Padre.Obs_Fiduciario else p_obs_fiduciario end),
          (case when p_obs_fiduciante = v_Row_Entidades_Old.Obs_Fiduciante then v_Row_Entidades_Padre.Obs_Fiduciante else p_obs_fiduciante end),
          (case when p_obs_beneficiario = v_Row_Entidades_Old.Obs_Beneficiario then v_Row_Entidades_Padre.Obs_Beneficiario else p_obs_beneficiario end),
          (case when p_obs_fideicomisario = v_Row_Entidades_Old.Obs_Fideicomisario then v_Row_Entidades_Padre.Obs_Fideicomisario else p_obs_fideicomisario end),
          (case when to_date(p_fecha_versionado, 'dd/mm/rrrr') = to_date(v_Row_Entidades_Old.Fecha_Versionado, 'dd/mm/rrrr') then to_date(v_Row_Entidades_Padre.Fecha_Versionado, 'dd/mm/rrrr') else to_date(p_fecha_versionado, 'dd/mm/rrrr') end),
          (case when p_sin_denominacion = v_Row_Entidades_Old.Sin_Denominacion then v_Row_Entidades_Padre.Sin_Denominacion else p_sin_denominacion end),
          (case when p_contrato_privado = v_Row_Entidades_Old.Contrato_Privado then v_Row_Entidades_Padre.Contrato_Privado else p_contrato_privado end),
          (case when p_cant_Fiduciario = v_Row_Entidades_Old.Cant_Fiduciario then v_Row_Entidades_Padre.Cant_Fiduciario else p_cant_Fiduciario end),
          (case when p_cant_Fiduciante = v_Row_Entidades_Old.Cant_Fiduciante then v_Row_Entidades_Padre.Cant_Fiduciante else p_cant_Fiduciante end),
          (case when p_cant_Beneficiario = v_Row_Entidades_Old.Cant_Beneficiario then v_Row_Entidades_Padre.Cant_Beneficiario else p_cant_Beneficiario end),
          (case when p_cant_Fideicomisario = v_Row_Entidades_Old.Cant_Fideicomisario then v_Row_Entidades_Padre.Cant_Fideicomisario else p_cant_Fideicomisario end),
          (case when to_date(p_fec_vig_hasta, 'dd/mm/rrrr') = to_date(v_Row_Entidades_Old.Fec_Vig_Hasta, 'dd/mm/rrrr') then to_date(v_Row_Entidades_Padre.Fec_Vig_Hasta, 'dd/mm/rrrr') else to_date(p_fec_vig_hasta, 'dd/mm/rrrr') end),
          v_Row_Entidades_Padre.Id_Tramite_Ipj,
          p_id_legajo,
          (case when p_acredita_grupo = v_Row_Entidades_Old.Acredita_Grupo then v_Row_Entidades_Padre.Acredita_Grupo else p_acredita_grupo end)
      );

      DBMS_OUTPUT.PUT_LINE('           Resultado = ' || o_rdo);
    EXCEPTION
      WHEN no_data_found THEN
        v_tramite_padre := NULL;
    END;

    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     WHEN OTHERS THEN
       o_rdo := '- SP_GUARDAR_ENTIDADES: ' || To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_GUARDAR_ENTIDADES;


  PROCEDURE SP_GUARDAR_ENTIDAD_SOCIOS(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_entidad_socio out number,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number,
    p_id_tipo_integrante in number,
    p_cuota in number,
    p_fecha_inicio in varchar2, -- es fecha
    p_fecha_fin in varchar2,  -- es fecha
    p_id_integrante in number,
    p_id_sexo in varchar2,
    p_nro_documento in varchar2,
    p_pai_cod_pais in varchar2,
    p_id_numero in number,
    p_cuil in varchar2,
    p_detalle in varchar2,
    p_nombre in varchar2,
    p_apellido in varchar2,
    p_error_dato in varchar2,
    p_eliminar in number, -- 1 Elimina
    p_n_tipo_documento in varchar2,
    p_id_entidad_socio in number,
    p_id_legajo_socio in number,
    p_cuit_empresa in varchar2,
    p_n_empresa in varchar2,
    p_cuota_compartida in varchar2,
    p_Id_Tramite_Ipj_entidad in number,
    p_porc_capital in varchar2,
    p_id_tipo_socio in number,
    p_en_formacion in char,
    p_matricula in varchar2,
    p_fec_acta in varchar2, --es fecha
    p_id_tipo_entidad in number,
    p_certificacion_contable in char,
    p_matricula_certificante in varchar2,
    p_folio in varchar2,
    p_anio in varchar2,
    p_persona_expuesta in char)
  IS
    v_valid_parametros varchar2(200);
    v_id_integrante number;
    v_bloque varchar2(100);
    v_tipo_mensaje number;
    v_id_legajo_socio number;
    v_id_tipo_socio number;
    v_id_tipo_entidad number;
    v_cuotas number;
    v_id_ubicacion number;
  BEGIN
  /*****************************************************
    Guarda los integrantes asociados a una entidad de un tramite.
    Si existe los actualizo, sino los agrego.
    No maneja transaccion, ya que se usa en cascada desde la aplicacion y lo administra el codigo.
  ******************************************************/

    if IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('HAB_LOG_GESTION') = '1' then
      IPJ.VARIOS.SP_GUARDAR_LOG(
        p_METODO => 'SP_GUARDAR_ENTIDAD_SOCIOS',
        p_NIVEL => 'Gesti�n',
        p_ORIGEN => 'DB',
        p_USR_LOG  => 'DB',
        p_USR_WINDOWS => 'Sistema',
        p_IP_PC  => null,
        p_EXCEPCION =>
          ' Tramite IPJ = ' || to_char(p_Id_Tramite_Ipj)
          || ' / Legajo = ' || to_char(p_id_legajo)
          || ' / Tipo Integr = ' || to_char(p_id_tipo_integrante)
          || ' / Fecha Ini = ' || p_fecha_inicio
          || ' / Fecha Fin = ' || p_fecha_fin
          || ' / Id Integrante = ' || to_char(p_id_integrante)
          || ' / Id Sexo = ' || p_id_sexo
          || ' / DNI = ' || p_nro_documento
          || ' / Pai Cod Pais = ' || p_pai_cod_pais
          || ' / Id Numero = ' || to_char(p_id_numero)
          || ' / Cuil = ' || p_cuil
          || ' / Detalle = ' || p_detalle
          || ' / Nombre = ' || p_nombre
          || ' / Apellido = ' || p_apellido
          || ' / Error Dato = ' || p_error_dato
          || ' / Eliminar = ' || to_char(p_eliminar)
          || ' / Tipo Documento = ' || p_n_tipo_documento
          || ' / Id Entidad Socio= ' || to_char(p_id_entidad_socio)
          || ' / Id Legajo Socio = ' || to_char(p_id_legajo_socio)
          || ' / Cuit Empresa = ' || p_cuit_empresa
          || ' / N_Empresa = ' || p_n_empresa
          || ' / Cuota compartida = ' || to_char(p_cuota_compartida)
          || ' / ID Tramite entidad = ' || to_char(p_Id_Tramite_Ipj_entidad)
          || ' / Porc capital = ' || p_porc_capital
          || ' / Id tipo socio = ' || to_char(p_id_tipo_socio)
          || ' / En formacion = ' || p_en_formacion
          || ' / Matricula = ' || p_matricula
          || ' / Fec Acta = ' || p_fec_acta
          || ' / Id Tipo Entidad = ' || to_char(p_Id_Tipo_Entidad)
          || ' / Certif contable = ' || p_certificacion_contable
          || ' / Matricula certif = ' || p_matricula_certificante
          || ' / Folio = ' || p_folio
          || ' / A�o = ' || to_char(p_anio)
          || ' / Persona Expuesta = ' || p_persona_expuesta
      );
    end if;


    -- VALIDACION DE PARAMETROS
    v_bloque := 'GUARDAR SOCIOS - Parametros : ';
    if IPJ.VARIOS.Valida_Entidad(nvl(p_Id_Tramite_Ipj, 0), p_id_legajo) = 0 then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('ENT_NOT')|| '(' || to_char(p_Id_Tramite_Ipj) ||' - ' || to_char(p_id_legajo)||')';
    end if;
    if p_eliminar <> 1 and IPJ.VARIOS.Valida_Tipo_Integrante(p_id_tipo_integrante) = 0 then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('TINT_NOT');
    end if;
    if p_eliminar <> 1 and p_fecha_inicio is not null and IPJ.VARIOS.Valida_Fecha(p_fecha_inicio) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT');
    end if;
    if p_eliminar <> 1 and p_fecha_fin is not null and IPJ.VARIOS.Valida_Fecha(p_fecha_fin) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT');
    end if;
    if p_eliminar <> 1 and p_fec_acta is not null and IPJ.VARIOS.Valida_Fecha(p_fec_acta) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT');
    end if;
    if p_eliminar <> 1 and p_porc_capital is not null and nvl(IPJ.VARIOS.ToNumber(p_porc_capital), -1) < 0 then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('MONTO_NOT');
    end if;

    -- Si los parametros no estan OK, no continuo
    if v_valid_parametros is not null then
      o_rdo := v_bloque || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    -- CUERPO DEL PROCEDIMEINTO
    if p_eliminar = 1 then
      delete ipj.t_entidades_socios_usuf
      where
        id_entidad_socio = p_id_entidad_socio;

      delete ipj.t_entidades_socios_copro
      where
        id_entidad_socio = p_id_entidad_socio;

      delete ipj.t_entidades_socios_acciones
      where
        id_entidad_socio = p_id_entidad_socio;

      delete IPJ.T_Entidades_Socio_Capital
      where
        id_entidad_socio = p_id_entidad_socio;

      delete ipj.t_entidades_socios_hist
      where
        id_entidad_socio = p_id_entidad_socio and
        Id_Tramite_Ipj = p_Id_Tramite_Ipj;

      delete ipj.t_entidades_socios
      where
        id_entidad_socio = p_id_entidad_socio and
        borrador = 'S';

      o_id_entidad_socio := 0;
    else
       -- Si no viene una Empresa y no es compartida, es una persona
       if p_n_empresa is null and nvl(p_cuota_compartida, 'N') = 'N' then
         -- Si el integrante no existe, lo agrego y luego continuo
         v_id_integrante := p_id_integrante;
         if IPJ.VARIOS.Valida_Integrante(p_id_integrante) = 0 then
           IPJ.ENTIDAD_PERSJUR.SP_GUARDAR_INTEGRANTES(
             o_rdo => v_valid_parametros,
             o_tipo_mensaje => v_tipo_mensaje,
             o_id_integrante => v_id_integrante,
             p_id_sexo => p_id_sexo,
             p_nro_documento => p_nro_documento,
             p_pai_cod_pais => p_pai_cod_pais,
             p_id_numero => p_id_numero,
             p_cuil => p_cuil,
             p_detalle => p_detalle,
             p_Nombre => p_Nombre,
             p_Apellido => p_Apellido,
             p_error_dato => p_error_dato,
             p_n_tipo_documento => p_n_tipo_documento );

           if v_valid_parametros <> TYPES.c_Resp_OK then
              o_rdo := IPJ.VARIOS.MENSAJE_ERROR('INT_NOT') || '. ' || v_valid_parametros;
              o_tipo_mensaje := v_tipo_mensaje;
              return;
           end if;
         else
           -- si esta marcado como dato erroneo, cambio la descripcion y lo marco.
           if nvl(p_error_dato, 'N') = 'S' then
             update ipj.t_integrantes
             set
               error_dato = p_error_dato,
               detalle = p_detalle
             where
               id_integrante = p_id_integrante;
           end if;
           v_id_integrante := p_id_integrante;
         end if;
       end if;

      v_bloque := 'Integrantes - Socios ';
      -- Si el Legajo o el Integrante vienen en 0, pongo NULL para respetar las FK
      v_id_legajo_socio := (case when p_id_legajo_socio = 0 then null else p_id_legajo_socio end);
      v_id_integrante := (case when v_id_integrante = 0 then null else v_id_integrante end);
      v_id_tipo_socio := (case when p_id_tipo_socio = 0 then null else p_id_tipo_socio end);
      v_id_tipo_entidad := (case when p_id_tipo_entidad = 0 then null else p_id_tipo_entidad end);

      -- Para SxA, se calcula la cantidad de acciones, sin contar las dadas de baja.
      select id_ubicacion_origen into v_id_ubicacion
      from ipj.t_tramitesipj
      where
        id_tramite_ipj = p_id_tramite_ipj;

      if v_id_ubicacion = IPJ.TYPES.C_AREA_SXA then
        select nvl(sum(cantidad), 0) into v_cuotas
        from ipj.t_entidades_socios_acciones
        where
          id_legajo = p_id_legajo and
          id_entidad_socio = p_id_entidad_socio and
          id_entidades_accion in
            (select id_entidades_accion from ipj.t_entidades_acciones where id_legajo = p_id_legajo and nvl(fecha_baja, sysdate) >= to_date(sysdate, 'dd/mm/rrrr'));
      else
        v_cuotas := p_cuota;
      end if;

      -- Si se asigna fecha de baja, se la asigno a los usufructos y copropietarios
      if p_fecha_fin is not null then
        update ipj.t_entidades_socios_usuf
        set fec_baja = to_date(p_fecha_fin, 'dd/mm/rrrr')
        where
          id_entidad_socio = p_id_entidad_socio and
          fec_baja is null;

        update ipj.t_entidades_socios_copro
        set
          fec_baja = to_date(p_fecha_fin, 'dd/mm/rrrr')
        where
          id_entidad_socio = p_id_entidad_socio and
          fec_baja is null;
      end if;

      -- Actualizo el Socio
      update ipj.t_entidades_socios e
      set
        cuota = v_cuotas,
        porc_capital = IPJ.VARIOS.ToNumber(p_porc_capital),
        fecha_fin = to_date(p_fecha_fin, 'dd/mm/rrrr'),
        id_tipo_socio = v_id_tipo_socio,
        id_tipo_entidad = v_id_tipo_entidad,
        certificacion_contable = p_certificacion_contable,
        matricula_certificante = p_matricula_certificante,
        fec_acta = to_date(p_fec_acta, 'dd/mm/rrrr'),
        matricula = p_matricula,
        en_formacion = p_en_formacion,
        folio = p_folio,
        anio = p_anio,
        persona_expuesta = p_persona_expuesta
      where
        id_entidad_socio = p_id_entidad_socio;

       -- si no existe, lo agrego
      if sql%rowcount = 0 then
        SELECT IPJ.SEQ_ENTIDAD_SOCIOS.nextval INTO o_id_entidad_socio FROM dual;

        insert into ipj.t_entidades_socios
          ( id_tipo_integrante, cuota, Id_Tramite_Ipj, fecha_inicio, fecha_fin, id_integrante,
            id_legajo, borrador, id_entidad_socio, id_legajo_socio, cuit_empresa,
            n_empresa, cuota_compartida, porc_capital, id_tipo_socio, matricula,
            fec_acta, id_tipo_entidad, certificacion_contable, matricula_certificante,
            en_formacion, folio, anio, persona_expuesta)
        values
          (p_id_tipo_integrante, v_cuotas, p_Id_Tramite_Ipj,
           to_date(p_fecha_inicio, 'dd/mm/rrrr'),  to_date(p_fecha_fin, 'dd/mm/rrrr'), v_id_integrante, p_id_legajo, 'S',
           o_id_entidad_socio, v_id_legajo_socio, p_cuit_empresa, p_n_empresa, nvl(p_cuota_compartida, 'N'),
           IPJ.VARIOS.ToNumber(p_porc_capital), v_id_tipo_socio, p_matricula,
            to_date(p_fec_acta, 'dd/mm/rrrr'), v_id_tipo_entidad, p_certificacion_contable,
            p_matricula_certificante, p_en_formacion, p_folio, p_anio, p_persona_expuesta);

      else
        o_id_entidad_socio := p_id_entidad_socio;
      end if;

      -- cargo el historial
      IPJ.ENTIDAD_PERSJUR.SP_GUARDAR_ENTIDAD_SOCIOS_HIST(
        o_rdo  => o_rdo,
        o_tipo_mensaje => o_tipo_mensaje,
        p_Id_Tramite_Ipj => p_Id_Tramite_Ipj_entidad,
        p_id_legajo => p_id_legajo,
        p_id_tipo_integrante => p_id_tipo_integrante,
        p_cuota => v_cuotas,
        p_fecha_inicio => p_fecha_inicio,
        p_fecha_fin => p_fecha_fin,
        p_id_integrante => v_id_integrante,
        p_id_entidad_socio => o_id_entidad_socio,
        p_id_legajo_socio => v_id_legajo_socio,
        p_cuit_empresa => p_cuit_empresa,
        p_n_empresa => p_n_empresa,
        p_cuota_compartida => p_cuota_compartida,
        p_porc_capital => p_porc_capital);

        if o_rdo <> TYPES.c_Resp_OK then
          o_rdo := 'SOCIOS HIST: ' || o_rdo;
          return;
        end if;
    end if;

    o_rdo := TYPES.c_Resp_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
    WHEN OTHERS THEN
       o_rdo := v_bloque || To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_GUARDAR_ENTIDAD_SOCIOS;

  PROCEDURE SP_GUARDAR_ENTIDAD_ADMIN(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_integrante out number,
    o_id_admin out number,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number,
    p_id_tipo_integrante in number,
    p_fecha_inicio in varchar2,-- Es Fecha
    p_fecha_fin in varchar2, -- Es Fecha
    p_id_integrante in number,
    p_id_sexo in varchar2,
    p_nro_documento in varchar2,
    p_pai_cod_pais in varchar2,
    p_id_numero in number,
    p_cuil in varchar2,
    p_detalle in varchar2,
    p_nombre in varchar2,
    p_apellido in varchar2,
    p_Id_Vin in number,
    p_Id_Tipo_Organismo in number,
    p_error_dato in varchar2,
    p_eliminar in number,  -- 1 Elimina
    p_n_tipo_documento in varchar2,
    p_Id_Tramite_Ipj_entidad in number,
    p_id_entidades_accion in number,
    p_id_motivo_baja in number,
    p_porc_garantia in varchar2,
    p_monto_garantia in varchar2,
    p_habilitado_present in char,
    p_persona_expuesta in char,
    p_id_moneda in varchar2,
    p_id_admin in number,
    p_cuit_empresa in varchar2,
    p_n_empresa in varchar2,
    p_matricula in varchar2,
    p_fec_acta in varchar2, -- es fecha
    p_id_legajo_empresa in number,
    p_folio in varchar2,
    p_anio in number,
    p_Id_Tipo_Entidad in NUMBER,
    p_es_admin_afip in varchar2
    )
  IS
    v_valid_parametros varchar2(200);
    v_id_integrante number;
    v_bloque varchar2(100);
    v_tipo_mensaje number;
    v_Id_Tipo_Organismo number;
    v_id_motivo_baja number;
    v_id_entidades_Accion number;
    v_duplicado number;
    v_expediente varchar2(50);
    v_sticker varchar2(50);
    v_tramite_ipj_dup number;
    v_n_estado varchar2(100);
  BEGIN
  /*****************************************************
    Guarda los integrantes asociados a una entidad de un tramite.
    Si existe los actualizo, sino los agrego.
    No maneja transaccion, ya que se usa en cascada desde la aplicacion y lo administra el codigo.
  ******************************************************/
    if IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('HAB_LOG_GESTION') = '1' then
      IPJ.VARIOS.SP_GUARDAR_LOG(
        p_METODO => 'SP_GUARDAR_ENTIDAD_ADMIN',
        p_NIVEL => 'Gesti�n',
        p_ORIGEN => 'DB',
        p_USR_LOG  => 'DB',
        p_USR_WINDOWS => 'Sistema',
        p_IP_PC  => null,
        p_EXCEPCION =>
          ' Tramite IPJ = ' || to_char(p_Id_Tramite_Ipj)
          || ' / Legajo = ' || to_char(p_id_legajo)
          || ' / Tipo Integr = ' || to_char(p_id_tipo_integrante)
          || ' / Fecha Ini = ' || p_fecha_inicio
          || ' / Fecha Fin = ' || p_fecha_fin
          || ' / Id Integrante = ' || to_char(p_id_integrante)
          || ' / Id Sexo = ' || p_id_sexo
          || ' / DNI = ' || p_nro_documento
          || ' / Pai Cod Pais = ' || p_pai_cod_pais
          || ' / Id Numero = ' || to_char(p_id_numero)
          || ' / Cuil = ' || p_cuil
          || ' / Detalle = ' || p_detalle
          || ' / Nombre = ' || p_nombre
          || ' / Apellido = ' || p_apellido
          || ' / Id Vin = ' || to_char(p_Id_Vin)
          || ' / Id Tipo Organismo = ' || to_char(p_Id_Tipo_Organismo)
          || ' / Error Dato = ' || p_error_dato
          || ' / Eliminar = ' || to_char(p_eliminar)
          || ' / Tipo Documento = ' || p_n_tipo_documento
          || ' / Id Tram. IPj Entidad = ' || to_char(p_Id_Tramite_Ipj_entidad)
          || ' / Id Entidad Accion = ' || to_char(p_id_entidades_accion)
          || ' / Id Motivo Baja = ' || to_char(p_id_motivo_baja)
          || ' / Porc Garantia = ' || p_porc_garantia
          || ' / Monto Garantia = ' || p_monto_garantia
          || ' / Habilitado Present = ' || p_habilitado_present
          || ' / Persona Expuesta = ' || p_persona_expuesta
          || ' / Id Moneda = ' || p_id_moneda
          || ' / Id Admin = ' || to_char(p_id_admin)
          || ' / Cuit Empresa = ' || p_cuit_empresa
          || ' / N_Empresa = ' || p_n_empresa
          || ' / Matricula = ' || p_matricula
          || ' / Fec Acta = ' || p_fec_acta
          || ' / Id Legajo Emp = ' || to_char(p_id_legajo_empresa)
          || ' / Folio = ' || p_folio
          || ' / A�o = ' || to_char(p_anio)
          || ' / Id Tipo Entidad = ' || to_char(p_Id_Tipo_Entidad)
      );
    end if;

    -- VALIDACION DE PARAMETROS
    v_bloque := 'GUARDAR ADMIN - Parametros : ';
    if IPJ.VARIOS.Valida_Entidad(nvl(p_Id_Tramite_Ipj, 0), p_id_legajo) = 0 then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('ENT_NOT')|| '(' || to_char(p_Id_Tramite_Ipj) ||' - ' || to_char(p_id_legajo)||')';
    end if;
    if IPJ.VARIOS.Valida_Tipo_Integrante(p_id_tipo_integrante) = 0 then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('TINT_NOT');
    end if;
    if p_fecha_inicio is not null and IPJ.VARIOS.Valida_Fecha(p_fecha_inicio) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT');
    end if;
    if p_fecha_fin is not null and IPJ.VARIOS.Valida_Fecha(p_fecha_fin) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT');
    end if;
    if p_fec_acta is not null and IPJ.VARIOS.Valida_Fecha(p_fec_acta) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT');
    end if;
    if p_monto_garantia is not null and nvl(IPJ.VARIOS.ToNumber(p_monto_garantia), -1) < 0 then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('MONTO_NOT') || ' (Monto Garant�a)';
    end if;
    if p_porc_garantia is not null and nvl(IPJ.VARIOS.ToNumber(p_porc_garantia), -1) < 0 then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('MONTO_NOT') || ' (Porc. Garant�a)';
    end if;

    -- Si los parametros no estan OK, no continuo
    if v_valid_parametros is not null then
      o_rdo := v_bloque || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    -- Si no es una modificaci�n, controlo que no existe cargado
    select count(1) into v_duplicado
    from ipj.t_entidades_admin
    where
      id_legajo = p_id_legajo and
      id_tipo_integrante = p_id_tipo_integrante and
      nvl(id_integrante, 0) = p_id_integrante and
      cuit_empresa = p_cuit_empresa and
      nvl(id_legajo_empresa, 0) = p_id_legajo_empresa and
      fecha_inicio = to_date(p_fecha_inicio, 'dd/mm/rrrr');

    if (nvl(p_id_admin, 0) = 0 and v_duplicado > 0) or  v_duplicado > 1 then
      select tr.id_tramite_ipj, expediente, sticker, e.n_estado
        into v_tramite_ipj_dup, v_expediente, v_sticker, v_n_estado
      from ipj.t_tramitesipj tr join ipj.t_entidades_admin ad
          on tr.id_tramite_ipj = ad.id_tramite_ipj
        join ipj.t_estados e on
          e.id_estado = tr.id_estado_ult
      where
        id_legajo = p_id_legajo and
        id_tipo_integrante = p_id_tipo_integrante and
        nvl(id_integrante, 0) = p_id_integrante and
        cuit_empresa = p_cuit_empresa and
        nvl(id_legajo_empresa, 0) = p_id_legajo_empresa and
        ad.fecha_inicio = to_date(p_fecha_inicio, 'dd/mm/rrrr') and
        rownum = 1;

      o_rdo := 'El administrador se encuentra cargado en el tr�mite ' || nvl(nvl(v_expediente, v_sticker), to_char(v_tramite_ipj_dup)) || ' (estado ' || v_n_estado || '), revise el historial.';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      return;
    end if;

    -- CUERPO DEL PROCEDIMEINTO
    if p_eliminar = 1 then
      delete ipj.t_entidades_admin_hist
      where
        Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        id_legajo = p_id_legajo and
        id_admin = p_id_admin;

      delete ipj.t_entidades_admin
      where
        Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        id_admin = p_id_admin and
        borrador = 'S';

      o_id_integrante := 0;
    else
      if p_n_empresa is null then
        -- Si el integrante no existe, lo agrego y luego continuo
        if IPJ.VARIOS.Valida_Integrante(p_id_integrante) = 0 then
          v_bloque := 'GUARDAR ADMIN - Guardar Integrante: ';
          IPJ.ENTIDAD_PERSJUR.SP_GUARDAR_INTEGRANTES(
            o_rdo => v_valid_parametros,
            o_tipo_mensaje => v_tipo_mensaje,
            o_id_integrante => v_id_integrante,
            p_id_sexo => p_id_sexo,
            p_nro_documento => p_nro_documento,
            p_pai_cod_pais => p_pai_cod_pais,
            p_id_numero => p_id_numero,
            p_cuil => p_cuil,
            p_detalle => p_detalle,
            p_Nombre => p_nombre,
            p_Apellido => p_apellido,
            p_error_dato => p_error_dato,
            p_n_tipo_documento => p_n_tipo_documento);

          if v_valid_parametros <> TYPES.c_Resp_OK then
            o_rdo := v_bloque || v_valid_parametros;
            o_tipo_mensaje := v_tipo_mensaje;
            o_id_integrante := 0;
            return;
          end if;
        else
          -- si esta marcado como dato erroneo, cambio la descripcion y lo marco.
          if p_error_dato = 'S' then
            update ipj.t_integrantes
            set
              error_dato = p_error_dato,
              detalle = p_detalle
            where
              id_integrante = p_id_integrante;
          end if;

          v_id_integrante := p_id_integrante;
          o_id_integrante := p_id_integrante;
        end if;
     end if;

      -- Cambio los 0 por NULL para respetar las claves foraneas
      v_Id_Tipo_Organismo := (case when p_Id_Tipo_Organismo = 0 then null else p_Id_Tipo_Organismo end);
      v_id_motivo_baja := (case when p_Id_motivo_baja = 0 then null else p_Id_motivo_baja end);
      v_id_entidades_Accion := (case when p_id_entidades_accion = 0 then null else p_id_entidades_accion end);
      v_id_integrante := (case when nvl(v_id_integrante, 0) = 0 then null else v_id_integrante end);

      v_bloque := 'GUARDAR ADMIN ';
      update ipj.t_entidades_admin e
      set
        fecha_fin = to_date(p_fecha_fin, 'dd/mm/rrrr'),
        id_entidades_accion = v_id_entidades_accion,
        id_motivo_baja = v_id_motivo_baja,
        porc_garantia = IPJ.VARIOS.ToNumber(p_porc_garantia),
        monto_garantia = IPJ.VARIOS.ToNumber(p_monto_garantia),
        habilitado_present = p_habilitado_present,
        persona_expuesta = p_persona_expuesta,
        id_moneda = p_id_moneda,
        Id_Tipo_Entidad = (case when nvl(Id_Tipo_Entidad, 0) = 0 then null else p_Id_Tipo_Entidad end),
        fec_acta = to_date(p_fec_acta, 'dd/mm/rrrr'),
        matricula = p_matricula,
        folio = p_folio,
        anio = p_anio,
        fecha_inicio = to_date(p_fecha_inicio, 'dd/mm/rrrr'),
        id_vin = decode(p_id_vin, 0, id_vin, null, id_vin, p_id_vin), -- BUG 11176
        es_admin_afip = p_es_admin_afip
      where
        id_admin = p_id_admin;

      o_id_admin := p_id_admin;

      -- si no existe, lo agrego
      if sql%rowcount = 0 then
        SELECT IPJ.SEQ_ENTIDAD_ADMIN.nextval INTO o_id_admin FROM dual;

        insert into ipj.t_entidades_admin
          ( id_tipo_integrante, Id_Tramite_Ipj, fecha_inicio, fecha_fin, id_integrante, id_legajo, borrador,
           Id_Vin, Id_Tipo_Organismo, id_entidades_accion, id_motivo_baja, porc_garantia,
           monto_garantia, habilitado_present, persona_expuesta, id_moneda, id_admin,
           cuit_empresa, n_empresa, matricula, fec_acta, id_legajo_empresa,
           folio, anio, id_tipo_entidad, es_admin_afip)
        values
          (p_id_tipo_integrante, p_Id_Tramite_Ipj, to_date(p_fecha_inicio, 'dd/mm/rrrr'),
          to_date(p_fecha_fin, 'dd/mm/rrrr'), v_id_integrante, p_id_legajo, 'S',
          decode(p_id_vin, 0, NULL, p_id_vin), v_Id_Tipo_Organismo, v_id_entidades_accion, v_id_motivo_baja,
          IPJ.VARIOS.ToNumber(p_porc_garantia), IPJ.VARIOS.ToNumber(p_monto_garantia),
          p_habilitado_present, p_persona_expuesta, p_id_moneda, o_id_admin,
          p_cuit_empresa, p_n_empresa, p_matricula, to_date(p_fec_acta, 'dd/mm/rrrr'),
          (case when nvl(p_id_legajo_empresa, 0) = 0 then null else p_id_legajo_empresa end),
          p_folio, p_anio, (case when nvl(p_Id_Tipo_Entidad, 0) = 0 then null else p_Id_Tipo_Entidad end),
          p_es_admin_afip);
      end if;

      IPJ.ENTIDAD_PERSJUR.SP_GUARDAR_ENTIDAD_ADMIN_HIST(
        o_rdo => o_rdo,
        o_tipo_mensaje => o_tipo_mensaje,
        p_Id_Tramite_Ipj => p_Id_Tramite_Ipj_entidad,
        p_id_legajo => p_id_legajo,
        p_id_admin => o_id_admin,
        p_id_tipo_integrante => p_id_tipo_integrante,
        p_fecha_inicio => p_fecha_inicio,
        p_fecha_fin => p_fecha_fin,
        p_id_integrante => v_id_integrante,
        p_Id_Vin => p_id_vin,
        p_Id_Tipo_Organismo => v_Id_Tipo_Organismo,
        p_id_motivo_baja => v_id_motivo_baja,
        p_cuit_empresa => p_cuit_empresa,
        p_n_empresa => p_n_empresa,
        p_id_legajo_empresa => (case when nvl(p_id_legajo_empresa, 0) = 0 then null else p_id_legajo_empresa end));

      if o_rdo <> TYPES.c_Resp_OK then
        o_rdo := 'ADMIN HIST: ' || o_rdo;
        o_id_integrante := 0;
        return;
      end if;
    end if;

    o_rdo := TYPES.c_Resp_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
    WHEN OTHERS THEN
       o_rdo := v_bloque || To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_GUARDAR_ENTIDAD_ADMIN;


  PROCEDURE SP_GUARDAR_ENTIDAD_RUBROS(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number,
    p_id_rubro in varchar2,
    p_id_tipo_actividad in varchar2,
    p_id_actividad in varchar2,
    p_fecha_inicio in varchar2,
    p_fecha_vencimiento in varchar2,
    p_eliminar in number) -- 1 elimina
  IS
    v_actividad_existente varchar2(20) := null;
    v_id_actividad varchar2(20) := null;
    v_id_rubro varchar2(3) := null;
    v_id_tipo_actividad varchar2(3) := null;
    v_valid_parametros varchar2(200);
    v_bloque varchar2(100);
    v_nro_orden number;
  BEGIN
  /***********************************************************
    Guarda los rubros y actividades asociados a una entidad de un tramite
    Si se utiliza solo Rubro y Tipo_Actividades, se guarda la primera actividad de la vista.
    Luego el programa se supone que no muestra ese nivel.
    Si lo utilizan, siempre viene por parametro y se utiliza normalmente.
    No maneja transaccion, ya que se usa en cascada desde la aplicacion y lo administra el codigo.
  ************************************************************/
    -- VALIDACION DE PARAMETROS
    v_bloque := 'GUARDAR RUBROS - Par�metros: ';
    if IPJ.VARIOS.Valida_Entidad(nvl(p_Id_Tramite_Ipj, 0), p_id_legajo) = 0 then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('ENT_NOT')|| '(' || to_char(p_Id_Tramite_Ipj) ||' - ' || to_char(p_id_legajo)||')';
    end if;
    if p_id_actividad is null then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('RUBRO_NOT');
    end if;
    if IPJ.VARIOS.Valida_Fecha(p_fecha_inicio) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT');
    end if;
    if p_fecha_vencimiento is not null and IPJ.VARIOS.Valida_Fecha(p_fecha_vencimiento) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT');
    end if;

    -- si los parametros no estan OK, no continuo
    if v_valid_parametros is not null then
      o_rdo := v_bloque || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    -- CUERPO DEL PROCEDIMIENTO
    if p_eliminar = 1 then
      delete ipj.t_entidades_rubros
      where
        Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        id_legajo = p_id_legajo and
        id_rubro = p_id_rubro and
        id_tipo_actividad = p_id_tipo_actividad and
        id_actividad = p_id_actividad and
        borrador = 'S';

    else
      v_bloque := '(Rubros/Actividades) ';
      -- Verifico si ya esta cargado o no.
      select nvl(max(id_actividad), '-1') into v_actividad_existente
      from ipj.t_entidades_rubros re join IPJ.t_entidades e
        on re.Id_Tramite_Ipj = e.Id_Tramite_Ipj and re.id_legajo = e.id_legajo
      where
        e.id_legajo = p_id_legajo and
        re.id_rubro = p_id_rubro and
        re.id_tipo_actividad = p_id_tipo_actividad and
        re.id_actividad = p_id_actividad;

      -- Si solo viene e codigo de Actividad, busco el rubto y tipo asociados
      if p_id_actividad is not null  and p_id_rubro is null and p_id_tipo_actividad is null then
        select id_rubro into v_id_rubro
        from t_comunes.vt_actividades a
        where
          a.id_actividad = p_id_actividad;

        select id_tipo_actividad into v_id_tipo_actividad
        from t_comunes.vt_actividades a
        where
          a.id_actividad = p_id_actividad;
      else
        v_id_rubro := p_id_rubro;
        v_id_tipo_actividad :=   p_id_tipo_actividad;
      end if;

      -- si no viene actividad, busco la primera del tipo dado
      if p_id_actividad is null then
        select id_actividad into v_id_actividad
        from t_comunes.vt_actividades a
        where
          a.ID_RUBRO = p_id_rubro and
          a.ID_TIPO_ACTIVIDAD = p_id_tipo_actividad and
          rownum = 1;
      else
        v_id_actividad := p_id_actividad;
      end if;

      -- Actualizo la fecha de baja
      update ipj.t_entidades_rubros
      set
        fecha_hasta = to_date(p_fecha_vencimiento, 'dd/mm/rrrr')
      where
        Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        id_legajo = p_id_legajo and
        id_rubro = v_id_rubro and
        id_tipo_actividad = v_id_tipo_actividad and
        id_actividad = nvl(p_id_actividad, v_actividad_existente);

      -- si no existe, lo agrego
      if sql%rowcount = 0 then
        -- (PBI 11833) Calculo el nro_orden
        begin
          select max(nvl(nro_orden, 0)) + 1 into v_nro_orden
          from ipj.t_entidades_rubros
          where
            id_legajo = p_id_legajo;
        exception
          when NO_DATA_FOUND then
            v_nro_orden := 1;
        end;

        insert into ipj.t_entidades_rubros
          (fecha_desde, fecha_hasta, id_rubro, id_tipo_actividad, id_actividad, Id_Tramite_Ipj,
           id_legajo, borrador, nro_orden)
        values
          (To_Date(p_fecha_inicio, 'dd/mm/rrrr'), to_date(p_fecha_vencimiento, 'dd/mm/rrrr'),
           v_id_rubro, v_id_tipo_actividad, nvl(p_id_actividad, v_id_actividad), p_Id_Tramite_Ipj,
           p_id_legajo, 'S', nvl(v_nro_orden, 1));
      end if;
    end if;

    if o_rdo is null then
      o_rdo := 'OK';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    end if;
  EXCEPTION
    WHEN OTHERS THEN
      o_rdo := v_bloque || To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_GUARDAR_ENTIDAD_RUBROS;


  PROCEDURE SP_GUARDAR_INTEGRANTES(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_integrante out number,
    o_pai_cod_pais out varchar2,
    p_id_sexo in varchar2,
    p_nro_documento in varchar2,
    p_pai_cod_pais in varchar2,
    p_id_numero in number,
    p_cuil in varchar2,
    p_detalle in varchar2,
    p_nombre in varchar2,
    p_apellido in varchar2,
    p_error_dato in varchar2,
    p_n_tipo_documento in varchar2,
    p_fec_nac in varchar2,
    p_id_sintys in number)
  IS
    /*************************************************
      Guarda nuevos integrantes en la tabla.
      No maneja transaccion, ya que se usa en cascada desde la aplicacion y lo administra el codigo.
  **************************************************/
    v_valid_parametros varchar2(200);
    v_mensaje varchar2(200);
    v_bloque varchar2 (100);
    v_Id_Retorno Number;
    v_Id_Sexo Varchar2(5);
    v_Nro_Documento Varchar2(20);
    v_Pai_Cod_Pais Varchar2(10);
    v_Id_Numero Number;
    v_Retorno Varchar2(3000);
    v_detalle varchar2(100);
    v_cod_pais varchar2(10);
    v_id_tipo_documento varchar2(20);
    v_org_emisor varchar2(20);
    v_cuil varchar2(20);
  BEGIN
    if IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('HAB_LOG_GESTION') = '1' then
      IPJ.VARIOS.SP_GUARDAR_LOG(
        p_METODO => 'SP_GUARDAR_INTEGRANTES',
        p_NIVEL => 'Parametros',
        p_ORIGEN => 'DB',
        p_USR_LOG  => 'DB',
        p_USR_WINDOWS => 'Sistema',
        p_IP_PC  => null,
        p_EXCEPCION =>
          'Id Sexo = ' || p_id_sexo
          || ' / Nro Documento = ' || p_nro_documento
          || ' / Pai Cod Pais = ' || p_pai_cod_pais
          || ' / Id Numero = ' || to_char(p_id_numero)
          || ' / Cuil = ' || p_cuil
          || ' / Detalle = ' || p_detalle
          || ' / Nombre = ' || p_nombre
          || ' / Apellido = ' || p_apellido
          || ' / Error Dato = ' || p_error_dato
          || ' / Tipo Documento = ' || p_n_tipo_documento
          || ' / Fec Nacimiento = ' || p_fec_nac
          || ' / Id Sintys = ' || to_char(p_id_sintys)
        );
    end if;

    -- VALIDACION DE PARAMETROS
    v_bloque := 'Par�metros: ';
    if p_id_sexo is null then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('SEX_NOT');
    end if;
    if p_nro_documento is null then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('DNI_NOT');
    end if;
    if nvl(p_id_numero, 0) < 0 then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('NRO_NOT');
    end if;
    if p_cuil is not null then
      t_comunes.pack_persona_juridica.VerificarCUIT (p_cuil, v_mensaje);
      if Upper(trim(v_mensaje)) != IPJ.TYPES.C_RESP_OK then
        v_valid_parametros := v_valid_parametros || v_Mensaje || ' .';
      end if;
    end if;
    if p_detalle is null and p_nombre is null and p_apellido is null then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('NOMBRE_NOT');
    end if;

    -- si los parametros no estan OK, nocontinuo
    if v_valid_parametros is not null then
      o_rdo := v_bloque || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    -- CUERPO DEL PROCEDIMIENTO
    if p_pai_cod_pais is null or p_n_tipo_documento is not null then
      begin
        select T.COD_PAIS_ORIGEN, T.ID_TIPO_DOCUMENTO, T.ID_ORGANISMO_EMISOR into v_cod_pais, v_id_tipo_documento, v_org_emisor
        from RCIVIL.VT_TIPOS_DOCUMENTOS t
        where
          n_tipo_documento = nvl(p_n_tipo_documento, ' ') and
          rownum = 1;
      exception
        when NO_DATA_FOUND then
          v_cod_pais  := 'ARG';
          v_id_tipo_documento := 'DNI';
          v_org_emisor := 'RNP';
      end;
    else
      v_cod_pais := p_pai_cod_pais;
    end if;

    -- valido si ya existe un integrante
    select SUM(id_integrante) into o_id_integrante
    from IPJ.t_integrantes
    where
      id_sexo = p_id_sexo and
      nro_documento = p_nro_documento and
      pai_cod_pais = nvl(v_cod_pais, 'ARG') and
      id_numero = nvl(p_id_numero, 0);

    -- Si no pasan CUIL, veo si existe alguno
    if p_cuil is null then
      begin
        select cuil into v_cuil
        from RCIVIL.VT_PERSONAS_CUIL
        where
          id_sexo = p_id_sexo and
          nro_documento = p_nro_documento and
          pai_cod_pais = v_cod_pais and
          id_numero = p_id_numero;
      exception
        when NO_DATA_FOUND then
          v_cuil := null;
      end;
    else
      v_cuil := p_cuil;
    end if;

    -- si no existe el integrante, inserto uno nuevo
    if NVL(o_id_integrante, 0) = 0 and p_Nombre is not null and p_apellido is not null then
      v_bloque := 'Insert RCivil: ';

      -- Lo agrego en RCIVIL
      if p_id_sintys > 0 then
        RCIVIL.PACK_PERSONA.INSERT_PERSONA_IDS(
          P_ID_APLICACION => IPJ.TYPES.C_ID_APLICACION,
          P_ID_SEXO => p_id_sexo,
          P_NRO_DOCUMENTO => p_nro_documento, -- para eliinar 0 a la izquierda
          P_PAI_COD_PAIS_ORIGEN => v_cod_pais,
          P_PAI_COD_PAIS_NACIONALIDAD => v_cod_pais,
          P_ID_TIPO_DOCUMENTO => v_id_tipo_documento,
          P_ORGANISMO_EMISOR_DOC => v_org_emisor,
          P_PAIS_TIPO_DOC => v_cod_pais,
          P_LOCALIDAD => 1, --select * from rcivil.vt_localidades;
          P_APELLIDO => p_apellido,
          P_NOMBRE => p_nombre,
          P_CLASE => NULL,
          P_OBSERVACIONES => NULL,
          P_FEC_NACIMIENTO => to_date (p_fec_nac, 'dd/mm/rrrr'),
          P_ID_GRUPO_FAMILIAR => NULL,
          P_ID_ESTADO_CIVIL => 'Z', --select * from Rcivil.T_estados_civil;
          P_VINCULO => 'D', --select * from Rcivil.T_vinculos;
          P_FECHA_DEFUNCION => NULL,
          P_PK_SINTYS => p_id_sintys,
          O_ID_RETORNO => v_id_retorno,
          O_ID_SEXO => v_id_sexo,
          O_NRO_DOCUMENTO => v_nro_documento,
          O_PAI_COD_PAIS => v_pai_cod_pais,
          O_ID_NUMERO => v_id_numero,
          O_RETORNO => v_retorno);
      else
        RCIVIL.PACK_PERSONA.INSERT_PERSONA(
          P_ID_APLICACION => IPJ.TYPES.C_ID_APLICACION,
          P_ID_SEXO => p_id_sexo,
          P_NRO_DOCUMENTO => p_nro_documento, -- para eliinar 0 a la izquierda
          P_PAI_COD_PAIS_ORIGEN => v_cod_pais,
          P_PAI_COD_PAIS_NACIONALIDAD => v_cod_pais,
          P_ID_TIPO_DOCUMENTO => v_id_tipo_documento,
          P_ORGANISMO_EMISOR_DOC => v_org_emisor,
          P_PAIS_TIPO_DOC => v_cod_pais,
          P_LOCALIDAD => 1, --select * from rcivil.vt_localidades;
          P_APELLIDO => p_apellido,
          P_NOMBRE => p_nombre,
          P_CLASE => NULL,
          P_OBSERVACIONES => NULL,
          P_FEC_NACIMIENTO => to_date (p_fec_nac, 'dd/mm/rrrr'),
          P_ID_GRUPO_FAMILIAR => NULL,
          P_ID_ESTADO_CIVIL => 'Z', --select * from Rcivil.T_estados_civil;
          P_VINCULO => 'D', --select * from Rcivil.T_vinculos;
          P_FECHA_DEFUNCION => NULL,
          O_ID_RETORNO => v_id_retorno,
          O_ID_SEXO => v_id_sexo,
          O_NRO_DOCUMENTO => v_nro_documento,
          O_PAI_COD_PAIS => v_pai_cod_pais,
          O_ID_NUMERO => v_id_numero,
          O_RETORNO => v_retorno);
        end if;

         v_detalle :=  p_nombre || ' ' || p_apellido;
    else
      -- Actualizo el nombre si existe
      update IPJ.t_integrantes
      set
        detalle = p_detalle,
        error_dato = p_error_dato,
        cuil = (case when v_cuil is null then cuil else v_cuil end)
      where
        id_integrante = o_id_integrante and
        (detalle <> p_detalle or nvl(cuil, '-') <> nvl(v_cuil, '-'));

      v_retorno := IPJ.TYPES.C_RESP_OK;
      v_id_sexo := p_id_sexo;
      v_nro_documento := p_nro_documento;
      v_pai_cod_pais := v_cod_pais;
      v_id_numero := p_id_numero;
      v_detalle := p_detalle;
    end if;

    o_pai_cod_pais := v_pai_cod_pais;

    if NVL(o_id_integrante, 0) = 0 then
      if UPPER(v_retorno) like IPJ.TYPES.C_RESP_OK || '%'then
        -- Lo agrego localmente con el resultado de Rcivil
        SELECT ipj.SEQ_Integrantes.nextval INTO o_id_integrante FROM dual;

        insert into ipj.t_integrantes
          (id_integrante, id_sexo, nro_documento, pai_cod_pais, id_numero, cuil, detalle, error_dato)
        values
          (o_id_integrante, v_id_sexo, v_nro_documento, v_pai_cod_pais, v_id_numero, v_cuil,
          v_detalle, p_error_dato);
      else
        o_rdo := v_retorno;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
        return;
      end if;
    end if;

    o_rdo := TYPES.c_Resp_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
    WHEN OTHERS THEN
      o_rdo := v_bloque || To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_GUARDAR_INTEGRANTES;


  PROCEDURE SP_CREAR_ENTIDAD_TRAMITE(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_legajo in number,
    p_id_sede in varchar2,
    p_Id_Tramite_Ipj in number,
    p_id_tramiteipj_accion in number)
  IS
    v_Id_Tramite_Ipj number;
    v_id_tramite_base number;
    v_id_legajo_base number;
    v_existe_persjur number;
    v_existe_persjur_sede number;
    v_id_vin_real number(20);
    v_id_sede varchar2(3);
    v_bloque varchar2(50);
    v_valid_parametros varchar2(200);
    v_mensaje varchar2(200);
    v_protocolo number;
    v_Result_log varchar2(500);
    v_Row_Legajos IPJ.t_legajos%ROWTYPE;
    v_ubidacion_leg number; -- SRL // SxA // Civiles y Fundaciones
    v_id_estado_accion number;
    v_id_usuario_accion varchar2(20);
    v_id_pagina_accion number(6);
    v_resp_hist varchar2(500);
    v_tipo_resp_hist number;
    v_id_tipo_accion number;
    v_ubicacion_ent number(6);
    v_matricula varchar2(20);
    v_versionado number(6);
    v_es_agrupable number;
  BEGIN
  /***********************************************************
    Crea una nueva entidad, asociandola al tramite indicado.
    Dado un Legajo y una sede, genero una nueva entrada en entidades con los ultimos datos no borradores
    de las entidades;si no hay datos previos en entidades, busca los de Persona Juridica y si no existe
    crea un registro con solo los datos del legajo.
  ************************************************************/
    /*
       Detalles de campos de entidad:
        - Denominacion_1: es la Razon Social
        - Denominacion_2: es el Nombre de la Sede
        - Alta_Temporal: es la fecha de inicio de actividades
        - Baja_Temporal: es la fecha de fin de actividades
    */
    --  VALIDACION DE PARAMETROS
    if p_id_sede is null then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('SEDE_NOT') ;
    end if;

    if nvl(p_Id_Tramite_Ipj, 0) <= 0 then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('ENT_NOT');
    end if;

    -- Si los parametros no estan OK, no continuo
    if v_valid_parametros is not null then
      o_rdo := v_bloque || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    -- Busco las notas de SUAC, si existen
    IPJ.TRAMITES_SUAC.SP_Bajar_Notas_SUAC(
      o_rdo => o_rdo,
      o_tipo_mensaje => o_tipo_mensaje,
      p_Id_Tramite_Ipj => p_Id_Tramite_Ipj
    );

    -- BUG 8367: Busca los documentos del portal
    SP_Bajar_Docs_Tramite(
      o_rdo => o_rdo,
      o_tipo_mensaje => o_tipo_mensaje,
      p_Id_Tramite_Ipj => p_id_tramite_ipj,
      p_id_legajo => p_id_legajo
    );
    commit;

    --  ***************** CUERPO DEL PROCEDIMEINTO ***************
    -- Si no hay legajo, salgo y no hago nada
    if nvl(p_id_legajo, 0) = 0 then
      o_rdo := IPJ.TYPES.C_RESP_OK;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
      return;
    end if;

    --Busco el legajo para tomar ciertos datos
    select * into v_Row_Legajos
    from ipj.t_legajos
    where
      nvl(eliminado, 0)  = 0 and
      id_legajo = p_id_legajo and ROWNUM < 2;

    begin
      select id_ubicacion into v_ubidacion_leg
      from IPJ.T_TIPOS_ENTIDADES
      where
        id_tipo_entidad =  v_Row_Legajos.id_tipo_entidad;
    exception
       when NO_DATA_FOUND then
         v_ubidacion_leg := 0;
    end;

    -- Si no hay una entidad creada para ese tramite, la crea
    if IPJ.VARIOS.VALIDA_ENTIDAD(p_Id_Tramite_Ipj, p_id_legajo ) = 0 then
        --Si la sede viene de 1 digito, le agrego un 0 adelante
        if length(p_id_sede) < 2 then
          v_id_sede := lpad(p_id_sede, 2, '0');
        else
          v_id_sede := p_id_sede;
        end if;

        -- obtengo el maximo tramite cerrado de la empresa y sede, para tomarlo como base
        IPJ.ENTIDAD_PERSJUR.SP_Buscar_Ult_Tramite(
          o_rdo => o_rdo,
          o_tipo_mensaje => o_tipo_mensaje,
          o_id_tramite_base => v_id_tramite_base,
          o_id_legajo_base => v_id_legajo_base,
          o_ubicacion_ent => v_ubicacion_ent,
          p_id_legajo => p_id_legajo,
          p_id_ubicacion => v_ubidacion_leg);

        if v_id_tramite_base > 0 then --Si hay registros, cargo la entidad previa
          v_bloque := 'Entidad: ';
         -- Creo la nueva entrada para entidad
          begin
            insert into ipj.t_entidades (
              Id_Tramite_Ipj, Cuit, Id_Moneda, Id_Sede, Id_Unidad_Med, Matricula, Fec_Inscripcion, Folio,
              Libro_Nro, Tomo, Anio, Objeto_Social, Monto, Acta_Contitutiva, Vigencia, Cierre_Ejercicio,
              Fec_Vig, Id_Vin_Comercial, Id_Vin_Real, Borrador, Fec_Acto, Denominacion_1, Denominacion_2,
              Alta_Temporal, Baja_Temporal, Tipo_Vigencia, Valor_Cuota, Matricula_Version, Cuotas,
              Id_Tipo_Entidad, Id_Legajo, Id_Tipo_Administracion, Id_Estado_Entidad, Obs_Cierre,
              Nro_Resolucion, Fec_Resolucion, Nro_Boletin, Fec_Boletin, Nro_Registro, Sede_Estatuto,
              Requiere_Sindico, Origen, Es_Sucursal, Observacion, Id_Tipo_Origen, Cierre_Fin_Mes,
              fiscalizacion_ejerc, incluida_lgs, condicion_fideic, obs_fiduciario,
              obs_fiduciante, obs_beneficiario, obs_fideicomisario, fecha_versionado,
              sin_denominacion, contrato_privado, fec_vig_hasta)
            select
              P_Id_Tramite_Ipj, trim(Cuit), Id_Moneda, Id_Sede, Id_Unidad_Med, Matricula, Fec_Inscripcion, Folio,
              Libro_Nro, Tomo, Anio, Objeto_Social, Monto, Acta_Contitutiva, Vigencia, Cierre_Ejercicio,
              Fec_Vig, Id_Vin_Comercial, Id_Vin_Real, 'S', Fec_Acto, Denominacion_1, Denominacion_2,
              Alta_Temporal, Baja_Temporal, Tipo_Vigencia, Valor_Cuota, Matricula_Version, Cuotas,
              Id_Tipo_Entidad, P_Id_Legajo, Id_Tipo_Administracion, Id_Estado_Entidad, Obs_Cierre,
              (case when v_ubidacion_leg = IPJ.TYPES.C_AREA_CYF then nro_resolucion else null end) Nro_Resolucion,
              (case when v_ubidacion_leg = IPJ.TYPES.C_AREA_CYF then Fec_Resolucion else null end) Fec_Resolucion,
              Nro_Boletin, Fec_Boletin, Nro_Registro, Sede_Estatuto,
              Requiere_Sindico, Origen, Es_Sucursal, Observacion, Id_Tipo_Origen, Cierre_Fin_Mes,
              fiscalizacion_ejerc, incluida_lgs, condicion_fideic, obs_fiduciario,
              obs_fiduciante, obs_beneficiario, obs_fideicomisario, null fecha_versionado,
              sin_denominacion, contrato_privado, fec_vig_hasta
            from IPJ.t_entidades e
            where
              e.Id_Tramite_Ipj = v_id_tramite_base and
              e.id_legajo = v_id_legajo_base;

            -- Si se cambia de Area, limpiamos la matr�cula, versi�n y ponemos el tipo de entidad del legajo
            if v_ubidacion_leg <> v_ubicacion_ent then
              -- busco su matr�cula, y la maxima version
              begin
                select max(matricula), max(matricula_version) into v_matricula, v_versionado
                from ipj.t_entidades e join ipj.t_tramitesipj tr
                  on e.Id_Tramite_Ipj = tr.Id_Tramite_Ipj
                where
                  e.id_legajo = P_Id_Legajo and
                  e.borrador = 'N' and
                  e.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_ubidacion_leg) and
                  tr.id_estado_ult < IPJ.TYPES.C_ESTADOS_RECHAZADO;
              exception
                when NO_DATA_FOUND then
                  v_matricula := null;
                  v_versionado := -1;
              end;

              update ipj.t_entidades
              set
                matricula = v_matricula,
                matricula_version = nvl(v_versionado, -1),
                id_tipo_entidad = v_Row_Legajos.id_tipo_entidad
              where
                Id_Tramite_Ipj = P_Id_Tramite_Ipj and
                id_legajo = P_Id_Legajo;
            end if;

          exception
            when others then
              o_rdo := v_bloque || To_Char(SQLCODE) || '-' || SQLERRM;
              rollback;
              IPJ.VARIOS.SP_GUARDAR_LOG ( 'SP_CREAR_ENTIDAD_TRAMITE', 'ERROR',
                 'T_ENTIDADES', 'USR_IPJ (DB)', null, null, To_Char(SQLCODE) || '-' || SQLERRM);
              o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
              return;
          end;
        else -- NO hay entidad previa, lo cargo desde Persona Juridica

          select Count(*) into v_existe_persjur_sede
          from T_COMUNES.vt_pers_juridicas_completa pj
          where
            pj.cuit = trim(v_Row_Legajos.Cuit) and
            id_sede = v_id_sede;

          select Count(*) into v_existe_persjur
          from T_COMUNES.vt_pers_juridicas_completa pj
          where
            pj.cuit = trim(v_Row_Legajos.Cuit);

          v_bloque := 'Persona Juridica: ';
          begin
            if v_existe_persjur_sede > 0 then -- Existe en Persona Juridica
              -- Agrego Persona Juridica y sede
              insert into ipj.t_entidades
                (Id_Tramite_Ipj, cuit, objeto_social, monto, acta_contitutiva, vigencia, id_moneda,
                 cierre_ejercicio, id_unidad_med, fec_vig, id_vin_comercial, id_vin_real,
                 matricula, fec_inscripcion, folio, libro_nro, tomo, anio, borrador,
                 denominacion_1, denominacion_2, id_sede, alta_temporal, baja_temporal,
                 id_legajo, MATRICULA_VERSION,
                 id_estado_entidad, obs_cierre, nro_registro, id_tipo_entidad)
              select
                p_Id_Tramite_Ipj, trim(pj.cuit), '', 0, null, 99, null,
                null, null, null, null, null,
                (case v_ubidacion_leg
                   when IPJ.TYPES.C_AREA_SRL then IPJ.VARIOS.FC_Formatear_Matricula(v_Row_Legajos.nro_ficha)
                   when IPJ.TYPES.C_AREA_SXA then IPJ.VARIOS.FC_Formatear_Matricula(v_Row_Legajos.nro_ficha)
                   else null
                end), --matricula
                null, null, null, null, null,  'S',
                pj.razon_social, pj.n_sede, pj.id_sede, pj.fec_inicio_act, pj.fec_fin_act,
                p_id_legajo, -1,
                null, '',
                (case v_ubidacion_leg
                    when IPJ.TYPES.C_AREA_CYF then v_Row_Legajos.nro_ficha
                    when IPJ.TYPES.C_AREA_SXA then to_char(v_Row_Legajos.registro)
                    else null
                end), --nro_registro
                v_Row_Legajos.id_tipo_entidad
              from T_COMUNES.vt_pers_juridicas_completa pj
              where
                pj.cuit = trim(v_Row_Legajos.cuit) and
                id_sede = v_id_sede;

              -- Actualizo la direccion de la sede
              begin
                select max(id_vin) into v_id_vin_real
                from T_COMUNES.vt_domicilio_persjur
                where
                  cuit = trim(v_Row_Legajos.Cuit) and
                  id_sede = v_id_sede;

                update IPJ.t_entidades
                set id_vin_real = v_id_vin_real
                where
                  Id_Tramite_Ipj = p_Id_Tramite_Ipj;
              exception
                when NO_DATA_FOUND then
                  update IPJ.t_entidades
                  set id_vin_real = null
                  where
                    Id_Tramite_Ipj = p_Id_Tramite_Ipj and
                    id_legajo = p_id_legajo;

                when others then
                  o_rdo := 'Domicilio ' || To_Char(SQLCODE) || '-' || SQLERRM;
                  rollback;
                  IPJ.VARIOS.SP_GUARDAR_LOG( 'SP_CREAR_ENTIDAD_TRAMITE', 'ERROR',
                    'T_COMUNES.vt_domicilio_persjur', 'USR_IPJ (DB)', null, null, To_Char(SQLCODE) || '-' || SQLERRM);
                  o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
                  return;
              end;
            else -- No existe la sede, cargo por el cuit
              if v_existe_persjur > 0 then
                insert into ipj.t_entidades
                  (Id_Tramite_Ipj, cuit, objeto_social, monto, acta_contitutiva, vigencia, id_moneda,
                   cierre_ejercicio, id_unidad_med, fec_vig, id_vin_comercial, id_vin_real,
                   matricula, fec_inscripcion, folio, libro_nro, tomo, anio, borrador,
                   denominacion_1, denominacion_2, id_sede, alta_temporal, baja_temporal,
                   id_legajo, MATRICULA_VERSION,
                   id_estado_entidad, obs_cierre, nro_registro, id_tipo_entidad)
                select
                  p_Id_Tramite_Ipj, trim(pj.cuit), '', 0, null, 99, null,
                  null, null, null, null, null,
                  (case v_ubidacion_leg
                      when IPJ.TYPES.C_AREA_SRL then IPJ.VARIOS.FC_Formatear_Matricula(v_Row_Legajos.nro_ficha)
                      when IPJ.TYPES.C_AREA_SXA then IPJ.VARIOS.FC_Formatear_Matricula(v_Row_Legajos.nro_ficha)
                      else null
                  end), --matricula
                  null, null, null, null, null, 'S',
                  pj.razon_social, 'Nueva Sede', v_id_sede, pj.fec_inicio_act, pj.fec_fin_act,
                  p_id_legajo, -1,
                  null, '',
                  (case v_ubidacion_leg
                      when IPJ.TYPES.C_AREA_CYF then v_Row_Legajos.nro_ficha
                      when IPJ.TYPES.C_AREA_SXA then to_char(v_Row_Legajos.registro)
                      else null
                  end), --nro_registro
                  v_Row_Legajos.id_tipo_entidad
                from T_COMUNES.vt_pers_juridicas_completa pj
                where
                  pj.cuit = trim(v_Row_Legajos.Cuit) and
                  rownum = 1;

              else
                 -- No existe Entidad ni Persona Juridica
                v_bloque := 'Nuevo: ';
                insert into ipj.t_entidades
                  (Id_Tramite_Ipj, id_legajo, objeto_social, monto, acta_contitutiva, vigencia, id_moneda,
                   cierre_ejercicio, id_unidad_med, fec_vig, id_vin_comercial, id_vin_real,
                   matricula, fec_inscripcion, folio, libro_nro, tomo, anio, borrador,
                   denominacion_1, denominacion_2, id_sede, alta_temporal, baja_temporal, cuit,
                   id_estado_entidad, obs_cierre, matricula_version, nro_registro, id_tipo_entidad )
                values (
                  p_Id_Tramite_Ipj, p_id_legajo, '', 0, null, 99, null,
                  null, null, null, null, null,
                  (case v_ubidacion_leg
                      when IPJ.TYPES.C_AREA_SRL then IPJ.VARIOS.FC_Formatear_Matricula(v_Row_Legajos.nro_ficha)
                      when IPJ.TYPES.C_AREA_SXA then IPJ.VARIOS.FC_Formatear_Matricula(v_Row_Legajos.nro_ficha)
                      else null
                  end), -- matricula
                  null, v_Row_Legajos.folio, null, v_Row_Legajos.tomo, v_Row_Legajos.anio, 'S',
                  substr(v_Row_Legajos.Denominacion_Sia, 1, 150), 'Nueva Sede' ,v_id_sede , null, null, trim(v_Row_Legajos.cuit),
                  null, '', -1,
                  (case v_ubidacion_leg
                      when IPJ.TYPES.C_AREA_CyF then v_Row_Legajos.nro_ficha
                      when IPJ.TYPES.C_AREA_SXA then to_char(v_Row_Legajos.registro)
                      else null
                  end), -- nro_registro
                  v_Row_Legajos.id_tipo_entidad);
              end if;
            end if;

          exception
            when others then
              o_rdo := v_bloque || To_Char(SQLCODE) || '-' || SQLERRM;
              rollback;
              IPJ.VARIOS.SP_GUARDAR_LOG( 'SP_CREAR_ENTIDAD_TRAMITE', 'ERROR',
                    '', 'USR_IPJ (DB)', null, null, To_Char(SQLCODE) || '-' || SQLERRM);
              o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
              return;
          end;

          -- CARGO LOS RUBROS Y ACTIVIDADES
          -- Agrego los rubos y actividades de la Persona Juridica
          begin
            v_bloque := 'Rubros PersJur: ';
            insert into IPJ.t_entidades_rubros
              (fecha_desde, fecha_hasta, id_rubro, id_tipo_actividad, id_actividad,
               Id_Tramite_Ipj, id_legajo)
            select pa.fecha_inicio, pa.fecha_fin, pa.id_rubro, pa.id_tipo_actividad,
               pa.id_actividad, p_Id_Tramite_Ipj, p_id_legajo
            from T_COMUNES.vt_perjur_actividades pa
            where
               pa.cuit = trim(v_Row_Legajos.cuit) and
               pa.fecha_fin is null and
               pa.fecha_fin_rubro is null and
               pa.fecha_fin_tact is null and
               pa.fecha_fin_act is null;
          exception
            when others then
              o_rdo := v_bloque || To_Char(SQLCODE) || '-' || SQLERRM;
              rollback;
              IPJ.VARIOS.SP_GUARDAR_LOG( 'SP_CREAR_ENTIDAD_TRAMITE', 'ERROR',
                    '', 'USR_IPJ (DB)', null, null, To_Char(SQLCODE) || '-' || SQLERRM);
              o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
              return;
          end;
        end if;


      -- Si la accion esta asociada a un protocolo (es alguna modificacion)
      -- actualizo el versionado de la matricula
      select  nvl(TT.ID_PROTOCOLO, 0)  into v_protocolo
      from IPJ.T_TRAMITESIPJ_ACCIONES t join IPJ.T_TIPOS_ACCIONESIPJ tt
        on T.ID_TIPO_ACCION = TT.ID_TIPO_ACCION
      where
        T.Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        T.ID_TRAMITEIPJ_ACCION = p_id_tramiteipj_accion;

      if ( v_protocolo > 0 ) then
        update IPJ.T_ENTIDADES
        set MATRICULA_VERSION = nvl(MATRICULA_VERSION, 0) + 1
        where
          Id_Tramite_Ipj = p_Id_Tramite_Ipj and
          id_legajo = p_id_legajo;
      else
        update IPJ.T_ENTIDADES
        set MATRICULA_VERSION = (case when MATRICULA_VERSION = -1 then 0 else MATRICULA_VERSION end)
        where
          Id_Tramite_Ipj = p_Id_Tramite_Ipj and
          id_legajo = p_id_legajo;
      end if;
    end if;


    --Si la accion estaba PENDIENTE, ASIGNADA u OBSERVADA, lo marco como EN PROCESO
    select id_estado, cuil_usuario, ta.id_pagina, t.id_tipo_accion, (select nvl(agrupable, 0) from ipj.t_paginas p where p.id_pagina = ta.id_pagina)
      into v_id_estado_accion, v_id_usuario_accion, v_id_pagina_accion, v_id_tipo_accion, v_es_agrupable
    from ipj.t_tramitesipj_acciones t join ipj.t_tipos_accionesipj ta
      on t.id_tipo_accion = ta.id_tipo_accion
    where
      Id_Tramite_Ipj = p_Id_Tramite_Ipj and
      id_tramiteipj_accion = p_id_tramiteipj_accion;

    -- Si no es un tr�mite digital, Actualizo el estado del tramite y sus acciones, si estaba observado
    if IPJ.TRAMITES.FC_Es_Tram_Digital (p_Id_Tramite_Ipj) = 0 then

      if v_id_estado_accion in (IPJ.TYPES.c_Estados_Archivo_Inicial, IPJ.TYPES.c_Estados_Asignado, Ipj.Types.C_Estados_Observado ) then
        -- Actualizo todas del mismo Usuario, Pagina, Estado y Persona (fisica o jur�dica)
        update ipj.t_tramitesipj_acciones
        set id_estado = IPJ.TYPES.C_ESTADOS_PROCESO
        where
          Id_Tramite_Ipj = p_Id_Tramite_Ipj and
          cuil_usuario = v_id_usuario_accion and
          nvl(id_legajo, 0) = nvl(p_id_legajo, 0) and
          nvl(id_integrante, 0) = 0 and -- como es entidad siempre deben estar null
          id_estado = v_id_estado_accion and
          ( (v_es_agrupable = 0 and id_tramiteipj_accion = p_id_tramiteipj_accion) or -- BUG 8620
            (v_es_agrupable = 1 and id_tipo_accion in (select a.id_tipo_accion
                                    from IPJ.t_tipos_accionesipj a join ipj.t_paginas p
                                      on a.id_pagina = p.id_pagina
                                    where
                                       a.id_pagina = v_id_pagina_accion and p.agrupable = 1)
            )
          );


        -- Agrega el paso en el historial de la accion
        IPJ.TRAMITES.SP_GUARDAR_TRAMITES_ACC_ESTADO(
          o_rdo => o_Rdo,--v_resp_hist,
          o_tipo_mensaje => o_tipo_mensaje, --v_tipo_resp_hist,
          p_fecha_pase => sysdate,
          p_id_estado => IPJ.TYPES.C_ESTADOS_PROCESO,
          p_cuil_usuario => v_id_usuario_accion,
          p_observacion => Null,
          p_Id_Tramite_Ipj => p_Id_Tramite_Ipj,
          p_id_tramite_Accion => p_id_tramiteipj_accion,
          p_id_tipo_accion => v_id_tipo_accion,
          p_id_documento => null,
          p_n_documento => null );


        update ipj.t_tramitesipj
        set id_estado_ult = IPJ.TYPES.c_Estados_Asignado
        where
          id_tramite_ipj = p_Id_Tramite_Ipj and
          id_estado_ult = Ipj.Types.C_Estados_Observado;
      end if;
    end if;

   -- Si nada informa error, asigo OK y hago commit
    if nvl(o_rdo, IPJ.TYPES.C_RESP_OK) = IPJ.TYPES.C_RESP_OK then
      o_rdo := IPJ.TYPES.C_RESP_OK;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
      commit;
    else
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
       rollback;
    end if;
  EXCEPTION
    WHEN OTHERS THEN
      o_rdo := v_bloque || To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
    rollback;
  END SP_CREAR_ENTIDAD_TRAMITE;


  PROCEDURE SP_ACTUALIZAR_PERS_JURIDICA(
    o_rdo out varchar2,
    o_metodo out varchar2,
    o_origen out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number)
  IS
    v_result_PersJur varchar2(4000);
    v_result_PersJur_Sede varchar2(4000);
    v_result_PersJur_Dom varchar2(4000);
    v_result_PersJur_Rubros varchar2(4000);
    v_result_PersJur_Actividades varchar2(4000);
    v_Row_Entidad IPJ.T_Entidades%ROWTYPE;
    v_Row_PersJur T_COMUNES.vt_pers_juridicas_completa%ROWTYPE;
    v_Row_Entidad_Domicilio DOM_MANAGER.VT_DOMICILIOS_COND%ROWTYPE;
    v_Row_Entidad_Rubros IPJ.t_Entidades_Rubros%ROWTYPE;
    v_PersJur_NewSede varchar2(3);
    v_Cursor_Rubros types.cursorType;
    v_Cursor_Actividades types.cursorType;
    v_Existe_PersJur number;
    v_Existe_Rubro number;
    v_Existe_Actividad number;
    v_bloque varchar2(1000);
    v_Result_log varchar2(4000);
  BEGIN
  /***********************************************************
  Una vez cerrado un tramite, actualiza los datos de la entidad en Personas Juridicas.
  Se llama desde SP_GUARDAR_TRAMITE de tramite, que se encarga del manejo de la transaccion.
  ************************************************************/
     /* Detalles de campos de entidad:
      - Denominacion_1: es la Razon Social
      - Denominacion_2: es el Nombre de la Sede
      - Alta_Temporal: es la fecha de inicio de actividades
      - Baja_Temporal: es la fecha de fin de actividades
    */

    -- Busco la entidad del tramite, y la paso a no borrador
    begin
      select * into v_Row_Entidad
      from IPJ.t_entidades e
      where
        e.Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        e.id_legajo = p_id_legajo;

    exception
      when NO_DATA_FOUND then
        o_rdo := IPJ.VARIOS.MENSAJE_ERROR('ENT_NOT') || To_Char(p_Id_Tramite_Ipj);
        o_metodo:= 'SP_ACTUALIZAR_PERS_JURIDICA';
        o_origen:= 'IPJ.T_Entidades';
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
        DBMS_OUTPUT.PUT_LINE('   Error 1 = ' ||o_rdo);
        return;
    end;

    /* Valido si existe la entidad como Persona Juridica para actualizar sus datos:
       - Si existe llamo al MODIFICAR
       - Si no existe llamo al INSERTAR
    */
    -- La primer accon actualiza datos, y las demas no hacen nada
    if nvl(v_Row_Entidad.borrador, 'S') <> 'N' then
      DBMS_OUTPUT.PUT_LINE('1- SP_ACTUALIZAR_PERS_JURIDICA - Quita borradores');

      SP_Actualizar_Borrador(
        o_rdo => o_rdo,
        o_tipo_mensaje => o_tipo_mensaje,
        p_Id_Tramite_Ipj => p_Id_Tramite_Ipj,
        p_id_legajo => p_id_legajo,
        p_borrador => 'N'
      );

      if o_rdo != 'OK' then
        o_rdo := IPJ.VARIOS.MENSAJE_ERROR('ERROR')  || v_bloque || v_result_PersJur;
        o_metodo:= 'SP_ACTUALIZAR_PERS_JURIDICA';
        o_origen:= 'SP_Marcar_Controlada';
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
        DBMS_OUTPUT.PUT_LINE('   Error 2 = ' ||o_rdo);
        return;
      end if;

      SP_ACTUALIZAR_ENT_GOB(
        o_rdo => o_rdo,
        o_tipo_mensaje => o_tipo_mensaje,
        p_Id_Tramite_Ipj => p_id_tramite_ipj,
        p_id_legajo => p_id_legajo
      );
    else
      o_rdo := 'OK';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    end if;
  EXCEPTION
    WHEN OTHERS THEN
      o_rdo := 'SP_ACTUALIZAR_PERS_JURIDICA: ' || v_bloque || To_Char(SQLCODE) || '-' || SQLERRM;
      o_metodo:= 'SP_ACTUALIZAR_PERS_JURIDICA';
      o_origen:= '';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      DBMS_OUTPUT.PUT_LINE('11- SP_ACTUALIZAR_PERS_JURIDICA - Exepci�n General');
  END SP_ACTUALIZAR_PERS_JURIDICA;


  PROCEDURE SP_Buscar_Personas_RCivil(
    p_id_sexo in varchar2,
    p_nro_documento in varchar2,
    p_Cursor OUT TYPES.cursorType)
  IS
  BEGIN
    /***********************************************************
  Dado el genero y documento, lista el conjunto de personas con esos datos.
  ************************************************************/
    OPEN p_Cursor FOR
      select p.id_sexo, p.nro_documento,  p.pai_cod_pais, p.id_numero, p.NOV_NOMBRE || ' ' ||p.NOV_APELLIDO Detalle,
        I.ID_INTEGRANTE, i.id_vin,
        i.id_sexo || i.nro_documento || i.pai_cod_pais || to_char(i.id_numero) Clave,
        p.cuil, p.id_estado_civil, p.fec_nacimiento, p.nombre, p.apellido, p.id_tipo_documento,
        (select n_tipo_documento from rcivil.vt_tipos_documento td where td.id_tipo_documento = p.id_tipo_documento and td.id_pais_td = p.pai_cod_pais_td ) n_tipo_documento
      from rcivil.vt_pk_persona  p left join IPJ.T_INTEGRANTES i
        on P.ID_SEXO = I.ID_SEXO and
          p.NRO_DOCUMENTO = I.NRO_DOCUMENTO and
          p.PAI_COD_PAIS = I.PAI_COD_PAIS and
          P.ID_NUMERO = I.ID_NUMERO
      where
        (p_id_sexo is null or p.id_sexo = p_id_sexo)
         and
        lpad(p.nro_documento, 12, '0') = lpad(p_nro_documento, 12, '0');

  END SP_Buscar_Personas_RCivil;


  PROCEDURE SP_GUARDAR_ENTIDAD_DOMICILIO(
    o_id_vin out number,
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number,
    p_id_sede in varchar2,
    p_id_vin_real in number,
    P_ID_PROVINCIA in varchar2,
    P_ID_DEPARTAMENTO in number,
    P_ID_LOCALIDAD in number,
    P_BARRIO in varchar2,
    P_CALLE in varchar2,
    P_ALTURA in number,
    P_DEPTO in varchar2,
    P_PISO in varchar2,
    p_torre in varchar2,
    p_manzana in varchar2,
    p_lote in varchar2,
    p_id_tipocalle in number,
    p_km in varchar,
    p_id_calle in number,
    p_id_barrio in number
    )
  IS
   /*************************************************************
    Inserta o Actualiza un domicilio para una empresa en DOM_MANAGER
    No maneja transaccion, ya lo hace DOM_MANAGER
  *************************************************************/
    v_valid_parametros varchar2(500);
    v_TIPO_DOM  NUMBER;
    v_N_TIPO_DOM  VARCHAR2(150);
    v_bloque varchar(200);
    v_usuario varchar2(20);
  BEGIN
    if IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('HAB_LOG_GESTION') = '1' then
      IPJ.VARIOS.SP_GUARDAR_LOG(
        p_METODO => 'SP_GUARDAR_ENTIDAD_DOMICILIO',
        p_NIVEL => 'Parametros',
        p_ORIGEN => 'DB',
        p_USR_LOG  => 'DB',
        p_USR_WINDOWS => 'Sistema',
        p_IP_PC  => null,
        p_EXCEPCION =>
          'Id Tramite IPJ = ' || to_char(p_Id_Tramite_Ipj)
          || ' / Id Legajo = ' || to_char(p_id_legajo)
          || ' / Id Sede = ' || p_id_sede
          || ' / Id Vin Real = ' || to_char(p_id_vin_real)
          || ' / Id Prov = ' || P_ID_PROVINCIA
          || ' / Id Departamento = ' || to_char(P_ID_DEPARTAMENTO)
          || ' / Id Localidad = ' || to_char(P_ID_LOCALIDAD)
          || ' / Barrio = ' || P_BARRIO
          || ' / Calle = ' || P_CALLE
          || ' / Altura = ' || to_char(P_ALTURA)
          || ' / Depto = ' || P_DEPTO
          || ' / Piso = ' || P_PISO
          || ' / Torre = ' || p_torre
          || ' / Manzana = ' || p_manzana
          || ' / Lote = ' || p_lote
          || ' / Tipo Calle = ' || to_char(p_id_tipocalle)
          || ' / Km = ' || p_km
          || ' / Id Calle = ' || to_char(p_id_calle)
          || ' / Id Barrio = ' || to_char(p_id_barrio)
      );
    end if;

    -- Busco el responsable de la primer accion
    select cuil_usuario into v_usuario
    from ipj.t_tramitesipj_acciones
    where
      id_tramite_ipj = p_id_tramite_ipj and
      id_estado < 100 and
      rownum = 1;

    IPJ.VARIOS.SP_GUARDAR_DOMICILIO(
      o_id_vin => o_id_vin,
      o_rdo => o_rdo,
      o_tipo_mensaje => o_tipo_mensaje,
      p_id_vin => p_id_vin_real,
      p_usuario => v_usuario,
      p_Tipo_Domicilio => 0, -- 3 Real //  0 Sin Asignar
      p_id_integrante => 0,
      p_id_legajo => p_id_legajo,
      p_cuit_empresa => null,
      p_id_fondo_comercio => 0,
      p_id_entidad_acta => 0,
      p_es_comerciante => 'N',
      p_es_admin_entidad => 'N',
      P_ID_PROVINCIA => p_id_provincia,
      P_ID_DEPARTAMENTO => p_id_departamento,
      P_ID_LOCALIDAD => p_id_localidad,
      P_BARRIO =>  p_barrio,
      P_CALLE => p_calle,
      P_ALTURA => p_altura,
      P_DEPTO => p_depto,
      P_PISO => p_piso,
      p_torre => p_torre,
      p_manzana => p_manzana,
      p_lote => p_lote,
      p_id_barrio => p_id_barrio,
      p_id_calle => p_id_calle,
      p_id_tipocalle => p_id_tipocalle,
      p_km => p_km
    );

  EXCEPTION
     WHEN OTHERS THEN
        o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;

  END SP_GUARDAR_ENTIDAD_DOMICILIO;


  PROCEDURE SP_GUARDAR_ENTIDAD_ADMIN_DOM(
    o_id_vin out number,
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number,
    p_id_tipo_integrante in number,
    p_id_integrante in number,
    p_fecha_inicio in varchar2,
    p_id_vin in number,
    P_ID_PROVINCIA in varchar2,
    P_ID_DEPARTAMENTO in number,
    P_ID_LOCALIDAD in number,
    P_BARRIO in varchar2,
    P_CALLE in varchar2,
    P_ALTURA in number,
    P_DEPTO in varchar2,
    P_PISO in varchar2,
    p_torre in varchar2,
    p_manzana in varchar2,
    p_lote in varchar2,
    p_id_admin in number,
    p_cuil_empresa in varchar2,
    p_id_tipocalle in number,
    p_km in varchar2,
    p_id_calle in number,
    p_id_barrio in number)
  IS
    /*************************************************************
    Inserta o Actualiza un domicilio para una empresa en DOM_MANAGER
    No maneja transaccion, ya lo hace DOM_MANAGER
  *************************************************************/
    v_valid_parametros varchar2(500);
    v_TIPO_DOM  NUMBER;
    v_N_TIPO_DOM  VARCHAR2(150);
    v_bloque varchar(200);
    v_usuario varchar2(20);
  BEGIN

    if IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('HAB_LOG_GESTION') = '1' then
      IPJ.VARIOS.SP_GUARDAR_LOG(
        p_METODO => 'SP_GUARDAR_ENTIDAD_ADMIN_DOM',
        p_NIVEL => 'Parametros',
        p_ORIGEN => 'DB',
        p_USR_LOG  => 'DB',
        p_USR_WINDOWS => 'Sistema',
        p_IP_PC  => null,
        p_EXCEPCION =>
          'Id Tramite Ipj = ' || to_char(p_Id_Tramite_Ipj)
          || ' / Id Legajo = ' || to_char(p_id_legajo)
          || ' / Id Tipo Integrante = ' || to_char(p_id_tipo_integrante)
          || ' / Id Integrante = ' || to_char(p_id_integrante)
          || ' / Fecha Inicio = ' || p_fecha_inicio
          || ' / Id Vin = ' || to_char(p_id_vin)
          || ' / Id Provincia = ' || P_ID_PROVINCIA
          || ' / Id Departamento = ' || to_char(P_ID_DEPARTAMENTO)
          || ' / Id Localidad = ' || to_char(P_ID_LOCALIDAD)
          || ' / Barrio = ' || P_BARRIO
          || ' / Calle = ' || P_CALLE
          || ' / Altura = ' || to_char(P_ALTURA)
          || ' / Depto = ' || P_DEPTO
          || ' / Piso = ' || P_PISO
          || ' / Torre = ' || p_torre
          || ' / Manzana = ' || p_manzana
          || ' / Lote = ' || p_lote
          || ' / Id_Admin = ' || to_char(p_id_admin)
          || ' / Cuil Empresa = ' || p_cuil_empresa
          || ' / Tipo Calle = ' || to_char(p_id_tipocalle)
          || ' / Km = ' || p_km
          || ' / Id Calle = ' || to_char(p_id_calle)
          || ' / Id Barrio = ' || to_char(p_id_barrio)
      );
      commit;
    end if;
    -- Busco el responsable de la primer accion
    begin
      select cuil_usuario into v_usuario
      from ipj.t_tramitesipj_acciones
      where
        id_tramite_ipj = p_id_tramite_ipj and
        id_estado < 100 and
        rownum = 1;
    exception
      when NO_DATA_FOUND then
        select cuil_usuario into v_usuario
        from ipj.t_tramitesipj_acciones
        where
          id_tramite_ipj = p_id_tramite_ipj and
          rownum = 1;
    end;

    IPJ.VARIOS.SP_GUARDAR_DOMICILIO(
      o_id_vin => o_id_vin,
      o_rdo => o_rdo,
      o_tipo_mensaje => o_tipo_mensaje,
      p_id_vin => p_id_vin,
      p_usuario => v_usuario,
      p_Tipo_Domicilio => 3, -- 3 Real //  0 Sin Asignar
      p_id_integrante => (case when nvl(p_id_integrante, 0) = 0 then null else p_id_integrante end),
      p_id_legajo => 0,
      p_cuit_empresa => p_cuil_empresa,
      p_id_fondo_comercio => 0,
      p_id_entidad_acta => 0,
      p_es_comerciante => 'N',
      p_es_admin_entidad => 'S',
      P_ID_PROVINCIA => p_id_provincia,
      P_ID_DEPARTAMENTO => p_id_departamento,
      P_ID_LOCALIDAD => p_id_localidad,
      P_BARRIO => p_barrio,
      P_CALLE => p_calle,
      P_ALTURA => p_altura,
      P_DEPTO => p_depto,
      P_PISO => p_piso,
      p_torre => p_torre,
      p_manzana => p_manzana,
      p_lote => p_lote,
      p_id_barrio => p_id_barrio,
      p_id_calle => p_id_calle,
      p_id_tipocalle => p_id_tipocalle,
      p_km => p_km
    );

     -- Actualizo el ID_VIN en el integrate
    if Upper(o_rdo) like '%' || IPJ.TYPES.C_RESP_OK || '%' then
      update ipj.t_entidades_admin e
      set
        id_vin = decode(o_id_vin,0,NULL,o_id_vin)
      where
        Id_Tramite_Ipj= p_Id_Tramite_Ipj and
        id_legajo = p_id_legajo and
        id_admin = p_id_admin;
    end if;
  EXCEPTION
     WHEN OTHERS THEN
        o_rdo := 'SP_GUARDAR_ENTIDAD_ADMIN_DOM: ' || To_Char(SQLCODE) || '-' || SQLERRM;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
        o_id_vin := 0;
  END SP_GUARDAR_ENTIDAD_ADMIN_DOM;

  PROCEDURE SP_Traer_PersJur_Rubricas(
      p_Id_Tramite_Ipj in number,
      p_id_legajo in number,
      p_Cursor OUT TYPES.cursorType)
  IS
    v_ubicacion_leg number(6);
  BEGIN
  /*****************************************************************
  Lista las rubricas de una empresa
  ******************************************************************/
    begin
      select
        (select id_ubicacion from ipj.t_tipos_entidades te where te.id_tipo_entidad = l.id_tipo_entidad) ubicacion_leg
        into v_ubicacion_leg
      from ipj.t_legajos l
      where
        l.id_legajo = p_id_legajo;
    exception
      when NO_DATA_FOUND then
        v_ubicacion_leg := 0;
    end;

    --Muestro primero los ultimos libros de cada tipo, y luego los demas
    OPEN p_Cursor FOR
      select Id_Tramite_Ipj, id_tipo_libro, TIPO_LIBRO, nro_libro, titulo, fecha_tramite,
        FD_PS, id_juzgado, sentencia, fecha_sent, borrador,
        N_JUZGADO, id_legajo, observacion, Hojas_Desde, Hojas_Hasta, Es_Autorizacion,
        expediente, Es_Ultimo
      from (
        Select ER.BORRADOR, ER.FD_PS, to_char(ER.FECHA_SENT, 'dd/mm/rrrr') FECHA_SENT,
          to_char(ER.FECHA_TRAMITE, 'dd/mm/rrrr') FECHA_TRAMITE,
          ER.ID_JUZGADO, ER.ID_TIPO_LIBRO, ER.Id_Tramite_Ipj,
          ER.id_legajo, ER.NRO_LIBRO, ER.SENTENCIA, ER.TITULO ,
          1 orden, TL.TIPO_LIBRO, J.N_JUZGADO, er.observacion,
          er.Hojas_Desde, er.Hojas_Hasta, er.Es_Autorizacion, tr.expediente,
          'S' Es_Ultimo
        from IPJ.T_ENTIDADES_RUBRICA ER join IPJ.T_TIPOS_LIBROS tl
            on ER.ID_TIPO_LIBRO = tl.id_tipo_libro
          left join IPJ.T_JUZGADOS j
            on ER.ID_JUZGADO = J.id_JUZGADO
          join IPJ.t_tramitesipj tr
            on er.Id_Tramite_Ipj = tr.Id_Tramite_Ipj
          join
            ( select ID_TIPO_libro, max(nro_libro) Maximo
              from IPJ.T_ENTIDADES_RUBRICA er
              where
                er.id_legajo = p_id_legajo and
                nvl(Er.Es_Autorizacion, 'N') = 'N'
              group by id_tipo_libro
            ) erm
            on er.id_tipo_libro = erm.id_tipo_libro and er.nro_libro = erm.maximo
          where
            er.id_legajo = p_id_legajo and
            nvl(Er.Es_Autorizacion, 'N') = 'N' and
            --tr.id_ubicacion_origen = v_ubicacion_leg and
            --nvl(e.matricula_version, 0) <= v_matricula_version and
            TL.MECANIZADO = 'N'
      union
        Select ER.BORRADOR, ER.FD_PS, to_char(ER.FECHA_SENT, 'dd/mm/rrrr') FECHA_SENT,
          to_char(ER.FECHA_TRAMITE, 'dd/mm/rrrr') FECHA_TRAMITE,
          ER.ID_JUZGADO, ER.ID_TIPO_LIBRO, ER.Id_Tramite_Ipj,
          ER.id_legajo, ER.NRO_LIBRO, ER.SENTENCIA, ER.TITULO ,
           2 orden, TL.TIPO_LIBRO, J.N_JUZGADO, er.observacion,
           er.Hojas_Desde, er.Hojas_Hasta, er.Es_Autorizacion, tr.expediente,
           'N' Es_Ultimo
        from IPJ.T_ENTIDADES_RUBRICA ER join IPJ.T_TIPOS_LIBROS tl
            on ER.ID_TIPO_LIBRO = tl.id_tipo_libro
          left join IPJ.T_JUZGADOS j
            on ER.ID_JUZGADO = J.id_JUZGADO
          join IPJ.t_tramitesipj tr
            on er.Id_Tramite_Ipj = tr.Id_Tramite_Ipj
          join
            (select ID_TIPO_libro, max(nro_libro) Maximo
             from IPJ.T_ENTIDADES_RUBRICA er
              where
                er.id_legajo = p_id_legajo and
                nvl(Er.Es_Autorizacion, 'N') = 'N'
             group by id_tipo_libro
          ) erm
            on er.id_tipo_libro = erm.id_tipo_libro and er.nro_libro < erm.maximo
      where
        er.id_legajo = p_id_legajo and
        nvl(Er.Es_Autorizacion, 'N') = 'N' and
        --tr.id_ubicacion_origen = v_ubicacion_leg and
        TL.MECANIZADO = 'N'
      ) rub
      where
        borrador = 'N' or
        Id_Tramite_Ipj = p_Id_Tramite_Ipj
      order by orden asc, tipo_libro asc, nro_libro desc;
  END SP_Traer_PersJur_Rubricas;

  PROCEDURE SP_Traer_PersJur_Rub_Mecan(
      p_Id_Tramite_Ipj in number,
      p_id_legajo in number,
      p_Cursor OUT TYPES.cursorType)
  IS
    v_ubicacion_leg number(6);
  BEGIN
  /*****************************************************************
  Lista los medios mecanizados
  ******************************************************************/
    begin
      select  (select id_ubicacion from ipj.t_tipos_entidades te where te.id_tipo_entidad = l.id_tipo_entidad) ubicacion_leg
        into v_ubicacion_leg
      from ipj.t_legajos l
      where
        l.id_legajo = p_id_legajo;
    exception
      when NO_DATA_FOUND then
        v_ubicacion_leg := 0;
    end;

    --Muestro primero los ultimos libros de cada tipo, y luego los demas
    OPEN p_Cursor FOR
      select Id_Tramite_Ipj , id_tipo_libro, TIPO_LIBRO, nro_libro, titulo, fecha_tramite,
        FD_PS, id_juzgado, sentencia, fecha_sent, borrador,
        N_JUZGADO, id_legajo, observacion, Hojas_Desde, Hojas_Hasta, Es_Autorizacion,
        expediente, Es_Ultimo
      from (
        Select ER.BORRADOR, ER.FD_PS, to_char(ER.FECHA_SENT, 'dd/mm/rrrr') FECHA_SENT,
          to_char(ER.FECHA_TRAMITE, 'dd/mm/rrrr') FECHA_TRAMITE,
          ER.ID_JUZGADO, ER.ID_TIPO_LIBRO, ER.Id_Tramite_Ipj,
          ER.id_legajo, ER.NRO_LIBRO, ER.SENTENCIA, ER.TITULO ,
          1 orden, TL.TIPO_LIBRO, J.N_JUZGADO, er.observacion,
          er.Hojas_Desde, er.Hojas_Hasta, er.Es_Autorizacion, tr.expediente,
          'S' Es_Ultimo
        from IPJ.T_ENTIDADES_RUBRICA ER join IPJ.T_TIPOS_LIBROS tl
            on ER.ID_TIPO_LIBRO = tl.id_tipo_libro
          left join IPJ.T_JUZGADOS j
            on ER.ID_JUZGADO = J.id_JUZGADO
          join IPJ.t_tramitesipj tr
            on er.Id_Tramite_Ipj = tr.Id_Tramite_Ipj
          join
            ( select ID_TIPO_libro, max(nro_libro) Maximo
              from IPJ.T_ENTIDADES_RUBRICA er
              where
                er.id_legajo = p_id_legajo and
                nvl(Er.Es_Autorizacion, 'N') = 'N'
              group by id_tipo_libro
            ) erm
            on er.id_tipo_libro = erm.id_tipo_libro and er.nro_libro = erm.maximo
          where
            er.id_legajo = p_id_legajo and
            nvl(Er.Es_Autorizacion, 'N') = 'N' and
            --tr.id_ubicacion_origen = v_ubicacion_leg and
            --nvl(e.matricula_version, 0) <= v_matricula_version and
            TL.MECANIZADO = 'S'
      union
      Select ER.BORRADOR, ER.FD_PS, to_char(ER.FECHA_SENT, 'dd/mm/rrrr') FECHA_SENT,
          to_char(ER.FECHA_TRAMITE, 'dd/mm/rrrr') FECHA_TRAMITE,
          ER.ID_JUZGADO, ER.ID_TIPO_LIBRO, ER.Id_Tramite_Ipj,
          ER.id_legajo, ER.NRO_LIBRO, ER.SENTENCIA, ER.TITULO ,
           2 orden, TL.TIPO_LIBRO, J.N_JUZGADO, er.observacion,
           er.Hojas_Desde, er.Hojas_Hasta, er.Es_Autorizacion, tr.expediente,
           'N' Es_Ultimo
      from IPJ.T_ENTIDADES_RUBRICA ER join IPJ.T_TIPOS_LIBROS tl
            on ER.ID_TIPO_LIBRO = tl.id_tipo_libro
          left join IPJ.T_JUZGADOS j
            on ER.ID_JUZGADO = J.id_JUZGADO
          left join IPJ.t_tramitesipj tr
            on er.Id_Tramite_Ipj = tr.Id_Tramite_Ipj
          join
            (select ID_TIPO_libro, max(nro_libro) Maximo
             from IPJ.T_ENTIDADES_RUBRICA er
             where
                er.id_legajo = p_id_legajo and
                nvl(Er.Es_Autorizacion, 'N') = 'N'
             group by id_tipo_libro
          ) erm
            on er.id_tipo_libro = erm.id_tipo_libro and er.nro_libro < erm.maximo
      where
        er.id_legajo = p_id_legajo and
        nvl(Er.Es_Autorizacion, 'N') = 'N' and
        --tr.id_ubicacion_origen = v_ubicacion_leg and
        TL.MECANIZADO = 'S'
      ) rub
      where
        borrador = 'N' or Id_Tramite_Ipj = p_Id_Tramite_Ipj
      order by orden asc, tipo_libro asc, nro_libro desc;
  END SP_Traer_PersJur_Rub_Mecan;

  PROCEDURE SP_Traer_PersJur_Aut_Mecan(
      p_Id_Tramite_Ipj in number,
      p_id_legajo in number,
      p_Cursor OUT TYPES.cursorType)
  IS
    v_ubicacion_leg number(6);
  BEGIN
  /*****************************************************************
    Lista las autorizaciones de medios mecanizados
  ******************************************************************/
    begin
      select  (select id_ubicacion from ipj.t_tipos_entidades te where te.id_tipo_entidad = l.id_tipo_entidad) ubicacion_leg
        into v_ubicacion_leg
      from ipj.t_legajos l
      where
        l.id_legajo = p_id_legajo;
    exception
      when NO_DATA_FOUND then
        v_ubicacion_leg := 0;
    end;

    --Muestro las autorizaciones de medios
    OPEN p_Cursor FOR
      Select ER.BORRADOR, ER.FD_PS, to_char(ER.FECHA_SENT, 'dd/mm/rrrr') FECHA_SENT,
        to_char(ER.FECHA_TRAMITE, 'dd/mm/rrrr') FECHA_TRAMITE,
        ER.ID_JUZGADO, ER.ID_TIPO_LIBRO, ER.Id_Tramite_Ipj,
        ER.id_legajo, ER.NRO_LIBRO, ER.SENTENCIA, ER.TITULO ,
        1 orden, TL.TIPO_LIBRO, J.N_JUZGADO, er.observacion,
        er.Hojas_Desde, er.Hojas_Hasta, er.Es_Autorizacion, tr.expediente
      from IPJ.T_ENTIDADES_RUBRICA ER join IPJ.T_TIPOS_LIBROS tl
          on ER.ID_TIPO_LIBRO = tl.id_tipo_libro
        left join IPJ.T_JUZGADOS j
          on ER.ID_JUZGADO = J.id_JUZGADO
        join IPJ.t_tramitesipj tr
          on er.Id_Tramite_Ipj = tr.Id_Tramite_Ipj
      where
        er.id_legajo = p_id_legajo and
        nvl(Er.Es_Autorizacion, 'N') = 'S' and
        tr.id_ubicacion_origen = v_ubicacion_leg and
        TL.MECANIZADO = 'S' and
        (borrador = 'N' or er.Id_Tramite_Ipj = p_Id_Tramite_Ipj)
      order by orden asc, id_tipo_libro asc;

  END SP_Traer_PersJur_Aut_Mecan;


PROCEDURE SP_Traer_PersJur_Rubricas_NO(
      p_Id_Tramite_Ipj in number,
      p_id_legajo in number,
      p_Cursor OUT TYPES.cursorType)
  IS
    v_matricula_version number(6,0);
    v_ubicacion_ent number(6);
    v_ubicacion_leg number(6);
  BEGIN
  /*****************************************************************
  Lista los libros de rubricas no utilizados por una Entidad
  ******************************************************************/
    begin
      select nvl(matricula_version, 0),
        (select id_ubicacion from ipj.t_tipos_entidades te where te.id_tipo_entidad = e.id_tipo_entidad) ubicacion_ent,
        (select id_ubicacion from ipj.t_tipos_entidades te where te.id_tipo_entidad = l.id_tipo_entidad) ubicacion_leg
        into v_matricula_version, v_ubicacion_ent, v_ubicacion_leg
      from IPJ.t_entidades e join ipj.t_legajos l
        on e.id_legajo = l.id_legajo
      where
        e.Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        e.id_legajo = p_id_legajo;
    exception
      when NO_DATA_FOUND then
        v_matricula_version := 0;
    end;

    OPEN p_Cursor FOR
      select T.ID_TIPO_LIBRO, T.TIPO_LIBRO, '' Titulo
      from IPJ.t_tipos_libros t
      where
        id_tipo_libro not in
          (select id_tipo_libro
           from IPJ.t_entidades_rubrica er join ipj.t_entidades e
             on er.Id_Tramite_Ipj = e.Id_Tramite_Ipj and er.id_legajo = e.id_legajo
           where
             e.id_legajo = p_id_legajo and
             e.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_ubicacion_leg)
           );

  END SP_Traer_PersJur_Rubricas_NO;


  PROCEDURE SP_GUARDAR_ENTIDAD_RUBRICAS(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number,
    p_id_tipo_libro in number,
    p_nro_libro in number,
    p_titulo in varchar2,
    p_fecha_tramite in varchar2, --es DATE
    p_Fs_Ps in varchar2,
    p_id_juzgado in number,
    p_sentencia in varchar2,
    p_fecha_sent in varchar2, --es DATE
    p_observacion in varchar2,
    p_eliminar in number, -- 1 eliminar
    p_nro_desde in number,
    p_nro_hasta in number,
    p_hojas_desde in number,
    p_hojas_hasta in number,
    p_es_autorizacion in char
  )
  IS
    /*************************************************************
      Guarda las rubricas y medios mecanizados de una entidad
      No maneja transaccion, ya que se usa en cascada desde la aplicacion y lo administra el codigo.
  *************************************************************/
    v_valid_parametros varchar2(500);
    v_row_libro IPJ.t_tipos_libros%ROWTYPE;
    v_Cant_Mayores number;
    v_id_tramite_ipj number;
    v_expediente varchar2(100);
    v_id_estado number;
  BEGIN
    if IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('HAB_LOG_GESTION') = '1' then
      IPJ.VARIOS.SP_GUARDAR_LOG(
        p_METODO => 'SP_GUARDAR_ENTIDAD_RUBRICAS',
        p_NIVEL => 'Parametros',
        p_ORIGEN => 'DB',
        p_USR_LOG  => 'DB',
        p_USR_WINDOWS => 'Sistema',
        p_IP_PC  => null,
        p_EXCEPCION =>
          'Id Tramite Ipj = ' || to_char(p_Id_Tramite_Ipj )
          || ' / Id Legajo = ' || to_char(p_id_legajo)
          || ' / Id Tipo Libro = ' || to_char(p_id_tipo_libro)
          || ' / Nro Libro = ' || to_char(p_nro_libro)
          || ' / Titulo = ' || p_titulo
          || ' / Fecha Tramite = ' || p_fecha_tramite
          || ' / Fs Ps = ' || p_Fs_Ps
          || ' / Id Juzgado = ' || to_char(p_id_juzgado)
          || ' / Sentencia = ' || p_sentencia
          || ' / Fecha Sent = ' || p_fecha_sent
          || ' / Obs = ' || p_observacion
          || ' / Eliminr = ' || to_char(p_eliminar)
          || ' / Nro Desde = ' || to_char(p_nro_desde)
          || ' / Nro Hasta = ' || to_char(p_nro_hasta )
          || ' / Hojas Desde = ' || to_char(p_hojas_desde)
          || ' / Hojas Hasta = ' || to_char(p_hojas_hasta)
          || ' / Es Autorizacion = ' || p_es_autorizacion
        );
    end if;

  -- VALIDACION DE PARAMETROS
    if p_id_tipo_libro is null then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('LIBRO_NOT');
    else
      select * into v_row_libro
      from IPJ.T_TIPOS_LIBROS
      where
        id_tipo_libro = p_id_tipo_libro;

      -- Control para r�bricas de Libros
      if nvl(p_eliminar, 0) <> 1 and v_row_libro.mecanizado = 'N' then
        -- Debe tener un nro o rango de numeros
        if nvl(p_nro_libro, 0) = 0 and nvl(p_nro_desde, 0) = 0 and nvl(p_nro_hasta, 0) = 0 then
          v_valid_parametros := v_valid_parametros || 'Las R�bricas requieren un Nro de Libro o un Rango de N�meros. ';
        else
          -- Si viene un rango, debe estar bien
          if nvl(p_nro_desde, 0) > nvl(p_nro_hasta, 0) then
            v_valid_parametros := v_valid_parametros || 'Rango de n�mero de libros inv�lido. ';
          end if;
        end if;
      end if;

      --Si necesita T�tulo, debe venir con algo
      if nvl(v_row_libro.Habilita_Titulo, 'N') = 'S' and p_titulo is null then
        v_valid_parametros := v_valid_parametros || 'Este libro requiere un t�tulo.';
      end if;
      if p_fecha_tramite is not null and IPJ.VARIOS.Valida_Fecha(p_fecha_tramite) = 'N' then
        v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT') || '(Fecha Tramite)';
      end if;
      if p_fecha_sent is not null and IPJ.VARIOS.Valida_Fecha(p_fecha_sent) = 'N' then
        v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT') || '(Fecha Sentencia)';
      end if;
    end if;


    if v_valid_parametros is not null then
      o_rdo := 'GUARDAR RUBRICA - Parametros: '  || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

   /*******************************************************
      CUERPO PROCEDIMIENTO
   *******************************************************/
    if p_eliminar = 1 then
      delete ipj.t_entidades_rubrica
      where
        Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        id_legajo = p_id_legajo and
        id_tipo_libro = p_id_tipo_libro and
        nro_libro = p_nro_libro and
        upper(trim(titulo)) = upper(trim(p_titulo)) and
        nvl(es_autorizacion, 'N') = nvl(p_es_autorizacion, 'N') and
        borrador = 'S';
    else
      -- Busco si el libro existe en otro tr�mite, entonces lo informo y no dejo modificarlo
      begin
        select distinct tr.id_tramite_ipj, tr.expediente, tr.id_estado_ult
          into v_id_tramite_ipj, v_expediente, v_id_estado
        from ipj.t_entidades_rubrica er join ipj.t_tramitesipj tr
          on er.id_tramite_ipj = tr.id_tramite_ipj
        where
          id_legajo = p_id_legajo and
          id_tipo_libro = p_id_tipo_libro and
          nro_libro = p_nro_libro and
          upper(trim(titulo)) = upper(trim(p_titulo)) and
          nvl(es_autorizacion, 'N') = nvl(p_es_autorizacion, 'N');
      exception
        when NO_DATA_FOUND then
          v_id_tramite_ipj := 0;
          v_expediente := null;
      end;

      -- Si se cargo en otro t�mite y esta pendiente, lo aviso y no modifico nada
      if v_id_tramite_ipj >0 and v_id_tramite_ipj <> p_id_tramite_ipj then
        -- Si el tr�mite esta pendiente, aviso que se encuentra cargado en ese tr�mite.
        if v_id_estado < IPJ.TYPES.C_ESTADOS_COMPLETADO then
          o_rdo := 'El libro Nro. ' || to_char(p_nro_libro) || ' se carg� en el expediente ' || v_expediente || ' que se encuentra en estudio.';
          o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
          return;
        end if;
      else
        -- Si existe un registro igual no hago nada
        update ipj.t_entidades_rubrica
        set
          fecha_tramite = to_date(p_fecha_tramite, 'dd/mm/rrrr'),
          fd_ps = p_Fs_Ps,
          observacion = p_observacion,
          id_juzgado = p_id_juzgado,
          titulo = p_titulo,
          sentencia = p_sentencia,
          fecha_sent = to_date(p_fecha_sent, 'dd/mm/rrrr'),
          hojas_desde = p_hojas_Desde,
          hojas_hasta = p_hojas_hasta
        where
          id_legajo = p_id_legajo and
          id_tipo_libro = p_id_tipo_libro and
          nro_libro = p_nro_libro and
          upper(trim(titulo)) = upper(trim(p_titulo)) and
          nvl(es_autorizacion, 'N') = nvl(p_es_autorizacion, 'N');

        if sql%rowcount = 0 then
          if v_row_libro.mecanizado = 'N' then
            --Se pueden insertar libros mayores al numero maximo (no controla saltos)
            select count(1) into v_Cant_Mayores
            from ipj.t_entidades_rubrica
            where
              id_legajo = p_id_legajo and
              id_tipo_libro = p_id_tipo_libro and
              upper(trim(titulo)) = upper(trim(p_titulo)) and
              nro_libro > GREATEST (nvl(p_nro_libro, 0), nvl(p_nro_desde, 0)) and
              nvl(es_autorizacion, 'N') = nvl(p_es_autorizacion, 'N');

            if nvl(v_Cant_Mayores, 0) > 0 then
              o_rdo :=  'No es posible cargar el libro ' || p_titulo || ', existe otro igual o mayor. ';
              o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
              return;
            end if;
          end if;

          for v_nro in GREATEST(nvl(p_nro_libro, 0), nvl(p_nro_desde, 0)) .. GREATEST(nvl(p_nro_libro, 0), nvl(p_nro_hasta, 0)) LOOP

          BEGIN
            insert into ipj.t_entidades_rubrica
              (Id_Tramite_Ipj, id_tipo_libro, nro_libro, titulo, fecha_tramite, fd_ps, id_juzgado,
               sentencia, fecha_sent, borrador, id_legajo, observacion, hojas_Desde,
               hojas_hasta, es_autorizacion)
            values
              (p_Id_Tramite_Ipj, p_id_tipo_libro, v_nro, upper(trim(p_titulo)),
               to_date(p_fecha_tramite, 'dd/mm/rrrr'), p_Fs_Ps, p_id_juzgado, p_sentencia,
               to_date(p_fecha_sent, 'dd/mm/rrrr'), 'S', p_id_legajo, p_observacion,
               p_hojas_desde, p_hojas_hasta, p_es_autorizacion);

          EXCEPTION WHEN OTHERS THEN
              o_rdo := 'Esta intentando agregar un libro que ya existe.';
              o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
              return;
          END;
          end loop;
        end if;
      end if;
    end if;

    /*  Ya no se valida como antes
    if 0 = IPJ.VARIOS.VALIDA_LIBRO_ENTIDAD(p_id_legajo, p_Id_Tramite_Ipj, p_id_tipo_libro) then
      o_rdo := IPJ.TYPES.C_RESP_OK;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    else
      o_rdo := IPJ.VARIOS.MENSAJE_ERROR('RUBRICA_NOT');
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
    end if;
    */

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     WHEN OTHERS THEN
       o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_GUARDAR_ENTIDAD_RUBRICAS;

  PROCEDURE SP_Validar_PersJur_InstLegal(
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number,
    p_id_juzgado in number,
    p_il_numero in number,
    p_rdo out number)
  IS
    v_res number;
  BEGIN
  /*******************************************************
    Valida que no exsita otro tramite con el mismo INS_LEGAL que se piensa agregar
  ********************************************************/
    begin
      select max(eil.Id_Tramite_Ipj) into v_res
      from ipj.t_entidades_ins_legal eil join ipj.t_tramitesipj_acciones tacc
          on tacc.Id_Tramite_Ipj = eil.Id_Tramite_Ipj and tacc.id_legajo = eil.id_legajo
      where
        EIL.Id_Tramite_Ipj <> p_Id_Tramite_Ipj and
        eil.id_legajo = p_id_legajo and
        eil.id_juzgado = p_id_juzgado and
        eil.il_numero = p_il_numero and
        tacc.id_estado < IPJ.TYPES.C_ESTADOS_RECHAZADO;
    exception
      when OTHERS then v_res := 0;
    end;

    p_rdo :=  NVL(v_res, 0);
  EXCEPTION
    when NO_DATA_FOUND then v_res := 0;
    when OTHERS then v_res := 0;
    p_rdo :=  v_res;
  END SP_Validar_PersJur_InstLegal;

  PROCEDURE SP_Validar_PersJur_Actas(
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number,
    p_id_Tipo_Acta in number,
    p_fec_acta in varchar2,
    p_rdo out number)
  IS
    v_res number;
  BEGIN
  /*******************************************************
    Valida que no exsita otro tramite con la misma Acta que se piensa agregar
  ********************************************************/
    begin
      select max(ea.Id_Tramite_Ipj) into v_res
      from ipj.t_entidades_acta ea join ipj.t_tramitesipj_acciones tacc
          on tacc.Id_Tramite_Ipj = ea.Id_Tramite_Ipj and tacc.id_legajo = ea.id_legajo
      where
        ea.Id_Tramite_Ipj <> p_Id_Tramite_Ipj and
        ea.id_legajo = p_id_legajo and
        tacc.id_estado < IPJ.TYPES.C_ESTADOS_RECHAZADO and
        EA.ID_TIPO_ACTA = p_id_tipo_acta and
        ea.fec_acta = to_date(p_fec_acta, 'dd/mm/rrrr');
    exception
      when OTHERS then v_res := 0;
    end;

    p_rdo :=  NVL(v_res, 0);
  EXCEPTION
    when NO_DATA_FOUND then v_res := 0;
    when OTHERS then v_res := 0;
    p_rdo :=  v_res;
  END SP_Validar_PersJur_Actas;

  PROCEDURE SP_Validar_PersJur_Admin(
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number,
    p_id_Tipo_Integrante in number,
    p_fecha_inicio in varchar2,
    p_id_sexo in varchar2,
    p_nro_documento in varchar2,
    p_pai_cod_pais in varchar2,
    p_rdo out number)
  IS
    v_res number;
    v_id_integrante number;
  BEGIN
  /*******************************************************
    Valida que no exsita otro tramite con el mismo Administrador que se piensa agregar
  ********************************************************/
    begin
      select id_integrante into v_id_integrante
      from ipj.t_integrantes
      where
        id_sexo = p_id_sexo and
        nro_documento = p_nro_documento and
        pai_cod_pais = p_pai_cod_pais;
    exception
      when OTHERS then
        v_id_integrante := 0;
    end;

    begin
      select max(ea.Id_Tramite_Ipj) into v_res
      from ipj.t_entidades_admin ea join ipj.t_tramitesipj_acciones tacc
          on tacc.Id_Tramite_Ipj = ea.Id_Tramite_Ipj and tacc.id_legajo = ea.id_legajo
      where
        ea.Id_Tramite_Ipj <> p_Id_Tramite_Ipj and
        ea.id_legajo = p_id_legajo and
        EA.id_integrante = v_id_integrante and
        tacc.id_estado < IPJ.TYPES.C_ESTADOS_RECHAZADO and
        ea.fecha_inicio = to_date(p_fecha_inicio, 'dd/mm/rrrr');
    exception
      when OTHERS then
        v_res := 0;
    end;

    p_rdo :=  NVL(v_res, 0);
  EXCEPTION
    when OTHERS then
      v_res := 0;
      p_rdo :=  v_res;
  END SP_Validar_PersJur_Admin;

  PROCEDURE SP_Validar_PersJur_Socios(
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number,
    p_id_Tipo_Integrante in number,
    p_id_sexo in varchar2,
    p_nro_documento in varchar2,
    p_pai_cod_pais in varchar2,
    p_rdo out number)
  IS
    v_res number;
    v_id_integrante number;
    v_id_ubicacion_legajo number;
  BEGIN
  /*******************************************************
    Valida que no exsita otro tramite con el mismo Socio que se piensa agregar.
    En ese caso se informa el ID de Tramite
  ********************************************************/
    begin
      select id_integrante into v_id_integrante
      from ipj.t_integrantes
      where
        id_sexo = p_id_sexo and
        nro_documento = p_nro_documento and
        pai_cod_pais = p_pai_cod_pais;
    exception
        when OTHERS then v_id_integrante := 0;
    end;

    -- busco el �rea atual del legajo
    select id_ubicacion into v_id_ubicacion_legajo
    from ipj.t_legajos l join ipj.t_tipos_entidades t
        on l.id_tipo_entidad = t.id_tipo_entidad
    where
      l.id_legajo = p_id_legajo;

    begin
      select max(es.Id_Tramite_Ipj) into v_res
      from ipj.t_entidades_socios es join ipj.t_tramitesipj_acciones tacc
          on tacc.Id_Tramite_Ipj = ES.Id_Tramite_Ipj and tacc.id_legajo = es.id_legajo
        join ipj.t_entidades e
          on es.id_tramite_ipj = e.id_tramite_ipj and es.id_legajo = e.id_legajo
      where
        es.Id_Tramite_Ipj <> p_Id_Tramite_Ipj and
        es.id_legajo = p_id_legajo and
        Es.id_integrante = v_id_integrante and
        TACC.ID_ESTADO < IPJ.TYPES.C_ESTADOS_RECHAZADO and
        e.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades te where te.id_ubicacion = v_id_ubicacion_legajo) -- Para ocultar transformaciones
        ;
    exception
      when OTHERS then v_res := 0;
    end;

    p_rdo :=  NVL(v_res, 0);
  EXCEPTION
    when OTHERS then
      v_res := 0;
      p_rdo :=  v_res;
  END SP_Validar_PersJur_Socios;

  PROCEDURE SP_Validar_PersJur_Rubrica(
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number,
    p_id_Tipo_Libro in number,
    p_Nro_Libro in number,
    p_rdo out number)
  IS
    v_res number;
  BEGIN
  /*******************************************************
    Valida que no exsita otro tramite con la misma Rubrica que se piensa agregar
  ********************************************************/
    begin
      select max(er.Id_Tramite_Ipj) into v_res
      from ipj.t_entidades_rubrica er join ipj.t_tramitesipj_acciones tacc
          on tacc.Id_Tramite_Ipj = Er.Id_Tramite_Ipj and tacc.id_legajo = er.id_legajo
      where
        er.Id_Tramite_Ipj <> p_Id_Tramite_Ipj and
        er.id_legajo = p_id_legajo and
        er.id_tipo_libro = p_id_tipo_libro and
        er.nro_libro = p_nro_libro and
        tacc.id_estado < IPJ.TYPES.C_ESTADOS_RECHAZADO;
    exception
      when OTHERS then v_res := 0;
    end;

    p_rdo :=  NVL(v_res, 0);
  EXCEPTION
    when NO_DATA_FOUND then v_res := 0;
    when OTHERS then v_res := 0;
    p_rdo :=  v_res;
  END SP_Validar_PersJur_Rubrica;

  PROCEDURE SP_Validar_PersJur_Rubros(
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number,
    p_id_rubro in varchar2,
    p_id_tipo_actividad in varchar2,
    p_id_actividad in varchar2,
    p_rdo out number)
  IS
    v_res number;
  BEGIN
  /*******************************************************
    Valida que no exsita otro tramite con el mismo Rubro que se piensa agregar
  ********************************************************/
    begin
      select max(er.Id_Tramite_Ipj) into v_res
      from ipj.t_entidades_rubros er join ipj.t_tramitesipj_acciones tacc
          on tacc.Id_Tramite_Ipj = ER.Id_Tramite_Ipj and tacc.id_legajo = er.id_legajo
      where
        er.Id_Tramite_Ipj <> p_Id_Tramite_Ipj and
        er.id_legajo = p_id_legajo and
        er.id_rubro = p_id_rubro and
        er.id_tipo_actividad = p_id_tipo_actividad and
        er.id_actividad = p_id_actividad and
        tacc.id_estado < IPJ.TYPES.C_ESTADOS_RECHAZADO;
    exception
      when OTHERS then v_res := 0;
    end;

    p_rdo :=  NVL(v_res, 0);
  EXCEPTION
    when NO_DATA_FOUND then v_res := 0;
    when OTHERS then v_res := 0;
    p_rdo :=  v_res;
  END SP_Validar_PersJur_Rubros;

  PROCEDURE SP_Hist_PersJur_Rubros(
      o_Cursor OUT TYPES.cursorType,
      p_Id_Tramite_Ipj in number,
      p_id_legajo in number )
  IS
    v_matricula_version number(6, 0);
  BEGIN
 /*****************************************************************
  Este procedimiento devuelve un listado de rubros y actividades asociados a una entidad de un tramite
  ******************************************************************/
    begin
      select nvl(matricula_version, 0) into v_matricula_version
      from IPJ.t_entidades
      where
        Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        id_legajo = p_id_legajo;
    exception
      when NO_DATA_FOUND then
        v_matricula_version := 0;
    end;

    OPEN o_Cursor FOR
      select
        to_char(r.fecha_desde, 'dd/mm/rrrr') fecha_desde,
        to_char(r.fecha_hasta, 'dd/mm/rrrr') fecha_hasta,
        min(e.matricula_version) matricula_version,
        r.id_rubro, R.ID_TIPO_ACTIVIDAD, R.ID_ACTIVIDAD,
        (select N_RUBRO from T_COMUNES.VT_ACTIVIDADES  where id_actividad = R.ID_ACTIVIDAD) N_RUBRO,
        '' n_tipo_actividad,
        (select N_ACTIVIDAD from T_COMUNES.VT_ACTIVIDADES  where id_actividad = R.ID_ACTIVIDAD) N_ACTIVIDAD,
        IPJ.TRAMITES.FC_FECHA_TRAMITE(p_id_legajo, min(E.MATRICULA_VERSION)) fecha_inicio,
        IPJ.TRAMITES.FC_USR_ACCION_TRAMITE(p_id_legajo, min(E.MATRICULA_VERSION)) descripcion
      from ipj.t_entidades_rubros r  join IPJ.t_entidades e
          on r.Id_Tramite_Ipj = e.Id_Tramite_Ipj and r.id_legajo = e.id_legajo
        join ipj.t_tramitesipj t
          on t.Id_Tramite_Ipj = e.Id_Tramite_Ipj
      where
        e.id_legajo = p_id_legajo and
        e.Id_Tramite_Ipj <> p_Id_Tramite_Ipj and
        e.matricula_version <= v_matricula_version and
        r.borrador = 'N' and
        nvl(r.fecha_hasta, sysdate) < sysdate and
        T.ID_ESTADO_ULT < IPJ.TYPES.C_ESTADOS_RECHAZADO
     group by r.fecha_desde, r.fecha_hasta, r.id_rubro, R.ID_TIPO_ACTIVIDAD, r.ID_ACTIVIDAD
     order by matricula_version desc;
  END SP_Hist_PersJur_Rubros;

  PROCEDURE SP_Hist_PersJur_Acciones(
      o_Cursor OUT TYPES.cursorType,
      p_Id_Tramite_Ipj in number,
      p_id_legajo in number )
  IS
    v_matricula_version number(6, 0);
    v_ubicacion_ent number;
    v_ubicacion_leg number;
  BEGIN
 /*****************************************************************
  Este procedimiento devuelve un listado de acciones dadas de baja, asociadas a una entidad de un tramite
  ******************************************************************/
    -- busto la ultima area y su version, para visualizar los datos correctos
    begin
      select nvl(matricula_version, 0),
        (select id_ubicacion from ipj.t_tipos_entidades te where te.id_tipo_entidad = e.id_tipo_entidad) ubicacion_ent,
        (select id_ubicacion from ipj.t_tipos_entidades te where te.id_tipo_entidad = l.id_tipo_entidad) ubicacion_leg
        into v_matricula_version, v_ubicacion_ent, v_ubicacion_leg
      from IPJ.t_entidades e join ipj.t_legajos l
        on e.id_legajo = l.id_legajo
      where
        e.Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        e.id_legajo = p_id_legajo;
    exception
      when NO_DATA_FOUND then
        v_matricula_version := 0;
    end;

    OPEN o_Cursor FOR
      select EA.ID_LEGAJO, EA.Id_Tramite_Ipj, EA.ID_ENTIDADES_ACCION, EA.ID_ACCION, TA.N_ACCION, EA.ID_FORMA_ACCION, TFA.N_FORMA_ACCION,
        EA.CANT_VOTOS, EA.CLASE, EA.BORRADOR, EA.SIN_CLASE
      from IPJ.T_ENTIDADES_ACCIONES ea join IPJ.T_TIPOS_ACCIONES ta
          on EA.ID_ACCION = TA.ID_ACCION
        join IPJ.T_TIPOS_FORMAS_ACCION tfa
          on EA.ID_FORMA_ACCION = TFA.ID_FORMA_ACCION
        join ipj.t_entidades tpj
          on tpj.id_tramite_ipj = ea.id_tramite_ipj and tpj.id_legajo = ea.id_legajo
      where
        EA.ID_LEGAJO = p_id_legajo and
        tpj.matricula_version <= v_matricula_version and
        tpj.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_ubicacion_leg) and
        ea.borrador = 'N'  and
        ea.fecha_baja < to_date(sysdate, 'dd/mm/rrrr')
      order by EA.CLASE;

  END SP_Hist_PersJur_Acciones;


  PROCEDURE SP_Hist_PersJur_Int_Socios(
    o_Cursor OUT TYPES.cursorType,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number)
  IS
    v_matricula_version number(6, 0);
    v_ubicacion_ent number(6);
    v_ubicacion_leg number(6);
  BEGIN
  /*********************************************************
  Este procemiento devuelve el listado de integrantes asociados a una entidad de un tramite,
  cuyo tipo sea igual a SOCIO
  **********************************************************/
    begin
      select nvl(matricula_version, 0),
        (select id_ubicacion from ipj.t_tipos_entidades te where te.id_tipo_entidad = e.id_tipo_entidad) ubicacion_ent,
        (select id_ubicacion from ipj.t_tipos_entidades te where te.id_tipo_entidad = l.id_tipo_entidad) ubicacion_leg
        into v_matricula_version, v_ubicacion_ent, v_ubicacion_leg
      from IPJ.t_entidades e join ipj.t_legajos l
        on e.id_legajo = l.id_legajo
      where
        e.Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        e.id_legajo = p_id_legajo;
    exception
      when NO_DATA_FOUND then
        v_matricula_version := 0;
    end;

    OPEN o_Cursor FOR
      select
        ie.cuota, to_char(ie.fecha_inicio, 'dd/mm/rrrr') fecha_inicio,
        to_char(ie.fecha_fin, 'dd/mm/rrrr') fecha_fin,
        min(E.MATRICULA_VERSION) MATRICULA_VERSION,
        i.id_integrante, I.ID_NUMERO, I.ID_SEXO, I.NRO_DOCUMENTO, I.PAI_COD_PAIS,
        ( select  TRIM(p.NOV_NOMBRE || ' ' ||p.NOV_APELLIDO) from rcivil.vt_pk_persona p
          where i.id_sexo = p.ID_SEXO and i.nro_documento = p.NRO_DOCUMENTO and i.pai_cod_pais = p.PAI_COD_PAIS and i.id_numero = p.ID_NUMERO)  detalle,
        IPJ.TRAMITES.FC_FECHA_TRAMITE(p_id_legajo, min(E.MATRICULA_VERSION)) Fecha_Acto,
        IPJ.TRAMITES.FC_USR_ACCION_TRAMITE(p_id_legajo, min(E.MATRICULA_VERSION)) descripcion
      from ipj.t_entidades_socios ie join ipj.t_integrantes i
          on ie.id_integrante = i.id_integrante
        join ipj.t_Entidades e
          on ie.Id_Tramite_Ipj = e.Id_Tramite_Ipj and ie.id_legajo = e.id_legajo
       join IPJ.t_tramitesipj t
          on ie.Id_Tramite_Ipj = t.Id_Tramite_Ipj
      where
        e.id_legajo = p_id_legajo and
        e.Id_Tramite_Ipj <> p_Id_Tramite_Ipj and
        e.matricula_version <= v_matricula_version and
        e.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_ubicacion_leg) and
        ie.borrador = 'N'  and
        nvl(ie.fecha_fin, sysdate) < sysdate and
        T.ID_ESTADO_ULT < IPJ.TYPES.C_ESTADOS_RECHAZADO
      group by ie.cuota, ie.fecha_inicio, ie.fecha_fin, i.id_integrante, I.ID_NUMERO, I.ID_SEXO, I.NRO_DOCUMENTO, I.PAI_COD_PAIS
      order by matricula_version desc;

  END SP_Hist_PersJur_Int_Socios;


  PROCEDURE SP_Hist_PersJur_Int_Socios_SxA(
    o_Cursor OUT TYPES.cursorType,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number)
  IS
    v_matricula_version number(6, 0);
    v_ubicacion_ent number(6);
    v_ubicacion_leg number(6);
  BEGIN
  /*********************************************************
  Este procemiento devuelve el historial de evoluci�n del capital social
  **********************************************************/
    begin
      select nvl(matricula_version, 0),
        (select id_ubicacion from ipj.t_tipos_entidades te where te.id_tipo_entidad = e.id_tipo_entidad) ubicacion_ent,
        (select id_ubicacion from ipj.t_tipos_entidades te where te.id_tipo_entidad = l.id_tipo_entidad) ubicacion_leg
        into v_matricula_version, v_ubicacion_ent, v_ubicacion_leg
      from IPJ.t_entidades e join ipj.t_legajos l
        on e.id_legajo = l.id_legajo
      where
        e.Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        e.id_legajo = p_id_legajo;
    exception
      when NO_DATA_FOUND then
        v_matricula_version := 0;
    end;

    OPEN o_Cursor FOR
      select
        trim(To_Char(nvl(e.monto, '0'), '99999999999999999990.99'))  monto, E.CUOTAS cuota,
        trim(To_Char(nvl(E.VALOR_CUOTA, '0'), '99999999999999999990.9999'))  VALOR_CUOTA,
        min(E.MATRICULA_VERSION) MATRICULA_VERSION,
        IPJ.TRAMITES.FC_FECHA_TRAMITE(p_id_legajo, min(E.MATRICULA_VERSION)) Fecha_Acto,
        IPJ.TRAMITES.FC_USR_ACCION_TRAMITE(p_id_legajo, min(E.MATRICULA_VERSION)) descripcion
      from ipj.t_Entidades e join IPJ.t_tramitesipj t
          on e.Id_Tramite_Ipj = t.Id_Tramite_Ipj
      where
        e.id_legajo = p_id_legajo and
        e.Id_Tramite_Ipj <> p_Id_Tramite_Ipj and
        e.matricula_version <= v_matricula_version and
        e.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_ubicacion_leg) and
        e.borrador = 'N'  and
        T.ID_ESTADO_ULT < IPJ.TYPES.C_ESTADOS_RECHAZADO
      group by e.monto, E.CUOTAS, E.VALOR_CUOTA
      order by matricula_version desc;

  END SP_Hist_PersJur_Int_Socios_SxA;

  PROCEDURE SP_Hist_PersJur_Int_Admin(
    o_Cursor OUT TYPES.cursorType,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number)
  IS
    v_matricula_version number(6, 0);
    v_ubicacion_ent number(6);
    v_ubicacion_leg number(6);
    v_dias_vigencia number;
  BEGIN
    /***************************************************************
      Este procemiento devuelve el listado de integrantes asociados a una entidad de un tramite,
      cuyo tipo sea distinto de SOCIO
    ***************************************************************/
    begin
      select nvl(matricula_version, 0),
        (select id_ubicacion from ipj.t_tipos_entidades te where te.id_tipo_entidad = e.id_tipo_entidad) ubicacion_ent,
        (select id_ubicacion from ipj.t_tipos_entidades te where te.id_tipo_entidad = l.id_tipo_entidad) ubicacion_leg
        into v_matricula_version, v_ubicacion_ent, v_ubicacion_leg
      from IPJ.t_entidades e join ipj.t_legajos l
        on e.id_legajo = l.id_legajo
      where
        e.Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        e.id_legajo = p_id_legajo;
    exception
      when NO_DATA_FOUND then
        v_matricula_version := 0;
    end;

    -- (PBI 9917) Busco los d�as de vigencia configurados
    v_dias_vigencia := to_number(IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('DIAS_ADMIN_LEY'));

    OPEN o_Cursor FOR
      select
        ie.id_tipo_integrante, to_char(ie.fecha_inicio, 'dd/mm/rrrr') fecha_inicio,
        to_char(ie.fecha_fin, 'dd/mm/rrrr') fecha_fin,
        min(E.MATRICULA_VERSION) MATRICULA_VERSION,
        i.id_integrante, I.ID_NUMERO, I.ID_SEXO, I.NRO_DOCUMENTO, I.PAI_COD_PAIS,
        (select n_tipo_integrante from ipj.t_tipos_integrante ti where ie.id_tipo_integrante = ti.id_tipo_integrante) n_tipo_integrante,
        (select n_tipo_organismo from IPJ.T_Tipos_Organismo tor where ie.id_tipo_organismo = tor.id_tipo_organismo) n_tipo_organismo,
        (select nro_orden from ipj.t_tipos_integrante ti where ie.id_tipo_integrante = ti.id_tipo_integrante) nro_orden_int,
        (select nro_orden from IPJ.T_Tipos_Organismo tor where ie.id_tipo_organismo = tor.id_tipo_organismo) nro_orden_org,
        nvl(ie.n_empresa,
          (select  TRIM(p.NOV_NOMBRE || ' ' ||p.NOV_APELLIDO) from rcivil.vt_pk_persona p
           where i.id_sexo = p.ID_SEXO and i.nro_documento = p.NRO_DOCUMENTO and i.pai_cod_pais = p.PAI_COD_PAIS and i.id_numero = p.ID_NUMERO)
         ) detalle,
        IPJ.TRAMITES.FC_FECHA_TRAMITE(p_id_legajo, min(E.MATRICULA_VERSION)) Fecha_Acto,
        IPJ.TRAMITES.FC_USR_ACCION_TRAMITE(p_id_legajo, min(E.MATRICULA_VERSION)) descripcion,
        ie.id_motivo_baja, mb.n_motivo_baja
      from ipj.t_entidades_admin ie left join ipj.t_integrantes i
          on ie.id_integrante = i.id_integrante
        join ipj.t_Entidades e
          on ie.Id_Tramite_Ipj = e.Id_Tramite_Ipj and ie.id_legajo = e.id_legajo
        join IPJ.t_tramitesipj t
          on ie.Id_Tramite_Ipj = t.Id_Tramite_Ipj
        join IPJ.t_tipos_integrante ti
          on ie.id_tipo_integrante = ti.id_tipo_integrante
        left join ipj.t_tipos_organismo org
          on org.id_tipo_organismo = ie.id_tipo_organismo
        left join ipj.T_Tipos_Motivos_Baja mb
          on ie.id_motivo_baja = mb.id_motivo_baja
    where
      e.id_legajo = p_id_legajo and
      e.Id_Tramite_Ipj <> p_Id_Tramite_Ipj and
      e.matricula_version <= v_matricula_version and
      e.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_ubicacion_leg) and
      ie.borrador = 'N'  and
      (nvl(ie.fecha_fin, sysdate) + v_dias_vigencia) < sysdate and
      T.ID_ESTADO_ULT < IPJ.TYPES.C_ESTADOS_RECHAZADO
    group by ie.id_tipo_organismo, ie.id_tipo_integrante, ie.fecha_inicio, ie.fecha_fin,
      i.id_integrante, I.ID_NUMERO, I.ID_SEXO, I.NRO_DOCUMENTO, I.PAI_COD_PAIS,
      ie.id_motivo_baja, mb.n_motivo_baja, ie.n_empresa
    order by matricula_version desc, nro_orden_org asc, nro_orden_int asc, detalle asc;

  END SP_Hist_PersJur_Int_Admin;


  PROCEDURE SP_Hist_PersJur_Razon_Social(
    o_Cursor OUT TYPES.cursorType,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number)
  IS
    v_matricula_version number(6, 0);
    v_ubicacion_ent number(6);
    v_ubicacion_leg number(6);
  BEGIN

    begin
      select nvl(matricula_version, 0),
        (select id_ubicacion from ipj.t_tipos_entidades te where te.id_tipo_entidad = e.id_tipo_entidad) ubicacion_ent,
        (select id_ubicacion from ipj.t_tipos_entidades te where te.id_tipo_entidad = l.id_tipo_entidad) ubicacion_leg
        into v_matricula_version, v_ubicacion_ent, v_ubicacion_leg
      from IPJ.t_entidades e join ipj.t_legajos l
        on e.id_legajo = l.id_legajo
      where
        e.Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        e.id_legajo = p_id_legajo;
    exception
      when NO_DATA_FOUND then
        v_matricula_version := 0;
    end;

    OPEN o_Cursor FOR
      select E.DENOMINACION_1, min(E.MATRICULA_VERSION) MATRICULA_VERSION,
        IPJ.TRAMITES.FC_FECHA_TRAMITE(p_id_legajo, min(E.MATRICULA_VERSION)) fecha_inicio,
        IPJ.TRAMITES.FC_USR_ACCION_TRAMITE(p_id_legajo, min(E.MATRICULA_VERSION)) descripcion
      from IPJ.T_ENTIDADES e join IPJ.t_tramitesipj t
          on e.Id_Tramite_Ipj = t.Id_Tramite_Ipj
      where
        e.id_legajo = p_id_legajo and
        e.Id_Tramite_Ipj <> p_Id_Tramite_Ipj and
        e.matricula_version <= v_matricula_version and
        e.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_ubicacion_leg) and
        e.borrador = 'N' and
        T.ID_ESTADO_ULT < IPJ.TYPES.C_ESTADOS_RECHAZADO
      group by E.DENOMINACION_1
      order by matricula_version desc;

  END SP_Hist_PersJur_Razon_Social;

  PROCEDURE SP_Hist_PersJur_Vigencia(
    o_Cursor OUT TYPES.cursorType,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number)
  IS
    v_matricula_version number(6, 0);
    v_ubicacion_ent number(6);
    v_ubicacion_leg number(6);
  BEGIN
    begin
      select nvl(matricula_version, 0),
        (select id_ubicacion from ipj.t_tipos_entidades te where te.id_tipo_entidad = e.id_tipo_entidad) ubicacion_ent,
        (select id_ubicacion from ipj.t_tipos_entidades te where te.id_tipo_entidad = l.id_tipo_entidad) ubicacion_leg
        into v_matricula_version, v_ubicacion_ent, v_ubicacion_leg
      from IPJ.t_entidades e join ipj.t_legajos l
        on e.id_legajo = l.id_legajo
      where
        e.Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        e.id_legajo = p_id_legajo;
    exception
      when NO_DATA_FOUND then
        v_matricula_version := 0;
    end;

    OPEN o_Cursor FOR
      select E.VIGENCIA, E.TIPO_VIGENCIA, min(E.MATRICULA_VERSION) MATRICULA_VERSION,
        IPJ.TRAMITES.FC_FECHA_TRAMITE(p_id_legajo, min(E.MATRICULA_VERSION)) fecha_inicio,
        IPJ.TRAMITES.FC_USR_ACCION_TRAMITE(p_id_legajo, min(E.MATRICULA_VERSION)) descripcion,
         (case E.TIPO_VIGENCIA
            when 0 then 'Fecha Inscripci�n'
            when 1 then 'Acta Constitutiva'
            when 2 then 'Otra Fecha'
            else ''
          end) N_Vigencia,
        IPJ.ENTIDAD_PERSJUR.FC_Fecha_Vigencia_Entidad(p_id_legajo,  min(E.MATRICULA_VERSION), e.tipo_vigencia) FEC_VIG
      from IPJ.T_ENTIDADES e join IPJ.t_tramitesipj t
          on e.Id_Tramite_Ipj = t.Id_Tramite_Ipj
      where
        e.id_legajo = p_id_legajo and
        e.Id_Tramite_Ipj <> p_Id_Tramite_Ipj and
        e.matricula_version <= v_matricula_version and
        e.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_ubicacion_leg) and
        e.borrador = 'N' and
        T.ID_ESTADO_ULT < IPJ.TYPES.C_ESTADOS_RECHAZADO
      group by E.VIGENCIA, E.TIPO_VIGENCIA
      order by matricula_version desc;

  END SP_Hist_PersJur_Vigencia;

  PROCEDURE SP_Hist_PersJur_Domicilio(
    o_Cursor OUT TYPES.cursorType,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number)
  IS
    v_matricula_version number(6, 0);
    v_ubicacion_ent number(6);
    v_ubicacion_leg number(6);
  BEGIN
    begin
      select nvl(matricula_version, 0),
        (select id_ubicacion from ipj.t_tipos_entidades te where te.id_tipo_entidad = e.id_tipo_entidad) ubicacion_ent,
        (select id_ubicacion from ipj.t_tipos_entidades te where te.id_tipo_entidad = l.id_tipo_entidad) ubicacion_leg
        into v_matricula_version, v_ubicacion_ent, v_ubicacion_leg
      from IPJ.t_entidades e join ipj.t_legajos l
        on e.id_legajo = l.id_legajo
      where
        e.Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        e.id_legajo = p_id_legajo;
    exception
      when NO_DATA_FOUND then
        v_matricula_version := 0;
    end;

    OPEN o_Cursor FOR
      select
        d.id_vin, min(e.matricula_version) matricula_version,
        min(d.id_tipodom) id_tipodom, min(d.n_tipodom) n_tipodom, min(d.id_tipocalle) id_tipocalle,
        min(d.n_tipocalle) n_tipocalle, min(d.id_calle) id_calle, min(d.n_calle) n_calle,
        min(d.altura) altura, min(d.depto) depto, min(d.piso) piso, min(d.torre) torre,
        min(d.id_barrio) id_barrio, min(d.n_barrio) n_barrio, min(D.mzna) Manzana, min(D.Lote) lote,
        min(d.id_localidad) id_localidad, min(d.n_localidad) n_localidad, min(d.id_departamento) id_departamento,
        min(d.n_departamento) n_departamento, min(d.id_provincia) id_provincia,
        min(d.n_provincia) n_provincia, min(d.cpa) cpa,
        IPJ.TRAMITES.FC_FECHA_TRAMITE(p_id_legajo, min(E.MATRICULA_VERSION)) fecha_inicio,
        IPJ.TRAMITES.FC_USR_ACCION_TRAMITE(p_id_legajo, min(E.MATRICULA_VERSION)) descripcion
      from ipj.t_entidades e  join dom_manager.VT_DOMICILIOS_COND d
            on d.id_vin = e.id_vin_real
          join IPJ.t_tramitesipj t
            on e.Id_Tramite_Ipj = t.Id_Tramite_Ipj
          join IPJ.T_TRAMITESIPJ_ACCIONES ac
            on ac.Id_Tramite_Ipj = t.Id_Tramite_Ipj and ac.id_legajo = e.id_legajo
          join ipj.t_usuarios u
            on U.CUIL_USUARIO = AC.CUIL_USUARIO
      where
        e.id_legajo = p_id_legajo and
        e.Id_Tramite_Ipj <> p_Id_Tramite_Ipj and
        e.matricula_version <= v_matricula_version and
        e.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_ubicacion_leg) and
        e.borrador = 'N' and
        T.ID_ESTADO_ULT < IPJ.TYPES.C_ESTADOS_RECHAZADO
      group by d.id_vin
      order by matricula_version desc;

  END SP_Hist_PersJur_Domicilio;

  PROCEDURE SP_GUARDAR_GRAVAMENES(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in number,
    p_id_tramiteipj_accion  in number,
    p_id_tipo_accion  in number,
    p_tipo_persona  in number,
    p_id_integrante  in number,
    p_id_legajo in number,
    p_fec_inscripcion in varchar2,
    p_matricula  in varchar2,
    p_matricula_version  in number,
    p_folio in varchar2,
    p_tomo in varchar2,
    p_anio in number,
    p_protocolo in varchar2,
    p_letra in varchar2
  )
  IS
    v_mensaje varchar2(200);
    v_valid_parametros varchar2(500);
    v_matricula_exists varchar2(500);
    v_id_tramiteipj_accion number;
    v_ID_INTEGRANTE NUMBER;
    v_id_legajo NUMBER;
     v_id_estado_accion number;
     v_id_usuario_accion varchar2(20);
     v_id_pagina_accion number(6);
  BEGIN
  /********************************************************
    Guarda los datos de un Gravamen relacionado a una accion.
    Si el gravamen ya existe, los actualiza, en caso contrario los ingresa
    No maneja transaccion, ya que se usa en cascada desde la aplicacion y lo administra el codigo.
  *********************************************************/
    if IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('HAB_LOG_GRAVAMEN') = '1' then
      IPJ.VARIOS.SP_GUARDAR_LOG(
        p_METODO => 'SP_GUARDAR_GRAVAMENES',
        p_NIVEL => 'Gravamenes',
        p_ORIGEN => 'DB',
        p_USR_LOG  => 'DB',
        p_USR_WINDOWS => 'Sistema',
        p_IP_PC  => null,
        p_EXCEPCION =>
          'Id_TramiteIPJ = ' || to_char(p_Id_Tramite_Ipj)
          || ' / Id TramitesIPJ Accion = ' || to_char(p_ID_TRAMITEIPJ_ACCION)
          || ' / Id Tipo Accion = ' || to_char(p_ID_TIPO_ACCION)
          || ' / Tipo Persona = ' || to_char(p_TIPO_PERSONA)
          || ' / Id Integrante = ' || to_char(p_ID_INTEGRANTE)
          || ' / Id Legajo = ' || to_char(p_id_legajo)
          || ' / Fec Inscrip. = ' || p_FEC_INSCRIPCION
          || ' / Matricula = ' || p_MATRICULA
          || ' / Matricula Versi�n = ' || to_char(p_MATRICULA_VERSION)
          || ' / Folio = ' || p_FOLIO
          || ' / Tomo = ' || p_TOMO
          || ' / A�o = ' || to_char(p_ANIO)
          || ' / Protocolo = ' || p_PROTOCOLO
          || ' / Letra = ' || p_letra
      );
    end if;

    --  VALIDACION DE PARAMETROS
    if p_FEC_INSCRIPCION is not null and IPJ.VARIOS.Valida_Fecha(p_FEC_INSCRIPCION) = 'N' or To_Date(p_FEC_INSCRIPCION, 'dd/mm/rrrr') > sysdate then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT')  || ' (Fecha Inscripci�n)';
    end if;
    if IPJ.VARIOS.TONUMBER(p_anio) is null then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('A�O_NOT');
    end if;

    if p_matricula is not null and p_letra is not null then
      IPJ.VARIOS.SP_Validar_Matricula(
         o_rdo => v_matricula_exists,
         o_tipo_mensaje => o_tipo_mensaje,
         p_Id_Tramite_Ipj => p_Id_Tramite_Ipj,
         p_id_legajo => p_id_legajo,
         p_id_tramiteipj_accion => p_id_tramiteipj_accion,
         p_cuit => null,
         p_matricula => upper(p_letra) || p_matricula);

       if v_matricula_exists != IPJ.TYPES.C_RESP_OK then
         v_valid_parametros :=  v_valid_parametros || v_matricula_exists;
       end if;
    end if;

    if nvl(p_id_legajo, 0) = 0 and nvl(p_id_integrante, 0) = 0then
       v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('PERS_NOT');
    end if;

    -- Si los paramentros no estan OK, no continuo
    if v_valid_parametros is not null then
      o_rdo := 'Par�metros : ' || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    --  CUERPO DEL PROCEDIMIETNO

    -- Controlos los ID, si son cero pongo NULL para respetar las FK
    v_id_integrante := (case when p_id_integrante = 0 then null else p_id_integrante end);
    v_id_legajo := (case when p_id_legajo = 0 then null else p_id_legajo end);

    -- Actualizo el registro (si existe)
    update ipj.t_gravamenes
    set
      tipo_persona = p_TIPO_PERSONA,
      id_integrante = v_ID_INTEGRANTE,
      id_legajo = v_id_legajo,
      fec_inscripcion = To_Date(p_FEC_INSCRIPCION, 'dd/mm/rrrr'),
      matricula = p_letra || p_matricula,
      folio = p_FOLIO,
      tomo = p_TOMO,
      a�o = p_ANIO,
      protocolo = p_PROTOCOLO
    where
      Id_Tramite_Ipj = p_Id_Tramite_Ipj and
      id_tramiteipj_accion = p_ID_TRAMITEIPJ_ACCION and
      id_tipo_accion = p_ID_TIPO_ACCION;

    -- Si no se actualizo nada, lo inserto.
    -- NO DEBERIA OCURRIR, VER SI INSERTO O MUESTRO ERROR
    if sql%rowcount = 0 then
      insert into ipj.t_gravamenes
        ( Id_Tramite_Ipj, ID_TRAMITEIPJ_ACCION, ID_TIPO_ACCION,
          TIPO_PERSONA, ID_INTEGRANTE, id_legajo, FEC_INSCRIPCION,
          MATRICULA, MATRICULA_VERSION, FOLIO, TOMO, A�O, PROTOCOLO)
      values
        (p_Id_Tramite_Ipj, p_ID_TRAMITEIPJ_ACCION, p_ID_TIPO_ACCION,
          p_TIPO_PERSONA, v_ID_INTEGRANTE, v_id_legajo, to_date(p_FEC_INSCRIPCION, 'dd/mm/rrrr'),
          p_letra || p_matricula, p_MATRICULA_VERSION, p_FOLIO, p_TOMO, p_ANIO, p_PROTOCOLO);
    end if;

    select max(id_tramiteipj_accion) into v_id_tramiteipj_accion
    from IPJ.T_TramitesIPJ_Acciones t join IPJ.T_TIPOS_ACCIONESIPJ ta
      on T.ID_TIPO_ACCION = TA.ID_TIPO_ACCION
    where
      Id_Tramite_Ipj = p_Id_Tramite_Ipj and
      id_tramiteipj_accion = p_ID_TRAMITEIPJ_ACCION and
      TA.ID_PROTOCOLO in(5, 7);

    IPJ.VARIOS.SP_Actualizar_Protocolo(
      o_rdo => o_rdo,
      o_tipo_mensaje => o_tipo_mensaje,
      p_Id_Tramite_Ipj => p_Id_Tramite_Ipj,
      p_id_legajo => p_id_legajo,
      p_id_tramiteipj_accion => v_id_tramiteipj_accion ,
      p_matricula => upper(p_letra) || p_matricula);

    if o_rdo != IPJ.TYPES.C_RESP_OK then
      o_rdo := 'SP_Actualizar_Protocolo ' || o_rdo;
      return;
    end if;

    --Una vez creada la entidad, marco la accion como EN PROCESO
    --Si la accion estaba PENDIENTE, ASIGNADO u OBSERVADO, lo marco como EN PROCESO
    select id_estado, cuil_usuario, ta.id_pagina into v_id_estado_accion, v_id_usuario_accion, v_id_pagina_accion
    from ipj.t_tramitesipj_acciones t join ipj.t_tipos_accionesipj ta
      on t.id_tipo_accion = ta.id_tipo_accion
    where
      Id_Tramite_Ipj = p_Id_Tramite_Ipj and
      id_tramiteipj_accion = p_id_tramiteipj_accion;

    if v_id_estado_accion in (IPJ.TYPES.c_Estados_Archivo_Inicial, IPJ.TYPES.c_Estados_Asignado, Ipj.Types.C_Estados_Observado) then
      -- Actualizo todas del mismo Usuario, Pagina, Estado y Persona (fisica o jur�dica)
      update ipj.t_tramitesipj_acciones
      set id_estado = IPJ.TYPES.C_ESTADOS_PROCESO
      where
          Id_Tramite_Ipj = p_Id_Tramite_Ipj and
          cuil_usuario = v_id_usuario_accion and
          nvl(id_legajo, 0) = nvl(p_id_legajo, 0) and
          nvl(id_integrante, 0) = nvl(p_id_integrante, 0) and
          id_estado = v_id_estado_accion and
          id_tipo_accion in (select a.id_tipo_accion
                                    from IPJ.t_tipos_accionesipj a join ipj.t_paginas p
                                      on a.id_pagina = p.id_pagina
                                    where
                                       a.id_pagina = v_id_pagina_accion and p.agrupable = 1);

      -- Actualizo el estado del tramite, si estaba observado
      update ipj.t_tramitesipj
      set id_estado_ult = IPJ.TYPES.c_Estados_Asignado
      where
        id_tramite_ipj = p_Id_Tramite_Ipj and
        id_estado_ult = Ipj.Types.C_Estados_Observado;

    end if;

    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     WHEN OTHERS THEN
       o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_GUARDAR_GRAVAMENES;

  PROCEDURE SP_Traer_Gravamenes(
      o_Cursor OUT TYPES.cursorType,
      o_rdo out varchar,
      p_Id_Tramite_Ipj in number,
      p_id_tramiteipj_accion in number,
      p_id_tipo_accion in number
      )
  IS
    v_existe_gravamen number;
    v_Tipo_Persona number;
    v_id_legajo number;
    v_id_integrante number;
    v_cuit varchar2(11);
    v_razon_social IPJ.T_LEGAJOS.denominacion_sia%TYPE;
    v_id_vin number;
    v_id_tipocalle number(2);
    v_tipo_calle varchar2(50);
    v_id_calle number(10);
    v_n_calle varchar2(200);
    v_altura varchar2(200);
    v_depto varchar2(200);
    v_piso varchar2(200);
    v_torre varchar2(20);
    v_id_barrio number(10);
    v_n_barrio varchar2(200);
    v_n_localidad varchar2(200);
    v_n_departamento varchar2(200);
    v_manzana varchar2(6);
    v_lote varchar2(6);
    v_km varchar2(10);
    v_N_Sexo varchar2(100);
    v_id_sexo varchar2(3);
    v_N_Integrante varchar2(200);
    v_Nro_Documento varchar2(20);
    v_ID_PROVINCIA varchar2(20);
    v_ID_DEPARTAMENTO number;
    v_ID_LOCALIDAD number;
    v_Row_Gravamen IPJ.t_Gravamenes%ROWTYPE;
    v_hay_protocolo number;
    v_entidad varchar2(30);
  BEGIN
  /**************************************************
     Este procedimiento busca un Gravamen
     Si no existe una gravamen asociada al tramite, devuelve un registro solo con
     los datos de la Persona.
     Si falla iforma el error
  ****************************************************/
     -- busco si la accion usa protocolo, para saber si se incrementa o no la matricula y su versionado
    select nvl(AC.ID_PROTOCOLO, 0) into v_hay_protocolo
    from IPJ.T_TIPOS_ACCIONESIPJ ac
    where id_tipo_accion = p_id_tipo_accion;

    begin
      select case nvl(id_integrante, 0) when 0 then 1 else 2 end case, id_integrante, id_legajo
        into v_Tipo_Persona, v_id_integrante, v_id_legajo
      from IPJ.T_TRAMITESIPJ_ACCIONES tAcc
      where
        Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        id_tramiteipj_accion = p_id_tramiteipj_accion and
        id_tipo_accion = p_id_tipo_accion;
    exception
       when NO_DATA_FOUND then
         v_id_integrante := 0;
         v_id_legajo := 0;
    end;

    --15868:[SG] - [Libros Digitales SAS] - Ficha Grav�menes - Mostrar el CUIT de la entidad si lo tuviese
    BEGIN
      SELECT l.cuit
        INTO v_cuit
        FROM ipj.t_legajos l
       WHERE l.id_legajo = v_id_legajo;
    EXCEPTION
      WHEN no_data_found THEN
        v_cuit := NULL;
    END;

    if nvl(v_id_integrante, 0) = 0 and nvl(v_id_legajo, 0) = 0 then
      o_rdo := IPJ.VARIOS.MENSAJE_ERROR('PERS_NOT');
      return;
    end if;

    if v_Tipo_Persona = 2 then -- Persona Fisica
      -- busco el integrante
      begin
        select I.DETALLE, S.TIPO_SEXO, s.id_sexo, I.NRO_DOCUMENTO, i.id_vin
          into v_N_Integrante, v_N_Sexo, v_id_sexo, v_nro_documento, v_id_vin
        from IPJ.T_INTEGRANTES i join T_COMUNES.VT_SEXOS s
            on i.id_sexo = S.ID_SEXO
        where
          i.id_integrante = v_id_integrante;

      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          v_N_Integrante := '';
          v_N_Sexo := '';
          v_nro_documento := '';
          v_id_sexo := 0;
          v_id_vin := 0;
      end;
       --Busco los datos de su direccion
      begin
         v_entidad :=  IPJ.VARIOS.FC_Generar_Entidad_Dom (
            p_id_integrante => v_id_integrante,
            p_id_legajo => 0,
            p_cuit_empresa => null,
            p_id_fondo_comercio => 0,
            p_id_entidad_acta => 0,
            p_es_comerciante => 'N',
            p_es_admin_entidad => 'N',
            p_id_admin => null);

        select d.n_calle, d.altura, d.depto, d.piso,  d.n_barrio, d.n_localidad, d.n_departamento,
          d.id_provincia, d.id_departamento, d.id_localidad, d.torre, d.mzna, d.lote, d.km, d.n_tipocalle,
          d.id_tipocalle, d.id_barrio, d.id_calle
          into v_n_calle, v_altura, v_depto, v_piso, v_n_barrio, v_n_localidad, v_n_departamento,
          v_ID_PROVINCIA, v_ID_DEPARTAMENTO, v_ID_LOCALIDAD, v_torre, v_manzana, v_lote, v_km, v_tipo_calle,
          v_id_tipocalle, v_id_barrio, v_id_calle
        from dom_manager.VT_DOMICILIOS_COND d
        where
           id_vin = v_id_vin;

      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          v_n_calle := '';
          v_altura := '';
          v_depto := '';
          v_piso := '';
          v_n_barrio := '';
          v_n_localidad := '';
          v_n_departamento := '';
          v_ID_PROVINCIA := Null;
          v_ID_DEPARTAMENTO := null;
          v_ID_LOCALIDAD := null;
          v_torre := '';
          v_manzana := null;
          v_lote := null;
          v_km := null;
          v_tipo_calle := null;
      end;

    else -- ES UNA PERSONA JURIDICA
      -- busco la ultima version de esa entidad
      begin
        select denominacion_1, id_vin_real into v_razon_social, v_id_vin
        from IPJ.T_ENTIDADES e
        where
          e.id_legajo = v_id_legajo and
          (e.borrador = 'N' or e.Id_Tramite_Ipj = p_Id_Tramite_Ipj) and
          rownum = 1
        order by Id_Tramite_Ipj desc;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          select Denominacion_Sia into v_razon_social
          from IPJ.T_Legajos l
          where L.id_legajo = v_id_legajo;

          v_id_vin := 0;
      end;

      --Busco los datos de su direccion
      begin
        v_entidad :=  IPJ.VARIOS.FC_Generar_Entidad_Dom (
            p_id_integrante => 0,
            p_id_legajo => v_id_legajo,
            p_cuit_empresa => null,
            p_id_fondo_comercio => 0,
            p_id_entidad_acta => 0,
            p_es_comerciante => 'N',
            p_es_admin_entidad => 'N',
            p_id_admin => null);

        select d.n_calle, d.altura, d.depto, d.piso,  d.n_barrio, d.n_localidad, d.n_departamento,
          d.id_provincia, d.id_departamento, d.id_localidad, d.torre, d.mzna, d.lote, d.km, d.n_tipocalle,
          d.id_tipocalle, d.id_barrio, d.id_calle
          into v_n_calle, v_altura, v_depto, v_piso, v_n_barrio, v_n_localidad, v_n_departamento,
          v_id_provincia, v_id_departamento, v_id_localidad, v_torre, v_manzana, v_lote, v_km, v_tipo_calle,
          v_id_tipocalle, v_id_barrio, v_id_calle
        from dom_manager.VT_DOMICILIOS_COND d
        where
           id_vin = v_id_vin;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          v_n_calle := '';
          v_altura := '';
          v_depto := '';
          v_piso := '';
          v_n_barrio := '';
          v_n_localidad := '';
          v_n_departamento := '';
          v_ID_PROVINCIA := Null;
          v_ID_DEPARTAMENTO := null;
          v_ID_LOCALIDAD := null;
          v_torre := '';
          v_manzana := null;
          v_lote := null;
          v_km := null;
          v_tipo_calle := null;
      end;
    end if;

    --Valido que exista alguna entidad, si no hay devuelvo error y un registro vacio (no null)
    select count(*) into v_existe_gravamen
    from ipj.t_gravamenes g
    where
      g.Id_Tramite_Ipj = p_Id_Tramite_Ipj and
      g.id_tramiteipj_accion = p_id_tramiteipj_accion and
      g.id_tipo_accion = p_id_tipo_accion;

    if v_existe_gravamen = 0 then
      -- Busco el ultimo gravamen de la persona
      begin
        select * into v_Row_Gravamen
        from
          ( select g.*
            from ipj.t_gravamenes g left join IPJ.T_Legajos L
              on G.id_legajo = L.id_legajo
            where
              ( (nvl(v_id_legajo, 0) <> 0 and L.id_legajo = v_id_legajo) or
                (nvl(v_id_integrante, 0) != 0 and g.id_integrante = v_id_integrante)
              )
            order by G.MATRICULA, G.MATRICULA_VERSION desc
           ) t
         where
           rownum = 1;
      exception
        when NO_DATA_FOUND then
          v_Row_Gravamen.fec_inscripcion := null;
          v_Row_Gravamen.matricula:= null;
          v_Row_Gravamen.matricula_version := null;
          v_Row_Gravamen.folio := null;
          v_Row_Gravamen.tomo := null;
          v_Row_Gravamen.a�o := null;
          v_Row_Gravamen.protocolo := null;
      end;

      -- veo si es una persona fisica o juridica
      OPEN o_Cursor FOR
      select v_Tipo_Persona tipo_persona, v_id_integrante id_integrante, v_id_legajo id_legajo,
        to_char(v_Row_Gravamen.fec_inscripcion, 'dd/mm/rrrr') fec_inscripcion,
        IPJ.VARIOS.FC_Numero_Matricula(v_Row_Gravamen.matricula) MATRICULA,
        IPJ.VARIOS.FC_Letra_Matricula(v_Row_Gravamen.matricula) Letra,
        (case when v_hay_protocolo = 0 then nvl(v_Row_Gravamen.matricula_version, 0)
                  else (case when v_Row_Gravamen.matricula_version is null then 0 else  v_Row_Gravamen.matricula_version +1 end)
         end) MATRICULA_VERSION,
        v_Row_Gravamen.folio folio, v_Row_Gravamen.tomo tomo, v_Row_Gravamen.a�o ano,
        v_Row_Gravamen.protocolo protocolo,  v_cuit cuit, v_razon_social denominacion_1,
        IPJ.VARIOS.FC_ARMAR_CALLE_DOM(v_n_calle, v_altura, v_piso, v_depto, v_torre, v_manzana, v_lote, v_n_barrio, v_km, v_tipo_calle) calle_inf,
        null id_tipodom, null n_tipodom, v_id_tipocalle id_tipocalle, v_tipo_calle n_tipocalle, v_id_calle id_calle,
        initcap(v_n_calle) n_calle, v_altura altura, v_depto depto, v_piso piso,  v_torre torre,
        v_id_barrio id_barrio, initcap(v_n_barrio) n_barrio, initcap(v_n_localidad) n_localidad,
        initcap(v_n_departamento) n_departamento, v_km km, v_manzana manzana, v_lote lote,
        null n_provincia, null cpa, v_id_vin id_vin_real, v_ID_PROVINCIA ID_PROVINCIA,
        v_ID_DEPARTAMENTO ID_DEPARTAMENTO, v_ID_LOCALIDAD ID_LOCALIDAD,
        v_N_Integrante Detalle,  v_N_Sexo Tipo_Sexo, v_id_sexo id_sexo,
        v_nro_documento Nro_Documento
      from dual;
      return;
    else
      OPEN o_Cursor FOR
      select g.tipo_persona, g.id_integrante, g.id_legajo,
        to_char(g.fec_inscripcion, 'dd/mm/rrrr') fec_inscripcion, nvl(G.MATRICULA_VERSION, 0) MATRICULA_VERSION,
        IPJ.VARIOS.FC_Numero_Matricula(g.matricula) MATRICULA,
        IPJ.VARIOS.FC_Letra_Matricula(g.matricula) Letra,
        g.folio, g.tomo, g.a�o ano, g.protocolo,
        v_cuit cuit, v_razon_social denominacion_1,
        null id_tipodom, null n_tipodom, v_id_tipocalle id_tipocalle, v_tipo_calle n_tipocalle, v_id_calle id_calle,
        IPJ.VARIOS.FC_ARMAR_CALLE_DOM(v_n_calle, v_altura, v_piso, v_depto, v_torre, v_manzana, v_lote, v_n_barrio, v_km, v_tipo_calle) calle_inf,
        initcap(v_n_calle) n_calle, v_altura altura, v_depto depto, v_piso piso,  v_torre torre,
        v_id_barrio id_barrio, initcap(v_n_barrio) n_barrio, initcap(v_n_localidad) n_localidad,
        initcap(v_n_departamento) n_departamento, v_km km, v_manzana manzana, v_lote lote,
        null n_provincia, null cpa, v_id_vin id_vin_real, v_ID_PROVINCIA ID_PROVINCIA,
        v_ID_DEPARTAMENTO ID_DEPARTAMENTO, v_ID_LOCALIDAD ID_LOCALIDAD,
        v_N_Integrante Detalle,  v_N_Sexo Tipo_Sexo, v_id_sexo id_sexo,
        v_nro_documento Nro_Documento
      from ipj.t_gravamenes g
      where
        g.Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        g.id_tramiteipj_accion = p_id_tramiteipj_accion and
        g.id_tipo_accion = p_id_tipo_accion;
    end if;

    if o_rdo is null then
      o_rdo := 'OK';
    end if;
  END SP_Traer_Gravamenes;

  PROCEDURE SP_GUARDAR_GRAVAMEN_INS_LEGAL(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in NUMBER,
    p_ID_TRAMITEIPJ_ACCION in NUMBER,
    p_ID_TIPO_ACCION in NUMBER,
    p_ID_JUZGADO in NUMBER,
    p_IL_TIPO in NUMBER,
    p_IL_NUMERO in NUMBER,
    p_ID_TIPO_GRAVAMEN in NUMBER,
    p_IL_FECHA in varchar2,
    p_TITULO in VARCHAR2,
    p_DESCRIPCION in VARCHAR2,
    p_FECHA in varchar2,
    p_eliminar in number)
  IS
    v_valid_parametros varchar2(500);
    v_id_legajo number;
    v_Hay_Libros_Dig number;
    v_informar_mail char(1);
  BEGIN
    /*************************************************************
      Guarda los instrumentos legales asociados a un gravamen de un tramite.
      No maneja transaccion, ya que se usa en cascada desde la aplicacion y lo administra el codigo.
  *************************************************************/
  -- VALIDACION DE PARAMETROS
    if IPJ.VARIOS.Valida_Fecha(p_il_fecha) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT') || '(Fecha Ins. Legal)';
    end if;
    if IPJ.VARIOS.Valida_Fecha(p_fecha) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT') || '(Fecha Insc.)';
    end if;
    if IPJ.VARIOS.Valida_Tipo_Ins_Legal(nvl(p_il_tipo, 0)) = 0 then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('INS_LEGAL_NOT');
    end if;

    if v_valid_parametros is not null then
      o_rdo := 'Parametros: '  || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    -- CUERPO PROCEDIMIENTO
    if p_eliminar = 1 then
      delete ipj.t_gravamenes_ins_legal
      where
        Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        id_tramiteipj_accion = p_id_tramiteipj_accion and
        id_tipo_accion = p_id_tipo_accion and
        id_juzgado = p_id_juzgado and
        il_tipo = p_il_tipo and
        il_numero = p_il_numero and
        id_tipo_gravamen = p_id_tipo_gravamen and
        borrador = 'S';

    else
      -- Si existe un registro igual no hago nada
      update ipj.t_gravamenes_ins_legal
      set
         titulo = p_TITULO,
         descripcion = p_DESCRIPCION,
         FECHA = to_date(p_FECHA, 'dd/mm/rrrr')
      where
        Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        id_tramiteipj_accion = p_id_tramiteipj_accion and
        id_tipo_accion = p_id_tipo_accion and
        id_juzgado = p_id_juzgado and
        il_tipo = p_il_tipo and
        il_numero = p_il_numero and
        id_tipo_gravamen = p_id_tipo_gravamen;

      if sql%rowcount = 0 then
        insert into ipj.t_gravamenes_ins_legal
          ( Id_Tramite_Ipj, ID_TRAMITEIPJ_ACCION, ID_TIPO_ACCION, ID_JUZGADO,
            IL_TIPO, IL_NUMERO, ID_TIPO_GRAVAMEN, IL_FECHA, TITULO, DESCRIPCION,
            BORRADOR, FECHA)
        values
          (p_Id_Tramite_Ipj, p_ID_TRAMITEIPJ_ACCION, p_ID_TIPO_ACCION, p_ID_JUZGADO,
            p_IL_TIPO, p_IL_NUMERO, p_ID_TIPO_GRAVAMEN, to_date(p_IL_FECHA, 'dd/mm/rrrr'),
            p_TITULO, p_DESCRIPCION, 'S', to_date(p_FECHA, 'dd/mm/rrrr'));
      end if;
    end if;

    -- Busco el legajo, para ver si tiene libros digitales
    select id_legajo into v_id_legajo
    from ipj.t_tramitesipj_acciones
    where
      id_tramite_ipj = p_id_tramite_ipj and
      id_tramiteipj_accion = p_id_tramiteipj_accion;

    SP_Validar_Libros_Dig (
      o_rdo => v_Hay_Libros_Dig,
      p_id_legajo => v_id_legajo );

    -- Verifico si el tipo de gravamen, debe informa por mail
    select upper(informar_libros) into v_informar_mail
    from ipj.t_tipos_gravamenes
    where
      id_tipo_gravamen = p_id_tipo_gravamen ;

    -- Si tiene libros digitales, y el grabamen debe informa por mail
    if v_Hay_Libros_Dig > 0 and v_informar_mail = 'S' then
      update IPJ.t_tramitesipj tr
      set tr.enviar_mail = 1
      where
        tr.id_tramite_ipj = p_Id_Tramite_Ipj;
    end if;

    o_rdo := 'OK';
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     WHEN OTHERS THEN
       o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_GUARDAR_GRAVAMEN_INS_LEGAL;

  PROCEDURE SP_GUARDAR_GRAVAMEN_FIN(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    -- Datos del instrumento del Gravamen Original
    p_id_tramite_ipj in number,
    p_id_tramiteipj_accion in number,
    p_id_tipo_accion in number,
    p_id_juzgado in number,
    p_il_tipo in number,
    p_il_numero in number,
    p_id_tipo_gravamen in number,
    -- Datos del Instrumento de Fin
    p_fin_id_juzgado in number,
    p_fin_il_tipo in number,
    p_fin_il_numero in number,
    p_fin_il_fecha in varchar2,
    p_fin_fecha in varchar2,
    p_fin_id_tramite_ipj in number)
  IS
    /*************************************************************
        Guarda los instrumentos legales asociados a un gravamen de un tramite.
        No maneja transaccion, ya que se usa en cascada desde la aplicacion y lo administra el codigo.
    *************************************************************/
    v_valid_parametros varchar2(500);
    v_id_legajo number;
    v_Hay_Libros_Dig number;
    v_informar_mail char(1);
    v_id_tipo_gravamen number;
  BEGIN
    -- VALIDACION DE PARAMETROS
    if IPJ.VARIOS.Valida_Fecha(p_fin_il_fecha) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT') || '(Fecha Fin Ins. Legal)';
    end if;
    if IPJ.VARIOS.Valida_Fecha(p_fin_fecha) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT') || '(Fecha Fin Insc.)';
    end if;
    if IPJ.VARIOS.Valida_Tipo_Ins_Legal(nvl(p_fin_il_tipo, 0)) = 0 then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('INS_LEGAL_NOT');
    end if;

    -- verifico que existe alg�n intrumento de gravamen con esos datos
    select id_tipo_gravamen into v_id_tipo_gravamen
    from ipj.t_gravamenes_ins_legal
    where
      Id_Tramite_Ipj = p_Id_Tramite_Ipj and
      id_tramiteipj_accion = p_id_tramiteipj_accion and
      id_tipo_accion = p_id_tipo_accion and
      id_juzgado = p_id_juzgado and
      il_tipo = p_il_tipo and
      il_numero = p_il_numero and
      id_tipo_gravamen = p_id_tipo_gravamen;

    -- Si no habia un tipo de gravamen registrado, salgo
    if v_id_tipo_gravamen = 0 then
      v_valid_parametros := v_valid_parametros || 'No existe un Instrumento legal para los datos indicados.';
    end if;

    if v_valid_parametros is not null then
      o_rdo := 'Parametros: '  || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    -- CUERPO PROCEDIMIENTO
    update ipj.t_gravamenes_ins_legal
    set
      fin_id_juzgado = p_fin_id_juzgado,
      fin_il_tipo = p_fin_il_tipo,
      fin_il_numero = p_fin_il_numero,
      fin_il_fecha = to_date(p_fin_il_fecha, 'dd/mm/rrrr'),
      fin_fecha = to_date(p_fin_fecha, 'dd/mm/rrrr'),
      fin_id_tramite_ipj = p_fin_id_tramite_ipj
    where
      Id_Tramite_Ipj = p_Id_Tramite_Ipj and
      id_tramiteipj_accion = p_id_tramiteipj_accion and
      id_tipo_accion = p_id_tipo_accion and
      id_juzgado = p_id_juzgado and
      il_tipo = p_il_tipo and
      il_numero = p_il_numero and
      id_tipo_gravamen = p_id_tipo_gravamen;

    -- Busco el legajo, para ver si tiene libros digitales
    select id_legajo into v_id_legajo
    from ipj.t_tramitesipj_acciones
    where
      id_tramite_ipj = p_id_tramite_ipj and
      id_tramiteipj_accion = p_id_tramiteipj_accion;

    SP_Validar_Libros_Dig (
      o_rdo => v_Hay_Libros_Dig,
      p_id_legajo => v_id_legajo );

    -- Verifico si el tipo de gravamen, debe informa por mail
    select upper(informar_libros) into v_informar_mail
    from ipj.t_tipos_gravamenes
    where
      id_tipo_gravamen = v_id_tipo_gravamen ;

    -- Si tiene libros digitales, y el grabamen debe informa por mail
    if v_Hay_Libros_Dig > 0 and v_informar_mail = 'S' then
      update IPJ.t_tramitesipj tr
      set tr.enviar_mail = 1
      where
        tr.id_tramite_ipj = p_Id_Tramite_Ipj;
    end if;

    o_rdo := 'OK';
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     WHEN OTHERS THEN
       o_rdo := 'SP_GUARDAR_GRAVAMEN_FIN : ' || To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_GUARDAR_GRAVAMEN_FIN;

  PROCEDURE SP_Traer_Gravamen_Ins_Legal(
    o_Cursor OUT TYPES.cursorType,
    p_Id_Tramite_Ipj in number,
    p_Id_TramiteIPJ_Accion in number,
    p_id_tipo_accion in number
    )
  IS
    v_id_legajo number;
    v_id_integrante number;
  BEGIN
  /*******************************************************
    Este procedimento devuelve los instrumentos legales que conforman un Gravamen
  ********************************************************/
    select nvl(g.id_legajo, 0), nvl(id_integrante, 0) into v_id_legajo, v_id_integrante
    from IPJ.T_TramitesIpj_Acciones g
    where
      g.Id_Tramite_Ipj = p_Id_Tramite_Ipj and
      g.id_tramiteipj_accion = p_id_tramiteipj_accion and
      g.id_tipo_accion = p_id_tipo_accion;

    -- Cuando armo el cursor, si es Tracto Registral marco todas las filas para imprimir,
    -- sino solo marco la primera.
    OPEN o_Cursor FOR
     select
       Id_Tramite_Ipj, ID_TRAMITEIPJ_ACCION, ID_TIPO_ACCION, ID_JUZGADO,
       IL_TIPO, IL_NUMERO, ID_TIPO_GRAVAMEN, IL_FECHA,
       TITULO, DESCRIPCION, BORRADOR, FECHA,
       N_JUZGADO, N_IL_TIPO, N_TIPO_GRAVAMEN,
       (case rownum when 1 then 1 else Imprimir end) imprimir,
       fin_id_juzgado, fin_il_tipo, fin_il_numero, fin_il_fecha, fin_fecha, fin_id_tramite_ipj
    from
      (select
         gil.Id_Tramite_Ipj, gil.ID_TRAMITEIPJ_ACCION, gil.ID_TIPO_ACCION, gil.ID_JUZGADO,
         gil.IL_TIPO, gil.IL_NUMERO, gil.ID_TIPO_GRAVAMEN, To_Char(gil.IL_FECHA, 'dd/mm/rrrr') IL_FECHA,
         gil.TITULO, gil.DESCRIPCION, gil.BORRADOR, To_Char(gil.FECHA, 'dd/mm/rrrr') FECHA,
         J.N_JUZGADO, L.N_IL_TIPO, TG.N_TIPO_GRAVAMEN,
         (case p_id_tipo_accion when IPJ.TYPES.c_Acc_Grav_TractoRegistral then 1 else 0 end) imprimir,
         gil.fin_id_juzgado, gil.fin_il_tipo, gil.fin_il_numero,
         to_char(gil.fin_il_fecha, 'dd/mm/rrrr') fin_il_fecha,
         to_char(gil.fin_fecha, 'dd/mm/rrrr') fin_fecha, gil.fin_id_tramite_ipj
      from ipj.t_gravamenes_ins_legal gil join ipj.t_juzgados j
          on gIL.ID_JUZGADO = J.ID_JUZGADO
        join IPJ.T_TIPOS_INS_LEGAL L
          on gIL.IL_TIPO = L.IL_TIPO
        join IPJ.T_TIPOS_GRAVAMENES tg
          on GIL.ID_TIPO_GRAVAMEN = TG.ID_TIPO_GRAVAMEN
        join IPJ.t_gravamenes g
          on gil.Id_Tramite_Ipj = g.Id_Tramite_Ipj and
              gil.id_tramiteipj_accion = g.id_tramiteipj_accion and
              gil.id_tipo_accion = g.id_tipo_accion
      where
        ( (v_id_legajo != 0 and g.id_legajo = v_id_legajo ) or
          (v_id_integrante != 0 and g.id_integrante = v_id_integrante)
        ) and
        (gil.borrador = 'N' or gIL.Id_Tramite_Ipj = p_Id_Tramite_Ipj) and
        (gil.fin_id_tramite_ipj is null or  gil.fin_id_tramite_ipj = p_id_tramite_ipj)
      order by gil.fecha desc, GIL.IL_FECHA desc) t ;

  END SP_Traer_Gravamen_Ins_Legal;

  PROCEDURE SP_GUARDAR_INTEG_DOMICILIO(
    o_id_vin out number,
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_id_tramiteipj_accion in number,
    p_id_tipo_Accion in number,
    p_id_integrante in number,
    p_id_vin_integrante in number,
    P_ID_PROVINCIA in varchar2,
    P_ID_DEPARTAMENTO in number,
    P_ID_LOCALIDAD in number,
    P_BARRIO in varchar2,
    P_CALLE in varchar2,
    P_ALTURA in number,
    P_DEPTO in varchar2,
    P_PISO in varchar2,
    p_torre in varchar2,
    p_manzana in varchar2,
    p_lote in varchar2
    )
  IS
    v_valid_parametros varchar2(500);
    v_TIPO_DOM  NUMBER;
    v_N_TIPO_DOM  VARCHAR2(150);
    v_bloque varchar(200);
    v_usuario varchar2(20);
  BEGIN
  /*************************************************************
    Inserta o Actualiza un domicilio para un integrante en DOM_MANAGER
    No maneja transaccion, ya lo hace DOM_MANAGER
  *************************************************************/
    -- Busco el responsable de la primer accion
    select cuil_usuario into v_usuario
    from ipj.t_tramitesipj_acciones
    where
      id_tramite_ipj = p_id_tramite_ipj and
      id_estado < 100 and
      rownum = 1;

    IPJ.VARIOS.SP_GUARDAR_DOMICILIO(
      o_id_vin => o_id_vin,
      o_rdo => o_rdo,
      o_tipo_mensaje => o_tipo_mensaje,
      p_id_vin => p_id_vin_integrante,
      p_usuario => v_usuario,
      p_Tipo_Domicilio => 0, -- 3 Real //  0 Sin Asignar
      p_id_integrante => p_id_integrante,
      p_id_legajo => 0,
      p_cuit_empresa => null,
      p_id_fondo_comercio => 0,
      p_id_entidad_acta => 0,
      p_es_comerciante => 'N',
      p_es_admin_entidad => 'N',
      P_ID_PROVINCIA => p_id_provincia,
      P_ID_DEPARTAMENTO => p_id_departamento,
      P_ID_LOCALIDAD => p_id_localidad,
      P_BARRIO => p_barrio,
      P_CALLE => p_calle,
      P_ALTURA => p_altura,
      P_DEPTO => p_depto,
      P_PISO => p_piso,
      p_torre => p_torre,
      p_manzana => p_manzana,
      p_lote => p_lote,
      p_id_barrio => null,
      p_id_calle => null,
      p_id_tipocalle => 9,
      p_km => null
    );

    -- Actualizo el ID_VIN en el integrate
    if Upper(o_rdo) = IPJ.TYPES.C_RESP_OK then
      update IPJ.t_integrantes
      set id_vin = o_id_vin
      where
        id_integrante = p_id_integrante;
    end if;

  EXCEPTION
     WHEN OTHERS THEN
        o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;

  END SP_GUARDAR_INTEG_DOMICILIO;

  PROCEDURE SP_GUARDAR_ENTIDAD_RELACIONADA(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_entidad_relacion out number,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number,
    p_id_tipo_relacion_empresa in number,
    p_id_legajo_relacionado in number,
    p_fecha_relacion in varchar2,
    p_eliminar in number, -- 1 elimina
    p_id_entidad_relacion in number,
    p_cuit_empresa in varchar2,
    p_n_empresa in varchar2,
    p_id_integrante in number,
    p_id_sexo in varchar2,
    p_nro_documento in varchar2,
    p_pai_cod_pais in varchar2,
    p_id_numero in number,
    p_cuil in varchar2,
    p_detalle in varchar2,
    p_nombre in varchar2,
    p_apellido in varchar2,
    p_error_dato in varchar2,
    p_n_tipo_documento in varchar2
    )
  IS
    v_existe number;
    v_valid_parametros varchar2(500);
    v_tipo_mensaje number;
    v_id_legajo_relacionado number;
    v_id_integrante number;
  BEGIN
    /*************************************************************
      Guarda los cuit de las empresas relacionadas a una entidad de un tramite.
      No maneja transaccion, ya que se usa en cascada desde la aplicacion y lo administra el codigo.
  *************************************************************/
  -- VALIDACION DE PARAMETROS
    if IPJ.VARIOS.Valida_Fecha(p_fecha_relacion) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT');
    end if;
    if IPJ.VARIOS.Valida_Entidad(nvl(p_Id_Tramite_Ipj, 0), p_id_legajo) = 0 then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('ENT_NOT')|| '(' || to_char(p_Id_Tramite_Ipj) ||' - ' || to_char(p_id_legajo)||')';
    end if;

    if v_valid_parametros is not null then
      o_rdo := 'GUARDAR RELACION - Parametros: '  || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    -- CUERPO PROCEDIMIENTO
    if p_eliminar = 1 then
      delete ipj.t_entidades_relacionadas
      where
        id_entidad_relacion = p_id_entidad_relacion and
        borrador = 'S';

      o_id_entidad_relacion := 0;
    else
       -- Si no viene una Empresa, es una persona
       if p_n_empresa is null then
         -- Si el integrante no existe, lo agrego y luego continuo
         v_id_integrante := p_id_integrante;
         if IPJ.VARIOS.Valida_Integrante(p_id_integrante) = 0 then
           IPJ.ENTIDAD_PERSJUR.SP_GUARDAR_INTEGRANTES(
             o_rdo => v_valid_parametros,
             o_tipo_mensaje => v_tipo_mensaje,
             o_id_integrante => v_id_integrante,
             p_id_sexo => p_id_sexo,
             p_nro_documento => p_nro_documento,
             p_pai_cod_pais => p_pai_cod_pais,
             p_id_numero => p_id_numero,
             p_cuil => p_cuil,
             p_detalle => p_detalle,
             p_Nombre => p_Nombre,
             p_Apellido => p_Apellido,
             p_error_dato => p_error_dato,
             p_n_tipo_documento => p_n_tipo_documento );

           if v_valid_parametros <> TYPES.c_Resp_OK then
              o_rdo := IPJ.VARIOS.MENSAJE_ERROR('INT_NOT') || '. ' || v_valid_parametros;
              o_tipo_mensaje := v_tipo_mensaje;
              return;
           end if;
         else
           -- si esta marcado como dato erroneo, cambio la descripcion y lo marco.
           if p_error_dato = 'S' then
             update ipj.t_integrantes
             set
               error_dato = p_error_dato,
               detalle = p_detalle
             where
               id_integrante = p_id_integrante;
           end if;
           v_id_integrante := p_id_integrante;
         end if;
       end if;

      -- Si existe un registro igual no hago nada
      select count(*) into v_existe
      from ipj.t_entidades_relacionadas er
      where
        er.Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        er.id_legajo = p_id_legajo and
        ER.ID_TIPO_RELACION_EMPRESA = p_ID_TIPO_RELACION_EMPRESA and
        ( ( p_id_legajo_relacionado <> 0 and er.id_legajo_relacionado = p_id_legajo_relacionado) or
          (p_id_integrante <> 0 and er.id_integrante = p_id_integrante)
        );


      if v_existe = 0 then
        v_id_legajo_relacionado := (case when p_id_legajo_relacionado = 0 then null else p_id_legajo_relacionado end);
        v_id_integrante := (case when v_id_integrante = 0 then null else v_id_integrante end);

        SELECT IPJ.SEQ_ENTIDAD_RELACION.nextval INTO o_id_entidad_relacion FROM dual;

        insert into ipj.t_entidades_relacionadas
          (Id_Tramite_Ipj, id_legajo, id_tipo_relacion_empresa, id_legajo_relacionado, fecha_relacion, borrador,
           id_entidad_relacion, cuit_empresa, n_empresa, id_integrante)
        values
          (p_Id_Tramite_Ipj, p_id_legajo, p_id_tipo_relacion_empresa, v_id_legajo_relacionado,
           to_date(p_fecha_relacion, 'dd/mm/rrrr'),  'S',
           o_id_entidad_relacion, p_cuit_empresa, p_n_empresa, v_id_integrante);

      else
        o_id_entidad_relacion := p_id_entidad_relacion;
      end if;
    end if;

    o_rdo := 'OK';
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     WHEN OTHERS THEN
       o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_GUARDAR_ENTIDAD_RELACIONADA;


  PROCEDURE SP_Traer_PersJur_Relacionada(
    o_Cursor OUT TYPES.cursorType,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number
    )
  IS
    v_matricula_version number(6,0);
    v_ubicacion_ent number(6);
    v_ubicacion_leg number(6);
  BEGIN
  /*******************************************************
    Este procedimento devuelve las empresas relacioandas a la entidad del tramite
  ********************************************************/
    begin
      select nvl(matricula_version, 0),
        (select id_ubicacion from ipj.t_tipos_entidades te where te.id_tipo_entidad = e.id_tipo_entidad) ubicacion_ent,
        (select id_ubicacion from ipj.t_tipos_entidades te where te.id_tipo_entidad = l.id_tipo_entidad) ubicacion_leg
        into v_matricula_version, v_ubicacion_ent, v_ubicacion_leg
      from IPJ.t_entidades e join ipj.t_legajos l
        on e.id_legajo = l.id_legajo
      where
        e.Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        e.id_legajo = p_id_legajo;
    exception
      when NO_DATA_FOUND then
        v_matricula_version := 0;
    end;

    OPEN o_Cursor FOR
      select er.id_entidad_relacion, ER.Id_Tramite_Ipj, ER.id_legajo, ER.ID_TIPO_RELACION_EMPRESA,
        to_char(ER.FECHA_RELACION , 'dd/mm/rrrr') FECHA_RELACION, ER.id_legajo_relacionado,
        ER.BORRADOR, R.N_TIPO_RELACION_EMPRESA, er.cuit_empresa cuit_relacionado,
        er.n_empresa RazonSocial, er.cuit_empresa, er.n_empresa, er.id_integrante,
        (p.NOMBRE || ' ' ||p.APELLIDO) detalle,
        I.ID_NUMERO, I.ID_SEXO, I.NRO_DOCUMENTO, I.PAI_COD_PAIS, I.ERROR_DATO,
        (case
           when er.id_integrante is null and er.cuit_empresa is not null then
              er.n_empresa
           when er.id_integrante is not null then
              (p.NOMBRE || ' ' ||p.APELLIDO)
         end) Persona,
        (case
          when er.id_integrante is null and er.cuit_empresa is not null then er.cuit_empresa
          when er.id_integrante is not null then I.NRO_DOCUMENTO
        end) Identificacion
      from ipj.t_entidades_relacionadas er join ipj.t_tipos_relacion_empresa r
            on ER.ID_TIPO_RELACION_EMPRESA = R.ID_TIPO_RELACION_EMPRESA
          join IPJ.T_Entidades e
            on er.Id_Tramite_Ipj = e.Id_Tramite_Ipj and er.id_legajo = e.id_legajo
          left join ipj.t_integrantes i
            on er.id_integrante = i.id_integrante
          left join rcivil.vt_pk_persona p
            on i.id_sexo = p.ID_SEXO and
              i.nro_documento = p.NRO_DOCUMENTO and
              i.pai_cod_pais = p.PAI_COD_PAIS and
              i.id_numero = p.ID_NUMERO
      where
        e.id_legajo = p_id_legajo and
        e.matricula_version <= v_matricula_version and
        e.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_ubicacion_leg) and
        (er.borrador = 'N' or er.Id_Tramite_Ipj = p_Id_Tramite_Ipj)
      order by
        ER.FECHA_RELACION desc;
  END SP_Traer_PersJur_Relacionada;


  PROCEDURE SP_Validar_PersJur_Relacion(
    o_rdo out number,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number,
    p_id_tipo_relacion_empresa in number,
    p_id_legajo_relacionado in number)
  IS
    v_res number;
  BEGIN
  /*******************************************************
    Valida que no exsita otro tramite de la misma entidad con la misma empresa relacionada que se piensa agregar
  ********************************************************/
    begin
      select max(er.Id_Tramite_Ipj) into v_res
      from ipj.t_entidades_relacionadas er join ipj.t_tramitesipj_acciones tacc
          on tacc.Id_Tramite_Ipj = er.Id_Tramite_Ipj and tacc.id_legajo = er.id_legajo
      where
        er.Id_Tramite_Ipj <> p_Id_Tramite_Ipj and
        er.id_legajo = p_id_legajo and
        --ER.ID_TIPO_RELACION_EMPRESA  = p_id_tipo_relacion_empresa and
        ER.id_legajo_relacionado = id_legajo_relacionado and
        tacc.id_estado < IPJ.TYPES.C_ESTADOS_RECHAZADO;
    exception
      when OTHERS then v_res := 0;
    end;

    o_rdo :=  NVL(v_res, 0);
  EXCEPTION
    when NO_DATA_FOUND then v_res := 0;
    when OTHERS then v_res := 0;
    o_rdo :=  v_res;
  END SP_Validar_PersJur_Relacion;

  PROCEDURE SP_Traer_Consulta_Fonetica(
    o_cursor OUT TYPES.cursorType,
    p_Clave in varchar2,
    p_cuil_usuario in varchar2)
  IS
    v_Es_Direccion number;
  BEGIN
  /*********************************************************
  Este procedimiento busca nombres de empresas similares segun su fonetica
  en legajos y reservas ok no vencidas
  **********************************************************/

    -- Controlo si tiene permisos de Direccion
    select count(*) into v_Es_Direccion
    from ipj.t_grupos_trab_ubicacion g
    where
      cuil = p_cuil_usuario and
      id_ubicacion = IPJ.TYPES.C_AREA_DIRECCION;

    OPEN o_Cursor FOR
      select
        c.id_legajo, C.DENOMINACION_SIA, C.CUIT, c.similar, C.TIPO, c.Id_Tramite_Ipj, c.id_tramiteipj_accion,
        to_char(fec_vence, 'dd/mm/rrrr') fec_vence
      from
        ( select
            l.id_legajo, L.DENOMINACION_SIA, L.CUIT, IPJ.PKG_SND.FC_Comparar_Fonetica(p_Clave, L.DENOMINACION_SIA) Similar,
            'E' TIPO, 0 Id_Tramite_Ipj, 0 id_tramiteipj_accion, null fec_vence
          from IPJ.T_Legajos L join IPJ.t_tipos_entidades te
            on L.id_tipo_entidad = te.id_tipo_entidad
          where
            nvl(eliminado, 0) = 0 and
            ( v_Es_Direccion >= 1 or
              te.id_ubicacion in (select distinct id_ubicacion from ipj.t_grupos_trab_ubicacion where cuil = p_cuil_usuario)
            )
          UNION
          select
            null id_legajo, tr.N_Reserva DENOMINACION_SIA, '' CUIT, IPJ.PKG_SND.FC_Comparar_Fonetica(p_Clave, tr.N_Reserva) Similar,
            'R' TIPO, tr.Id_Tramite_Ipj, tr.id_tramiteipj_accion, tr.fec_vence
          from IPJ.T_tramitesipj_acc_reserva tr join ipj.t_tramitesipj tra
            on tr.Id_Tramite_Ipj = tra.Id_Tramite_Ipj
          where
            TR.ID_ESTADO_RESERVA = IPJ.Types.c_Reserva_OK and
            TR.FEC_VENCE is not null and
            TR.FEC_VENCE >= sysdate and
            ( v_Es_Direccion >= 1 or
              tra.id_ubicacion_origen in (select distinct id_ubicacion from ipj.t_grupos_trab_ubicacion where cuil = p_cuil_usuario)
            )
         ) C
      where
        similar > 0
      order by similar desc, DENOMINACION_SIA asc;

  END SP_Traer_Consulta_Fonetica;

  PROCEDURE SP_Traer_Consulta_Textual(
    o_cursor OUT TYPES.cursorType,
    p_Clave in varchar2,
    p_cuil_usuario in varchar2)
  IS
    v_Es_Direccion number;
  BEGIN
  /*********************************************************
  Este procedimiento busca nombres de empresas similares segun su escritura
  en legajos y reservas OK no vencidas, restringidos a las areas del usuario logueado.
  Si es de la direcci�n, busca en todos.
  **********************************************************/

    -- Controlo si tiene permisos de Direccion
    select count(*) into v_Es_Direccion
    from ipj.t_grupos_trab_ubicacion g
    where
      cuil = p_cuil_usuario and
      id_ubicacion = IPJ.TYPES.C_AREA_DIRECCION;

    OPEN o_Cursor FOR
      select
        c.id_legajo, C.DENOMINACION_SIA, C.CUIT, c.similar, c.TIPO, c.Id_Tramite_Ipj, c.id_tramiteipj_accion,
        to_char(fec_vence, 'dd/mm/rrrr') fec_vence,
        (case
           when tipo = 'R' then (select expediente from ipj.t_tramitesipj where Id_Tramite_Ipj = c.Id_Tramite_Ipj)
           else IPJ.Varios.FC_Ultimo_Expediente(id_legajo)
         end)Expediente
      from
        -- Busca en Legajos
        ( select
            l.id_legajo, L.DENOMINACION_SIA, L.CUIT,
            IPJ.PKG_SND.FC_COMPARAR_FRASES(p_Clave, replace(L.DENOMINACION_SIA, ' ', '')) Similar,
            'E' TIPO, 0 Id_Tramite_Ipj, 0 id_tramiteipj_accion, null fec_vence
          from IPJ.T_Legajos L join IPJ.t_tipos_entidades te
            on L.id_tipo_entidad = te.id_tipo_entidad
          where
            nvl(eliminado, 0) = 0 and
            ( v_Es_Direccion >= 1 or
              te.id_ubicacion in (select distinct id_ubicacion from ipj.t_grupos_trab_ubicacion where cuil = p_cuil_usuario)
            )
          -- Suma los tramites de Reservas
          UNION
          select
            null id_legajo, tr.N_Reserva DENOMINACION_SIA, '' CUIT,
            IPJ.PKG_SND.FC_COMPARAR_FRASES(p_Clave, replace(tr.N_Reserva, ' ', '')) Similar,
            'R' TIPO, tr.Id_Tramite_Ipj, tr.id_tramiteipj_accion, tr.fec_vence
          from IPJ.T_tramitesipj_acc_reserva tr join ipj.t_tramitesipj tra
            on tr.Id_Tramite_Ipj = tra.Id_Tramite_Ipj
          where
            TR.ID_ESTADO_RESERVA = IPJ.Types.c_Reserva_OK and
            TR.FEC_VENCE is not null and
            TR.FEC_VENCE >= sysdate and
            ( v_Es_Direccion >= 1 or
              tra.id_ubicacion_origen in (select distinct id_ubicacion from ipj.t_grupos_trab_ubicacion where cuil = p_cuil_usuario)
            )
          UNION
          -- Suma los tramites abiertos
          select
            e.id_legajo, e.Denominacion_1 DENOMINACION_SIA, e.CUIT,
            IPJ.PKG_SND.FC_COMPARAR_FRASES(FC_FORMATEAR_CADENA_COMP(p_Clave), replace(FC_FORMATEAR_CADENA_COMP(e.Denominacion_1), ' ', '')) Simil,
            'E' TIPO, 0 Id_Tramite_Ipj, 0 id_tramiteipj_accion, null fec_vence
          from ipj.t_tramitesipj tr join ipj.t_entidades e
            on tr.Id_Tramite_Ipj = e.Id_Tramite_Ipj
          where
            TR.ID_ESTADO_ULT < IPJ.TYPES.C_ESTADOS_COMPLETADO and
            ( v_Es_Direccion >= 1 or
              tr.id_ubicacion in (select distinct id_ubicacion from ipj.t_grupos_trab_ubicacion where cuil = p_cuil_usuario )
            )
        ) C
      where
        similar > 0
      order by similar desc, DENOMINACION_SIA asc;

  END SP_Traer_Consulta_Textual;

  PROCEDURE SP_Traer_Consulta_Nombre(
    o_cursor OUT TYPES.cursorType,
    p_Clave in varchar2,
    p_cuil_usuario in varchar2)
  IS
    v_Es_Direccion number;
  BEGIN
  /*********************************************************
  Este procedimiento devuelve el listado de la busqueda fon�tica y por similitud unidos,
  mostrando solo 1 vez cada registro, ordenado por similitud y  nombre
  **********************************************************/

    -- Controlo si tiene permisos de Direccion
    select count(*) into v_Es_Direccion
    from ipj.t_grupos_trab_ubicacion g
    where
      cuil = p_cuil_usuario and
      id_ubicacion = IPJ.TYPES.C_AREA_DIRECCION;

    OPEN o_Cursor FOR
      select
        nvl(id_legajo, 0) id_legajo, DENOMINACION_SIA, CUIT, (case when similar >= sim_fon then similar else sim_fon end) similar,
        TIPO, Id_Tramite_Ipj, id_tramiteipj_accion,  to_char(fec_vence, 'dd/mm/rrrr') fec_vence,
        (case when similar >= sim_fon then 'Textual' else 'Fon�tico' end) Observacion,
        IPJ.VARIOS.FC_Conteo_Palabras(DENOMINACION_SIA) Palabras,
        (case
           when tipo = 'R' then (select expediente from ipj.t_tramitesipj where Id_Tramite_Ipj = tmp.Id_Tramite_Ipj)
           else IPJ.Varios.FC_Ultimo_Expediente(id_legajo)
         end)Expediente
      from
        ( select distinct
            c.id_legajo, C.DENOMINACION_SIA, C.CUIT, max(c.simil) similar, max(c.similar_fon) sim_fon,
            c.TIPO, c.Id_Tramite_Ipj, c.id_tramiteipj_accion, fec_vence
          from
            -- Busca desde Legajos
            ( select
                l.id_legajo, L.DENOMINACION_SIA, L.CUIT,
                IPJ.PKG_SND.FC_COMPARAR_FRASES(FC_FORMATEAR_CADENA_COMP(ipj.varios.FC_Quitar_Tipo_Empresa(p_Clave)), replace(FC_FORMATEAR_CADENA_COMP(L.DENOMINACION_SIA), ' ', '')) Simil,
                IPJ.PKG_SND.FC_Comparar_Fonetica(FC_FORMATEAR_CADENA_COMP(ipj.varios.FC_Quitar_Tipo_Empresa(p_Clave)), FC_FORMATEAR_CADENA_COMP(L.DENOMINACION_SIA)) Similar_Fon,
                'E' TIPO, 0 Id_Tramite_Ipj, 0 id_tramiteipj_accion, null fec_vence
              from IPJ.T_Legajos L join IPJ.t_tipos_entidades te
                on L.id_tipo_entidad = te.id_tipo_entidad
              where
                nvl(eliminado, 0) = 0 and
                fecha_baja_entidad is null and
                ( v_Es_Direccion >= 1 or
                  te.id_ubicacion in (select distinct id_ubicacion from ipj.t_grupos_trab_ubicacion where cuil = p_cuil_usuario )
                )
              UNION
              -- Suma las Reservas
              select
                null id_legajo, tr.N_Reserva DENOMINACION_SIA, '' CUIT,
                IPJ.PKG_SND.FC_COMPARAR_FRASES(FC_FORMATEAR_CADENA_COMP(ipj.varios.FC_Quitar_Tipo_Empresa(p_Clave)), replace(FC_FORMATEAR_CADENA_COMP(tr.N_Reserva), ' ', '')) Simil,
                IPJ.PKG_SND.FC_Comparar_Fonetica(FC_FORMATEAR_CADENA_COMP(ipj.varios.FC_Quitar_Tipo_Empresa(p_Clave)), FC_FORMATEAR_CADENA_COMP(tr.N_Reserva)) Similar_Fon,
                'R' TIPO, tr.Id_Tramite_Ipj, tr.id_tramiteipj_accion, tr.fec_vence
              from IPJ.T_tramitesipj_acc_reserva tr join ipj.t_tramitesipj tra
                on tr.Id_Tramite_Ipj = tra.Id_Tramite_Ipj
              where
                TR.ID_ESTADO_RESERVA = IPJ.Types.c_Reserva_OK and
                TR.FEC_VENCE is not null and
                TR.FEC_VENCE >= sysdate and
                ( v_Es_Direccion >= 1 or
                  tra.id_ubicacion_origen in (select distinct id_ubicacion from ipj.t_grupos_trab_ubicacion where cuil = p_cuil_usuario )
                )
              UNION
              -- Suma los tramites abiertos
              select
                e.id_legajo, e.Denominacion_1 DENOMINACION_SIA, e.CUIT,
                IPJ.PKG_SND.FC_COMPARAR_FRASES(FC_FORMATEAR_CADENA_COMP(ipj.varios.FC_Quitar_Tipo_Empresa(p_Clave)), replace(FC_FORMATEAR_CADENA_COMP(e.Denominacion_1), ' ', '')) Simil,
                IPJ.PKG_SND.FC_Comparar_Fonetica(FC_FORMATEAR_CADENA_COMP(ipj.varios.FC_Quitar_Tipo_Empresa(p_Clave)), FC_FORMATEAR_CADENA_COMP(e.Denominacion_1)) Similar_Fon,
                'E' TIPO, 0 Id_Tramite_Ipj, 0 id_tramiteipj_accion, null fec_vence
              from ipj.t_tramitesipj tr join ipj.t_entidades e
                  on tr.Id_Tramite_Ipj = e.Id_Tramite_Ipj
                join ipj.t_legajos l
                  on e.id_legajo = l.id_legajo
              where
                nvl(L.ELIMINADO, 0) = 0 and
                L.Fecha_Baja_Entidad is null and
                TR.ID_ESTADO_ULT < IPJ.TYPES.C_ESTADOS_COMPLETADO and
                ( v_Es_Direccion >= 1 or
                  tr.id_ubicacion in (select distinct id_ubicacion from ipj.t_grupos_trab_ubicacion where cuil = p_cuil_usuario )
                )
            ) C
         where
           simil > 0 or
           similar_fon > 0
         group by  c.id_legajo, C.DENOMINACION_SIA, C.CUIT, c.TIPO, c.Id_Tramite_Ipj, c.id_tramiteipj_accion, fec_vence
        ) tmp
       order by Observacion desc, similar desc, Palabras asc, Observacion desc, DENOMINACION_SIA asc;

  END SP_Traer_Consulta_Nombre;


  PROCEDURE SP_Traer_Consulta_Nombre_Comp(
    o_cursor OUT TYPES.cursorType,
    p_Clave in varchar2,
    p_id_ubicacion in number)
  IS
  v_ubic_rp NUMBER;
  v_ubic_sxa NUMBER;
  v_ubic_acyf NUMBER;

  BEGIN
  /*********************************************************
  Este procedimiento devuelve el listado de la busqueda fon�tica y por similitud unidos,
  mostrando solo 1 vez cada registro, ordenado por similitud y  nombre
  Busca los nombres en el area indicada.
  **********************************************************/
    IF p_id_ubicacion IN (1,4) THEN
      v_ubic_rp := 1;
      v_ubic_sxa := 4;
    ELSE
      v_ubic_acyf := 5;
    END IF;

    OPEN o_Cursor FOR
      select
        nvl(id_legajo, 0) id_legajo, DENOMINACION_SIA, CUIT, (case when similar >= sim_fon then similar else sim_fon end) similar,
        TIPO, Id_Tramite_Ipj, id_tramiteipj_accion,  to_char(fec_vence, 'dd/mm/rrrr') fec_vence,
        (case when similar >= sim_fon then 'Textual' else 'Fon�tico' end) Observacion,
        IPJ.VARIOS.FC_Conteo_Palabras(DENOMINACION_SIA) Palabras,
        (case
           when tipo = 'R' then (select expediente from ipj.t_tramitesipj where Id_Tramite_Ipj = tmp.Id_Tramite_Ipj)
           else IPJ.Varios.FC_Ultimo_Expediente(id_legajo)
         end)Expediente
      from
        ( select distinct
            c.id_legajo, C.DENOMINACION_SIA, C.CUIT, --max(c.simil) similar, max(c.similar_fon) sim_fon,
            IPJ.PKG_SND.FC_COMPARAR_FRASES(IPJ.Entidad_PersJur.FC_FORMATEAR_CADENA_COMP(p_clave), replace(IPJ.Entidad_PersJur.FC_FORMATEAR_CADENA_COMP(DENOMINACION_SIA), ' ', '')) Similar,
            IPJ.PKG_SND.FC_Comparar_Fonetica(ipj.Entidad_PersJur.FC_FORMATEAR_CADENA_COMP(p_clave), IPJ.Entidad_PersJur.FC_FORMATEAR_CADENA_COMP(DENOMINACION_SIA)) Sim_Fon,
            c.TIPO, c.Id_Tramite_Ipj, c.id_tramiteipj_accion, fec_vence
          from
            -- Busca en Legajos
            ( select
                l.id_legajo, L.DENOMINACION_SIA, L.CUIT,
                --IPJ.PKG_SND.FC_COMPARAR_FRASES(IPJ.Entidad_PersJur.FC_FORMATEAR_CADENA_COMP(:p_clave), replace(IPJ.Entidad_PersJur.FC_FORMATEAR_CADENA_COMP(L.DENOMINACION_SIA), ' ', '')) Simil,
                --IPJ.PKG_SND.FC_Comparar_Fonetica(ipj.Entidad_PersJur.FC_FORMATEAR_CADENA_COMP(:p_clave), IPJ.Entidad_PersJur.FC_FORMATEAR_CADENA_COMP(L.DENOMINACION_SIA)) Similar_Fon,
                'E' TIPO, 0 Id_Tramite_Ipj, 0 id_tramiteipj_accion, null fec_vence
              from IPJ.T_Legajos L
                --join IPJ.t_tipos_entidades te on L.id_tipo_entidad = te.id_tipo_entidad
              where
                nvl(eliminado, 0) = 0 and
                fecha_baja_entidad is null and
                --te.id_ubicacion = :p_id_ubicacion
                l.id_tipo_entidad in (select id_tipo_entidad from IPJ.t_tipos_entidades te where te.id_ubicacion IN (v_ubic_acyf, v_ubic_rp, v_ubic_sxa))
              group by l.id_legajo, L.DENOMINACION_SIA, L.CUIT
              having
                IPJ.PKG_SND.FC_COMPARAR_FRASES(IPJ.Entidad_PersJur.FC_FORMATEAR_CADENA_COMP(p_clave), replace(IPJ.Entidad_PersJur.FC_FORMATEAR_CADENA_COMP(L.DENOMINACION_SIA), ' ', '')) +
                IPJ.PKG_SND.FC_Comparar_Fonetica(ipj.Entidad_PersJur.FC_FORMATEAR_CADENA_COMP(p_clave), IPJ.Entidad_PersJur.FC_FORMATEAR_CADENA_COMP(L.DENOMINACION_SIA)) > 0

              UNION all
              -- Suma los tramites de reservas
              select
                null id_legajo, tr.N_Reserva DENOMINACION_SIA, '' CUIT,
                --IPJ.PKG_SND.FC_COMPARAR_FRASES(IPJ.Entidad_PersJur.FC_FORMATEAR_CADENA_COMP(:p_clave), replace(IPJ.Entidad_PersJur.FC_FORMATEAR_CADENA_COMP(tr.N_Reserva), ' ', '')) Simil,
                --IPJ.PKG_SND.FC_Comparar_Fonetica(IPJ.Entidad_PersJur.FC_FORMATEAR_CADENA_COMP(:p_clave), IPJ.Entidad_PersJur.FC_FORMATEAR_CADENA_COMP(tr.N_Reserva)) Similar_Fon,
                'R' TIPO, tr.Id_Tramite_Ipj, tr.id_tramiteipj_accion, tr.fec_vence
              from ipj.t_tramitesipj tra join IPJ.T_tramitesipj_acc_reserva tr
                on tr.Id_Tramite_Ipj = tra.Id_Tramite_Ipj
              where
                tra.id_estado_ult < IPJ.TYPES.C_ESTADOS_RECHAZADO and
                tra.id_ubicacion_origen IN (v_ubic_acyf, v_ubic_rp, v_ubic_sxa) and
                TR.ID_ESTADO_RESERVA = IPJ.Types.c_Reserva_OK and
                TR.FEC_VENCE is not null and
                TR.FEC_VENCE >= sysdate
              group by tr.N_Reserva, tr.Id_Tramite_Ipj, tr.id_tramiteipj_accion, tr.fec_vence
              having
                IPJ.PKG_SND.FC_COMPARAR_FRASES(IPJ.Entidad_PersJur.FC_FORMATEAR_CADENA_COMP(p_clave), replace(IPJ.Entidad_PersJur.FC_FORMATEAR_CADENA_COMP(tr.N_Reserva), ' ', '')) +
                IPJ.PKG_SND.FC_Comparar_Fonetica(IPJ.Entidad_PersJur.FC_FORMATEAR_CADENA_COMP(p_clave), IPJ.Entidad_PersJur.FC_FORMATEAR_CADENA_COMP(tr.N_Reserva)) > 0

              UNION all
              -- Suma los tramites abiertos
              select
                e.id_legajo, e.Denominacion_1 DENOMINACION_SIA, e.CUIT,
                --IPJ.PKG_SND.FC_COMPARAR_FRASES(IPJ.Entidad_PersJur.FC_FORMATEAR_CADENA_COMP(:p_clave), replace(IPJ.Entidad_PersJur.FC_FORMATEAR_CADENA_COMP(e.Denominacion_1), ' ', '')) Simil,
                --IPJ.PKG_SND.FC_Comparar_Fonetica(IPJ.Entidad_PersJur.FC_FORMATEAR_CADENA_COMP(:p_clave), IPJ.Entidad_PersJur.FC_FORMATEAR_CADENA_COMP(e.Denominacion_1)) Similar_Fon,
                'E' TIPO, 0 Id_Tramite_Ipj, 0 id_tramiteipj_accion, null fec_vence
              from ipj.t_tramitesipj tr join ipj.t_entidades e
                  on tr.Id_Tramite_Ipj = e.Id_Tramite_Ipj
                join ipj.t_legajos l
                  on e.id_legajo = l.id_legajo
              where
                TR.ID_ESTADO_ULT < IPJ.TYPES.C_ESTADOS_COMPLETADO and
                tr.id_ubicacion_origen IN (v_ubic_acyf, v_ubic_rp, v_ubic_sxa) and
                nvl(l.eliminado, 0) = 0 and
                l.fecha_baja_entidad is null
              group by e.id_legajo, e.Denominacion_1, e.CUIT
              having
                IPJ.PKG_SND.FC_COMPARAR_FRASES(IPJ.Entidad_PersJur.FC_FORMATEAR_CADENA_COMP(p_clave), replace(IPJ.Entidad_PersJur.FC_FORMATEAR_CADENA_COMP(e.Denominacion_1), ' ', '')) +
                IPJ.PKG_SND.FC_Comparar_Fonetica(IPJ.Entidad_PersJur.FC_FORMATEAR_CADENA_COMP(p_clave), IPJ.Entidad_PersJur.FC_FORMATEAR_CADENA_COMP(e.Denominacion_1)) > 0
            ) C
         --where
         --  simil + similar_fon > 0
         group by  c.id_legajo, C.DENOMINACION_SIA, C.CUIT, c.TIPO, c.Id_Tramite_Ipj, c.id_tramiteipj_accion, fec_vence
        ) tmp
       order by Observacion desc, similar desc, Palabras asc, Observacion desc, DENOMINACION_SIA asc;

  END SP_Traer_Consulta_Nombre_Comp;


  PROCEDURE SP_Traer_PersJur_Actas_Orden(
    o_Cursor OUT TYPES.cursorType,
    p_ID_ENTIDAD_ACTA in number)
  IS
  BEGIN
  /*******************************************************
    Este procedimento devuelve la orden del d�a del acta (asamblea)
  ********************************************************/
    OPEN o_Cursor FOR
      select EAO.ID_ENTIDAD_ACTA, O.ID_TIPO_ORDEN_DIA, O.N_TIPO_ORDEN_DIA,
        O.SIGLAS
      from IPJ.T_ENTIDADES_ACTA_ORDEN eao join IPJ.T_TIPOS_ORDEN_DIA o
          on EAO.ID_TIPO_ORDEN_DIA = O.ID_TIPO_ORDEN_DIA
      where
        EAO.ID_ENTIDAD_ACTA = p_ID_ENTIDAD_ACTA;
  END SP_Traer_PersJur_Actas_Orden;

  PROCEDURE SP_Traer_Lista_Actas_Orden(
    o_Cursor OUT TYPES.cursorType,
    p_Lista_entidad_acta in varchar2)
  IS
    /*******************************************************
    Este procedimento devuelve lista de los distintos ordenes del d�a de la lista
    de asambleas pasadas por par�metro
   ********************************************************/
    v_Cursor_SQL varchar2(4000);
  BEGIN

     if IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('HAB_LOG_GESTION') = '1' then
        IPJ.VARIOS.SP_GUARDAR_LOG(
          p_METODO => 'SP_Traer_Lista_Actas_Orden',
          p_NIVEL => 'Gesti�n',
          p_ORIGEN => 'DB',
          p_USR_LOG  => 'DB',
          p_USR_WINDOWS => 'Sistema',
          p_IP_PC  => null,
          p_EXCEPCION =>
            'Lista Actas = ' || p_Lista_entidad_acta
        );
     end if;

    v_Cursor_SQL :=
      'select distinct O.ID_TIPO_ORDEN_DIA, O.N_TIPO_ORDEN_DIA, O.SIGLAS ' ||
      'from IPJ.T_ENTIDADES_ACTA_ORDEN eao join IPJ.T_TIPOS_ORDEN_DIA o ' ||
      '    on EAO.ID_TIPO_ORDEN_DIA = O.ID_TIPO_ORDEN_DIA ' ||
      'where ' ||
      '  EAO.ID_ENTIDAD_ACTA in (' ||p_Lista_entidad_acta ||') ';

    DBMS_OUTPUT.PUT_LINE('SP_Traer_Lista_Actas_Orden: ' || v_Cursor_SQL);

    OPEN o_Cursor FOR
      v_Cursor_SQL;

  END SP_Traer_Lista_Actas_Orden;

  PROCEDURE SP_Traer_PersJur_Actas_Arch(
    o_Cursor OUT TYPES.cursorType,
    p_ID_ENTIDAD_ACTA in number,
    p_id_tipo_acta in number)
  IS
  BEGIN
  /*******************************************************
    Este procedimento devuelve el listado de archivos de un acta (asamblea)
  ********************************************************/
    OPEN o_Cursor FOR
      select AA.ID_ARCHIVO, AA.ABREVIACION, AA.N_ARCHIVO,
        EAA.ID_ENTIDAD_ACTA, to_char(EAA.FECHA, 'dd/mm/rrrr') FECHA,
        EAA.ID_INTEGRANTE, EAA.ID_TIPO_ACTA, I.DETALLE
      from IPJ.T_ARCHIVOS_ACTA aa left join IPJ.T_ENTIDADES_ACTA_ARCH eaa
          on EAA.ID_TIPO_ACTA = AA.ID_TIPO_ACTA and EAA.ID_ARCHIVO = AA.ID_ARCHIVO and EAA.ID_ENTIDAD_ACTA = P_ID_ENTIDAD_ACTA
        left join IPJ.T_INTEGRANTES i
          on EAA.ID_INTEGRANTE = I.ID_INTEGRANTE
      where
        AA.ID_TIPO_ACTA = p_ID_TIPO_ACTA;

  END SP_Traer_PersJur_Actas_Arch;

  PROCEDURE SP_Traer_PersJur_Balances(
    o_Cursor OUT TYPES.cursorType,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number)
  IS
    v_matricula_version number(6,0);
    v_ubicacion_ent number(6);
    v_ubicacion_leg number(6);
  BEGIN
  /*******************************************************
    Este procedimento lista los balances de una entidad, si no son borrador,
    o estan en el tramite indicado
  ********************************************************/
    begin
      select nvl(matricula_version, 0),
        (select id_ubicacion from ipj.t_tipos_entidades te where te.id_tipo_entidad = e.id_tipo_entidad) ubicacion_ent,
        (select id_ubicacion from ipj.t_tipos_entidades te where te.id_tipo_entidad = l.id_tipo_entidad) ubicacion_leg
        into v_matricula_version, v_ubicacion_ent, v_ubicacion_leg
      from IPJ.t_entidades e join ipj.t_legajos l
        on e.id_legajo = l.id_legajo
      where
        e.Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        e.id_legajo = p_id_legajo;
    exception
      when NO_DATA_FOUND then
        v_matricula_version := 0;
    end;

    OPEN o_Cursor FOR
      select Eb.Id_Tramite_Ipj, Eb.Id_Legajo, Eb.Id_Integrante, eb.irregular,
        To_Char(Eb.Fecha, 'dd/mm/rrrr') Fecha, Eb.Matricula, Eb.Observacion,
        Trim(To_Char(Nvl(Eb.Monto, '0'), '99999999999999999990.99')) Monto, Eb.Reserva_Legal, Eb.Borrador,
        Trim(To_Char(Nvl(Eb.Activos, '0'), '99999999999999999990.99')) Activos,
        Trim(To_Char(Nvl(Eb.Pasivos, '0'), '99999999999999999990.99')) Pasivos,
        Trim(To_Char(Nvl(Eb.Neto, '0'), '99999999999999999990.99')) Neto,
        Trim(To_Char(Nvl(Eb.Ingreso_Ejerc, '0'), '99999999999999999990.99')) Ingreso_Ejerc,
        Trim(To_Char(Nvl(Eb.Costo_Ejerc, '0'), '99999999999999999990.99')) Costo_Ejerc,
        Trim(To_Char(Nvl(Eb.Result_Ejerc, '0'), '99999999999999999990.99')) Result_Ejerc,
        to_char(Eb.Fecha_Certif, 'dd/mm/rrrr') Fecha_Certif, Eb.Id_Tipo_Dictamen,
        nvl(I.Detalle, eb.detalle_contador) Detalle, I.Nro_Documento, I.Id_Numero,
        I.Id_Sexo, I.Id_Vin, I.Pai_Cod_Pais, I.Cuil,
        Nvl(Tr.Expediente, IPJ.TRAMITES_SUAC.FC_BUSCAR_EXPEDIENTE(tr.id_tramite)) Nro_Expediente,
        (Case When Upper(Nvl(Eb.Reserva_Legal, 'N')) = 'S' Then 'Si' Else 'No' End) N_Reserva_Legal,
        P_Id_Tramite_Ipj Id_Tramite_Ipj_Actual,
        (select N_estado from Ipj.T_Estados es where es.id_estado = Tr.Id_Estado_Ult) N_Estado,
        Tr.Id_Estado_Ult id_estado
      from ipj.t_entidades_balance eb left join IPJ.t_integrantes I
          on I.ID_INTEGRANTE = EB.ID_INTEGRANTE
        join ipj.t_entidades e
          on e.Id_Tramite_Ipj = eb.Id_Tramite_Ipj and e.id_legajo = eb.id_legajo
        join ipj.t_tramitesipj tr
          on TR.Id_Tramite_Ipj = e.Id_Tramite_Ipj
      where
        eb.id_legajo = p_id_legajo and
        e.matricula_version <= v_matricula_version and
        e.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_ubicacion_leg) and
        (tr.Id_Estado_Ult <= 200 or Tr.Id_Estado_Ult = 210) -- Oculto Estados rechazados distintos del Inactivo
   UNION ALL
   select Eb.Id_Tramite_Ipj, Eb.Id_Legajo, Eb.Id_Integrante, eb.irregular,
        To_Char(Eb.Fecha, 'dd/mm/rrrr') Fecha, Eb.Matricula, Eb.Observacion,
        Trim(To_Char(Nvl(Eb.Monto, '0'), '99999999999999999990.99')) Monto, Eb.Reserva_Legal, Eb.Borrador,
        Trim(To_Char(Nvl(Eb.Activos, '0'), '99999999999999999990.99')) Activos,
        Trim(To_Char(Nvl(Eb.Pasivos, '0'), '99999999999999999990.99')) Pasivos,
        Trim(To_Char(Nvl(Eb.Neto, '0'), '99999999999999999990.99')) Neto,
        Trim(To_Char(Nvl(Eb.Ingreso_Ejerc, '0'), '99999999999999999990.99')) Ingreso_Ejerc,
        Trim(To_Char(Nvl(Eb.Costo_Ejerc, '0'), '99999999999999999990.99')) Costo_Ejerc,
        Trim(To_Char(Nvl(Eb.Result_Ejerc, '0'), '99999999999999999990.99')) Result_Ejerc,
        to_char(Eb.Fecha_Certif, 'dd/mm/rrrr') Fecha_Certif, Eb.Id_Tipo_Dictamen,
        nvl(I.Detalle, eb.detalle_contador) Detalle, I.Nro_Documento, I.Id_Numero,
        I.Id_Sexo, I.Id_Vin, I.Pai_Cod_Pais, I.Cuil,
        Nvl(Tr.Expediente, IPJ.TRAMITES_SUAC.FC_BUSCAR_EXPEDIENTE(tr.id_tramite)) Nro_Expediente,
        (Case When Upper(Nvl(Eb.Reserva_Legal, 'N')) = 'S' Then 'Si' Else 'No' End) N_Reserva_Legal,
        P_Id_Tramite_Ipj Id_Tramite_Ipj_Actual,
        (select N_estado from Ipj.T_Estados es where es.id_estado = Tr.Id_Estado_Ult) N_Estado,
        Tr.Id_Estado_Ult id_estado
      from ipj.t_entidades_balance eb left join IPJ.t_integrantes I
          on I.ID_INTEGRANTE = EB.ID_INTEGRANTE
        join ipj.t_entidades e
          on e.Id_Tramite_Ipj = eb.Id_Tramite_Ipj and e.id_legajo = eb.id_legajo
        join ipj.t_tramitesipj tr
          on TR.Id_Tramite_Ipj = e.Id_Tramite_Ipj
      where
        eb.id_legajo = (SELECT max(e.id_legajo) FROM ipj.t_entidades e WHERE e.id_tramite_ipj = tr.id_tramite_ipj) AND
        tr.id_tramite_ipj_padre = P_Id_Tramite_Ipj AND
        e.matricula_version <= v_matricula_version and
        e.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_ubicacion_leg) and
        (tr.Id_Estado_Ult < 200 or Tr.Id_Estado_Ult = 210) -- Oculto Estados rechazados distintos del Inactivo
      order BY FECHA desc;
  END SP_Traer_PersJur_Balances;

  PROCEDURE SP_Traer_PersJur_Notif(
    o_Cursor OUT TYPES.cursorType,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number)
  IS
  BEGIN
  /*******************************************************
    Este procedimento las notificaciones de un tr�mite de una entidad
  ********************************************************/
    OPEN o_Cursor FOR
      select EN.NRO, EN.ID_ESTADO_NOTA , to_char(EN.FEC_CUMPL, 'dd/mm/rrrr') FEC_CUMPL,
        to_char(EN.FEC_MODIF, 'dd/mm/rrrr') FEC_MODIF, EN.id_tramite_ipj,
        EN.ID_LEGAJO, EN.OBSERVACION, en.informar_suac, TEN.N_ESTADO_NOTA,
        (case when instr(en.observacion, chr(13)) <> 0 then substr(en.observacion, 1, instr(en.observacion, chr(13)))
            else en.Observacion
         end) Ultima_Linea,
         nvl(en.es_nuevo, 'N') es_nuevo,
         en.id_nota_sistema
      from ipj.t_entidades_notas en left join ipj.t_tramitesipj_persjur tpj
          on tpj.Id_Tramite_Ipj = en.Id_Tramite_Ipj and tpj.id_legajo = en.id_legajo
        join ipj.t_tipos_estado_nota ten
          on tEN.ID_ESTADO_NOTA = EN.ID_ESTADO_NOTA
      where
--        (nvl(p_id_legajo, 0) = 0 or en.id_legajo = p_id_legajo) and
        En.Id_Tramite_Ipj = p_Id_Tramite_Ipj
      order by
        En.nro asc;

  END SP_Traer_PersJur_Notif;

  PROCEDURE SP_Traer_PersJur_Sindicos(
    o_Cursor OUT TYPES.cursorType,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number)
  IS
    v_matricula_version number(6,0);
    v_ubicacion_ent number(6);
    v_ubicacion_leg number(6);
    v_dias_vigencia number;
    v_codigo_online NUMBER;
  BEGIN
  /*******************************************************
    Este procedimento lista los sindicos de una entidad, si no son borrador,
    o estan en el tramite indicado
  ********************************************************/
    begin
      select nvl(matricula_version, 0),
        (select id_ubicacion from ipj.t_tipos_entidades te where te.id_tipo_entidad = e.id_tipo_entidad) ubicacion_ent,
        (select id_ubicacion from ipj.t_tipos_entidades te where te.id_tipo_entidad = l.id_tipo_entidad) ubicacion_leg
        into v_matricula_version, v_ubicacion_ent, v_ubicacion_leg
      from IPJ.t_entidades e join ipj.t_legajos l
        on e.id_legajo = l.id_legajo
      where
        e.Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        e.id_legajo = p_id_legajo;
    exception
      when NO_DATA_FOUND then
        v_matricula_version := 0;
    end;

     BEGIN
       SELECT codigo_online
         INTO v_codigo_online
         FROM ipj.t_tramitesipj
        WHERE id_tramite_ipj = p_id_tramite_ipj;
     EXCEPTION
       WHEN no_data_found THEN
         NULL;
     END;

    -- (PBI 9917) Busco los d�as de vigencia configurados
    v_dias_vigencia := to_number(IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('DIAS_ADMIN_LEY'));

    OPEN o_Cursor FOR
      select Es.Id_Tramite_Ipj, Es.Id_Legajo, Es.Id_Integrante,
        To_Char(Es.Fecha_Alta, 'dd/mm/rrrr') Fec_Alta, To_Char(Es.Fecha_Baja, 'dd/mm/rrrr') Fec_Baja,
        Es.Id_Tipo_Integrante Id_Tipo_Sindico, Es.Id_Tipo_Integrante,
        Es.Matricula, Es.Borrador,  Ti.N_Tipo_Integrante N_Tipo_Sindico, Ti.N_Tipo_Integrante,
        Es.Id_Legajo_Sindico, Es.Cuit_Empresa, Es.N_Empresa, Es.Id_Tipo_Entidad,
        Es.Matricula_Empresa, es.Id_Entidades_Accion, Es.En_Formacion,
        to_char(Es.Fec_Acta, 'dd/mm/rrrr') Fec_Acta, Eacc.Clase,
        Es.Id_Entidad_Sindico, org.N_Tipo_Organismo, org.es_admin, es.id_motivo_baja,
        es.folio, es.anio,
        I.Detalle, I.Nro_Documento, I.Id_Numero, I.Id_Sexo, I.Id_Vin, I.Pai_Cod_Pais, I.Cuil,
        (case
          when es.n_empresa is not null then es.n_empresa
          when es.id_integrante is not null then (select p.nombre||' '||p.apellido from rcivil.vt_pk_persona p where p.id_sexo = i.id_sexo and p.nro_documento = i.nro_documento and p.pai_cod_pais = i.pai_cod_pais and p.id_numero = i.id_numero)
        end) Persona,
        (case
           when es.n_empresa is not null then es.cuit_empresa
           when es.id_integrante is not null then I.NRO_DOCUMENTO
         end) Identificacion,
         p_Id_Tramite_Ipj IdTramiteIpj_Entidad, mb.n_motivo_baja, es.persona_expuesta,
         ti.nro_orden ti_nro_orden, org.nro_orden org_nro_orden,
         to_char(es.fecha_baja + v_dias_vigencia, 'dd/mm/rrrr') fin_vigencia, -- (PBI 9917)
         (select expediente from ipj.t_tramitesipj tr where tr.id_tramite_ipj = es.id_tramite_ipj) Expediente,
         ipj.ENTIDAD_PERSJUR.FC_Fecha_Orig_Sindico(es.id_legajo, es.id_entidad_sindico) Fecha_Fin_Orig,
         ti.id_tipo_organismo, ti.es_suplente,(SELECT cuil
                                                FROM ipj.t_integrantes
                                               WHERE id_integrante IN (SELECT id_integrante_repres
                                                                         FROM ipj.t_ol_entidad_admin ead
                                                                        WHERE ead.codigo_online = v_codigo_online
                                                                          AND ead.cuit_empresa = Es.Cuit_Empresa))Cuil_Repres_Sindicos,
         (SELECT NVL(i.Cuil,p2.cuil)
            FROM ipj.t_entidades_representante er
            WHERE er.id_integrante = i.id_integrante
              AND fecha_baja IS NULL
            GROUP BY er.id_integrante)Cuil_Repres
      from ipj.t_entidades_sindico es left join IPJ.t_integrantes I
          on I.ID_INTEGRANTE = Es.ID_INTEGRANTE
        left join IPJ.T_TIPOS_INTEGRANTE ti
          on ES.ID_TIPO_INTEGRANTE = TI.ID_TIPO_INTEGRANTE
        left join ipj.t_tipos_organismo org
          on org.id_tipo_organismo = ti.id_tipo_organismo
        left join ipj.t_tipos_motivos_baja mb
          on es.id_motivo_baja = mb.id_motivo_baja
        left join ipj.t_entidades e
          on e.Id_Tramite_Ipj = es.Id_Tramite_Ipj and e.id_legajo = es.id_legajo
        left join ipj.t_entidades_acciones eacc
          on eacc.id_legajo = es.id_legajo and eacc.Id_Entidades_Accion = es.Id_Entidades_Accion
        --se agrega para cuil representante
        left join RCIVIL.VT_PERSONAS_CUIL p2
          on p2.id_sexo = i.id_sexo and p2.nro_documento = i.nro_documento and p2.pai_cod_pais = i.pai_cod_pais and p2.id_numero = i.id_numero
      where
        es.id_legajo = p_id_legajo and
        e.matricula_version <= v_matricula_version and
        e.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_ubicacion_leg) and
        (es.borrador = 'N' or Es.Id_Tramite_Ipj = p_Id_Tramite_Ipj) and
        (es.fecha_baja is null or (es.fecha_baja + v_dias_vigencia) >= to_date(sysdate, 'dd/mm/rrrr') or es.Id_Tramite_Ipj = p_Id_Tramite_Ipj)
    UNION ALL
    select Es.Id_Tramite_Ipj, Es.Id_Legajo, Es.Id_Integrante,
        To_Char(Es.Fecha_Alta, 'dd/mm/rrrr') Fec_Alta, To_Char(Es.Fecha_Baja, 'dd/mm/rrrr') Fec_Baja,
        Es.Id_Tipo_Integrante Id_Tipo_Sindico, Es.Id_Tipo_Integrante,
        Es.Matricula, Es.Borrador,  Ti.N_Tipo_Integrante N_Tipo_Sindico, Ti.N_Tipo_Integrante,
        Es.Id_Legajo_Sindico, Es.Cuit_Empresa, Es.N_Empresa, Es.Id_Tipo_Entidad,
        Es.Matricula_Empresa, es.Id_Entidades_Accion, Es.En_Formacion,
        to_char(Es.Fec_Acta, 'dd/mm/rrrr') Fec_Acta, Eacc.Clase,
        Es.Id_Entidad_Sindico, org.N_Tipo_Organismo, org.es_admin, es.id_motivo_baja,
        es.folio, es.anio,
        I.Detalle, I.Nro_Documento, I.Id_Numero, I.Id_Sexo, I.Id_Vin, I.Pai_Cod_Pais, I.Cuil,
        (case
          when es.n_empresa is not null then es.n_empresa
          when es.id_integrante is not null  then (select p.nombre||' '||p.apellido from rcivil.vt_pk_persona p where p.id_sexo = i.id_sexo and p.nro_documento = i.nro_documento and p.pai_cod_pais = i.pai_cod_pais and p.id_numero = i.id_numero)
        end) Persona,
        (case
           when es.n_empresa is not null then es.cuit_empresa
           when es.id_integrante is not null then I.NRO_DOCUMENTO
         end) Identificacion,
         p_Id_Tramite_Ipj IdTramiteIpj_Entidad, mb.n_motivo_baja, es.persona_expuesta,
         ti.nro_orden ti_nro_orden, org.nro_orden org_nro_orden,
         to_char(es.fecha_baja + v_dias_vigencia, 'dd/mm/rrrr') fin_vigencia, -- (PBI 9917)
         (select expediente from ipj.t_tramitesipj tr where tr.id_tramite_ipj = es.id_tramite_ipj) Expediente,
         ipj.ENTIDAD_PERSJUR.FC_Fecha_Orig_Sindico(es.id_legajo, es.id_entidad_sindico) Fecha_Fin_Orig,
         ti.id_tipo_organismo, ti.es_suplente,(SELECT cuil
                                                FROM ipj.t_integrantes
                                               WHERE id_integrante IN (SELECT id_integrante_repres
                                                                         FROM ipj.t_ol_entidad_admin ead
                                                                        WHERE ead.codigo_online = v_codigo_online
                                                                          AND ead.cuit_empresa = Es.Cuit_Empresa))Cuil_Repres_Sindicos,
         (SELECT NVL(i.Cuil,p2.cuil)
            FROM ipj.t_entidades_representante er
            WHERE er.id_integrante = i.id_integrante
              AND fecha_baja IS NULL
            GROUP BY er.id_integrante)Cuil_Repres
      from ipj.t_entidades_sindico es left join IPJ.t_integrantes I
          on I.ID_INTEGRANTE = Es.ID_INTEGRANTE
        left join IPJ.T_TIPOS_INTEGRANTE ti
          on ES.ID_TIPO_INTEGRANTE = TI.ID_TIPO_INTEGRANTE
        left join ipj.t_tipos_organismo org
          on org.id_tipo_organismo = ti.id_tipo_organismo
        left join ipj.t_tipos_motivos_baja mb
          on es.id_motivo_baja = mb.id_motivo_baja
        left join ipj.t_entidades e
          on e.Id_Tramite_Ipj = es.Id_Tramite_Ipj and e.id_legajo = es.id_legajo
        left join ipj.t_entidades_acciones eacc
          on eacc.id_legajo = es.id_legajo and eacc.Id_Entidades_Accion = es.Id_Entidades_Accion
        JOIN ipj.t_tramitesipj tra
          ON es.id_tramite_ipj = tra.id_tramite_ipj
        --se agrega para cuil representante
        left join RCIVIL.VT_PERSONAS_CUIL p2
          on p2.id_sexo = i.id_sexo and p2.nro_documento = i.nro_documento and p2.pai_cod_pais = i.pai_cod_pais and p2.id_numero = i.id_numero
      where
        es.id_legajo = (SELECT max(e.id_legajo) FROM ipj.t_entidades e WHERE e.id_tramite_ipj = tra.id_tramite_ipj) and
        e.matricula_version <= v_matricula_version and
        e.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_ubicacion_leg) and
        tra.id_tramite_ipj_padre = p_Id_Tramite_Ipj and
        (es.fecha_baja is null or (es.fecha_baja + v_dias_vigencia) >= to_date(sysdate, 'dd/mm/rrrr') or tra.Id_Tramite_Ipj_padre = p_Id_Tramite_Ipj)
      order by Id_Tramite_Ipj, org_nro_orden asc, ti_nro_orden asc, detalle asc;

  END SP_Traer_PersJur_Sindicos;


  PROCEDURE SP_Traer_PersJur_Represent(
    o_Cursor OUT TYPES.cursorType,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number)
  IS
    v_matricula_version number(6,0);
    v_ubicacion_ent number(6);
    v_ubicacion_leg number(6);
  BEGIN
  /*******************************************************
    Este procedimento lista los representates activos de una entidad, si no son borrador,
    o estan en el tramite indicado
  ********************************************************/
    begin
      select nvl(matricula_version, 0),
        (select id_ubicacion from ipj.t_tipos_entidades te where te.id_tipo_entidad = e.id_tipo_entidad) ubicacion_ent,
        (select id_ubicacion from ipj.t_tipos_entidades te where te.id_tipo_entidad = l.id_tipo_entidad) ubicacion_leg
        into v_matricula_version, v_ubicacion_ent, v_ubicacion_leg
      from IPJ.t_entidades e join ipj.t_legajos l
        on e.id_legajo = l.id_legajo
      where
        e.Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        e.id_legajo = p_id_legajo;
    exception
      when NO_DATA_FOUND then
        v_matricula_version := 0;
    end;

    OPEN o_Cursor FOR
      select er.id_tramite_ipj, er.id_legajo, er.id_integrante,
        to_char(er.fecha_baja, 'dd/mm/rrrr') fec_baja, to_char(er.fecha_alta, 'dd/mm/rrrr') fec_alta, er.borrador,
        (select p.nombre||' '||p.apellido from rcivil.vt_pk_persona p where p.id_sexo = i.id_sexo and p.nro_documento = i.nro_documento and p.pai_cod_pais = i.pai_cod_pais and p.id_numero = i.id_numero) detalle,
        i.nro_documento, i.id_numero, i.id_sexo, i.id_vin, i.pai_cod_pais, i.cuil,
        er.id_motivo_baja, (select n_motivo_baja from IPJ.T_TIPOS_MOTIVOS_BAJA b where b.id_motivo_baja = er.id_motivo_baja) n_motivo_baja,
        p_Id_Tramite_Ipj Id_Tramite_Ipj_Entidad
      from ipj.t_entidades_representante er join IPJ.t_integrantes I
          on I.ID_INTEGRANTE = Er.ID_INTEGRANTE
        join ipj.t_entidades e
          on e.Id_Tramite_Ipj = er.Id_Tramite_Ipj and e.id_legajo = er.id_legajo
      where
        er.id_legajo = p_id_legajo and
        e.matricula_version <= v_matricula_version and
        e.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_ubicacion_leg) and
        ((er.borrador = 'N' and ER.FECHA_BAJA is null) or
          Er.Id_Tramite_Ipj = p_Id_Tramite_Ipj)
    UNION ALL
      select er.id_tramite_ipj, er.id_legajo, er.id_integrante,
        to_char(er.fecha_baja, 'dd/mm/rrrr') fec_baja, to_char(er.fecha_alta, 'dd/mm/rrrr') fec_alta, er.borrador,
        (select p.nombre||' '||p.apellido from rcivil.vt_pk_persona p where p.id_sexo = i.id_sexo and p.nro_documento = i.nro_documento and p.pai_cod_pais = i.pai_cod_pais and p.id_numero = i.id_numero) detalle,
        i.nro_documento, i.id_numero, i.id_sexo, i.id_vin, i.pai_cod_pais, i.cuil,
        er.id_motivo_baja, (select n_motivo_baja from IPJ.T_TIPOS_MOTIVOS_BAJA b where b.id_motivo_baja = er.id_motivo_baja) n_motivo_baja,
        p_Id_Tramite_Ipj Id_Tramite_Ipj_Entidad
        from ipj.t_entidades_representante er join IPJ.t_integrantes I
          on I.ID_INTEGRANTE = Er.ID_INTEGRANTE
        join ipj.t_entidades e
          on e.Id_Tramite_Ipj = er.Id_Tramite_Ipj and e.id_legajo = er.id_legajo
        JOIN ipj.t_tramitesipj tra
          ON e.id_tramite_ipj = tra.id_tramite_ipj
      where
        er.id_legajo = (SELECT max(e.id_legajo) FROM ipj.t_entidades e WHERE e.id_tramite_ipj = tra.id_tramite_ipj) and
        e.matricula_version <= v_matricula_version and
        e.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_ubicacion_leg) and
        tra.id_tramite_ipj_padre = p_Id_Tramite_Ipj;

  END SP_Traer_PersJur_Represent;

  PROCEDURE SP_Traer_PersJur_Organos(
    o_Cursor OUT TYPES.cursorType,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number)
  IS
    v_matricula_version number(6,0);
    v_ubicacion_ent number(6);
    v_ubicacion_leg number(6);
  BEGIN
  /*******************************************************
    Este procedimento lista Organos y sus duracion de cargos
  ********************************************************/
    begin
      select nvl(matricula_version, 0),
        (select id_ubicacion from ipj.t_tipos_entidades te where te.id_tipo_entidad = e.id_tipo_entidad) ubicacion_ent,
        (select id_ubicacion from ipj.t_tipos_entidades te where te.id_tipo_entidad = l.id_tipo_entidad) ubicacion_leg
        into v_matricula_version, v_ubicacion_ent, v_ubicacion_leg
      from IPJ.t_entidades e join ipj.t_legajos l
        on e.id_legajo = l.id_legajo
      where
        e.Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        e.id_legajo = p_id_legajo;
    exception
      when NO_DATA_FOUND then
        v_matricula_version := 0;
    end;

    OPEN o_Cursor FOR
      select eo.Id_Tramite_Ipj, eo.id_legajo, eo.id_tipo_organismo, eo.cant_meses,
        Eo.Id_Tipo_Duracion, Eo.Max_Titular, Eo.Min_Titular, Eo.Suplente_Igual,
        Eo.Suplente_Mayor, Eo.Suplente_Menor, Eo.Max_Suplente, Eo.Min_Suplente,
        eo.observacion, eo.id_tipo_administracion, eo.periodos_reeleccion,
        o.N_Tipo_Organismo, o.Es_Admin,
        (case when eo.duracion_indefinida = 'S' then 'Indefinido' else d.N_Tipo_Duracion end) N_Tipo_Duracion,
        eo.duracion_indefinida, eo.id_tipo_administracion, ta.n_tipo_administracion
      from ipj.T_Entidades_Organismo eo join ipj.t_tipos_organismo o
          on eo.Id_Tipo_Organismo = o.Id_Tipo_Organismo
        join ipj.t_entidades e
          on eo.Id_Tramite_Ipj = e.Id_Tramite_Ipj and eo.id_legajo = e.id_legajo
        left join IPJ.T_TIPOS_DURACION d
          on eo.Id_Tipo_Duracion = d.Id_Tipo_Duracion
        left join IPJ.T_TIPOS_ADMINISTRACION ta
          on ta.id_tipo_administracion = eo.id_tipo_administracion
      where
        eo.id_legajo = p_id_legajo and
        e.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_ubicacion_leg) and
        eo.fecha_baja is null
     ;

  END SP_Traer_PersJur_Organos;

  PROCEDURE SP_GUARDAR_ENTIDAD_BALANCES(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number,
    p_fecha in varchar2, -- es fecha
    p_monto in varchar2,
    p_reserva_legal in varchar2,
    p_borrador in varchar2,
    p_Activos in varchar2,
    p_Pasivos in varchar2,
    p_Neto in varchar2,
    p_Ingreso_Ejerc in varchar2,
    p_Costo_Ejerc in varchar2,
    p_Result_Ejerc in varchar2,
    p_Fecha_Certif in varchar2, -- es fecha
    p_matricula in varchar2,
    p_id_integrante in number,
    p_DETALLE in varchar2,
    p_Nombre in varchar2,
    p_Apellido in varchar2,
    p_NRO_DOCUMENTO in varchar2,
    p_ID_NUMERO in number,
    p_ID_SEXO in varchar2,
    p_ID_VIN in number,
    p_PAI_COD_PAIS in varchar2,
    p_CUIL in varchar2,
    p_error_dato in varchar2,
    p_n_tipo_documento in varchar2,
    p_eliminar in number, -- 1 elimina
    p_observacion in varchar2,
    p_id_tipo_dictamen in number,
    p_irregular in char)
  IS
   /*************************************************************
      Guarda los balances asociados a una entidad de un tramite.
      No maneja transaccion, ya que se usa en cascada desde la aplicacion y lo administra el codigo.
  *************************************************************/
    v_existe number;
    v_tipo_mensaje number;
    v_id_integrante number;
    v_valid_parametros varchar2(500);
    v_cierre_ejerc date;
    v_cierre_fin_mes char(1);
  BEGIN
    if IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('HAB_LOG_GESTION') = '1' then
      IPJ.VARIOS.SP_GUARDAR_LOG(
        p_METODO => 'SP_GUARDAR_ENTIDAD_BALANCES',
        p_NIVEL => 'Gestion - Balances',
        p_ORIGEN => 'DB',
        p_USR_LOG  => 'DB',
        p_USR_WINDOWS => 'Sistema',
        p_IP_PC  => null,
        p_EXCEPCION =>
          'Id Tramite Ipj = ' || to_char(p_Id_Tramite_Ipj)
          || ' / Id Legajo = ' || to_char(p_id_legajo)
          || ' / Fecha = ' || p_fecha
          || ' / Monto = ' || p_monto
          || ' / Reserva Legal = ' || p_reserva_legal
          || ' / Borrador = ' || p_borrador
          || ' / Activos = ' || p_Activos
          || ' / Pasivos = ' || p_Pasivos
          || ' / Neto = ' || p_Neto
          || ' / Ing Ejerc = ' || p_Ingreso_Ejerc
          || ' / Costo Ejerc = ' || p_Costo_Ejerc
          || ' / Result Ejerc = ' || p_Result_Ejerc
          || ' / Fecha Certif = ' || p_Fecha_Certif
          || ' / Matricula = ' || p_matricula
          || ' / Id Integrante = ' || to_char(p_id_integrante)
          || ' / Detalle = ' || p_DETALLE
          || ' / Nombre = ' || p_Nombre
          || ' / Apellido = ' || p_Apellido
          || ' / DNI = ' || p_NRO_DOCUMENTO
          || ' / Id Numero = ' || to_char(p_ID_NUMERO)
          || ' / Id Sexo = ' || p_ID_SEXO
          || ' / Id Vin = ' || to_char(p_ID_VIN)
          || ' / Pai Cod Pais = ' || p_PAI_COD_PAIS
          || ' / CUIL = ' || p_CUIL
          || ' / Error Dato = ' || p_error_dato
          || ' / Tipo DNI = ' || p_n_tipo_documento
          || ' / Eliminar = ' || to_char(p_eliminar )
          || ' / Obs = ' || p_observacion
          || ' / Id Tipo Dictamen = ' || p_id_tipo_dictamen
          || ' / Irregular = ' || p_irregular
      );
    end if;

  -- VALIDACION DE PARAMETROS
    if p_eliminar <> 1 and (IPJ.VARIOS.Valida_Fecha(p_fecha) = 'N'  or to_date(p_fecha, 'dd/mm/rrrr') > sysdate) then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT') || ' (Fec. Estado Contable)';
    end if;
    if p_Fecha_Certif is not null and IPJ.VARIOS.Valida_Fecha(p_Fecha_Certif) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT') || ' (Fec. Certif)';
    end if;
    if IPJ.VARIOS.Valida_Entidad(nvl(p_Id_Tramite_Ipj, 0), p_id_legajo) = 0 then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('ENT_NOT')|| '(' || to_char(p_Id_Tramite_Ipj) ||' - ' || to_char(p_id_legajo)||')';
    end if;
    if p_monto is not null and nvl(IPJ.VARIOS.ToNumber(p_monto), -1) < 0 then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('MONTO_NOT') || ' (Monto)';
    end if;
    if p_Activos is not null and nvl(IPJ.VARIOS.ToNumber(p_Activos), -1) < 0 then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('MONTO_NOT') || ' (Activos)';
    end if;
    if p_Pasivos is not null and nvl(IPJ.VARIOS.ToNumber(p_Pasivos), -1) < 0 then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('MONTO_NOT') || ' (Pasivos)';
    end if;
    if p_Neto is not null and IPJ.VARIOS.ToNumber(p_Neto) is null then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('MONTO_NOT') || ' (Neto)';
    end if;
    if p_Ingreso_Ejerc is not null and nvl(IPJ.VARIOS.ToNumber(p_Ingreso_Ejerc), -1) < 0 then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('MONTO_NOT') || ' (Ingreso Ejercicio)';
    end if;
    if p_Costo_Ejerc is not null and nvl(IPJ.VARIOS.ToNumber(p_Costo_Ejerc), -1) < 0 then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('MONTO_NOT') || ' (Egreso Ejercicio)';
    end if;
    if p_Result_Ejerc is not null and IPJ.VARIOS.ToNumber(p_Result_Ejerc) is null then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('MONTO_NOT') || ' (Resultado Ejercicio)';
    end if;


    if v_valid_parametros is not null then
      o_rdo := 'GUARDAR BALANCES - Parametros: '  || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    -- CUERPO PROCEDIMIENTO
    if p_eliminar = 1 then
      delete ipj.t_entidades_balance
      where
        id_legajo = p_id_legajo and
        Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        fecha = to_date(p_fecha, 'dd/mm/rrrr') and
        borrador = 'S';

    else
      -- Si no viene el ID_INTEGRANTE lo agrego
      if IPJ.VARIOS.Valida_Integrante(p_id_integrante) = 0 then

        if p_nombre is null and p_apellido is null and p_n_tipo_documento is null then
          v_id_integrante := 0;
        else
          IPJ.ENTIDAD_PERSJUR.SP_GUARDAR_INTEGRANTES(
            o_rdo => v_valid_parametros,
            o_tipo_mensaje => v_tipo_mensaje,
            o_id_integrante => v_id_integrante,
            p_id_sexo => p_id_sexo,
            p_nro_documento => p_nro_documento,
            p_pai_cod_pais => p_pai_cod_pais,
            p_id_numero => p_id_numero,
            p_cuil => p_cuil,
            p_detalle => p_detalle,
            p_Nombre => p_Nombre,
            p_Apellido => p_Apellido,
            p_error_dato => p_error_dato,
            p_n_tipo_documento => p_n_tipo_documento );

          if v_valid_parametros <> TYPES.c_Resp_OK then
            o_rdo := IPJ.VARIOS.MENSAJE_ERROR('INT_NOT') || '. ' || v_valid_parametros;
            o_tipo_mensaje := v_tipo_mensaje;
            return;
          end if;
        end if;
      else
         if p_error_dato = 'S' then
           update ipj.t_integrantes
           set
             error_dato = p_error_dato,
             detalle = p_detalle
           where
             id_integrante = p_id_integrante;
         end if;
         v_id_integrante := p_id_integrante;
       end if;

      v_id_integrante := (case when v_id_integrante = 0 then null else v_id_integrante end);
      -- Si existe un registro igual no hago nada
      update ipj.t_entidades_balance
      set
        monto = to_number (p_monto, '99999999999999999990.99'),
        reserva_legal = p_reserva_legal,
        matricula = p_matricula,
        observacion = p_observacion,
        activos =  to_number (p_Activos, '99999999999999999990.99'),
        pasivos = to_number (p_Pasivos, '99999999999999999990.99'),
        neto = to_number (p_Neto, '99999999999999999990.99'),
        Ingreso_Ejerc = to_number (p_Ingreso_Ejerc, '99999999999999999990.99'),
        Costo_Ejerc = to_number (p_Costo_Ejerc, '99999999999999999990.99'),
        Result_Ejerc = to_number (p_Result_Ejerc, '99999999999999999990.99'),
        Fecha_Certif = to_date(p_Fecha_Certif, 'dd/mm/rrrr'),
        Id_Tipo_Dictamen = (case when p_Id_Tipo_Dictamen = 0 then null else p_Id_Tipo_Dictamen end),
        detalle_contador = p_detalle
      where
        id_legajo = p_id_legajo and
        Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        nvl(id_integrante, 0) = nvl(v_id_integrante, 0) and
        fecha = to_date(p_fecha, 'dd/mm/rrrr');

      -- Si no existe, se inserta
      if sql%rowcount = 0 then
        -- Controlo que la fecha del ejercicio corresponda con la fecha de cierre
        select E.Cierre_Ejercicio, nvl(e.Cierre_Fin_Mes, 'N')  into v_cierre_ejerc, v_cierre_fin_mes
        from ipj.t_entidades e
        where
          Id_Tramite_Ipj = p_Id_Tramite_Ipj and
          id_legajo = p_id_legajo;

        if v_cierre_fin_mes = 'N' then
          if v_cierre_ejerc is null then
            o_rdo := 'GUARDAR BALANCES - Parametros: No posee cierre de ejercicio configurado.';
            o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
            return;
          end if;

          if nvl(p_irregular, 'N') = 'N'  and to_char(v_cierre_ejerc, 'dd/mm') <> to_char(to_date(p_fecha, 'dd/mm/rrrr'), 'dd/mm') then
            o_rdo := 'GUARDAR BALANCES - Parametros: La fecha de balance no se correspone con el cierre de ejercicio.';
            o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
            return;
          end if;
        else
          if nvl(p_irregular, 'N') = 'N'  and to_char(to_date('01/03/' ||to_char(to_date(p_fecha, 'dd/mm/rrrr'), 'rrrr'), 'dd/mm/rrrr')-1, 'dd/mm') <> to_char(to_date(p_fecha, 'dd/mm/rrrr'), 'dd/mm') then
            o_rdo := 'GUARDAR BALANCES - Parametros: La fecha no se correspone con el cierre de ejercicio.';
            o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
            return;
          end if;
        end if;

        insert into ipj.t_entidades_balance
          (Id_Tramite_Ipj, id_legajo, fecha, monto, reserva_legal, borrador, matricula,
          id_integrante, observacion, activos, pasivos, neto, Ingreso_Ejerc, Costo_Ejerc,
          Result_Ejerc, Fecha_Certif, Id_Tipo_Dictamen, detalle_contador, irregular)
        values
          (p_Id_Tramite_Ipj, p_id_legajo, to_date(p_fecha, 'dd/mm/rrrr'),
           to_number(p_monto, '99999999999999999990.99'), p_reserva_legal, 'S',
           p_matricula, v_id_integrante, p_observacion,
           to_number(p_activos, '99999999999999999990.99'), to_number(p_pasivos, '99999999999999999990.99'),
           to_number(p_neto, '99999999999999999990.99'), to_number(p_Ingreso_Ejerc, '99999999999999999990.99'),
           to_number(p_Costo_Ejerc, '99999999999999999990.99'), to_number(p_Result_Ejerc, '99999999999999999990.99'),
           to_date(p_Fecha_Certif, 'dd/mm/rrrr'), (case when p_Id_Tipo_Dictamen = 0 then null else p_Id_Tipo_Dictamen end),
           p_DETALLE, p_irregular);
      end if;
    end if;

    o_rdo := 'OK';
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     WHEN OTHERS THEN
       o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_GUARDAR_ENTIDAD_BALANCES;

  PROCEDURE SP_GUARDAR_ENTIDAD_NOTAS(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_nro out number,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number,
    p_nro in number,
    p_fec_modif in varchar2,
    p_id_estado_nota in number,
    p_observacion in varchar2,
    p_eliminar in number,
    p_es_nueva in char,
    p_id_nota_sistema in number
    )
  IS
    /*************************************************************
      Guarda o actualiza una nota de un tramite de una entidad
      No maneja transaccion, ya que se usa en cascada desde la aplicacion y lo administra el codigo.
  *************************************************************/
    v_existe number;
    v_tipo_mensaje number;
    v_id_integrante number;
    v_valid_parametros varchar2(500);
    v_ultima_linea varchar2(4000);
    v_id_estado number(5);
    v_id_legajo number;
    v_es_nuevo char(1);
  BEGIN

    if IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('HAB_LOG_GESTION') = '1' then
      IPJ.VARIOS.SP_GUARDAR_LOG(
        p_METODO => 'Sp_Guardar_Entidad_Notas',
        p_NIVEL => 'Gestion',
        p_ORIGEN => 'DB',
        p_USR_LOG  => 'DB',
        p_USR_WINDOWS => 'Sistema',
        p_IP_PC  => null,
        p_EXCEPCION =>
          'Id Tramite = ' || to_char(p_Id_Tramite_Ipj)
          || ' / Id Legajo = ' || to_char(p_id_legajo)
          || ' / Nro = ' || to_char(p_nro)
          || ' / Fec Modif = ' || p_fec_modif
          || ' / Id Estado = ' || to_char(p_id_estado_nota)
          || ' / Obs = ' || p_observacion
          || ' / Eliminar = ' || to_char(p_eliminar)
          || ' / Es Nueva = ' || p_es_nueva
          || ' / Id Nota Sistema = ' || to_char(p_id_nota_sistema)
      );
    end if;
  -- VALIDACION DE PARAMETROS
    if p_fec_modif is not null and p_eliminar <> 1 and IPJ.VARIOS.Valida_Fecha(p_fec_modif) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT');
    end if;

    if v_valid_parametros is not null then
      o_rdo := 'GUARDAR NOTAS - Parametros: '  || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    -- CUERPO PROCEDIMIENTO
    -- Busco la ultima linea y si viene lo mismo no lo repito
    begin
      select (case when instr(en.observacion, chr(13)) <> 0 then substr(en.observacion, 1, instr(en.observacion, chr(13)))
              else en.Observacion
           end), id_estado_nota into v_ultima_linea, v_id_estado
      from ipj.t_entidades_notas en
      where
        Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        nro = p_nro;
    exception
      when NO_DATA_FOUND then
        v_ultima_linea := null;
        v_id_estado := 0;
    end;


    if nvl(p_eliminar, 0) = 1 then
      select es_nuevo into v_es_nuevo
      from ipj.t_entidades_notas
      where
        Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        nro = p_nro;

      -- Si no esta pendiente de informar a SUAC, no dejo eliminarla
      if v_es_nuevo = 'S' then
        delete ipj.t_entidades_notas
        where
          Id_Tramite_Ipj = p_Id_Tramite_Ipj and
          nro = p_nro ;

        update ipj.t_entidades_notas
        set nro = nro - 1
        where
          Id_Tramite_Ipj = p_Id_Tramite_Ipj and
          nro > p_nro;
      end if;
    else
          -- Si existe un registro lo actualizo, sino lo inserto
      if ( (p_observacion is not null and nvl(p_observacion, '@') <> nvl(v_Ultima_Linea, '@'))or nvl(v_id_estado, 0) <> p_id_estado_nota) then
        update ipj.t_entidades_notas
        set
           observacion = (case
                                    when p_observacion is null  then observacion
                                    when p_observacion is not null and p_es_nueva = 'S' then p_observacion
                                    else substr(p_observacion || chr(13) || observacion, 1, 2000)
                                 end),
          ID_ESTADO_NOTA  = p_id_estado_nota,
           fec_cumpl = (case
                                 when p_id_estado_nota = 1 then to_date(sysdate, 'dd/mm/rrrr')
                                 else null
                               end),
           fec_modif = to_date(sysdate, 'dd/mm/rrrr'),
           Informar_Suac = 'S',
           id_nota_sistema = 0 --(BUG 8911)
        where
          Id_Tramite_Ipj = p_Id_Tramite_Ipj and
          nro = p_nro;

        if sql%rowcount = 0 then
          -- calculo la proxima nota para agregar
          select nvl(max(nro), 0) + 1 into o_nro
          from ipj.t_entidades_notas
          where
            Id_Tramite_Ipj = p_Id_Tramite_Ipj;

          v_id_legajo := (case when p_id_legajo = 0 then null else p_id_legajo end);

          insert into ipj.t_entidades_notas
            (Id_Tramite_Ipj, id_legajo, nro, fec_modif, ID_ESTADO_NOTA, fec_cumpl,
             observacion, Informar_Suac, es_nuevo, id_nota_sistema)
          values
            (p_Id_Tramite_Ipj, v_id_legajo, o_nro, nvl(to_date(p_fec_modif, 'dd/mm/rrrr'), to_date(sysdate, 'dd/mm/rrrr')),
             p_id_estado_nota, (case when p_id_estado_nota = 1 then to_date(sysdate, 'dd/mm/rrrr') else null end),
             p_observacion, 'S', 'S', nvl(p_id_nota_sistema, 0));
        else
          o_nro := p_nro;
        end if;
      else
        o_nro := p_nro;
      end if;
    end if;

    o_rdo := 'OK';
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     WHEN OTHERS THEN
       o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_GUARDAR_ENTIDAD_NOTAS;

  PROCEDURE SP_GUARDAR_ENTIDAD_SINDICO(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_entidad_sindico out number,
    p_id_entidad_sindico in number,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number,
    p_fec_alta in varchar2,
    p_fec_baja in varchar2,
    p_matricula in varchar2,
    p_id_tipo_integrante in number,
    p_borrador in varchar2,
    p_id_integrante in number,
    p_DETALLE in varchar2,
    p_Nombre in varchar2,
    p_Apellido in varchar2,
    p_NRO_DOCUMENTO in varchar2,
    p_ID_NUMERO in number,
    p_ID_SEXO in varchar2,
    p_ID_VIN in number,
    p_PAI_COD_PAIS in varchar2,
    p_CUIL in varchar2,
    p_error_dato in varchar2,
    p_n_tipo_documento in varchar2,
    p_id_legajo_sindico in number,
    p_matricula_empresa in varchar2,
    p_id_tipo_entidad in number,
    p_cuit_empresa in varchar2,
    p_n_empresa in varchar2,
    p_id_entidades_accion in number,
    p_eliminar in number, -- 1 elimina
    p_Id_Tramite_Ipj_entidad in number,
    p_id_motivo_baja in number,
    p_folio in varchar2,
    p_anio in varchar2,
    p_en_formacion in char,
    p_fec_acta in varchar2, -- Es Fecha
    p_persona_expuesta in char
   )
  IS
  /*************************************************************
      Guarda los balances asociados a una entidad de un tramite.
      No maneja transaccion, ya que se usa en cascada desde la aplicacion y lo administra el codigo.
  *************************************************************/
    v_existe number;
    v_tipo_mensaje number;
    v_id_integrante number;
    v_valid_parametros varchar2(500);
    v_id_tipo_entidad number(10);
    v_id_entidades_Accion number(5);
    v_id_entidad_sindico number(10);
    v_id_motivo_baja number;
  BEGIN

  -- VALIDACION DE PARAMETROS
    if nvl(p_eliminar, 0) <> 1 and IPJ.VARIOS.Valida_Fecha(p_fec_alta) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT');
    end if;
    if nvl(p_eliminar, 0) <> 1 and p_fec_baja is not null and IPJ.VARIOS.Valida_Fecha(p_fec_baja) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT');
    end if;
    if nvl(p_eliminar, 0) <> 1 and p_fec_acta is not null and IPJ.VARIOS.Valida_Fecha(p_fec_acta) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT');
    end if;
    if IPJ.VARIOS.Valida_Entidad(nvl(p_Id_Tramite_Ipj, 0), p_id_legajo) = 0 then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('ENT_NOT')|| '( Tram. ' || to_char(p_Id_Tramite_Ipj) ||' -  Leg. ' || to_char(p_id_legajo)||')';
    end if;

    if v_valid_parametros is not null then
      o_rdo := 'GUARDAR INS LEG - Parametros: '  || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    -- CUERPO PROCEDIMIENTO
    if p_eliminar = 1 then
      -- Para soportar el formato viejo
      if p_id_entidad_sindico is null then
        delete ipj.t_entidades_sindico
        where
          id_legajo = p_id_legajo and
          Id_Tramite_Ipj = p_Id_Tramite_Ipj and
          id_integrante = p_id_integrante and
          fecha_alta = to_date(p_fec_alta, 'dd/mm/rrrr') and
          borrador = 'S';
      else
        delete ipj.t_entidades_sindico
        where
          id_entidad_sindico = p_id_entidad_sindico and
          borrador = 'S';
      end if;

    else
      if p_n_empresa is null then
        -- Si no viene el ID_INTEGRANTE lo agrego
        if IPJ.VARIOS.Valida_Integrante(p_id_integrante) = 0 then
           IPJ.ENTIDAD_PERSJUR.SP_GUARDAR_INTEGRANTES(
             o_rdo => v_valid_parametros,
             o_tipo_mensaje => v_tipo_mensaje,
             o_id_integrante => v_id_integrante,
             p_id_sexo => p_id_sexo,
             p_nro_documento => p_nro_documento,
             p_pai_cod_pais => p_pai_cod_pais,
             p_id_numero => p_id_numero,
             p_cuil => p_cuil,
             p_detalle => p_detalle,
             p_Nombre => p_Nombre,
             p_Apellido => p_Apellido,
             p_error_dato => p_error_dato,
             p_n_tipo_documento => p_n_tipo_documento);

           if v_valid_parametros <> TYPES.c_Resp_OK then
              o_rdo := IPJ.VARIOS.MENSAJE_ERROR('INT_NOT') || '. ' || v_valid_parametros;
              o_tipo_mensaje := v_tipo_mensaje;
              return;
           end if;
        else
          if p_error_dato = 'S' then
            update ipj.t_integrantes
            set
              error_dato = p_error_dato,
              detalle = p_detalle
            where
              id_integrante = p_id_integrante;
          end if;
          v_id_integrante := p_id_integrante;
        end if;
      end if;

      -- Paso 0 a NULL para respetar las FK
      v_id_tipo_entidad := (case when p_id_tipo_entidad = 0 then null else p_id_tipo_entidad end);
      v_id_entidades_Accion := (case when p_id_entidades_accion = 0 then null else p_id_entidades_accion end);
      v_id_integrante := (case when nvl(v_id_integrante, 0) = 0 then null else v_id_integrante end);
      v_id_motivo_baja := (case when p_id_motivo_baja = 0 then null else p_id_motivo_baja end);

      -- Si existe un registro igual no hago nada
      v_id_entidad_sindico := p_id_entidad_sindico;
      update ipj.t_entidades_sindico
      set
        matricula = p_matricula,
        fecha_baja = to_date(p_fec_baja, 'dd/mm/rrrr'),
        id_tipo_integrante = p_id_tipo_integrante,
        cuit_empresa = p_cuit_empresa,
        n_empresa = p_n_empresa,
        matricula_empresa = p_matricula_empresa,
        id_tipo_entidad = p_id_tipo_entidad,
        id_entidades_Accion = p_id_entidades_Accion,
        id_motivo_baja = v_id_motivo_baja,
        folio = p_folio,
        anio = p_anio,
        en_formacion = p_en_formacion,
        fec_acta = to_date(p_fec_acta, 'dd/mm/rrrr'),
        persona_expuesta = p_persona_expuesta,
        fecha_alta = to_date(p_fec_alta, 'dd/mm/rrrr')
      where
        id_entidad_sindico = p_id_entidad_sindico;

      -- Si no existe, se agrega
      if sql%rowcount = 0 then
        SELECT IPJ.SEQ_T_ENTIDADES_SINDICO.nextval INTO v_id_entidad_sindico FROM dual;

        insert into ipj.t_entidades_sindico
          (Id_Tramite_Ipj, id_legajo, id_integrante, fecha_alta, matricula, id_tipo_integrante,
          borrador, fecha_baja, id_entidad_sindico, matricula_empresa, id_tipo_entidad,
          id_legajo_sindico, cuit_empresa, n_empresa, id_entidades_Accion, id_motivo_baja,
          folio, anio, en_formacion, fec_acta, persona_expuesta)
        values
          (p_Id_Tramite_Ipj, p_id_legajo, v_id_integrante, to_date(p_fec_alta, 'dd/mm/rrrr'),
           p_matricula, p_id_tipo_integrante, 'S',  to_date(p_fec_baja, 'dd/mm/rrrr'),
           v_id_entidad_sindico, p_matricula_empresa, v_id_tipo_entidad,
           p_id_legajo_sindico, p_cuit_empresa, p_n_empresa, v_id_entidades_Accion,
           v_id_motivo_baja, p_folio, p_anio, p_en_formacion, to_date(p_fec_acta, 'dd/mm/rrrr'),
           p_persona_expuesta);

      end if;

      -- Guardo los cambios en el historia
      IPJ.ENTIDAD_PERSJUR.SP_GUARDAR_ENTIDAD_SIND_HIST(
        o_rdo => o_rdo,
        o_tipo_mensaje => o_tipo_mensaje,
        p_id_entidad_sindico => v_id_entidad_sindico,
        p_Id_Tramite_Ipj => p_Id_Tramite_Ipj_entidad,
        p_id_legajo => p_id_legajo,
        p_fec_alta => p_fec_alta,
        p_fec_baja => p_fec_baja,
        p_matricula => p_matricula,
        p_id_tipo_integrante => p_id_tipo_integrante,
        p_borrador => p_borrador,
        p_id_integrante => v_id_integrante,
        p_id_legajo_sindico => p_id_legajo_sindico,
        p_matricula_empresa => p_matricula_empresa,
        p_id_tipo_entidad => v_id_tipo_entidad,
        p_cuit_empresa => p_cuit_empresa,
        p_n_empresa => p_n_empresa,
        p_id_entidades_accion => v_id_entidades_Accion,
        p_id_motivo_baja => v_id_motivo_baja
      );

      if o_rdo <> TYPES.c_Resp_OK then
        o_rdo := 'SINDICOS HIST: ' || o_rdo;
        o_id_entidad_sindico := 0;
        return;
      end if;
    end if;



    o_id_entidad_sindico :=  (case when nvl(p_id_entidad_sindico, 0) = 0 then v_id_entidad_sindico else p_id_entidad_sindico end);
    o_rdo := 'OK';
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     WHEN OTHERS THEN
       o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_GUARDAR_ENTIDAD_SINDICO;


  PROCEDURE SP_GUARDAR_ENTIDAD_REPRES(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number,
    p_fec_alta in varchar2,
    p_fec_baja in varchar2,
    p_borrador in varchar2,
    p_id_integrante in number,
    p_DETALLE in varchar2,
    p_Nombre in varchar2,
    p_Apellido in varchar2,
    p_NRO_DOCUMENTO in varchar2,
    p_ID_NUMERO in number,
    p_ID_SEXO in varchar2,
    p_ID_VIN in number,
    p_PAI_COD_PAIS in varchar2,
    p_CUIL in varchar2,
    p_error_dato in varchar2,
    p_n_tipo_documento in varchar2,
    p_eliminar in number, -- 1 elimina
    p_id_motivo_baja in number,
    p_id_tramite_ipj_entidad in number
  )
  IS
    /*************************************************************
      Guarda los balances asociados a una entidad de un tramite.
      No maneja transaccion, ya que se usa en cascada desde la aplicacion y lo administra el codigo.
  *************************************************************/
    v_repres_row IPJ.T_ENTIDADES_REPRESENTANTE%rowtype;
    v_tipo_mensaje number;
    v_id_integrante number;
    v_valid_parametros varchar2(500);
  BEGIN
   if IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('HAB_LOG_GESTION') = '1' then
      IPJ.VARIOS.SP_GUARDAR_LOG(
        p_METODO => 'SP_GUARDAR_ENTIDAD_REPRES',
        p_NIVEL => 'Gestion',
        p_ORIGEN => 'DB',
        p_USR_LOG  => 'DB',
        p_USR_WINDOWS => 'Sistema',
        p_IP_PC  => null,
        p_EXCEPCION =>
          'ID_Tramite Ipj = ' || to_char(p_Id_Tramite_Ipj)
          || ' / ID Legajo = ' || to_char(p_id_legajo)
          || ' / Fec Alta = ' || p_fec_alta
          || ' / Fec Baja = ' || p_fec_baja
          || ' / Borrador = ' || p_borrador
          || ' / Id Integrante = ' || to_char(p_id_integrante)
          || ' / Detalle = ' || p_DETALLE
          || ' / Nombre = ' || p_Nombre
          || ' / Apellido = ' || p_Apellido
          || ' / DNI = ' || p_NRO_DOCUMENTO
          || ' / Id Numero = ' || to_char(p_ID_NUMERO)
          || ' / ID Sexo = ' || p_ID_SEXO
          || ' / Id Vin = ' || to_char(p_ID_VIN)
          || ' / Pai Cod Pais = ' || p_PAI_COD_PAIS
          || ' / Cuil = ' || p_CUIL
          || ' / Error Dato = ' || p_error_dato
          || ' / Tipo Documento = ' || p_n_tipo_documento
          || ' / Eliminar = ' || to_char(p_eliminar)
          || ' / Id Motivo Baja = ' || to_char(p_id_motivo_baja)
          || ' / ID Tramite Entidad = ' || to_char(p_id_tramite_ipj_entidad)
      );
    end if;

  -- VALIDACION DE PARAMETROS
    if IPJ.VARIOS.Valida_Fecha(p_fec_alta) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT');
    end if;
    if p_fec_baja is not null and IPJ.VARIOS.Valida_Fecha(p_fec_baja) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT');
    end if;
    if IPJ.VARIOS.Valida_Entidad(nvl(p_Id_Tramite_Ipj, 0), p_id_legajo) = 0 then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('ENT_NOT')|| '(' || to_char(p_Id_Tramite_Ipj) ||' - ' || to_char(p_id_legajo)||')';
    end if;

    if v_valid_parametros is not null then
      o_rdo := 'GUARDAR INS LEG - Parametros: '  || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    -- CUERPO PROCEDIMIENTO
    if p_eliminar = 1 then
      if p_borrador = 'S' then
        delete ipj.t_entidades_representante
        where
          id_legajo = p_id_legajo and
          Id_Tramite_Ipj = p_Id_Tramite_Ipj and
          id_integrante = p_id_integrante and
          fecha_alta = to_date(p_fec_alta, 'dd/mm/rrrr');
      else
        update ipj.t_entidades_representante
        set
          fecha_baja = to_date(sysdate, 'dd/mm/rrrr'),
          id_motivo_baja = decode(p_id_motivo_baja, 0, null, p_id_motivo_baja)
        where
          id_legajo = p_id_legajo and
          Id_Tramite_Ipj = p_Id_Tramite_Ipj and
          id_integrante = p_id_integrante and
          fecha_alta = to_date(p_fec_alta, 'dd/mm/rrrr');
      end if;

    else
      -- Si no viene el ID_INTEGRANTE lo agrego
      if IPJ.VARIOS.Valida_Integrante(p_id_integrante) = 0 then
         IPJ.ENTIDAD_PERSJUR.SP_GUARDAR_INTEGRANTES(
           o_rdo => v_valid_parametros,
           o_tipo_mensaje => v_tipo_mensaje,
           o_id_integrante => v_id_integrante,
           p_id_sexo => p_id_sexo,
           p_nro_documento => p_nro_documento,
           p_pai_cod_pais => p_pai_cod_pais,
           p_id_numero => p_id_numero,
           p_cuil => p_cuil,
           p_detalle => p_detalle,
           p_Nombre => p_Nombre,
           p_Apellido => p_Apellido,
           p_error_dato => p_error_Dato,
           p_n_tipo_documento => p_n_tipo_documento);

         if v_valid_parametros <> TYPES.c_Resp_OK then
            o_rdo := IPJ.VARIOS.MENSAJE_ERROR('INT_NOT') || '. ' || v_valid_parametros;
            o_tipo_mensaje := v_tipo_mensaje;
            return;
         end if;
      else
          if p_error_dato = 'S' then
            update ipj.t_integrantes
            set
              error_dato = p_error_dato,
              detalle = p_detalle
            where
              id_integrante = p_id_integrante;
          end if;
         v_id_integrante := p_id_integrante;
       end if;

      -- Si existe un registro igual no hago nada
      update ipj.t_entidades_representante
      set
        id_motivo_baja = decode(p_id_motivo_baja, 0, null, p_id_motivo_baja),
        fecha_baja = to_date(p_fec_baja, 'dd/mm/rrrr')
      where
        id_legajo = p_id_legajo and
        Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        id_integrante = p_id_integrante and
        fecha_alta = to_date(p_fec_alta, 'dd/mm/rrrr');

      if sql%rowcount = 0 then
        insert into ipj.t_entidades_representante
          (Id_Tramite_Ipj, id_legajo, id_integrante, fecha_alta, borrador, id_motivo_baja)
        values
          (p_Id_Tramite_Ipj, p_id_legajo, v_id_integrante, to_date(p_fec_alta, 'dd/mm/rrrr'), 'S',
            decode(p_id_motivo_baja, 0, null, p_id_motivo_baja));
      end if;

      --Busco los datos del registro
      select * into v_repres_row
      from ipj.t_entidades_representante
      where
        id_legajo = p_id_legajo and
        Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        id_integrante = p_id_integrante and
        fecha_alta = to_date(p_fec_alta, 'dd/mm/rrrr');

      -- Cargo el historial de representantes
      if nvl(p_id_tramite_ipj_entidad, 0) <> 0 then
          IPJ.ENTIDAD_PERSJUR.SP_GUARDAR_ENTIDAD_REPR_HIST(
            o_rdo => o_rdo,
            o_tipo_mensaje => o_tipo_mensaje,
            p_id_tramite_ipj => p_id_tramite_ipj_entidad,
            p_id_legajo => p_id_legajo,
            p_id_integrante => p_id_integrante,
            p_fecha_alta => p_fec_alta,
            p_borrador => p_borrador,
            p_fecha_baja => p_fec_baja,
            p_persona_expuesta => v_repres_row.persona_expuesta,
            p_id_vin_especial => v_repres_row.id_vin_especial,
            p_id_motivo_baja => p_id_motivo_baja
          );
      end if;
    end if;

    o_rdo := 'OK';
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     WHEN OTHERS THEN
       o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_GUARDAR_ENTIDAD_REPRES;

  PROCEDURE SP_Traer_PersJur_Veedores(
    o_Cursor OUT TYPES.cursorType,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number)
  IS
  BEGIN
  /*******************************************************
    Este procedimento lista los veedores activos en un tramite dado
  ********************************************************/
    OPEN o_Cursor FOR
      select v.Id_Tramite_Ipj, v.Id_Legajo, v.Id_Integrante, v.id_rol_veedor,
        i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, i.cuil,
        (p.NOMBRE || ' ' ||p.APELLIDO)  detalle,
        i.id_vin, i.error_dato, tv.n_rol_veedor,
        to_char(v.fec_alta, 'dd/mm/rrrr') fec_alta, to_char(v.fec_baja, 'dd/mm/rrrr') fec_baja
      from IPJ.t_Veedores v join IPJ.t_integrantes i
          on V.id_integrante = i.id_integrante
        join IPJ.T_TIPOS_VEEDOR tv
          on tv.id_rol_veedor = v.id_rol_veedor
        left join rcivil.vt_pk_persona p
          on i.id_sexo = p.ID_SEXO and
              i.nro_documento = p.NRO_DOCUMENTO and
              i.pai_cod_pais = p.PAI_COD_PAIS and
              i.id_numero = p.ID_NUMERO
      where
       V.Id_Tramite_Ipj = p_Id_Tramite_Ipj and
       V.ID_LEGAJO = p_id_legajo;

  END SP_Traer_PersJur_Veedores;

  PROCEDURE SP_GUARDAR_ENTIDAD_VEEDOR(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_id_tramiteipj_accion in number,
    p_id_legajo in number,
    p_fecha_veed in varchar2,
    p_fec_alta in varchar2,
    p_fec_baja in varchar2,
    p_id_rol_veedor in number,
    p_id_integrante in number,
    p_id_sexo in varchar2,
    p_nro_documento in varchar2,
    p_pai_cod_pais in varchar2,
    p_id_numero in number,
    p_cuil in varchar2,
    p_detalle in varchar2,
    p_nombre in varchar2,
    p_apellido in varchar2,
    p_error_dato in varchar2,
    p_n_tipo_documento in varchar2,
    p_eliminar in number) -- 1 elimina
  IS
    v_existe number;
    v_tipo_mensaje number;
    v_id_integrante number;
    v_valid_parametros varchar2(500);
    v_integrante number(10);
  BEGIN
    /*************************************************************
      Guarda los veedores cargados en un tr�mite
  *************************************************************/
  -- VALIDACION DE PARAMETROS
    if p_id_legajo = 0 then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('ENT_NOT')|| '(' || to_char(p_Id_Tramite_Ipj) ||' - ' || to_char(p_id_legajo)||')';
    end if;
    if p_fecha_veed is not null and IPJ.VARIOS.Valida_Fecha(p_fecha_veed, 'dd/mm/rrrr HH:MI:SS AM') = 'N'  then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT') || ' (Fecha Veeduria)';
    end if;
    if p_fec_alta is not null and IPJ.VARIOS.Valida_Fecha(p_fec_alta) = 'N'  then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT') || ' (Fecha Alta)';
    end if;
    if p_fec_baja is not null and IPJ.VARIOS.Valida_Fecha(p_fec_baja) = 'N'  then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT') || ' (Fecha Baja)';
    end if;

    if v_valid_parametros is not null then
      o_rdo := 'GUARDAR VEEDOR - Parametros: '  || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    -- CUERPO PROCEDIMIENTO
    -- Si viene algun usuario, lo cargo
    if p_eliminar = 1 then
      delete ipj.t_veedores
      where
        id_legajo = p_id_legajo and
        Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        id_integrante = p_id_integrante;

    else
       v_id_integrante := p_id_integrante;
       if IPJ.VARIOS.Valida_Integrante(p_id_integrante) = 0 then
         IPJ.ENTIDAD_PERSJUR.SP_GUARDAR_INTEGRANTES(
           o_rdo => v_valid_parametros,
           o_tipo_mensaje => v_tipo_mensaje,
           o_id_integrante => v_id_integrante,
           p_id_sexo => p_id_sexo,
           p_nro_documento => p_nro_documento,
           p_pai_cod_pais => p_pai_cod_pais,
           p_id_numero => p_id_numero,
           p_cuil => p_cuil,
           p_detalle => p_detalle,
           p_Nombre => p_Nombre,
           p_Apellido => p_Apellido,
           p_error_dato => p_error_dato,
           p_n_tipo_documento => p_n_tipo_documento );

         if v_valid_parametros <> TYPES.c_Resp_OK then
            o_rdo := IPJ.VARIOS.MENSAJE_ERROR('INT_NOT') || '. ' || v_valid_parametros;
            o_tipo_mensaje := v_tipo_mensaje;
            return;
         end if;
       else
         -- si esta marcado como dato erroneo, cambio la descripcion y lo marco.
         if p_error_dato = 'S' then
           update ipj.t_integrantes
           set
             error_dato = p_error_dato,
             detalle = p_detalle
           where
             id_integrante = p_id_integrante;
         end if;
       end if;


      -- Si existe un registro igual no hago nada
      update ipj.t_veedores v
      set
        fec_alta = to_date(p_fec_alta, 'dd/mm/rrrr'),
        fec_baja = (case when p_fec_baja is null then null else to_date(p_fec_baja, 'dd/mm/rrrr') end),
        id_rol_veedor = p_id_rol_veedor
      where
        id_legajo = p_id_legajo and
        Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        id_integrante = v_id_integrante;

      if sql%rowcount = 0 then
        insert into ipj.t_veedores (Id_Tramite_Ipj, id_legajo, id_integrante, fec_alta, fec_baja, id_rol_veedor)
        values
          (p_Id_Tramite_Ipj, p_id_legajo, v_id_integrante, to_date(p_fec_alta, 'dd/mm/rrrr'),
          (case when p_fec_baja is null then null else to_date(p_fec_baja, 'dd/mm/rrrr') end), p_id_rol_veedor);
      end if;
    end if;

    -- Actualizo a fecha en la accion
    UPDATE IPJ.T_TRAMITESIPJ_ACCIONES
    set FEC_VEEDURIA = to_date(p_fecha_veed, 'dd/mm/rrrr HH:MI:SS AM')
    where
      Id_Tramite_Ipj = p_Id_Tramite_Ipj and
      id_legajo = p_id_legajo and
      id_tramiteipj_accion = p_id_tramiteipj_accion;

    o_rdo := 'OK';
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     WHEN OTHERS THEN
       o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_GUARDAR_ENTIDAD_VEEDOR;

  PROCEDURE SP_Hist_PersJur_Represent(
    o_Cursor OUT TYPES.cursorType,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number)
  IS
    v_matricula_version number(6,0);
    v_ubicacion_ent number(6);
    v_ubicacion_leg number(6);
  BEGIN
  /*********************************************************
  Este procemiento devuelve el listado de Representantes asociados a una entidad
  de un tramite, que no sean borradores y esten dedos de baja.
  **********************************************************/
    begin
      select nvl(matricula_version, 0),
        (select id_ubicacion from ipj.t_tipos_entidades te where te.id_tipo_entidad = e.id_tipo_entidad) ubicacion_ent,
        (select id_ubicacion from ipj.t_tipos_entidades te where te.id_tipo_entidad = l.id_tipo_entidad) ubicacion_leg
        into v_matricula_version, v_ubicacion_ent, v_ubicacion_leg
      from IPJ.t_entidades e join ipj.t_legajos l
        on e.id_legajo = l.id_legajo
      where
        e.Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        e.id_legajo = p_id_legajo;
    exception
      when NO_DATA_FOUND then
        v_matricula_version := 0;
    end;

    OPEN o_Cursor FOR
      select
        to_char(ER.FECHA_BAJA, 'dd/mm/rrrr') FEC_BAJA,
        to_char(ER.FECHA_ALTA, 'dd/mm/rrrr') FEC_ALTA,
        i.id_integrante, I.ID_NUMERO, I.ID_SEXO, I.NRO_DOCUMENTO, I.PAI_COD_PAIS,
        min(E.MATRICULA_VERSION) MATRICULA_VERSION,
        ( select  TRIM(p.NOV_NOMBRE || ' ' ||p.NOV_APELLIDO) from rcivil.vt_pk_persona p
           where i.id_sexo = p.ID_SEXO and i.nro_documento = p.NRO_DOCUMENTO and i.pai_cod_pais = p.PAI_COD_PAIS and i.id_numero = p.ID_NUMERO)  detalle,
        IPJ.TRAMITES.FC_FECHA_TRAMITE(p_id_legajo, min(E.MATRICULA_VERSION)) fecha_inicio,
        IPJ.TRAMITES.FC_USR_ACCION_TRAMITE(p_id_legajo, min(E.MATRICULA_VERSION)) descripcion
      from ipj.t_entidades_representante er join IPJ.t_integrantes I
          on I.ID_INTEGRANTE = Er.ID_INTEGRANTE
        join ipj.t_entidades e
          on e.Id_Tramite_Ipj = er.Id_Tramite_Ipj and e.id_legajo = er.id_legajo
        join ipj.t_tramitesipj t
          on t.Id_Tramite_Ipj = er.Id_Tramite_Ipj
      where
        e.id_legajo = p_id_legajo and
        e.Id_Tramite_Ipj <> p_Id_Tramite_Ipj and
        e.matricula_version <= v_matricula_version and
        e.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_ubicacion_leg) and
        er.borrador = 'N' and
        nvl(ER.FECHA_BAJA, sysdate)  < sysdate and
        T.ID_ESTADO_ULT < IPJ.TYPES.C_ESTADOS_RECHAZADO
      group by er.fecha_alta, er.fecha_baja, i.id_integrante, I.ID_NUMERO, I.ID_SEXO, I.NRO_DOCUMENTO, I.PAI_COD_PAIS
      order by matricula_version desc;
  END SP_Hist_PersJur_Represent;


  PROCEDURE SP_Hist_PersJur_Sindicos(
    o_Cursor OUT TYPES.cursorType,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number)
  IS
    v_matricula_version number(6, 0);
    v_ubicacion_ent number(6);
    v_ubicacion_leg number(6);
    v_dias_vigencia number;
  BEGIN
  /*******************************************************
    Este procedimento lista los sindicos de una entidad, que no son borrador y
    estan dados de baja
  ********************************************************/
    begin
      select nvl(matricula_version, 0),
        (select id_ubicacion from ipj.t_tipos_entidades te where te.id_tipo_entidad = e.id_tipo_entidad) ubicacion_ent,
        (select id_ubicacion from ipj.t_tipos_entidades te where te.id_tipo_entidad = l.id_tipo_entidad) ubicacion_leg
        into v_matricula_version, v_ubicacion_ent, v_ubicacion_leg
      from IPJ.t_entidades e join ipj.t_legajos l
        on e.id_legajo = l.id_legajo
      where
        e.Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        e.id_legajo = p_id_legajo;
    exception
      when NO_DATA_FOUND then
        v_matricula_version := 0;
    end;

    -- (PBI 9917) Busco los d�as de vigencia configurados
    v_dias_vigencia := to_number(IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('DIAS_ADMIN_LEY'));

    OPEN o_Cursor FOR
      select
        to_char(ES.FECHA_ALTA, 'dd/mm/rrrr') FEC_ALTA, to_char(ES.FECHA_BAJA, 'dd/mm/rrrr') FEC_BAJA,
        ES.ID_TIPO_INTEGRANTE ID_TIPO_SINDICO, ES.ID_TIPO_INTEGRANTE,
        ES.MATRICULA,
        i.id_integrante, I.ID_NUMERO, I.ID_SEXO, I.NRO_DOCUMENTO, I.PAI_COD_PAIS,
        min(E.MATRICULA_VERSION) MATRICULA_VERSION,
        ( select  TRIM(p.NOMBRE || ' ' ||p.APELLIDO) from rcivil.vt_pk_persona p
          where i.id_sexo = p.ID_SEXO and i.nro_documento = p.NRO_DOCUMENTO and i.pai_cod_pais = p.PAI_COD_PAIS and i.id_numero = p.ID_NUMERO)  detalle,
        (select N_TIPO_INTEGRANTE from IPJ.T_TIPOS_INTEGRANTE ts where ES.ID_TIPO_INTEGRANTE = TS.ID_TIPO_INTEGRANTE) N_TIPO_SINDICO,
        (select N_TIPO_INTEGRANTE from IPJ.T_TIPOS_INTEGRANTE ts where ES.ID_TIPO_INTEGRANTE = TS.ID_TIPO_INTEGRANTE) N_TIPO_INTEGRANTE,
        IPJ.TRAMITES.FC_FECHA_TRAMITE(p_id_legajo, min(E.MATRICULA_VERSION)) fecha_inicio,
        IPJ.TRAMITES.FC_USR_ACCION_TRAMITE(p_id_legajo, min(E.MATRICULA_VERSION)) descripcion,
        es.id_motivo_baja, mb.n_motivo_baja
      from ipj.t_entidades_sindico es join IPJ.t_integrantes I
          on I.ID_INTEGRANTE = Es.ID_INTEGRANTE
        join ipj.t_entidades e
          on e.Id_Tramite_Ipj = es.Id_Tramite_Ipj and e.id_legajo = es.id_legajo
        join ipj.t_tramitesipj t
          on t.Id_Tramite_Ipj = es.Id_Tramite_Ipj
        join ipj.t_tipos_integrante ti
          on es.id_tipo_integrante = ti.id_tipo_integrante
        left join ipj.t_tipos_organismo org
          on ti.id_tipo_organismo = org.id_tipo_organismo
        left join ipj.T_Tipos_Motivos_Baja mb
          on es.id_motivo_baja = mb.id_motivo_baja
      where
        e.id_legajo = p_id_legajo and
        e.Id_Tramite_Ipj <> p_Id_Tramite_Ipj and
        e.matricula_version <= v_matricula_version and
        e.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_ubicacion_leg) and
        es.borrador = 'N' and
        (NVL(es.fecha_baja, sysdate) + v_dias_vigencia) < sysdate and
        T.ID_ESTADO_ULT < IPJ.TYPES.C_ESTADOS_RECHAZADO
      group by ES.ID_TIPO_INTEGRANTE, ES.MATRICULA, es.fecha_alta, es.fecha_baja,
        i.id_integrante, I.ID_NUMERO, I.ID_SEXO, I.NRO_DOCUMENTO, I.PAI_COD_PAIS,
        org.nro_orden, ti.nro_orden, detalle, es.id_motivo_baja, mb.n_motivo_baja
      order by matricula_version desc, org.nro_orden desc, ti.nro_orden desc, detalle desc;

  END SP_Hist_PersJur_Sindicos;

  PROCEDURE SP_Informar_Pers_Juridica(
      p_Id_Tramite_Ipj in number,
      p_id_legajo in number,
      p_id_sede in varchar2 := '00',
      p_Cursor OUT TYPES.cursorType,
      o_rdo out varchar)
  IS
    v_id_sede varchar2(3);
    v_existe_entidad number;
    v_Row_Entidad_Actual IPJ.t_entidades%ROWTYPE;
    v_version_denominacion number;
    v_version_vigencia number;
    v_version_domicilio number;
    v_version_obj_social number;
    v_version_monto number;
    v_version_socios number;
    v_version_admin number;
    v_version_socios_temp number;
    v_version_admin_temp number;
    v_version_estado number;
    v_version_cierre number;
    v_version_Tipo_Admin number;
    v_matricula_denominacion varchar2(50);
    v_matricula_vigencia varchar2(50);
    v_matricula_domicilio varchar2(50);
    v_matricula_obj_social varchar2(50);
    v_matricula_estado varchar2(50);
    v_matricula_monto varchar2(50);
    v_matricula_cierre varchar2(50);
    v_matricula_vigencia_texto varchar2(200);
    v_matricula_domicilio_texto varchar2(200);
    v_matricula_obj_social_texto varchar2(200);
    v_matricula_socios varchar2(200);
    v_matricula_admin varchar2(200);
    v_matricula_socios_temp varchar2(200);
    v_matricula_admin_temp varchar2(200);
    v_matricula_Tipo_Admin varchar2(200);
    v_anio_denominacion number;
    v_anio_vigencia number;
    v_anio_const number;
    v_anio_domicilio number;
    v_anio_obj_social number;
    v_anio_monto number;
    v_anio_socios number;
    v_anio_admin number;
    v_anio_socios_temp number;
    v_anio_admin_temp number;
    v_anio_estado number;
    v_anio_cierre number;
    v_anio_Tipo_Admin number;
    v_folio_denominacion varchar2(100);
    v_folio_vigencia varchar2(100);
    v_folio_const varchar2(100);
    v_folio_domicilio varchar2(100);
    v_folio_obj_social varchar2(100);
    v_folio_monto varchar2(100);
    v_folio_socios varchar2(100);
    v_folio_admin varchar2(100);
    v_folio_socios_temp varchar2(100);
    v_folio_admin_temp varchar2(100);
    v_folio_estado varchar2(100);
    v_folio_cierre varchar2(100);
    v_folio_Tipo_Admin varchar2(100);
    v_nro_expediente_admin varchar2(50);
    v_Cursor_Admin types.cursorType;
    v_Cursor_Socios types.cursorType;
    v_Col_Ent_Admin IPJ.T_ENTIDADES_ADMIN_HIST%ROWTYPE;
    v_Col_Ent_Admin_Sind ipj.t_entidades_sindico_HIST%ROWTYPE;
    v_Col_Ent_Socio IPJ.T_ENTIDADES_SOCIOS%ROWTYPE;
    v_ACyF_VALIDA number;
    v_Balances_Pend varchar2(1000);
    v_id_Ult_autor number;
    v_id_Ult_autor_fiscal number;
    v_Id_Tramite_Ipj number;
    v_id_tramiteipj_accion number;
    v_id_ubicacion_ent number;
    v_check_date DATE;
    v_localidad varchar2(300);
    v_departamento varchar2(300);
    v_calle varchar2(2000);
    v_rdo_versiones varchar2(2000);
    v_tipo_mensaje_versiones number;
  BEGIN
  /**************************************************
   Este procedimiento busca una Persona Juridica y muestra los valores necesarios
   para generar un informe
  ****************************************************/
    /* Detalles de campos de entidad:
      - Denominacion_1: es la Razon Social
      - Denominacion_2: es el Nombre de la Sede
      - Alta_Temporal: es la fecha de inicio de actividades
      - Baja_Temporal: es la fecha de fin de actividades
    */

    if IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('HAB_LOG_GESTION') = '1' then
      IPJ.VARIOS.SP_GUARDAR_LOG(
        p_METODO => 'SP_Informar_Pers_Juridica',
        p_NIVEL => 'Gestion',
        p_ORIGEN => 'DB',
        p_USR_LOG  => 'DB',
        p_USR_WINDOWS => 'Sistema',
        p_IP_PC  => null,
        p_EXCEPCION =>
          'Tramite IPJ = ' || to_char(p_Id_Tramite_Ipj)
          || ' / Legajo = ' || to_char(p_id_legajo)
          || ' / Sede = ' || p_id_sede
      );
      commit;
    end if;

    --Si la sede viene de 1 digito, le agrego un 0 adelante
    if length(p_id_sede) < 2 then
      v_id_sede := lpad(p_id_sede, 2, '0');
    else
      v_id_sede := p_id_sede;
    end if;

    --Valido que exista alguna entidad, si no hay devuelvo error y un registro vacio (no null)
    select count(*) into v_existe_entidad
    from ipj.t_entidades e
    where
      e.Id_Tramite_Ipj = p_Id_Tramite_Ipj and
      e.id_legajo = p_id_legajo;

    if v_existe_entidad = 0 then
      -- busco el ultimo tr�mite de la entidad
      IPJ.Entidad_PersJur.SP_Buscar_Entidad_Tramite(
        o_Id_Tramite_Ipj => v_Id_Tramite_Ipj,
        o_id_tramiteipj_accion => v_id_tramiteipj_accion,
        p_id_legajo => p_id_legajo,
        p_abiertos => 'N');

      if v_Id_Tramite_Ipj = 0 and v_id_tramiteipj_accion = 0 then
        -- NO EXISTE, DEVULEVO UN REGISTRO VACIO
        o_rdo := IPJ.VARIOS.MENSAJE_ERROR('ENT_NOT');
        OPEN p_Cursor FOR
          select '' desc_obj_social, '0' monto, null acta_contitutiva, null vigencia, null id_unidad_med,
            null id_moneda, null cierre_ejercicio, null fec_vig, '' cuit, '' denominacion_1,
            null alta_temporal, null baja_temporal, 'N' Existe, null Matricula
          from dual;
        return;
      end if;
    else
      v_Id_Tramite_Ipj := p_Id_Tramite_Ipj;
      v_id_tramiteipj_accion := 0;
    end if;

     -- BUSCO LOS DATOS ACTUALES DE LA ENTIDAD
    select * into v_Row_Entidad_Actual
    from IPJ.t_entidades
    where
      Id_Tramite_Ipj = v_Id_Tramite_Ipj and
      id_legajo = p_id_legajo;

    -- Busco el area actual de la entidad
    select id_ubicacion into v_id_ubicacion_ent
    from ipj.t_tipos_entidades
    where
      id_tipo_entidad = v_Row_Entidad_Actual.id_tipo_entidad;

    -- CALCULOS LAS MATRICULAS DE LOS CAMBIOS REALIZADOS
    -- Versionado Constitucion
    select folio, anio into v_folio_const, v_anio_const
    from
      ( select e.matricula, e.matricula_version, e.folio, e.anio
        from IPJ.t_entidades e join ipj.t_tramitesipj t
          on e.Id_Tramite_Ipj = t.Id_Tramite_Ipj
        where
          e.id_legajo = p_id_legajo and
          e.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_id_ubicacion_ent) and
          T.ID_ESTADO_ULT < IPJ.TYPES.C_ESTADOS_RECHAZADO and
          (E.Id_Tramite_Ipj = v_Row_Entidad_Actual.Id_Tramite_Ipj or e.borrador = 'N')
        order by e.matricula desc, e.matricula_version asc, e.anio asc, e.folio asc
      ) t
    where rownum = 1;

    -- Busco todas las versiones del Item
    IPJ.ENTIDAD_PERSJUR.SP_Buscar_Versiones_Ent(
      o_rdo => v_rdo_versiones,
      o_tipo_mensaje => v_tipo_mensaje_versiones,
      -- Vigencia
      o_matricula_Vigencia => v_matricula_vigencia,
      o_matricula_version_Vigencia => v_version_vigencia,
      o_folio_Vigencia => v_folio_vigencia,
      o_anio_Vigencia => v_anio_vigencia,
      -- Estado
      o_matricula_Estado => v_matricula_estado,
      o_matricula_version_Estado => v_version_estado,
      o_folio_Estado => v_folio_estado,
      o_anio_Estado => v_anio_estado,
      -- Cierre Ejercicio
      o_matricula_Cierre => v_matricula_cierre,
      o_matricula_version_Cierre => v_version_cierre,
      o_folio_Cierre => v_folio_cierre,
      o_anio_Cierre => v_anio_cierre,
      -- Domicilio
      o_matricula_Dom => v_matricula_domicilio,
      o_matricula_version_Dom => v_version_domicilio,
      o_folio_Dom => v_folio_domicilio,
      o_anio_Dom => v_anio_domicilio,
      -- Denominacion
      o_matricula_Denom => v_matricula_denominacion,
      o_matricula_version_Denom => v_version_denominacion,
      o_folio_Denom => v_folio_denominacion,
      o_anio_Denom => v_anio_denominacion,
      -- Tipo Administracion
      o_matricula_TipoAd => v_matricula_Tipo_Admin,
      o_matricula_version_TipoAd => v_version_Tipo_Admin,
      o_folio_TipoAd => v_folio_Tipo_Admin,
      o_anio_TipoAd => v_anio_Tipo_Admin,
      -- Objeto Social
      o_matricula_Obj => v_matricula_obj_social,
      o_matricula_version_Obj => v_version_obj_social,
      o_folio_Obj => v_folio_obj_social,
      o_anio_Obj => v_anio_obj_social,
      -- Capital
      o_matricula_Capital => v_matricula_monto,
      o_matricula_version_Capital => v_version_monto,
      o_folio_Capital => v_folio_monto,
      o_anio_Capital => v_anio_monto,

      p_Id_Tramite_Ipj => v_Row_Entidad_Actual.Id_Tramite_Ipj,
      p_id_legajo => v_Row_Entidad_Actual.Id_Legajo
    );

    /*
    -- Versionado Vigencia
    select matricula, matricula_version, folio, anio into v_matricula_vigencia, v_version_vigencia, v_folio_vigencia, v_anio_vigencia
    from
      ( select e.matricula, e.matricula_version, e.folio, e.anio
        from IPJ.t_entidades e join ipj.t_tramitesipj t
          on e.Id_Tramite_Ipj = t.Id_Tramite_Ipj
        where
          e.id_legajo = p_id_legajo and
          e.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_id_ubicacion_ent) and
          T.ID_ESTADO_ULT < IPJ.TYPES.C_ESTADOS_RECHAZADO and
          (E.Id_Tramite_Ipj = v_Row_Entidad_Actual.Id_Tramite_Ipj or e.borrador = 'N') and
          nvl(e.vigencia, -1) = nvl(v_Row_Entidad_Actual.vigencia, -1) and
          nvl(e.tipo_vigencia, -1) = nvl(v_Row_Entidad_Actual.tipo_vigencia, -1) and
          nvl(to_char(e.fec_vig_hasta, 'dd/mm/rrrr'), '-') = nvl(to_char(v_Row_Entidad_Actual.fec_vig_hasta, 'dd/mm/rrrr'), '-') and
          nvl(to_char(e.fec_vig, 'dd/mm/rrrr'), '-') = nvl(to_char(v_Row_Entidad_Actual.fec_vig, 'dd/mm/rrrr'), '-')
        order by e.matricula desc, e.matricula_version asc, e.anio asc, e.folio asc
      ) t
    where rownum = 1;

    -- Versionado Estado
    select matricula, matricula_version, folio, anio into v_matricula_estado, v_version_estado, v_folio_estado, v_anio_estado
    from
      ( select e.matricula, e.matricula_version, e.folio, e.anio
        from ipj.t_entidades e join ipj.t_tramitesipj t
          on e.id_tramite_ipj = t.id_tramite_ipj
        where
          e.id_legajo = p_id_legajo and
          e.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_id_ubicacion_ent) and
          t.id_estado_ult < ipj.types.c_estados_rechazado and
          (e.id_tramite_ipj = v_row_entidad_actual.id_tramite_ipj or e.borrador = 'N') and
          nvl(e.id_estado_entidad , '-') = nvl(v_row_entidad_actual.id_estado_entidad, '-')
        order by e.matricula desc, e.matricula_version asc, e.anio asc, e.folio asc
      ) t
    where rownum = 1;

    -- Versionado Cierre Ejercicio
    select matricula, matricula_version, folio, anio into v_matricula_cierre, v_version_cierre, v_folio_cierre, v_anio_cierre
    from
      ( select e.matricula, e.matricula_version, e.folio, e.anio
        from ipj.t_entidades e join ipj.t_tramitesipj t
          on e.id_tramite_ipj = t.id_tramite_ipj
        where
          e.id_legajo = p_id_legajo and
          e.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_id_ubicacion_ent) and
          t.id_estado_ult < ipj.types.c_estados_rechazado and
          (e.id_tramite_ipj = v_row_entidad_actual.id_tramite_ipj or e.borrador = 'N') and
          nvl(to_char(e.cierre_ejercicio, 'dd/mm'), '-') = nvl(to_char(v_row_entidad_actual.cierre_ejercicio, 'dd/mm'), '-') and
          nvl(e.cierre_fin_mes , '-') = nvl(v_row_entidad_actual.cierre_fin_mes, '-')
        order by e.matricula desc, e.matricula_version asc, e.anio asc, e.folio asc
      ) t
    where rownum = 1;

    -- Versionado Domicilio
    select matricula, matricula_version, folio, anio into v_matricula_domicilio, v_version_domicilio, v_folio_domicilio, v_anio_domicilio
    from
      ( select e.matricula, e.matricula_version, e.folio, e.anio
        from IPJ.t_entidades e join ipj.t_tramitesipj t
          on e.Id_Tramite_Ipj = t.Id_Tramite_Ipj
        where
          e.id_legajo = p_id_legajo and
          e.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_id_ubicacion_ent) and
          T.ID_ESTADO_ULT < IPJ.TYPES.C_ESTADOS_RECHAZADO and
          (E.Id_Tramite_Ipj = v_Row_Entidad_Actual.Id_Tramite_Ipj or e.borrador = 'N') and
          nvl(id_vin_real, 0) = nvl(v_Row_Entidad_Actual.id_vin_real, 0)
        order by e.matricula desc, e.matricula_version asc, e.anio asc, e.folio asc
       ) t
    where rownum = 1;

    -- Versionado Denominacion
    select matricula, matricula_version, folio, anio into v_matricula_denominacion, v_version_denominacion, v_folio_denominacion, v_anio_denominacion
    from
      ( select e.matricula, e.matricula_version, e.folio, e.anio
        from IPJ.t_entidades e join ipj.t_tramitesipj t
          on e.Id_Tramite_Ipj = t.Id_Tramite_Ipj
        where
          e.id_legajo = p_id_legajo and
          e.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_id_ubicacion_ent) and
          T.ID_ESTADO_ULT < IPJ.TYPES.C_ESTADOS_RECHAZADO and
          (E.Id_Tramite_Ipj = v_Row_Entidad_Actual.Id_Tramite_Ipj or e.borrador = 'N') and
          e.denominacion_1 = v_Row_Entidad_Actual.denominacion_1
        order by e.matricula desc, e.matricula_version asc, e.anio asc, e.folio asc
       ) t
    where rownum = 1;

    -- Versionado Tipo Administracion (Representacion)
    select matricula, matricula_version, folio, anio into v_matricula_Tipo_Admin, v_version_Tipo_Admin, v_folio_Tipo_Admin, v_anio_Tipo_Admin
    from
      ( select e.matricula, e.matricula_version, e.folio, e.anio
        from IPJ.t_entidades e join ipj.t_tramitesipj t
          on e.Id_Tramite_Ipj = t.Id_Tramite_Ipj
        where
          e.id_legajo = p_id_legajo and
          e.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_id_ubicacion_ent) and
          T.ID_ESTADO_ULT < IPJ.TYPES.C_ESTADOS_RECHAZADO and
          (E.Id_Tramite_Ipj = v_Row_Entidad_Actual.Id_Tramite_Ipj or e.borrador = 'N') and
          nvl(e.id_tipo_administracion , 0) = nvl(v_Row_Entidad_Actual.id_tipo_administracion, 0)
        order by e.matricula desc, e.matricula_version asc, e.anio asc, e.folio asc
       ) t
    where rownum = 1;

    -- Versionado Objeto Social
    select matricula, matricula_version, folio, anio into v_matricula_obj_social, v_version_obj_social, v_folio_obj_social, v_anio_obj_social
    from
      ( select e.matricula, e.matricula_version, e.folio, e.anio
        from IPJ.t_entidades e join ipj.t_tramitesipj t
          on e.Id_Tramite_Ipj = t.Id_Tramite_Ipj
        where
          e.id_legajo = p_id_legajo and
          e.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_id_ubicacion_ent) and
          T.ID_ESTADO_ULT < IPJ.TYPES.C_ESTADOS_RECHAZADO and
          (E.Id_Tramite_Ipj = v_Row_Entidad_Actual.Id_Tramite_Ipj or e.borrador = 'N') and
          dbms_lob.compare(nvl(objeto_social, 'NULL'), nvl(v_Row_Entidad_Actual.objeto_social, 'NULL')) = 0
        order by e.matricula desc, e.matricula_version asc, e.anio asc, e.folio asc
       ) t
    where rownum = 1;

    -- Versionado Capital, Cuotas y Monto Cuotas
    select matricula, matricula_version, folio, anio into v_matricula_monto, v_version_monto, v_folio_monto, v_anio_monto
    from
      ( select e.matricula, e.matricula_version, e.folio, e.anio
        from IPJ.t_entidades e join ipj.t_tramitesipj t
          on e.Id_Tramite_Ipj = t.Id_Tramite_Ipj
        where
          e.id_legajo = p_id_legajo and
          e.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_id_ubicacion_ent) and
          T.ID_ESTADO_ULT < IPJ.TYPES.C_ESTADOS_RECHAZADO and
          (E.Id_Tramite_Ipj = v_Row_Entidad_Actual.Id_Tramite_Ipj or e.borrador = 'N') and
          nvl(e.valor_cuota, 0) = nvl(v_Row_Entidad_Actual.valor_cuota, 0) and
          nvl(e.cuotas, 0) = nvl(v_Row_Entidad_Actual.cuotas, 0) and
          nvl(e.monto, 0) = nvl(v_Row_Entidad_Actual.monto, 0) and
          nvl(e.id_moneda, '0') = nvl(v_Row_Entidad_Actual.id_moneda, '0')
        order by e.matricula desc, e.matricula_version asc, e.anio asc, e.folio asc
       ) t
    where rownum = 1;
    */
    -- Versionado Socios
    begin
      select matricula, matricula_version, folio, anio into v_matricula_socios, v_version_socios, v_folio_socios, v_anio_socios
      from
        ( select e.matricula, e.matricula_version, e.folio, e.anio
          from IPJ.t_entidades_socios es join IPJ.t_entidades e
              on es.Id_Tramite_Ipj = e.Id_Tramite_Ipj and es.id_legajo = e.id_legajo
            join ipj.t_tramitesipj t
              on e.Id_Tramite_Ipj = t.Id_Tramite_Ipj
          where
            e.id_legajo = p_id_legajo and
            e.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_id_ubicacion_ent) and
            T.ID_ESTADO_ULT < IPJ.TYPES.C_ESTADOS_RECHAZADO and
            (E.Id_Tramite_Ipj = v_Row_Entidad_Actual.Id_Tramite_Ipj or
              (e.borrador = 'N' and ES.BORRADOR = 'N'))
          order by e.matricula desc, e.matricula_version desc, nvl(e.anio, 0) desc, e.folio desc
         ) t
      where rownum = 1;
    exception
      when NO_DATA_FOUND then
        v_matricula_socios := v_matricula_monto;
        v_version_socios := v_version_monto;
        v_folio_socios := v_folio_monto;
        v_anio_socios := v_anio_monto;
    end;

    -- *************** Versionado Admin ********************
    -- BUSCO el ultimo tr�mite de administradores, ordenando dependiendo del area
    v_id_Ult_autor := FC_Ult_Tram_Admin(p_id_legajo, v_id_ubicacion_ent,  v_Row_Entidad_Actual.matricula_version, v_Row_Entidad_Actual.Id_Tramite_Ipj);
    begin
      -- Armo un cursor con la administraci�n y busco el maximo versionado de los minimos versionado de cada administrador
      OPEN v_Cursor_Admin FOR
        select ie.*
        from ipj.t_entidades_admin_hist ie
        where
          ie.id_legajo = p_id_legajo and
          ie.Id_Tramite_Ipj = v_id_Ult_autor;

      v_matricula_admin := null;
      v_version_admin := 0;
      v_folio_admin := null;
      v_anio_admin:= 0;
      LOOP
        fetch v_Cursor_Admin into v_Col_Ent_Admin;
        EXIT WHEN v_Cursor_Admin%NOTFOUND or v_Cursor_Admin%NOTFOUND is null;

        --para cada administrador, busco su minima version
        select matricula, matricula_version, folio, anio into v_matricula_admin_temp, v_version_admin_temp, v_folio_admin_temp, v_anio_admin_temp
        from
          ( select e.matricula, e.matricula_version, e.folio, e.anio
            from ipj.t_entidades_admin_hist eah  join ipj.t_entidades_Admin ea
                on ea.id_legajo = eah.id_legajo and ea.id_admin = eah.id_admin
              join ipj.t_tramitesipj t
                on ea.Id_Tramite_Ipj = t.Id_Tramite_Ipj
              join ipj.t_entidades e
                on e.Id_Tramite_Ipj = ea.Id_Tramite_Ipj and e.id_legajo = ea.id_legajo
            where
              ea.id_legajo = p_id_legajo and
              ea.id_integrante = v_Col_Ent_Admin.id_integrante and
              ea.fecha_inicio = v_Col_Ent_Admin.fecha_inicio and
              ea.id_tipo_integrante = v_Col_Ent_Admin.id_tipo_integrante and
              nvl(ea.fecha_fin, sysdate) = nvl(v_Col_Ent_Admin.fecha_fin, sysdate) and
              T.ID_ESTADO_ULT < IPJ.TYPES.C_ESTADOS_RECHAZADO
            order by e.matricula asc, e.matricula_version asc, e.anio asc, e.folio asc
           ) t
        where rownum = 1;

        -- Comparo el versionado del intergrante con el ultimo, y me quedo con el mayor
        if (v_matricula_admin is null and v_matricula_admin_temp is not null) or (v_matricula_admin_temp >= v_matricula_admin) then
           v_matricula_admin := v_matricula_admin_temp;
           if nvl(v_version_admin_temp, 0) > nvl(v_version_admin, 0) then
             v_version_admin := nvl(v_version_admin_temp, 0);
           end if;
        else
          if nvl(v_anio_admin_temp, 0) > nvl(v_anio_admin, 0) then
            v_anio_admin := v_anio_admin_temp;
            v_folio_admin := v_folio_admin_temp;
          else
            if (v_folio_admin is null and v_folio_admin_temp is not null) or (v_folio_admin_temp > v_folio_admin) then
              v_folio_admin := v_folio_admin_temp;
            end if;
          end if;
        end if;


      end loop;
      CLOSE v_Cursor_Admin;

      -- Armo un cursor con sindicos y busco el maximo versionado de los minimos
      -- versionado de cada sindico, arrancando con los de los administradores
      v_id_Ult_autor_fiscal := FC_Ult_Tram_Sindico(p_id_legajo, v_id_ubicacion_ent,  v_Row_Entidad_Actual.matricula_version, v_Row_Entidad_Actual.Id_Tramite_Ipj);
      OPEN v_Cursor_Admin FOR
        select ie.*
        from ipj.t_entidades_sindico_hist ie
        where
          ie.id_legajo = p_id_legajo and
          ie.Id_Tramite_Ipj = v_id_Ult_autor_fiscal;

      LOOP
        fetch v_Cursor_Admin into v_Col_Ent_Admin_Sind;
        EXIT WHEN v_Cursor_Admin%NOTFOUND or v_Cursor_Admin%NOTFOUND is null;

        --para cada sindico, busco su minima version
        select matricula, matricula_version, folio, anio into v_matricula_admin_temp, v_version_admin_temp, v_folio_admin_temp, v_anio_admin_temp
        from
          ( select e.matricula, e.matricula_version, e.folio, e.anio
            from ipj.t_entidades_sindico_hist eah  join ipj.t_entidades_sindico ea
                on ea.id_legajo = eah.id_legajo and ea.id_entidad_sindico = eah.id_entidad_sindico and ea.fecha_alta = eah.fecha_alta and eah.id_tipo_integrante = ea.id_tipo_integrante
              join ipj.t_tramitesipj t
                on ea.Id_Tramite_Ipj = t.Id_Tramite_Ipj
              join ipj.t_entidades e
                on e.Id_Tramite_Ipj = ea.Id_Tramite_Ipj and e.id_legajo = ea.id_legajo
            where
              ea.id_legajo = p_id_legajo and
              ea.id_entidad_sindico = v_Col_Ent_Admin_Sind.id_entidad_sindico and
              ea.fecha_alta = v_Col_Ent_Admin_Sind.fecha_alta and
              ea.id_tipo_integrante = v_Col_Ent_Admin_Sind.id_tipo_integrante and
              nvl(ea.fecha_baja, sysdate) = nvl(v_Col_Ent_Admin_Sind.fecha_baja, sysdate) and
              T.ID_ESTADO_ULT < IPJ.TYPES.C_ESTADOS_RECHAZADO
            order by e.matricula asc, e.matricula_version asc, e.anio asc, e.folio asc
           ) t
        where rownum = 1;

        -- Comparo el versionado del intergrante con el ultimo, y me quedo con el mayor
        if v_matricula_admin_temp >= v_matricula_admin then
           v_matricula_admin := v_matricula_admin_temp;
           if v_version_admin_temp > v_version_admin then
             v_version_admin := v_version_admin_temp;
           end if;
        else
          if v_anio_admin_temp > v_anio_admin then
            v_anio_admin := v_anio_admin_temp;
            v_folio_admin := v_folio_admin_temp;
          else
            if v_folio_admin_temp > v_folio_admin_temp then
              v_folio_admin := v_folio_admin_temp;
            end if;
          end if;
        end if;

      end loop;
      CLOSE v_Cursor_Admin;

    exception
      when NO_DATA_FOUND then
        v_matricula_admin := v_Row_Entidad_Actual.matricula;
        v_version_admin := v_Row_Entidad_Actual.matricula_version;
        v_folio_admin := v_Row_Entidad_Actual.folio;
        v_anio_admin := v_Row_Entidad_Actual.anio;
    end;

    /********************************************************
     Dependiendo de si viene matricula, verison 0 o mayor, o folio y a�o, armo la linea para informar
    ********************************************************/
    --Administracion
    if v_matricula_admin is null then
      v_matricula_admin := 'Inscripta bajo Folio ' || v_folio_admin || ' del A�o ' || to_char(v_anio_admin);
    else
      if nvl(v_version_admin, 0) = 0 then
        v_matricula_admin := 'Matr�cula Nro.' || IPJ.VARIOS.FC_Numero_Matricula(v_matricula_admin) || '-' || replace(IPJ.VARIOS.FC_Letra_Matricula(v_matricula_admin), '#', '');
      else
        v_matricula_admin := 'Modificaci�n inscripta bajo Matr�cula Nro.' || IPJ.VARIOS.FC_Numero_Matricula(v_matricula_admin) || '-' || replace(IPJ.VARIOS.FC_Letra_Matricula(v_matricula_admin), '#', '') || to_char(v_version_admin);
      end if;
    end if;
    --Vigencia
    if v_matricula_vigencia is null then
      v_matricula_vigencia_texto := 'Inscripta bajo Folio ' || v_folio_vigencia || ' del A�o ' || to_char(v_anio_vigencia);
    else
      if nvl(v_version_vigencia, 0) = 0 then
        v_matricula_vigencia_texto := 'Matr�cula Nro.' || IPJ.VARIOS.FC_Numero_Matricula(v_matricula_vigencia) || '-' || replace(IPJ.VARIOS.FC_Letra_Matricula(v_matricula_vigencia), '#', '');
      else
        v_matricula_vigencia_texto := 'Modificaci�n inscripta bajo Matr�cula Nro.' || IPJ.VARIOS.FC_Numero_Matricula(v_matricula_vigencia) || '-' || replace(IPJ.VARIOS.FC_Letra_Matricula(v_matricula_vigencia), '#', '') || to_char(v_version_vigencia);
      end if;
    end if;
    --Domicilio
    if v_matricula_domicilio is null then
      v_matricula_domicilio_texto := 'Inscripta bajo Folio ' || v_folio_domicilio || ' del A�o ' || to_char(v_anio_domicilio);
    else
      if nvl(v_version_domicilio, 0) = 0 then
        v_matricula_domicilio_texto := 'Matr�cula Nro.' || IPJ.VARIOS.FC_Numero_Matricula(v_matricula_domicilio) || '-' || replace(IPJ.VARIOS.FC_Letra_Matricula(v_matricula_domicilio), '#', '');
      else
        v_matricula_domicilio_texto := 'Modificaci�n inscripta bajo Matr�cula Nro.' || IPJ.VARIOS.FC_Numero_Matricula(v_matricula_domicilio) || '-' || replace(IPJ.VARIOS.FC_Letra_Matricula(v_matricula_domicilio), '#', '') || to_char(v_version_domicilio);
      end if;
    end if;
    --Objeto Social
    if v_matricula_obj_social is null then
      v_matricula_obj_social_texto := 'Inscripta bajo Folio ' || v_folio_obj_social || ' del A�o ' || to_char(v_anio_obj_social);
    else
      if nvl(v_version_obj_social, 0) = 0 then
        v_matricula_obj_social_texto := 'Matr�cula Nro.' || IPJ.VARIOS.FC_Numero_Matricula(v_matricula_obj_social) || '-' || replace(IPJ.VARIOS.FC_Letra_Matricula(v_matricula_obj_social), '#', '');
      else
        v_matricula_obj_social_texto := 'Modificaci�n inscripta bajo Matr�cula Nro.' || IPJ.VARIOS.FC_Numero_Matricula(v_matricula_obj_social) || '-' || replace(IPJ.VARIOS.FC_Letra_Matricula(v_matricula_obj_social), '#', '') || to_char(v_version_obj_social);
      end if;
    end if;
    --Socios y Cuotas Sociales
    if v_matricula_socios is null and  v_matricula_monto is null then
      if nvl(v_anio_socios, 0) > nvl(v_anio_monto, 0) then
        v_matricula_socios := 'Inscripta bajo Folio ' || v_folio_socios || ' del A�o ' || to_char(v_anio_socios);
      else
        if nvl(v_anio_socios, 0) < nvl(v_anio_monto, 0) then
          v_matricula_socios := 'Inscripta bajo Folio ' || v_folio_monto || ' del A�o ' || to_char(v_anio_monto);
        else
          if v_folio_socios >= v_folio_monto then
            v_matricula_socios := 'Inscripta bajo Folio ' || v_folio_socios || ' del A�o ' || to_char(v_anio_socios);
          else
            v_matricula_socios := 'Inscripta bajo Folio ' || v_folio_monto || ' del A�o ' || to_char(v_anio_monto);
          end if;
        end if;
      end if;
    else
      if GREATEST(nvl(v_version_socios, 0), nvl(v_version_monto, 0)) = 0 then
        v_matricula_socios := 'Matr�cula Nro.' || IPJ.VARIOS.FC_Numero_Matricula(nvl(v_matricula_socios, v_matricula_monto)) || '-' || replace(IPJ.VARIOS.FC_Letra_Matricula(nvl(v_matricula_socios, v_matricula_monto)), '#', '');
      else
        v_matricula_socios := 'Modificaci�n inscripta bajo Matr�cula Nro.' || IPJ.VARIOS.FC_Numero_Matricula(nvl(v_matricula_socios, v_matricula_monto)) || '-' || replace(IPJ.VARIOS.FC_Letra_Matricula(nvl(v_matricula_socios, v_matricula_monto)), '#', '') || to_char(GREATEST(nvl(v_version_socios, 0), nvl(v_version_monto, 0)));
      end if;
    end if;

    -- Busco expediente del versionado de admin
    begin
      if v_id_ubicacion_ent = IPJ.TYPES.C_AREA_CYF then
        --Para ACyF busco el primer expediente que nombro alguna autoridad
        select expediente into v_nro_expediente_admin
        from
          ( select tr.expediente
            from ipj.t_entidades_admin_hist h join ipj.t_entidades_admin e
                on h.id_legajo = e.id_legajo and h.id_admin = e.id_admin
              join ipj.t_tramitesipj tr
                on tr.Id_Tramite_Ipj = e.Id_Tramite_Ipj
            where
              h.id_tramite_ipj = v_id_Ult_autor and
              h.id_legajo = p_id_legajo
            order by nvl(tr.id_proceso, 0) desc, TR.ID_TRAMITE asc, tr.id_tramite_ipj asc
            ) t
        where rownum = 1;
      else
        -- Si no es ACyF, busco el expediente ordenado por versionado
        select expediente into v_nro_expediente_admin
        from
          ( select tr.expediente
            from ipj.t_entidades_admin_hist h join ipj.t_entidades e
                on h.Id_Tramite_Ipj = e.Id_Tramite_Ipj and h.id_legajo = e.id_legajo
              join ipj.t_tramitesipj tr
                on tr.Id_Tramite_Ipj = h.Id_Tramite_Ipj
            where
              h.id_legajo = p_id_legajo and
              e.Matricula_Version <= v_version_admin and
              ( h.id_tramite_ipj = p_id_tramite_ipj or
                (tr.Id_Estado_Ult < IPJ.TYPES.C_ESTADOS_RECHAZADO and tr.Id_Estado_Ult >= IPJ.TYPES.C_ESTADOS_COMPLETADO)
              )
            order by e.matricula asc , matricula_version desc, nvl(e.anio, 0) desc, e.folio desc, h.Id_Tramite_Ipj desc
            ) t
        where rownum = 1;
      end if;
    exception
      when NO_DATA_FOUND then
        v_nro_expediente_admin := null;
    end;

    --***********************************************************
    -- Armo el estado de la entidad, controlando BALANCES, VIGENCIA y AUTORIDADES
    --***********************************************************
    v_ACyF_VALIDA := 0;
    v_Balances_Pend := IPJ.ENTIDAD_PERSJUR.FC_Balances_Pendientes(p_id_legajo, v_Id_Tramite_Ipj);
    if v_Balances_Pend is not null then
      v_ACyF_VALIDA := v_ACyF_VALIDA + 1;
    end if;

    /*  AGREGADO EL D�A 02/06/2017 POR DANIEL CHAUQUE
         PARA CONTROLAR QUE SEA VALIDO 29 DE FEBRERO DEL 2099 (SUMANDOLES LA VIGENCIA)
        QUE REALMENTE SEA BICIESTRO.
   */

    if(v_Row_Entidad_Actual.Fec_Inscripcion <> null) then
      if(ipj.varios.f_date(to_char(v_Row_Entidad_Actual.Fec_Inscripcion, 'dd/mm')
        || '/' || to_char(to_number(to_char(v_Row_Entidad_Actual.Fec_Inscripcion, 'rrrr'))+
        nvl(v_Row_Entidad_Actual.vigencia, 0))) = 0) then
           v_check_date:= to_date('01/03/' || to_char(to_number(to_char(v_Row_Entidad_Actual.Fec_Inscripcion, 'rrrr'))+ nvl(v_Row_Entidad_Actual.vigencia, 0)), 'dd/mm/rrrr');
      else
        v_check_date:=  to_date( to_char(v_Row_Entidad_Actual.Fec_Inscripcion, 'dd/mm')  || '/' || to_char(to_number(to_char(v_Row_Entidad_Actual.Fec_Inscripcion, 'rrrr'))+ nvl(v_Row_Entidad_Actual.vigencia, 0)), 'dd/mm/rrrr');
      end if;
    end if;

    if v_Row_Entidad_Actual.tipo_vigencia = 1 and -- fecha Inscripcion
        ( v_Row_Entidad_Actual.Fec_Inscripcion is null or
         v_check_date < sysdate
        ) then
      v_ACyF_VALIDA := v_ACyF_VALIDA + 2;
    end if;


    if v_Row_Entidad_Actual.tipo_vigencia = 2 and -- Acta Constitutiva
        ( v_Row_Entidad_Actual.acta_contitutiva is null or
          to_date(to_char(to_date(v_Row_Entidad_Actual.acta_contitutiva, 'dd/mm/rrrr'), 'dd/mm') || '/' || to_char(to_number(to_char(to_date(v_Row_Entidad_Actual.acta_contitutiva, 'dd/mm/rrrr'), 'rrrr'))+ nvl(v_Row_Entidad_Actual.vigencia, 0)), 'dd/mm/rrrr') < sysdate
        ) then
      v_ACyF_VALIDA := v_ACyF_VALIDA + 2;
    end if;
    if v_Row_Entidad_Actual.tipo_vigencia = 3 and -- Otra Fecha
        ( v_Row_Entidad_Actual.fec_vig is null or
          to_date(to_char(v_Row_Entidad_Actual.fec_vig, 'dd/mm') || '/' || to_char(to_number(to_char(v_Row_Entidad_Actual.fec_vig, 'rrrr'))+ nvl(v_Row_Entidad_Actual.vigencia, 0)), 'dd/mm/rrrr') < sysdate
        ) then
      v_ACyF_VALIDA := v_ACyF_VALIDA + 2;
    end if;
    if v_Row_Entidad_Actual.tipo_vigencia is null then -- No tiene vigencia
      v_ACyF_VALIDA := v_ACyF_VALIDA + 2;
    end if;

    -- Busco los datos del Domicilio
    begin
      select initcap(d.n_localidad), initcap(d.n_departamento) ,
        IPJ.VARIOS.FC_ARMAR_CALLE_DOM(d.n_calle, d.altura, d.piso, d.depto, d.torre, d.mzna, d.lote, d.n_barrio, d.km, d.n_tipocalle)
        into v_localidad, v_departamento, v_calle
      from dom_manager.VT_DOMICILIOS_COND d
      where
        id_vin = (select id_vin_real from ipj.t_entidades where Id_Tramite_Ipj = v_Id_Tramite_Ipj and id_legajo = p_id_legajo);
    exception
      when OTHERS then
        v_localidad := null;
        v_departamento := null;
        v_calle := null;
    end;

    -- ARMO EL CURSOR CON TODOS LOS DATOS PARA EL INFORME
    OPEN p_Cursor FOR
      select --distinct
        e.objeto_social desc_obj_social, trim(To_Char(nvl(e.monto, '0'), '99999999999999999990'))  monto , e.acta_contitutiva,
        e.vigencia, e.id_unidad_med, e.id_moneda, To_Char(e.cierre_ejercicio, 'DD/MM') cierre_ejercicio,
        to_char(e.fec_vig, 'dd/mm/rrrr') fec_vig, e.cuit, e.denominacion_1, to_char(e.alta_temporal, 'dd/mm/rrrr') alta_temporal,
        to_char(e.baja_temporal, 'dd/mm/rrrr') baja_temporal, nvl(e.borrador, 'S') borrador,
        TOR.ID_TIPO_ORIGEN, TOR.N_TIPO_ORIGEN, e.Cierre_Fin_Mes,
        case nvl(e.denominacion_1, '') when '' then 'N' else 'S' end Existe,
        e.Id_Tramite_Ipj,  e.ID_SEDE,
        to_char(e.FEC_INSCRIPCION, 'dd/mm/rrrr') FEC_INSCRIPCION, v_folio_const FOLIO, e.LIBRO_NRO,  e.TOMO,  v_anio_const anio,
        e.ID_VIN_COMERCIAL, e.ID_VIN_REAL, e.FEC_ACTO,  e.DENOMINACION_2, e.tipo_vigencia,
        trim(To_Char(nvl(e.valor_cuota, '0'), '99999999999999999990.99')) valor_cuota,
        e.cuotas, e.MATRICULA_VERSION,  e.id_tipo_administracion, initcap(nvl(M.N_MONEDA_PLURAL, M.N_MONEDA)) N_MONEDA,
        nvl(M.SIGNO, '$') Signo_Moneda, TA.N_TIPO_ADMINISTRACION, e.id_tipo_entidad,
        E.ID_ESTADO_ENTIDAD, TE.ESTADO_ENTIDAD, E.OBS_CIERRE,
        IPJ.ENTIDAD_PERSJUR.FC_ESTADO_GRAVAMEN_PERSJUR(e.id_legajo) Estado_Gravamen,
        e.NRO_RESOLUCION, to_char(e.FEC_RESOLUCION, 'dd/mm/rrrr') FEC_RESOLUCION,
        e.NRO_BOLETIN, to_char(e.FEC_BOLETIN, 'dd/mm/rrrr') FEC_BOLETIN,
        e.NRO_REGISTRO, e.SEDE_ESTATUTO, e.REQUIERE_SINDICO, e.ORIGEN, e.ES_SUCURSAL, e.OBSERVACION,
        IPJ.TYPES.fc_numero_a_letras(e.Monto) Monto_Letras,
        IPJ.TYPES.fc_numero_a_letras(e.valor_cuota) Valor_Cuotas_Letras,
        IPJ.TYPES.fc_numero_a_letras(e.cuotas) Cuotas_Letras,
        IPJ.VARIOS.FC_Numero_Matricula(e.MATRICULA) MATRICULA,
        replace(IPJ.VARIOS.FC_Letra_Matricula(e.MATRICULA), '#', '') Letra,
        v_matricula_vigencia_texto version_vigencia, v_matricula_domicilio_texto version_domicilio,
        v_matricula_obj_social_texto version_obj_social,
        v_matricula_socios  version_socios,
        v_matricula_admin version_admin,
        (case nvl(tipo_vigencia, 0)
           when 1 then 'desde la fecha de inscripci�n'
           when 2 then 'desde el acta constitutiva'
           when 3 then 'desde el '
           else ''
        end) N_TIPO_VIGENCIA, v_nro_expediente_admin nro_expediente_admin,
        E.NRO_REGISTRO ,
        IPJ.TRAMITES.FC_EXPEDIENTE_TRAMITE_IPJ(e.Id_Tramite_Ipj) Exp_Entidad,
        v_Balances_Pend ENT_BALANCES, v_ACyF_VALIDA ACyF_VALIDA,
        to_char(e.fec_vig_hasta, 'dd/mm/rrrr') fec_vig_hasta,
        v_localidad n_lodalidad, v_departamento n_departamento, v_calle calle_inf,
        IPJ.ENTIDAD_PERSJUR.FC_ESTADO_GRAVAMEN_INTERV(e.id_legajo) Est_Interv,
        IPJ.VARIOS.FC_Numero_Matricula(e.MATRICULA) MATRICULA,
        replace(IPJ.VARIOS.FC_Letra_Matricula(e.MATRICULA), '#', '') Letra,
        to_char(IPJ.VARIOS.FC_Numero_Matricula(v_matricula_vigencia)) || '-' ||replace(IPJ.VARIOS.FC_Letra_Matricula(v_matricula_vigencia), '#', '') || to_char(decode(v_version_vigencia, 0, null, v_version_vigencia)) version_nro_vigencia,
        to_char(IPJ.VARIOS.FC_Numero_Matricula(v_matricula_domicilio)) || '-' ||replace(IPJ.VARIOS.FC_Letra_Matricula(v_matricula_domicilio), '#', '') || to_char(decode(v_version_domicilio, 0, null, v_version_domicilio)) version_nro_domicilio,
        to_char(IPJ.VARIOS.FC_Numero_Matricula(v_matricula_obj_social)) || '-' ||replace(IPJ.VARIOS.FC_Letra_Matricula(v_matricula_obj_social), '#', '') || to_char(decode(v_version_obj_social, 0, null, v_version_obj_social)) version_nro_obj_social,
        to_char(IPJ.VARIOS.FC_Numero_Matricula(v_matricula_denominacion)) || '-' ||replace(IPJ.VARIOS.FC_Letra_Matricula(v_matricula_denominacion), '#', '') || to_char(decode(v_version_denominacion, 0, null, v_version_denominacion)) version_nro_denominacion,
        to_char(IPJ.VARIOS.FC_Numero_Matricula(v_matricula_estado)) || '-' ||replace(IPJ.VARIOS.FC_Letra_Matricula(v_matricula_estado), '#', '') || to_char(decode(v_version_estado, 0, null, v_version_estado)) version_nro_Estado,
        to_char(IPJ.VARIOS.FC_Numero_Matricula(v_matricula_monto)) || '-' ||replace(IPJ.VARIOS.FC_Letra_Matricula(v_matricula_monto), '#', '') || to_char(decode(v_version_monto, 0, null, v_version_monto)) version_nro_monto,
        to_char(IPJ.VARIOS.FC_Numero_Matricula(v_matricula_cierre)) || '-' ||replace(IPJ.VARIOS.FC_Letra_Matricula(v_matricula_cierre), '#', '') || to_char(decode(v_version_cierre, 0, null, v_version_cierre)) version_nro_cierre,
        v_anio_vigencia anio_vigencia, v_anio_domicilio anio_domicilio, v_anio_obj_social anio_obj_social, v_anio_denominacion anio_denominacion, v_anio_estado anio_estado,
        v_anio_monto anio_monto, v_anio_cierre anio_cierre,
        v_folio_vigencia folio_vigencia, v_folio_domicilio folio_domicilio, v_folio_obj_social folio_obj_social, v_folio_denominacion folio_denominacion, v_folio_estado folio_estado,
        v_folio_monto folio_monto, v_folio_cierre folio_cierre,
        (select t.tipo_entidad from ipj.t_tipos_entidades t where t.id_tipo_entidad = e.id_tipo_entidad) tipo_entidad,
        to_char(IPJ.VARIOS.FC_Numero_Matricula(v_matricula_Tipo_Admin)) || '-' ||replace(IPJ.VARIOS.FC_Letra_Matricula(v_matricula_Tipo_Admin), '#', '') || to_char(decode(v_version_Tipo_Admin, 0, null, v_version_Tipo_Admin)) version_Tipo_Admin,
        v_anio_Tipo_Admin anio_Tipo_Admin, v_folio_Tipo_Admin folio_Tipo_Admin
      from ipj.t_entidades e left join T_COMUNES.T_MONEDAS m
          on E.ID_MONEDA = M.ID_MONEDA
        left join IPJ.T_TIPOS_ADMINISTRACION ta
          on  E.ID_TIPO_ADMINISTRACION = TA.ID_TIPO_ADMINISTRACION
        left join IPJ.T_ESTADOS_ENTIDADES te
          on E.ID_ESTADO_ENTIDAD = TE.ID_ESTADO_ENTIDAD
        left join IPJ.t_tipos_origen tor
          on e.id_tipo_origen = tor.id_tipo_origen
      where
        e.Id_Tramite_Ipj = v_Id_Tramite_Ipj and
        e.id_legajo = p_id_legajo;

    if o_rdo is null then
      o_rdo := 'OK';
    end if;
  END SP_Informar_Pers_Juridica;

  PROCEDURE SP_Informar_Entidad_Soc_Usuf(
    o_Cursor OUT TYPES.cursorType,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number)
  IS
  /*********************************************************
  Este procemiento devuelve el listado de usufructuarios relacionados a un socio
  **********************************************************/
    v_id_ubicacion_ent number;
  BEGIN
    -- Buso el area actual de la entidad, para filtrar los datos
    select te.id_ubicacion into v_id_ubicacion_ent
    from ipj.t_entidades e join ipj.t_tipos_entidades te
      on e.id_tipo_entidad = te.id_tipo_entidad
    where
      e.id_tramite_ipj = p_id_tramite_ipj and
      e.Id_Legajo = p_id_legajo;

    OPEN o_Cursor FOR
      select esu.id_entidad_socio,  esu.id_integrante, esu.borrador,
        to_char(esu.fec_alta, 'dd/mm/rrrr') fec_alta,
        to_char(esu.fec_baja, 'dd/mm/rrrr') fec_baja,
        (p.NOMBRE || ' ' ||p.APELLIDO)  detalle,
        I.ID_NUMERO, I.ID_SEXO, I.NRO_DOCUMENTO, I.PAI_COD_PAIS,
        I.ERROR_DATO
      from ipj.t_entidades_socios es join ipj.T_ENTIDADES_SOCIOS_USUF esu
           on es.id_entidad_socio = esu.id_entidad_socio
        join ipj.t_entidades e
          on es.id_legajo = e.id_legajo and es.id_tramite_ipj = e.id_tramite_ipj
        join ipj.t_integrantes i
          on esu.id_integrante = i.id_integrante
        left join rcivil.vt_pk_persona p
          on i.id_sexo = p.ID_SEXO and
            i.nro_documento = p.NRO_DOCUMENTO and
            i.pai_cod_pais = p.PAI_COD_PAIS and
            i.id_numero = p.ID_NUMERO
      where
        es.id_legajo = p_id_legajo and
        e.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_id_ubicacion_ent) and
        (es.Id_Tramite_Ipj = p_Id_Tramite_Ipj or es.borrador = 'N') and
        (esu.fec_baja is null or esu.fec_baja >= sysdate or esu.borrador ='S');

  END SP_Informar_Entidad_Soc_Usuf;

  PROCEDURE SP_GUARDAR_ENTIDAD_ACTAS_ORDEN(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_entidad_acta in number,
    p_id_tipo_orden_dia in number,
    p_eliminar in number) -- 1 Elimina
  IS
    v_existe number;
    v_valid_parametros varchar2(500);
    v_id_entidad_acta number;
  BEGIN
  /*******************************************************
    Este procedimiento guarda la orden del dia de una asamblea o acta.
  ********************************************************/
    if IPJ.VARIOS.Valida_Tipo_OrdenDia(nvl(p_id_tipo_orden_dia, 0)) = 0 then
      v_valid_parametros := v_valid_parametros || 'Orden del D�a Inv�lida';
    end if;

    if v_valid_parametros is not null then
      o_rdo := 'GUARDAR ACTAS ORDEN - Parametros: '  || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    -- CUERPO PROCEDIMIENTO
    if p_eliminar  = 1 then
      delete IPJ.T_Entidades_Acta_Orden
      where
        id_entidad_acta = p_id_entidad_acta and
        id_tipo_orden_dia = p_id_tipo_orden_dia;
    else
      -- Si existe un registro, actualizo los datos
      select count(*) into v_existe
      from IPJ.t_entidades_acta_orden
      where
        id_entidad_acta = p_id_entidad_acta and
        id_tipo_orden_dia = p_id_tipo_orden_dia;

      if nvl(v_existe, 0) = 0 and p_id_entidad_acta <> 0 then

        insert into ipj.t_entidades_acta_orden
          (id_entidad_acta, id_tipo_orden_dia)
        values
          (p_id_entidad_acta, p_id_tipo_orden_dia);
       end if;
    end if;

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     WHEN OTHERS THEN
       o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_GUARDAR_ENTIDAD_ACTAS_ORDEN;

  PROCEDURE SP_GUARDAR_ENTIDAD_ACTAS_ARCH(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_entidad_acta in number,
    p_id_archivo in number,
    p_id_tipo_acta in number,
    p_fecha in varchar2,
    p_id_integrante in number,
    p_id_sexo in varchar2,
    p_nro_documento in varchar2,
    p_pai_cod_pais in varchar2,
    p_id_numero in number,
    p_detalle in varchar2,
    p_nombre in varchar2,
    p_apellido in varchar2,
    p_error_Dato in varchar2,
    p_n_tipo_documento in varchar2,
    p_eliminar in number)-- 1 Elimina
  IS
    v_existe number;
    v_valid_parametros varchar2(500);
    v_id_entidad_acta number;
    v_id_integrante number;
    v_tipo_mensaje number;
  BEGIN
  /*******************************************************
    Este procedimento guarda los archivos presentados en una asamblea o acta
  ********************************************************/
    if IPJ.VARIOS.Valida_Arch_Acta(nvl(p_id_archivo, 0)) = 0 then
      v_valid_parametros := v_valid_parametros || 'Archivo Inv�lido';
    end if;
    if p_fecha is not null and IPJ.VARIOS.Valida_Fecha(p_fecha) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT');
    end if;

    if v_valid_parametros is not null then
      o_rdo := 'GUARDAR ACTAS ARCH - Parametros: '  || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

     -- CUERPO PROCEDIMIENTO
    if p_eliminar  = 1 then
      delete IPJ.T_ENTIDADES_ACTA_ARCH
      where
        id_entidad_acta = p_id_entidad_acta and
        id_archivo = p_id_archivo and
        id_tipo_Acta = p_id_tipo_acta;
    else
      -- Si el integrante no existe, lo agrego y luego continuo
      if IPJ.VARIOS.Valida_Integrante(p_id_integrante) = 0 and p_nro_documento is not null then
        IPJ.ENTIDAD_PERSJUR.SP_GUARDAR_INTEGRANTES(
          o_rdo => v_valid_parametros,
          o_tipo_mensaje => v_tipo_mensaje,
          o_id_integrante => v_id_integrante,
          p_id_sexo => p_id_sexo,
          p_nro_documento => p_nro_documento,
          p_pai_cod_pais => p_pai_cod_pais,
          p_id_numero => p_id_numero,
          p_cuil => null,
          p_detalle => p_detalle,
          p_Nombre => p_nombre,
          p_Apellido => p_apellido,
          p_error_dato => p_error_Dato,
          p_n_tipo_documento => p_n_tipo_documento );

        if v_valid_parametros <> TYPES.c_Resp_OK then
           o_rdo := IPJ.VARIOS.MENSAJE_ERROR('INT_NOT') || '. ' || v_valid_parametros;
           o_tipo_mensaje := v_tipo_mensaje;
           return;
        end if;
      else
        if p_error_dato = 'S' then
          update ipj.t_integrantes
          set
            error_dato = p_error_dato,
            detalle = p_detalle
          where
            id_integrante = p_id_integrante;
        end if;
        v_id_integrante := p_id_integrante;
      end if;

      -- Si existe un registro, actualizo los datos
      -- cambio los 0 por null para respetar la FK
      if v_id_integrante = 0 then
        v_id_integrante := null;
      end if;

      update IPJ.T_ENTIDADES_ACTA_ARCH
      set
        fecha = to_date(fecha, 'dd/mm/rrrr'),
        id_integrante = v_id_integrante
      where
        id_entidad_acta = p_id_entidad_acta and
        id_archivo = p_id_archivo and
        id_tipo_Acta = p_id_tipo_acta;

      if sql%rowcount = 0 then
        insert into ipj.T_ENTIDADES_ACTA_ARCH
          (id_entidad_acta, id_archivo, id_tipo_acta, fecha, id_integrante)
        values
          (p_id_entidad_acta, p_id_archivo, p_id_tipo_acta, to_date(p_fecha, 'dd/mm/rrrr'), v_id_integrante);
       end if;
    end if;

    o_rdo := 'OK';
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     WHEN OTHERS THEN
       o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_GUARDAR_ENTIDAD_ACTAS_ARCH;

  PROCEDURE SP_GUARDAR_ENTIDAD_ACTA_DOM(
    o_id_vin out number,
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number,
    p_id_entidad_Acta in number,
    p_id_vin in number,
    P_ID_PROVINCIA in varchar2,
    P_ID_DEPARTAMENTO in number,
    P_ID_LOCALIDAD in number,
    P_BARRIO in varchar2,
    P_CALLE in varchar2,
    P_ALTURA in number,
    P_DEPTO in varchar2,
    P_PISO in varchar2,
    p_torre in varchar2,
    p_manzana in varchar2,
    p_lote in varchar2,
    p_id_tipocalle in number,
    p_km in varchar,
    p_id_calle in number,
    p_id_barrio in number)
  IS
    v_usuario varchar2(20);
  BEGIN
  /*************************************************************
    Inserta o Actualiza un domicilio para una empresa en DOM_MANAGER
    No maneja transaccion, ya lo hace DOM_MANAGER
  *************************************************************/
    -- Busco el responsable de la primer accion
    select cuil_usuario into v_usuario
    from ipj.t_tramitesipj_acciones
    where
      id_tramite_ipj = p_id_tramite_ipj and
      id_estado < 100 and
      rownum = 1;

    IPJ.VARIOS.SP_GUARDAR_DOMICILIO(
      o_id_vin => o_id_vin,
      o_rdo => o_rdo,
      o_tipo_mensaje => o_tipo_mensaje,
      p_id_vin => p_id_vin,
      p_usuario => v_usuario,
      p_Tipo_Domicilio => 0, -- 3 Real //  0 Sin Asignar
      p_id_integrante => 0,
      p_id_legajo => 0,
      p_cuit_empresa => null,
      p_id_fondo_comercio => 0,
      p_id_entidad_acta => p_id_entidad_acta,
      p_es_comerciante => 'N',
      p_es_admin_entidad => 'N',
      P_ID_PROVINCIA => p_id_provincia,
      P_ID_DEPARTAMENTO => p_id_departamento,
      P_ID_LOCALIDAD => p_id_localidad,
      P_BARRIO => p_barrio,
      P_CALLE => p_calle,
      P_ALTURA => p_altura,
      P_DEPTO => p_depto,
      P_PISO => p_piso,
      p_torre => p_torre,
      p_manzana => p_manzana,
      p_lote => p_lote,
      p_id_barrio => p_id_barrio,
      p_id_calle => p_id_calle,
      p_id_tipocalle => p_id_tipocalle,
      p_km => p_km
    );

    -- Si se grabo bien, actualizo la direccion en el acta.
    if o_rdo = IPJ.TYPES.C_RESP_OK then
      update IPJ.t_entidades_acta
      set id_vin = o_id_vin
      where
        id_entidad_acta = p_id_entidad_acta and
        Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        id_legajo = p_id_legajo;
    end if;

  EXCEPTION
     WHEN OTHERS THEN
        o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_GUARDAR_ENTIDAD_ACTA_DOM;

  PROCEDURE SP_Traer_PersJur_Acta_Dom(
      p_Id_Tramite_Ipj in number,
      p_id_legajo in number,
      p_id_entidad_Acta in number,
      p_Cursor OUT TYPES.cursorType)
  IS
    v_existe_domicilio number;
  BEGIN
    /********************************************************
      Este procemiento devuelve la direccion de la sede una entidad de un tramite
    *********************************************************/
    -- controlo si la entidad ya tiene una direccion
    begin
      select id_vin into v_existe_domicilio
      from IPJ.T_ENTIDADES_ACTA
      where
        Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        id_legajo = p_id_legajo and
        id_entidad_acta = p_id_entidad_acta;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN v_existe_domicilio := 0;
    end;

    ipj.varios.SP_Traer_Domicilio(
      p_id_vin => v_existe_domicilio,
      p_id_integrante => 0,
      p_id_legajo  => 0,
      p_cuit_empresa => null,
      p_id_fondo_comercio => 0,
      p_id_entidad_acta => p_id_entidad_acta,
      p_es_comerciante => 'N',
      p_es_admin_entidad => 'N',
      p_Cursor => p_cursor
    );
  END SP_Traer_PersJur_Acta_Dom;


 PROCEDURE SP_Traer_PersJur_Int_Admin_Dom(
      p_Id_Tramite_Ipj in number,
      p_id_legajo in number,
      p_id_tipo_integrante in number,
      p_id_integrante in number,
      p_fecha_inicio in varchar2,
      p_Cursor OUT TYPES.cursorType)
  IS
    v_existe_domicilio number;
  BEGIN
    /********************************************************
      Este procemiento devuelve la direccion de un admistrador de una entidad de un tramite
    *********************************************************/
    -- controlo si la entidad ya tiene una direccion
    begin
      select id_vin into v_existe_domicilio
      from IPJ.T_ENTIDADES_admin
      where
        Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        id_legajo = p_id_legajo and
        id_tipo_integrante = p_id_tipo_integrante and
        fecha_inicio = to_date(p_fecha_inicio, 'dd/mm/rrrr');

    EXCEPTION
      WHEN NO_DATA_FOUND THEN v_existe_domicilio := 0;
    end;

    ipj.varios.SP_Traer_Domicilio(
      p_id_vin => v_existe_domicilio,
      p_id_integrante => p_id_integrante,
      p_id_legajo  => 0,
      p_cuit_empresa => null,
      p_id_fondo_comercio => 0,
      p_id_entidad_acta => 0,
      p_es_comerciante => 'N',
      p_es_admin_entidad => 'S',
      p_Cursor => p_cursor
    );
  END SP_Traer_PersJur_Int_Admin_Dom;


  PROCEDURE SP_GENERAR_MATRICULA_FUNDACION(
    o_matricula out varchar2,
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_razon_social in varchar2)
  IS
    v_letra varchar2(1);
    v_numero_Leg number;
    v_numero_Ent number;
  BEGIN
  /********************************************************
   Genera una matricula para una AC o Fundacion.
   La matricula es la primer letra del nombre y un numero creciente por letra.
  *********************************************************/
    v_letra := substr(trim(p_razon_social), 1, 1);
    begin
      begin
        select NRO_FICHA into o_matricula
        FROM IPJ.T_LEGAJOS L join IPJ.T_TIPOS_ENTIDADES te
            on l.id_tipo_entidad = te.id_tipo_entidad
        WHERE
          trim(L.DENOMINACION_SIA) = trim(p_razon_social) and
          id_ubicacion= IPJ.TYPES.C_AREA_CYF  AND
          NRO_FICHA is not null;
      exception
        when NO_DATA_FOUND then
          -- Busco el ultimo en Legajos
          SELECT to_char(MAX(TO_NUMBER(trim(translate(upper(l.NRO_FICHA), 'ABCDEFGHIJKLMN�OPQRSTUVWXYZ-', '                            '))))+1) into v_numero_Leg
          FROM IPJ.T_LEGAJOS L join IPJ.T_TIPOS_ENTIDADES te
              on l.id_tipo_entidad = te.id_tipo_entidad
          WHERE
            id_ubicacion= IPJ.TYPES.C_AREA_CYF  AND
            NRO_FICHA LIKE v_letra || '-%'  and
            trim(L.DENOMINACION_SIA) <> trim(p_razon_social);

          -- Busco en Entidades
          SELECT to_char(MAX(TO_NUMBER(trim(translate(upper(e.NRO_REGISTRO), 'ABCDEFGHIJKLMN�OPQRSTUVWXYZ-', '                            '))))+1) into v_numero_Ent
          FROM IPJ.T_ENTIDADES e join IPJ.T_TIPOS_ENTIDADES te
              on E.id_tipo_entidad = te.id_tipo_entidad
          WHERE
            id_ubicacion= IPJ.TYPES.C_AREA_CYF  AND
            NRO_REGISTRO LIKE v_letra || '-%'  and
            trim(E.DENOMINACION_1) <> trim(p_razon_social);

          -- Paso el maximo +1
          if nvl(v_numero_ent, 0) >= nvl(v_numero_leg, 0) then
            o_matricula := v_letra || '-' || to_char(nvl(v_numero_ent, 0));
          else
            o_matricula := v_letra || '-' || to_char(nvl(v_numero_leg, 0));
          end if;
      end;

      o_rdo := IPJ.TYPES.C_RESP_OK;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    exception
      when NO_DATA_FOUND then
        o_matricula := v_letra || '-1';
        o_rdo := IPJ.VARIOS.MENSAJE_ERROR('PROT_NOT');
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      when OTHERS then
        o_rdo := IPJ.VARIOS.MENSAJE_ERROR('PROT_NOT');
        o_matricula :=  IPJ.VARIOS.MENSAJE_ERROR('PROT_NOT');
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
    end;

  END SP_GENERAR_MATRICULA_FUNDACION;

  PROCEDURE SP_Traer_Entidad_Soc_Usuf(
    o_Cursor OUT TYPES.cursorType,
    p_id_entidad_socio in number)
  IS
  /*********************************************************
  Este procemiento devuelve el listado de usufructuarios relacionados a un socio
  **********************************************************/
  BEGIN

    OPEN o_Cursor FOR
      select esu.id_entidad_socio,  esu.id_integrante, esu.borrador,
        to_char(esu.fec_alta, 'dd/mm/rrrr') fec_alta,
        to_char(esu.fec_baja, 'dd/mm/rrrr') fec_baja,
        (p.NOMBRE || ' ' ||p.APELLIDO) detalle,
        I.ID_NUMERO, I.ID_SEXO, I.NRO_DOCUMENTO, I.PAI_COD_PAIS,
        I.ERROR_DATO, ESU.CUOTA,
      (SELECT decode(NVL(s.id_integrante,0),0,s.n_empresa,(SELECT upper(p1.nov_nombre || ' ' || p1.nov_apellido)
                                                             FROM rcivil.vt_personas_cuil p1
                                                            WHERE p1.id_sexo = i2.id_sexo
                                                            AND p1.nro_documento = i2.nro_documento
                                                            AND p1.pai_cod_pais = i2.pai_cod_pais
                                                            AND p1.id_numero = i2.id_numero))
          FROM ipj.t_entidades_socios s
        LEFT JOIN ipj.t_integrantes i2  ON s.id_integrante = i2.id_integrante
         WHERE s.id_entidad_socio = p_id_entidad_socio) socio
      from ipj.T_ENTIDADES_SOCIOS_USUF esu join ipj.t_integrantes i
          on esu.id_integrante = i.id_integrante
        left join rcivil.vt_pk_persona p
          on i.id_sexo = p.ID_SEXO and
            i.nro_documento = p.NRO_DOCUMENTO and
            i.pai_cod_pais = p.PAI_COD_PAIS and
            i.id_numero = p.ID_NUMERO
      where
       esu.id_entidad_socio = p_id_entidad_socio and
        (esu.fec_baja is null or esu.fec_baja >= sysdate or esu.borrador ='S');

  END SP_Traer_Entidad_Soc_Usuf;

  PROCEDURE SP_Traer_Entidad_Soc_Copro(
    o_Cursor OUT TYPES.cursorType,
    p_id_entidad_socio in number)
  IS
  /*********************************************************
  Este procemiento devuelve el listado de copropietarios relacionados a una cuota de socios
  **********************************************************/
  BEGIN

    OPEN o_Cursor FOR
      select esc.id_entidad_socio,  esc.id_integrante, esc.borrador,
        to_char(esc.fec_alta, 'dd/mm/rrrr') fec_alta,
        to_char(esc.fec_baja, 'dd/mm/rrrr') fec_baja,
        (p.NOMBRE || ' ' ||p.APELLIDO) detalle,
        I.ID_NUMERO, I.ID_SEXO, I.NRO_DOCUMENTO, I.PAI_COD_PAIS, I.ERROR_DATO
      from ipj.T_ENTIDADES_SOCIOS_COPRO esc join ipj.t_integrantes i
          on esc.id_integrante = i.id_integrante
        left join rcivil.vt_pk_persona p
          on i.id_sexo = p.ID_SEXO and
            i.nro_documento = p.NRO_DOCUMENTO and
            i.pai_cod_pais = p.PAI_COD_PAIS and
            i.id_numero = p.ID_NUMERO
      where
        esc.id_entidad_socio = p_id_entidad_socio and
        (esc.fec_baja is null or esc.fec_baja >= sysdate or esc.borrador ='S');

  END SP_Traer_Entidad_Soc_Copro;


  PROCEDURE SP_Guardar_Entidad_Soc_Usuf(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_entidad_socio in number,
    p_fec_alta in varchar2,
    p_fec_baja in varchar2,
    p_id_integrante in number,
    p_id_sexo in varchar2,
    p_nro_documento in varchar2,
    p_pai_cod_pais in varchar2,
    p_id_numero in number,
    p_cuil in varchar2,
    p_detalle in varchar2,
    p_nombre in varchar2,
    p_apellido in varchar2,
    p_error_dato in varchar2,
    p_n_tipo_documento in varchar2,
    p_eliminar in number,  -- 1 Elimina
    p_cuota in number)
  IS
    v_valid_parametros varchar2(200);
    v_id_integrante number;
    v_bloque varchar2(100);
    v_tipo_mensaje number;
    v_existe number;
  BEGIN
  /*****************************************************
    Guarda los integrantes usufructuarios relacionados a un socio
    Si existe los actualizo, sino los agrego.
    No maneja transaccion, ya que se usa en cascada desde la aplicacion y lo administra el codigo.
  ******************************************************/
    -- VALIDACION DE PARAMETROS
    v_bloque := 'GUARDAR USUFRUCTO - Parametros : ';

    if p_fec_alta is not null and IPJ.VARIOS.Valida_Fecha(p_fec_alta) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT');
    end if;
    if p_fec_baja is not null and IPJ.VARIOS.Valida_Fecha(p_fec_baja) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT');
    end if;
    if nvl(p_id_entidad_socio, 0) = 0 then
      v_valid_parametros := v_valid_parametros || 'Relaci�n a Socio inv�lido';
    end if;
    -- Si los parametros no estan OK, no continuo
    if v_valid_parametros is not null then
      o_rdo := v_bloque || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    -- CUERPO DEL PROCEDIMEINTO
    if p_eliminar = 1 then
      delete ipj.T_ENTIDADES_SOCIOS_USUF
      where
        id_entidad_socio = p_id_entidad_socio and
        id_integrante = p_id_integrante and
        borrador = 'S';

    else
       v_id_integrante := p_id_integrante;
       if IPJ.VARIOS.Valida_Integrante(p_id_integrante) = 0 then
         IPJ.ENTIDAD_PERSJUR.SP_GUARDAR_INTEGRANTES(
           o_rdo => v_valid_parametros,
           o_tipo_mensaje => v_tipo_mensaje,
           o_id_integrante => v_id_integrante,
           p_id_sexo => p_id_sexo,
           p_nro_documento => p_nro_documento,
           p_pai_cod_pais => p_pai_cod_pais,
           p_id_numero => p_id_numero,
           p_cuil => p_cuil,
           p_detalle => p_detalle,
           p_Nombre => p_Nombre,
           p_Apellido => p_Apellido,
           p_error_dato => p_error_dato,
           p_n_tipo_documento => p_n_tipo_documento );

         if v_valid_parametros <> TYPES.c_Resp_OK then
            o_rdo := IPJ.VARIOS.MENSAJE_ERROR('INT_NOT') || '. ' || v_valid_parametros;
            o_tipo_mensaje := v_tipo_mensaje;
            return;
         end if;
       else
         -- si esta marcado como dato erroneo, cambio la descripcion y lo marco.
         if p_error_dato = 'S' then
           update ipj.t_integrantes
           set
             error_dato = p_error_dato,
             detalle = p_detalle
           where
             id_integrante = p_id_integrante;
         end if;
       end if;

       v_bloque := 'Integrantes - Usufructo ';
       update ipj.T_ENTIDADES_SOCIOS_USUF
       set
         fec_baja = (case when p_fec_baja is not null then to_date(p_fec_baja, 'dd/mm/rrrr') else null end),
         cuota = p_cuota
       where
         id_entidad_socio = p_id_entidad_socio and
         id_integrante = v_id_integrante;

       v_existe := sql%rowcount ;

       -- si no existe, lo agrego
      if v_existe = 0 then
        insert into ipj.T_ENTIDADES_SOCIOS_USUF
          ( id_entidad_socio, id_integrante, fec_alta, fec_baja, borrador, cuota)
        values
          (p_id_entidad_socio, v_id_integrante,
           to_date(p_fec_alta, 'dd/mm/rrrr'),  to_date(p_fec_baja, 'dd/mm/rrrr'), 'S', p_cuota);
      end if;

    end if;

    o_rdo := TYPES.c_Resp_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
    WHEN OTHERS THEN
       o_rdo := v_bloque || To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Guardar_Entidad_Soc_Usuf;

  PROCEDURE SP_Guardar_Entidad_Soc_Copro(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_entidad_socio in number,
    p_fec_alta in varchar2,
    p_fec_baja in varchar2,
    p_id_integrante in number,
    p_id_sexo in varchar2,
    p_nro_documento in varchar2,
    p_pai_cod_pais in varchar2,
    p_id_numero in number,
    p_cuil in varchar2,
    p_detalle in varchar2,
    p_nombre in varchar2,
    p_apellido in varchar2,
    p_error_dato in varchar2,
    p_n_tipo_documento in varchar2,
    p_eliminar in number) -- 1 Elimina
  IS
    v_valid_parametros varchar2(200);
    v_id_integrante number;
    v_bloque varchar2(100);
    v_tipo_mensaje number;
    v_existe number;
  BEGIN
  /*****************************************************
    Guarda los integrantes copropietarios relacionados a un cuota de socios
    Si existe los actualizo, sino los agrego.
    No maneja transaccion, ya que se usa en cascada desde la aplicacion y lo administra el codigo.
  ******************************************************/
    -- VALIDACION DE PARAMETROS
    v_bloque := 'GUARDAR COPROPIETARIO - Parametros : ';

    if p_fec_alta is not null and IPJ.VARIOS.Valida_Fecha(p_fec_alta) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT');
    end if;
    if p_fec_baja is not null and IPJ.VARIOS.Valida_Fecha(p_fec_baja) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT');
    end if;
    if nvl(p_id_entidad_socio, 0) = 0 then
      v_valid_parametros := v_valid_parametros || 'Relaci�n a Socio inv�lido';
    end if;
    -- Si los parametros no estan OK, no continuo
    if v_valid_parametros is not null then
      o_rdo := v_bloque || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    -- CUERPO DEL PROCEDIMEINTO
    if p_eliminar = 1 then
      delete ipj.T_ENTIDADES_SOCIOS_COPRO
      where
        id_entidad_socio = p_id_entidad_socio and
        id_integrante = p_id_integrante and
        borrador = 'S';

    else
       v_id_integrante := p_id_integrante;
       if IPJ.VARIOS.Valida_Integrante(p_id_integrante) = 0 then
         IPJ.ENTIDAD_PERSJUR.SP_GUARDAR_INTEGRANTES(
           o_rdo => v_valid_parametros,
           o_tipo_mensaje => v_tipo_mensaje,
           o_id_integrante => v_id_integrante,
           p_id_sexo => p_id_sexo,
           p_nro_documento => p_nro_documento,
           p_pai_cod_pais => p_pai_cod_pais,
           p_id_numero => p_id_numero,
           p_cuil => p_cuil,
           p_detalle => p_detalle,
           p_Nombre => p_Nombre,
           p_Apellido => p_Apellido,
           p_error_dato => p_error_dato,
           p_n_tipo_documento => p_n_tipo_documento );

         if v_valid_parametros <> TYPES.c_Resp_OK then
            o_rdo := IPJ.VARIOS.MENSAJE_ERROR('INT_NOT') || '. ' || v_valid_parametros;
            o_tipo_mensaje := v_tipo_mensaje;
            return;
         end if;
       else
         -- si esta marcado como dato erroneo, cambio la descripcion y lo marco.
         if p_error_dato = 'S' then
           update ipj.t_integrantes
           set
             error_dato = p_error_dato,
             detalle = p_detalle
           where
             id_integrante = p_id_integrante;
         end if;
       end if;

       v_bloque := 'Integrantes - Copropietario ';
       if p_fec_baja is not null then
         update ipj.T_ENTIDADES_SOCIOS_COPRO
         set fec_baja = to_date(p_fec_baja, 'dd/mm/rrrr')
         where
           id_entidad_socio = p_id_entidad_socio and
           id_integrante = v_id_integrante;

           v_existe := sql%rowcount;
       else
         select count(*) into v_existe
         from T_ENTIDADES_SOCIOS_COPRO
         where
           id_entidad_socio = p_id_entidad_socio and
           id_integrante = v_id_integrante;
       end if;

       -- si no existe, lo agrego
      if v_existe = 0 then
        insert into ipj.T_ENTIDADES_SOCIOS_COPRO
          ( id_entidad_socio, id_integrante, fec_alta, fec_baja, borrador)
        values
          (p_id_entidad_socio, v_id_integrante,
           to_date(p_fec_alta, 'dd/mm/rrrr'),  to_date(p_fec_baja, 'dd/mm/rrrr'), 'S');

      end if;

    end if;

    o_rdo := TYPES.c_Resp_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
    WHEN OTHERS THEN
       o_rdo := v_bloque || To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Guardar_Entidad_Soc_Copro;


  PROCEDURE SP_Listar_Comis_Venc(
    o_Cursor OUT TYPES.cursorType,
    p_cuil_usuario in varchar2)
  IS
  /*********************************************************
  Lista las Cominiciones Normalizadoras abiertas con mas de 60 dias
  **********************************************************/
  BEGIN

    OPEN o_Cursor FOR
      select TACC.Cuil_Usuario,
        to_char(
          (case
            when tacc.id_tipo_accion = ipj.types.c_Tipo_Acc_Norm_ACyF then
              GREATEST(nvl((select GREATEST(nvl(d.fec_resol_cn, to_date('01/01/1900', 'dd/mm/rrrr')), nvl(d.fecha_cargos, to_date('01/01/1900', 'dd/mm/rrrr'))) from ipj.t_denuncias d where d.id_tramite_ipj = tr.id_tramite_ipj), tr.fecha_ini_suac), tr.fecha_ini_suac)
            else tr.fecha_ini_suac
          end), 'dd/mm/rrrr') Fecha,
        tr.Sticker, TR.Expediente,
        TACC.ID_LEGAJO Legajo, l.denominacion_sia RazonSocial, u.descripcion Usuario,
        (case
            when tr.fecha_ini_suac = (case when tacc.id_tipo_accion = ipj.types.c_Tipo_Acc_Norm_ACyF then GREATEST(nvl((select GREATEST(nvl(d.fec_resol_cn, to_date('01/01/1900', 'dd/mm/rrrr')), nvl(d.fecha_cargos, to_date('01/01/1900', 'dd/mm/rrrr'))) from ipj.t_denuncias d where d.id_tramite_ipj = tr.id_tramite_ipj), tr.fecha_ini_suac), tr.fecha_ini_suac)
                                                        else tr.fecha_ini_suac end) then 'Fecha Inicio Suac'
            when (select nvl(d.fec_resol_cn, to_date('01/01/1900', 'dd/mm/rrrr')) from ipj.t_denuncias d where d.id_tramite_ipj = tr.id_tramite_ipj) = (case when tacc.id_tipo_accion = ipj.types.c_Tipo_Acc_Norm_ACyF then GREATEST(nvl((select GREATEST(nvl(d.fec_resol_cn, to_date('01/01/1900', 'dd/mm/rrrr')), nvl(d.fecha_cargos, to_date('01/01/1900', 'dd/mm/rrrr'))) from ipj.t_denuncias d where d.id_tramite_ipj = tr.id_tramite_ipj), tr.fecha_ini_suac), tr.fecha_ini_suac)
                                                        else tr.fecha_ini_suac end) then 'Fecha Resoluci�m CN'
            when (select nvl(d.fecha_cargos, to_date('01/01/1900', 'dd/mm/rrrr')) from ipj.t_denuncias d where d.id_tramite_ipj = tr.id_tramite_ipj) = (case when tacc.id_tipo_accion = ipj.types.c_Tipo_Acc_Norm_ACyF then GREATEST(nvl((select GREATEST(nvl(d.fec_resol_cn, to_date('01/01/1900', 'dd/mm/rrrr')), nvl(d.fecha_cargos, to_date('01/01/1900', 'dd/mm/rrrr'))) from ipj.t_denuncias d where d.id_tramite_ipj = tr.id_tramite_ipj), tr.fecha_ini_suac), tr.fecha_ini_suac)
                                                        else tr.fecha_ini_suac end) then 'Fecha Acept. Cargos'
           else tr.observacion
         end ) Observacion,
        IPJ.VARIOS.FC_Dias_Laborables(
           to_date((case
              when tacc.id_tipo_accion = ipj.types.c_Tipo_Acc_Norm_ACyF then
                GREATEST(nvl((select GREATEST(nvl(d.fec_resol_cn, to_date('01/01/1900', 'dd/mm/rrrr')), nvl(d.fecha_cargos, to_date('01/01/1900', 'dd/mm/rrrr'))) from ipj.t_denuncias d where d.id_tramite_ipj = tr.id_tramite_ipj), tr.fecha_inicio), tr.fecha_inicio)
              else
                nvl(nvl( tacc.fecha_toma_acta, tacc.fec_veeduria), tr.fecha_ini_suac )
           end), 'dd/mm/rrrr')
           , to_date(sysdate, 'dd/mm/rrrr'),
          'N') Dias
      from ipj.t_tramitesipj tr join ipj.t_tramitesipj_acciones tacc
          on tr.Id_Tramite_Ipj = tacc.Id_Tramite_Ipj
        join ipj.t_legajos l
          on tacc.id_legajo = l.id_legajo
        join ipj.t_usuarios u
          on tacc.cuil_usuario = u.cuil_usuario
      where
        TR.ID_UBICACION_ORIGEN in (select distinct g.id_ubicacion
                                                     from ipj.t_grupos_trab_ubicacion g join ipj.t_tipos_clasif_ipj clas
                                                         on g.id_ubicacion = clas.id_ubicacion
                                                     where
                                                       cuil = p_cuil_usuario)  and
        tacc.id_tipo_accion in (ipj.types.c_Tipo_Acc_Norm_SxA, ipj.types.c_Tipo_Acc_Norm_ACyF) and
        tacc.id_estado < ipj.types.c_Estados_Completado and
        IPJ.VARIOS.FC_Dias_Laborables(
           to_date((case
              when tacc.id_tipo_accion = ipj.types.c_Tipo_Acc_Norm_ACyF then
                GREATEST(nvl((select GREATEST(nvl(d.fec_resol_cn, to_date('01/01/1900', 'dd/mm/rrrr')), nvl(d.fecha_cargos, to_date('01/01/1900', 'dd/mm/rrrr'))) from ipj.t_denuncias d where d.id_tramite_ipj = tr.id_tramite_ipj), tr.fecha_ini_suac), tr.fecha_ini_suac)
              else
                nvl(nvl( tacc.fecha_toma_acta, tacc.fec_veeduria), tr.fecha_ini_suac )
           end), 'dd/mm/rrrr')
         , to_date(sysdate, 'dd/mm/rrrr'), 'N') > 60
      order by dias desc;

  END SP_Listar_Comis_Venc;

  PROCEDURE SP_Listar_Interv_Venc(
    o_Cursor OUT TYPES.cursorType,
    p_cuil_usuario in varchar2)
  IS
  /*********************************************************
  Lista las Intervenciones abiertas con mas de 60 dias
  **********************************************************/
  BEGIN

    OPEN o_Cursor FOR
      select TACC.Cuil_Usuario,  to_char(tr.fecha_inicio, 'dd/mm/rrrr') Fecha, tr.Sticker, TR.Expediente,
        TACC.ID_LEGAJO Legajo, l.denominacion_sia RazonSocial, u.descripcion Usuario, tr.Observacion,
        (to_date(sysdate, 'dd/mm/rrrr') - to_date(tr.fecha_inicio, 'dd/mm/rrrr')) Dias
      from ipj.t_tramitesipj tr join ipj.t_tramitesipj_acciones tacc
          on tr.Id_Tramite_Ipj = tacc.Id_Tramite_Ipj
        join ipj.t_legajos l
          on tacc.id_legajo = l.id_legajo
        join ipj.t_usuarios u
          on tacc.cuil_usuario = u.cuil_usuario
      where
          TR.ID_UBICACION_ORIGEN in ( select distinct g.id_ubicacion
                                                     from ipj.t_grupos_trab_ubicacion g join ipj.t_tipos_clasif_ipj clas
                                                         on g.id_ubicacion = clas.id_ubicacion
                                                     where
                                                       cuil = p_cuil_usuario)  and
        tacc.id_tipo_accion in (IPJ.Types.c_Tipo_Acc_Interv_SxA, IPJ.Types.c_Tipo_Acc_Interv_ACyF) and
        tacc.id_estado < IPJ.Types.c_Estados_Completado
        and sysdate - tr.fecha_inicio > 60
      order by tr.fecha_inicio asc;

  END SP_Listar_Interv_Venc;

  PROCEDURE SP_Listar_Pers_Venc(
    o_Cursor OUT TYPES.cursorType,
    p_cuil_usuario in varchar2)
  IS
  /*********************************************************
  Lista las Personerias (Constituciones) abiertas con mas de 6 meses
  **********************************************************/
  BEGIN

    OPEN o_Cursor FOR
      select TACC.Cuil_Usuario,  to_char(tr.fecha_inicio, 'dd/mm/rrrr') Fecha, tr.Sticker, TR.Expediente,
        TACC.ID_LEGAJO Legajo, l.denominacion_sia RazonSocial, u.descripcion Usuario, tr.Observacion,
        (to_date(sysdate, 'dd/mm/rrrr') - to_date(tr.fecha_inicio, 'dd/mm/rrrr')) Dias
      from ipj.t_tramitesipj tr join ipj.t_tramitesipj_acciones tacc
          on tr.Id_Tramite_Ipj = tacc.Id_Tramite_Ipj
        join ipj.t_legajos l
          on tacc.id_legajo = l.id_legajo
        join ipj.t_usuarios u
          on tacc.cuil_usuario = u.cuil_usuario
      where
        TR.ID_UBICACION_ORIGEN in ( select distinct g.id_ubicacion
                                                     from ipj.t_grupos_trab_ubicacion g join ipj.t_tipos_clasif_ipj clas
                                                         on g.id_ubicacion = clas.id_ubicacion
                                                     where
                                                       cuil = p_cuil_usuario) and
        tacc.id_tipo_accion in (IPJ.Types.c_Tipo_Acc_Const_SRL, IPJ.Types.c_Tipo_Acc_Const_SxA, IPJ.Types.c_Tipo_Acc_Const_ACyF) and
        tacc.id_estado < IPJ.Types.c_Estados_Completado
        and sysdate - tr.fecha_inicio > 180
      order by tr.fecha_inicio asc;

  END SP_Listar_Pers_Venc;

  PROCEDURE SP_Listar_Entidad_Irregular(
    o_Cursor OUT TYPES.cursorType,
    p_cuil_usuario in varchar2)
  IS
  /*********************************************************
  Lista las Entidades que no han presendado nada en 1 a�o
  **********************************************************/
  BEGIN

    OPEN o_Cursor FOR
      select '' Cuil_Usuario,  to_char(tm.fec_acta, 'dd/mm/rrrr') Fecha, tr.Sticker, TR.Expediente,
        tm.Id_Legajo Legajo, l.denominacion_sia RazonSocial, '' Usuario, '' Observacion,
        (to_date(sysdate, 'dd/mm/rrrr') - to_date(tm.fec_acta, 'dd/mm/rrrr')) Dias
      from
        (select tr.Id_Tramite_Ipj, tacc.id_legajo, max(eac.fec_acta) fec_acta
         from ipj.t_tramitesipj tr join ipj.t_tramitesipj_acciones tacc
             on tr.Id_Tramite_Ipj = tacc.Id_Tramite_Ipj
           join IPJ.t_entidades_acta eac
             on tacc.Id_Tramite_Ipj = eac.Id_Tramite_Ipj and tacc.id_legajo = eac.id_legajo
         where
           Tr.Id_Ubicacion_Origen in ( select distinct g.id_ubicacion
                                                     from ipj.t_grupos_trab_ubicacion g join ipj.t_tipos_clasif_ipj clas
                                                         on g.id_ubicacion = clas.id_ubicacion
                                                     where
                                                       cuil = p_cuil_usuario) and
           tr.id_estado_ult < IPJ.Types.c_Estados_Rechazado and
           tacc.id_estado < IPJ.Types.c_Estados_Rechazado
         group by tr.Id_Tramite_Ipj, tacc.id_legajo
        ) tm --- calcula la ultima asamblea entidades
        join ipj.t_tramitesipj tr
          on tm.Id_Tramite_Ipj = tr.Id_Tramite_Ipj
        join ipj.t_legajos l
          on tm.id_legajo = l.id_legajo
      where
        sysdate - tm.fec_acta > 360
      order by tm.fec_acta asc;

  END SP_Listar_Entidad_Irregular;

  PROCEDURE SP_GUARDAR_ENTIDAD_SOCIOS_HIST(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number,
    p_id_tipo_integrante in number,
    p_cuota in number,
    p_fecha_inicio in varchar2,
    p_fecha_fin in varchar2,
    p_id_integrante in number,
    p_id_entidad_socio in number,
    p_id_legajo_socio in number,
    p_cuit_empresa in varchar2,
    p_n_empresa in varchar2,
    p_cuota_compartida in varchar2,
    p_porc_capital in varchar2)
  IS
    v_valid_parametros varchar2(200);
    v_bloque varchar2(100);
    v_tipo_mensaje number;
  BEGIN
  /*****************************************************
    Guarda los datos del socio, relacionado al tr�mite que se esta modificando.
    Sirve para armar un historial de las modificaciones.
  ******************************************************/
    -- VALIDACION DE PARAMETROS
    v_bloque := 'GUARDAR SOCIOS - Parametros : ';
    if IPJ.VARIOS.Valida_Entidad(nvl(p_Id_Tramite_Ipj, 0), p_id_legajo) = 0 then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('ENT_NOT')|| '(' || to_char(p_Id_Tramite_Ipj) ||' - ' || to_char(p_id_legajo)||')';
    end if;
    if IPJ.VARIOS.Valida_Tipo_Integrante(p_id_tipo_integrante) = 0 then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('TINT_NOT');
    end if;
    if p_fecha_inicio is not null and IPJ.VARIOS.Valida_Fecha(p_fecha_inicio) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT');
    end if;
    if p_fecha_fin is not null and IPJ.VARIOS.Valida_Fecha(p_fecha_fin) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT');
    end if;

    -- Si los parametros no estan OK, no continuo
    if v_valid_parametros is not null then
      o_rdo := v_bloque || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    update ipj.t_entidades_socios_hist e
    set
      cuota = p_cuota,
      porc_capital = IPJ.VARIOS.ToNumber(p_porc_capital),
      fecha_fin = to_date(p_fecha_fin, 'dd/mm/rrrr')
    where
      id_entidad_socio = p_id_entidad_socio and
      Id_Tramite_Ipj = p_Id_Tramite_Ipj and
      id_legajo = p_id_legajo;

     -- si no existe, lo agrego
    if sql%rowcount = 0 then
      insert into ipj.t_entidades_socios_hist
        ( id_tipo_integrante, cuota, Id_Tramite_Ipj, fecha_inicio, fecha_fin, id_integrante,
          id_legajo, id_entidad_socio, id_legajo_socio, cuit_empresa, n_empresa,
          cuota_compartida, porc_capital)
      values
        (p_id_tipo_integrante, p_cuota, p_Id_Tramite_Ipj,
         to_date(p_fecha_inicio, 'dd/mm/rrrr'),  to_date(p_fecha_fin, 'dd/mm/rrrr'), p_id_integrante, p_id_legajo,
         p_id_entidad_socio, p_id_legajo_socio, p_cuit_empresa, p_n_empresa, nvl(p_cuota_compartida, 'N'),
         IPJ.VARIOS.ToNumber(p_porc_capital));
    end if;

    o_rdo := TYPES.c_Resp_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
    WHEN OTHERS THEN
       o_rdo := v_bloque || To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_GUARDAR_ENTIDAD_SOCIOS_HIST;

  PROCEDURE SP_GUARDAR_ENTIDAD_ADMIN_HIST(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number,
    p_id_admin in number,
    p_id_tipo_integrante in number,
    p_fecha_inicio in varchar2,
    p_fecha_fin in varchar2,
    p_id_integrante in number,
    p_Id_Vin in number,
    p_Id_Tipo_Organismo in number,
    p_id_motivo_baja in number,
    p_cuit_empresa in varchar2,
    p_n_empresa in varchar2,
    p_id_legajo_empresa in varchar2)
  IS
    v_valid_parametros varchar2(200);
    v_bloque varchar2(100);
    v_tipo_mensaje number;
  BEGIN
  /*****************************************************
    Guarda los datos del administrador, relacionado al tr�mite que se esta modificando.
    Sirve para armar un historial de las modificaciones.
  ******************************************************/
    -- VALIDACION DE PARAMETROS
    v_bloque := 'GUARDAR ADMIN - Parametros : ';
    if IPJ.VARIOS.Valida_Entidad(nvl(p_Id_Tramite_Ipj, 0), p_id_legajo) = 0 then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('ENT_NOT')|| '(' || to_char(p_Id_Tramite_Ipj) ||' - ' || to_char(p_id_legajo)||')';
    end if;
    if IPJ.VARIOS.Valida_Tipo_Integrante(p_id_tipo_integrante) = 0 then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('TINT_NOT');
    end if;
    if p_fecha_inicio is not null and IPJ.VARIOS.Valida_Fecha(p_fecha_inicio) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT');
    end if;
    if p_fecha_fin is not null and IPJ.VARIOS.Valida_Fecha(p_fecha_fin) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT');
    end if;

    -- Si los parametros no estan OK, no continuo
    if v_valid_parametros is not null then
      o_rdo := v_bloque || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    update ipj.t_entidades_admin_hist e
    set
      fecha_fin = to_date(p_fecha_fin, 'dd/mm/rrrr'),
      id_vin = decode(p_id_vin,0,NULL,p_id_vin),
      id_motivo_baja = p_id_motivo_baja,
      cuit_empresa = p_cuit_empresa,
      n_empresa = p_n_empresa,
      fecha_inicio = to_date(p_fecha_inicio, 'dd/mm/rrrr')
    where
      Id_Tramite_Ipj= p_Id_Tramite_Ipj and
      id_legajo = p_id_legajo and
      id_admin = p_id_admin;

    -- si no existe, lo agrego
    if sql%rowcount = 0 then

      insert into ipj.t_entidades_admin_hist
        ( id_tipo_integrante, Id_Tramite_Ipj, fecha_inicio, fecha_fin, id_integrante, id_legajo, borrador,
         Id_Vin, Id_Tipo_Organismo, id_motivo_baja, id_admin, cuit_empresa, n_empresa,
         id_legajo_empresa)
      values
        (p_id_tipo_integrante, p_Id_Tramite_Ipj, to_date(p_fecha_inicio, 'dd/mm/rrrr'),
        to_date(p_fecha_fin, 'dd/mm/rrrr'), p_id_integrante, p_id_legajo, 'S',
        decode(p_id_vin,0,NULL,p_id_vin), p_Id_Tipo_Organismo, p_id_motivo_baja, p_id_admin, p_cuit_empresa,
        p_n_empresa, p_id_legajo_empresa);
    end if;

    o_rdo := TYPES.c_Resp_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
    WHEN OTHERS THEN
       o_rdo := v_bloque || To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_GUARDAR_ENTIDAD_ADMIN_HIST;

  PROCEDURE SP_GUARDAR_ENTIDAD_SIND_HIST(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_entidad_sindico in number,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number,
    p_fec_alta in varchar2,
    p_fec_baja in varchar2,
    p_matricula in varchar2,
    p_id_tipo_integrante in number,
    p_borrador in varchar2,
    p_id_integrante in number,
    p_id_legajo_sindico in number,
    p_matricula_empresa in varchar2,
    p_id_tipo_entidad in number,
    p_cuit_empresa in varchar2,
    p_n_empresa in varchar2,
    p_id_entidades_accion in number,
    p_id_motivo_baja in number)
  IS
    v_valid_parametros varchar2(200);
    v_bloque varchar2(100);
    v_tipo_mensaje number;
  BEGIN
  /*****************************************************
    Guarda los datos del sindico, relacionado al tr�mite que se esta modificando.
    Sirve para armar un historial de las modificaciones.
  ******************************************************/
    -- VALIDACION DE PARAMETROS
    v_bloque := 'GUARDAR ADMIN - Parametros : ';
    if IPJ.VARIOS.Valida_Entidad(nvl(p_Id_Tramite_Ipj, 0), p_id_legajo) = 0 then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('ENT_NOT')|| '(' || to_char(p_Id_Tramite_Ipj) ||' - ' || to_char(p_id_legajo)||')';
    end if;
    if IPJ.VARIOS.Valida_Tipo_Integrante(p_id_tipo_integrante) = 0 then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('TINT_NOT');
    end if;
    if p_fec_alta is not null and IPJ.VARIOS.Valida_Fecha(p_fec_alta) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT');
    end if;
    if p_fec_baja is not null and IPJ.VARIOS.Valida_Fecha(p_fec_baja) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT');
    end if;

    -- Si los parametros no estan OK, no continuo
    if v_valid_parametros is not null then
      o_rdo := v_bloque || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    update ipj.t_entidades_sindico_hist e
    set
      id_integrante = p_id_integrante,
      fecha_alta = to_date(p_fec_alta, 'dd/mm/rrrr'),
      matricula = p_matricula,
      id_tipo_integrante = p_id_tipo_integrante,
      borrador = p_borrador,
      fecha_baja = to_date(p_fec_baja, 'dd/mm/rrrr'),
      matricula_empresa = p_matricula_empresa,
      id_tipo_entidad = p_id_tipo_entidad,
      id_legajo_sindico = p_id_legajo_sindico,
      cuit_empresa = p_cuit_empresa,
      n_empresa = p_n_empresa,
      id_entidades_accion = p_id_entidades_Accion,
      id_motivo_baja = p_id_motivo_baja
    where
      Id_Tramite_Ipj= p_Id_Tramite_Ipj and
      id_legajo = p_id_legajo and
      id_entidad_sindico = p_id_entidad_sindico;

    -- si no existe, lo agrego
    if sql%rowcount = 0 then

      Insert Into Ipj.T_Entidades_Sindico_Hist (
        Id_Tramite_Ipj, Id_Legajo, Id_Entidad_Sindico, Id_Integrante, Fecha_Alta, Matricula,
        Id_Tipo_Integrante, Borrador, Fecha_Baja, Matricula_Empresa, Id_Tipo_Entidad,
        Id_Legajo_Sindico, Cuit_Empresa, N_Empresa, Id_Entidades_Accion, id_motivo_baja)
      Values (
        p_Id_Tramite_Ipj, p_Id_Legajo, p_Id_Entidad_Sindico, p_Id_Integrante, to_date(p_Fec_Alta, 'dd/mm/rrrr'),
        p_Matricula, p_Id_Tipo_Integrante, p_Borrador, to_date(p_Fec_Baja, 'dd/mm/rrrr'), p_Matricula_Empresa,
        p_Id_Tipo_Entidad, p_Id_Legajo_Sindico, p_Cuit_Empresa, p_N_Empresa, p_Id_Entidades_Accion,
        p_id_motivo_baja);
    end if;

    o_rdo := TYPES.c_Resp_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
    WHEN OTHERS THEN
       o_rdo := v_bloque || To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_GUARDAR_ENTIDAD_SIND_HIST;

  PROCEDURE SP_Buscar_Entidad_Tramite(
      o_Id_Tramite_Ipj out number,
      o_id_tramiteipj_accion out number,
      p_id_legajo in number )
  IS
   /*****************************************************************
    Este procedimiento devuelve el utlimo tr�mite y Accion de una entidad, aunque este en borrador.
  ******************************************************************/
  BEGIN

     IPJ.ENTIDAD_PERSJUR.SP_Buscar_Entidad_Tramite(
      o_Id_Tramite_Ipj => o_Id_Tramite_Ipj,
      o_id_tramiteipj_accion => o_id_tramiteipj_accion,
      p_id_legajo => p_id_legajo,
      p_Abiertos => 'S');

  END SP_Buscar_Entidad_Tramite;

  PROCEDURE SP_Buscar_Entidad_Tramite(
      o_Id_Tramite_Ipj out number,
      o_id_tramiteipj_accion out number,
      p_id_legajo in number,
      p_Abiertos in char )
  IS
 /*****************************************************************
  Este procedimiento devuelve el utlimo tr�mite y Accion de una entidad, aunque este en borrador.
******************************************************************/
    v_id_ubicacion_leg number;
  BEGIN
    -- Busco la ultima area de la entidad, para filtrar las busquedas hist�ricas
    select nvl(id_ubicacion, 0) into v_id_ubicacion_leg
    from ipj.t_legajos l left join ipj.t_tipos_entidades te
      on l.id_tipo_entidad = te.id_tipo_entidad
    where
      l.id_legajo = p_id_legajo;

    -- Ordeno tr�mites para RP y SxA
    if v_id_ubicacion_leg in (ipj.types.c_area_SxA, ipj.types.c_area_SRL) then
      select Id_Tramite_Ipj, id_tramiteipj_accion into o_Id_Tramite_Ipj, o_id_tramiteipj_accion
      from
        ( select e.Id_Tramite_Ipj, nvl(tacc.id_tramiteipj_accion, 0) id_tramiteipj_accion
          from ipj.t_entidades e join ipj.t_tramitesipj tr
              on e.Id_Tramite_Ipj = tr.Id_Tramite_Ipj
            left join IPJ.t_TramitesIPJ_Acciones tacc
              on tacc.id_legajo = e.id_legajo and tacc.Id_Tramite_Ipj = e.Id_Tramite_Ipj
          where
            e.id_legajo = p_id_legajo and
            (nvl(tr.id_estado_ult, 0) < 200  or nvl(tr.id_estado_ult, 0) = 210) and -- Tramite no rechazado
            e.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_id_ubicacion_leg) and -- para soporta cambios de SA a SRL
            nvl(tacc.id_estado, 0) < IPJ.TYPES.C_ESTADOS_RECHAZADO and
            (upper(p_abiertos) = 'S' or nvl(tacc.id_estado, 0) > IPJ.TYPES.C_ESTADOS_COMPLETADO) and
            (upper(p_abiertos) = 'S' or nvl(tr.id_estado_ult, 0) between 100 and 199 ) and -- Tramite entre completo y cerrado
            tacc.id_tipo_accion not in (IPJ.Types.c_Tipo_Acc_Retiro,IPJ.Types.c_Tipo_Acc_Archivar_Expediente) and
            tacc.id_tipo_accion not in (select id_tipo_accion from ipj.t_informes_tramite where id_informe <> 18) -- Informe Caratula
          order by matricula asc , matricula_version desc, nvl(anio, 0) desc, folio desc, Id_Tramite_Ipj desc
        )
      where rownum = 1;
    end if;

    -- Ordeno tr�mites para ACyF
    if v_id_ubicacion_leg in (ipj.types.c_area_CyF) then
      select Id_Tramite_Ipj, id_tramiteipj_accion into o_Id_Tramite_Ipj, o_id_tramiteipj_accion
      from
        ( select e.Id_Tramite_Ipj, nvl(tacc.id_tramiteipj_accion, 0) id_tramiteipj_accion,
             IPJ.ENTIDAD_PERSJUR.FC_BUSCAR_PRIM_ACTA(tr.id_tramite_ipj) Fecha_Acta
          from ipj.t_entidades e join ipj.t_tramitesipj tr
              on e.Id_Tramite_Ipj = tr.Id_Tramite_Ipj
            left join IPJ.t_TramitesIPJ_Acciones tacc
              on tacc.id_legajo = e.id_legajo and tacc.Id_Tramite_Ipj = e.Id_Tramite_Ipj
          where
            e.id_legajo = p_id_legajo and
            (nvl(tr.id_estado_ult, 0) < 200  or nvl(tr.id_estado_ult, 0) = 210) and -- Tramite no rechazado
            e.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_id_ubicacion_leg) and -- para soporta cambios de SA a SRL
            nvl(tacc.id_estado, 0) < IPJ.TYPES.C_ESTADOS_RECHAZADO and
            (upper(p_abiertos) = 'S' or nvl(tacc.id_estado, 0) > IPJ.TYPES.C_ESTADOS_COMPLETADO) and
            (upper(p_abiertos) = 'S' or nvl(tr.id_estado_ult, 0) between 100 and 199 ) and -- Tramite entre completo y cerrado
            tacc.id_tipo_accion not in (IPJ.Types.c_Tipo_Acc_Retiro,IPJ.Types.c_Tipo_Acc_Archivar_Expediente) and
            tacc.id_tipo_accion not in (select id_tipo_accion from ipj.t_informes_tramite where id_informe <> 18) -- Informe Caratula
          order by Fecha_Acta desc, tacc.Id_Tramite_Ipj desc
        )
      where rownum = 1;
    end if;

  EXCEPTION
    when NO_DATA_FOUND then
      o_Id_Tramite_Ipj := 0;
      o_id_tramiteipj_accion := 0;

  END SP_Buscar_Entidad_Tramite;

  PROCEDURE SP_Buscar_Entidad_Tramite_PROV(
      o_Id_Tramite_Ipj out number,
      o_id_tramiteipj_accion out number,
      p_id_legajo in number,
      p_Abiertos in char )
  IS
 /*****************************************************************
  Este procedimiento es PROVISORIO para limitar el uso de una funcionalidad a los datos
  controlados y cargados por los Data Entry.
  Es igual que el SP_BUSCAR_ENTIDAD_TRAMITE
******************************************************************/
    v_existe number;
  BEGIN
    -- Controlo si el legajo esta en el listado de ACyF cargados por los Data Entry
    select count(1) into v_existe
    from ipj.t_entidades e
    where
      Id_Tramite_Ipj in
        ( select Id_Tramite_Ipj
          from ipj.t_tramitesipj tr
          where
            id_ubicacion_origen = 5 and
            ( cuil_creador in ('27330441173','24361484882','27373157282','27221622893','20321372709',
                '27187513753', '27361457574','20404003969','20371968661','27358943204','20346325985',
                '20336999260', '23345608184','27361241105','23364301529','27333206159','20339750522',
                '23359635214', '20324585932','20401102745','27358945029','23346325429','20346327279'
                )
              or
              ( tr.fecha_inicio > to_date('01/10/2016', 'dd/mm/rrrr') and
                exists (select * from ipj.t_tramitesipj_acciones where id_tramite_ipj = tr.id_tramite_ipj and id_tipo_accion in (37, 38, 48))
              )
            )
           and id_estado_ult >= Ipj.Types.c_Estados_Completado
           and id_estado_ult < Ipj.Types.c_Estados_Rechazado )
       and id_legajo = p_id_legajo;

    if v_existe > 0 then
      select Id_Tramite_Ipj, id_tramiteipj_accion into o_Id_Tramite_Ipj, o_id_tramiteipj_accion
      from
        ( select e.Id_Tramite_Ipj, nvl(tacc.id_tramiteipj_accion, 0) id_tramiteipj_accion
          from ipj.t_entidades e join ipj.t_tramitesipj tr
              on e.Id_Tramite_Ipj = tr.Id_Tramite_Ipj
            left join IPJ.t_TramitesIPJ_Acciones tacc
              on tacc.id_legajo = e.id_legajo and tacc.Id_Tramite_Ipj = e.Id_Tramite_Ipj
          where
            e.id_legajo = p_id_legajo and
            nvl(tacc.id_estado, 0) < IPJ.TYPES.C_ESTADOS_RECHAZADO and
            (upper(p_abiertos) = 'S' or nvl(tacc.id_estado, 0) > IPJ.TYPES.C_ESTADOS_COMPLETADO)
          order by nvl(tr.id_proceso, 0) asc, e.matricula_version desc, tacc.Id_Tramite_Ipj desc, tacc.id_tramiteipj_accion desc
        )
      where rownum = 1;
    else
      o_Id_Tramite_Ipj := 0;
      o_id_tramiteipj_accion := 0;
    end if;

  EXCEPTION
    when NO_DATA_FOUND then
      o_Id_Tramite_Ipj := 0;
      o_id_tramiteipj_accion := 0;

  END SP_Buscar_Entidad_Tramite_PROV;


  PROCEDURE SP_Buscar_Ent_Tramites(
    o_Cursor OUT TYPES.cursorType,
    p_id_legajo in number)
  IS
  /* Lista los tramites para una entidad determinada segun parametros de entrada*/
  BEGIN
    OPEN o_Cursor FOR
       select Id_Tramite_Suac, Sticker, n_estado, fecha_inicio, Observacion, id_proceso
       from
          (select to_number(vtIPJ.ID_TRAMITE) Id_Tramite_Suac,
             nvl(vtIPJ.Sticker, ipj.tramites_suac.fc_buscar_stiker_publico(vtIPJ.ID_TRAMITE)) Sticker,
             vtIPJ.n_estado, ipj.tramites_suac.FC_Buscar_Fec_Ini_SUAC(vtIPJ.ID_TRAMITE) fecha_inicio,
             ipj.tramites_suac.fc_buscar_asunto_suac(vtIPJ.ID_TRAMITE) Observacion,
             vtIPJ.id_proceso, vtIPJ.Fecha_Asignacion
            from IPJ.VT_TRAMITE_ADMIN_IPJ vtIPJ join IPJ.t_tramitesipj_persjur tpj
                on vtIPJ.Id_Tramite_Ipj = tpj.Id_Tramite_Ipj
            where
                tpj.id_legajo = p_id_legajo

            UNION
            select 0 Id_Tramite_Suac, SUBSTR(tl.sticker, 1, 9) || SUBSTR(tl.sticker, 12, 14) as Sticker,
              '' n_estado, null fecha_inicio, '' Observacion, 0 id_proceso,
              null Fecha_Asignacion
            from IPJ.t_Tramites_Legajo tl
            where tl.id_legajo = p_id_legajo
          )
       order by Fecha_Asignacion asc;

  END SP_Buscar_Ent_Tramites;


  PROCEDURE SP_Buscar_Ent_Tramites(
    o_Cursor OUT TYPES.cursorType,
    p_id_legajo in number,
    p_id_estados in varchar2) -- T = Todos, P = Pendientes
  IS
  /* Lista los tramites para una entidad determinada segun parametros de entrada*/
  BEGIN

    OPEN o_Cursor FOR
      select
        vtIPJ.id_tramite Id_Tramite_Suac, nvl(vtIPJ.Sticker, ipj.tramites_suac.fc_buscar_stiker_publico(vtIPJ.id_tramite)) Sticker,
        (case vtIPJ.id_proceso
            when 4 then ''
            when 5 then vtIPJ.Observacion
            else IPJ.TRAMITES_SUAC.FC_BUSCAR_ASUNTO_SUAC(vtIPJ.id_tramite)
        end) Asunto,
        nvl(vtIPJ.Expediente, ipj.tramites_suac.fc_buscar_expediente(vtIPJ.id_tramite)) Nro_Expediente,
        vtIPJ.Id_Tramite_Ipj, vtIPJ.id_clasif_ipj, vtIPJ.Observacion, vtIPJ.Id_Estado,
        vtIPJ.cuil_usuario, vtIPJ.n_estado, vtIPJ.N_Clasif_Ipj, vtIPJ.Fecha_Asignacion,
        vtIPJ.Acc_Abiertas, vtIPJ.Acc_Cerradas, vtIPJ.Acc_Observadas,
        vtIPJ.URGENTE, vtIPJ.usuario, vtIPJ.n_ubicacion,
        vtIPJ.Cuil_Creador, vtIPJ.id_ubicacion, vtIPJ.id_grupo, vtIPJ.id_proceso,
        vtIPJ.cuil_usuario cuil_usuario_old
      from
        IPJ.VT_TRAMITE_ADMIN_IPJ vtIPJ join IPJ.t_tramitesipj_persjur tpj
          on vtIPJ.Id_Tramite_Ipj = tpj.Id_Tramite_Ipj
      where
        tpj.id_legajo = p_id_legajo and
        (upper(p_id_estados) = 'T' or vtIPJ.Id_Estado in (1, 2, 3, 4,5))
      order by vtIPJ.Fecha_Asignacion asc;

  END SP_Buscar_Ent_Tramites;


  PROCEDURE SP_Buscar_Ent_Tramites_Pend(
    o_Cursor OUT TYPES.cursorType,
    p_id_legajo in number)
  IS
  /* Lista los tramites pendientes para una entidad determinada */
  BEGIN

    OPEN o_Cursor FOR
      select
        vtIPJ.id_tramite Id_Tramite_Suac,
        nvl(vtIPJ.Sticker, ipj.tramites_suac.fc_buscar_stiker_publico(vtIPJ.id_tramite)) Sticker,
        (case vtIPJ.id_proceso
            when 4 then 'GENERACION DE HISTORICOS'
            when 5 then 'CARGA DE HISTORICOS'
            else IPJ.tramites_suac.FC_Buscar_SubTipo_SUAC(vtIPJ.id_tramite)
        end) Tipo,
        (case vtIPJ.id_proceso
            when 4 then ''
            when 5 then vtIPJ.Observacion
            else ipj.tramites_suac.fc_buscar_asunto_suac(vtIPJ.id_tramite)
        end) Asunto,
        nvl(vtIPJ.Expediente, ipj.tramites_suac.fc_buscar_expediente(vtIPJ.id_tramite)) Nro_Expediente,
        vtIPJ.Id_Tramite_Ipj, vtIPJ.id_clasif_ipj, vtIPJ.Observacion, vtIPJ.Id_Estado,
        vtIPJ.cuil_usuario, vtIPJ.n_estado, vtIPJ.N_Clasif_Ipj, vtIPJ.Fecha_Asignacion,
        vtIPJ.Acc_Abiertas, vtIPJ.Acc_Cerradas, vtIPJ.Acc_Observadas,
        vtIPJ.URGENTE, vtIPJ.usuario, vtIPJ.n_ubicacion,
        vtIPJ.Cuil_Creador, vtIPJ.id_ubicacion, vtIPJ.id_grupo, vtIPJ.id_proceso,
        vtIPJ.cuil_usuario cuil_usuario_old
      from
        IPJ.VT_TRAMITE_ADMIN_IPJ vtIPJ join IPJ.t_tramitesipj_persjur tpj
          on vtIPJ.Id_Tramite_Ipj = tpj.Id_Tramite_Ipj
      where
        tpj.id_legajo = p_id_legajo and
        vtIPJ.Id_Estado < IPJ.TYPES.c_Estados_Completado
      order by vtIPJ.Fecha_Asignacion asc;

  END SP_Buscar_Ent_Tramites_Pend;

  PROCEDURE Sp_Guardar_Entidad_Organos(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number,
    p_id_tipo_organismo in number,
    p_cant_meses in varchar2,
    p_id_tipo_duracion in number,
    p_min_titular in number,
    p_max_titular in number,
    p_suplente_igual in char,
    p_suplente_menor in char,
    p_suplente_mayor in char,
    p_Max_Suplente in number,
    p_Min_Suplente in number,
    p_observacion in varchar2,
    p_duracion_indefinida in char,
    p_id_tipo_administracion in number,
    p_periodos_reeleccion IN number
    )
  IS
    v_existe number;
    v_tipo_mensaje number;
    v_id_integrante number;
    v_valid_parametros varchar2(500);
  BEGIN
    /*************************************************************
      Guarda o actualiza la cantidad de mese de plazo de los organismos de una Fundacion
      Cant_meses antes guadaba ese dato, ahora pueden ser Meses, A�os o Ejercicios
  *************************************************************/
  -- VALIDACION DE PARAMETROS
    if nvl(p_min_titular, 0) > nvl(p_max_titular, 0) then
      v_valid_parametros := v_valid_parametros || 'La cantidad m�nima de Titulares no puede ser mayor que la m�xima.';
    end if;
    if nvl(p_min_suplente, 0) > nvl(p_max_suplente, 0) then
      v_valid_parametros := v_valid_parametros || 'La cantidad m�nima de Suplentes no puede ser mayor que la m�xima.';
    end if;

    if v_valid_parametros is not null then
      o_rdo := 'GUARDAR ORGANISMO - Parametros: '  || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    -- ************** CUERPO PROCEDIMIENTO **********************
    -- Si existe un registro lo actualizo, sino lo inserto
    update ipj.t_entidades_organismo
    set
       cant_meses = p_cant_meses,
       id_tipo_duracion = p_id_tipo_duracion,
       min_titular = p_min_titular,
       max_titular = p_max_titular,
       suplente_menor = p_suplente_menor,
       suplente_mayor = p_suplente_mayor,
       suplente_igual = p_suplente_igual,
       Max_Suplente = p_max_suplente,
       Min_Suplente = p_min_suplente,
       observacion = p_observacion,
       duracion_indefinida = p_duracion_indefinida,
       id_tipo_administracion = (case when p_id_tipo_administracion = 0 then null else p_id_tipo_administracion end),
       periodos_reeleccion = p_periodos_reeleccion
    where
      id_legajo = p_id_legajo and
      --Id_Tramite_Ipj = p_Id_Tramite_Ipj and
      Id_Tipo_Organismo = p_Id_Tipo_Organismo;

    if sql%rowcount = 0 then
      insert into ipj.t_entidades_organismo
        (Id_Tramite_Ipj, id_legajo, Id_Tipo_Organismo, cant_meses, id_tipo_duracion,
        min_titular, max_titular, suplente_menor, suplente_mayor, suplente_igual,
        Max_Suplente, Min_Suplente, observacion, duracion_indefinida, id_tipo_administracion,
        periodos_reeleccion)
      values
        (p_Id_Tramite_Ipj, p_id_legajo, p_Id_Tipo_Organismo, p_cant_meses,
        p_id_tipo_duracion, p_min_titular, p_max_titular, p_suplente_menor, p_suplente_mayor,
        p_suplente_igual, p_Max_Suplente, p_Min_Suplente, p_observacion,
        p_duracion_indefinida, (case when p_id_tipo_administracion = 0 then null else p_id_tipo_administracion end),
        p_periodos_reeleccion);
    end if;

    o_rdo := 'OK';
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     WHEN OTHERS THEN
       o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END Sp_Guardar_Entidad_Organos;

  PROCEDURE SP_Traer_Entidad_Acciones(
    o_Cursor OUT types.cursorType,
    p_id_legajo in number,
    p_Id_Tramite_Ipj in number)
  IS
  /**********************************************************
    Lista todas las acciones definidas para una entidad de un tr�mite
  **********************************************************/
  v_matricula_version number(6);
  v_ubicacion_ent number(6);
  v_ubicacion_leg number(6);
  BEGIN
    -- busco la ultima area y su version, para visualizar los datos correctos
    begin
      select nvl(matricula_version, 0),
        (select id_ubicacion from ipj.t_tipos_entidades te where te.id_tipo_entidad = e.id_tipo_entidad) ubicacion_ent,
        (select id_ubicacion from ipj.t_tipos_entidades te where te.id_tipo_entidad = l.id_tipo_entidad) ubicacion_leg
        into v_matricula_version, v_ubicacion_ent, v_ubicacion_leg
      from IPJ.t_entidades e join ipj.t_legajos l
        on e.id_legajo = l.id_legajo
      where
        e.Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        e.id_legajo = p_id_legajo;
    exception
      when NO_DATA_FOUND then
        v_matricula_version := 0;
    end;

    OPEN o_Cursor FOR
      SELECT Ea.Id_Legajo, Ea.Id_Tramite_Ipj, Ea.Id_Entidades_Accion, Ea.Id_Accion, Ta.N_Accion,
        Ea.Id_Forma_Accion, Tfa.N_Forma_Accion, Ea.Cant_Votos, Ea.Clase clase, Ea.Borrador,
        Ea.Sin_Clase, to_char(ea.fecha_baja, 'dd/mm/rrrr') Fecha_Baja
        from IPJ.T_ENTIDADES_ACCIONES ea join IPJ.T_TIPOS_ACCIONES ta
          on EA.ID_ACCION = TA.ID_ACCION
        join IPJ.T_TIPOS_FORMAS_ACCION tfa
          on EA.ID_FORMA_ACCION = TFA.ID_FORMA_ACCION
        join ipj.t_entidades tpj
          on tpj.id_tramite_ipj = ea.id_tramite_ipj and tpj.id_legajo = ea.id_legajo
        JOIN ipj.t_tramitesipj tra
          ON tra.id_tramite_ipj = tpj.id_tramite_ipj
      where
        EA.ID_LEGAJO = p_id_legajo and
        tpj.matricula_version <= v_matricula_version and
        tpj.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_ubicacion_leg) and
        (ea.borrador = 'N' or ea.Id_Tramite_Ipj = p_Id_Tramite_Ipj) and
        (ea.fecha_baja is null or ea.fecha_baja >= to_date(sysdate, 'dd/mm/rrrr'))
    UNION ALL
      select Ea.Id_Legajo, Ea.Id_Tramite_Ipj, Ea.Id_Entidades_Accion, Ea.Id_Accion, Ta.N_Accion,
            Ea.Id_Forma_Accion, Tfa.N_Forma_Accion, Ea.Cant_Votos, Ea.Clase clase, Ea.Borrador,
            Ea.Sin_Clase, to_char(ea.fecha_baja, 'dd/mm/rrrr') Fecha_Baja
        from IPJ.T_ENTIDADES_ACCIONES ea join IPJ.T_TIPOS_ACCIONES ta
          on EA.ID_ACCION = TA.ID_ACCION
        join IPJ.T_TIPOS_FORMAS_ACCION tfa
          on EA.ID_FORMA_ACCION = TFA.ID_FORMA_ACCION
        join ipj.t_entidades tpj
          on tpj.id_tramite_ipj = ea.id_tramite_ipj and tpj.id_legajo = ea.id_legajo
        JOIN ipj.t_tramitesipj tra
          ON tra.id_tramite_ipj = tpj.id_tramite_ipj
       where
         EA.ID_LEGAJO = (SELECT max(e.id_legajo) FROM ipj.t_entidades e WHERE e.id_tramite_ipj = tra.id_tramite_ipj) AND --legajo del hijo
         tpj.matricula_version <= v_matricula_version and
         tpj.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_ubicacion_leg) and
         tra.Id_Tramite_Ipj_padre = p_Id_Tramite_Ipj and
         (ea.fecha_baja is null or ea.fecha_baja >= to_date(sysdate, 'dd/mm/rrrr'))
      order by clase;

  END SP_Traer_Entidad_Acciones;

  PROCEDURE SP_Guardar_Entidad_Acciones(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_entidades_accion out number,
    p_id_legajo in number,
    p_Id_Tramite_Ipj in number,
    p_id_entidades_accion in number,
    p_Clase in varchar2,
    p_Cant_Votos in number,
    p_Id_Accion in number,
    p_Id_Forma_Accion in number,
    p_eliminar in number,
    p_borrador in varchar2, -- 1 elimina
    p_sin_clase in varchar2,
    p_fecha_baja in varchar2 -- Es Fecha
  )
  IS
   /**********************************************************
    Guarda, actualiza o elimina las acciones definidas para una entidad
  **********************************************************/
    v_hay_socios number(5);
    v_hay_admin number(5);
    v_hay_sindicos number(5);
    v_Hay_SinClase number(5);
    v_valid_parametros varchar2(500);
  BEGIN
    if p_fecha_baja is not null and IPJ.VARIOS.Valida_Fecha(p_fecha_baja) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT');
    end if;

    if v_valid_parametros is not null then
      o_rdo := 'GUARDAR ACCIONES - Par�metros: '  || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    /********************************************************
      CUERPO DEL PROCEDIMIENTO
    ********************************************************/
    if p_eliminar = 1 then
      -- Controlo que no este utilizada por socios, administradores o s�ndicos
      select count(1) into v_hay_socios
      from ipj.t_entidades_socios_acciones
      where
        id_legajo = p_id_legajo and
        id_entidades_accion = p_id_entidades_accion;

      select count(1) into v_hay_admin
      from ipj.t_entidades_admin
      where
        id_legajo = p_id_legajo and
        id_entidades_accion = p_id_entidades_accion;

      select count(1) into v_hay_sindicos
      from ipj.t_entidades_sindico
      where
        id_legajo = p_id_legajo and
        id_entidades_accion = p_id_entidades_accion;

      if nvl(v_hay_socios, 0) + nvl(v_hay_admin, 0) + nvl(v_hay_sindicos, 0) = 0 then
        delete IPJ.T_ENTIDADES_ACCIONES
        where
          id_entidades_accion = p_id_entidades_accion and
          Id_Tramite_Ipj = p_Id_Tramite_Ipj and
          id_legajo = p_id_legajo and
          borrador = 'S';
      else
          o_rdo := 'Las acciones de Clase ' || p_Clase || ' est�n utilizada por Socios y/o Administradores. No puede eliminarse.';
          o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
          o_id_entidades_accion := 0;
          return;
      end if;

      o_id_entidades_accion := 0;
    else
      -- si viene p_Id_Ol_Entidad_Accion se inserta, sino se actualia
      if p_id_entidades_accion <= 0 then
        -- si hay sin clases, no permito agregar nuevas
        select count(1) into v_Hay_SinClase
        from ipj.t_entidades_acciones ac join ipj.t_tramitesipj tr
            on ac.id_tramite_ipj = tr.id_tramite_ipj
        WHERE
          id_legajo = p_id_legajo and
          sin_clase = 'S' and
          nvl(fecha_baja, sysdate) >= to_date(sysdate, 'dd/mm/rrrr') and
          tr.id_estado_ult < 200;


        if v_Hay_SinClase = 0 then
          -- busco el maximo ID y le sumo uno
          SELECT nvl(max(id_entidades_accion), 0) +1 INTO o_id_entidades_accion
          FROM IPJ.T_ENTIDADES_ACCIONES
          WHERE
            id_legajo = p_id_legajo ;

          INSERT INTO IPJ.T_ENTIDADES_ACCIONES (id_legajo, Id_Tramite_Ipj, Id_Entidades_Accion,
            Clase, Cant_Votos, Id_Accion, Id_Forma_Accion, borrador, sin_clase, fecha_baja)
          VALUES ( p_id_legajo, p_Id_Tramite_Ipj, o_id_entidades_accion, p_Clase, p_Cant_Votos,
            p_Id_Accion, p_Id_Forma_Accion, p_borrador, p_sin_clase,
            to_date(p_fecha_baja, 'dd/mm/rrrr'));
        else
          o_rdo := 'Esta definidco como Sin Clase, no pueden agregarse nuevas Clases.';
          o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
          o_id_entidades_accion := 0;
          return;
        end if;
      else
        UPDATE IPJ.T_ENTIDADES_ACCIONES
        SET
          Clase = p_Clase,
          Cant_Votos = p_Cant_Votos,
          Id_Accion = p_Id_Accion,
          Id_Forma_Accion = p_Id_Forma_Accion,
          sin_clase = p_sin_clase,
          fecha_baja = to_date(p_fecha_baja, 'dd/mm/rrrr')
        WHERE
          id_entidades_accion = p_id_entidades_accion and
          Id_Tramite_Ipj = p_Id_Tramite_Ipj and
          id_legajo = p_id_legajo;

        o_id_entidades_accion := p_id_entidades_accion;
      end if;

      -- Actualiza la cantidad de acciones de los socios
      if p_fecha_baja is not null then
        update ipj.t_entidades_socios
        set  cuota =
          ( select nvl(sum(cantidad), 0)
            from IPJ.T_Entidades_socios_acciones
            where
              id_legajo = p_id_legajo and
              Id_entidad_socio = ipj.t_entidades_socios.Id_entidad_socio and
              id_entidades_accion in
                (select id_entidades_accion from ipj.t_entidades_acciones where id_legajo = p_id_legajo and nvl(fecha_baja, sysdate) >= to_date(sysdate, 'dd/mm/rrrr'))
          )
        where
          id_legajo = p_id_legajo and
          nvl(fecha_fin, sysdate) >= to_date(sysdate, 'dd/mm/rrrr');
      end if;
    end if;

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     WHEN OTHERS THEN
       o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;

  END SP_Guardar_Entidad_Acciones;

  PROCEDURE SP_Traer_Entidad_Integracion(
    o_Cursor OUT types.cursorType,
    p_id_legajo in number,
    p_Id_Tramite_Ipj in number,
    p_Id_Entidad_Socio in number)
  IS
   /**********************************************************
    Lista la Integraci�n de Capital de la sociedad
  **********************************************************/
  BEGIN

    OPEN o_Cursor FOR
      select ESC.ID_LEGAJO, ESC.Id_Tramite_Ipj, ESC.ID_ENTIDAD_SOCIO_CAPITAL,
          ESC.DOMINIO, trim(To_Char(nvl(ESC.MONTO, '0'), '99999999999999999990.99'))  MONTO,
          ESC.ID_ENTIDAD_SOCIO, ESC.ID_CAPITAL, TC.N_CAPITAL, ESC.BORRADOR,
          ESC.Id_Tramite_Ipj_Baja, esc.obs, esc.id_moneda, M.N_MONEDA,
          (case
             when es.Cuota_Compartida = 'S' and nvl(es.ID_INTEGRANTE, 0) = 0 then 'Compartida: ' || ipj.entidad_persjur.FC_LISTAR_SOCIOS_COPROP(ES.ID_ENTIDAD_SOCIO, ES.ID_TRAMITE_IPJ)
             when es.Cuota_Compartida <> 'S' and nvl(es.ID_INTEGRANTE, 0) = 0 then es.N_Empresa
             else P.NOV_NOMBRE ||' ' || P.NOV_APELLIDO
           end) Socio
        from IPJ.T_ENTIDADES_SOCIO_CAPITAL esc left join IPJ.T_ENTIDADES_SOCIOS es
          on ESC.ID_ENTIDAD_SOCIO = ES.ID_ENTIDAD_SOCIO
        join ipj.t_tipos_capital tc
          on esc.id_capital = tc.id_capital
   left join T_COMUNES.T_MONEDAS m
          on esc.id_moneda = m.id_moneda
   left join IPJ.T_INTEGRANTES i
          on ES.ID_INTEGRANTE = I.ID_INTEGRANTE
   left join rcivil.vt_pk_persona p
          on i.id_sexo = p.ID_SEXO AND i.nro_documento = p.NRO_DOCUMENTO AND i.pai_cod_pais = p.PAI_COD_PAIS AND i.id_numero = p.ID_NUMERO
       WHERE ESC.ID_LEGAJO = p_id_legajo and
             (p_Id_Entidad_Socio = 0 or esc.ID_ENTIDAD_SOCIO = p_Id_Entidad_Socio) and
             esc.Id_Tramite_Ipj_Baja is NULL
   UNION ALL
      select ESC.ID_LEGAJO, ESC.Id_Tramite_Ipj, ESC.ID_ENTIDAD_SOCIO_CAPITAL,
          ESC.DOMINIO, trim(To_Char(nvl(ESC.MONTO, '0'), '99999999999999999990.99'))  MONTO,
          ESC.ID_ENTIDAD_SOCIO, ESC.ID_CAPITAL, TC.N_CAPITAL, ESC.BORRADOR,
          ESC.Id_Tramite_Ipj_Baja, esc.obs, esc.id_moneda, M.N_MONEDA,
          (case
           when es.Cuota_Compartida = 'S' and nvl(es.ID_INTEGRANTE, 0) = 0 then 'Compartida: ' || ipj.entidad_persjur.FC_LISTAR_SOCIOS_COPROP(ES.ID_ENTIDAD_SOCIO, ES.ID_TRAMITE_IPJ)
           when es.Cuota_Compartida <> 'S' and nvl(es.ID_INTEGRANTE, 0) = 0 then es.N_Empresa
           else P.NOV_NOMBRE ||' ' || P.NOV_APELLIDO
           end) Socio
        from IPJ.T_ENTIDADES_SOCIO_CAPITAL esc left join IPJ.T_ENTIDADES_SOCIOS es
          on ESC.ID_ENTIDAD_SOCIO = ES.ID_ENTIDAD_SOCIO
        join ipj.t_tipos_capital tc
          on esc.id_capital = tc.id_capital
   left join T_COMUNES.T_MONEDAS m
          on esc.id_moneda = m.id_moneda
   left join IPJ.T_INTEGRANTES i
          on ES.ID_INTEGRANTE = I.ID_INTEGRANTE
   left join rcivil.vt_pk_persona p
          on i.id_sexo = p.ID_SEXO AND i.nro_documento = p.NRO_DOCUMENTO AND i.pai_cod_pais = p.PAI_COD_PAIS AND i.id_numero = p.ID_NUMERO
        JOIN ipj.t_tramitesipj tra
          ON esc.id_tramite_ipj = tra.id_tramite_ipj
       WHERE ESC.ID_LEGAJO = (SELECT max(e.id_legajo) FROM ipj.t_entidades e WHERE e.id_tramite_ipj = tra.id_tramite_ipj) and
             esc.Id_Tramite_Ipj_Baja is NULL AND
             (p_Id_Entidad_Socio = 0 or esc.ID_ENTIDAD_SOCIO = p_Id_Entidad_Socio) and
             tra.id_tramite_ipj_padre = p_Id_Tramite_Ipj;

  END SP_Traer_Entidad_Integracion;

  PROCEDURE SP_Guardar_Entidad_Integ(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_entidad_socio_capital out number,
    p_id_legajo in number,
    p_Id_Tramite_Ipj in number,
    p_id_entidad_socio_capital in number,
    p_id_entidad_socio in number,
    p_id_capital in number,
    p_dominio in varchar2,
    p_monto in varchar2,
    p_borrador in varchar2,
    p_eliminar in number, -- 1 elimina
    p_Id_Tramite_Ipj_entidad in number,
    p_obs in varchar2,
    p_id_moneda in varchar2)
  IS
   /**********************************************************
    Guarda o elimina la integraci�n de capital que aporta cada socio
  **********************************************************/
    v_valid_parametros varchar2(1000);
    v_id_entidad_socio number;
  BEGIN
    if IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('HAB_LOG_GESTION') = '1' then
      IPJ.VARIOS.SP_GUARDAR_LOG(
        p_METODO => 'SP_Guardar_Entidad_Integ',
        p_NIVEL => 'Gestion - Entidad',
        p_ORIGEN => 'DB',
        p_USR_LOG  => 'DB',
        p_USR_WINDOWS => 'Sistema',
        p_IP_PC  => null,
        p_EXCEPCION =>
          'Id Legajo = ' || to_char(p_id_legajo)
          || ' / Id Tramite Ipj = ' || to_char(p_Id_Tramite_Ipj)
          || ' / Id Entidad Socio Capital = ' || to_char(p_id_entidad_socio_capital)
          || ' / Id Entidad Socio = ' || to_char(p_id_entidad_socio)
          || ' / Id Capital = ' || to_char(p_id_capital)
          || ' / Dominio = ' || p_dominio
          || ' / Monto = ' || to_char(p_monto)
          || ' / Borrador = ' || p_borrador
          || ' / Eliminar = ' || to_char(p_eliminar)
          || ' / Id Tramite Baja = ' || to_char(p_Id_Tramite_Ipj_entidad)
          || ' / Obs = ' || p_obs
          || ' / Id Moneda = ' || p_id_moneda
       );
    END IF;

    -- Validacion de par�metros
    if IPJ.VARIOS.Valida_Entidad(nvl(p_Id_Tramite_Ipj, 0), p_id_legajo) = 0 then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('ENT_NOT')|| '(' || to_char(p_Id_Tramite_Ipj) ||' - ' || to_char(p_id_legajo)||')';
    end if;
    if p_eliminar <> 1 and p_monto is not null and nvl(IPJ.VARIOS.ToNumber(p_monto), -1) < 0 then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('MONTO_NOT');
    end if;

    -- Si los parametros no estan OK, no continuo
    if v_valid_parametros is not null then
      o_rdo := v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    --******************************
    -- CUERPO DEL PROCEDIMIENTO
    --******************************
    if p_eliminar = 1 then
      delete IPJ.T_ENTIDADES_SOCIO_CAPITAL
      where
        Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        id_legajo = p_id_legajo and
        id_entidad_socio_capital = p_id_entidad_socio_capital and
        borrador = 'S';

      update IPJ.T_ENTIDADES_SOCIO_CAPITAL
      set Id_Tramite_Ipj_Baja = p_Id_Tramite_Ipj_entidad
      where
        Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        id_legajo = p_id_legajo and
        id_entidad_socio_capital = p_id_entidad_socio_capital and
        borrador = 'N';

        o_id_entidad_socio_capital := 0;
    else
      -- se intenta actualizar la cantidad
      if nvl(p_id_entidad_socio_capital, 0) = 0 then
        -- Cambio los 0 por NULL para respetar la FK
        v_id_entidad_socio := (case when p_id_entidad_socio = 0 then null else p_id_entidad_socio end);

        SELECT IPJ.SEQ_ENTIDAD_SOC_CAP.nextval INTO o_Id_Entidad_Socio_Capital FROM dual;

        INSERT INTO IPJ.T_ENTIDADES_SOCIO_CAPITAL (id_entidad_socio_capital, id_entidad_socio, id_capital,
          Id_Tramite_Ipj, id_legajo, dominio, borrador, monto, obs, id_moneda)
        VALUES (o_Id_Entidad_Socio_Capital, v_id_entidad_socio, p_id_capital,
          p_Id_Tramite_Ipj, p_id_legajo, p_dominio, p_borrador, IPJ.VARIOS.ToNumber(p_monto),
          p_obs, p_id_moneda);
      else
        UPDATE IPJ.T_ENTIDADES_SOCIO_CAPITAL
        SET
          ID_CAPITAL = p_id_capital,
          MONTO = IPJ.VARIOS.ToNumber(p_monto),
          DOMINIO = p_dominio,
          obs = p_obs,
          id_moneda = p_id_moneda
        WHERE
          Id_Tramite_Ipj = p_Id_Tramite_Ipj and
          id_legajo = p_id_legajo and
          Id_Entidad_Socio_Capital = p_Id_Entidad_Socio_Capital;

        o_Id_Entidad_Socio_Capital := p_Id_Entidad_Socio_Capital;
      end if;

    end if;

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     WHEN OTHERS THEN
       o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Guardar_Entidad_Integ;

  PROCEDURE SP_Traer_Entidad_Soc_Acc(
    o_Cursor OUT types.cursorType,
    p_id_legajo in number,
    p_Id_Tramite_Ipj in number,
    p_Id_Entidad_Socio in number)
  IS
   /**********************************************************
    Lista las acciones de un socio
  **********************************************************/
  BEGIN
    OPEN o_Cursor FOR
      select Esa.Id_Legajo, Esa.Id_Tramite_Ipj, Esa.Id_Entidad_Socio, Esa.cantidad,
        Esa.Borrador, Esa.Id_Entidades_Accion, EA.CLASE,
         (case
           when nvl(es.ID_INTEGRANTE, 0) = 0 then es.N_Empresa
           else P.NOV_NOMBRE || ' ' || P.NOV_APELLIDO
         end) Socio, to_char(ea.fecha_baja, 'dd/mm/rrrr') Fecha_Baja
      from IPJ.T_ENTIDADES_SOCIOS_ACCIONES esa join IPJ.T_ENTIDADES_SOCIOS es
          on Esa.Id_Entidad_Socio = Es.Id_Entidad_Socio
        join ipj.t_entidades_acciones ea
          on ea.id_legajo = esa.id_legajo and Ea.Id_Entidades_Accion = Esa.Id_Entidades_Accion
        left join IPJ.T_INTEGRANTES i
          on ES.ID_INTEGRANTE = I.ID_INTEGRANTE
        left join rcivil.vt_pk_persona p
          on i.id_sexo = p.ID_SEXO and
            i.nro_documento = p.NRO_DOCUMENTO and
            i.pai_cod_pais = p.PAI_COD_PAIS and
            i.id_numero = p.ID_NUMERO
      where
        Esa.Id_Legajo = p_id_legajo and
        esa.ID_ENTIDAD_SOCIO = p_Id_Entidad_Socio;

  END SP_Traer_Entidad_Soc_Acc;


  PROCEDURE SP_Guardar_Entidad_Soc_Acc(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_legajo in number,
    p_Id_Tramite_Ipj in number,
    p_id_entidad_socio in number,
    p_id_entidades_accion in number,
    p_cantidad in number,
    p_borrador in varchar2,
    p_eliminar in number) -- 1 elimina
  IS
   /**********************************************************
    Guarda, Modifica o elimina las acciones de un socio
  **********************************************************/
  v_valid_parametros varchar2(1000);
  v_id_ubicacion number;
  BEGIN
    -- Validacion de par�metros
    if IPJ.VARIOS.Valida_Entidad(nvl(p_Id_Tramite_Ipj, 0), p_id_legajo) = 0 then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('ENT_NOT')|| '(' || to_char(p_Id_Tramite_Ipj) ||' - ' || to_char(p_id_legajo)||')';
    end if;

    -- Si los parametros no estan OK, no continuo
    if v_valid_parametros is not null then
      o_rdo := v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    --******************************
    -- CUERPO DEL PROCEDIMIENTO
    --******************************
    if p_eliminar = 1 then
      delete IPJ.T_ENTIDADES_SOCIOS_ACCIONES
      where
        id_legajo = p_id_legajo and
        id_entidad_socio = p_id_entidad_socio and
        id_entidades_accion = p_id_entidades_accion;

    else
      update IPJ.T_ENTIDADES_SOCIOS_ACCIONES
      set cantidad = p_cantidad
      where
        id_legajo = p_id_legajo and
        id_entidad_socio = p_id_entidad_socio and
        id_entidades_accion = p_id_entidades_accion;

      -- se intenta actualizar la cantidad
      if sql%rowcount = 0 then
        INSERT INTO IPJ.T_ENTIDADES_SOCIOS_ACCIONES (Id_Tramite_Ipj,
          Id_Legajo, Id_Entidad_Socio, Id_Entidades_Accion, Cantidad, Borrador)
        VALUES (p_Id_Tramite_Ipj, p_Id_Legajo, p_Id_Entidad_Socio, p_Id_Entidades_Accion,
          p_Cantidad, 'S');
      end if;

      -- Para SxA, se calcula la cantidad de acciones, sin contar las dadas de baja.
      select id_ubicacion_origen into v_id_ubicacion
      from ipj.t_tramitesipj
      where
        id_tramite_ipj = p_id_tramite_ipj;

      if v_id_ubicacion = IPJ.TYPES.C_AREA_SXA then
        update ipj.t_entidades_socios
        set  cuota =
          ( select nvl(sum(cantidad), 0)
            from IPJ.T_Entidades_socios_acciones
            where
              id_legajo = p_id_legajo and
              Id_entidad_socio = ipj.t_entidades_socios.Id_entidad_socio and
              id_entidades_accion in
                (select id_entidades_accion from ipj.t_entidades_acciones where id_legajo = p_id_legajo and nvl(fecha_baja, sysdate) >= to_date(sysdate, 'dd/mm/rrrr'))
          )
        where
          id_legajo = p_id_legajo and
          nvl(fecha_fin, sysdate) >= to_date(sysdate, 'dd/mm/rrrr');
      end if;

    end if;

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     WHEN OTHERS THEN
       o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Guardar_Entidad_Soc_Acc;


  PROCEDURE Sp_Traer_Datos_Empresa_IPJ(
    o_Cursor out IPJ.TYPES.CURSORTYPE,
    p_id_legajo in number
    )
  IS
  /****************************************************
       Dado un LEGAJO busca los ultimos datos registrados para la misma:
         - Matricula, Tipo Entidad, Fecha Acta, Domicilio, Cuit y Razon Social
  *****************************************************/
    v_Id_Tramite_Ipj number(10);
    v_id_tramiteipj_accion number(10);
  BEGIN
    -- busco el ultimo tr�mite par mostrar los valores
    IPJ.ENTIDAD_PERSJUR.SP_Buscar_Entidad_Tramite(
      o_Id_Tramite_Ipj => v_Id_Tramite_Ipj,
      o_id_tramiteipj_accion => v_id_tramiteipj_accion,
      p_id_legajo => p_id_legajo );

    if v_Id_Tramite_Ipj = 0 and v_Id_Tramite_Ipj = 0 then
      OPEN o_Cursor FOR
        select
          l.id_legajo, l.cuit Cuit_Empresa, l.denominacion_sia, l.denominacion_sia N_Empresa,
          l.nro_ficha Matricula, l.id_tipo_entidad, TE.TIPO_ENTIDAD,
          null Fec_Acta, null  En_Formacion, l.folio, l.anio,
          null id_tipodom, null n_tipodom, null id_tipocalle, null n_tipocalle, null id_calle,
          null n_calle, null altura, null depto, null piso, null torre, null id_barrio,
          null  n_barrio, null id_localidad, null n_localidad,
          null id_departamento, null n_departamento,
          null id_provincia, null n_provincia, null cpa,
          IPJ.VARIOS.FC_Buscar_Telefono(L.cuit || '00', IPJ.Types.c_id_Aplicacion) Telefono,
          IPJ.VARIOS.FC_Buscar_Mail(L.cuit || '00', IPJ.Types.c_id_Aplicacion) Email,
          (select max(id_vin_real) from ipj.t_entidades where id_legajo = p_id_legajo) id_vin_empresa
        from IPJ.t_legajos L left join IPJ.t_tipos_entidades te
          on l.id_tipo_entidad = te.id_tipo_entidad
        where
          l.id_legajo = p_id_legajo;
    else
      OPEN o_Cursor FOR
        select
          e.id_legajo, E.Cuit Cuit_Empresa, E.Denominacion_1 Denominacion_Sia, E.Denominacion_1 N_Empresa, E.Matricula ,
          E.Id_Tipo_Entidad, E.Acta_Contitutiva Fec_Acta,
          'N' En_Formacion,
          (case when te.id_ubicacion = IPJ.TYPES.C_AREA_CYF then null else e.folio end),
          e.anio,
          IPJ.VARIOS.FC_ARMAR_CALLE_DOM(d.n_calle, d.altura, d.piso, d.depto, d.torre, d.mzna, d.lote, d.n_barrio, d.km, d.n_tipocalle) calle_inf,
          d.id_tipodom, d.n_tipodom, d.id_tipocalle, d.n_tipocalle, d.id_calle,
          initcap(d.n_calle) n_calle, d.altura, d.depto, d.piso, d.torre, d.id_barrio,
          initcap(d.n_barrio) n_barrio, d.id_localidad, initcap(d.n_localidad) n_localidad,
          D.mzna Manzana, D.Lote, d.km,
          d.id_departamento, initcap(d.n_departamento) n_departamento,
          d.id_provincia, initcap(d.n_provincia) n_provincia, d.cpa,
          IPJ.VARIOS.FC_Buscar_Telefono(e.cuit || '00', IPJ.Types.c_id_Aplicacion) Telefono,
          IPJ.VARIOS.FC_Buscar_Mail(e.cuit || '00', IPJ.Types.c_id_Aplicacion) Email,
          E.ID_VIN_REAL id_vin
        from IPJ.t_entidades e left join IPJ.t_tipos_entidades te
            on e.id_tipo_entidad = te.id_tipo_entidad
          left join Dom_Manager.Vt_Domicilios_Cond d
            on d.id_vin = E.Id_Vin_Real
        where
          e.Id_Tramite_Ipj = v_Id_Tramite_Ipj and
          e.id_legajo = p_id_legajo;
    end if;
  END Sp_Traer_Datos_Empresa_IPJ;


  PROCEDURE SP_Traer_Entidad_Autorizados(
    o_Cursor OUT types.cursorType,
    p_Id_Tramite_Ipj in number)
  IS
   /**********************************************************
    Lista las personas autorizadas para un tramite
  **********************************************************/
  BEGIN
    OPEN o_Cursor FOR
      select Ea.Id_Tramite_Ipj, ea.id_legajo, Ea.Id_Integrante,
        (select p.nombre||' '||p.apellido from rcivil.vt_pk_persona p where p.id_sexo = i.id_sexo and p.nro_documento = i.nro_documento and p.pai_cod_pais = i.pai_cod_pais and p.id_numero = i.id_numero) detalle,
        I.Id_Sexo, I.Id_Numero, I.Nro_Documento, I.Pai_Cod_Pais, vt.cuil,
        nvl(ea.mail_autorizado, 'N') mail_autorizado,
        IPJ.VARIOS.FC_Buscar_Mail(I.Id_Sexo || I.Nro_Documento || I.Pai_Cod_Pais || to_char(I.Id_Numero), IPJ.Types.c_id_Aplicacion) Mail,
        IPJ.VARIOS.FC_Buscar_Telefono(I.Id_Sexo || I.Nro_Documento || I.Pai_Cod_Pais || to_char(I.Id_Numero), IPJ.Types.c_id_Aplicacion) Telefono
      from IPJ.T_ENTIDADES_AUTORIZADOS ea join ipj.t_integrantes i
          on EA.ID_INTEGRANTE = i.id_integrante
        join rcivil.vt_pk_persona vt
          on i.id_sexo = vt.id_sexo and i.nro_documento = vt.nro_documento and i.pai_cod_pais = vt.pai_cod_pais and i.id_numero = vt.id_numero
      where
        EA.Id_Tramite_Ipj = p_Id_Tramite_Ipj;

  END SP_Traer_Entidad_Autorizados;

  PROCEDURE SP_Guardar_Entidad_Autorizados(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number,
    p_id_integrante in number,
    p_id_sexo in varchar2,
    p_nro_documento in varchar2,
    p_pai_cod_pais in varchar2,
    p_id_numero in number,
    p_nombre in varchar2,
    p_eliminar in number -- 1 Elimina
    )
  IS
    v_valid_parametros varchar2(200);
    v_id_integrante number;
    v_bloque varchar2(100);
    v_existe number;
    v_tipo_mensaje number;
    v_id_legajo number;
  BEGIN
  /*****************************************************
    Agrega, modifica o elimina los autorizados para el tr�mite.
    Guarda los datos asociados en RCivil.
    No maneja transaccion, ya que se usa en cascada desde la aplicacion y lo administra el codigo.
  ******************************************************/

    -- CUERPO DEL PROCEDIMEINTO
    -- Al eliminar, se quitan los detalles relacionados al socio
    if p_eliminar = 1 then
      delete ipj.t_entidades_autorizados
      where
        Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        id_integrante = p_id_integrante;

      update ipj.t_entidades_autorizados
      set Informar_Suac = 'S'
      where
        Id_Tramite_Ipj = p_Id_Tramite_Ipj;

    else
      -- Si el integrante no existe, lo agrego y luego continuo
      v_id_integrante := p_id_integrante;
      if IPJ.VARIOS.Valida_Integrante(p_id_integrante) = 0 then
        IPJ.ENTIDAD_PERSJUR.SP_GUARDAR_INTEGRANTES(
          o_rdo => v_valid_parametros,
          o_tipo_mensaje => v_tipo_mensaje,
          o_id_integrante => v_id_integrante,
          p_id_sexo => p_id_sexo,
          p_nro_documento => p_nro_documento,
          p_pai_cod_pais => p_pai_cod_pais,
          p_id_numero => p_id_numero,
          p_cuil => null,
          p_detalle => p_Nombre,
          p_Nombre => null,
          p_Apellido => null,
          p_error_dato => 'N',
          p_n_tipo_documento => null );

        if v_valid_parametros <> TYPES.c_Resp_OK then
           o_rdo := IPJ.VARIOS.MENSAJE_ERROR('INT_NOT') || '. ' || v_valid_parametros;
           o_tipo_mensaje := v_tipo_mensaje;
           return;
        end if;
      end if;

      -- Si el Integrante vienen en 0, pongo NULL para respetar las FK
      v_id_integrante := (case when v_id_integrante = 0 then null else v_id_integrante end);
      v_id_legajo := (case when p_id_legajo = 0 then null else p_id_legajo end);

      -- verifico si ya existe y no hago nada
      select count(*) into v_existe
      from ipj.t_entidades_autorizados
      where
        Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        id_integrante = v_id_integrante;

       -- si no existe, lo agrego
      if v_existe = 0 then
        INSERT INTO IPJ.T_ENTIDADES_AUTORIZADOS (Id_Tramite_Ipj, Id_Legajo,
          Id_Integrante, Informar_Suac, mail_autorizado)
        VALUES (p_Id_Tramite_Ipj, v_id_legajo, v_id_integrante, 'S', 'N');

      end if;
    end if;

    o_rdo := TYPES.c_Resp_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
    WHEN OTHERS THEN
       o_rdo := v_bloque || To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Guardar_Entidad_Autorizados;

  PROCEDURE SP_Traer_Entidad_ClasifIPJ(
    o_Cursor OUT types.cursorType,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number)
  IS
   /**********************************************************
    Lista las Clasificaciones IPJ asociadas a una entidad entidad
  **********************************************************/
  BEGIN
    OPEN o_Cursor FOR
      select er.Id_Tramite_Ipj, er.Id_Legajo, er.Id_Tipo_Rubro_Ipj id_rubro_ipj,
        TR.N_TIPO_RUBRO_IPJ n_rubro_ipj
        from IPJ.T_ENTIDADES_TRUBRO_IPJ er join IPJ.T_TIPOS_RUBROS_IPJ tr
          on Er.Id_Tipo_Rubro_Ipj = Tr.Id_Tipo_Rubro_Ipj
       WHERE er.id_legajo = p_id_legajo and
             (ER.ID_TRAMITE_IPJ = p_id_tramite_ipj or er.fecha_baja is null)
   UNION ALL
      select er.Id_Tramite_Ipj, er.Id_Legajo, er.Id_Tipo_Rubro_Ipj id_rubro_ipj,
        TR.N_TIPO_RUBRO_IPJ n_rubro_ipj
        from IPJ.T_ENTIDADES_TRUBRO_IPJ er
        join IPJ.T_TIPOS_RUBROS_IPJ tr
          on Er.Id_Tipo_Rubro_Ipj = Tr.Id_Tipo_Rubro_Ipj
        JOIN ipj.t_tramitesipj tra
          ON er.id_tramite_ipj = tra.id_tramite_ipj
       WHERE
         tra.id_tramite_ipj_padre = p_id_tramite_ipj  and
         er.id_legajo = (SELECT max(e.id_legajo) FROM ipj.t_entidades e WHERE e.id_tramite_ipj = tra.id_tramite_ipj) and
         (tra.id_tramite_ipj_padre = p_id_tramite_ipj or er.fecha_baja is null);

  END SP_Traer_Entidad_ClasifIPJ;

  PROCEDURE SP_Guardar_Entidad_ClasifIPJ(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number,
    p_id_rubro_ipj in number,
    p_eliminar in number) -- 1 Elimina
  IS
   /**********************************************************
    Guardar o elimina una  Clasificaciones IPJ asociadas a una entidad
  **********************************************************/
  v_existe number;
  BEGIN
    -- Elimina el registro indicado
    if nvl(p_eliminar, 0) = 1 then
      update IPJ.T_ENTIDADES_TRUBRO_IPJ
      set fecha_baja = to_date(sysdate, 'dd/mm/rrrr')
      where
        Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        id_legajo = p_id_legajo and
        id_tipo_rubro_ipj = p_id_rubro_ipj;
    else
      --si no existe lo agrego, sino no hago nada
      select count(*) into v_existe
      from IPJ.T_ENTIDADES_TRUBRO_IPJ
      where
        id_legajo = p_id_legajo and
        id_tipo_rubro_ipj = p_id_rubro_ipj and
        (id_tramite_ipj = p_id_tramite_ipj or fecha_baja is null); -- (BUG 8855)

      if v_existe = 0 then
        insert into IPJ.T_ENTIDADES_TRUBRO_IPJ (Id_Tramite_Ipj, Id_Legajo, Id_Tipo_Rubro_Ipj, fecha_alta, fecha_baja)
        values (p_Id_Tramite_Ipj, p_Id_Legajo, p_Id_Rubro_Ipj, to_date(sysdate, 'dd/mm/rrrr'), null);
      end if;
    end if;

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     WHEN OTHERS THEN
       o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;

  END SP_Guardar_Entidad_ClasifIPJ;

  PROCEDURE SP_Traer_PersJur_Fiduciario(
    o_Cursor OUT TYPES.cursorType,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number)
  IS
  /*********************************************************
  Este procemiento devuelve el listado de fiduciarios asociados a una entidad de un tramite
  **********************************************************/
    v_matricula_version number(6,0);
    v_ubicacion_ent number(6);
    v_ubicacion_leg number(6);
  BEGIN

    -- busto la ultima area y su version, para visualizar los datos correctos
    begin
      select nvl(matricula_version, 0),
        (select id_ubicacion from ipj.t_tipos_entidades te where te.id_tipo_entidad = e.id_tipo_entidad) ubicacion_ent,
        (select id_ubicacion from ipj.t_tipos_entidades te where te.id_tipo_entidad = l.id_tipo_entidad) ubicacion_leg
        into v_matricula_version, v_ubicacion_ent, v_ubicacion_leg
      from IPJ.t_entidades e join ipj.t_legajos l
        on e.id_legajo = l.id_legajo
      where
        e.Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        e.id_legajo = p_id_legajo;
    exception
      when NO_DATA_FOUND then
        v_matricula_version := 0;
    end;

    OPEN o_Cursor FOR
      select ie.id_entidad_fiduciario, ie.id_legajo, ie.Id_Tramite_Ipj,
        to_char(ie.fecha_inicio, 'dd/mm/rrrr') fecha_inicio, to_char(ie.fecha_fin, 'dd/mm/rrrr') fecha_fin,
        ie.id_integrante, ie.id_legajo_emp, ie.cuit_empresa, ie.n_empresa, ie.folio, ie.anio,
        ie.En_Formacion, ie.Matricula, to_char(ie.Fec_Acta, 'dd/mm/rrrr') Fec_Acta,
        ie.Id_Tipo_Entidad, ie.Certificacion_Contable, ie.Matricula_Certificante, ie.borrador,
        (case
           when ie.id_integrante is null and ie.n_empresa is not null then
              ie.n_empresa
           when ie.id_integrante is not null then
              (p.NOMBRE || ' ' ||p.APELLIDO)
           else ''
         end) Persona,
        (case
           when ie.id_integrante is null and ie.n_empresa is not null then ie.cuit_empresa
           when ie.id_integrante is not null then I.CUIL
           else  ''
         end) Identificacion,
        p_Id_Tramite_Ipj IdTramiteIpj_Entidad, ie.persona_expuesta, ie.tipo_participacion,
        ie.id_integrante_representante
      from ipj.t_entidades_fiduciarios ie join ipj.t_Entidades tpj
          on ie.Id_Tramite_Ipj = tpj.Id_Tramite_Ipj and ie.id_legajo = tpj.id_legajo
        left join IPJ.t_tipos_entidades te
          on te.Id_Tipo_Entidad = ie.Id_Tipo_Entidad
        left join ipj.t_integrantes i
          on ie.id_integrante = i.id_integrante
        left join rcivil.vt_pk_persona p
          on i.id_sexo = p.ID_SEXO and
            i.nro_documento = p.NRO_DOCUMENTO and
            i.pai_cod_pais = p.PAI_COD_PAIS and
            i.id_numero = p.ID_NUMERO
      where
        tpj.id_legajo = p_id_legajo and
        tpj.matricula_version <= v_matricula_version and
        tpj.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_ubicacion_leg) and
        (ie.borrador = 'N' or ie.Id_Tramite_Ipj = p_Id_Tramite_Ipj) and
        (ie.fecha_fin is null or ie.fecha_fin >= to_date(sysdate, 'dd/mm/rrrr') or  ie.Id_Tramite_Ipj = p_Id_Tramite_Ipj);

  END SP_Traer_PersJur_Fiduciario;

  PROCEDURE SP_Traer_PersJur_Fiduciante(
    o_Cursor OUT TYPES.cursorType,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number)
  IS
  /*********************************************************
  Este procemiento devuelve el listado de fiduciantes asociados a una entidad de un tramite
  **********************************************************/
    v_matricula_version number(6,0);
    v_ubicacion_ent number(6);
    v_ubicacion_leg number(6);
  BEGIN

    -- busto la ultima area y su version, para visualizar los datos correctos
    begin
      select nvl(matricula_version, 0),
        (select id_ubicacion from ipj.t_tipos_entidades te where te.id_tipo_entidad = e.id_tipo_entidad) ubicacion_ent,
        (select id_ubicacion from ipj.t_tipos_entidades te where te.id_tipo_entidad = l.id_tipo_entidad) ubicacion_leg
        into v_matricula_version, v_ubicacion_ent, v_ubicacion_leg
      from IPJ.t_entidades e join ipj.t_legajos l
        on e.id_legajo = l.id_legajo
      where
        e.Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        e.id_legajo = p_id_legajo;
    exception
      when NO_DATA_FOUND then
        v_matricula_version := 0;
    end;

    OPEN o_Cursor FOR
      select ie.id_entidad_fiduciante, ie.id_legajo, ie.Id_Tramite_Ipj,
        to_char(ie.fecha_inicio, 'dd/mm/rrrr') fecha_inicio, to_char(ie.fecha_fin, 'dd/mm/rrrr') fecha_fin,
        ie.id_integrante, ie.id_legajo_emp, ie.cuit_empresa, ie.n_empresa, ie.folio, ie.anio,
        ie.En_Formacion, ie.Matricula, to_char(ie.Fec_Acta, 'dd/mm/rrrr') Fec_Acta,
        ie.Id_Tipo_Entidad, ie.Certificacion_Contable, ie.Matricula_Certificante, ie.borrador,
        (case
           when ie.id_integrante is null and ie.n_empresa is not null then
              ie.n_empresa
           when ie.id_integrante is not null then
              (p.NOMBRE || ' ' ||p.APELLIDO)
           else ''
         end) Persona,
        (case
           when ie.id_integrante is null and ie.n_empresa is not null then ie.cuit_empresa
           when ie.id_integrante is not null then I.CUIL
           else  ''
         end) Identificacion,
        p_Id_Tramite_Ipj IdTramiteIpj_Entidad, ie.persona_expuesta, ie.tipo_participacion
      from ipj.t_entidades_fiduciantes ie join ipj.t_Entidades tpj
          on ie.Id_Tramite_Ipj = tpj.Id_Tramite_Ipj and ie.id_legajo = tpj.id_legajo
        left join IPJ.t_tipos_entidades te
          on te.Id_Tipo_Entidad = ie.Id_Tipo_Entidad
        left join ipj.t_integrantes i
          on ie.id_integrante = i.id_integrante
        left join rcivil.vt_pk_persona p
          on i.id_sexo = p.ID_SEXO and
            i.nro_documento = p.NRO_DOCUMENTO and
            i.pai_cod_pais = p.PAI_COD_PAIS and
            i.id_numero = p.ID_NUMERO
      where
        tpj.id_legajo = p_id_legajo and
        tpj.matricula_version <= v_matricula_version and
        tpj.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_ubicacion_leg) and
        (ie.borrador = 'N' or ie.Id_Tramite_Ipj = p_Id_Tramite_Ipj) and
        (ie.fecha_fin is null or ie.fecha_fin >= to_date(sysdate, 'dd/mm/rrrr') or  ie.Id_Tramite_Ipj = p_Id_Tramite_Ipj);

  END SP_Traer_PersJur_Fiduciante;

  PROCEDURE SP_Traer_PersJur_Benef(
    o_Cursor OUT TYPES.cursorType,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number)
  IS
  /*********************************************************
  Este procemiento devuelve el listado de Benefinciarios asociados a una entidad de un tramite
  **********************************************************/
    v_matricula_version number(6,0);
    v_ubicacion_ent number(6);
    v_ubicacion_leg number(6);
  BEGIN

    -- busto la ultima area y su version, para visualizar los datos correctos
    begin
      select nvl(matricula_version, 0),
        (select id_ubicacion from ipj.t_tipos_entidades te where te.id_tipo_entidad = e.id_tipo_entidad) ubicacion_ent,
        (select id_ubicacion from ipj.t_tipos_entidades te where te.id_tipo_entidad = l.id_tipo_entidad) ubicacion_leg
        into v_matricula_version, v_ubicacion_ent, v_ubicacion_leg
      from IPJ.t_entidades e join ipj.t_legajos l
        on e.id_legajo = l.id_legajo
      where
        e.Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        e.id_legajo = p_id_legajo;
    exception
      when NO_DATA_FOUND then
        v_matricula_version := 0;
    end;

    OPEN o_Cursor FOR
      select ie.id_entidad_benef, ie.id_legajo, ie.Id_Tramite_Ipj,
        to_char(ie.fecha_inicio, 'dd/mm/rrrr') fecha_inicio, to_char(ie.fecha_fin, 'dd/mm/rrrr') fecha_fin,
        ie.id_integrante, ie.id_legajo_emp, ie.cuit_empresa, ie.n_empresa, ie.folio, ie.anio,
        ie.En_Formacion, ie.Matricula, to_char(ie.Fec_Acta, 'dd/mm/rrrr') Fec_Acta,
        ie.Id_Tipo_Entidad, ie.Certificacion_Contable, ie.Matricula_Certificante, ie.borrador,
        (case
           when ie.id_integrante is null and ie.n_empresa is not null then
              ie.n_empresa
           when ie.id_integrante is not null then
              (p.NOMBRE || ' ' ||p.APELLIDO)
           else ''
         end) Persona,
        (case
           when ie.id_integrante is null and ie.n_empresa is not null then ie.cuit_empresa
           when ie.id_integrante is not null then I.CUIL
           else  ''
         end) Identificacion,
        p_Id_Tramite_Ipj IdTramiteIpj_Entidad, ie.persona_expuesta, ie.tipo_participacion
      from ipj.t_entidades_benef ie join ipj.t_Entidades tpj
          on ie.Id_Tramite_Ipj = tpj.Id_Tramite_Ipj and ie.id_legajo = tpj.id_legajo
        left join IPJ.t_tipos_entidades te
          on te.Id_Tipo_Entidad = ie.Id_Tipo_Entidad
        left join ipj.t_integrantes i
          on ie.id_integrante = i.id_integrante
        left join rcivil.vt_pk_persona p
          on i.id_sexo = p.ID_SEXO and
            i.nro_documento = p.NRO_DOCUMENTO and
            i.pai_cod_pais = p.PAI_COD_PAIS and
            i.id_numero = p.ID_NUMERO
      where
        tpj.id_legajo = p_id_legajo and
        tpj.matricula_version <= v_matricula_version and
        tpj.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_ubicacion_leg) and
        (ie.borrador = 'N' or ie.Id_Tramite_Ipj = p_Id_Tramite_Ipj) and
        (ie.fecha_fin is null or ie.fecha_fin >= to_date(sysdate, 'dd/mm/rrrr') or  ie.Id_Tramite_Ipj = p_Id_Tramite_Ipj);

  END SP_Traer_PersJur_Benef;


  PROCEDURE SP_Traer_PersJur_Fideicom(
    o_Cursor OUT TYPES.cursorType,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number)
  IS
  /*********************************************************
  Este procemiento devuelve el listado de Fideicomisarios asociados a una entidad de un tramite
  **********************************************************/
    v_matricula_version number(6,0);
    v_ubicacion_ent number(6);
    v_ubicacion_leg number(6);
  BEGIN

    -- busto la ultima area y su version, para visualizar los datos correctos
    begin
      select nvl(matricula_version, 0),
        (select id_ubicacion from ipj.t_tipos_entidades te where te.id_tipo_entidad = e.id_tipo_entidad) ubicacion_ent,
        (select id_ubicacion from ipj.t_tipos_entidades te where te.id_tipo_entidad = l.id_tipo_entidad) ubicacion_leg
        into v_matricula_version, v_ubicacion_ent, v_ubicacion_leg
      from IPJ.t_entidades e join ipj.t_legajos l
        on e.id_legajo = l.id_legajo
      where
        e.Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        e.id_legajo = p_id_legajo;
    exception
      when NO_DATA_FOUND then
        v_matricula_version := 0;
    end;

    OPEN o_Cursor FOR
      select ie.id_entidad_fideicom, ie.id_legajo, ie.Id_Tramite_Ipj,
        to_char(ie.fecha_inicio, 'dd/mm/rrrr') fecha_inicio, to_char(ie.fecha_fin, 'dd/mm/rrrr') fecha_fin,
        ie.id_integrante, ie.id_legajo_emp, ie.cuit_empresa, ie.n_empresa, ie.folio, ie.anio,
        ie.En_Formacion, ie.Matricula, to_char(ie.Fec_Acta, 'dd/mm/rrrr') Fec_Acta,
        ie.Id_Tipo_Entidad, ie.Certificacion_Contable, ie.Matricula_Certificante, ie.borrador,
        (case
           when ie.id_integrante is null and ie.n_empresa is not null then
              ie.n_empresa
           when ie.id_integrante is not null then
              (p.NOMBRE || ' ' ||p.APELLIDO)
           else ''
         end) Persona,
        (case
           when ie.id_integrante is null and ie.n_empresa is not null then ie.cuit_empresa
           when ie.id_integrante is not null then I.CUIL
           else  ''
         end) Identificacion,
        p_Id_Tramite_Ipj IdTramiteIpj_Entidad, ie.persona_expuesta, ie.tipo_participacion
      from ipj.t_entidades_fideicom ie join ipj.t_Entidades tpj
          on ie.Id_Tramite_Ipj = tpj.Id_Tramite_Ipj and ie.id_legajo = tpj.id_legajo
        left join IPJ.t_tipos_entidades te
          on te.Id_Tipo_Entidad = ie.Id_Tipo_Entidad
        left join ipj.t_integrantes i
          on ie.id_integrante = i.id_integrante
        left join rcivil.vt_pk_persona p
          on i.id_sexo = p.ID_SEXO and
            i.nro_documento = p.NRO_DOCUMENTO and
            i.pai_cod_pais = p.PAI_COD_PAIS and
            i.id_numero = p.ID_NUMERO
      where
        tpj.id_legajo = p_id_legajo and
        tpj.matricula_version <= v_matricula_version and
        tpj.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_ubicacion_leg) and
        (ie.borrador = 'N' or ie.Id_Tramite_Ipj = p_Id_Tramite_Ipj) and
        (ie.fecha_fin is null or ie.fecha_fin >= to_date(sysdate, 'dd/mm/rrrr') or  ie.Id_Tramite_Ipj = p_Id_Tramite_Ipj);

  END SP_Traer_PersJur_Fideicom;

  PROCEDURE SP_GUARDAR_ENTIDAD_FIDUCIARIO(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_entidad_fiduciario out number,
    p_id_entidad_fiduciario in number,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number,
    -- Datos particulares
    p_fecha_inicio in varchar2, -- es fecha
    p_fecha_fin in varchar2,  -- es fecha
    p_tipo_participacion in varchar2,
    p_persona_expuesta in char,
    -- Persona F�sica
    p_id_integrante in number,
    -- Empresa
    p_id_legajo_emp in number,
    p_cuit_empresa in varchar2,
    p_n_empresa in varchar2,
    p_id_tipo_entidad in number,
    p_matricula in varchar2,
    p_folio in varchar2,
    p_anio in varchar2,
    p_en_formacion in char,
    p_fec_acta in varchar2, --es fecha
    p_certificacion_contable in char,
    p_matricula_certificante in varchar2,
    -- Varios
    p_eliminar in number -- 1 Elimina
    )
  IS
    /*****************************************************
    Guarda los Fiduciarios asociados a una entidad de un tramite.
    Si existe los actualizo, sino los agrego.
    No maneja transaccion, ya que se usa en cascada desde la aplicacion y lo administra el codigo.
  ******************************************************/
    v_valid_parametros varchar2(200);
    v_id_integrante number;
    v_bloque varchar2(100);
    v_tipo_mensaje number;
    v_id_legajo_emp number;
    v_id_tipo_entidad number;
  BEGIN

    -- VALIDACION DE PARAMETROS
    v_bloque := 'GUARDAR SOCIOS - Parametros : ';
    if IPJ.VARIOS.Valida_Entidad(nvl(p_Id_Tramite_Ipj, 0), p_id_legajo) = 0 then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('ENT_NOT')|| '(' || to_char(p_Id_Tramite_Ipj) ||' - ' || to_char(p_id_legajo)||')';
    end if;
    if p_eliminar <> 1 and p_fecha_inicio is not null and IPJ.VARIOS.Valida_Fecha(p_fecha_inicio) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT');
    end if;
    if p_eliminar <> 1 and p_fecha_fin is not null and IPJ.VARIOS.Valida_Fecha(p_fecha_fin) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT');
    end if;
    if p_eliminar <> 1 and p_fec_acta is not null and IPJ.VARIOS.Valida_Fecha(p_fec_acta) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT');
    end if;

    -- Si los parametros no estan OK, no continuo
    if v_valid_parametros is not null then
      o_rdo := v_bloque || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    -- CUERPO DEL PROCEDIMEINTO
    if p_eliminar = 1 then
      delete ipj.t_entidades_fiduciarios
      where
        id_entidad_fiduciario = p_id_entidad_fiduciario and
        borrador = 'S';

      o_id_entidad_fiduciario := 0;
    else
      v_bloque := 'Integrantes - Fiduciarios ';
      -- Si el Legajo o el Integrante vienen en 0, pongo NULL para respetar las FK
      v_id_legajo_emp := (case when p_id_legajo_emp = 0 then null else p_id_legajo_emp end);
      v_id_integrante := (case when p_id_integrante = 0 then null else p_id_integrante end);
      v_id_tipo_entidad := (case when p_id_tipo_entidad = 0 then null else p_id_tipo_entidad end);

      -- Actualizo el Fiduciario
      update ipj.t_entidades_fiduciarios e
      set
        fecha_fin = to_date(p_fecha_fin, 'dd/mm/rrrr'),
        n_empresa = p_n_empresa,
        id_tipo_entidad = v_id_tipo_entidad,
        certificacion_contable = p_certificacion_contable,
        matricula_certificante = p_matricula_certificante,
        fec_acta = to_date(p_fec_acta, 'dd/mm/rrrr'),
        matricula = p_matricula,
        en_formacion = p_en_formacion,
        folio = p_folio,
        anio = p_anio,
        persona_expuesta = p_persona_expuesta,
        tipo_participacion = p_tipo_participacion
      where
        id_entidad_fiduciario = p_id_entidad_fiduciario;

       -- si no existe, lo agrego
      if sql%rowcount = 0 then
        SELECT IPJ.SEQ_ENTIDAD_FIDUCIARIOS.nextval INTO o_id_entidad_fiduciario FROM dual;

        INSERT INTO IPJ.T_ENTIDADES_FIDUCIARIOS (
          Id_Entidad_Fiduciario, Id_Tramite_Ipj, Id_Legajo, Id_Integrante, Id_Legajo_Emp,
          Cuit_Empresa, N_Empresa, Id_Tipo_Entidad, Matricula, Folio, Anio, En_Formacion,
          Fec_Acta, Certificacion_Contable, Matricula_Certificante, Id_Vin_Empresa,
          Persona_Expuesta, Tipo_Participacion, Fecha_Inicio, Fecha_Fin, Borrador)
        values
          (o_id_entidad_fiduciario, p_id_tramite_ipj, p_id_legajo, v_id_integrante, v_id_legajo_emp,
           p_cuit_empresa, p_n_empresa, v_id_tipo_entidad, p_matricula, p_folio, p_anio, p_en_formacion,
            to_date(p_fec_acta, 'dd/mm/rrrr'), p_certificacion_contable, p_matricula_certificante, null,
            p_persona_expuesta, p_tipo_participacion,
            to_date(p_fecha_inicio, 'dd/mm/rrrr'),  to_date(p_fecha_fin, 'dd/mm/rrrr'), 'S');

      else
        o_id_entidad_fiduciario := p_id_entidad_fiduciario;
      end if;
    end if;

    o_rdo := TYPES.c_Resp_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
    WHEN OTHERS THEN
       o_rdo := v_bloque || To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_GUARDAR_ENTIDAD_FIDUCIARIO;

  PROCEDURE SP_GUARDAR_ENTIDAD_FIDUCIANTE(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_entidad_fiduciante out number,
    p_id_entidad_fiduciante in number,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number,
    -- Datos particulares
    p_fecha_inicio in varchar2, -- es fecha
    p_fecha_fin in varchar2,  -- es fecha
    p_tipo_participacion in varchar2,
    p_persona_expuesta in char,
    -- Persona F�sica
    p_id_integrante in number,
    -- Empresa
    p_id_legajo_emp in number,
    p_cuit_empresa in varchar2,
    p_n_empresa in varchar2,
    p_id_tipo_entidad in number,
    p_matricula in varchar2,
    p_folio in varchar2,
    p_anio in varchar2,
    p_en_formacion in char,
    p_fec_acta in varchar2, --es fecha
    p_certificacion_contable in char,
    p_matricula_certificante in varchar2,
    -- Varios
    p_eliminar in number -- 1 Elimina
    )
  IS
    /*****************************************************
    Guarda los Fiduciantes asociados a una entidad de un tramite.
    Si existe los actualizo, sino los agrego.
    No maneja transaccion, ya que se usa en cascada desde la aplicacion y lo administra el codigo.
  ******************************************************/
    v_valid_parametros varchar2(200);
    v_id_integrante number;
    v_bloque varchar2(100);
    v_tipo_mensaje number;
    v_id_legajo_emp number;
    v_id_tipo_entidad number;
  BEGIN

    -- VALIDACION DE PARAMETROS
    v_bloque := 'GUARDAR SOCIOS - Parametros : ';
    if IPJ.VARIOS.Valida_Entidad(nvl(p_Id_Tramite_Ipj, 0), p_id_legajo) = 0 then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('ENT_NOT')|| '(' || to_char(p_Id_Tramite_Ipj) ||' - ' || to_char(p_id_legajo)||')';
    end if;
    if p_eliminar <> 1 and p_fecha_inicio is not null and IPJ.VARIOS.Valida_Fecha(p_fecha_inicio) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT');
    end if;
    if p_eliminar <> 1 and p_fecha_fin is not null and IPJ.VARIOS.Valida_Fecha(p_fecha_fin) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT');
    end if;
    if p_eliminar <> 1 and p_fec_acta is not null and IPJ.VARIOS.Valida_Fecha(p_fec_acta) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT');
    end if;

    -- Si los parametros no estan OK, no continuo
    if v_valid_parametros is not null then
      o_rdo := v_bloque || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    -- CUERPO DEL PROCEDIMEINTO
    if p_eliminar = 1 then
      delete ipj.t_entidades_fiduciantes
      where
        id_entidad_fiduciante = p_id_entidad_fiduciante and
        borrador = 'S';

      o_id_entidad_fiduciante := 0;
    else
      v_bloque := 'Integrantes - Fiduciantes ';
      -- Si el Legajo o el Integrante vienen en 0, pongo NULL para respetar las FK
      v_id_legajo_emp := (case when p_id_legajo_emp = 0 then null else p_id_legajo_emp end);
      v_id_integrante := (case when p_id_integrante = 0 then null else p_id_integrante end);
      v_id_tipo_entidad := (case when p_id_tipo_entidad = 0 then null else p_id_tipo_entidad end);

      -- Actualizo el Fiduciario
      update ipj.t_entidades_fiduciantes e
      set
        fecha_fin = to_date(p_fecha_fin, 'dd/mm/rrrr'),
        n_empresa = p_n_empresa,
        id_tipo_entidad = v_id_tipo_entidad,
        certificacion_contable = p_certificacion_contable,
        matricula_certificante = p_matricula_certificante,
        fec_acta = to_date(p_fec_acta, 'dd/mm/rrrr'),
        matricula = p_matricula,
        en_formacion = p_en_formacion,
        folio = p_folio,
        anio = p_anio,
        persona_expuesta = p_persona_expuesta,
        tipo_participacion = p_tipo_participacion
      where
        id_entidad_fiduciante = p_id_entidad_fiduciante;

       -- si no existe, lo agrego
      if sql%rowcount = 0 then
        SELECT IPJ.SEQ_ENTIDAD_FIDUCIANTE.nextval INTO o_id_entidad_fiduciante FROM dual;

        INSERT INTO IPJ.T_ENTIDADES_FIDUCIANTES (
          Id_Entidad_Fiduciante, Id_Tramite_Ipj, Id_Legajo, Id_Integrante, Id_Legajo_Emp,
          Cuit_Empresa, N_Empresa, Id_Tipo_Entidad, Matricula, Folio, Anio, En_Formacion,
          Fec_Acta, Certificacion_Contable, Matricula_Certificante, Id_Vin_Empresa,
          Persona_Expuesta, Tipo_Participacion, Fecha_Inicio, Fecha_Fin, Borrador)
        values
          (o_id_entidad_fiduciante, p_id_tramite_ipj, p_id_legajo, v_id_integrante, v_id_legajo_emp,
           p_cuit_empresa, p_n_empresa, v_id_tipo_entidad, p_matricula, p_folio, p_anio, p_en_formacion,
            to_date(p_fec_acta, 'dd/mm/rrrr'), p_certificacion_contable, p_matricula_certificante, null,
            p_persona_expuesta, p_tipo_participacion,
            to_date(p_fecha_inicio, 'dd/mm/rrrr'),  to_date(p_fecha_fin, 'dd/mm/rrrr'), 'S');

      else
        o_id_entidad_fiduciante := p_id_entidad_fiduciante;
      end if;
    end if;

    o_rdo := TYPES.c_Resp_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
    WHEN OTHERS THEN
       o_rdo := v_bloque || To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_GUARDAR_ENTIDAD_FIDUCIANTE;


  PROCEDURE SP_GUARDAR_ENTIDAD_BENEF(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_entidad_benef out number,
    p_id_entidad_benef in number,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number,
    -- Datos particulares
    p_fecha_inicio in varchar2, -- es fecha
    p_fecha_fin in varchar2,  -- es fecha
    p_tipo_participacion in varchar2,
    p_persona_expuesta in char,
    -- Persona F�sica
    p_id_integrante in number,
    -- Empresa
    p_id_legajo_emp in number,
    p_cuit_empresa in varchar2,
    p_n_empresa in varchar2,
    p_id_tipo_entidad in number,
    p_matricula in varchar2,
    p_folio in varchar2,
    p_anio in varchar2,
    p_en_formacion in char,
    p_fec_acta in varchar2, --es fecha
    p_certificacion_contable in char,
    p_matricula_certificante in varchar2,
    -- Varios
    p_eliminar in number -- 1 Elimina
    )
  IS
    /*****************************************************
    Guarda los Beneficiarios asociados a una entidad de un tramite.
    Si existe los actualizo, sino los agrego.
    No maneja transaccion, ya que se usa en cascada desde la aplicacion y lo administra el codigo.
  ******************************************************/
    v_valid_parametros varchar2(200);
    v_id_integrante number;
    v_bloque varchar2(100);
    v_tipo_mensaje number;
    v_id_legajo_emp number;
    v_id_tipo_entidad number;
  BEGIN

    -- VALIDACION DE PARAMETROS
    v_bloque := 'GUARDAR SOCIOS - Parametros : ';
    if IPJ.VARIOS.Valida_Entidad(nvl(p_Id_Tramite_Ipj, 0), p_id_legajo) = 0 then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('ENT_NOT')|| '(' || to_char(p_Id_Tramite_Ipj) ||' - ' || to_char(p_id_legajo)||')';
    end if;
    if p_eliminar <> 1 and p_fecha_inicio is not null and IPJ.VARIOS.Valida_Fecha(p_fecha_inicio) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT');
    end if;
    if p_eliminar <> 1 and p_fecha_fin is not null and IPJ.VARIOS.Valida_Fecha(p_fecha_fin) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT');
    end if;
    if p_eliminar <> 1 and p_fec_acta is not null and IPJ.VARIOS.Valida_Fecha(p_fec_acta) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT');
    end if;

    -- Si los parametros no estan OK, no continuo
    if v_valid_parametros is not null then
      o_rdo := v_bloque || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    -- CUERPO DEL PROCEDIMEINTO
    if p_eliminar = 1 then
      delete ipj.t_entidades_benef
      where
        id_entidad_benef = p_id_entidad_benef and
        borrador = 'S';

      o_id_entidad_benef := 0;
    else
      v_bloque := 'Integrantes - Beneficiarios ';
      -- Si el Legajo o el Integrante vienen en 0, pongo NULL para respetar las FK
      v_id_legajo_emp := (case when p_id_legajo_emp = 0 then null else p_id_legajo_emp end);
      v_id_integrante := (case when p_id_integrante = 0 then null else p_id_integrante end);
      v_id_tipo_entidad := (case when p_id_tipo_entidad = 0 then null else p_id_tipo_entidad end);

      -- Actualizo el Beneficiario
      update ipj.t_entidades_benef e
      set
        fecha_fin = to_date(p_fecha_fin, 'dd/mm/rrrr'),
        n_empresa = p_n_empresa,
        id_tipo_entidad = v_id_tipo_entidad,
        certificacion_contable = p_certificacion_contable,
        matricula_certificante = p_matricula_certificante,
        fec_acta = to_date(p_fec_acta, 'dd/mm/rrrr'),
        matricula = p_matricula,
        en_formacion = p_en_formacion,
        folio = p_folio,
        anio = p_anio,
        persona_expuesta = p_persona_expuesta,
        tipo_participacion = p_tipo_participacion
      where
        id_entidad_benef = p_id_entidad_benef;

       -- si no existe, lo agrego
      if sql%rowcount = 0 then
        SELECT IPJ.SEQ_ENTIDAD_BENEF.nextval INTO o_id_entidad_benef FROM dual;

        INSERT INTO IPJ.T_ENTIDADES_BENEF (
          Id_Entidad_benef, Id_Tramite_Ipj, Id_Legajo, Id_Integrante, Id_Legajo_Emp,
          Cuit_Empresa, N_Empresa, Id_Tipo_Entidad, Matricula, Folio, Anio, En_Formacion,
          Fec_Acta, Certificacion_Contable, Matricula_Certificante, Id_Vin_Empresa,
          Persona_Expuesta, Tipo_Participacion, Fecha_Inicio, Fecha_Fin, Borrador)
        values
          (o_id_entidad_benef, p_id_tramite_ipj, p_id_legajo, v_id_integrante, v_id_legajo_emp,
           p_cuit_empresa, p_n_empresa, v_id_tipo_entidad, p_matricula, p_folio, p_anio, p_en_formacion,
            to_date(p_fec_acta, 'dd/mm/rrrr'), p_certificacion_contable, p_matricula_certificante, null,
            p_persona_expuesta, p_tipo_participacion,
            to_date(p_fecha_inicio, 'dd/mm/rrrr'),  to_date(p_fecha_fin, 'dd/mm/rrrr'), 'S');

      else
        o_id_entidad_benef := p_id_entidad_benef;
      end if;
    end if;

    o_rdo := TYPES.c_Resp_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
    WHEN OTHERS THEN
       o_rdo := v_bloque || To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_GUARDAR_ENTIDAD_BENEF;


  PROCEDURE SP_GUARDAR_ENTIDAD_FIDEICOM(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_entidad_fideicom out number,
    p_id_entidad_fideicom in number,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number,
    -- Datos particulares
    p_fecha_inicio in varchar2, -- es fecha
    p_fecha_fin in varchar2,  -- es fecha
    p_tipo_participacion in varchar2,
    p_persona_expuesta in char,
    -- Persona F�sica
    p_id_integrante in number,
    -- Empresa
    p_id_legajo_emp in number,
    p_cuit_empresa in varchar2,
    p_n_empresa in varchar2,
    p_id_tipo_entidad in number,
    p_matricula in varchar2,
    p_folio in varchar2,
    p_anio in varchar2,
    p_en_formacion in char,
    p_fec_acta in varchar2, --es fecha
    p_certificacion_contable in char,
    p_matricula_certificante in varchar2,
    -- Varios
    p_eliminar in number -- 1 Elimina
    )
  IS
    /*****************************************************
    Guarda los Fedeicomisarios asociados a una entidad de un tramite.
    Si existe los actualizo, sino los agrego.
    No maneja transaccion, ya que se usa en cascada desde la aplicacion y lo administra el codigo.
  ******************************************************/
    v_valid_parametros varchar2(200);
    v_id_integrante number;
    v_bloque varchar2(100);
    v_tipo_mensaje number;
    v_id_legajo_emp number;
    v_id_tipo_entidad number;
  BEGIN

    -- VALIDACION DE PARAMETROS
    v_bloque := 'GUARDAR SOCIOS - Parametros : ';
    if IPJ.VARIOS.Valida_Entidad(nvl(p_Id_Tramite_Ipj, 0), p_id_legajo) = 0 then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('ENT_NOT')|| '(' || to_char(p_Id_Tramite_Ipj) ||' - ' || to_char(p_id_legajo)||')';
    end if;
    if p_eliminar <> 1 and p_fecha_inicio is not null and IPJ.VARIOS.Valida_Fecha(p_fecha_inicio) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT');
    end if;
    if p_eliminar <> 1 and p_fecha_fin is not null and IPJ.VARIOS.Valida_Fecha(p_fecha_fin) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT');
    end if;
    if p_eliminar <> 1 and p_fec_acta is not null and IPJ.VARIOS.Valida_Fecha(p_fec_acta) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT');
    end if;

    -- Si los parametros no estan OK, no continuo
    if v_valid_parametros is not null then
      o_rdo := v_bloque || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    -- CUERPO DEL PROCEDIMEINTO
    if p_eliminar = 1 then
      delete ipj.t_entidades_fideicom
      where
        id_entidad_fideicom = p_id_entidad_fideicom and
        borrador = 'S';

      o_id_entidad_fideicom := 0;
    else
      v_bloque := 'Integrantes - Fideicomisarios ';
      -- Si el Legajo o el Integrante vienen en 0, pongo NULL para respetar las FK
      v_id_legajo_emp := (case when p_id_legajo_emp = 0 then null else p_id_legajo_emp end);
      v_id_integrante := (case when p_id_integrante = 0 then null else p_id_integrante end);
      v_id_tipo_entidad := (case when p_id_tipo_entidad = 0 then null else p_id_tipo_entidad end);

      -- Actualizo el Fideicomisario
      update ipj.t_entidades_fideicom e
      set
        fecha_fin = to_date(p_fecha_fin, 'dd/mm/rrrr'),
        n_empresa = p_n_empresa,
        id_tipo_entidad = v_id_tipo_entidad,
        certificacion_contable = p_certificacion_contable,
        matricula_certificante = p_matricula_certificante,
        fec_acta = to_date(p_fec_acta, 'dd/mm/rrrr'),
        matricula = p_matricula,
        en_formacion = p_en_formacion,
        folio = p_folio,
        anio = p_anio,
        persona_expuesta = p_persona_expuesta,
        tipo_participacion = p_tipo_participacion
      where
        id_entidad_fideicom = p_id_entidad_fideicom;

       -- si no existe, lo agrego
      if sql%rowcount = 0 then
        SELECT IPJ.SEQ_ENTIDAD_FIDEICOM.nextval INTO o_id_entidad_FIDEICOM FROM dual;

        INSERT INTO IPJ.T_ENTIDADES_FIDEICOM (
          Id_Entidad_fideicom, Id_Tramite_Ipj, Id_Legajo, Id_Integrante, Id_Legajo_Emp,
          Cuit_Empresa, N_Empresa, Id_Tipo_Entidad, Matricula, Folio, Anio, En_Formacion,
          Fec_Acta, Certificacion_Contable, Matricula_Certificante, Id_Vin_Empresa,
          Persona_Expuesta, Tipo_Participacion, Fecha_Inicio, Fecha_Fin, Borrador)
        values
          (o_id_entidad_fideicom, p_id_tramite_ipj, p_id_legajo, v_id_integrante, v_id_legajo_emp,
           p_cuit_empresa, p_n_empresa, v_id_tipo_entidad, p_matricula, p_folio, p_anio, p_en_formacion,
            to_date(p_fec_acta, 'dd/mm/rrrr'), p_certificacion_contable, p_matricula_certificante, null,
            p_persona_expuesta, p_tipo_participacion,
            to_date(p_fecha_inicio, 'dd/mm/rrrr'),  to_date(p_fecha_fin, 'dd/mm/rrrr'), 'S');

      else
        o_id_entidad_fideicom := p_id_entidad_fideicom;
      end if;
    end if;

    o_rdo := TYPES.c_Resp_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
    WHEN OTHERS THEN
       o_rdo := v_bloque || To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_GUARDAR_ENTIDAD_FIDEICOM;

  PROCEDURE SP_Traer_PersJur_Docs(
    o_Cursor OUT TYPES.cursorType,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number
    )
  IS
  /*******************************************************
    Este proceso lista los documentos asociados a un tr�mite y/o una empresa:
    - Si viene legajo, muestra todos los documentos asociados (sin importar el tr�mite)
    - Si no viene legajo, muestra los documentos del t�mite indicado
  ********************************************************/
  BEGIN

    if nvl(p_id_legajo, 0) = 0 then
      -- busca por tr�mite
      OPEN o_Cursor FOR
        select distinct ed.id_tramite_ipj, ed.id_legajo, ed.id_documento, ed.n_documento,
          ed.observacion, ed.cuil_usr, to_char(ed.fecha_alta, 'dd/mm/rrrr') fecha_alta,
          ed.cuil_usr, ed.id_tipo_documento_cdd, vt_cdd.n_tipo_documento,
          ed.nro_escribania_orig, ed.id_cargo_escrib_orig, ed.n_cargo_escrib_orig
        from ipj.t_entidades_documentos ed
          left join cdd.vt_tipos_documento_ipj vt_cdd
            on ed.id_tipo_documento_cdd = vt_cdd.id_tipo_documento
        where
          ed.id_tramite_ipj = p_id_tramite_ipj
      UNION ALL
        select distinct ed.id_tramite_ipj, ed.id_legajo, ed.id_documento, ed.n_documento,
          ed.observacion, ed.cuil_usr, to_char(ed.fecha_alta, 'dd/mm/rrrr') fecha_alta,
          ed.cuil_usr, ed.id_tipo_documento_cdd, vt_cdd.n_tipo_documento,
          ed.nro_escribania_orig, ed.id_cargo_escrib_orig, ed.n_cargo_escrib_orig
          from ipj.t_entidades_documentos ed
     left join cdd.vt_tipos_documento_ipj vt_cdd
            on ed.id_tipo_documento_cdd = vt_cdd.id_tipo_documento
          JOIN ipj.t_tramitesipj tra
            ON ed.id_tramite_ipj = tra.id_tramite_ipj
         WHERE tra.id_tramite_ipj_padre = p_id_tramite_ipj
        order by fecha_alta, n_documento DESC;
    else
      -- busca por legajo
      OPEN o_Cursor FOR
        select distinct ed.id_tramite_ipj, ed.id_legajo, ed.id_documento, ed.n_documento,
          ed.observacion, ed.cuil_usr, to_char(ed.fecha_alta, 'dd/mm/rrrr') fecha_alta,
          ed.cuil_usr, ed.id_tipo_documento_cdd, vt_cdd.n_tipo_documento
        from ipj.t_entidades_documentos ed
          left join cdd.vt_tipos_documento_ipj vt_cdd
            on ed.id_tipo_documento_cdd = vt_cdd.id_tipo_documento
        where
          ed.id_legajo = p_id_legajo
        order by fecha_alta, ed.n_documento desc;
    end if;
  END SP_Traer_PersJur_Docs;

  PROCEDURE SP_Guardar_Entidad_Docs(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number,
    p_id_documento in number,
    p_n_documento in varchar2,
    p_observacion in varchar2,
    p_cuil_usr in varchar2,
    p_fecha_alta in varchar2, -- es fecha
    p_id_tipo_documento_cdd in number,
    p_eliminar in number) -- 1 Elimina
  IS
  /*******************************************************
    Guarda un documento asociado a una empresa y un tr�mite.
  ********************************************************/
    v_valid_parametros varchar2(1000);
  BEGIN

    -- VALIDACION DE PARAMETROS
    if nvl(p_id_tramite_ipj, 0) = 0 or nvl(p_id_legajo, 0) = 0 or nvl(p_id_documento, 0)= 0 then
      v_valid_parametros := 'Se debe asociar el documento a una empresa en un tr�mite.';
    end if;

    if p_eliminar <> 1 and IPJ.VARIOS.Valida_Fecha(p_fecha_alta) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT');
    end if;

    if v_valid_parametros is not null then
      o_rdo := 'GUARDAR DOCS - Par�metros: '  || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
    end if;

    /*********************************************************
      CUERPO DEL PROCEDIMEINTO
    *********************************************************/
    if nvl(p_eliminar, 0) = 1 then
      delete ipj.t_entidades_documentos
      where
        id_tramite_ipj = p_id_tramite_ipj and
        id_legajo = p_id_legajo and
        id_documento = p_id_documento;

    else
      update ipj.t_entidades_documentos
      set
        observacion = p_observacion,
        cuil_usr = p_cuil_usr,
        fecha_alta = to_date(p_fecha_alta, 'dd/mm/rrrr'),
        id_tipo_documento_cdd = p_id_tipo_documento_cdd
      where
        id_tramite_ipj = p_id_tramite_ipj and
        id_legajo = p_id_legajo and
        id_documento = p_id_documento;

      if sql%rowcount = 0 then
        insert into ipj.t_entidades_documentos (id_tramite_ipj, id_legajo, id_documento,
          n_documento, observacion, cuil_usr, fecha_alta, id_tipo_documento_cdd)
        values
          (p_id_tramite_ipj, p_id_legajo, p_id_documento, p_n_documento,
           p_observacion, p_cuil_usr, to_date(p_fecha_alta, 'dd/mm/rrrr'), p_id_tipo_documento_cdd);
      end if;
    end if;

    o_rdo := 'OK';
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     WHEN OTHERS THEN
       o_rdo := 'Guardar Documentos: ' || To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Guardar_Entidad_Docs;

  PROCEDURE SP_Traer_Entidad_Autoriz_Boe(
    o_Cursor OUT types.cursorType,
    p_Id_Tramite_Ipj in number)
  IS
   /**********************************************************
    Lista las personas autorizadas BOE para un tramite
  **********************************************************/
  BEGIN
    OPEN o_Cursor FOR
    select Ea.Id_Tramite_Ipj, Ea.Id_Integrante, I.DETALLE,
           I.Id_Sexo, I.Id_Numero, I.Nro_Documento, I.Pai_Cod_Pais, nvl(i.cuil, p.cuil) cuil,
           IPJ.VARIOS.FC_Buscar_Mail(I.Id_Sexo || I.Nro_Documento || I.Pai_Cod_Pais || to_char(I.Id_Numero), IPJ.Types.c_id_Aplicacion) Mail,
           IPJ.VARIOS.FC_Buscar_Telefono(I.Id_Sexo || I.Nro_Documento || I.Pai_Cod_Pais || to_char(I.Id_Numero), IPJ.Types.c_id_Aplicacion) Telefono
      from IPJ.T_TRAMITESIPJ_AUTORIZ_BOE ea join ipj.t_integrantes i
          on EA.ID_INTEGRANTE = i.id_integrante
        join rcivil.vt_pk_persona p
          on p.id_sexo = i.id_sexo and p.nro_documento = i.nro_documento and p.pai_cod_pais = i.pai_cod_pais and p.id_numero = i.id_numero
     WHERE EA.Id_Tramite_Ipj = p_Id_Tramite_Ipj and
           ea.fecha_baja is NULL
    UNION ALL
    select Ea.Id_Tramite_Ipj, Ea.Id_Integrante, I.DETALLE,
           I.Id_Sexo, I.Id_Numero, I.Nro_Documento, I.Pai_Cod_Pais, nvl(i.cuil, p.cuil) cuil,
           IPJ.VARIOS.FC_Buscar_Mail(I.Id_Sexo || I.Nro_Documento || I.Pai_Cod_Pais || to_char(I.Id_Numero), IPJ.Types.c_id_Aplicacion) Mail,
           IPJ.VARIOS.FC_Buscar_Telefono(I.Id_Sexo || I.Nro_Documento || I.Pai_Cod_Pais || to_char(I.Id_Numero), IPJ.Types.c_id_Aplicacion) Telefono
      from IPJ.T_TRAMITESIPJ_AUTORIZ_BOE ea join ipj.t_integrantes i
          on EA.ID_INTEGRANTE = i.id_integrante
        join rcivil.vt_pk_persona p
          on p.id_sexo = i.id_sexo and p.nro_documento = i.nro_documento and p.pai_cod_pais = i.pai_cod_pais and p.id_numero = i.id_numero
        JOIN ipj.t_tramitesipj tra
          ON ea.id_tramite_ipj = tra.id_tramite_ipj
     WHERE tra.Id_Tramite_Ipj_padre = p_Id_Tramite_Ipj and
           ea.fecha_baja is NULL;

  END SP_Traer_Entidad_Autoriz_Boe;

  PROCEDURE SP_Guardar_Entidad_Autoriz_Boe(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_id_integrante in number,
    p_eliminar in number -- 1 Elimina
    )
  IS
    v_valid_parametros varchar2(200);
    v_id_integrante number;
    v_existe number;
  BEGIN
  /*****************************************************
    Agrega, modifica o elimina los autorizados BOE para el tr�mite.
    No maneja transaccion, ya que se usa en cascada desde la aplicacion y lo administra el codigo.
  ******************************************************/

    -- CUERPO DEL PROCEDIMEINTO
    if p_eliminar = 1 then
      update ipj.t_tramitesipj_autoriz_boe
      set fecha_baja = to_date(sysdate, 'dd/mm/rrrr')
      where
        Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        id_integrante = p_id_integrante;

    else
      -- verifico si ya existe y no hago nada
      update ipj.t_tramitesipj_autoriz_boe
      set
        fecha = to_date(sysdate, 'dd/mm/rrrr'),
        fecha_baja = null
      where
        Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        id_integrante = p_id_integrante;

       -- si no existe, lo agrego
      if sql%rowcount = 0 then
        INSERT INTO IPJ.T_TRAMITESIPJ_AUTORIZ_BOE (Id_Tramite_Ipj, Id_Integrante, fecha, fecha_baja)
        VALUES (p_Id_Tramite_Ipj, p_id_integrante, to_date(sysdate, 'dd/mm/rrrr'),
          null);

      end if;
    end if;

    o_rdo := TYPES.c_Resp_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
    WHEN OTHERS THEN
       o_rdo := 'SP_Guardar_Entidad_Autoriz_Boe: ' || To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Guardar_Entidad_Autoriz_Boe;


  PROCEDURE SP_Traer_Entidad_Edicto(
    o_Cursor OUT types.cursorType,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number)
  IS
   /**********************************************************
    Muestra el �ltimo edicto cargado para la empresa:
     - Si existe un edico pendiente o enviado, se muestra ese
     - Si no existe uno pendiente o enviado, se muestra el m�ximo edicto (el ultimo?)
  **********************************************************/
    v_existe_pendiente number;
    v_existe number;
    v_cuit varchar2(20);
  BEGIN
    -- Obtengo el CUIT de la empresa, del legajo o de la entidad del tr�mite
    select nvl(l.cuit, e.cuit) into v_cuit
    from ipj.t_entidades e join ipj.t_legajos l
      on e.id_legajo = l.id_legajo
    where
      e.id_tramite_ipj = p_id_tramite_ipj and
      e.id_legajo = p_id_legajo;

    -- Veo si existe alg�n edicto pendiente, y muestro siempre ese
    select count(1) into v_existe_pendiente
    from IPJ.T_ENTIDADES_EDICTO_BOE eb
    where
      eb.Id_Tramite_Ipj = p_Id_Tramite_Ipj and
      eb.id_legajo = p_id_legajo and
      eb.id_estado_boe in (1, 2);-- Pendiente IPJ o Enviado

    if v_existe_pendiente = 0 then
      -- Veo si existe alg�n edicto
      select count(1) into v_existe
      from IPJ.T_ENTIDADES_EDICTO_BOE eb
      where
        eb.Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        eb.id_legajo = p_id_legajo;

      if v_existe > 0 then
        -- Si no hay pendientes, traigo el maximo edicto
        OPEN o_Cursor FOR
          select id_tramite_ipj, id_legajo, nro_edicto, to_char(fecha_envio, 'dd/mm/rrrr') fecha_envio,
            to_char(fecha_cancela, 'dd/mm/rrrr') fecha_cancela, to_char(fecha_publica, 'dd/mm/rrrr') fecha_publica,
            id_estado_boe, enviar_mail, v_cuit CuitSociedad
          from
            ( select eb.id_tramite_ipj, eb.id_legajo, eb.nro_edicto, eb.fecha_envio,
                eb.fecha_cancela, eb.fecha_publica, eb.id_estado_boe, eb.enviar_mail
              from IPJ.T_ENTIDADES_EDICTO_BOE eb
              where
                eb.Id_Tramite_Ipj = p_Id_Tramite_Ipj and
                eb.id_legajo = p_id_legajo
              order by eb.nro_edicto desc
            )
          where
            rownum = 1;
      else
        -- Si no hay nada, muestro un registro solo con el cuit
        OPEN o_Cursor FOR
          select p_id_tramite_ipj id_tramite_ipj, p_id_legajo id_legajo, null nro_edicto,
            null fecha_envio, null fecha_cancela, null fecha_publica, null id_estado_boe,
            null enviar_mail, v_cuit CuitSociedad
          from dual;
      end if;
    else
      -- Si hay pendientes, siempre muestra ese
      OPEN o_Cursor FOR
        select eb.id_tramite_ipj, eb.id_legajo, eb.nro_edicto, to_char(eb.fecha_envio, 'dd/mm/rrrr') fecha_envio,
          to_char(eb.fecha_cancela, 'dd/mm/rrrr') fecha_cancela, to_char(eb.fecha_publica, 'dd/mm/rrrr') fecha_publica,
          eb.id_estado_boe, eb.enviar_mail, v_cuit CuitSociedad
        from IPJ.T_ENTIDADES_EDICTO_BOE eb
        where
          eb.Id_Tramite_Ipj = p_Id_Tramite_Ipj and
          eb.id_legajo = p_id_legajo and
          eb.id_estado_boe in (1, 2); -- Pendiente IPJ o Enviado
    end if;

  END SP_Traer_Entidad_Edicto;

  PROCEDURE SP_Guardar_Envio_Edicto(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_cant_edictos out number,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number,
    p_nro_edicto in number,
    p_fecha_envio in varchar2, -- es fecha
    p_enviar_mail in char
  )
  IS
   /**********************************************************
    Agrega un nuevo envio de edicto:
    - El Nro Edicto debe ser distinto de 0
    - Si no se pasa fecha, se pone la fecha de hoy
    - Se agrega en estado Enviado, y sin mail pendiente
    - Si el edicto es nuevo y Boe le dio Nro Edicto, pongo las acciones del tr�mite en estado Pendiente Boe
  **********************************************************/
   v_valid_parametros varchar2(200);
   v_mail_edicto char(1);
  BEGIN
    if IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('HAB_LOG_BOE') = '1' then
      IPJ.VARIOS.SP_GUARDAR_LOG(
        p_METODO => 'SP_Guardar_Envio_Edicto',
        p_NIVEL => 'Edicto BOE',
        p_ORIGEN => 'DB',
        p_USR_LOG  => 'DB',
        p_USR_WINDOWS => 'Sistema',
        p_IP_PC  => null,
        p_EXCEPCION =>
           'Id Tramite Ip = ' || to_char(p_Id_Tramite_Ipj)
           || ' / Id Legajo = ' || to_char(p_id_legajo)
           || ' / Nro Edicto = ' || to_char(p_nro_edicto)
           || ' / Fecha Envio = ' || p_fecha_envio
           || ' / Enviar Mail = ' || p_enviar_mail
      );
    end if;

    -- VALIDACION DE PARAMETROS
    if p_fecha_envio is not null and IPJ.VARIOS.Valida_Fecha(p_fecha_envio) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT');
    end if;

    -- Si los parametros no estan OK, no continuo
    if v_valid_parametros is not null then
      o_rdo := 'ERROR de Par�metros: ' || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      return;
    end if;

    -- Busco si el edicto tenia pendiente el envio de mail
    begin
      select enviar_mail into v_mail_edicto
      from ipj.t_entidades_edicto_boe
      where
        id_tramite_ipj = p_id_tramite_ipj and
        id_legajo = p_id_legajo and
        nro_edicto = p_nro_edicto;
    exception
      when NO_DATA_FOUND then
        v_mail_edicto := 'N';
    end;

    -- Si cambia el aviso de mail, solo actualizo eso
    if v_mail_edicto <> p_enviar_mail then
      update ipj.t_entidades_edicto_boe
      set
        enviar_mail = p_enviar_mail
      where
        id_tramite_ipj = p_id_tramite_ipj and
        id_legajo = p_id_legajo and
        nro_edicto = p_nro_edicto;

    else
      -- Trato de actualizar el edicto pendiente si existe
      update ipj.t_entidades_edicto_boe
      set
        nro_edicto = nvl(p_nro_edicto, 0),
        fecha_envio = nvl(to_date(p_fecha_envio, 'dd/mm/rrrr'), to_date(sysdate, 'dd/mm/rrrr')),
        id_estado_boe = (case when nvl(p_nro_edicto, 0) = 0 then 1 else 2 end)
      where
        id_tramite_ipj = p_id_tramite_ipj and
        id_legajo = p_id_legajo and
        id_estado_boe in (1, 2); -- Pendiente IPJ o BOE

      -- Si no existe un edicto pendiente, agrego uno nuevo
      if sql%rowcount = 0 then
        insert into ipj.t_entidades_edicto_boe (id_tramite_ipj, id_legajo, nro_edicto,
          fecha_envio, fecha_cancela, fecha_publica, id_estado_boe, enviar_mail)
        values (p_id_tramite_ipj, p_id_legajo, nvl(p_nro_edicto, 0),
          nvl(to_date(p_fecha_envio, 'dd/mm/rrrr'), to_date(sysdate, 'dd/mm/rrrr')), null, null,
          (case when nvl(p_nro_edicto, 0) = 0 then 1 else 2 end),-- Si viene edicto lo cargo pendiente Boe, sino pendiente IPJ
          'N');
      end if;

      -- Si el edicto se envi� a BOE, marco todas las acciones como pendientes de BOE, para deshabilitar sus opciones
      if nvl(p_nro_edicto, 0) > 0 then
        update ipj.t_tramitesipj_acciones t
        -- pendiente boe. Si es acci�n de edicto, pongo el estado nuevo para poder habilitar bot�n abrir
        /*############DESA##############*/
        -- set t.id_estado = decode(t.id_tipo_accion,113,18,174,18,IPJ.TYPES.C_ESTADOS_BOE)
        /*############PROD##############*/
        set t.id_estado = decode(t.id_tipo_accion,117,18,174,18,IPJ.TYPES.C_ESTADOS_BOE)
        where
          t.id_tramite_ipj = p_id_tramite_ipj and
          t.id_estado < IPJ.TYPES.C_ESTADOS_COMPLETADO ;
      end if;
    end if;

    -- Cuento cuantos edictos hay
    select count(1) into o_cant_edictos
    from ipj.t_entidades_edicto_boe
    where
      id_tramite_ipj = p_id_tramite_ipj and
      id_legajo = p_id_legajo;

    commit;
    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
    WHEN OTHERS THEN
       o_rdo := 'SP_Guardar_Envio_Edicto: ' || To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Guardar_Envio_Edicto;

  PROCEDURE SP_Act_Autorizado_Dom_Elec(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_integrante IN ipj.t_integrantes.id_integrante%TYPE,
    p_id_tramite_ipj IN ipj.t_entidades_autorizados.id_tramite_ipj%TYPE)
  IS
   /**********************************************************
    Marca cu�l autorizado es el Domicilio Electr�nico
  **********************************************************/
  BEGIN
    --Actualizo el mail autorizado, si es el integrante que viene por par�metro se marca en S, si no en N
    update ipj.t_entidades_autorizados ea
    set ea.mail_autorizado = decode(p_id_integrante, ea.id_integrante,'S','N')
    where
      ea.id_tramite_ipj = p_id_tramite_ipj;

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     WHEN OTHERS THEN
       o_rdo := 'SP_Act_Autorizado_Dom_Elec: ' || To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Act_Autorizado_Dom_Elec;

  PROCEDURE SP_Act_Entidad_Anexados(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_sede IN ipj.t_entidades.id_sede%TYPE,
    p_cuit IN ipj.t_entidades.cuit%TYPE,
    p_id_moneda IN ipj.t_entidades.id_moneda%TYPE,
    p_id_unidad_med IN ipj.t_entidades.id_unidad_med%TYPE,
    p_matricula IN ipj.t_entidades.matricula%TYPE,
    p_fecha_inscripcion IN ipj.t_entidades.fec_inscripcion%TYPE,
    p_folio IN ipj.t_entidades.folio%TYPE,
    p_libro_nro IN ipj.t_entidades.libro_nro%TYPE,
    p_tomo IN ipj.t_entidades.tomo%TYPE,
    p_anio IN ipj.t_entidades.anio%TYPE,
    p_desc_obj_social IN ipj.t_entidades.objeto_social%TYPE,
    p_monto IN ipj.t_entidades.monto%TYPE,
    p_acta_constitutiva IN ipj.t_entidades.acta_contitutiva%TYPE,
    p_vigencia IN ipj.t_entidades.vigencia%TYPE,
    p_cierre_fin_mes IN ipj.t_entidades.cierre_fin_mes%TYPE,
    p_fec_vig IN ipj.t_entidades.fec_vig%TYPE,
    p_id_vin_comercial IN ipj.t_entidades.id_vin_comercial%TYPE,
    p_id_vin_real IN ipj.t_entidades.id_vin_real%TYPE,
    p_borrador IN ipj.t_entidades.borrador%TYPE,
    p_fecha_acto IN ipj.t_entidades.fec_acto%TYPE,
    p_denominacion_1 IN ipj.t_entidades.denominacion_1%TYPE,
    p_n_Sede IN ipj.t_entidades.denominacion_2%TYPE,
    p_alta_temporal IN ipj.t_entidades.alta_temporal%TYPE,
    p_tipo_vigencia IN ipj.t_entidades.tipo_vigencia%TYPE,
    p_valor_cuota IN ipj.t_entidades.valor_cuota%TYPE,
    p_matricula_version IN ipj.t_entidades.matricula_version%TYPE,
    p_cuotas IN ipj.t_entidades.cuotas%TYPE,
    p_id_tipo_admin IN ipj.t_entidades.id_tipo_administracion%TYPE,
    p_id_tipo_entidad IN ipj.t_entidades.id_tipo_entidad%TYPE,
    p_baja_temporal IN ipj.t_entidades.baja_temporal%TYPE,
    p_id_estado_entidad IN ipj.t_entidades.id_estado_entidad%TYPE,
    p_obs_cierre IN ipj.t_entidades.obs_cierre%TYPE,
    p_Nro_Resolucion IN ipj.t_entidades.nro_resolucion%TYPE,
    p_Fec_Resolucion IN ipj.t_entidades.fec_resolucion%TYPE,
    p_Nro_Boletin IN ipj.t_entidades.nro_boletin%TYPE,
    p_Fec_Boletin IN ipj.t_entidades.fec_boletin%TYPE,
    p_Nro_Registro IN ipj.t_entidades.nro_registro%TYPE,
    p_Sede_Estatuto IN ipj.t_entidades.sede_estatuto%TYPE,
    p_Requiere_Sindico IN ipj.t_entidades.requiere_sindico%TYPE,
    p_Origen IN ipj.t_entidades.origen%TYPE,
    p_Es_Sucursal IN ipj.t_entidades.es_sucursal%TYPE,
    p_Observacion IN ipj.t_entidades.observacion%TYPE,
    p_Id_Tipo_Origen IN ipj.t_entidades.id_tipo_origen%TYPE,
    p_cierre_ejercicio IN ipj.t_entidades.cierre_ejercicio%TYPE, --cierre ejercicio
    p_fiscalizacion_ejerc IN ipj.t_entidades.fiscalizacion_ejerc%TYPE,
    p_incluida_lgs IN ipj.t_entidades.incluida_lgs%TYPE,
    p_condicion_fideic IN ipj.t_entidades.condicion_fideic%TYPE,
    p_obs_fiduciario IN ipj.t_entidades.obs_fiduciario%TYPE,
    p_obs_fiduciante IN ipj.t_entidades.obs_fiduciante%TYPE,
    p_obs_beneficiario IN ipj.t_entidades.obs_beneficiario%TYPE,
    p_obs_fideicomisario IN ipj.t_entidades.obs_fideicomisario%TYPE,
    p_fecha_versionado IN ipj.t_entidades.fecha_versionado%TYPE,
    p_sin_denominacion IN ipj.t_entidades.sin_denominacion%TYPE,
    p_contrato_privado IN ipj.t_entidades.contrato_privado%TYPE,
    p_cant_Fiduciario IN ipj.t_entidades.cant_fiduciario%TYPE,
    p_cant_Fiduciante IN ipj.t_entidades.cant_fiduciante%TYPE,
    p_cant_Beneficiario IN ipj.t_entidades.cant_beneficiario%TYPE,
    p_cant_Fideicomisario IN ipj.t_entidades.cant_fideicomisario%TYPE,
    p_fec_vig_hasta IN ipj.t_entidades.fec_vig_hasta%TYPE,
    p_id_tramite_ipj IN ipj.t_entidades.id_tramite_ipj%TYPE,
    p_id_legajo IN ipj.t_entidades.id_legajo%TYPE,
    p_acredita_grupo IN ipj.t_entidades.acredita_grupo%TYPE)
  IS
  /**********************************************************
    Actualiza los datos de la entidad del tr�mite padre con los datos del anexado
  **********************************************************/
  BEGIN
    UPDATE ipj.t_entidades e
       SET e.id_sede = p_id_sede,
           e.cuit = p_cuit,
           e.id_moneda = p_id_moneda,
           e.id_unidad_med = p_id_unidad_med,
           e.matricula = p_matricula,
           e.fec_inscripcion = p_fecha_inscripcion,
           e.folio = p_folio,
           e.libro_nro = p_libro_nro,
           e.tomo = p_tomo,
           e.anio = p_anio,
           e.objeto_social = p_desc_obj_social,
           e.monto = p_monto,
           e.acta_contitutiva = p_acta_constitutiva,
           e.vigencia = p_vigencia,
           e.cierre_fin_mes = p_cierre_fin_mes,
           e.fec_vig = p_fec_vig,
           e.id_vin_comercial = p_id_vin_comercial,
           e.id_vin_real = p_id_vin_real,
           e.borrador = p_borrador,
           e.fec_acto = p_fecha_acto,
           e.denominacion_1 = p_denominacion_1,
           e.denominacion_2 = p_n_Sede,
           e.alta_temporal = p_alta_temporal,
           e.tipo_vigencia = p_tipo_vigencia,
           e.valor_cuota = p_valor_cuota,
           e.matricula_version = p_matricula_version,
           e.cuotas = p_cuotas,
           e.id_tipo_administracion = p_id_tipo_admin,
           e.id_tipo_entidad = p_id_tipo_entidad,
           e.baja_temporal = p_baja_temporal,
           e.id_estado_entidad = p_id_estado_entidad,
           e.obs_cierre = p_obs_cierre,
           e.nro_resolucion = p_Nro_Resolucion,
           e.fec_resolucion = p_Fec_Resolucion,
           e.nro_boletin = p_Nro_Boletin,
           e.fec_boletin = p_Fec_Boletin,
           e.nro_registro = p_Nro_Registro,
           e.sede_estatuto = p_Sede_Estatuto,
           e.requiere_sindico = p_Requiere_Sindico,
           e.origen = p_Origen,
           e.es_sucursal = p_Es_Sucursal,
           e.observacion = p_Observacion,
           e.id_tipo_origen = p_Id_Tipo_Origen,
           e.cierre_ejercicio = p_cierre_ejercicio,
           e.fiscalizacion_ejerc = p_fiscalizacion_ejerc,
           e.incluida_lgs = p_incluida_lgs,
           e.condicion_fideic = p_condicion_fideic,
           e.obs_fiduciario = p_obs_fiduciario,
           e.obs_fiduciante = p_obs_fiduciante,
           e.obs_beneficiario = p_obs_beneficiario,
           e.obs_fideicomisario = p_obs_fideicomisario,
           e.fecha_versionado = p_fecha_versionado,
           e.sin_denominacion = p_sin_denominacion,
           e.contrato_privado = p_contrato_privado,
           e.cant_fiduciario = p_cant_Fiduciario,
           e.cant_fiduciante = p_cant_Fiduciante,
           e.cant_beneficiario = p_cant_Beneficiario,
           e.cant_fideicomisario = p_cant_Fideicomisario,
           e.fec_vig_hasta = p_fec_vig_hasta
     WHERE e.id_tramite_ipj = p_id_tramite_ipj
       AND e.id_legajo = p_id_legajo;

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     WHEN OTHERS THEN
       o_rdo := 'SP_Act_Entidad_Anexados: ' || To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Act_Entidad_Anexados;

  PROCEDURE SP_Buscar_Entidad_Informe(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_Id_Tramite_Ipj out number,
    o_id_tramiteipj_accion out number,
    o_id_documento out number,
    o_firmado out number, -- 0 Sin firma / >0 Con Firma
    p_id_legajo in number)
  IS
   /*****************************************************************
    Busca el �ltimo informe que se genero en IPJ para la empresa indicada.
  ******************************************************************/
    v_id_ubicacion number;
  BEGIN
     -- Busco el �rea de la empresa
     select id_ubicacion into v_id_ubicacion
     from ipj.t_legajos l join ipj.t_tipos_entidades te
       on l.id_tipo_entidad = te.id_tipo_entidad
     where
       l.id_legajo = p_id_legajo;

     /**** PARA CADA AREA DEFINO QUE BUSCO Y COMO ******/
     -- EN ACyF, veo si existe al�n informe
    if v_id_ubicacion = IPJ.TYPES.C_AREA_CYF then
      -- Busco si hay alg�n informe generado
      begin
        select tmp.id_tramite_ipj, tmp.id_tramiteipj_accion, tmp.id_documento
          into o_id_tramite_ipj, o_id_tramiteipj_accion, o_id_documento
        from
          ( select tr.id_tramite_ipj, tra.id_tramiteipj_accion, tra.id_documento
            from ipj.t_tramitesipj tr join ipj.t_tramitesipj_acciones tra
                on tr.id_tramite_ipj = tra.id_tramite_ipj
            where
              tr.id_estado_ult between 100 and 199 and
              tra.id_legajo = p_id_legajo and
              tra.id_tipo_accion = 48 and -- Informe de Autoridades
              tra.id_documento > 0
              --and nvl(tr.id_tramite, 0) > 0
            order by  nvl(TR.FECHA_INI_SUAC, TR.FECHA_INICIO) desc
          ) tmp
        where
          rownum = 1;

        -- Busco si el informe paso por el estado de firma digital
        select count(1) into o_firmado
        from IPJ.T_TRAMITESIPJ_ACCIONES_ESTADO ae
        where
          ae.id_tramite_ipj = o_id_tramite_ipj and
          ae.id_tramiteipj_accion = o_id_tramiteipj_accion and
          ae.id_estado = 10; -- Pendiente de Firma

        o_rdo := IPJ.TYPES.C_RESP_OK;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
      exception
        when OTHERS then
--          o_id_tramite_ipj := 0;
--          o_id_tramiteipj_accion := 0;
--          o_id_documento := 0;
--          o_firmado := 0;
            o_rdo := 'La entidad indicada no se encuentra digitalizada.';
            o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
            o_id_tramite_ipj := 0;
            o_id_tramiteipj_accion := 0;
            o_id_documento := 0;
            o_firmado := 0;
      end;

      -- Si no hay informes, busco el �ltimo tr�mite para usar
      /*
      if o_id_tramite_ipj = 0 then
        begin
          select Id_Tramite_Ipj, id_tramiteipj_accion into o_Id_Tramite_Ipj, o_id_tramiteipj_accion
          from
            ( select e.Id_Tramite_Ipj, nvl(tacc.id_tramiteipj_accion, 0) id_tramiteipj_accion
              from ipj.t_entidades e join ipj.t_tramitesipj tr
                  on e.Id_Tramite_Ipj = tr.Id_Tramite_Ipj
                left join IPJ.t_TramitesIPJ_Acciones tacc
                  on tacc.id_legajo = e.id_legajo and tacc.Id_Tramite_Ipj = e.Id_Tramite_Ipj
              where
                e.id_legajo = p_id_legajo and
                nvl(tr.id_estado_ult, 0) < IPJ.TYPES.C_ESTADOS_RECHAZADO and
                nvl(tr.id_estado_ult, 0) >= IPJ.TYPES.C_ESTADOS_COMPLETADO
              order by nvl(tr.id_proceso, 0) asc, e.matricula_version desc, tacc.Id_Tramite_Ipj desc, tacc.id_tramiteipj_accion desc
            )
          where rownum = 1;

          o_rdo := IPJ.TYPES.C_RESP_OK;
          o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
        exception
          when OTHERS then
            o_rdo := 'La entidad indicada no se encuentra digitalizada.';
            o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
            o_id_tramite_ipj := 0;
            o_id_tramiteipj_accion := 0;
            o_id_documento := 0;
            o_firmado := 0;
        end;
      end if;
      */
    end if;

    -- EN SRL, veo si existe alg�n informe de subsistencia completo
    if v_id_ubicacion = IPJ.TYPES.C_AREA_SRL then
       o_firmado := 0;
      -- Busco si hay alg�n informe generado
      begin
        select tmp.id_tramite_ipj, tmp.id_tramiteipj_accion, tmp.id_documento
          into o_id_tramite_ipj, o_id_tramiteipj_accion, o_id_documento
        from
          ( select tr.id_tramite_ipj, tra.id_tramiteipj_accion, tra.id_documento
            from ipj.t_tramitesipj tr join ipj.t_tramitesipj_acciones tra
                on tr.id_tramite_ipj = tra.id_tramite_ipj
            where
              tr.id_estado_ult between 100 and 199 and
              tra.id_legajo = p_id_legajo and
              tra.id_tipo_accion = 12 and -- Subsistencia Completo S.R.L.
              tra.id_documento > 0
            order by TR.FECHA_INI_SUAC desc
          ) tmp
        where
          rownum = 1;

        o_rdo := IPJ.TYPES.C_RESP_OK;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
      exception
        when NO_DATA_FOUND then
          o_rdo := 'La entidad indicada no se encuentra digitalizada.';
          o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
          o_id_tramite_ipj := 0;
          o_id_tramiteipj_accion := 0;
          o_id_documento := 0;
          o_firmado := 0;
      end;
    end if;

    --EN SxA, veo si existe alg�n informe de subsistencia completo
    if v_id_ubicacion = IPJ.TYPES.C_AREA_SXA  then
       o_firmado := 0;
      -- Busco si hay alg�n informe generado
      begin
        select tmp.id_tramite_ipj, tmp.id_tramiteipj_accion, tmp.id_documento
          into o_id_tramite_ipj, o_id_tramiteipj_accion, o_id_documento
        from
          ( select tr.id_tramite_ipj, tra.id_tramiteipj_accion, tra.id_documento
            from ipj.t_tramitesipj tr join ipj.t_tramitesipj_acciones tra
                on tr.id_tramite_ipj = tra.id_tramite_ipj
            where
              tr.id_estado_ult between 100 and 199 and
              tra.id_legajo = p_id_legajo and
              tra.id_tipo_accion = 126 and -- Subsistencia Completo S.A.
              tra.id_documento > 0
            order by TR.FECHA_INI_SUAC desc
          ) tmp
        where
          rownum = 1;

        o_rdo := IPJ.TYPES.C_RESP_OK;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
      exception
        when NO_DATA_FOUND then
          o_rdo := 'La entidad indicada no se encuentra digitalizada.';
          o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
          o_id_tramite_ipj := 0;
          o_id_tramiteipj_accion := 0;
          o_id_documento := 0;
          o_firmado := 0;
      end;
    end if;

  END SP_Buscar_Entidad_Informe;

  PROCEDURE SP_GUARDAR_ENTIDAD_CUIT(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number,
    p_cuit in varchar2)
  IS
    v_cuit varchar2(30);
    v_valid_parametros varchar2(2000);
    v_mensaje varchar2(2000);
    v_existe_gobierno number;
    v_row_entidad ipj.t_entidades%rowtype;
    v_PersJur_NewSede varchar2(20);
    v_result_PersJur varchar2(2000);
  BEGIN
  /***********************************************************
    Valida que el CUIT sea correcto, y lo guarda en la entidad del tr�mite y el legajo
  ************************************************************/
    -- VALIDACION DE PARAMETROS
    if p_cuit is not null then
      v_cuit := replace(p_cuit, '-', '');
      t_comunes.pack_persona_juridica.VerificarCUIT (v_cuit, v_mensaje);
      if Upper(trim(v_mensaje)) <> 'OK' then --La validacion de CUIT responde siempre CUIL
        v_valid_parametros := v_valid_parametros || replace(v_mensaje, 'CUIL', 'CUIT') || '. ';
      end if;
    else
      o_rdo := 'El CUIT no puede ser nulo.';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      return;
    end if;

    -- si los parametros no estan OK, no continuo
    if v_valid_parametros is not null then
      o_rdo := v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      return;
    end if;

    -- Actualizo el cuit en el tr�mite
    update ipj.t_entidades
    set
      cuit = v_cuit
    where
      Id_Tramite_Ipj = p_Id_Tramite_Ipj and
      id_legajo = p_id_legajo;

    -- Actualizo el cuit en el legajo
    update ipj.t_legajos
    set cuit = v_cuit
    where
      id_legajo = p_id_legajo;

    -- Marco la fecha del asignaci�n del CUIT por AFIP
    update ipj.t_tramitesipj
    set asigna_cuit_afip = to_date(sysdate, 'dd/mm/rrrr')
    where
      id_tramite_ipj = p_id_tramite_ipj;

    -- Verifico si la existe en gobierno una entidad con ese cuit
    select count(*) into v_existe_gobierno
    from T_COMUNES.vt_pers_juridicas_completa
    where
      cuit = v_cuit and
      id_sede = nvl(v_Row_Entidad.id_sede, '00');

    -- Si no existe una empresa, la creo
    if v_existe_gobierno = 0 then
      -- Busco los datos de la entidad
      select * into v_row_entidad
      from ipj.t_entidades
      where
        Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        id_legajo = p_id_legajo;

      -- La cargo en Gobierno
      T_COMUNES.pack_persona_juridica.INSERTA_PERSJUR_UNIF_VERT(
        P_CUIT => v_Row_Entidad.Cuit,
        P_RAZON_SOCIAL => v_Row_Entidad.Denominacion_1,
        P_NOM_FAN => null,
        P_ID_FOR_JUR => null,
        P_ID_COND_IVA => null,
        P_ID_APLICACION => IPJ.Types.c_id_Aplicacion ,
        P_FEC_INICIO_ACT => nvl(v_Row_Entidad.Alta_Temporal, to_date(v_Row_Entidad.Acta_Contitutiva, 'dd/mm/rrrr')),
        P_NRO_ING_BRUTO => null,
        P_ID_COND_INGBRUTO => null,
        P_ABREVIADO => null,
        P_FEC_INSCRIPCION => v_Row_Entidad.FEC_INSCRIPCION,
        P_ID_ESTADO => v_Row_Entidad.ID_ESTADO_ENTIDAD,
        P_GRAVAMEN => IPJ.ENTIDAD_PERSJUR.FC_ESTADO_GRAVAMEN_PERSJUR(v_Row_Entidad.id_legajo),
        P_INTERVENCION => IPJ.ENTIDAD_PERSJUR.FC_ESTADO_GRAVAMEN_INTERV(v_Row_Entidad.id_legajo),
        P_NRO_MATRICULA => v_Row_Entidad.MATRICULA,
        P_NRO_HAB_MUNI => null,
        O_ID_SEDE => v_PersJur_NewSede,
        O_RESULTADO => v_result_PersJur
      );
    end if;

    --Verifico si fallo o no
    if nvl(v_result_PersJur, IPJ.TYPES.C_RESP_OK) <> IPJ.TYPES.C_RESP_OK then
      o_rdo:= 'No se pudo registrar la entidad en Gobierno (' || v_result_PersJur || ').';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
    else
      o_rdo := IPJ.TYPES.C_RESP_OK;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    end if;
  EXCEPTION
    WHEN OTHERS THEN
      o_rdo := 'SP_GUARDAR_ENTIDAD_CUIT :' || To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_GUARDAR_ENTIDAD_CUIT;

  PROCEDURE SP_Traer_PersJur_Docs_Adj(
    o_Cursor OUT TYPES.cursorType,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number
    )
  IS
  /*******************************************************
    Este proceso lista los documentos adjuntos a un tr�mite  de una empresda:

  ********************************************************/
  BEGIN

    OPEN o_Cursor FOR
      select id_tramite_ipj, id_legajo, id_documento, n_documento,
        fecha_verif, id_integrante, id_tipo_documento_cdd, id_estado_doc, fecha_modif,
        n_tipo_documento, n_estado_doc, id_sexo, nro_documento, pai_cod_pais, id_numero,
        detalle, expediente, sticker, fecha_ini_suac, observacion, utiliza_firma
      from
        ( select ed.id_tramite_ipj, ed.id_legajo, ed.id_documento, ed.n_documento,
            to_char(ed.fecha_verif, 'dd/mm/rrrr') fecha_verif, ed.id_integrante,
            ed.id_tipo_documento_cdd, ed.id_estado_doc, to_char(ed.fecha_modif, 'dd/mm/rrrr') fecha_modif,
            (select da.n_tipo_doc_adjunto from ipj.t_tipos_doc_adjuntos da where da.id_tipo_doc_adjunto = ed.id_tipo_doc_adjunto) n_tipo_documento,
            e.n_estado_doc, i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero,
            (select p.nombre||' '||p.apellido from rcivil.vt_pk_persona p where p.id_sexo = i.id_sexo and p.nro_documento = i.nro_documento and p.pai_cod_pais = i.pai_cod_pais and p.id_numero = i.id_numero) detalle,
            tr.expediente, tr.sticker, to_char(tr.fecha_ini_suac, 'dd/mm/rrrr') fecha_ini_suac,
            ed.observacion, ed.utiliza_firma
          from ipj.t_entidades_doc_adjuntos ed join ipj.t_tramitesipj tr
              on ed.id_tramite_ipj = tr.id_tramite_ipj
            left join ipj.t_integrantes i
              on ed.id_integrante = i.id_integrante
            left join ipj.t_tipos_estados_doc e
              on ed.id_estado_doc = e.id_estado_doc
          where
            ed.id_tramite_ipj = p_id_tramite_ipj
          UNION ALL
          select ed.id_tramite_ipj, ed.id_legajo, ed.id_documento, ed.n_documento,
            to_char(ed.fecha_verif, 'dd/mm/rrrr') fecha_verif, ed.id_integrante,
            ed.id_tipo_documento_cdd, ed.id_estado_doc, to_char(ed.fecha_modif, 'dd/mm/rrrr') fecha_modif,
            (select da.n_tipo_doc_adjunto from ipj.t_tipos_doc_adjuntos da where da.id_tipo_doc_adjunto = ed.id_tipo_doc_adjunto) n_tipo_documento,
            e.n_estado_doc, i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero,
            (select p.nombre||' '||p.apellido from rcivil.vt_pk_persona p where p.id_sexo = i.id_sexo and p.nro_documento = i.nro_documento and p.pai_cod_pais = i.pai_cod_pais and p.id_numero = i.id_numero) detalle,
            tra.expediente, tra.sticker, to_char(tra.fecha_ini_suac, 'dd/mm/rrrr') fecha_ini_suac,
            ed.observacion, ed.utiliza_firma
          from ipj.t_entidades_doc_adjuntos ed
            left join ipj.t_integrantes i
              on ed.id_integrante = i.id_integrante
            left join ipj.t_tipos_estados_doc e
              on ed.id_estado_doc = e.id_estado_doc
            join ipj.t_tramitesipj tra
              on ed.id_tramite_ipj = tra.id_tramite_ipj
          WHERE
            tra.id_tramite_ipj_padre = p_id_tramite_ipj
        ) tmp
      order by n_tipo_documento, to_date(fecha_ini_suac, 'dd/mm/rrrr') asc, id_tramite_ipj
      ;

  END SP_Traer_PersJur_Docs_Adj;

  PROCEDURE SP_Guardar_Entidad_Docs_Adj(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in number,
    p_id_legajo in number,
    p_id_documento in number,
    p_n_documento in varchar2,
    p_fecha_verif in varchar2, -- Es Fecha
    p_id_integrante in number,
    p_id_tipo_documento_cdd in number,
    p_id_estado_doc in number,
    p_fecha_modif in varchar2, -- Es Fecha
    p_observacion in varchar2,
    p_eliminar in number, -- 1 Elimina
    p_utiliza_firma in number)
  IS
  /*******************************************************
    Guarda un documento adjunto asociado a un tramite y empresa.
    Si el tr�mite ya existe, solo se actualiza el estado y la fecha.
  ********************************************************/
    v_valid_parametros varchar2(1000);
  BEGIN

    -- VALIDACION DE PARAMETROS
    if nvl(p_id_tramite_ipj, 0) = 0 or nvl(p_id_legajo, 0) = 0 or nvl(p_id_documento, 0)= 0 then
      v_valid_parametros := 'Se debe asociar el documento a una empresa en un tr�mite.';
    end if;

    if p_eliminar <> 1 and IPJ.VARIOS.Valida_Fecha(p_fecha_modif) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT');
    end if;

    if p_id_documento <= 0 then
      v_valid_parametros := v_valid_parametros || 'El documento no se guard� en CDD.';
    end if;

    if v_valid_parametros is not null then
      o_rdo := 'GUARDAR DOCS - Par�metros: '  || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
    end if;

    /*********************************************************
      CUERPO DEL PROCEDIMEINTO
    *********************************************************/
    if nvl(p_eliminar, 0) = 1 then
      delete ipj.t_entidades_doc_adjuntos
      where
        id_tramite_ipj = p_id_tramite_ipj and
        id_legajo = p_id_legajo and
        id_documento = p_id_documento;

    else
      update ipj.t_entidades_doc_adjuntos
      set
        fecha_modif = to_date(nvl(p_fecha_modif, sysdate), 'dd/mm/rrrr'),
        id_estado_doc = p_id_estado_doc,
        observacion = p_observacion,
        utiliza_firma = p_utiliza_firma
      where
        id_tramite_ipj = p_id_tramite_ipj and
        id_legajo = p_id_legajo and
        id_documento = p_id_documento;

      if sql%rowcount = 0 then
        insert into ipj.t_entidades_doc_adjuntos (id_tramite_ipj, id_legajo, id_documento,
          n_documento, fecha_verif, id_integrante, id_tipo_documento_cdd,
          id_estado_doc, fecha_modif, observacion, utiliza_firma)
        values
          (p_id_tramite_ipj, p_id_legajo, p_id_documento, p_n_documento,
          to_date(p_fecha_verif, 'dd/mm/rrrr'), decode(p_id_integrante, 0, null, p_id_integrante),
          p_id_tipo_documento_cdd, p_id_estado_doc, to_date(nvl(p_fecha_modif, sysdate), 'dd/mm/rrrr'),
          p_observacion, p_utiliza_firma);
      end if;
    end if;

    o_rdo := 'OK';
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     WHEN OTHERS THEN
       o_rdo := 'SP_Guardar_Entidad_Docs_Adj: ' || To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Guardar_Entidad_Docs_Adj;


  PROCEDURE SP_Act_Datos_Resolucion(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj IN ipj.t_entidades.id_tramite_ipj%TYPE,
    p_Id_Legajo IN ipj.t_entidades.id_legajo%TYPE,
    p_Matricula IN ipj.t_entidades.matricula%TYPE,
    p_Letra IN VARCHAR2,
    p_Matricula_Version IN ipj.t_entidades.matricula_version%TYPE,
    p_Fecha_Versionado IN VARCHAR2,
    p_Nro_resolucion IN ipj.t_entidades.nro_resolucion%TYPE,
    p_Fecha_Resolucion IN VARCHAR2,
    p_Cuit IN VARCHAR2)
  IS
  v_valid_parametros varchar2(2000);
  v_matricula_exists varchar2(2000);
  v_cuit varchar2(20);
  v_mensaje varchar2(2000);
  v_id_tramiteipj_accion number;
  /**********************************************************
    Setea la fecha de resolucion en las autorizaciones  de medios mecanizados
  **********************************************************/
  BEGIN

    --  VALIDACION DE PARAMETROS
    if p_Cuit is not null then
      v_cuit := replace(p_Cuit, '-', '');
      t_comunes.pack_persona_juridica.VerificarCUIT (v_cuit, v_mensaje);
      if Upper(trim(v_mensaje)) <> 'OK' then --La validacion de CUIT responde siempre CUIL
        v_valid_parametros := v_valid_parametros || replace(v_mensaje, 'CUIL', 'CUIT') || '. ';
      end if;
    else
      v_cuit := null;
    end if;

    -- Valido que la matricula sea valida y no duplicada
    if p_Matricula is not null and p_Letra is not null then
      IPJ.VARIOS.SP_Validar_Matricula(
        o_rdo => v_matricula_exists,
        o_tipo_mensaje => o_tipo_mensaje,
        p_Id_Tramite_Ipj => p_Id_Tramite_Ipj,
        p_id_legajo => p_Id_Legajo,
        p_id_tramiteipj_accion => null,
        p_cuit => v_cuit,
        p_matricula => Upper(p_letra) || p_matricula);

      if v_matricula_exists != IPJ.TYPES.C_RESP_OK then
        o_rdo := 'GUARDAR ENTIDAD - Parametros : ' || v_matricula_exists;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
        return;
      end if;
    end if;

    -- Actualizo el registro (si existe)
    UPDATE ipj.t_entidades e
       SET e.matricula = (case when p_Letra is null or p_Matricula is null then null else upper(p_Letra) || p_Matricula end)
         , e.matricula_version = p_Matricula_Version
         , e.fecha_versionado = to_date(p_Fecha_Versionado, 'dd/mm/rrrr')
         , e.nro_resolucion = p_Nro_Resolucion
         , e.fec_resolucion = to_date(p_Fecha_Resolucion, 'dd/mm/rrrr')
    WHERE e.id_tramite_ipj = p_Id_Tramite_Ipj
       AND e.id_legajo = p_Id_Legajo;

     -- Actualiza la fecha de resoluci�n en las autorizaciones de libros del tr�mite
    update IPJ.T_ENTIDADES_RUBRICA
    set fecha_tramite = to_date(p_Fecha_Resolucion, 'dd/mm/rrrr')
    where
      id_tramite_ipj = p_Id_Tramite_Ipj and
      es_autorizacion = 'S';

    --Busco la accion asociada al protocolo para actualizarlo
    begin
      select a.id_tramiteipj_accion into v_id_tramiteipj_accion
      from ipj.t_tramitesipj_acciones a join ipj.t_tipos_accionesipj acc
          on a.id_tipo_accion = acc.id_tipo_accion
      where
        id_tramite_ipj = p_id_tramite_ipj and
        acc.id_protocolo = (select id_protocolo from ipj.t_config_protocolo p where p.sigla = p_Letra) and
       rownum = 1;
    exception
      when OTHERS then
        v_id_tramiteipj_accion := 0;
    end;

    if v_id_tramiteipj_accion > 0 then
      IPJ.VARIOS.SP_Actualizar_Protocolo(
        o_rdo => o_rdo,
        o_tipo_mensaje => o_tipo_mensaje,
        p_id_tramite_ipj => p_Id_Tramite_Ipj,
        p_id_legajo => p_Id_Legajo,
        p_id_tramiteipj_accion => v_id_tramiteipj_accion,
        p_matricula => p_Letra  || p_Matricula
      );
    end if;

    if nvl(o_rdo,  IPJ.TYPES.C_RESP_OK) = IPJ.TYPES.C_RESP_OK then
      o_rdo := IPJ.TYPES.C_RESP_OK;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
      COMMIT;
    else
      rollback;
    end if;
  EXCEPTION
    WHEN OTHERS THEN
      o_rdo := 'SP_Act_Datos_Resolucion - '|| o_rdo || '-' || To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Act_Datos_Resolucion;

   PROCEDURE SP_Buscar_CUIT_Gobierno(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_denominacion out varchar2,
    p_cuit in varchar2,
    p_id_tramite_ipj in number)
  IS
    /***********************************************************
      Busca si existe una empresa por CUIT en gobierno
    ************************************************************/
    v_Row_Entidad IPJ.t_entidades%rowtype;
    v_PersJur_NewSede varchar2(10);
    v_result_PersJur varchar2(1000);
  BEGIN
    -- Busco el legajo asociado a la accion
    begin
      -- Si existe, todo bien
      select razon_social into o_denominacion
      from T_COMUNES.VT_PERS_JURIDICAS_ABRV p
      where
        p.cuit = p_cuit and
        p.id_sede = '00';
    exception
      -- Si no existe intento agregarla
      WHEN NO_DATA_FOUND THEN
        select l.cuit, l.denominacion_sia, l.id_legajo, l.nro_ficha
          into v_Row_Entidad.cuit, v_Row_Entidad.Denominacion_1, v_Row_Entidad.id_legajo, v_Row_Entidad.MATRICULA
        from ipj.t_tramitesipj_persjur tp join ipj.t_legajos l
          on tp.id_legajo = l.id_legajo
        where
          id_tramite_ipj = p_id_tramite_ipj and
          cuit = p_cuit;

        -- Agrego la empresa en la estructura de gobierno
        DBMS_OUTPUT.PUT_LINE('    T_COMUNES.pack_persona_juridica.INSERTA_PERSJUR_UNIF_VERT = ');
        T_COMUNES.pack_persona_juridica.INSERTA_PERSJUR_UNIF_VERT(
          P_CUIT => v_Row_Entidad.Cuit,
          P_RAZON_SOCIAL => v_Row_Entidad.Denominacion_1,
          P_NOM_FAN => null,
          P_ID_FOR_JUR => null,
          P_ID_COND_IVA => null,
          P_ID_APLICACION => Types.c_id_Aplicacion ,
          P_FEC_INICIO_ACT => null,
          P_NRO_ING_BRUTO => null,
          P_ID_COND_INGBRUTO => null,
          P_ABREVIADO => null,
          P_FEC_INSCRIPCION => null,
          P_ID_ESTADO => null,
          P_GRAVAMEN => IPJ.ENTIDAD_PERSJUR.FC_ESTADO_GRAVAMEN_PERSJUR(v_Row_Entidad.id_legajo),
          P_INTERVENCION => IPJ.ENTIDAD_PERSJUR.FC_ESTADO_GRAVAMEN_INTERV(v_Row_Entidad.id_legajo),
          P_NRO_MATRICULA => v_Row_Entidad.MATRICULA,
          P_NRO_HAB_MUNI => null,
          O_ID_SEDE => v_PersJur_NewSede,
          O_RESULTADO => v_result_PersJur
        );
        DBMS_OUTPUT.PUT_LINE('        O_resultado = ' ||v_result_PersJur);

        -- Si no se pudo registrar, indico que no esta digitalizada
        if v_result_PersJur != IPJ.TYPES.C_RESP_OK then
          o_rdo := 'Entidad NO digitalizada';
          o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
          o_denominacion := '-';
          return;
        end if;
    end;

    -- Si se encontro en gobierno o se pudo cargar, retorna OK
    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    o_denominacion := nvl(o_denominacion, v_Row_Entidad.Denominacion_1);
  EXCEPTION
     WHEN OTHERS THEN
       o_rdo := 'SP_Buscar_CUIT_Gobierno: ' || To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
       o_denominacion := '-';
  END SP_Buscar_CUIT_Gobierno;

  PROCEDURE SP_Traer_PersJur_Libros_Dig(
    o_Cursor OUT IPJ.TYPES.cursorType,
    p_id_legajo in number
  )
  IS
    /*****************************************************************
       Lista los libros digitales utilizados por la entidad, solo si es del area SxA
  ******************************************************************/
    v_ubicacion_leg number(6);
    v_Id_Tipo_Entidad ipj.t_tipos_entidades.id_tipo_entidad%TYPE;
  BEGIN
    --15394:[Libros Digitales SA] - Espacio de Libros
    begin
      select  (select id_ubicacion from ipj.t_tipos_entidades te where te.id_tipo_entidad = l.id_tipo_entidad) ubicacion_leg, l.id_tipo_entidad
        into v_ubicacion_leg, v_Id_Tipo_Entidad
      from ipj.t_legajos l
      where
        l.id_legajo = p_id_legajo;
    exception
      when NO_DATA_FOUND then
        v_ubicacion_leg := 0;
    end;

    OPEN o_Cursor FOR
      Select er.borrador, er.fd_ps, to_char(er.fecha_sent, 'dd/mm/rrrr') fecha_sent,
        to_char(er.fecha_tramite, 'dd/mm/rrrr') fecha_tramite,
        er.id_juzgado, er.id_tipo_libro, er.id_tramite_ipj,
        er.id_legajo, er.nro_libro, er.sentencia, er.titulo ,
        1 orden, tl.tipo_libro, null n_juzgado, er.observacion,
        er.hojas_desde, er.hojas_hasta, er.es_autorizacion, tr.expediente
      from ipj.t_entidades_rubrica er join ipj.t_tipos_libros tl
          on er.id_tipo_libro = tl.id_tipo_libro
        join ipj.t_tramitesipj tr
          on er.Id_Tramite_Ipj = tr.Id_Tramite_Ipj
      WHERE er.id_legajo = p_id_legajo
        --AND v_ubicacion_leg = IPJ.TYPES.C_AREA_SXA
        AND tl.mecanizado = 'D'
        AND er.borrador = 'N'
        AND nvl(tl.id_tipo_entidad, v_Id_Tipo_Entidad) = v_Id_Tipo_Entidad
      order by er.fecha_tramite desc;

  END SP_Traer_PersJur_Libros_Dig;

  PROCEDURE SP_Traer_Intervencion(
    o_Cursor OUT TYPES.cursorType,
    o_rdo out varchar,
    p_Id_Tramite_Ipj in number,
    p_id_tramiteipj_accion in number,
    p_id_tipo_accion in number
    )
  IS
  /*******************************************************
    Lista los datos registrados en una acci�n de intervenci�n
  ********************************************************/
    v_row_accion ipj.t_tramitesipj_acciones%rowtype;
  BEGIN
    -- Busco los datos de la acci�n
    select * into v_row_accion
    from ipj.t_tramitesipj_acciones a
    where
      a.id_tramite_ipj = p_id_tramite_ipj and
      a.id_tramiteipj_accion = p_id_tramiteipj_accion;

    -- Muestro siempre el legajo, y si ya guardaron datos, los de la accion
    OPEN o_Cursor FOR
      select nvl(i.id_tramite_ipj, p_id_tramite_ipj) id_tramite_ipj,
        nvl(i.id_tramiteipj_accion, p_id_tramiteipj_accion), nvl(i.id_tipo_accion, p_id_tipo_accion) id_tipo_accion,
        nvl(i.id_legajo, l.id_legajo) id_legajo, i.id_integrante, i.observacion , l.denominacion_sia denominacion,
        l.cuit,
        (select expediente from ipj.t_tramitesipj tr where tr.id_tramite_ipj = p_id_tramite_ipj) Expediente,
        (select sticker from ipj.t_tramitesipj tr where tr.id_tramite_ipj = p_id_tramite_ipj) Sticker,
        (select n_ubicacion from ipj.t_ubicaciones u join ipj.t_tramitesipj tr on u.id_ubicacion = tr.id_ubicacion_origen where tr.id_tramite_ipj = p_id_tramite_ipj) n_ubicacion
      from ipj.t_legajos l left join ipj.t_intervenciones i
          on i.id_legajo = l.id_legajo and i.id_tramite_ipj = p_id_tramite_ipj and i.id_tramiteipj_accion = p_id_tramiteipj_accion
      where
        l.id_legajo = v_row_accion.id_legajo ;

  END SP_Traer_Intervencion;

  PROCEDURE SP_Traer_Interv_Ins_Legal(
    o_Cursor OUT TYPES.cursorType,
    p_Id_Tramite_Ipj in number,
    p_Id_TramiteIPJ_Accion in number,
    p_id_tipo_accion in number
    )
  IS
    /*******************************************************
    Este procedimento devuelve los instrumentos legales que conforman un Gravamen
  ********************************************************/
    v_id_legajo number;
    v_id_integrante number;
  BEGIN
    -- Busco la empresa o el integrante
    select nvl(g.id_legajo, 0), nvl(id_integrante, 0) into v_id_legajo, v_id_integrante
    from IPJ.T_TramitesIpj_Acciones g
    where
      g.Id_Tramite_Ipj = p_Id_Tramite_Ipj and
      g.id_tramiteipj_accion = p_id_tramiteipj_accion and
      g.id_tipo_accion = p_id_tipo_accion;

    -- Lista las intervenciones del tr�mite
    OPEN o_Cursor FOR
      select
        iil.id_tramite_ipj, iil.id_tramiteipj_accion, iil.id_tipo_accion, iil.id_juzgado,
        iil.il_tipo, iil.il_numero, iil.id_tipo_intervencion, to_char(iil.il_fecha, 'dd/mm/rrrr') il_fecha,
        iil.titulo, iil.descripcion, to_char(iil.fecha, 'dd/mm/rrrr') fecha,
        j.n_juzgado, l.n_il_tipo, ti.n_tipo_intervencion
      from ipj.t_intervenciones_ins_legal iil join ipj.t_juzgados j
          on iil.id_juzgado = j.id_juzgado
        join ipj.t_tipos_ins_legal l
          on iil.il_tipo = l.il_tipo
        join ipj.t_tipos_intervencion ti
          on iil.id_tipo_intervencion = ti.id_tipo_intervencion
      where
        iil.Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        iil.id_tramiteipj_accion = p_id_tramiteipj_accion and
        iil.id_tipo_accion = p_id_tipo_accion
      order by iil.fecha desc, iIL.IL_FECHA desc;

  END SP_Traer_Interv_Ins_Legal;

  PROCEDURE SP_GUARDAR_INTERV_INS_LEGAL(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in number,
    p_id_tramiteipj_accion in number,
    p_id_tipo_accion in number,
    p_id_legajo in number,
    p_id_integrante in number,
    p_id_tipo_intervencion in number,
    p_id_juzgado in number,
    p_il_tipo in number,
    p_il_numero in number,
    p_il_fecha in varchar2, -- Es Fecha
    p_titulo in varchar2,
    p_descripcion in varchar2,
    p_fecha in varchar2, -- es fecha
    p_eliminar in number-- 1 elimina
  )
  IS
    /*************************************************************
      Guarda los instrumentos legales asociados a una interveci�n de un tramite.
      Si la intervenci�n no esta registrada, la registra
  *************************************************************/
    v_valid_parametros varchar2(500);
    v_existe_interv number;
    v_informar_libro char(1);
    v_id_ubicacion number;
  BEGIN

  -- VALIDACION DE PARAMETROS
    if IPJ.VARIOS.Valida_Fecha(p_il_fecha) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT') || '(Fecha Ins. Legal)';
    end if;
    if IPJ.VARIOS.Valida_Fecha(p_fecha) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT') || '(Fecha Insc.)';
    end if;
    if IPJ.VARIOS.Valida_Tipo_Ins_Legal(nvl(p_il_tipo, 0)) = 0 then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('INS_LEGAL_NOT');
    end if;

    if v_valid_parametros is not null then
      o_rdo := 'Parametros: '  || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    -- CUERPO PROCEDIMIENTO
    if p_eliminar = 1 then
      delete ipj.t_intervenciones_ins_legal
      where
        Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        id_tramiteipj_accion = p_id_tramiteipj_accion and
        id_tipo_accion = p_id_tipo_accion and
        id_tipo_intervencion = p_id_tipo_intervencion;

    else
      -- Verifico si existe la intervencion
      select count(1) into v_existe_interv
      from ipj.t_intervenciones i
      where
        i.id_tramite_ipj = p_id_tramite_ipj and
        i.id_tramiteipj_accion = p_id_tramiteipj_accion and
        i.id_tipo_accion = p_id_tipo_accion;

      -- Si no hay ninguna, la registro
      if nvl(v_existe_interv, 0) = 0 then
        insert into ipj.t_intervenciones (id_tramite_ipj, id_tramiteipj_accion, id_tipo_accion,
          id_integrante, id_legajo, observacion)
        values (p_id_tramite_ipj, p_id_tramiteipj_accion, p_id_tipo_accion,
          decode(p_id_integrante, 0, null, p_id_integrante),
          decode(p_id_legajo, 0, null, p_id_legajo), null);
      end if;

      -- Si existe un registro igual no hago nada
      update ipj.t_intervenciones_ins_legal
      set
         titulo = p_titulo,
         descripcion = p_descripcion,
         fecha = to_date(p_fecha, 'dd/mm/rrrr'),
         id_juzgado = p_id_juzgado,
         il_tipo = p_il_tipo,
         il_numero = p_il_numero,
         il_fecha = to_date(p_il_fecha, 'dd/mm/rrrr')
      where
        Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        id_tramiteipj_accion = p_id_tramiteipj_accion and
        id_tipo_accion = p_id_tipo_accion and
        id_tipo_intervencion = p_id_tipo_intervencion;

      if sql%rowcount = 0 then
        insert into ipj.t_intervenciones_ins_legal (id_tramite_ipj, id_tramiteipj_accion,
          id_tipo_accion, id_tipo_intervencion, id_juzgado, il_tipo, il_numero, il_fecha,
          titulo, descripcion, fecha)
        values (p_id_tramite_ipj, p_id_tramiteipj_accion, p_id_tipo_accion, p_id_tipo_intervencion,
          p_id_juzgado, p_il_tipo, p_il_numero,
          to_date(p_il_fecha, 'dd/mm/rrrr'), p_titulo, p_descripcion, to_date(p_fecha, 'dd/mm/rrrr'));
      end if;
    end if;

    --Busco si el tipo de intervenci�n informa libro
    select nvl(informar_libros, 'N') into v_informar_libro
    from ipj.t_tipos_intervencion
    where
      id_tipo_intervencion = p_id_tipo_intervencion;

    -- Busco el area para saber si marca o no el envio de mail
    select id_ubicacion_origen into v_id_ubicacion
    from ipj.t_tramitesipj
    where
      id_tramite_ipj = p_id_tramite_ipj;

    -- Marco el tr�mite para que informe por mail
    if v_id_ubicacion <> IPJ.TYPES.C_AREA_CYF and v_informar_libro = 'S' then
      update IPJ.t_tramitesipj tr
      set tr.enviar_mail = 1
      where
        tr.id_tramite_ipj = p_Id_Tramite_Ipj;
    end if;

    o_rdo := 'OK';
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     WHEN OTHERS THEN
       o_rdo := 'SP_GUARDAR_INTERV_INS_LEGAL: '|| To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_GUARDAR_INTERV_INS_LEGAL;

  PROCEDURE SP_GUARDAR_ENTIDAD_REPR_HIST(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in number,
    p_id_legajo in number,
    p_id_integrante in number,
    p_fecha_alta in varchar2,
    p_borrador in varchar2,
    p_fecha_baja in varchar2,
    p_persona_expuesta char,
    p_id_vin_especial number,
    p_id_motivo_baja number
  )
  IS
    v_valid_parametros varchar2(200);
    v_bloque varchar2(100);
    v_tipo_mensaje number;
  BEGIN
  /*****************************************************
    Guarda los datos del representante, relacionado al tr�mite que se esta modificando.
    Sirve para armar un historial de las modificaciones.
  ******************************************************/
    update IPJ.T_ENTIDADES_REPRES_HIST e
    set
      borrador = p_borrador,
      fecha_baja = to_char(p_fecha_baja, 'dd/mm/rrrr'),
      persona_expuesta = p_persona_expuesta,
      id_vin_especial = id_vin_especial,
      id_motivo_baja = decode(p_id_motivo_baja, 0,  null, p_id_motivo_baja)
    where
      id_tramite_ipj = p_id_tramite_ipj and
      id_legajo = p_id_legajo and
      id_integrante = p_id_integrante and
      fecha_alta = to_date(p_fecha_alta, 'dd/mm/rrrr');

    -- si no existe, lo agrego
    if sql%rowcount = 0 then
      insert into ipj.t_entidades_repres_hist (id_tramite_ipj, id_legajo, id_integrante,
       fecha_alta, borrador, fecha_baja, persona_expuesta, id_vin_especial,
       id_motivo_baja)
     values (p_id_tramite_ipj, p_id_legajo, p_id_integrante,
       to_date(p_fecha_alta, 'dd/mm/rrrr'), p_borrador,
       to_date(p_fecha_baja, 'dd/mm/rrrr'), p_persona_expuesta, p_id_vin_especial,
       decode(p_id_motivo_baja, 0,  null, p_id_motivo_baja));
    end if;

    o_rdo := TYPES.c_Resp_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
    WHEN OTHERS THEN
       o_rdo := 'SP_GUARDAR_ENTIDAD_REPR_HIST: ' || To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_GUARDAR_ENTIDAD_REPR_HIST;

  PROCEDURE SP_Validar_Libros_Dig(
    o_rdo out number,
    p_id_legajo in number )
  IS
    /*******************************************************
      Valida si una entidad tiene libros digitales
    ********************************************************/
    v_res number;
    v_Id_Tipo_Entidad ipj.t_tipos_entidades.id_tipo_entidad%TYPE;
  BEGIN
    --15394:[Libros Digitales SA] - Espacio de Libros
    SELECT l.id_tipo_entidad
      INTO v_Id_Tipo_Entidad
      FROM ipj.t_legajos l
     WHERE l.id_legajo = p_id_legajo;

    -- Cuento si tiene libros digitales
    select count(1) into v_res
    from ipj.t_entidades_rubrica r join ipj.t_tipos_libros l
        on r.id_tipo_libro = l.id_tipo_libro
    WHERE r.id_legajo = p_id_legajo
      and l.mecanizado = 'D'
      AND nvl(l.id_tipo_entidad, v_Id_Tipo_Entidad) = v_Id_Tipo_Entidad
      ;

    o_rdo :=  NVL(v_res, 0);
  EXCEPTION
    when OTHERS then v_res := 0;
    o_rdo :=  v_res;
  END SP_Validar_Libros_Dig;

  FUNCTION FC_Buscar_Prim_Acta(p_id_tramite_ipj in number) return date is
  /**********************************************************
    Busca la m�nima fecha de las Asambleas, actas o reuniones del tr�mite
  **********************************************************/
    v_Min_Date date;
  BEGIN
    select min(fec_acta) into v_Min_Date
    from ipj.t_entidades_acta
    where
      id_tramite_ipj = p_id_tramite_ipj;

    Return nvl(v_Min_Date, to_date('01/01/1800', 'dd/mm/rrrr'));
  Exception
    When Others Then
      Return to_date('01/01/1800', 'dd/mm/rrrr');
  End FC_Buscar_Prim_Acta;

  FUNCTION FC_Buscar_Socio_Matr(p_id_tramite_ipj in number, p_id_entidad_socio in number) return varchar2 is
  /**********************************************************
    Busca la m�nima fecha de las Asambleas, actas o reuniones del tr�mite
  **********************************************************/
    v_row_socio_actual ipj.t_entidades_socios%rowtype;
    v_row_hist_Socio ipj.t_entidades_socios_hist%rowtype;
    v_id_tramite_ipj number;
    v_cursor_Socio IPJ.TYPES.CURSORTYPE;
    v_Ok_Version char;
    v_matricula varchar2(20);
  BEGIN
    -- Busco los datos del Socio en el tr�mite actual
    select * into v_row_socio_actual
    from ipj.t_entidades_socios
    where
      id_entidad_socio = p_id_entidad_socio;

    -- Armo cursor con el historial de ese socio, de manera decreciente
    OPEN v_cursor_Socio FOR
      select sh.*
      from IPJ.t_entidades e join ipj.t_tramitesipj t
          on e.Id_Tramite_Ipj = t.Id_Tramite_Ipj
        join ipj.t_entidades_socios_hist sh
          on sh.id_tramite_ipj = t.id_tramite_ipj and sh.id_legajo = e.id_legajo
      where
        e.id_legajo = v_row_socio_actual.id_legajo and
        sh.id_entidad_socio = p_id_entidad_socio and
        T.ID_ESTADO_ULT < IPJ.TYPES.C_ESTADOS_RECHAZADO and
        (E.Id_Tramite_Ipj = p_id_tramite_ipj or e.borrador = 'N')
      order by e.matricula asc, e.matricula_version desc, nvl(e.anio, 0) desc, e.folio desc;

    v_Ok_Version := 'N';
    -- Recorro todos los tr�mites, para obtener las inscripciones de los datos vigentes.
    LOOP
      fetch v_cursor_Socio into v_row_hist_Socio;
      EXIT WHEN v_cursor_Socio%NOTFOUND or v_cursor_Socio%NOTFOUND is null or v_Ok_Version = 'S';

      -- Si estoy en el tr�mite inicial, seteo el inicio de las variables
      if v_row_hist_Socio.id_tramite_ipj = p_id_tramite_ipj then
        v_Ok_Version := 'I';
        v_id_tramite_ipj := p_id_tramite_ipj;
      else
        -- Si no se iniciaron las variables, no hago nada y dejo pasar el tr�mite.
        if v_Ok_Version <> 'N' then

          -- Valido cambios en cuotas
          if v_Ok_Version = 'I' and
            nvl(v_row_hist_Socio.cuota, 0) = nvl(v_row_socio_actual.Cuota, 0)
          then
            v_id_tramite_ipj := v_row_hist_Socio.id_Tramite_ipj;
          else
            v_Ok_Version := 'S';
          end if;
        end if;
      end if;
    END LOOP;
    close v_cursor_Socio;

    -- Una vez que termina busco la matricula y la informo
    select matricula into v_matricula
    from ipj.t_entidades e
    where
      id_tramite_ipj = v_id_tramite_ipj and
      id_legajo = v_row_socio_actual.id_legajo;

    Return v_matricula;
  Exception
    When Others Then
      Return null;
  End FC_Buscar_Socio_Matr;

  FUNCTION FC_Buscar_Socio_Matr_Vers(p_id_tramite_ipj in number, p_id_entidad_socio in number) return number is
  /**********************************************************
    Busca la m�nima fecha de las Asambleas, actas o reuniones del tr�mite
  **********************************************************/
    v_row_socio_actual ipj.t_entidades_socios%rowtype;
    v_row_hist_Socio ipj.t_entidades_socios_hist%rowtype;
    v_id_tramite_ipj number;
    v_cursor_Socio IPJ.TYPES.CURSORTYPE;
    v_Ok_Version char;
    v_matricula_version number;
  BEGIN
    -- Busco los datos del Socio en el tr�mite actual
    select * into v_row_socio_actual
    from ipj.t_entidades_socios
    where
      id_entidad_socio = p_id_entidad_socio;

    -- Armo cursor con el historial de ese socio, de manera decreciente
    OPEN v_cursor_Socio FOR
      select sh.*
      from IPJ.t_entidades e join ipj.t_tramitesipj t
          on e.Id_Tramite_Ipj = t.Id_Tramite_Ipj
        join ipj.t_entidades_socios_hist sh
          on sh.id_tramite_ipj = t.id_tramite_ipj and sh.id_legajo = e.id_legajo
      where
        e.id_legajo = v_row_socio_actual.id_legajo and
        sh.id_entidad_socio = p_id_entidad_socio and
        T.ID_ESTADO_ULT < IPJ.TYPES.C_ESTADOS_RECHAZADO and
        (E.Id_Tramite_Ipj = p_id_tramite_ipj or e.borrador = 'N')
      order by e.matricula asc, e.matricula_version desc, nvl(e.anio, 0) desc, e.folio desc;

    v_Ok_Version := 'N';
    -- Recorro todos los tr�mites, para obtener las inscripciones de los datos vigentes.
    LOOP
      fetch v_cursor_Socio into v_row_hist_Socio;
      EXIT WHEN v_cursor_Socio%NOTFOUND or v_cursor_Socio%NOTFOUND is null or v_Ok_Version = 'S';

      -- Si estoy en el tr�mite inicial, seteo el inicio de las variables
      if v_row_hist_Socio.id_tramite_ipj = p_id_tramite_ipj then
        v_Ok_Version := 'I';
        v_id_tramite_ipj := p_id_tramite_ipj;
      else
        -- Si no se iniciaron las variables, no hago nada y dejo pasar el tr�mite.
        if v_Ok_Version <> 'N' then

          -- Valido cambios en cuotas
          if v_Ok_Version = 'I' and
            nvl(v_row_hist_Socio.cuota, 0) = nvl(v_row_socio_actual.Cuota, 0)
          then
            v_id_tramite_ipj := v_row_hist_Socio.id_Tramite_ipj;
          else
            v_Ok_Version := 'S';
          end if;
        end if;
      end if;
    END LOOP;
    close v_cursor_Socio;

    -- Una vez que termina busco la matricula y la informo
    select matricula_version into v_matricula_version
    from ipj.t_entidades e
    where
      id_tramite_ipj = v_id_tramite_ipj and
      id_legajo = v_row_socio_actual.id_legajo;

    Return v_matricula_version;
  Exception
    When Others Then
      Return 0;
  End FC_Buscar_Socio_Matr_Vers;

  FUNCTION FC_Buscar_Socio_Folio(p_id_tramite_ipj in number, p_id_entidad_socio in number) return varchar2 is
  /**********************************************************
    Busca la m�nima fecha de las Asambleas, actas o reuniones del tr�mite
  **********************************************************/
    v_row_socio_actual ipj.t_entidades_socios%rowtype;
    v_row_hist_Socio ipj.t_entidades_socios_hist%rowtype;
    v_id_tramite_ipj number;
    v_cursor_Socio IPJ.TYPES.CURSORTYPE;
    v_Ok_Version char;
    v_Folio varchar2(20);
  BEGIN
    -- Busco los datos del Socio en el tr�mite actual
    select * into v_row_socio_actual
    from ipj.t_entidades_socios
    where
      id_entidad_socio = p_id_entidad_socio;

    -- Armo cursor con el historial de ese socio, de manera decreciente
    OPEN v_cursor_Socio FOR
      select sh.*
      from IPJ.t_entidades e join ipj.t_tramitesipj t
          on e.Id_Tramite_Ipj = t.Id_Tramite_Ipj
        join ipj.t_entidades_socios_hist sh
          on sh.id_tramite_ipj = t.id_tramite_ipj and sh.id_legajo = e.id_legajo
      where
        e.id_legajo = v_row_socio_actual.id_legajo and
        sh.id_entidad_socio = p_id_entidad_socio and
        T.ID_ESTADO_ULT < IPJ.TYPES.C_ESTADOS_RECHAZADO and
        (E.Id_Tramite_Ipj = p_id_tramite_ipj or e.borrador = 'N')
      order by e.matricula asc, e.matricula_version desc, nvl(e.anio, 0) desc, e.folio desc;

    v_Ok_Version := 'N';
    -- Recorro todos los tr�mites, para obtener las inscripciones de los datos vigentes.
    LOOP
      fetch v_cursor_Socio into v_row_hist_Socio;
      EXIT WHEN v_cursor_Socio%NOTFOUND or v_cursor_Socio%NOTFOUND is null or v_Ok_Version = 'S';

      -- Si estoy en el tr�mite inicial, seteo el inicio de las variables
      if v_row_hist_Socio.id_tramite_ipj = p_id_tramite_ipj then
        v_Ok_Version := 'I';
        v_id_tramite_ipj := p_id_tramite_ipj;
      else
        -- Si no se iniciaron las variables, no hago nada y dejo pasar el tr�mite.
        if v_Ok_Version <> 'N' then

          -- Valido cambios en cuotas
          if v_Ok_Version = 'I' and
            nvl(v_row_hist_Socio.cuota, 0) = nvl(v_row_socio_actual.Cuota, 0)
          then
            v_id_tramite_ipj := v_row_hist_Socio.id_Tramite_ipj;
          else
            v_Ok_Version := 'S';
          end if;
        end if;
      end if;
    END LOOP;
    close v_cursor_Socio;

    -- Una vez que termina busco la matricula y la informo
    select Folio into v_Folio
    from ipj.t_entidades e
    where
      id_tramite_ipj = v_id_tramite_ipj and
      id_legajo = v_row_socio_actual.id_legajo;

    Return v_Folio;
  Exception
    When Others Then
      Return null;
  End FC_Buscar_Socio_Folio;

  FUNCTION FC_Buscar_Socio_Anio(p_id_tramite_ipj in number, p_id_entidad_socio in number) return number is
  /**********************************************************
    Busca la m�nima fecha de las Asambleas, actas o reuniones del tr�mite
  **********************************************************/
    v_row_socio_actual ipj.t_entidades_socios%rowtype;
    v_row_hist_Socio ipj.t_entidades_socios_hist%rowtype;
    v_id_tramite_ipj number;
    v_cursor_Socio IPJ.TYPES.CURSORTYPE;
    v_Ok_Version char;
    v_Anio number;
  BEGIN
    -- Busco los datos del Socio en el tr�mite actual
    select * into v_row_socio_actual
    from ipj.t_entidades_socios
    where
      id_entidad_socio = p_id_entidad_socio;

    -- Armo cursor con el historial de ese socio, de manera decreciente
    OPEN v_cursor_Socio FOR
      select sh.*
      from IPJ.t_entidades e join ipj.t_tramitesipj t
          on e.Id_Tramite_Ipj = t.Id_Tramite_Ipj
        join ipj.t_entidades_socios_hist sh
          on sh.id_tramite_ipj = t.id_tramite_ipj and sh.id_legajo = e.id_legajo
      where
        e.id_legajo = v_row_socio_actual.id_legajo and
        sh.id_entidad_socio = p_id_entidad_socio and
        T.ID_ESTADO_ULT < IPJ.TYPES.C_ESTADOS_RECHAZADO and
        (E.Id_Tramite_Ipj = p_id_tramite_ipj or e.borrador = 'N')
      order by e.matricula asc, e.matricula_version desc, nvl(e.anio, 0) desc, e.folio desc;

    v_Ok_Version := 'N';
    -- Recorro todos los tr�mites, para obtener las inscripciones de los datos vigentes.
    LOOP
      fetch v_cursor_Socio into v_row_hist_Socio;
      EXIT WHEN v_cursor_Socio%NOTFOUND or v_cursor_Socio%NOTFOUND is null or v_Ok_Version = 'S';

      -- Si estoy en el tr�mite inicial, seteo el inicio de las variables
      if v_row_hist_Socio.id_tramite_ipj = p_id_tramite_ipj then
        v_Ok_Version := 'I';
        v_id_tramite_ipj := p_id_tramite_ipj;
      else
        -- Si no se iniciaron las variables, no hago nada y dejo pasar el tr�mite.
        if v_Ok_Version <> 'N' then

          -- Valido cambios en cuotas
          if v_Ok_Version = 'I' and
            nvl(v_row_hist_Socio.cuota, 0) = nvl(v_row_socio_actual.Cuota, 0)
          then
            v_id_tramite_ipj := v_row_hist_Socio.id_Tramite_ipj;
          else
            v_Ok_Version := 'S';
          end if;
        end if;
      end if;
    END LOOP;
    close v_cursor_Socio;

    -- Una vez que termina busco la matricula y la informo
    select Anio into v_Anio
    from ipj.t_entidades e
    where
      id_tramite_ipj = v_id_tramite_ipj and
      id_legajo = v_row_socio_actual.id_legajo;

    Return v_Anio;
  Exception
    When Others Then
      Return 0;
  End FC_Buscar_Socio_Anio;

  PROCEDURE SP_Traer_Autoriz_Mail(
    o_Cursor OUT types.cursorType,
    p_Id_Tramite_Ipj in number,
    p_hab_autoriz in char,
    p_hab_admin in char,
    p_hab_repres in char,
    p_hab_socios in char,
    p_hab_fiduciario in char,
    p_hab_fiduciante in char,
    p_hab_denunciante in char,
    p_hab_normalizador in char
    )
  IS
   /**********************************************************
    Lista las personas f�sicas a las que se deben enviar mail para un tramite
  **********************************************************/
    v_ubicacion_leg number;
    v_matricula_version number;
    v_id_legajo number;
    v_dias_vigencia number;
  BEGIN
    -- Busco la primero entidad del tr�mite
    select id_legajo into v_id_legajo
    from ipj.t_tramitesipj_persjur p
    where
      id_tramite_ipj = p_id_tramite_ipj and
      rownum = 1;

    v_dias_vigencia := to_number(IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('DIAS_ADMIN_LEY'));

    -- Busco los datos de la entidad y del tr�mite
    begin
      select nvl(matricula_version, 0),
        (select id_ubicacion from ipj.t_tipos_entidades te where te.id_tipo_entidad = l.id_tipo_entidad) ubicacion_leg
        into v_matricula_version,  v_ubicacion_leg
      from IPJ.t_entidades e join ipj.t_legajos l
        on e.id_legajo = l.id_legajo
      where
        e.Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        e.id_legajo = v_id_legajo;
    exception
      when NO_DATA_FOUND then
        v_matricula_version := 0;

        select
          (select id_ubicacion from ipj.t_tipos_entidades te where te.id_tipo_entidad = l.id_tipo_entidad) ubicacion_leg
           into v_ubicacion_leg
        from ipj.t_legajos l
        where
          l.id_legajo = v_id_legajo;
    end;

    -- LISTO LAS PERSONAS F�SICAS DEL TRAMITE
    OPEN o_Cursor FOR
      select distinct p_id_tramite_ipj Id_Tramite_Ipj, Id_Integrante, (vt.nov_nombre || ' ' || vt.nov_apellido) Detalle, vt.Id_Sexo, vt.Id_Numero,
        vt.Nro_Documento, vt.Pai_Cod_Pais, vt.cuil cuil
        --, Tipo
      from
        ( -- Lista de Autorizados del Tr�mite
          select Ea.Id_Tramite_Ipj, Ea.Id_Integrante, I.Id_Sexo, I.Id_Numero, I.Nro_Documento, I.Pai_Cod_Pais
            --, 'Autorizado' Tipo
          from ipj.t_entidades_autorizados ea join ipj.t_integrantes i
              on ea.id_integrante = i.id_integrante
          where
            p_hab_autoriz = 'S' and
            ea.Id_Tramite_Ipj = p_Id_Tramite_Ipj

          -- AUTORIDADES ORGANO ADMINISTRACION
          UNION ALL
          select Ea.Id_Tramite_Ipj, Ea.Id_Integrante, I.Id_Sexo, I.Id_Numero, I.Nro_Documento, I.Pai_Cod_Pais
            --, 'Administrador' Tipo
          from ipj.t_entidades_admin ea join ipj.t_integrantes i
              on ea.id_integrante = i.id_integrante
            join ipj.t_entidades e
              on e.id_tramite_ipj = ea.id_tramite_ipj and e.id_legajo = ea.id_legajo
          where
            p_hab_admin = 'S' and
            e.id_legajo = v_id_legajo and
            e.matricula_version <= v_matricula_version and
            e.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_ubicacion_leg) and
           (ea.borrador = 'N' or ea.Id_Tramite_Ipj = p_Id_Tramite_Ipj) and
           (ea.fecha_fin is null or (ea.fecha_fin + v_dias_vigencia) >= to_date(sysdate, 'dd/mm/rrrr') or ea.Id_Tramite_Ipj = p_Id_Tramite_Ipj)
         -- TRAMITES ANEXOS DE AUTORIDADES
         UNION ALL
          select Ea.Id_Tramite_Ipj, Ea.Id_Integrante, I.Id_Sexo, I.Id_Numero, I.Nro_Documento, I.Pai_Cod_Pais
            --, 'Administrador' Anexo Tipo
          from ipj.t_entidades_admin ea join ipj.t_integrantes i
              on ea.id_integrante = i.id_integrante
            join ipj.t_entidades e
              on e.id_tramite_ipj = ea.id_tramite_ipj and e.id_legajo = ea.id_legajo
            join ipj.t_tramitesipj tr
              on ea.id_tramite_ipj = tr.id_tramite_ipj
          where
            p_hab_admin = 'S' and
            e.id_legajo = v_id_legajo and
            e.matricula_version <= v_matricula_version and
            e.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_ubicacion_leg) and
            tr.id_tramite_ipj_padre = p_Id_Tramite_Ipj and
            (ea.fecha_fin is null or (ea.fecha_fin + v_dias_vigencia) >= to_date(sysdate, 'dd/mm/rrrr') or  tr.id_tramite_ipj_padre = p_Id_Tramite_Ipj)

          -- AUTORIDADES ORGANO REPRESENTACION
          UNION ALL
          select er.Id_Tramite_Ipj, er.Id_Integrante, I.Id_Sexo, I.Id_Numero, I.Nro_Documento, I.Pai_Cod_Pais
            --, 'Representante' Tipo
          from ipj.t_entidades_representante er join ipj.t_integrantes i
              on er.id_integrante = i.id_integrante
            join ipj.t_entidades e
              on er.id_tramite_ipj = e.id_tramite_ipj and er.id_legajo = e.id_legajo
          where
            p_hab_repres = 'S' and
            e.id_legajo = v_id_legajo and
            e.matricula_version <= v_matricula_version and
            e.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_ubicacion_leg) and
           ((er.borrador = 'N' and er.fecha_baja is null) or Er.Id_Tramite_Ipj = p_Id_Tramite_Ipj)
          -- TRAMITES ANEXOS DE REPRESENTANTES
          UNION ALL
          select er.Id_Tramite_Ipj, er.Id_Integrante, I.Id_Sexo, I.Id_Numero, I.Nro_Documento, I.Pai_Cod_Pais
            --, 'Representante' Tipo
          from ipj.t_entidades_representante er join ipj.t_integrantes i
              on er.id_integrante = i.id_integrante
            join ipj.t_entidades e
              on er.id_tramite_ipj = e.id_tramite_ipj and er.id_legajo = e.id_legajo
            join ipj.t_tramitesipj tr
              on er.id_tramite_ipj = tr.id_tramite_ipj
          where
            p_hab_repres = 'S' and
            e.id_legajo = v_id_legajo and
            e.matricula_version <= v_matricula_version and
            e.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_ubicacion_leg) and
            tr.id_tramite_ipj_padre = p_Id_Tramite_Ipj

          -- SOCIOS
          UNION ALL
          select s.Id_Tramite_Ipj, s.Id_Integrante, I.Id_Sexo, I.Id_Numero, I.Nro_Documento, I.Pai_Cod_Pais
            --, 'Socios' Tipo
          from ipj.t_entidades_socios s join ipj.t_integrantes i
              on s.id_integrante = i.id_integrante
          where
            p_hab_socios = 'S' and
            s.Id_Tramite_Ipj = p_Id_Tramite_Ipj and
            s.id_tipo_socio in (3, 4) -- Fundarores y Constituyentes

            -- FIDUCIARIOS
          UNION ALL
          select f.Id_Tramite_Ipj, f.Id_Integrante, I.Id_Sexo, I.Id_Numero, I.Nro_Documento, I.Pai_Cod_Pais
            --, 'Fiduciarios' Tipo
          from ipj.t_entidades_fiduciarios f join ipj.t_integrantes i
              on f.id_integrante = i.id_integrante
          where
            p_hab_fiduciario = 'S' and
            f.Id_Tramite_Ipj = p_Id_Tramite_Ipj

          -- FIDUCIANTES
          UNION ALL
          select f.Id_Tramite_Ipj, f.Id_Integrante, I.Id_Sexo, I.Id_Numero, I.Nro_Documento, I.Pai_Cod_Pais
            --, 'Fiduciantes' Tipo
          from ipj.t_entidades_fiduciantes f join ipj.t_integrantes i
              on f.id_integrante = i.id_integrante
          where
            p_hab_fiduciante = 'S' and
            f.Id_Tramite_Ipj = p_Id_Tramite_Ipj

          -- AUTORIDADES DENUNCIANTES
          UNION ALL
          select d.id_tramite_ipj, d.Id_Integrante, I.Id_Sexo, I.Id_Numero, I.Nro_Documento, I.Pai_Cod_Pais
            --, 'Denunciante' Tipo
          from IPJ.T_DENUNCIANTES d join ipj.t_integrantes i
              on d.id_integrante = i.id_integrante
          where
            p_hab_denunciante = 'S' and
            d.id_tramite_ipj = p_id_tramite_ipj

          -- AUTORIDADES SOLICITANTES COMISIONES
          UNION ALL
          select s.id_tramite_ipj, s.Id_Integrante, I.Id_Sexo, I.Id_Numero, I.Nro_Documento, I.Pai_Cod_Pais
            --, 'Solicitante CN' Tipo
          from IPJ.T_CN_SOLICITANTE s join ipj.t_integrantes i
              on s.id_integrante = i.id_integrante
          where
            p_hab_normalizador = 'S' and
            s.id_tramite_ipj = p_id_tramite_ipj

          -- AUTORIDADES NORMALIZADORES COMISIONES
          UNION ALL
          select n.id_tramite_ipj, n.Id_Integrante, I.Id_Sexo, I.Id_Numero, I.Nro_Documento, I.Pai_Cod_Pais
            --, 'Normalizadores CN' Tipo
          from IPJ.T_CN_NORMALIZADOR n join ipj.t_integrantes i
              on n.id_integrante = i.id_integrante
          where
            p_hab_normalizador = 'S' and
            n.id_tramite_ipj = p_id_tramite_ipj
          ) tmp
          join rcivil.vt_pk_persona vt
              on tmp.id_sexo = vt.id_sexo and tmp.nro_documento = vt.nro_documento and tmp.pai_cod_pais = vt.pai_cod_pais and tmp.id_numero = vt.id_numero
          where
            vt.cuil is not null;

  END SP_Traer_Autoriz_Mail;

  PROCEDURE SP_Traer_PersJur_Admin_Borr(
    o_Cursor OUT TYPES.cursorType,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number)
  IS
  /***************************************************************
  Este procemiento devuelve el listado de Autoridades registradas en tr�mites en borrador
  ***************************************************************/
    v_ubicacion_leg number(6);
  BEGIN

    if IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('HAB_LOG_GESTION') = '1' then
      IPJ.VARIOS.SP_GUARDAR_LOG(
        p_METODO => 'SP_Traer_PersJur_Admin_Borr',
        p_NIVEL => 'Gestion',
        p_ORIGEN => 'DB',
        p_USR_LOG  => 'DB',
        p_USR_WINDOWS => 'Sistema',
        p_IP_PC  => null,
        p_EXCEPCION =>
          'Id Tramite IPJ = ' || to_char(p_Id_Tramite_Ipj)
          || ' / Id Legajo = ' || to_char(p_id_legajo)
      );
    end if;

    -- Busco el tipo de entidad del legajo
    begin
      select
        (select id_ubicacion from ipj.t_tipos_entidades te where te.id_tipo_entidad = l.id_tipo_entidad) ubicacion_leg
        into v_ubicacion_leg
      from IPJ.t_entidades e join ipj.t_legajos l
        on e.id_legajo = l.id_legajo
      where
        e.Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        e.id_legajo = p_id_legajo;
    exception
      when NO_DATA_FOUND then
        select id_ubicacion into v_ubicacion_leg
        from ipj.t_legajos l join ipj.t_tipos_entidades te
          on te.id_tipo_entidad = l.id_tipo_entidad
        where
          l.id_legajo = p_id_legajo;
    end;

    OPEN o_Cursor FOR
      select
        ie.id_tipo_integrante, to_char(ie.fecha_inicio, 'dd/mm/rrrr') fecha_inicio,
        to_char(ie.fecha_fin, 'dd/mm/rrrr') fecha_fin, ie.id_motivo_baja,
        i.id_integrante, i.id_numero, i.id_sexo, i.nro_documento, i.pai_cod_pais,
        (select n_tipo_integrante from ipj.t_tipos_integrante ti where ie.id_tipo_integrante = ti.id_tipo_integrante) n_tipo_integrante,
        (select n_tipo_organismo from IPJ.T_Tipos_Organismo tor where ie.id_tipo_organismo = tor.id_tipo_organismo) n_tipo_organismo,
        (select nro_orden from ipj.t_tipos_integrante ti where ie.id_tipo_integrante = ti.id_tipo_integrante) nro_orden_int,
        (select nro_orden from IPJ.T_Tipos_Organismo tor where ie.id_tipo_organismo = tor.id_tipo_organismo) nro_orden_org,
        nvl(ie.n_empresa,
          (select  TRIM(p.NOV_NOMBRE || ' ' ||p.NOV_APELLIDO) from rcivil.vt_pk_persona p
           where i.id_sexo = p.ID_SEXO and i.nro_documento = p.NRO_DOCUMENTO and i.pai_cod_pais = p.PAI_COD_PAIS and i.id_numero = p.ID_NUMERO)
         ) detalle,
        (select n_motivo_baja from IPJ.T_Tipos_Motivos_Baja mb where ie.id_motivo_baja = mb.id_motivo_baja) n_motivo_baja,
        t.expediente, to_char(t.fecha_ini_suac, 'dd/mm/rrrr') fecha_ini_suac
      from ipj.t_entidades_admin ie left join ipj.t_integrantes i
          on ie.id_integrante = i.id_integrante
        join ipj.t_Entidades e
          on ie.Id_Tramite_Ipj = e.Id_Tramite_Ipj and ie.id_legajo = e.id_legajo
        join IPJ.t_tramitesipj t
          on ie.Id_Tramite_Ipj = t.Id_Tramite_Ipj
    where
      e.id_legajo = p_id_legajo and
      e.Id_Tramite_Ipj <> p_Id_Tramite_Ipj and
      e.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_ubicacion_leg) and
      ie.borrador = 'S'  and
      (t.id_estado_ult between 2 and 99 or t.id_estado_ult = 210) -- Tramites en estudio o inactivo
    order by t.fecha_ini_suac desc, nro_orden_org asc, nro_orden_int asc, detalle asc;

  END SP_Traer_PersJur_Admin_Borr;

  PROCEDURE SP_Traer_PersJur_Sind_Borr(
    o_Cursor OUT TYPES.cursorType,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number)
  IS
  /***************************************************************
  Este procemiento devuelve el listado de Sindicos registrados en tr�mites en borrador
  ***************************************************************/
    v_ubicacion_leg number(6);
  BEGIN

    if IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('HAB_LOG_GESTION') = '1' then
      IPJ.VARIOS.SP_GUARDAR_LOG(
        p_METODO => 'SP_Traer_PersJur_Sind_Borr',
        p_NIVEL => 'Gestion',
        p_ORIGEN => 'DB',
        p_USR_LOG  => 'DB',
        p_USR_WINDOWS => 'Sistema',
        p_IP_PC  => null,
        p_EXCEPCION =>
          'Id Tramite IPJ = ' || to_char(p_Id_Tramite_Ipj)
          || ' / Id Legajo = ' || to_char(p_id_legajo)
      );
    end if;

    -- Busco el tipo de entidad del legajo
    begin
      select
        (select id_ubicacion from ipj.t_tipos_entidades te where te.id_tipo_entidad = l.id_tipo_entidad) ubicacion_leg
        into v_ubicacion_leg
      from IPJ.t_entidades e join ipj.t_legajos l
        on e.id_legajo = l.id_legajo
      where
        e.Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        e.id_legajo = p_id_legajo;
    exception
      when NO_DATA_FOUND then
        select id_ubicacion into v_ubicacion_leg
        from ipj.t_legajos l join ipj.t_tipos_entidades te
          on te.id_tipo_entidad = l.id_tipo_entidad
        where
          l.id_legajo = p_id_legajo;
    end;


    OPEN o_Cursor FOR
      select
        ie.id_tipo_integrante, to_char(ie.fecha_alta, 'dd/mm/rrrr') fecha_alta,
        to_char(ie.fecha_baja, 'dd/mm/rrrr') fecha_baja, ie.id_motivo_baja,
        i.id_integrante, i.id_numero, i.id_sexo, i.nro_documento, i.pai_cod_pais,
        (select n_tipo_integrante from ipj.t_tipos_integrante ti where ie.id_tipo_integrante = ti.id_tipo_integrante) n_tipo_integrante,
        (select n_tipo_organismo from IPJ.T_Tipos_Organismo tor join ipj.t_tipos_integrante ti on tor.id_tipo_organismo = ti.id_tipo_organismo where ie.id_tipo_integrante = ti.id_tipo_integrante) n_tipo_organismo,
        (select nro_orden from ipj.t_tipos_integrante ti where ie.id_tipo_integrante = ti.id_tipo_integrante) nro_orden_int,
        (select tor.nro_orden from IPJ.T_Tipos_Organismo tor join ipj.t_tipos_integrante ti on tor.id_tipo_organismo = ti.id_tipo_organismo where ie.id_tipo_integrante = ti.id_tipo_integrante) nro_orden_org,
        nvl(ie.n_empresa,
          (select  TRIM(p.NOV_NOMBRE || ' ' ||p.NOV_APELLIDO) from rcivil.vt_pk_persona p
           where i.id_sexo = p.ID_SEXO and i.nro_documento = p.NRO_DOCUMENTO and i.pai_cod_pais = p.PAI_COD_PAIS and i.id_numero = p.ID_NUMERO)
         ) detalle,
        (select n_motivo_baja from IPJ.T_Tipos_Motivos_Baja mb where ie.id_motivo_baja = mb.id_motivo_baja) n_motivo_baja,
        t.expediente, to_char(t.fecha_ini_suac, 'dd/mm/rrrr') fecha_ini_suac
      from ipj.t_entidades_sindico ie left join ipj.t_integrantes i
          on ie.id_integrante = i.id_integrante
        join ipj.t_Entidades e
          on ie.Id_Tramite_Ipj = e.Id_Tramite_Ipj and ie.id_legajo = e.id_legajo
        join IPJ.t_tramitesipj t
          on ie.Id_Tramite_Ipj = t.Id_Tramite_Ipj
    where
      e.id_legajo = p_id_legajo and
      e.Id_Tramite_Ipj <> p_Id_Tramite_Ipj and
      e.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_ubicacion_leg) and
      ie.borrador = 'S'  and
      (t.id_estado_ult between 2 and 99 or t.id_estado_ult = 210) -- Tramites en estudio o inactivo
    order by t.fecha_ini_suac desc, nro_orden_org asc, nro_orden_int asc, detalle asc;

  END SP_Traer_PersJur_Sind_Borr;

  PROCEDURE SP_Traer_Actas_Inscr(
    o_Cursor OUT TYPES.cursorType,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number
  )
  IS
    v_matricula_version number(6,0);
    v_ubicacion_ent number(6);
    v_ubicacion_leg number(6);
  BEGIN
  /*******************************************************
    Este procedimento devuelve las actas de un tr�mite de una empresa, y sus anexos
  ********************************************************/
    begin
      select nvl(matricula_version, 0),
        (select id_ubicacion from ipj.t_tipos_entidades te where te.id_tipo_entidad = e.id_tipo_entidad) ubicacion_ent,
        (select id_ubicacion from ipj.t_tipos_entidades te where te.id_tipo_entidad = l.id_tipo_entidad) ubicacion_leg
        into v_matricula_version, v_ubicacion_ent, v_ubicacion_leg
      from IPJ.t_entidades e join ipj.t_legajos l
        on e.id_legajo = l.id_legajo
      where
        e.Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        e.id_legajo = p_id_legajo;
    exception
      when no_data_found then
        v_matricula_version := 0;
    end;

    -- Traigo solo las actas de este tr�mite, y sus anexos
    OPEN o_Cursor FOR
      select n_tipo_acta, id_legajo, id_tipo_acta, fec_acta, observacion, borrador,
        id_entidad_acta, id_tramite_ipj, art_reformados, id_vin,
        Sticker, Id_Tramite_Suac, es_unanime, es_autoconvocada, es_en_sede,
        fecha_convocatoria, fecha_acta_directorio, fecha_libro_asamblea, publicado_boe,
        fecha_primera_boe, fecha_ultima_boe, publicado_diario, fecha_primera_diario,
        fecha_ultima_diario, porc_capital_social, quorum_convocatoria1, quorum_convocatoria2,
        fecha_cuarto_inter, calle_inf,id_tipodom, n_tipodom, id_tipocalle, n_tipocalle,
        id_calle, n_calle, altura, depto, piso, torre, id_barrio, n_barrio, id_localidad,
        n_localidad, id_departamento, Manzana, Lote, km, n_departamento, id_provincia,
        n_provincia, cpa, en_sede, imprimir, Nro_Expediente, Id_Tramite_Ipj_actual,
        N_Estado, id_estado, id_tramite_ipj_padre, hora_acta
      from
        ( select ta.n_tipo_acta, ec.id_legajo, ec.id_tipo_acta, to_char(ec.fec_acta, 'dd/mm/rrrr') fec_acta,
            EC.OBSERVACION, ec.borrador, EC.ID_ENTIDAD_ACTA,
            EC.Id_Tramite_Ipj, EC.ART_REFORMADOS, EC.ID_VIN,
            nvl(tr.sticker,  IPJ.TRAMITES_SUAC.FC_BUSCAR_STIKER_PUBLICO(tr.id_tramite)) Sticker,
            tr.id_tramite Id_Tramite_Suac,
            ec.es_unanime, ec.es_autoconvocada, ec.es_en_sede,
            to_char(ec.fecha_convocatoria, 'dd/mm/rrrr') fecha_convocatoria,
            to_char(ec.fecha_acta_directorio, 'dd/mm/rrrr') fecha_acta_directorio,
            to_char(ec.fecha_libro_asamblea, 'dd/mm/rrrr') fecha_libro_asamblea,
            ec.publicado_boe, to_char(ec.fecha_primera_boe, 'dd/mm/rrrr') fecha_primera_boe,
            to_char(ec.fecha_ultima_boe, 'dd/mm/rrrr') fecha_ultima_boe,
            ec.publicado_diario, to_char(ec.fecha_primera_diario, 'dd/mm/rrrr') fecha_primera_diario,
            to_char(ec.fecha_ultima_diario, 'dd/mm/rrrr') fecha_ultima_diario,
            to_char(ec.porc_capital_social,  '999990.99') porc_capital_social,
            to_char(ec.quorum_convocatoria1, '999990.99') quorum_convocatoria1,
            to_char(ec.quorum_convocatoria2, '999990.99') quorum_convocatoria2,
            to_char(ec.fecha_cuarto_inter, 'dd/mm/rrrr') fecha_cuarto_inter,
            IPJ.VARIOS.FC_ARMAR_CALLE_DOM(d.n_calle, d.altura, d.piso, d.depto, d.torre, d.mzna, d.lote, d.n_barrio, d.km, d.n_tipocalle) calle_inf,
            d.id_tipodom, d.n_tipodom, d.id_tipocalle, d.n_tipocalle, d.id_calle,
            initcap(d.n_calle) n_calle, d.altura, d.depto, d.piso, d.torre, d.id_barrio,
            initcap(d.n_barrio) n_barrio,
            d.id_localidad, initcap(d.n_localidad) n_localidad, d.id_departamento,
            D.mzna Manzana, D.Lote, d.km,
            initcap(d.n_departamento) n_departamento, d.id_provincia,
            initcap(d.n_provincia) n_provincia, d.cpa,
            (case when nvl(ec.id_vin, 0) = 0 then 1 else 0 end) en_sede,
            (case when ec.Id_Tramite_Ipj = p_Id_Tramite_Ipj then 1 else 0 end) imprimir,
            nvl(tr.expediente, IPJ.TRAMITES_SUAC.FC_BUSCAR_EXPEDIENTE(tr.id_tramite)) Nro_Expediente,
            p_Id_Tramite_Ipj Id_Tramite_Ipj_actual,
            (select N_estado from Ipj.T_Estados es where es.id_estado = Tr.Id_Estado_Ult) N_Estado,
            Tr.Id_Estado_Ult id_estado, tr.id_tramite_ipj_padre,
            decode(to_char(ec.fec_acta,'HH24:Mi'),'00:00', NULL, to_char(ec.fec_acta,'HH24:Mi')) hora_acta
          from ipj.t_entidades_acta ec join ipj.t_tipos_acta ta
              on ta.id_tipo_acta = ec.id_tipo_acta
            join ipj.t_Entidades tpj
              on ec.Id_Tramite_Ipj = tpj.Id_Tramite_Ipj and ec.id_legajo = tpj.id_legajo
            join ipj.t_tramitesipj tr
              on TR.Id_Tramite_Ipj = TPJ.Id_Tramite_Ipj
            left join dom_manager.VT_DOMICILIOS_COND d
              on d.id_vin = ec.id_vin
          where
            tr.id_tramite_ipj = p_id_tramite_ipj and
            tpj.id_legajo = p_id_legajo

          UNION ALL
          -- Sumo los anexos
          select ta.n_tipo_acta, ec.id_legajo, ec.id_tipo_acta, to_char(ec.fec_acta, 'dd/mm/rrrr') fec_acta,
            EC.OBSERVACION, ec.borrador, EC.ID_ENTIDAD_ACTA,
            EC.Id_Tramite_Ipj, EC.ART_REFORMADOS, EC.ID_VIN,
            nvl(tr.sticker,  IPJ.TRAMITES_SUAC.FC_BUSCAR_STIKER_PUBLICO(tr.id_tramite)) Sticker,
            tr.id_tramite Id_Tramite_Suac,
            ec.es_unanime, ec.es_autoconvocada, ec.es_en_sede,
            to_char(ec.fecha_convocatoria, 'dd/mm/rrrr') fecha_convocatoria,
            to_char(ec.fecha_acta_directorio, 'dd/mm/rrrr') fecha_acta_directorio,
            to_char(ec.fecha_libro_asamblea, 'dd/mm/rrrr') fecha_libro_asamblea,
            ec.publicado_boe, to_char(ec.fecha_primera_boe, 'dd/mm/rrrr') fecha_primera_boe,
            to_char(ec.fecha_ultima_boe, 'dd/mm/rrrr') fecha_ultima_boe,
            ec.publicado_diario, to_char(ec.fecha_primera_diario, 'dd/mm/rrrr') fecha_primera_diario,
            to_char(ec.fecha_ultima_diario, 'dd/mm/rrrr') fecha_ultima_diario,
            to_char(ec.porc_capital_social,  '999990.99') porc_capital_social,
            to_char(ec.quorum_convocatoria1, '999990.99') quorum_convocatoria1,
            to_char(ec.quorum_convocatoria2, '999990.99') quorum_convocatoria2,
            to_char(ec.fecha_cuarto_inter, 'dd/mm/rrrr') fecha_cuarto_inter,
            IPJ.VARIOS.FC_ARMAR_CALLE_DOM(d.n_calle, d.altura, d.piso, d.depto, d.torre, d.mzna, d.lote, d.n_barrio, d.km, d.n_tipocalle) calle_inf,
            d.id_tipodom, d.n_tipodom, d.id_tipocalle, d.n_tipocalle, d.id_calle,
            initcap(d.n_calle) n_calle, d.altura, d.depto, d.piso, d.torre, d.id_barrio,
            initcap(d.n_barrio) n_barrio,
            d.id_localidad, initcap(d.n_localidad) n_localidad, d.id_departamento,
            D.mzna Manzana, D.Lote, d.km,
            initcap(d.n_departamento) n_departamento, d.id_provincia,
            initcap(d.n_provincia) n_provincia, d.cpa,
            (case when nvl(ec.id_vin, 0) = 0 then 1 else 0 end) en_sede,
            (case when ec.Id_Tramite_Ipj = p_Id_Tramite_Ipj then 1 else 0 end) imprimir,
            nvl(tr.expediente, IPJ.TRAMITES_SUAC.FC_BUSCAR_EXPEDIENTE(tr.id_tramite)) Nro_Expediente,
            p_Id_Tramite_Ipj Id_Tramite_Ipj_actual,
            (select N_estado from Ipj.T_Estados es where es.id_estado = Tr.Id_Estado_Ult) N_Estado,
            Tr.Id_Estado_Ult id_estado, tr.id_tramite_ipj_padre,
            decode(to_char(ec.fec_acta,'HH24:Mi'),'00:00', NULL, to_char(ec.fec_acta,'HH24:Mi')) hora_acta
          from ipj.t_entidades_acta ec join ipj.t_tipos_acta ta
              on ta.id_tipo_acta = ec.id_tipo_acta
            join ipj.t_Entidades tpj
              on ec.Id_Tramite_Ipj = tpj.Id_Tramite_Ipj and ec.id_legajo = tpj.id_legajo
            join ipj.t_tramitesipj tr
              on TR.Id_Tramite_Ipj = TPJ.Id_Tramite_Ipj
            left join dom_manager.VT_DOMICILIOS_COND d
              on d.id_vin = ec.id_vin
          where
            tr.id_tramite_ipj_padre = p_id_tramite_ipj and
            tpj.id_legajo = p_id_legajo
        ) tmp
      order by fec_acta desc;

  END SP_Traer_Actas_Inscr;

  PROCEDURE SP_Traer_Otorgantes_Fideo_Inf(
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number,
    p_Cursor OUT TYPES.cursorType)
  IS
  /***************************************************************
    ADVERTENCIA: ESTE SP carga la lista de otorgantes de fideicomisos:
      1) Fiduciario
      2) Fiduciante
      3) Beneficiario
      4) Fideicomisario
  ***************************************************************/
    v_matricula_version number(6,0);
    v_now date;
    v_id_ubicacion_ent number;
    v_id_admin number;
  BEGIN
    -- Busco la version de la matricula, asociada al tramite
    begin
      select nvl(e.matricula_version, 0), te.id_ubicacion
          into v_matricula_version, v_id_ubicacion_ent
      from IPJ.t_entidades e  join ipj.t_tipos_entidades te
        on e.id_tipo_entidad = te.id_tipo_entidad
      where
        e.Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        e.id_legajo = p_id_legajo;
    exception
      when NO_DATA_FOUND then
        v_matricula_version := 0;

        select id_ubicacion into v_id_ubicacion_ent
        from ipj.t_legajos l join ipj.t_tipos_entidades te
          on l.id_tipo_entidad = te.id_tipo_entidad
        where
          l.id_legajo = p_id_legajo;
    end;

    OPEN p_Cursor FOR
      select Cargo, Orden, orden_part, tipo_participacion, Denominacion Detalle,
        cuit_cuil, to_char(fecha_inicio, 'dd/mm/rrrr') fecha_inicio, to_char(fecha_fin, 'dd/mm/rrrr') fecha_fin,
        Matricula
      from
       (--FIDUCIARIOS Personas F�sicas Vigentes
        select 'Fiduciario/a' Cargo, 1 Orden, decode(upper(ef.tipo_participacion), 'TITULAR', 0, 1) orden_part, ef.tipo_participacion,
          p.nombre || ' ' || p.apellido Denominacion,
          p.cuil cuit_cuil, ef.fecha_inicio, ef.fecha_fin,
          to_char(IPJ.VARIOS.FC_Numero_Matricula(tpj.matricula)) || '-' ||replace(IPJ.VARIOS.FC_Letra_Matricula(tpj.matricula), '#', '') || to_char(decode(tpj.matricula_version, 0, null, tpj.matricula_version)) Matricula
        from ipj.t_entidades_fiduciarios ef join ipj.t_integrantes i
            on ef.id_integrante = i.id_integrante
          join ipj.t_Entidades tpj
            on ef.Id_Tramite_Ipj = tpj.Id_Tramite_Ipj and ef.id_legajo = tpj.id_legajo
          left join rcivil.vt_pk_persona p
            on i.id_sexo = p.ID_SEXO and
              i.nro_documento = p.NRO_DOCUMENTO and
              i.pai_cod_pais = p.PAI_COD_PAIS and
              i.id_numero = p.ID_NUMERO
        where
          ef.id_integrante is not null and
          tpj.id_legajo = p_id_legajo and
          tpj.matricula_version <= v_matricula_version and
          (ef.fecha_fin is null OR ef.fecha_fin >= trunc(sysdate))

        UNION ALL

        --FIDUCIARIOS Empresas Vigentes
        select 'Fiduciario/a' Cargo, 1 Orden, decode(upper(ef.tipo_participacion), 'TITULAR', 0, 1) orden_part, ef.tipo_participacion,
          ef.n_empresa Denominacion,
          ef.CUIT_EMPRESA cuit_cuil, ef.fecha_inicio, ef.fecha_fin,
          to_char(IPJ.VARIOS.FC_Numero_Matricula(tpj.matricula)) || '-' ||replace(IPJ.VARIOS.FC_Letra_Matricula(tpj.matricula), '#', '') || to_char(decode(tpj.matricula_version, 0, null, tpj.matricula_version)) Matricula
        from ipj.t_entidades_fiduciarios ef join ipj.t_Entidades tpj
            on ef.Id_Tramite_Ipj = tpj.Id_Tramite_Ipj and ef.id_legajo = tpj.id_legajo
        where
          ef.id_integrante is null and
          tpj.id_legajo = p_id_legajo and
          tpj.matricula_version <= v_matricula_version and
          (ef.fecha_fin is null OR ef.fecha_fin >= trunc(sysdate))

        UNION ALL

        --FIDUCIANTE Personas F�sicas Vigentes
        select 'Fiduciante' Cargo, 2 Orden, decode(upper(ef.tipo_participacion), 'TITULAR', 0, 1) orden_part, ef.tipo_participacion,
          p.nombre || ' ' || p.apellido Denominacion,
          p.cuil cuit_cuil, ef.fecha_inicio, ef.fecha_fin,
          to_char(IPJ.VARIOS.FC_Numero_Matricula(tpj.matricula)) || '-' ||replace(IPJ.VARIOS.FC_Letra_Matricula(tpj.matricula), '#', '') || to_char(decode(tpj.matricula_version, 0, null, tpj.matricula_version)) Matricula
        from ipj.t_entidades_fiduciantes ef join ipj.t_integrantes i
            on ef.id_integrante = i.id_integrante
          join ipj.t_Entidades tpj
            on ef.Id_Tramite_Ipj = tpj.Id_Tramite_Ipj and ef.id_legajo = tpj.id_legajo
          left join rcivil.vt_pk_persona p
            on i.id_sexo = p.ID_SEXO and
              i.nro_documento = p.NRO_DOCUMENTO and
              i.pai_cod_pais = p.PAI_COD_PAIS and
              i.id_numero = p.ID_NUMERO
        where
          ef.id_integrante is not null and
          tpj.id_legajo = p_id_legajo and
          tpj.matricula_version <= v_matricula_version and
          (ef.fecha_fin is null OR ef.fecha_fin >= trunc(sysdate))

        UNION ALL

        --FIDUCIANTE Empresas Vigentes
        select 'Fiduciante' Cargo, 2 Orden, decode(upper(ef.tipo_participacion), 'TITULAR', 0, 1) orden_part, ef.tipo_participacion,
          ef.n_empresa Denominacion,
          ef.CUIT_EMPRESA cuit_cuil, ef.fecha_inicio, ef.fecha_fin,
          to_char(IPJ.VARIOS.FC_Numero_Matricula(tpj.matricula)) || '-' ||replace(IPJ.VARIOS.FC_Letra_Matricula(tpj.matricula), '#', '') || to_char(decode(tpj.matricula_version, 0, null, tpj.matricula_version)) Matricula
        from ipj.t_entidades_fiduciantes ef join ipj.t_Entidades tpj
            on ef.Id_Tramite_Ipj = tpj.Id_Tramite_Ipj and ef.id_legajo = tpj.id_legajo
        where
          ef.id_integrante is null and
          tpj.id_legajo = p_id_legajo and
          tpj.matricula_version <= v_matricula_version and
          (ef.fecha_fin is null OR ef.fecha_fin >= trunc(sysdate))

        UNION ALL

        --BENEFICIARIO Personas F�sicas Vigentes
        select 'Beneficiario/a' Cargo, 3 Orden, decode(upper(ef.tipo_participacion), 'TITULAR', 0, 1) orden_part, ef.tipo_participacion,
          p.nombre || ' ' || p.apellido Denominacion,
          p.cuil cuit_cuil, ef.fecha_inicio, ef.fecha_fin,
          to_char(IPJ.VARIOS.FC_Numero_Matricula(tpj.matricula)) || '-' ||replace(IPJ.VARIOS.FC_Letra_Matricula(tpj.matricula), '#', '') || to_char(decode(tpj.matricula_version, 0, null, tpj.matricula_version)) Matricula
        from ipj.t_entidades_benef ef join ipj.t_integrantes i
            on ef.id_integrante = i.id_integrante
          join ipj.t_Entidades tpj
            on ef.Id_Tramite_Ipj = tpj.Id_Tramite_Ipj and ef.id_legajo = tpj.id_legajo
          left join rcivil.vt_pk_persona p
            on i.id_sexo = p.ID_SEXO and
              i.nro_documento = p.NRO_DOCUMENTO and
              i.pai_cod_pais = p.PAI_COD_PAIS and
              i.id_numero = p.ID_NUMERO
        where
          ef.id_integrante is not null and
          tpj.id_legajo = p_id_legajo and
          tpj.matricula_version <= v_matricula_version and
          (ef.fecha_fin is null OR ef.fecha_fin >= trunc(sysdate))

        UNION ALL

        --BENEFICIARIO Empresas Vigentes
        select 'Beneficiario/a' Cargo, 3 Orden, decode(upper(ef.tipo_participacion), 'TITULAR', 0, 1) orden_part, ef.tipo_participacion,
          ef.n_empresa Denominacion,
          ef.CUIT_EMPRESA cuit_cuil, ef.fecha_inicio, ef.fecha_fin,
          to_char(IPJ.VARIOS.FC_Numero_Matricula(tpj.matricula)) || '-' ||replace(IPJ.VARIOS.FC_Letra_Matricula(tpj.matricula), '#', '') || to_char(decode(tpj.matricula_version, 0, null, tpj.matricula_version)) Matricula
        from ipj.t_entidades_benef ef join ipj.t_Entidades tpj
            on ef.Id_Tramite_Ipj = tpj.Id_Tramite_Ipj and ef.id_legajo = tpj.id_legajo
        where
          ef.id_integrante is null and
          tpj.id_legajo = p_id_legajo and
          tpj.matricula_version <= v_matricula_version and
          (ef.fecha_fin is null OR ef.fecha_fin >= trunc(sysdate))

        UNION ALL

        --FIDEICOMISARIO Personas F�sicas Vigentes
        select 'Fideicomisario/a' Cargo, 4 Orden, decode(upper(ef.tipo_participacion), 'TITULAR', 0, 1) orden_part, ef.tipo_participacion,
          p.nombre || ' ' || p.apellido Denominacion,
          p.cuil cuit_cuil, ef.fecha_inicio, ef.fecha_fin,
          to_char(IPJ.VARIOS.FC_Numero_Matricula(tpj.matricula)) || '-' ||replace(IPJ.VARIOS.FC_Letra_Matricula(tpj.matricula), '#', '') || to_char(decode(tpj.matricula_version, 0, null, tpj.matricula_version)) Matricula
        from ipj.t_entidades_fideicom ef join ipj.t_integrantes i
            on ef.id_integrante = i.id_integrante
          join ipj.t_Entidades tpj
            on ef.Id_Tramite_Ipj = tpj.Id_Tramite_Ipj and ef.id_legajo = tpj.id_legajo
          left join rcivil.vt_pk_persona p
            on i.id_sexo = p.ID_SEXO and
              i.nro_documento = p.NRO_DOCUMENTO and
              i.pai_cod_pais = p.PAI_COD_PAIS and
              i.id_numero = p.ID_NUMERO
        where
          ef.id_integrante is not null and
          tpj.id_legajo = p_id_legajo and
          tpj.matricula_version <= v_matricula_version and
          (ef.fecha_fin is null OR ef.fecha_fin >= trunc(sysdate))

        UNION ALL

        --FIDEICOMISARIO Empresas Vigentes
        select 'Fideicomisario/a' Cargo, 4 Orden, decode(upper(ef.tipo_participacion), 'TITULAR', 0, 1) orden_part, ef.tipo_participacion,
          ef.n_empresa Denominacion,
          ef.CUIT_EMPRESA cuit_cuil, ef.fecha_inicio, ef.fecha_fin,
          to_char(IPJ.VARIOS.FC_Numero_Matricula(tpj.matricula)) || '-' ||replace(IPJ.VARIOS.FC_Letra_Matricula(tpj.matricula), '#', '') || to_char(decode(tpj.matricula_version, 0, null, tpj.matricula_version)) Matricula
        from ipj.t_entidades_fideicom ef join ipj.t_Entidades tpj
            on ef.Id_Tramite_Ipj = tpj.Id_Tramite_Ipj and ef.id_legajo = tpj.id_legajo
        where
          ef.id_integrante is null and
          tpj.id_legajo = p_id_legajo and
          tpj.matricula_version <= v_matricula_version and
          (ef.fecha_fin is null OR ef.fecha_fin >= trunc(sysdate))
      ) tmp
    order by Orden asc, orden_part asc;

  END SP_Traer_Otorgantes_Fideo_Inf;

  PROCEDURE SP_Informar_Gravamenes(
    o_Cursor OUT TYPES.cursorType,
    p_id_legajo in number)
  IS
  /*******************************************************
    Lista todos los gravamenes registrados para una entidad.
  ********************************************************/
  BEGIN
    OPEN o_Cursor FOR
      select g.matricula, g.matricula_version,
        to_char(gil.fecha, 'dd/mm/rrrr') fecha, tg.n_tipo_gravamen, gil.descripcion,
        gil.titulo, gil.il_numero, to_char(gil.il_fecha, 'dd/mm/rrrr') il_fecha,
        (select ti.n_il_tipo from ipj.t_tipos_ins_legal ti where ti.il_tipo = gil.il_tipo) n_il_tipo,
        (select j.n_juzgado  from ipj.t_juzgados j where j.id_juzgado = gil.id_juzgado) n_juzgado,
        (select denominacion_sia from ipj.t_legajos l where l.id_legajo = g.id_legajo) Denominacion,
        ipj.varios.fc_letra_matricula(g.matricula) Matricula_Letra,
        ipj.varios.fc_numero_matricula(g.matricula) Matricula_Nro
      from IPJ.t_gravamenes g join ipj.t_gravamenes_ins_legal gil
          on gil.Id_Tramite_Ipj = g.Id_Tramite_Ipj and
              gil.id_tramiteipj_accion = g.id_tramiteipj_accion and
              gil.id_tipo_accion = g.id_tipo_accion
        join IPJ.T_TIPOS_GRAVAMENES tg
          on GIL.ID_TIPO_GRAVAMEN = TG.ID_TIPO_GRAVAMEN
      where
        g.id_legajo = p_id_legajo and
        gil.fin_id_tramite_ipj is null
      order by gil.fecha desc;

  END SP_Informar_Gravamenes;

  PROCEDURE SP_Informar_Gravamenes_Pers(
    o_Cursor OUT TYPES.cursorType,
    p_id_integrante in number)
  IS
  /*******************************************************
    Lista todos los gravamenes registrados para una Persona F�sica
  ********************************************************/
  BEGIN
    OPEN o_Cursor FOR
      select g.matricula, g.matricula_version,
        to_char(gil.fecha, 'dd/mm/rrrr') fecha, tg.n_tipo_gravamen, gil.descripcion,
        gil.titulo, gil.il_numero, to_char(gil.il_fecha, 'dd/mm/rrrr') il_fecha,
        (select ti.n_il_tipo from ipj.t_tipos_ins_legal ti where ti.il_tipo = gil.il_tipo) n_il_tipo,
        (select j.n_juzgado  from ipj.t_juzgados j where j.id_juzgado = gil.id_juzgado) n_juzgado,
        (select nombre || ' ' || apellido from ipj.t_integrantes i join rcivil.vt_pk_persona p on p.id_sexo = i.id_sexo and i.nro_documento = p.nro_documento and i.pai_cod_pais = p.pai_cod_pais and i.id_numero = p.id_numero where i.id_integrante = g.id_integrante) Denominacion
      from IPJ.t_gravamenes g join ipj.t_gravamenes_ins_legal gil
          on gil.Id_Tramite_Ipj = g.Id_Tramite_Ipj and
              gil.id_tramiteipj_accion = g.id_tramiteipj_accion and
              gil.id_tipo_accion = g.id_tipo_accion
        join IPJ.T_TIPOS_GRAVAMENES tg
          on GIL.ID_TIPO_GRAVAMEN = TG.ID_TIPO_GRAVAMEN
      where
        g.id_integrante = p_id_integrante and
        gil.fin_id_tramite_ipj is null
      order by gil.fecha desc;

  END SP_Informar_Gravamenes_Pers;

  PROCEDURE SP_Inf_Hist_Razon_Social(
    o_Cursor OUT TYPES.cursorType,
    p_id_legajo in number,
    p_anio_desde in number,
    p_anio_hasta in number,
    p_desde_const in char)
  IS
    v_ubicacion_leg number(6);
    v_fecha_desde date;
    v_fecha_hasta date;
  BEGIN
    -- Busco el tipo de Entidad
    begin
      select
        te.id_ubicacion into v_ubicacion_leg
      from ipj.t_legajos l join ipj.t_tipos_entidades te
        on te.id_tipo_entidad = l.id_tipo_entidad
      where
        l.id_legajo = p_id_legajo;
    exception
      when NO_DATA_FOUND then
        v_ubicacion_leg := 0;
    end;

    -- Armo el plado de fechas correcto
    SP_Armar_Rango_Inf_Hist(
      o_fecha_Desde => v_fecha_desde,
      o_fecha_hasta => v_fecha_hasta,
      p_id_legajo => p_id_legajo,
      p_anio_desde => p_anio_desde,
      p_anio_hasta => p_anio_hasta,
      p_desde_Const => p_desde_const,
      p_buscar_anterio => 'S',
      p_datos_entidad => 'S'
    );

    -- Listo los distintos cambios de nombres
    OPEN o_Cursor FOR
      select e.denominacion_1, IPJ.VARIOS.FC_Numero_Matricula(min(e.matricula)) Matricula_nro,
        IPJ.VARIOS.FC_Letra_Matricula(replace(min(e.matricula), '#', '')) Matricula_Letra,
        min(e.matricula_version) Matricula_Version, min(e.folio) Folio, min(decode(e.anio, 0, null, e.anio)) Anio
      from IPJ.T_ENTIDADES e join IPJ.t_tramitesipj t
          on e.Id_Tramite_Ipj = t.Id_Tramite_Ipj
      where
        e.id_legajo = p_id_legajo and
        e.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_ubicacion_leg) and
        e.borrador = 'N' and
        T.ID_ESTADO_ULT between 100 and 199 and-- Estados Completados OK
        e.fecha_versionado between v_fecha_desde and v_fecha_hasta
      group by e.denominacion_1
      order by Matricula_nro desc, matricula_version desc, anio desc, folio desc;
  EXCEPTION
     WHEN OTHERS THEN
       IPJ.VARIOS.SP_GUARDAR_LOG(
         p_METODO => 'SP_Inf_Hist_Razon_Social',
         p_NIVEL => 'Error',
         p_ORIGEN => 'DB',
         p_USR_LOG  => 'DB',
         p_USR_WINDOWS => 'Sistema',
         p_IP_PC  => null,
         p_EXCEPCION =>
           'Id Legajo = ' || to_char(p_id_legajo)
           || ' / A�o Desde = ' || to_char(p_anio_desde)
           || ' / A�os Hasta = ' || to_char(p_anio_hasta)
           || ' / Desde Const. = ' || p_desde_const
           || ' / ERROR = ' || To_Char(SQLCODE) || '-' || SQLERRM
       );

       -- Armo un cursor vacio para que no explote el informe
       OPEN o_Cursor FOR
         select null denominacion_1, null Matricula_nro, null Matricula_Letra, null Matricula_Version, null Folio, null Anio
         from dual;
  END SP_Inf_Hist_Razon_Social;

  PROCEDURE SP_Inf_Hist_Objeto(
    o_Cursor OUT TYPES.cursorType,
    p_id_legajo in number,
    p_anio_desde in number,
    p_anio_hasta in number,
    p_desde_const in char)
  IS
    v_ubicacion_leg number(6);
    v_fecha_desde date;
    v_fecha_hasta date;
  BEGIN
    -- Busco el tipo de Entidad
    begin
      select
        te.id_ubicacion into v_ubicacion_leg
      from ipj.t_legajos l join ipj.t_tipos_entidades te
        on te.id_tipo_entidad = l.id_tipo_entidad
      where
        l.id_legajo = p_id_legajo;
    exception
      when NO_DATA_FOUND then
        v_ubicacion_leg := 0;
    end;

    -- Armo el plado de fechas correcto
    SP_Armar_Rango_Inf_Hist(
      o_fecha_Desde => v_fecha_desde,
      o_fecha_hasta => v_fecha_hasta,
      p_id_legajo => p_id_legajo,
      p_anio_desde => p_anio_desde,
      p_anio_hasta => p_anio_hasta,
      p_desde_Const => p_desde_const,
      p_buscar_anterio => 'S',
      p_datos_entidad => 'S'
    );

    -- Listo los distintos objetos sociales de la entidad
    OPEN o_Cursor FOR
      -- Con este select sobre las versiones, busco los objetos para mostrarlos.
      select t.*,
      ( select objeto_social
        from ipj.t_entidades ee join ipj.t_tramitesipj tr on ee.id_tramite_ipj = tr.id_tramite_ipj
        where
          ee.id_legajo = p_id_legajo and
          ee.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_ubicacion_leg) and
          ee.borrador = 'N'and
          tr.id_estado_ult between 100 and 199 and
          ee.fecha_versionado between v_fecha_desde and v_fecha_hasta and
          to_char(DBMS_LOB.SUBSTR(ee.objeto_social, 4000, 1)) = t.objeto_breve and
          length(ee.objeto_social) = t.Long_Objeto and
          IPJ.VARIOS.FC_Numero_Matricula(ee.matricula) = t.Matricula_nro and
          IPJ.VARIOS.FC_Letra_Matricula(replace(ee.matricula, '#', '')) = t.Matricula_Letra and
          nvl(ee.folio, '@') = nvl(t.Folio, '@') and
          nvl(ee.anio, 0) = nvl(t.Anio, 0)
          and rownum = 1
        ) objeto_social
    from
      -- Ese es el select para ver las versiones de los objetos (por tama�o y os primeros 4000 caracteres)
      ( select IPJ.VARIOS.FC_Numero_Matricula(min(e.matricula)) Matricula_nro,
          IPJ.VARIOS.FC_Letra_Matricula(replace(min(e.matricula), '#', '')) Matricula_Letra,  min(e.matricula_version) Matricula_Version, min(e.folio) Folio, min(decode(e.anio, 0, null, e.anio)) Anio,
          to_char(DBMS_LOB.SUBSTR(e.objeto_social, 4000, 1)) Objeto_Breve, length(e.objeto_social) Long_Objeto
        from IPJ.T_ENTIDADES e join IPJ.t_tramitesipj t on e.Id_Tramite_Ipj = t.Id_Tramite_Ipj
        where
          e.id_legajo = p_id_legajo and
          e.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_ubicacion_leg) and
          e.borrador = 'N' and
          t.id_estado_ult between 100 and 199
          and e.fecha_versionado between v_fecha_desde and v_fecha_hasta
        group by to_char(DBMS_LOB.SUBSTR(e.objeto_social, 4000, 1)), length(e.objeto_social)
      )t
    order by Matricula_nro desc, matricula_version desc, anio desc, folio desc;

  EXCEPTION
    when OTHERS then
      IPJ.VARIOS.SP_GUARDAR_LOG(
        p_METODO => 'SP_Inf_Hist_Objeto',
        p_NIVEL => 'Error',
        p_ORIGEN => 'DB',
        p_USR_LOG  => 'DB',
        p_USR_WINDOWS => 'Sistema',
        p_IP_PC  => null,
        p_EXCEPCION =>
          'Id Legajo = ' || to_char(p_id_legajo)
          || ' / A�o Desde = ' || to_char(p_anio_desde)
          || ' / A�os Hasta = ' || to_char(p_anio_hasta)
          || ' / Desde Const. = ' || p_desde_const
          || ' / ERROR = ' || To_Char(SQLCODE) || '-' || SQLERRM
      );
      -- Si falla algo, muestro un cursor vacio para que no explote el SP y se muestre el informe
      OPEN o_Cursor FOR
        select null objeto_social, null Matricula_nro, null Matricula_Letra, null Matricula_Version, null Folio, null Anio from dual;
  END SP_Inf_Hist_Objeto;

  PROCEDURE SP_Inf_Hist_Cierre_Ejerc(
    o_Cursor OUT TYPES.cursorType,
    p_id_legajo in number,
    p_anio_desde in number,
    p_anio_hasta in number,
    p_desde_const in char)
  IS
    v_ubicacion_leg number(6);
    v_fecha_desde date;
    v_fecha_hasta date;
  BEGIN
    -- Busco el tipo de Entidad
    begin
      select
        te.id_ubicacion into v_ubicacion_leg
      from ipj.t_legajos l join ipj.t_tipos_entidades te
        on te.id_tipo_entidad = l.id_tipo_entidad
      where
        l.id_legajo = p_id_legajo;
    exception
      when NO_DATA_FOUND then
        v_ubicacion_leg := 0;
    end;

    -- Armo el plado de fechas correcto
    SP_Armar_Rango_Inf_Hist(
      o_fecha_Desde => v_fecha_desde,
      o_fecha_hasta => v_fecha_hasta,
      p_id_legajo => p_id_legajo,
      p_anio_desde => p_anio_desde,
      p_anio_hasta => p_anio_hasta,
      p_desde_Const => p_desde_const,
      p_buscar_anterio => 'S',
      p_datos_entidad => 'S'
    );

    -- Listo los distintos cierres de ejercicio (Fecha o Fin de mes)
    OPEN o_Cursor FOR
      select
        to_char(decode(e.cierre_fin_mes, 'S', to_Date(null), to_date(to_char(e.cierre_ejercicio, 'dd/mm') || '/' || to_char(sysdate, 'rrrr'), 'dd/mm/rrrr')), 'dd/mm/rrrr') Cierre_Ejercicio,
        e.cierre_fin_mes,
        IPJ.VARIOS.FC_Numero_Matricula(min(e.matricula)) Matricula_nro,
        IPJ.VARIOS.FC_Letra_Matricula(replace(min(e.matricula), '#', '')) Matricula_Letra,
        min(e.matricula_version) Matricula_Version, min(e.folio) Folio, min(decode(e.anio, 0, null, e.anio)) Anio
      from IPJ.T_ENTIDADES e join IPJ.t_tramitesipj t
          on e.Id_Tramite_Ipj = t.Id_Tramite_Ipj
      where
        e.id_legajo = p_id_legajo and
        e.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_ubicacion_leg) and
        e.borrador = 'N' and
        T.ID_ESTADO_ULT between 100 and 199 and-- Estados Completados OK
        e.fecha_versionado between v_fecha_desde and v_fecha_hasta
      group by decode(e.cierre_fin_mes, 'S', to_date(null), to_date(to_char(e.cierre_ejercicio, 'dd/mm') || '/' || to_char(sysdate, 'rrrr'), 'dd/mm/rrrr')), e.cierre_fin_mes
      order by Matricula_nro desc, matricula_version desc, anio desc, folio desc;
  EXCEPTION
     WHEN OTHERS THEN
       IPJ.VARIOS.SP_GUARDAR_LOG(
         p_METODO => 'SP_Inf_Hist_Cierre_Ejerc',
         p_NIVEL => 'Error',
         p_ORIGEN => 'DB',
         p_USR_LOG  => 'DB',
         p_USR_WINDOWS => 'Sistema',
         p_IP_PC  => null,
         p_EXCEPCION =>
           'Id Legajo = ' || to_char(p_id_legajo)
           || ' / A�o Desde = ' || to_char(p_anio_desde)
           || ' / A�os Hasta = ' || to_char(p_anio_hasta)
           || ' / Desde Const. = ' || p_desde_const
           || ' / ERROR = ' || To_Char(SQLCODE) || '-' || SQLERRM
       );

       -- Armo un cursor vacio para que no explote el informe
       OPEN o_Cursor FOR
         select null Cierre_Ejercicio, null cierre_fin_mes, null Matricula_nro, null Matricula_Letra, null Matricula_Version, null Folio, null Anio
         from dual;
  END SP_Inf_Hist_Cierre_Ejerc;

  PROCEDURE SP_Inf_Hist_Sede(
    o_Cursor OUT TYPES.cursorType,
    p_id_legajo in number,
    p_anio_desde in number,
    p_anio_hasta in number,
    p_desde_const in char)
  IS
    v_ubicacion_leg number(6);
    v_fecha_desde date;
    v_fecha_hasta date;
  BEGIN
    -- Busco el tipo de Entidad
    begin
      select
        te.id_ubicacion into v_ubicacion_leg
      from ipj.t_legajos l join ipj.t_tipos_entidades te
        on te.id_tipo_entidad = l.id_tipo_entidad
      where
        l.id_legajo = p_id_legajo;
    exception
      when NO_DATA_FOUND then
        v_ubicacion_leg := 0;
    end;

    -- Armo el plado de fechas correcto
    SP_Armar_Rango_Inf_Hist(
      o_fecha_Desde => v_fecha_desde,
      o_fecha_hasta => v_fecha_hasta,
      p_id_legajo => p_id_legajo,
      p_anio_desde => p_anio_desde,
      p_anio_hasta => p_anio_hasta,
      p_desde_Const => p_desde_const,
      p_buscar_anterio => 'S',
      p_datos_entidad => 'S'
    );

    -- Listo los distintos domicilios
    OPEN o_Cursor FOR
      select e.id_vin_real , IPJ.VARIOS.FC_Numero_Matricula(min(e.matricula)) Matricula_nro,
        IPJ.VARIOS.FC_Letra_Matricula(replace(min(e.matricula), '#', '')) Matricula_Letra,
        min(e.matricula_version) Matricula_Version, min(e.folio) Folio, min(decode(e.anio, 0, null, e.anio)) Anio,
        (select d.n_provincia from dom_manager.vt_domicilios_cond d where d.id_vin = e.id_vin_real) Provincia,
        (select d.n_departamento from dom_manager.vt_domicilios_cond d where d.id_vin = e.id_vin_real) Departamento,
        (select d.n_localidad from dom_manager.vt_domicilios_cond d where d.id_vin = e.id_vin_real) Localidad,
        IPJ.VARIOS.FC_Armar_Calle_Dom_Id(e.id_vin_real) Calle_Inf
      from IPJ.T_ENTIDADES e join IPJ.t_tramitesipj t
          on e.Id_Tramite_Ipj = t.Id_Tramite_Ipj
      where
        e.id_legajo = p_id_legajo and
        e.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_ubicacion_leg) and
        e.borrador = 'N' and
        T.ID_ESTADO_ULT between 100 and 199 and-- Estados Completados OK
        e.fecha_versionado between v_fecha_desde and v_fecha_hasta
      group by e.id_vin_real
      order by Matricula_nro desc, matricula_version desc, anio desc, folio desc;
  EXCEPTION
     WHEN OTHERS THEN
       IPJ.VARIOS.SP_GUARDAR_LOG(
         p_METODO => 'SP_Inf_Hist_Sede',
         p_NIVEL => 'Error',
         p_ORIGEN => 'DB',
         p_USR_LOG  => 'DB',
         p_USR_WINDOWS => 'Sistema',
         p_IP_PC  => null,
         p_EXCEPCION =>
           'Id Legajo = ' || to_char(p_id_legajo)
           || ' / A�o Desde = ' || to_char(p_anio_desde)
           || ' / A�os Hasta = ' || to_char(p_anio_hasta)
           || ' / Desde Const. = ' || p_desde_const
           || ' / ERROR = ' || To_Char(SQLCODE) || '-' || SQLERRM
       );

       -- Armo un cursor vacio para que no explote el informe
       OPEN o_Cursor FOR
         select null id_vin_real , null Matricula_nro, null Matricula_Letra, null Matricula_Version, null Folio, null Anio, null Provincia, null Departamento,
           null Localidad, null Calle_Inf
         from dual;
  END SP_Inf_Hist_Sede;

  PROCEDURE SP_Inf_Hist_Vigencia(
    o_Cursor OUT TYPES.cursorType,
    p_id_legajo in number,
    p_anio_desde in number,
    p_anio_hasta in number,
    p_desde_const in char)
  IS
    v_ubicacion_leg number(6);
    v_fecha_desde date;
    v_fecha_hasta date;
  BEGIN
    -- Busco el tipo de Entidad
    begin
      select
        te.id_ubicacion into v_ubicacion_leg
      from ipj.t_legajos l join ipj.t_tipos_entidades te
        on te.id_tipo_entidad = l.id_tipo_entidad
      where
        l.id_legajo = p_id_legajo;
    exception
      when NO_DATA_FOUND then
        v_ubicacion_leg := 0;
    end;

    -- Armo el plado de fechas correcto
    SP_Armar_Rango_Inf_Hist(
      o_fecha_Desde => v_fecha_desde,
      o_fecha_hasta => v_fecha_hasta,
      p_id_legajo => p_id_legajo,
      p_anio_desde => p_anio_desde,
      p_anio_hasta => p_anio_hasta,
      p_desde_Const => p_desde_const,
      p_buscar_anterio => 'S',
      p_datos_entidad => 'S'
    );

    -- Listo los distintos cambios de vigencias_ duraci�n, tipo, desde o hasta
    OPEN o_Cursor FOR
      select e.vigencia, e.tipo_vigencia, to_char(e.fec_vig, 'dd/mm/rrrr') fec_vig, to_char(e.fec_vig_hasta, 'dd/mm/rrrr') fec_vig_hasta,
        IPJ.VARIOS.FC_Numero_Matricula(min(e.matricula)) Matricula_nro,
        IPJ.VARIOS.FC_Letra_Matricula(replace(min(e.matricula), '#', '')) Matricula_Letra,
        min(e.matricula_version) Matricula_Version, min(e.folio) Folio, min(decode(e.anio, 0, null, e.anio)) Anio,
        (select n_tipo_vigencia from ipj.t_tipos_vigencia v where v.id_tipo_vigencia = e.tipo_vigencia) n_tipo_vigencia
      from IPJ.T_ENTIDADES e join IPJ.t_tramitesipj t
          on e.Id_Tramite_Ipj = t.Id_Tramite_Ipj
      where
        e.id_legajo = p_id_legajo and
        e.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_ubicacion_leg) and
        e.borrador = 'N' and
        T.ID_ESTADO_ULT between 100 and 199 and-- Estados Completados OK
        e.fecha_versionado between v_fecha_desde and v_fecha_hasta
      group by e.vigencia, e.tipo_vigencia, e.fec_vig, e.fec_vig_hasta
      order by Matricula_nro desc, matricula_version desc, anio desc, folio desc;
  EXCEPTION
     WHEN OTHERS THEN
       IPJ.VARIOS.SP_GUARDAR_LOG(
         p_METODO => 'SP_Inf_Hist_Tipo_Admin',
         p_NIVEL => 'Error',
         p_ORIGEN => 'DB',
         p_USR_LOG  => 'DB',
         p_USR_WINDOWS => 'Sistema',
         p_IP_PC  => null,
         p_EXCEPCION =>
           'Id Legajo = ' || to_char(p_id_legajo)
           || ' / A�o Desde = ' || to_char(p_anio_desde)
           || ' / A�os Hasta = ' || to_char(p_anio_hasta)
           || ' / Desde Const. = ' || p_desde_const
           || ' / ERROR = ' || To_Char(SQLCODE) || '-' || SQLERRM
       );

       -- Armo un cursor vacio para que no explote el Informe
       OPEN o_Cursor FOR
         select null vigencia, null tipo_vigencia, null fec_vig, null fec_vig_hasta, null Matricula_nro, null Matricula_Letra,
           null Matricula_Version, null Folio, null Anio, null n_tipo_vigencia
         from dual;
  END SP_Inf_Hist_Vigencia;

  PROCEDURE SP_Inf_Hist_Capital(
    o_Cursor OUT TYPES.cursorType,
    p_id_legajo in number,
    p_anio_desde in number,
    p_anio_hasta in number,
    p_desde_const in char)
  IS
    v_ubicacion_leg number(6);
    v_fecha_desde date;
    v_fecha_hasta date;
  BEGIN
    -- Busco el tipo de Entidad
    begin
      select
        te.id_ubicacion into v_ubicacion_leg
      from ipj.t_legajos l join ipj.t_tipos_entidades te
        on te.id_tipo_entidad = l.id_tipo_entidad
      where
        l.id_legajo = p_id_legajo;
    exception
      when NO_DATA_FOUND then
        v_ubicacion_leg := 0;
    end;

    -- Armo el plado de fechas correcto
    SP_Armar_Rango_Inf_Hist(
      o_fecha_Desde => v_fecha_desde,
      o_fecha_hasta => v_fecha_hasta,
      p_id_legajo => p_id_legajo,
      p_anio_desde => p_anio_desde,
      p_anio_hasta => p_anio_hasta,
      p_desde_Const => p_desde_const,
      p_buscar_anterio => 'S',
      p_datos_entidad => 'S'
    );

    -- Listo los distintos cambios capital social: Monto, Moneda, acciones, valor
    OPEN o_Cursor FOR
      select e.id_moneda, e.cuotas,
        trim(To_Char(nvl(e.monto, '0'), '99999999999999999990')) monto,
        trim(To_Char(nvl(e.valor_cuota, '0'), '99999999999999999990.99')) valor_cuota,
        (select initcap(nvl(m.n_moneda_plural, m.n_moneda)) from t_comunes.t_monedas m where m.id_moneda =  e.id_moneda) n_moneda,
        (select nvl(m.signo, '$') from t_comunes.t_monedas m where m.id_moneda =  e.id_moneda) signo_moneda,
        IPJ.TYPES.fc_numero_a_letras(e.Monto) Monto_Letras,
        IPJ.TYPES.fc_numero_a_letras(e.valor_cuota) Valor_Cuotas_Letras,
        IPJ.TYPES.fc_numero_a_letras(e.cuotas) Cuotas_Letras,
        IPJ.VARIOS.FC_Numero_Matricula(min(e.matricula)) Matricula_nro,
        IPJ.VARIOS.FC_Letra_Matricula(replace(min(e.matricula), '#', '')) Matricula_Letra,
        min(e.matricula_version) Matricula_Version, min(e.folio) Folio, min(decode(e.anio, 0, null, e.anio)) Anio
      from IPJ.T_ENTIDADES e join IPJ.t_tramitesipj t
          on e.Id_Tramite_Ipj = t.Id_Tramite_Ipj
      where
        e.id_legajo = p_id_legajo and
        e.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_ubicacion_leg) and
        e.borrador = 'N' and
        T.ID_ESTADO_ULT between 100 and 199 and-- Estados Completados OK
        e.fecha_versionado between v_fecha_desde and v_fecha_hasta
      group by e.id_moneda, e.monto, e.cuotas, e.valor_cuota
      order by Matricula_nro desc, matricula_version desc, anio desc, folio desc;
  EXCEPTION
     WHEN OTHERS THEN
       IPJ.VARIOS.SP_GUARDAR_LOG(
         p_METODO => 'SP_Inf_Hist_Capital',
         p_NIVEL => 'Error',
         p_ORIGEN => 'DB',
         p_USR_LOG  => 'DB',
         p_USR_WINDOWS => 'Sistema',
         p_IP_PC  => null,
         p_EXCEPCION =>
           'Id Legajo = ' || to_char(p_id_legajo)
           || ' / A�o Desde = ' || to_char(p_anio_desde)
           || ' / A�os Hasta = ' || to_char(p_anio_hasta)
           || ' / Desde Const. = ' || p_desde_const
           || ' / ERROR = ' || To_Char(SQLCODE) || '-' || SQLERRM
       );

       -- Armo un cursor vacio para que no falle el informe
       OPEN o_Cursor FOR
         select null id_moneda, null cuotas, null monto, null valor_cuota, null n_moneda, null signo_moneda, null Monto_Letras, null Valor_Cuotas_Letras,
           null Cuotas_Letras,   null Matricula_nro, null Matricula_Letra, null Matricula_Version, null Folio, null Anio
         from dual;
  END SP_Inf_Hist_Capital;

  PROCEDURE SP_Inf_Hist_Tipo_Admin(
    o_Cursor OUT TYPES.cursorType,
    p_id_legajo in number,
    p_anio_desde in number,
    p_anio_hasta in number,
    p_desde_const in char)
  IS
    v_ubicacion_leg number(6);
    v_fecha_desde date;
    v_fecha_hasta date;
  BEGIN
    -- Busco el tipo de Entidad
    begin
      select
        te.id_ubicacion into v_ubicacion_leg
      from ipj.t_legajos l join ipj.t_tipos_entidades te
        on te.id_tipo_entidad = l.id_tipo_entidad
      where
        l.id_legajo = p_id_legajo;
    exception
      when NO_DATA_FOUND then
        v_ubicacion_leg := 0;
    end;

    -- Armo el plado de fechas correcto
    SP_Armar_Rango_Inf_Hist(
      o_fecha_Desde => v_fecha_desde,
      o_fecha_hasta => v_fecha_hasta,
      p_id_legajo => p_id_legajo,
      p_anio_desde => p_anio_desde,
      p_anio_hasta => p_anio_hasta,
      p_desde_Const => p_desde_const,
      p_buscar_anterio => 'S',
      p_datos_entidad => 'S'
    );

    -- Listo los distintos  tipos de administracion
    OPEN o_Cursor FOR
      select e.id_tipo_administracion,
        (select a.n_tipo_administracion from ipj.t_tipos_administracion a where a.id_tipo_administracion = e.id_tipo_administracion ) n_tipo_administracion,
        IPJ.VARIOS.FC_Numero_Matricula(min(e.matricula)) Matricula_nro,
        IPJ.VARIOS.FC_Letra_Matricula(replace(min(e.matricula), '#', '')) Matricula_Letra,
        min(e.matricula_version) Matricula_Version, min(e.folio) Folio, min(decode(e.anio, 0, null, e.anio)) Anio
      from IPJ.T_ENTIDADES e join IPJ.t_tramitesipj t
          on e.Id_Tramite_Ipj = t.Id_Tramite_Ipj
      where
        e.id_legajo = p_id_legajo and
        e.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_ubicacion_leg) and
        e.borrador = 'N' and
        T.ID_ESTADO_ULT between 100 and 199 and-- Estados Completados OK
        e.fecha_versionado between v_fecha_desde and v_fecha_hasta
      group by e.id_tipo_administracion
      order by Matricula_nro desc, matricula_version desc, anio desc, folio desc;
  EXCEPTION
     WHEN OTHERS THEN
       IPJ.VARIOS.SP_GUARDAR_LOG(
         p_METODO => 'SP_Inf_Hist_Tipo_Admin',
         p_NIVEL => 'Error',
         p_ORIGEN => 'DB',
         p_USR_LOG  => 'DB',
         p_USR_WINDOWS => 'Sistema',
         p_IP_PC  => null,
         p_EXCEPCION =>
           'Id Legajo = ' || to_char(p_id_legajo)
           || ' / A�o Desde = ' || to_char(p_anio_desde)
           || ' / A�os Hasta = ' || to_char(p_anio_hasta)
           || ' / Desde Const. = ' || p_desde_const
           || ' / ERROR = ' || To_Char(SQLCODE) || '-' || SQLERRM
       );

       -- Armo un cursor vacio para que no explote el SP
       OPEN o_Cursor FOR
         select null id_tipo_administracion, null n_tipo_administracion, null Matricula_nro, null Matricula_Letra, null Matricula_Version, null Folio, null Anio
         from dual;
  END SP_Inf_Hist_Tipo_Admin;

  PROCEDURE SP_Inf_Hist_Admin(
    o_Cursor OUT TYPES.cursorType,
    p_id_legajo in number,
    p_anio_desde in number,
    p_anio_hasta in number,
    p_desde_const in char)
  IS
    v_ubicacion_leg number(6);
    v_fecha_desde date;
    v_fecha_hasta date;
    v_Ult_Fec_Admin date;
  BEGIN
    -- Busco el tipo de Entidad
    begin
      select
        te.id_ubicacion into v_ubicacion_leg
      from ipj.t_legajos l join ipj.t_tipos_entidades te
        on te.id_tipo_entidad = l.id_tipo_entidad
      where
        l.id_legajo = p_id_legajo;
    exception
      when NO_DATA_FOUND then
        v_ubicacion_leg := 0;
    end;

    -- Armo el plazo de fechas correcto
    SP_Armar_Rango_Inf_Hist(
      o_fecha_Desde => v_fecha_desde,
      o_fecha_hasta => v_fecha_hasta,
      p_id_legajo => p_id_legajo,
      p_anio_desde => p_anio_desde,
      p_anio_hasta => p_anio_hasta,
      p_desde_Const => p_desde_const,
      p_buscar_anterio => 'N',
      p_datos_entidad => 'N'
    );

    -- Para las SxA, si no hay autoridades se informa desde la �ltima registrada.
    if v_ubicacion_leg = 4 then
      -- Busco la maxima fecha de autoridades registradas
      select max(a.fecha_fin) into v_Ult_Fec_Admin
      from ipj.t_entidades_admin a join IPJ.T_ENTIDADES e
          on a.id_tramite_ipj = e.id_tramite_ipj and a.id_legajo = e.id_legajo
        join IPJ.t_tramitesipj t
          on e.Id_Tramite_Ipj = t.Id_Tramite_Ipj
      where
        e.id_legajo = p_id_legajo and
        e.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_ubicacion_leg) and
        e.borrador = 'N' and
        T.ID_ESTADO_ULT between 100 and 199; -- Estados Completados OK

      -- Si el inicio es mayor que la ultima autoridad, entonces informo la ultima autoridad
      if v_fecha_desde > nvl(v_Ult_Fec_Admin, v_fecha_desde) then
        v_fecha_desde := v_Ult_Fec_Admin;
      end if;
    end if;

    -- Listo los distintos administradores
    OPEN o_Cursor FOR
      select IPJ.VARIOS.FC_Numero_Matricula(e.matricula) Matricula_nro,
        replace(IPJ.VARIOS.FC_Letra_Matricula(e.matricula), '#', '') Matricula_Letra,
        e.matricula_version Matricula_Version, e.folio, e.anio,
        ti.n_tipo_integrante, to_char (a.fecha_inicio, 'dd/mm/rrrr') fecha_inicio,
        to_char (a.fecha_fin, 'dd/mm/rrrr') fecha_fin, i.nro_documento, a.cuit_empresa,
        (select Cuil from rcivil.vt_pk_persona p where P.ID_SEXO = i.id_sexo and p.nro_documento = i.nro_documento and p.pai_cod_pais = i.pai_cod_pais and p.id_numero = i.id_numero) Cuil,
        (select nombre || ' ' || apellido from rcivil.vt_pk_persona p where P.ID_SEXO = i.id_sexo and p.nro_documento = i.nro_documento and p.pai_cod_pais = i.pai_cod_pais and p.id_numero = i.id_numero) Nombre,
        A.N_EMPRESA
      from ipj.t_entidades_admin a join IPJ.T_ENTIDADES e
          on a.id_tramite_ipj = e.id_tramite_ipj and a.id_legajo = e.id_legajo
        join IPJ.t_tramitesipj t
          on e.Id_Tramite_Ipj = t.Id_Tramite_Ipj
        join ipj.t_tipos_integrante ti
          on a.id_tipo_integrante = ti.id_tipo_integrante
        left join ipj.t_tipos_organismo o
          on a.id_tipo_organismo = o.id_tipo_organismo
        left join ipj.t_integrantes i
          on a.id_integrante = i.id_integrante
      WHERE e.id_legajo = p_id_legajo and
           e.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_ubicacion_leg) and
           e.borrador = 'N' and
           T.ID_ESTADO_ULT between 100 and 199 and-- Estados Completados OK
           ( a.fecha_inicio between v_fecha_desde and v_fecha_hasta or
             nvl(a.fecha_fin, (CASE
                                WHEN a.fecha_inicio > v_fecha_hasta THEN SYSDATE
                                ELSE v_fecha_hasta
                                END)) between v_fecha_desde and v_fecha_hasta
       )
      order by e.matricula desc, e.matricula_version desc, anio desc, folio desc, o.nro_orden asc, ti.nro_orden asc;
  EXCEPTION
     WHEN OTHERS THEN
       IPJ.VARIOS.SP_GUARDAR_LOG(
         p_METODO => 'SP_Inf_Hist_Admin',
         p_NIVEL => 'Error',
         p_ORIGEN => 'DB',
         p_USR_LOG  => 'DB',
         p_USR_WINDOWS => 'Sistema',
         p_IP_PC  => null,
         p_EXCEPCION =>
           'Id Legajo = ' || to_char(p_id_legajo)
           || ' / A�o Desde = ' || to_char(p_anio_desde)
           || ' / A�os Hasta = ' || to_char(p_anio_hasta)
           || ' / Desde Const. = ' || p_desde_const
           || ' / ERROR = ' || To_Char(SQLCODE) || '-' || SQLERRM
       );

       -- Armo un cursor vacio para que no falle el informe
      OPEN o_Cursor FOR
        select null Matricula_nro, null Matricula_Letra, null Matricula_Version, null folio, null anio, null n_tipo_integrante, null fecha_inicio,
          null fecha_fin, null nro_documento, null cuit_empresa, null Cuil, null Nombre, null N_EMPRESA
        from dual;
  END SP_Inf_Hist_Admin;

  PROCEDURE SP_Inf_Hist_Repres(
    o_Cursor OUT TYPES.cursorType,
    p_id_legajo in number,
    p_anio_desde in number,
    p_anio_hasta in number,
    p_desde_const in char)
  IS
    v_ubicacion_leg number(6);
    v_fecha_desde date;
    v_fecha_hasta date;
    v_Ult_Fec_Admin date;
  BEGIN
    -- Busco el tipo de Entidad
    begin
      select
        te.id_ubicacion into v_ubicacion_leg
      from ipj.t_legajos l join ipj.t_tipos_entidades te
        on te.id_tipo_entidad = l.id_tipo_entidad
      where
        l.id_legajo = p_id_legajo;
    exception
      when NO_DATA_FOUND then
        v_ubicacion_leg := 0;
    end;

    -- Armo el plado de fechas correcto
    SP_Armar_Rango_Inf_Hist(
      o_fecha_Desde => v_fecha_desde,
      o_fecha_hasta => v_fecha_hasta,
      p_id_legajo => p_id_legajo,
      p_anio_desde => p_anio_desde,
      p_anio_hasta => p_anio_hasta,
      p_desde_Const => p_desde_const,
      p_buscar_anterio => 'N',
      p_datos_entidad => 'N'
    );

    -- Para las SxA, si no hay autoridades se informa desde la �ltima registrada.
    if v_ubicacion_leg = 4 then
      -- Busco la maxima fecha de autoridades registradas
      select max(a.fecha_fin) into v_Ult_Fec_Admin
      from ipj.t_entidades_admin a join IPJ.T_ENTIDADES e
          on a.id_tramite_ipj = e.id_tramite_ipj and a.id_legajo = e.id_legajo
        join IPJ.t_tramitesipj t
          on e.Id_Tramite_Ipj = t.Id_Tramite_Ipj
      where
        e.id_legajo = p_id_legajo and
        e.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_ubicacion_leg) and
        e.borrador = 'N' and
        T.ID_ESTADO_ULT between 100 and 199; -- Estados Completados OK

      -- Si el inicio es mayor que la ultima autoridad, entonces informo la ultima autoridad
      if v_fecha_desde > nvl(v_Ult_Fec_Admin, v_fecha_desde) then
        v_fecha_desde := v_Ult_Fec_Admin;
      end if;
    end if;

    -- Listo los distintos representantes
    OPEN o_Cursor FOR
      select IPJ.VARIOS.FC_Numero_Matricula(e.matricula) Matricula_Nro, replace(IPJ.VARIOS.FC_Letra_Matricula(e.matricula), '#', '') Matricula_Letra,
        e.matricula_version Matricula_Version, e.folio, e.anio,
        to_char (a.fecha_alta, 'dd/mm/rrrr') fecha_alta,
        to_char (a.fecha_baja, 'dd/mm/rrrr') fecha_baja, i.nro_documento,
        (select Cuil from rcivil.vt_pk_persona p where P.ID_SEXO = i.id_sexo and p.nro_documento = i.nro_documento and p.pai_cod_pais = i.pai_cod_pais and p.id_numero = i.id_numero) Cuil,
        (select nombre || ' ' || apellido from rcivil.vt_pk_persona p where P.ID_SEXO = i.id_sexo and p.nro_documento = i.nro_documento and p.pai_cod_pais = i.pai_cod_pais and p.id_numero = i.id_numero) Nombre
      from ipj.t_entidades_representante a join IPJ.T_ENTIDADES e
          on a.id_tramite_ipj = e.id_tramite_ipj and a.id_legajo = e.id_legajo
        join IPJ.t_tramitesipj t
          on e.Id_Tramite_Ipj = t.Id_Tramite_Ipj
        join ipj.t_integrantes i
          on a.id_integrante = i.id_integrante
      where
        e.id_legajo = p_id_legajo and
        e.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_ubicacion_leg) and
        e.borrador = 'N' and
        T.ID_ESTADO_ULT between 100 and 199 and-- Estados Completados OK
        ( a.fecha_baja between v_fecha_desde and v_fecha_hasta or
          nvl(a.fecha_baja, sysdate) between v_fecha_desde and v_fecha_hasta
        )
      order by e.matricula desc, e.matricula_version desc, anio desc, folio desc;
  EXCEPTION
     WHEN OTHERS THEN
       IPJ.VARIOS.SP_GUARDAR_LOG(
         p_METODO => 'SP_Inf_Hist_Repres',
         p_NIVEL => 'Error',
         p_ORIGEN => 'DB',
         p_USR_LOG  => 'DB',
         p_USR_WINDOWS => 'Sistema',
         p_IP_PC  => null,
         p_EXCEPCION =>
           'Id Legajo = ' || to_char(p_id_legajo)
           || ' / A�o Desde = ' || to_char(p_anio_desde)
           || ' / A�os Hasta = ' || to_char(p_anio_hasta)
           || ' / Desde Const. = ' || p_desde_const
           || ' / ERROR = ' || To_Char(SQLCODE) || '-' || SQLERRM
       );

       -- Se arma un cursor vacio para que no explote el informe
       OPEN o_Cursor FOR
         select null Matricula_Nro, null Matricula_Letra, null Matricula_Version, null folio, null anio, null fecha_alta, null fecha_baja, null nro_documento,
           null Cuil, null Nombre
         from dual;
  END SP_Inf_Hist_Repres;

  PROCEDURE SP_Inf_Hist_Socios(
    o_Cursor OUT TYPES.cursorType,
    p_id_legajo in number,
    p_anio_desde in number,
    p_anio_hasta in number,
    p_desde_const in char)
  IS
    v_ubicacion_leg number(6);
    v_fecha_desde date;
    v_fecha_hasta date;
  BEGIN
    -- Busco el tipo de Entidad
    begin
      select
        te.id_ubicacion into v_ubicacion_leg
      from ipj.t_legajos l join ipj.t_tipos_entidades te
        on te.id_tipo_entidad = l.id_tipo_entidad
      where
        l.id_legajo = p_id_legajo;
    exception
      when NO_DATA_FOUND then
        v_ubicacion_leg := 0;
    end;

    -- Armo el plazo de fechas correcto
    SP_Armar_Rango_Inf_Hist(
      o_fecha_Desde => v_fecha_desde,
      o_fecha_hasta => v_fecha_hasta,
      p_id_legajo => p_id_legajo,
      p_anio_desde => p_anio_desde,
      p_anio_hasta => p_anio_hasta,
      p_desde_Const => p_desde_const,
      p_buscar_anterio => 'N',
      p_datos_entidad => 'N'
    );

    -- Listo los distintos socios
    OPEN o_Cursor FOR
      select
        IPJ.VARIOS.FC_Numero_Matricula(e.matricula) Matricula_Nro, replace(IPJ.VARIOS.FC_Letra_Matricula(e.matricula), '#', '') Matricula_Letra,
        e.matricula_version Matricula_Version, e.folio, e.anio,
        to_char (s.fecha_inicio, 'dd/mm/rrrr') fecha_inicio, to_char (s.fecha_fin, 'dd/mm/rrrr') fecha_fin, i.nro_documento, s.cuit_empresa,
        (select Cuil from rcivil.vt_pk_persona p where P.ID_SEXO = i.id_sexo and p.nro_documento = i.nro_documento and p.pai_cod_pais = i.pai_cod_pais and p.id_numero = i.id_numero) Cuil,
        (select nombre || ' ' || apellido from rcivil.vt_pk_persona p where P.ID_SEXO = i.id_sexo and p.nro_documento = i.nro_documento and p.pai_cod_pais = i.pai_cod_pais and p.id_numero = i.id_numero) Nombre,
        s.n_empresa, s.cuota, s.Cuota_Compartida, s.porc_capital,
        FC_LISTAR_SOCIOS_COPROP(s.id_entidad_socio, s.Id_Tramite_Ipj) Lista_Copo,
        FC_LISTAR_SOCIOS_USUF(s.id_entidad_socio, s.Id_Tramite_Ipj) Lista_Usuf
      from ipj.t_entidades_socios_hist s join IPJ.T_ENTIDADES e
          on s.id_tramite_ipj = e.id_tramite_ipj and s.id_legajo = e.id_legajo
        join IPJ.t_tramitesipj t
          on e.Id_Tramite_Ipj = t.Id_Tramite_Ipj
        left join ipj.t_integrantes i
          on s.id_integrante = i.id_integrante
      where
        e.id_legajo = p_id_legajo and
        e.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_ubicacion_leg) and
        e.borrador = 'N' and
        s.fecha_fin is null and
        T.ID_ESTADO_ULT between 100 and 199 and-- Estados Completados OK
        ( s.fecha_inicio between v_fecha_desde and v_fecha_hasta or
          nvl(s.fecha_fin, (CASE
                            WHEN s.fecha_inicio > v_fecha_hasta THEN SYSDATE
                            ELSE v_fecha_hasta
                            END)) between v_fecha_desde and v_fecha_hasta
        )
      order by e.matricula desc, e.matricula_version desc, anio desc, folio desc;
  EXCEPTION
     WHEN OTHERS THEN
       IPJ.VARIOS.SP_GUARDAR_LOG(
         p_METODO => 'SP_Inf_Hist_Socios',
         p_NIVEL => 'Error',
         p_ORIGEN => 'DB',
         p_USR_LOG  => 'DB',
         p_USR_WINDOWS => 'Sistema',
         p_IP_PC  => null,
         p_EXCEPCION =>
           'Id Legajo = ' || to_char(p_id_legajo)
           || ' / A�o Desde = ' || to_char(p_anio_desde)
           || ' / A�os Hasta = ' || to_char(p_anio_hasta)
           || ' / Desde Const. = ' || p_desde_const
           || ' / ERROR = ' || To_Char(SQLCODE) || '-' || SQLERRM
       );

       -- Armo un cursor vacio para que no explote el informe
       OPEN o_Cursor FOR
         select null Matricula_Nro, null Matricula_Letra, null Matricula_Version, null folio, null anio, null fecha_inicio, null fecha_fin, null nro_documento,
           null cuit_empresa, null Cuil, null Nombre, null n_empresa, null cuota, null Cuota_Compartida, null porc_capital, null Lista_Copo, null Lista_Usuf
         from dual;
  END SP_Inf_Hist_Socios;

  PROCEDURE SP_Inf_Hist_Fiduciario(
    o_Cursor OUT TYPES.cursorType,
    p_id_legajo in number,
    p_anio_desde in number,
    p_anio_hasta in number,
    p_desde_const in char)
  IS
    v_ubicacion_leg number(6);
    v_fecha_desde date;
    v_fecha_hasta date;
  BEGIN
    -- Busco el tipo de Entidad
    begin
      select
        te.id_ubicacion into v_ubicacion_leg
      from ipj.t_legajos l join ipj.t_tipos_entidades te
        on te.id_tipo_entidad = l.id_tipo_entidad
      where
        l.id_legajo = p_id_legajo;
    exception
      when NO_DATA_FOUND then
        v_ubicacion_leg := 0;
    end;

    -- Armo el plado de fechas correcto
    SP_Armar_Rango_Inf_Hist(
      o_fecha_Desde => v_fecha_desde,
      o_fecha_hasta => v_fecha_hasta,
      p_id_legajo => p_id_legajo,
      p_anio_desde => p_anio_desde,
      p_anio_hasta => p_anio_hasta,
      p_desde_Const => p_desde_const,
      p_buscar_anterio => 'N',
      p_datos_entidad => 'N'
    );

    -- Listo los distintos fiduciarios
    OPEN o_Cursor FOR
      select IPJ.VARIOS.FC_Numero_Matricula(e.matricula) Matricula_Nro, replace(IPJ.VARIOS.FC_Letra_Matricula(e.matricula), '#', '') Matricula_Letra,
        e.matricula_version Matricula_Version, e.folio, e.anio,
        to_char (f.fecha_inicio, 'dd/mm/rrrr') fecha_inicio, to_char (f.fecha_fin, 'dd/mm/rrrr') fecha_fin, i.nro_documento, f.cuit_empresa,
        (select Cuil from rcivil.vt_pk_persona p where P.ID_SEXO = i.id_sexo and p.nro_documento = i.nro_documento and p.pai_cod_pais = i.pai_cod_pais and p.id_numero = i.id_numero) Cuil,
        (select nombre || ' ' || apellido from rcivil.vt_pk_persona p where P.ID_SEXO = i.id_sexo and p.nro_documento = i.nro_documento and p.pai_cod_pais = i.pai_cod_pais and p.id_numero = i.id_numero) Nombre,
        f.n_empresa, f.tipo_participacion,
        decode(upper(f.tipo_participacion), 'TITULAR', 0, 1) Orden
      from ipj.t_entidades_fiduciarios f join IPJ.T_ENTIDADES e
          on f.id_tramite_ipj = e.id_tramite_ipj and f.id_legajo = e.id_legajo
        join IPJ.t_tramitesipj t
          on e.Id_Tramite_Ipj = t.Id_Tramite_Ipj
        left join ipj.t_integrantes i
          on f.id_integrante = i.id_integrante
      where
        e.id_legajo = p_id_legajo and
        e.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_ubicacion_leg) and
        e.borrador = 'N' and
        T.ID_ESTADO_ULT between 100 and 199 and-- Estados Completados OK
        ( f.fecha_inicio between v_fecha_desde and v_fecha_hasta or
          nvl(f.fecha_fin, sysdate) between v_fecha_desde and v_fecha_hasta
        )
      order by e.matricula desc, e.matricula_version desc, anio desc, folio desc, orden asc;
  EXCEPTION
     WHEN OTHERS THEN
       IPJ.VARIOS.SP_GUARDAR_LOG(
         p_METODO => 'SP_Inf_Hist_Fiduciario',
         p_NIVEL => 'Error',
         p_ORIGEN => 'DB',
         p_USR_LOG  => 'DB',
         p_USR_WINDOWS => 'Sistema',
         p_IP_PC  => null,
         p_EXCEPCION =>
           'Id Legajo = ' || to_char(p_id_legajo)
           || ' / A�o Desde = ' || to_char(p_anio_desde)
           || ' / A�os Hasta = ' || to_char(p_anio_hasta)
           || ' / Desde Const. = ' || p_desde_const
           || ' / ERROR = ' || To_Char(SQLCODE) || '-' || SQLERRM
       );

       -- Armo un cursor vacio para que no explote el informe
       OPEN o_Cursor FOR
         select null Matricula_Nro, null Matricula_Letra, null Matricula_Version, null folio, null anio, null fecha_inicio, null fecha_fin, null nro_documento,
           null cuit_empresa, null Cuil, null Nombre, null n_empresa, null tipo_participacion, null Orden
         from dual;
  END SP_Inf_Hist_Fiduciario;

  PROCEDURE SP_Inf_Hist_Fiduciante(
    o_Cursor OUT TYPES.cursorType,
    p_id_legajo in number,
    p_anio_desde in number,
    p_anio_hasta in number,
    p_desde_const in char)
  IS
    v_ubicacion_leg number(6);
    v_fecha_desde date;
    v_fecha_hasta date;
  BEGIN
    -- Busco el tipo de Entidad
    begin
      select
        te.id_ubicacion into v_ubicacion_leg
      from ipj.t_legajos l join ipj.t_tipos_entidades te
        on te.id_tipo_entidad = l.id_tipo_entidad
      where
        l.id_legajo = p_id_legajo;
    exception
      when NO_DATA_FOUND then
        v_ubicacion_leg := 0;
    end;

    -- Armo el plado de fechas correcto
    SP_Armar_Rango_Inf_Hist(
      o_fecha_Desde => v_fecha_desde,
      o_fecha_hasta => v_fecha_hasta,
      p_id_legajo => p_id_legajo,
      p_anio_desde => p_anio_desde,
      p_anio_hasta => p_anio_hasta,
      p_desde_Const => p_desde_const,
      p_buscar_anterio => 'N',
      p_datos_entidad => 'N'
    );

    -- Listo los distintos fiduciantes
    OPEN o_Cursor FOR
      select IPJ.VARIOS.FC_Numero_Matricula(e.matricula) Matricula_Nro, replace(IPJ.VARIOS.FC_Letra_Matricula(e.matricula), '#', '') Matricula_Letra,
        e.matricula_version Matricula_Version, e.folio, e.anio,
        to_char (f.fecha_inicio, 'dd/mm/rrrr') fecha_inicio, to_char (f.fecha_fin, 'dd/mm/rrrr') fecha_fin, i.nro_documento, f.cuit_empresa,
        (select Cuil from rcivil.vt_pk_persona p where P.ID_SEXO = i.id_sexo and p.nro_documento = i.nro_documento and p.pai_cod_pais = i.pai_cod_pais and p.id_numero = i.id_numero) Cuil,
        (select nombre || ' ' || apellido from rcivil.vt_pk_persona p where P.ID_SEXO = i.id_sexo and p.nro_documento = i.nro_documento and p.pai_cod_pais = i.pai_cod_pais and p.id_numero = i.id_numero) Nombre,
        f.n_empresa, f.tipo_participacion,
        decode(upper(f.tipo_participacion), 'TITULAR', 0, 1) Orden
      from ipj.t_entidades_fiduciantes f join IPJ.T_ENTIDADES e
          on f.id_tramite_ipj = e.id_tramite_ipj and f.id_legajo = e.id_legajo
        join IPJ.t_tramitesipj t
          on e.Id_Tramite_Ipj = t.Id_Tramite_Ipj
        left join ipj.t_integrantes i
          on f.id_integrante = i.id_integrante
      where
        e.id_legajo = p_id_legajo and
        e.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_ubicacion_leg) and
        e.borrador = 'N' and
        T.ID_ESTADO_ULT between 100 and 199 and-- Estados Completados OK
        ( f.fecha_inicio between v_fecha_desde and v_fecha_hasta or
          nvl(f.fecha_fin, sysdate) between v_fecha_desde and v_fecha_hasta
        )
      order by e.matricula desc, e.matricula_version desc, anio desc, folio desc, orden asc;
  EXCEPTION
     WHEN OTHERS THEN
       IPJ.VARIOS.SP_GUARDAR_LOG(
         p_METODO => 'SP_Inf_Hist_Fiduciante',
         p_NIVEL => 'Error',
         p_ORIGEN => 'DB',
         p_USR_LOG  => 'DB',
         p_USR_WINDOWS => 'Sistema',
         p_IP_PC  => null,
         p_EXCEPCION =>
           'Id Legajo = ' || to_char(p_id_legajo)
           || ' / A�o Desde = ' || to_char(p_anio_desde)
           || ' / A�os Hasta = ' || to_char(p_anio_hasta)
           || ' / Desde Const. = ' || p_desde_const
           || ' / ERROR = ' || To_Char(SQLCODE) || '-' || SQLERRM
       );

       -- Se arma un cursor vacio para que no explote el informe
       OPEN o_Cursor FOR
         select null Matricula_Nro, null Matricula_Letra, null Matricula_Version, null folio, null anio, null fecha_inicio, null fecha_fin, null nro_documento,
           null cuit_empresa, null Cuil, null Nombre, null n_empresa, null tipo_participacion, null Orden
         from dual;
  END SP_Inf_Hist_Fiduciante;

  PROCEDURE SP_Inf_Hist_Beneficiario(
    o_Cursor OUT TYPES.cursorType,
    p_id_legajo in number,
    p_anio_desde in number,
    p_anio_hasta in number,
    p_desde_const in char)
  IS
    v_ubicacion_leg number(6);
    v_fecha_desde date;
    v_fecha_hasta date;
  BEGIN
    -- Busco el tipo de Entidad
    begin
      select
        te.id_ubicacion into v_ubicacion_leg
      from ipj.t_legajos l join ipj.t_tipos_entidades te
        on te.id_tipo_entidad = l.id_tipo_entidad
      where
        l.id_legajo = p_id_legajo;
    exception
      when NO_DATA_FOUND then
        v_ubicacion_leg := 0;
    end;

    -- Armo el plado de fechas correcto
    SP_Armar_Rango_Inf_Hist(
      o_fecha_Desde => v_fecha_desde,
      o_fecha_hasta => v_fecha_hasta,
      p_id_legajo => p_id_legajo,
      p_anio_desde => p_anio_desde,
      p_anio_hasta => p_anio_hasta,
      p_desde_Const => p_desde_const,
      p_buscar_anterio => 'N',
      p_datos_entidad => 'N'
    );

    -- Listo los distintos beneficiarios
    OPEN o_Cursor FOR
      select IPJ.VARIOS.FC_Numero_Matricula(e.matricula) Matricula_Nro, replace(IPJ.VARIOS.FC_Letra_Matricula(e.matricula), '#', '') Matricula_Letra,
        e.matricula_version Matricula_Version, e.folio, e.anio,
        to_char (f.fecha_inicio, 'dd/mm/rrrr') fecha_inicio, to_char (f.fecha_fin, 'dd/mm/rrrr') fecha_fin, i.nro_documento, f.cuit_empresa,
        (select Cuil from rcivil.vt_pk_persona p where P.ID_SEXO = i.id_sexo and p.nro_documento = i.nro_documento and p.pai_cod_pais = i.pai_cod_pais and p.id_numero = i.id_numero) Cuil,
        (select nombre || ' ' || apellido from rcivil.vt_pk_persona p where P.ID_SEXO = i.id_sexo and p.nro_documento = i.nro_documento and p.pai_cod_pais = i.pai_cod_pais and p.id_numero = i.id_numero) Nombre,
        f.n_empresa, f.tipo_participacion,
        decode(upper(f.tipo_participacion), 'TITULAR', 0, 1) Orden
      from ipj.t_entidades_benef f join IPJ.T_ENTIDADES e
          on f.id_tramite_ipj = e.id_tramite_ipj and f.id_legajo = e.id_legajo
        join IPJ.t_tramitesipj t
          on e.Id_Tramite_Ipj = t.Id_Tramite_Ipj
        left join ipj.t_integrantes i
          on f.id_integrante = i.id_integrante
      where
        e.id_legajo = p_id_legajo and
        e.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_ubicacion_leg) and
        e.borrador = 'N' and
        T.ID_ESTADO_ULT between 100 and 199 and-- Estados Completados OK
        ( f.fecha_inicio between v_fecha_desde and v_fecha_hasta or
          nvl(f.fecha_fin, sysdate) between v_fecha_desde and v_fecha_hasta
        )
      order by e.matricula desc, e.matricula_version desc, anio desc, folio desc, orden asc;
  EXCEPTION
     WHEN OTHERS THEN
       IPJ.VARIOS.SP_GUARDAR_LOG(
         p_METODO => 'SP_Inf_Hist_Beneficiario',
         p_NIVEL => 'Error',
         p_ORIGEN => 'DB',
         p_USR_LOG  => 'DB',
         p_USR_WINDOWS => 'Sistema',
         p_IP_PC  => null,
         p_EXCEPCION =>
           'Id Legajo = ' || to_char(p_id_legajo)
           || ' / A�o Desde = ' || to_char(p_anio_desde)
           || ' / A�os Hasta = ' || to_char(p_anio_hasta)
           || ' / Desde Const. = ' || p_desde_const
           || ' / ERROR = ' || To_Char(SQLCODE) || '-' || SQLERRM
       );

       -- Armo un ciursor vacio para que no explote el informe
       OPEN o_Cursor FOR
         select null Matricula_Nro, null Matricula_Letra, null Matricula_Version, null folio, null anio, null fecha_inicio, null fecha_fin,
           null nro_documento, null cuit_empresa, null Cuil, null Nombre, null ln_empresa, null tipo_participacion, null Orden
         from dual;
  END SP_Inf_Hist_Beneficiario;

  PROCEDURE SP_Inf_Hist_Fideicomisario(
    o_Cursor OUT TYPES.cursorType,
    p_id_legajo in number,
    p_anio_desde in number,
    p_anio_hasta in number,
    p_desde_const in char)
  IS
    v_ubicacion_leg number(6);
    v_fecha_desde date;
    v_fecha_hasta date;
  BEGIN
    -- Busco el tipo de Entidad
    begin
      select
        te.id_ubicacion into v_ubicacion_leg
      from ipj.t_legajos l join ipj.t_tipos_entidades te
        on te.id_tipo_entidad = l.id_tipo_entidad
      where
        l.id_legajo = p_id_legajo;
    exception
      when NO_DATA_FOUND then
        v_ubicacion_leg := 0;
    end;

    -- Armo el plado de fechas correcto
    SP_Armar_Rango_Inf_Hist(
      o_fecha_Desde => v_fecha_desde,
      o_fecha_hasta => v_fecha_hasta,
      p_id_legajo => p_id_legajo,
      p_anio_desde => p_anio_desde,
      p_anio_hasta => p_anio_hasta,
      p_desde_Const => p_desde_const,
      p_buscar_anterio => 'N',
      p_datos_entidad => 'N'
    );

    -- Listo los distintos fideicomisarios
    OPEN o_Cursor FOR
      select IPJ.VARIOS.FC_Numero_Matricula(e.matricula) Matricula_Nro, replace(IPJ.VARIOS.FC_Letra_Matricula(e.matricula), '#', '') Matricula_Letra,
        e.matricula_version Matricula_Version, e.folio, e.anio,
        to_char (f.fecha_inicio, 'dd/mm/rrrr') fecha_inicio, to_char (f.fecha_fin, 'dd/mm/rrrr') fecha_fin, i.nro_documento, f.cuit_empresa,
        (select Cuil from rcivil.vt_pk_persona p where P.ID_SEXO = i.id_sexo and p.nro_documento = i.nro_documento and p.pai_cod_pais = i.pai_cod_pais and p.id_numero = i.id_numero) Cuil,
        (select nombre || ' ' || apellido from rcivil.vt_pk_persona p where P.ID_SEXO = i.id_sexo and p.nro_documento = i.nro_documento and p.pai_cod_pais = i.pai_cod_pais and p.id_numero = i.id_numero) Nombre,
        f.n_empresa, f.tipo_participacion,
        decode(upper(f.tipo_participacion), 'TITULAR', 0, 1) Orden
      from ipj.t_entidades_fideicom f join IPJ.T_ENTIDADES e
          on f.id_tramite_ipj = e.id_tramite_ipj and f.id_legajo = e.id_legajo
        join IPJ.t_tramitesipj t
          on e.Id_Tramite_Ipj = t.Id_Tramite_Ipj
        left join ipj.t_integrantes i
          on f.id_integrante = i.id_integrante
      where
        e.id_legajo = p_id_legajo and
        e.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_ubicacion_leg) and
        e.borrador = 'N' and
        T.ID_ESTADO_ULT between 100 and 199 and-- Estados Completados OK
        ( f.fecha_inicio between v_fecha_desde and v_fecha_hasta or
          nvl(f.fecha_fin, sysdate) between v_fecha_desde and v_fecha_hasta
        )
      order by e.matricula desc, e.matricula_version desc, anio desc, folio desc, orden asc;
  EXCEPTION
     WHEN OTHERS THEN
       IPJ.VARIOS.SP_GUARDAR_LOG(
         p_METODO => 'SP_Inf_Hist_Fideicomisario',
         p_NIVEL => 'Error',
         p_ORIGEN => 'DB',
         p_USR_LOG  => 'DB',
         p_USR_WINDOWS => 'Sistema',
         p_IP_PC  => null,
         p_EXCEPCION =>
           'Id Legajo = ' || to_char(p_id_legajo)
           || ' / A�o Desde = ' || to_char(p_anio_desde)
           || ' / A�os Hasta = ' || to_char(p_anio_hasta)
           || ' / Desde Const. = ' || p_desde_const
           || ' / ERROR = ' || To_Char(SQLCODE) || '-' || SQLERRM
       );

       -- Armo un cursor vacio para que no explote el informe
       OPEN o_Cursor FOR
         select null Matricula_Nro, null Matricula_Letra, null Matricula_Version, null folio, null anio, null fecha_inicio, null fecha_fin,
           null nro_documento, null cuit_empresa, null Cuil, null Nombre, null n_empresa, null tipo_participacion, null Orden
         from dual;
  END SP_Inf_Hist_Fideicomisario;

  PROCEDURE SP_Armar_Rango_Inf_Hist(
    o_fecha_Desde out date,
    o_fecha_hasta out date,
    p_id_legajo in number,
    p_anio_desde in number,
    p_anio_hasta in number,
    p_desde_Const char,
    p_buscar_anterio char,
    p_datos_entidad char)
  IS
  /**********************************************************
    Arma el rango de fechas para la b�squeda de informes hist�ricos.
    Si el rango esta vacio, y se indica buscar anterior, amplio el desde hasta la m�xima
      fecha anterior al plazo indicado.
  **********************************************************/
    v_hay_datos number;
    v_ubicacion_leg number;
    v_max_versionado date;
  BEGIN
    -- Busco el tipo de Entidad
    begin
      select
        te.id_ubicacion into v_ubicacion_leg
      from ipj.t_legajos l join ipj.t_tipos_entidades te
        on te.id_tipo_entidad = l.id_tipo_entidad
      where
        l.id_legajo = p_id_legajo;
    exception
      when NO_DATA_FOUND then
        v_ubicacion_leg := 0;
    end;

    -- Armo la fecha de Fin como el 31/12 del a�o indicado o la fecha de hoy si esta vacio
    select decode(nvl(p_anio_hasta, 0), 0, trunc(sysdate), to_date('31/12/' || to_char(p_anio_hasta), 'dd/mm/rrrr')) into o_fecha_hasta
    from dual;

    -- Busco la fecha de inicio correcta
    if upper(p_desde_Const) = 'S' then
      -- Busco la fecha de Acta o la inscripci�n
      select min(nvl(to_date(e.acta_contitutiva, 'dd/mm/rrrr'), e.fec_inscripcion)) into o_fecha_Desde
      from ipj.t_entidades e join ipj.t_tramitesipj tr
        on e.id_tramite_ipj = tr.id_tramite_ipj
      where
        e.id_legajo = p_id_legajo and
        e.id_tipo_entidad  in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_ubicacion_leg) and
        e.borrador = 'N' and
        tr.id_estado_ult between 100 and 199;
    else
      -- Tomo el 01/01 del a�o indicado
      o_fecha_Desde := to_date('01/01/' || to_char(p_anio_desde), 'dd/mm/rrrr');

      -- Si puede ser desde Antes, busco si hay alg�n dato, sino acivo la fecha
      if p_buscar_anterio = 'S' then
        -- Busco el tipo de Entidad
        begin
          select te.id_ubicacion into v_ubicacion_leg
          from ipj.t_legajos l join ipj.t_tipos_entidades te
            on te.id_tipo_entidad = l.id_tipo_entidad
          where
            l.id_legajo = p_id_legajo;
        exception
          when NO_DATA_FOUND then
            v_ubicacion_leg := 0;
        end;

        -- Para los datos de la entidad, si no hay versioandos, busco el �ltimo anterior
        if p_datos_entidad = 'S' then
          -- Cuento para verificar si hay datos
          select count(1) into v_hay_datos
          from IPJ.T_ENTIDADES e join IPJ.t_tramitesipj t
              on e.Id_Tramite_Ipj = t.Id_Tramite_Ipj
          where
            e.id_legajo = p_id_legajo and
            e.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_ubicacion_leg) and
            e.borrador = 'N' and
            T.ID_ESTADO_ULT between 100 and 199 and-- Estados Completados OK
            e.fecha_versionado between o_fecha_Desde and o_fecha_Hasta;

          -- Si no hay datos en el plazo indicado, busco uan nueva fecha desde
          if v_hay_datos = 0 then
            -- Busco el maximo versionado anterior
            select max(fecha_versionado) into v_max_versionado
            from IPJ.T_ENTIDADES e join IPJ.t_tramitesipj t
                on e.Id_Tramite_Ipj = t.Id_Tramite_Ipj
            where
              e.id_legajo = p_id_legajo and
              e.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_ubicacion_leg) and
              e.borrador = 'N' and
              T.ID_ESTADO_ULT between 100 and 199 and-- Estados Completados OK
              e.fecha_versionado < o_fecha_Desde;

            -- Si encontr� alguno, lo uso como inicio
            if v_max_versionado is not null then
              o_fecha_Desde := v_max_versionado;
            end if;
          end if;
        end if;
      end if;
    end if;
  END SP_Armar_Rango_Inf_Hist;

  PROCEDURE SP_Traer_Fiduciario_Repres(
    o_Cursor OUT types.cursorType,
    p_id_tramite_ipj in number,
    p_id_entidad_fiduciario in number)
  IS
   /**********************************************************
    Lista los datos del representante de un fiduciario
  **********************************************************/
  BEGIN
    OPEN o_Cursor FOR
      select
        f.id_tramite_ipj, f.id_entidad_fiduciario, f.Id_Integrante_Representante, f.Id_Tipo_Repres,
        to_date(f.fecha_poder, 'dd/mm/rrrr') fecha_poder,
        (select p.nombre||' '||p.apellido from rcivil.vt_pk_persona p where p.id_sexo = i.id_sexo and p.nro_documento = i.nro_documento and p.pai_cod_pais = i.pai_cod_pais and p.id_numero = i.id_numero) detalle_repres,
        i.cuil cuil_repres, i.id_numero id_numero_repres, i.id_sexo id_sexo_repres, i.nro_documento nro_documento_repres, i.pai_cod_pais pai_cod_pais_repres, tr.n_tipo_repres,
        (select lower(n_estado_civil) from RCIVIL.VT_ESTADOS_CIVIL where id_estado_civil = (case when i.id_estado_civil is not null then i.id_estado_civil else (select Id_Estado_Civil from rcivil.vt_pk_persona p where p.id_sexo = i.id_sexo and p.nro_documento = i.nro_documento and p.pai_cod_pais = i.pai_cod_pais and p.id_numero = i.id_numero) end))  N_Estado_Civil_Repres,
        i.id_sexo || i.nro_documento || i.pai_cod_pais || to_char(i.id_numero) Clave_Repres,
        nvl(f.preres_persona_expuesta, 'N') persona_expuesta,
        (select p.apellido from rcivil.vt_pk_persona p where p.id_sexo = i.id_sexo and p.nro_documento = i.nro_documento and p.pai_cod_pais = i.pai_cod_pais and p.id_numero = i.id_numero) apellido,
        (select p.nombre from rcivil.vt_pk_persona p where p.id_sexo = i.id_sexo and p.nro_documento = i.nro_documento and p.pai_cod_pais = i.pai_cod_pais and p.id_numero = i.id_numero) nombre
      from IPJ.t_entidades_fiduciarios f left join ipj.t_integrantes i
            on f.id_integrante_representante = i.id_integrante
          left join IPJ.t_tipos_repres tr
            on tr.id_tipo_repres = f.id_tipo_repres
      where
        f.id_tramite_ipj = p_id_tramite_ipj and
        f.id_entidad_fiduciario = p_id_entidad_fiduciario;

  END SP_Traer_Fiduciario_Repres;

  PROCEDURE SP_Guardar_Fiduciario_Repres(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_entidad_fiduciario in number,
    p_id_tramite_ipj in number,
    -- Datos Persona
    p_id_integrante in number,
    p_id_sexo in varchar2,
    p_nro_documento in varchar2,
    p_pai_cod_pais in varchar2,
    p_id_numero in number,
    p_detalle in varchar2,
    p_nombre in varchar2,
    p_apellido in varchar2,
    p_n_tipo_documento in varchar2,
    -- Datos Particulaes
    p_cuil in varchar2,
    p_id_tipo_repres in number,
    p_repres_persona_expuesta in char,
    p_fecha_poder in varchar -- es fecha
    )
  IS
    v_bloque varchar2(100);
    v_nombre varchar2(200);
    v_rdo_integ varchar2(1000);
    v_tipo_mensaje_integ number;
    v_id_integrante number;
    v_existe_rcivil number;
    v_nombre_rc varchar2(250);
    v_apellido varchar2(250);
  BEGIN
  /*****************************************************
    Agrega o modifica el representante de un Fiduciario Empresa
    Guarda los datos asociados en RCivil.
    No maneja transaccion, ya que se usa en cascada desde la aplicacion y lo administra el codigo.
  ******************************************************/
    if IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('HAB_LOG_GESTION') = '1' then
      IPJ.VARIOS.SP_GUARDAR_LOG(
        p_METODO => 'SP_Guardar_Fiduciario_Repres',
        p_NIVEL => 'DB - Gestion',
        p_ORIGEN => 'DB',
        p_USR_LOG  => 'DB',
        p_USR_WINDOWS => 'Sistema',
        p_IP_PC  => null,
        p_EXCEPCION =>
          'Id_Entidad_Fiduciario = ' || to_char(p_id_entidad_fiduciario)
          || ' / Tramite IPJ = ' || to_char(p_id_tramite_ipj)
          || ' / Id Integrante = ' || to_char(p_id_integrante)
          || ' / Id Sexo = ' || p_id_sexo
          || ' / Nro Documento = ' || p_nro_documento
          || ' / Pai Cod Pais = ' || p_pai_cod_pais
          || ' / Id Numero = ' || to_char(p_id_numero)
          || ' / Detalle = ' || p_detalle
          || ' / Nombre = ' || p_nombre
          || ' / Apellido = ' || p_apellido
          || ' / Tipo Documento = ' || p_n_tipo_documento
          || ' / Cuil = ' || p_Cuil
          || ' / ID Tipo Repres = ' || to_char(p_id_tipo_repres)
          || ' / Repres Persona Expuesta = ' || p_repres_persona_expuesta
          || ' / Fecha Poder = ' || p_fecha_poder
      );
    end if;

    -- Controlo que venga una persona
    if nvl(p_id_integrante, 0) = 0 then
      -- Verifico si esta o no en RCIVIL para ajustar los par�metros en el guardar
      select count(1) into v_existe_rcivil
      from rcivil.vt_pk_persona
      where
        id_sexo = p_id_sexo and
        nro_documento = p_nro_documento and
        pai_cod_pais = p_pai_cod_pais and
        id_numero = p_id_numero;

      if v_existe_rcivil > 0 then
        v_nombre_rc := null;
        v_apellido := null;
      else
        v_nombre_rc := p_nombre;
        v_apellido := p_apellido;
      end if;

      IPJ.ENTIDAD_PERSJUR.SP_GUARDAR_INTEGRANTES(
        o_rdo => v_rdo_integ,
        o_tipo_mensaje => v_tipo_mensaje_integ,
        o_id_integrante => v_id_integrante,
        p_id_sexo => p_id_sexo,
        p_nro_documento => p_nro_documento,
        p_pai_cod_pais => p_pai_cod_pais,
        p_id_numero => p_id_numero,
        p_cuil => p_cuil,
        p_detalle => p_detalle,
        p_nombre => v_nombre_rc,
        p_apellido => v_apellido,
        p_error_dato => 'N',
        p_n_tipo_documento => p_n_tipo_documento
      );

      if v_rdo_integ <> TYPES.c_Resp_OK then
        o_rdo := 'ERROR: No se pudo registrar la persona';
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
        return;
      end if;
    else
      v_id_integrante := p_id_integrante;
    end if;

    --******************************************************
    -- CUERPO DEL PROCEDIMEINTO
    --******************************************************
    -- actualizo el tipo de solicitante
    update IPJ.t_entidades_fiduciarios
    set
      id_integrante_representante = v_id_integrante,
      id_tipo_repres = p_id_tipo_repres,
      preres_persona_expuesta = p_repres_persona_expuesta,
      fecha_poder = to_date(p_fecha_poder, 'dd/mm/rrrr')
    where
      id_entidad_fiduciario = p_id_entidad_fiduciario and
      id_tramite_ipj = p_id_tramite_ipj;

    -- GUARDO EL CUIL EN RCIVIL
    if p_cuil is not null then
      v_bloque := 'Repres Fiduciario - CUIL ';
      IPJ.VARIOS.SP_Guardar_CUIL_RCivil(
        o_rdo => o_rdo,
        o_tipo_mensaje => o_tipo_mensaje,
        p_cuil => replace(p_cuil, '-', ''),
        p_id_sexo => p_id_sexo,
        p_nro_documento => p_nro_documento,
        p_pai_cod_pais => p_pai_cod_pais,
        p_id_numero => p_id_numero
      );
    end if;

    if o_rdo  <> IPJ.TYPES.C_RESP_OK then
      o_rdo := v_bloque || ': ' || o_rdo;
      rollback;
      return;
    end if;

    o_rdo := TYPES.c_Resp_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    commit;
  EXCEPTION
    WHEN OTHERS THEN
       o_rdo := 'ERROR SP_Guardar_Fiduciario_Repres (' || v_bloque || ') :' || To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
       rollback;
  END SP_Guardar_Fiduciario_Repres;

  PROCEDURE SP_Buscar_Versiones_Ent(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    -- Vigencia
    o_matricula_Vigencia out varchar2,
    o_matricula_version_Vigencia out number,
    o_folio_Vigencia out varchar2,
    o_anio_Vigencia out number,
    -- Estado
    o_matricula_Estado out varchar2,
    o_matricula_version_Estado out number,
    o_folio_Estado out varchar2,
    o_anio_Estado out number,
    -- Cierre Ejercicio
    o_matricula_Cierre out varchar2,
    o_matricula_version_Cierre out number,
    o_folio_Cierre out varchar2,
    o_anio_Cierre out number,
    -- Domicilio
    o_matricula_Dom out varchar2,
    o_matricula_version_Dom out number,
    o_folio_Dom out varchar2,
    o_anio_Dom out number,
    -- Denominacion
    o_matricula_Denom out varchar2,
    o_matricula_version_Denom out number,
    o_folio_Denom out varchar2,
    o_anio_Denom out number,
    -- Tipo Administracion
    o_matricula_TipoAd out varchar2,
    o_matricula_version_TipoAd out number,
    o_folio_TipoAd out varchar2,
    o_anio_TipoAd out number,
    -- Objeto Social
    o_matricula_Obj out varchar2,
    o_matricula_version_Obj out number,
    o_folio_Obj out varchar2,
    o_anio_Obj out number,
    -- Capital
    o_matricula_Capital out varchar2,
    o_matricula_version_Capital out number,
    o_folio_Capital out varchar2,
    o_anio_Capital out number,

    p_Id_Tramite_Ipj in number,
    p_id_legajo in number)
  IS
    /***********************************************************
      Dado un tr�mite y una entidad, busca las versiones de las ultima inscripci�n de los datos.
    ************************************************************/
    v_row_entidad_actual ipj.t_entidades%rowtype;
    v_row_entidad ipj.t_entidades%rowtype;
    v_cursor_entidad IPJ.TYPES.CURSORTYPE;
    v_id_ubicacion_ent number;
    v_Ok_Vigencia char;
    v_Ok_Estado char;
    v_Ok_Cierre char;
    v_Ok_Dom char;
    v_Ok_Denom char;
    v_Ok_TipoAd char;
    v_Ok_Obj char;
    v_Ok_Capital char;
  BEGIN
    -- Busco los datos y la versi�n de la entidad
    begin
      select * into v_row_entidad_actual
      from ipj.t_entidades
      where
        id_tramite_ipj = p_id_tramite_ipj and
        id_legajo = p_id_legajo;
    exception
      when NO_DATA_FOUND then
        o_rdo := 'No hay datos para el tr�mite indicado';
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
        return;
    end;

    -- Busco el �rea actual de la entidad
    select id_ubicacion into v_id_ubicacion_ent
    from ipj.t_tipos_entidades te
    where
      te.id_tipo_entidad = v_row_entidad_actual.id_tipo_entidad;

    -- Armo un cursor con todos los tr�mites aprobados de la entidad,
    -- ordenados de manera descendente segun el criterio de cda �rea
    if v_id_ubicacion_ent in (IPJ.TYPES.C_AREA_SRL, IPJ.TYPES.C_AREA_SXA) then
      OPEN v_cursor_entidad FOR
        select e.*
        from IPJ.t_entidades e join ipj.t_tramitesipj t
          on e.Id_Tramite_Ipj = t.Id_Tramite_Ipj
        where
          e.id_legajo = p_id_legajo and
          e.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_id_ubicacion_ent) and
          T.ID_ESTADO_ULT < IPJ.TYPES.C_ESTADOS_RECHAZADO and
          (E.Id_Tramite_Ipj = v_Row_Entidad_Actual.Id_Tramite_Ipj or e.borrador = 'N')
        order by e.matricula asc, e.matricula_version desc, nvl(e.anio, 0) desc, e.folio desc;
    else
      OPEN v_cursor_entidad FOR
        select e.*
        from IPJ.t_entidades e join ipj.t_tramitesipj t
          on e.Id_Tramite_Ipj = t.Id_Tramite_Ipj
        where
          e.id_legajo = p_id_legajo and
          e.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_id_ubicacion_ent) and
          T.ID_ESTADO_ULT < IPJ.TYPES.C_ESTADOS_RECHAZADO and
          (E.Id_Tramite_Ipj = v_Row_Entidad_Actual.Id_Tramite_Ipj or e.borrador = 'N')
        order by IPJ.ENTIDAD_PERSJUR.FC_Buscar_Prim_Acta (e.id_tramite_ipj) desc , e.Id_Tramite_Ipj desc;
    end if;

    v_Ok_Vigencia := 'N';
    v_Ok_Estado := 'N';
    v_Ok_Cierre := 'N';
    v_Ok_Dom := 'N';
    v_Ok_Denom := 'N';
    v_Ok_TipoAd := 'N';
    v_Ok_Obj := 'N';
    v_Ok_Capital := 'N';

    -- Recorro todos los tr�mites, para obtener las inscripciones de los datos vigentes.
    loop
      fetch v_cursor_entidad into v_row_entidad;
      EXIT WHEN v_cursor_entidad%NOTFOUND or v_cursor_entidad%NOTFOUND is null;

      -- Si estoy en el tr�mite inicial, seteo el inicio de las variables
      if v_row_entidad.id_tramite_ipj = v_row_entidad_actual.id_tramite_ipj then
        v_Ok_Vigencia := 'I';
        v_Ok_Estado := 'I';
        v_Ok_Cierre := 'I';
        v_Ok_Dom := 'I';
        v_Ok_Denom := 'I';
        v_Ok_TipoAd := 'I';
        v_Ok_Obj := 'I';
        v_Ok_Capital := 'I';

        -- Vigencia
        o_matricula_Vigencia := v_row_entidad.matricula;
        o_matricula_version_Vigencia := v_row_entidad.matricula_version;
        o_folio_Vigencia := v_row_entidad.Folio;
        o_anio_Vigencia := v_row_entidad.anio;
        -- Estado
        o_matricula_Estado := v_row_entidad.matricula;
        o_matricula_version_Estado := v_row_entidad.matricula_version;
        o_folio_Estado := v_row_entidad.Folio;
        o_anio_Estado := v_row_entidad.anio;
        -- Cierre Ejercicio
        o_matricula_Cierre := v_row_entidad.matricula;
        o_matricula_version_Cierre := v_row_entidad.matricula_version;
        o_folio_Cierre := v_row_entidad.Folio;
        o_anio_Cierre := v_row_entidad.anio;
        -- Domicilio
        o_matricula_Dom := v_row_entidad.matricula;
        o_matricula_version_Dom := v_row_entidad.matricula_version;
        o_folio_Dom := v_row_entidad.Folio;
        o_anio_Dom := v_row_entidad.anio;
        -- Denominacion
        o_matricula_Denom := v_row_entidad.matricula;
        o_matricula_version_Denom := v_row_entidad.matricula_version;
        o_folio_Denom := v_row_entidad.Folio;
        o_anio_Denom := v_row_entidad.anio;
        -- Tipo Administracion
        o_matricula_TipoAd := v_row_entidad.matricula;
        o_matricula_version_TipoAd := v_row_entidad.matricula_version;
        o_folio_TipoAd := v_row_entidad.Folio;
        o_anio_TipoAd := v_row_entidad.anio;
        -- Objeto Social
        o_matricula_Obj := v_row_entidad.matricula;
        o_matricula_version_Obj := v_row_entidad.matricula_version;
        o_folio_Obj := v_row_entidad.Folio;
        o_anio_Obj := v_row_entidad.anio;
        -- Capital
        o_matricula_Capital := v_row_entidad.matricula;
        o_matricula_version_Capital := v_row_entidad.matricula_version;
        o_folio_Capital := v_row_entidad.Folio;
        o_anio_Capital := v_row_entidad.anio;
      else
        -- Si no se iniciaron las variables, no hago nada y dejo pasar el tr�mite.
        if v_Ok_Vigencia <> 'N' then

          -- Valido cambios en Vigencia
          if v_Ok_Vigencia = 'I' and
            nvl(v_Row_Entidad.vigencia, -1) = nvl(v_Row_Entidad_Actual.vigencia, -1) and
            nvl(v_Row_Entidad.tipo_vigencia, -1) = nvl(v_Row_Entidad_Actual.tipo_vigencia, -1) and
            nvl(to_char(v_Row_Entidad.fec_vig_hasta, 'dd/mm/rrrr'), '-') = nvl(to_char(v_Row_Entidad_Actual.fec_vig_hasta, 'dd/mm/rrrr'), '-') and
            nvl(to_char(v_Row_Entidad.fec_vig, 'dd/mm/rrrr'), '-') = nvl(to_char(v_Row_Entidad_Actual.fec_vig, 'dd/mm/rrrr'), '-')
          then
            o_matricula_Vigencia := v_row_entidad.matricula;
            o_matricula_version_Vigencia := v_row_entidad.matricula_version;
            o_folio_Vigencia := v_row_entidad.Folio;
            o_anio_Vigencia := v_row_entidad.anio;
          else
            v_Ok_Vigencia := 'S';
          end if;

          -- Valido cambio de Estado
          if v_Ok_Estado = 'I' and
            nvl(v_row_entidad.id_estado_entidad , '-') = nvl(v_row_entidad_actual.id_estado_entidad, '-')
          then
            o_matricula_Estado := v_row_entidad.matricula;
            o_matricula_version_Estado := v_row_entidad.matricula_version;
            o_folio_Estado := v_row_entidad.Folio;
            o_anio_Estado := v_row_entidad.anio;
          else
            v_Ok_Estado := 'S';
          end if;

          -- Valido cambio en Cierre de Ejercicio
          if v_Ok_Cierre = 'I' and
            nvl(to_char(v_row_entidad.cierre_ejercicio, 'dd/mm'), '-') = nvl(to_char(v_row_entidad_actual.cierre_ejercicio, 'dd/mm'), '-') and
            nvl(v_row_entidad.cierre_fin_mes , '-') = nvl(v_row_entidad_actual.cierre_fin_mes, '-')
          then
            o_matricula_Cierre := v_row_entidad.matricula;
            o_matricula_version_Cierre := v_row_entidad.matricula_version;
            o_folio_Cierre := v_row_entidad.Folio;
            o_anio_Cierre := v_row_entidad.anio;
          else
            v_Ok_Cierre := 'S';
          end if;

          -- Valido cambio en Domicilio
          if v_Ok_Dom = 'I' and
            nvl(v_Row_Entidad.id_vin_real, 0) = nvl(v_Row_Entidad_Actual.id_vin_real, 0)
          then
            o_matricula_Dom := v_row_entidad.matricula;
            o_matricula_version_Dom := v_row_entidad.matricula_version;
            o_folio_Dom := v_row_entidad.Folio;
            o_anio_Dom := v_row_entidad.anio;
          else
            v_Ok_Dom := 'S';
          end if;

          -- Valido cambio de Denominacion
          if v_Ok_Denom = 'I' and
            v_Row_Entidad.denominacion_1 = v_Row_Entidad_Actual.denominacion_1
          then
            o_matricula_Denom := v_row_entidad.matricula;
            o_matricula_version_Denom := v_row_entidad.matricula_version;
            o_folio_Denom := v_row_entidad.Folio;
            o_anio_Denom := v_row_entidad.anio;
          else
            v_Ok_Denom := 'S';
          end if;

          -- Valido cambio en Tipo de Administracion
          if v_Ok_TipoAd = 'I' and
            nvl(v_Row_Entidad.id_tipo_administracion , 0) = nvl(v_Row_Entidad_Actual.id_tipo_administracion, 0)
          then
            o_matricula_TipoAd := v_row_entidad.matricula;
            o_matricula_version_TipoAd := v_row_entidad.matricula_version;
            o_folio_TipoAd := v_row_entidad.Folio;
            o_anio_TipoAd := v_row_entidad.anio;
          else
            v_Ok_TipoAd := 'S';
          end if;

          -- Valido cambio en Objeto Social
          if v_Ok_Obj = 'I' and
            dbms_lob.compare(nvl(v_Row_Entidad.objeto_social, 'NULL'), nvl(v_Row_Entidad_Actual.objeto_social, 'NULL')) = 0
          then
            o_matricula_Obj := v_row_entidad.matricula;
            o_matricula_version_Obj := v_row_entidad.matricula_version;
            o_folio_Obj := v_row_entidad.Folio;
            o_anio_Obj := v_row_entidad.anio;
          else
            v_Ok_Obj := 'S';
          end if;

          -- Valido cambios en Capital
          if v_Ok_Capital = 'I' and
            nvl(v_Row_Entidad.valor_cuota, 0) = nvl(v_Row_Entidad_Actual.valor_cuota, 0) and
            nvl(v_Row_Entidad.cuotas, 0) = nvl(v_Row_Entidad_Actual.cuotas, 0) and
            nvl(v_Row_Entidad.monto, 0) = nvl(v_Row_Entidad_Actual.monto, 0) and
            nvl(v_Row_Entidad.id_moneda, '0') = nvl(v_Row_Entidad_Actual.id_moneda, '0')
          then
            o_matricula_Capital := v_row_entidad.matricula;
            o_matricula_version_Capital := v_row_entidad.matricula_version;
            o_folio_Capital := v_row_entidad.Folio;
            o_anio_Capital := v_row_entidad.anio;
          else
            v_Ok_Capital := 'S';
          end if;
        end if;
      end if;

    END LOOP;
    close v_cursor_entidad;

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     WHEN OTHERS THEN
       o_rdo := 'ERROR SP_Buscar_Versiones_Ent: ' || To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Buscar_Versiones_Ent;

  FUNCTION FC_Buscar_Ult_Tram(p_id_legajo in number, p_Tram_Abierto in char) return number is
  /**********************************************************
    Busca el �ltimo Tr�mite de la entidad
    - p_Tram_Abierto: indica si utiliza tramitres pendientes o cerrados
  **********************************************************/
    v_Id_Tramite_Ipj number;
    v_id_tramiteipj_accion number;
  BEGIN

    IPJ.ENTIDAD_PERSJUR.SP_Buscar_Entidad_Tramite(
      o_Id_Tramite_Ipj => v_Id_Tramite_Ipj,
      o_id_tramiteipj_accion => v_id_tramiteipj_accion,
      p_id_legajo => p_id_legajo,
      p_Abiertos => p_Tram_Abierto);

    Return v_Id_Tramite_Ipj;
  Exception
    When Others Then
      Return null;
  End FC_Buscar_Ult_Tram;

  PROCEDURE SP_ACTUALIZAR_ENT_GOB(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number)
  IS
    v_result_PersJur varchar2(4000);
    v_result_PersJur_Sede varchar2(4000);
    v_result_PersJur_Dom varchar2(4000);
    v_result_PersJur_Rubros varchar2(4000);
    v_result_PersJur_Actividades varchar2(4000);
    v_Row_Entidad IPJ.T_Entidades%ROWTYPE;
    v_Row_PersJur T_COMUNES.vt_pers_juridicas_completa%ROWTYPE;
    v_Row_Entidad_Domicilio DOM_MANAGER.VT_DOMICILIOS_COND%ROWTYPE;
    v_Row_Entidad_Rubros IPJ.t_Entidades_Rubros%ROWTYPE;
    v_PersJur_NewSede varchar2(3);
    v_Cursor_Rubros types.cursorType;
    v_Cursor_Actividades types.cursorType;
    v_Existe_PersJur number;
    v_Existe_Rubro number;
    v_Existe_Actividad number;
    v_bloque VARCHAR2(8000);
    v_Result_log varchar2(4000);
  BEGIN
  /***********************************************************
    Al cerrar un tr�mite en Gestion en cualquier estado, Actualiza los datos m�nimos en Gobierno
    Actualiza :
    - nombre, estado y fecha de baja
    - gravamenes e intervenciones.
    - Domicilio
    - Rubros y Actvidades
  ************************************************************/
    -- Busco la entidad del tramite, si no tiene no hago nada
    begin
      select * into v_Row_Entidad
      from IPJ.t_entidades e
      where
        e.Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        e.id_legajo = p_id_legajo;

    exception
      when NO_DATA_FOUND then
        o_rdo := IPJ.TYPES.C_RESP_OK ;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
        return;
    end;

    -- Si no tiene CUIT, no se puede pasar a Gobierno
    if v_Row_Entidad.cuit is null then
      o_rdo := IPJ.TYPES.C_RESP_OK ;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
      return;
    end if;

    v_bloque := 'PERSJUR';
    v_Existe_PersJur := 0;
    begin
      select * into v_Row_PersJur
      from T_COMUNES.vt_pers_juridicas_completa
      where
        cuit = v_Row_Entidad.Cuit and
        id_sede = nvl(v_Row_Entidad.id_sede, '00');

      v_bloque := v_bloque || ' (Modif): ';
      DBMS_OUTPUT.PUT_LINE('2- SP_ACTUALIZAR_ENT_GOB - Llama a T_COMUNES.pack_persona_juridica.MODIFICA_PERSJUR_UNIF_VERT');
      T_COMUNES.PACK_PERSONA_JURIDICA.MODIFICA_PERSJUR_UNIF_VERT(
        P_CUIT => v_Row_Entidad.Cuit,
        P_RAZON_SOCIAL => v_Row_Entidad.Denominacion_1,
        P_NOM_FAN => v_Row_PersJur.nombre_fantasia,
        P_ID_FOR_JUR => v_Row_PersJur.id_forma_juridica,
        P_ID_COND_IVA => v_Row_PersJur.id_condicion_iva,
        P_ID_APLICACION => Types.c_id_Aplicacion,
        P_NRO_ING_BRUTO => v_Row_PersJur.nro_ingbruto,
        P_ID_COND_INGBRUTO => v_Row_PersJur.id_condicion_ingbruto,
        P_ABREVIADO => v_Row_PersJur.N_abreviado,
        P_FEC_INSCRIPCION => v_Row_Entidad.FEC_INSCRIPCION,
        P_ID_ESTADO => v_Row_Entidad.ID_ESTADO_ENTIDAD,
        P_GRAVAMEN => replace(IPJ.ENTIDAD_PERSJUR.FC_ESTADO_GRAVAMEN_PERSJUR(v_Row_Entidad.id_legajo), '--', ''),
        P_INTERVENCION => replace(IPJ.ENTIDAD_PERSJUR.FC_ESTADO_GRAVAMEN_INTERV(v_Row_Entidad.id_legajo), '--', ''),
        P_NRO_MATRICULA => v_Row_Entidad.MATRICULA,
        P_NRO_HAB_MUNI => v_Row_PersJur.nro_hab_municipal,
        O_RESULTADO => v_result_PersJur
       );
      DBMS_OUTPUT.PUT_LINE('        O_resultado = ' ||v_result_PersJur);

      v_Existe_PersJur := 1;
    exception
      when NO_DATA_FOUND then
        v_bloque := v_bloque || ' (Alta): ';
        DBMS_OUTPUT.PUT_LINE('3- SP_ACTUALIZAR_ENT_GOB - Llama a T_COMUNES.pack_persona_juridica.INSERTA_PERSJUR_UNIF');
        T_COMUNES.pack_persona_juridica.INSERTA_PERSJUR_UNIF_VERT(
          P_CUIT => v_Row_Entidad.Cuit,
          P_RAZON_SOCIAL => v_Row_Entidad.Denominacion_1,
          P_NOM_FAN => null,
          P_ID_FOR_JUR => null,
          P_ID_COND_IVA => null,
          P_ID_APLICACION => Types.c_id_Aplicacion ,
          P_FEC_INICIO_ACT => nvl(v_Row_Entidad.Alta_Temporal, to_date(v_Row_Entidad.Acta_Contitutiva, 'dd/mm/rrrr')),
          P_NRO_ING_BRUTO => null,
          P_ID_COND_INGBRUTO => null,
          P_ABREVIADO => null,
          P_FEC_INSCRIPCION => v_Row_Entidad.FEC_INSCRIPCION,
          P_ID_ESTADO => v_Row_Entidad.ID_ESTADO_ENTIDAD,
          P_GRAVAMEN => IPJ.ENTIDAD_PERSJUR.FC_ESTADO_GRAVAMEN_PERSJUR(v_Row_Entidad.id_legajo),
          P_INTERVENCION => IPJ.ENTIDAD_PERSJUR.FC_ESTADO_GRAVAMEN_INTERV(v_Row_Entidad.id_legajo),
          P_NRO_MATRICULA => v_Row_Entidad.MATRICULA,
          P_NRO_HAB_MUNI => null,
          O_ID_SEDE => v_PersJur_NewSede,
          O_RESULTADO => v_result_PersJur
        );
        DBMS_OUTPUT.PUT_LINE('        O_resultado = ' ||v_result_PersJur);
         -- Si la sede era nueva, asigno el numero que devuelve el paquete
        update IPJ.t_entidades
        set id_sede = v_PersJur_NewSede
        where
          Id_Tramite_Ipj = p_Id_Tramite_Ipj and
          id_legajo = p_id_legajo;
         v_Row_Entidad.Id_Sede := v_PersJur_NewSede;
    end;

    if v_result_PersJur != 'OK' then
      o_rdo := IPJ.VARIOS.MENSAJE_ERROR('ERROR')  || v_bloque || v_result_PersJur;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      DBMS_OUTPUT.PUT_LINE('   Error 3 = ' ||o_rdo);
      return;
    end if;


    /* Valido si existe la Sede como Sede de Persona Juridica para actualizar sus datos:
       - Si existe llamo al MODIFICAR
       - Si no existe llamo al INSERTAR
       - Siempre actualizo el domicilio de la sede
    */
    v_bloque := 'SEDE_PERSJUR';
    if v_Existe_PersJur > 0 or v_PersJur_NewSede = '00' then
      v_bloque := v_bloque || ' (Modif): ';
      if v_Row_Entidad.Baja_Temporal is not null then
        DBMS_OUTPUT.PUT_LINE('4- SP_ACTUALIZAR_ENT_GOB - Llama a T_COMUNES.pack_persona_juridica.MODIFICA_SEDE_PERSJUR_VERT');
        T_COMUNES.pack_persona_juridica.MODIFICA_SEDE_PERSJUR_VERT(
          P_CUIT => v_Row_Entidad.Cuit,
          P_ID_SEDE => v_Row_Entidad.Id_Sede,
          P_FEC_INICIO_ACT => v_Row_Entidad.Alta_Temporal,
          P_FEC_FIN_ACT => v_Row_Entidad.Baja_Temporal,
          P_SEDE => nvl(v_Row_Entidad.Denominacion_2, 'CENTRAL'),
          O_RESULTADO => v_result_PersJur_Sede);
      end if;
    else
      v_bloque := v_bloque || ' (Alta): ';
      DBMS_OUTPUT.PUT_LINE('5- SP_ACTUALIZAR_ENT_GOB - Llama a T_COMUNES.pack_persona_juridica.INSERTA_SEDES_PERSJUR_VERT');
      T_COMUNES.pack_persona_juridica.INSERTA_SEDES_PERSJUR_VERT(
        P_CUIT => v_Row_Entidad.Cuit,
        P_SEDE => v_Row_Entidad.Denominacion_2,
        P_FEC_INICIO_ACT => v_Row_Entidad.Alta_Temporal,
        O_ID_SEDE => v_PersJur_NewSede,
        O_RESULTADO =>  v_result_PersJur_Sede);
    end if;

    if v_result_PersJur_Sede != 'OK' then
      o_rdo := IPJ.VARIOS.MENSAJE_ERROR('ERROR')  || v_bloque || v_result_PersJur_Sede;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      DBMS_OUTPUT.PUT_LINE('   Error 4 = ' ||o_rdo);
      return;
    end if;


    /* Actualizo la direccion de la sede, buscando en ID_ENTIDAD con CUIT + SEDE:
    */
    v_bloque := 'DOMICILIO_PERSJUR: ';
    if v_Row_Entidad.Id_Vin_Real is not null then
       begin
         select * into v_Row_Entidad_Domicilio
         from DOM_MANAGER.VT_DOMICILIOS_COND
         where
           id_vin = v_Row_Entidad.Id_Vin_Real;
       exception
         when NO_DATA_FOUND then
             v_result_PersJur_Dom := 'No Data';
       end;
        if nvl(v_result_PersJur_Dom, 'OK') != 'No Data' then
         DBMS_OUTPUT.PUT_LINE('6- SP_ACTUALIZAR_ENT_GOB - Llama a T_COMUNES.pack_persona_juridica.INSERTA_ID_VIN_PERSJUR');
         T_COMUNES.PACK_PERSONA_JURIDICA.INSERTA_ID_VIN_PERSJUR(
           P_ID_APLICACION => Types.c_id_Aplicacion,
           P_CUIT => v_Row_Entidad.Cuit,
           P_ID_SEDE => nvl(v_Row_Entidad.Id_Sede, '00'),
           P_ID_TIPO_DOM => v_Row_Entidad_Domicilio.id_tipodom,
           P_ID_VIN => v_Row_Entidad.Id_Vin_Real,
           O_RESULTADO => v_result_PersJur_Dom);

          if v_result_PersJur_Dom != 'OK' then
            o_rdo := IPJ.VARIOS.MENSAJE_ERROR('ERROR')  || v_bloque || v_result_PersJur_Dom;
            o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
            DBMS_OUTPUT.PUT_LINE('   Error 5 = ' ||o_rdo);
            return;
          end if;
       end if;
    end if;

    /* Armo un cursor para los Rubos, y valido cada uno:
       - Si existe llamo al MODIFICAR
       - Si no existe llamo al INSERTAR
    */
    v_bloque := 'RUBROS_PERSJUR';
    select count(*) into v_Existe_Rubro
    from IPJ.t_Entidades_Rubros e
    where
      e.Id_Tramite_Ipj = v_Row_Entidad.Id_Tramite_Ipj  and
      E.id_legajo = v_Row_Entidad.id_legajo and
      borrador = 'N';

    if v_Existe_Rubro > 0 then
      OPEN v_Cursor_Rubros FOR
        select
          min(e.Id_Tramite_Ipj) Id_Tramite_Ipj, min(id_legajo) id_legajo,
          e.id_rubro, min(e.id_tipo_actividad) id_tipo_actividad, min(e.id_actividad) id_actividad,
          min(fecha_desde) fecha_desde, min(fecha_hasta)fecha_hasta, 'N' borrador, 0 nro_orden
        from IPJ.t_Entidades_Rubros e
        where
          id_legajo = v_Row_Entidad.id_legajo and
          Id_Tramite_Ipj = v_Row_Entidad.Id_Tramite_Ipj  and
          borrador = 'N'
        group by id_rubro;

      LOOP
        fetch v_Cursor_Rubros into v_Row_Entidad_Rubros;
        EXIT WHEN v_Cursor_Rubros%NOTFOUND or v_Cursor_Rubros%NOTFOUND is null;

        select count(*) into v_Existe_Rubro
        from T_COMUNES.vt_rubros_persjur
        where
          cuit = v_Row_Entidad.Cuit and
          id_rubro = v_Row_Entidad_Rubros.Id_Rubro;

        if v_Existe_Rubro > 0 and v_Row_Entidad_Rubros.Fecha_Hasta is not null then
          v_bloque := v_bloque || ' (Modif): ';
          DBMS_OUTPUT.PUT_LINE('7- SP_ACTUALIZAR_ENT_GOB - Llama a T_COMUNES.pack_persona_juridica.MODIFICA_RUBRO_VERT');
          T_COMUNES.pack_persona_juridica.MODIFICA_RUBRO_VERT(
            P_CUIT => v_Row_Entidad.Cuit,
            P_ID_RUBRO_VIEJO => v_Row_Entidad_Rubros.Id_Rubro,
            P_ID_RUBRO_NUEVO => null,
            P_FEC_INI => v_Row_Entidad_Rubros.Fecha_Desde,
            P_FEC_FIN => v_Row_Entidad_Rubros.Fecha_Hasta,
            O_RESULTADO => v_result_PersJur_Rubros);
        else
          if v_Existe_Rubro = 0 then
            v_bloque := v_bloque || ' (Alta): ';
            DBMS_OUTPUT.PUT_LINE('8- SP_ACTUALIZAR_ENT_GOB - Llama a T_COMUNES.pack_persona_juridica.INSERTA_RUBROS_PERSJUR_VERT');
            T_COMUNES.pack_persona_juridica.INSERTA_RUBROS_PERSJUR_VERT(
              P_CUIT => v_Row_Entidad.cuit,
              P_ID_RUBRO => v_Row_Entidad_Rubros.Id_Rubro,
              P_FEC_INI => v_Row_Entidad_Rubros.Fecha_Desde,
              O_RESULTADO => v_result_PersJur_Rubros);
          end if;
        end if;

        if v_result_PersJur_Rubros != 'OK' then
          o_rdo := IPJ.VARIOS.MENSAJE_ERROR('ERROR')  || v_bloque || '(Rubro ' || v_Row_Entidad_Rubros.Id_Rubro || ') ' || v_result_PersJur_Rubros;
          o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
          DBMS_OUTPUT.PUT_LINE('   Error 6 = ' ||o_rdo);
          return;
        end if;

      END LOOP;
      CLOSE v_Cursor_Rubros;
    end if;

    /* Armo un cursor para las Actividades, y valido cada uno:
       - Si existe llamo al MODIFICAR
       - Si no existe llamo al INSERTAR
    */
    v_bloque := 'ACTIVIDADES_PERSJUR';
    select count(*) into v_Existe_Actividad
    from IPJ.t_Entidades_Rubros e
    where
      e.Id_Tramite_Ipj = v_Row_Entidad.Id_Tramite_Ipj  and
      id_legajo = v_Row_Entidad.id_legajo and
      borrador = 'N';

    if v_Existe_Actividad > 0 then
      OPEN v_Cursor_Actividades FOR
        select *
        from IPJ.t_Entidades_Rubros e
        where
          id_legajo = v_Row_Entidad.id_legajo and
          Id_Tramite_Ipj = v_Row_Entidad.Id_Tramite_Ipj and
          borrador = 'N';

      LOOP
        fetch v_Cursor_Actividades into v_Row_Entidad_Rubros;
        EXIT WHEN v_Cursor_Actividades%NOTFOUND;

        select count(*) into v_Existe_Actividad
        from T_COMUNES.vt_perjur_actividades
        where
          cuit = v_Row_Entidad.Cuit and
          id_rubro = v_Row_Entidad_Rubros.Id_Rubro and
          id_tipo_Actividad = v_Row_Entidad_Rubros.Id_Tipo_Actividad and
          id_actividad = v_Row_Entidad_Rubros.Id_Actividad;

        if v_Existe_Actividad > 0  and v_Row_Entidad_Rubros.Fecha_Hasta is not null then
          v_bloque := v_bloque || ' (Modif): ';
          DBMS_OUTPUT.PUT_LINE('8- SP_ACTUALIZAR_ENT_GOB - Llama a T_COMUNES.pack_persona_juridica.MODIFICA_ACTIVIDADES_PERSJUR_V');
          T_COMUNES.pack_persona_juridica.MODIFICA_ACTIVIDADES_PERSJUR_V(
            P_CUIT => v_Row_Entidad.Cuit,
            P_ID_RUBRO => v_Row_Entidad_Rubros.Id_Rubro,
            P_ID_TIPO_ACTIVIDAD => v_Row_Entidad_Rubros.Id_Tipo_Actividad,
            P_ID_ACTIVIDAD => v_Row_Entidad_Rubros.Id_Actividad,
            P_FECHA_INICIO => v_Row_Entidad_Rubros.Fecha_Desde,
            P_FECHA_FIN => v_Row_Entidad_Rubros.Fecha_Hasta,
            O_RESULTADO => v_result_PersJur_Actividades);
        else
          if v_Existe_Actividad = 0 then
            v_bloque := v_bloque || ' (Alta): ';
            --DBMS_OUTPUT.PUT_LINE('9- SP_ACTUALIZAR_ENT_GOB - Llama a T_COMUNES.pack_persona_juridica.INSERTA_ACTIVIDADES_PERSJUR_V');
            T_COMUNES.pack_persona_juridica.INSERTA_ACTIVIDADES_PERSJUR_V(
              P_CUIT => v_Row_Entidad.Cuit,
              P_ID_RUBRO => v_Row_Entidad_Rubros.Id_Rubro,
              P_ID_TIPO_ACTIVIDAD => v_Row_Entidad_Rubros.Id_Tipo_Actividad,
              P_ID_ACTIVIDAD => v_Row_Entidad_Rubros.Id_Actividad,
              P_FECHA_INICIO => v_Row_Entidad_Rubros.Fecha_Desde,
              O_RESULTADO => v_result_PersJur_Actividades);
          end if;
        end if;

        if v_result_PersJur_Actividades != 'OK' then
          o_rdo := IPJ.VARIOS.MENSAJE_ERROR('ERROR')  || v_bloque || '(Activ. ' || v_Row_Entidad_Rubros.Id_Actividad || ') ' || v_result_PersJur_Actividades;
          o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
          DBMS_OUTPUT.PUT_LINE('   Error 7 = ' ||o_rdo);
          return;
        end if;
      END LOOP;
      CLOSE v_Cursor_Actividades;
    end if;

    DBMS_OUTPUT.PUT_LINE('10- SP_ACTUALIZAR_ENT_GOB - Finaliza');
    o_rdo := 'OK';
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
    WHEN OTHERS THEN
      o_rdo := 'SP_ACTUALIZAR_ENT_GOB: ' || v_bloque || To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      DBMS_OUTPUT.PUT_LINE('11- SP_ACTUALIZAR_ENT_GOB - Exepci�n General');
  END SP_ACTUALIZAR_ENT_GOB;

  PROCEDURE SP_Traer_PersJur_Resol(
    o_Cursor OUT TYPES.cursorType,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number
    )
  IS
  /*******************************************************
    Lista las acciones con resoluciones digitales
  ********************************************************/

   v_cant_rect NUMBER;
  BEGIN

    SELECT COUNT(*)
      INTO v_cant_rect
      FROM ipj.t_tramitesipj tr
      JOIN ipj.t_tramitesipj_acciones a ON tr.id_tramite_ipj = a.id_tramite_ipj
      JOIN ipj.t_tipos_accionesipj ta ON a.id_tipo_accion = ta.id_tipo_accion
      JOIN ipj.t_entidades e ON tr.id_tramite_ipj = e.id_tramite_ipj
      JOIN ipj.t_legajos l ON e.id_legajo = l.id_legajo
     WHERE a.id_tramite_ipj = p_id_tramite_ipj
       AND a.id_legajo = p_id_legajo
       --AND nvl(a.id_documento, 0) > 0
       AND a.id_estado <> 200
       AND ta.id_tipo_accion IN (169, 170, 171);

    IF nvl(v_cant_rect , 0) > 0 THEN

      OPEN o_Cursor FOR
        SELECT a.id_tramite_ipj, a.id_legajo, a.id_documento,
               decode(nvl(a.id_documento, 0), 0, a.n_documento, nvl(a.n_documento, 'DESCONOCIDO.PDF')) n_documento,
               ta.id_tipo_documento_cdd, ta.n_tipo_accion n_tipo_documento, tr.expediente, tr.sticker,
               to_char(tr.fecha_ini_suac, 'dd/mm/rrrr') fecha_ini_suac, nvl(nvl(l.cuit, e.cuit), 0) cuit
          FROM ipj.t_tramitesipj tr
          JOIN ipj.t_tramitesipj_acciones a ON tr.id_tramite_ipj = a.id_tramite_ipj
          JOIN ipj.t_tipos_accionesipj ta ON a.id_tipo_accion = ta.id_tipo_accion
          JOIN ipj.t_entidades e ON tr.id_tramite_ipj = e.id_tramite_ipj
          JOIN ipj.t_legajos l ON e.id_legajo = l.id_legajo
         WHERE a.id_tramite_ipj = p_id_tramite_ipj
           AND a.id_legajo = p_id_legajo
           AND nvl(a.id_documento, 0) > 0
           AND a.id_estado <> 200 -- Acciones no rechazadas
         ORDER BY n_documento, to_date(fecha_ini_suac, 'dd/mm/rrrr') ASC, id_tramite_ipj;

    ELSE

      OPEN o_Cursor FOR
        SELECT a.id_tramite_ipj, a.id_legajo, a.id_documento,
             decode(nvl(a.id_documento, 0), 0, a.n_documento, nvl(a.n_documento, 'DESCONOCIDO.PDF')) n_documento,
             ta.id_tipo_documento_cdd, ta.n_tipo_accion n_tipo_documento, tr.expediente, tr.sticker,
             to_char(tr.fecha_ini_suac, 'dd/mm/rrrr') fecha_ini_suac, nvl(nvl(l.cuit, e.cuit), 0) cuit
          FROM ipj.t_tramitesipj tr
          JOIN ipj.t_tramitesipj_acciones a ON tr.id_tramite_ipj = a.id_tramite_ipj
          JOIN ipj.t_tipos_accionesipj ta ON a.id_tipo_accion = ta.id_tipo_accion
          JOIN ipj.t_entidades e ON tr.id_tramite_ipj = e.id_tramite_ipj
          JOIN ipj.t_legajos l ON e.id_legajo = l.id_legajo
         WHERE a.id_tramite_ipj = p_id_tramite_ipj
           AND a.id_legajo = p_id_legajo
           AND nvl(a.id_documento, 0) > 0
           AND a.id_estado <> 200 -- Acciones no rechazadas
           AND ta.id_tipo_accion NOT IN (34, 42, 43, 93, 94, 103, 115, 118, 121, 122, 128, 131, 132, 133,
                                        134, 135, 136, 137, 146, 147, 150, 151, 153, 154, 155, 156, 157, 159, 160, 161, 175, 176)
         ORDER BY n_documento, to_date(fecha_ini_suac, 'dd/mm/rrrr') ASC, id_tramite_ipj;

    END IF;

  END SP_Traer_PersJur_Resol;


  /*********************************************************
  **********************************************************
                   PROCEDIMINETO CON SOBRECARGA DE PARAMETROS
  **********************************************************
  **********************************************************/

  PROCEDURE SP_TRAER_CONSULTA_NOMBRE_COMP(
    O_CURSOR OUT TYPES.CURSORTYPE,
    P_CLAVE IN VARCHAR2,
    P_CUIL_USUARIO IN VARCHAR2)
  IS
    /*********************************************************
    Este procedimiento devuelve el listado de la busqueda fon�tica y por similitud unidos,
    mostrando solo 1 vez cada registro, ordenado por similitud y  nombre
    **********************************************************/
    V_ES_DIRECCION NUMBER;
  BEGIN
    -- Controlo si tiene permisos de Direccion
    V_ES_DIRECCION := 1;

    OPEN O_CURSOR FOR
      SELECT NVL(ID_LEGAJO, 0) ID_LEGAJO,
             DENOMINACION_SIA,
             CUIT,
             (CASE
               WHEN SIMILAR >= SIM_FON THEN
                SIMILAR
               ELSE
                SIM_FON
             END) SIMILAR,
             TIPO,
             Id_Tramite_Ipj,
             ID_TRAMITEIPJ_ACCION,
             TO_CHAR(FEC_VENCE, 'dd/mm/rrrr') FEC_VENCE,
             (CASE
               WHEN SIMILAR >= SIM_FON THEN
                'Textual'
               ELSE
                'Fon�tico'
             END) OBSERVACION,
             IPJ.VARIOS.FC_CONTEO_PALABRAS(DENOMINACION_SIA) PALABRAS,
             (CASE
               WHEN TIPO = 'R' THEN
                (SELECT EXPEDIENTE
                   FROM IPJ.T_TRAMITESIPJ
                  WHERE Id_Tramite_Ipj = TMP.Id_Tramite_Ipj)
               ELSE
                IPJ.VARIOS.FC_ULTIMO_EXPEDIENTE(ID_LEGAJO)
             END) EXPEDIENTE
        FROM (SELECT C.ID_LEGAJO,
                     C.DENOMINACION_SIA,
                     C.CUIT,
                     MAX(C.SIMIL) SIMILAR,
                     MAX(C.SIMILAR_FON) SIM_FON,
                     C.TIPO,
                     C.Id_Tramite_Ipj,
                     C.ID_TRAMITEIPJ_ACCION,
                     FEC_VENCE
                FROM -- Busca en Legajos
                      (SELECT L.ID_LEGAJO,
                              L.DENOMINACION_SIA,
                              L.CUIT,
                              IPJ.PKG_SND.FC_COMPARAR_FRASES(FC_FORMATEAR_CADENA_COMP(P_CLAVE),
                                                             REPLACE(FC_FORMATEAR_CADENA_COMP(L.DENOMINACION_SIA),
                                                                     ' ',
                                                                     '')) SIMIL,
                              IPJ.PKG_SND.FC_COMPARAR_FONETICA(FC_FORMATEAR_CADENA_COMP(P_CLAVE),
                                                               FC_FORMATEAR_CADENA_COMP(L.DENOMINACION_SIA)) SIMILAR_FON,
                              'E' TIPO,
                              0 Id_Tramite_Ipj,
                              0 ID_TRAMITEIPJ_ACCION,
                              NULL FEC_VENCE
                         FROM IPJ.T_LEGAJOS L
                         JOIN IPJ.T_TIPOS_ENTIDADES TE
                           ON L.ID_TIPO_ENTIDAD = TE.ID_TIPO_ENTIDAD
                        WHERE NVL(ELIMINADO, 0) = 0
                          --AND (V_ES_DIRECCION >= 1 OR
                          --     TE.ID_UBICACION IN
                          --     (SELECT DISTINCT ID_UBICACION
                          --        FROM IPJ.T_GRUPOS_TRAB_UBICACION
                          --       WHERE CUIL = P_CUIL_USUARIO))
                       UNION
                       -- Suma los tramites de reservas
                       SELECT NULL ID_LEGAJO,
                              TR.N_RESERVA DENOMINACION_SIA,
                              '' CUIT,
                              IPJ.PKG_SND.FC_COMPARAR_FRASES(FC_FORMATEAR_CADENA_COMP(P_CLAVE),
                                                             REPLACE(FC_FORMATEAR_CADENA_COMP(TR.N_RESERVA),
                                                                     ' ',
                                                                     '')) SIMIL,
                              IPJ.PKG_SND.FC_COMPARAR_FONETICA(FC_FORMATEAR_CADENA_COMP(P_CLAVE),
                                                               FC_FORMATEAR_CADENA_COMP(TR.N_RESERVA)) SIMILAR_FON,
                              'R' TIPO,
                              TR.Id_Tramite_Ipj,
                              TR.ID_TRAMITEIPJ_ACCION,
                              TR.FEC_VENCE
                         FROM IPJ.T_TRAMITESIPJ_ACC_RESERVA TR
                         JOIN IPJ.T_TRAMITESIPJ TRA
                           ON TR.Id_Tramite_Ipj = TRA.Id_Tramite_Ipj
                        WHERE TR.ID_ESTADO_RESERVA = IPJ.TYPES.C_RESERVA_OK
                          AND TR.FEC_VENCE IS NOT NULL
                          AND TR.FEC_VENCE >= SYSDATE
                          --AND (V_ES_DIRECCION >= 1 OR
                          --    TRA.ID_UBICACION_ORIGEN IN
                          --    (SELECT DISTINCT ID_UBICACION
                          --        FROM IPJ.T_GRUPOS_TRAB_UBICACION
                          --       WHERE CUIL = P_CUIL_USUARIO))
                       UNION
                       -- Suma los tramites abiertos
                       SELECT E.ID_LEGAJO,
                              E.DENOMINACION_1 DENOMINACION_SIA,
                              E.CUIT,
                              IPJ.PKG_SND.FC_COMPARAR_FRASES(FC_FORMATEAR_CADENA_COMP(P_CLAVE),
                                                             REPLACE(FC_FORMATEAR_CADENA_COMP(E.DENOMINACION_1),
                                                                     ' ',
                                                                     '')) SIMIL,
                              IPJ.PKG_SND.FC_COMPARAR_FONETICA(FC_FORMATEAR_CADENA_COMP(P_CLAVE),
                                                               FC_FORMATEAR_CADENA_COMP(E.DENOMINACION_1)) SIMILAR_FON,
                              'E' TIPO,
                              0 Id_Tramite_Ipj,
                              0 ID_TRAMITEIPJ_ACCION,
                              NULL FEC_VENCE
                         FROM IPJ.T_TRAMITESIPJ TR
                         JOIN IPJ.T_ENTIDADES E
                           ON TR.Id_Tramite_Ipj = E.Id_Tramite_Ipj
                        WHERE TR.ID_ESTADO_ULT <
                              IPJ.TYPES.C_ESTADOS_COMPLETADO
                          --AND (V_ES_DIRECCION >= 1 OR
                          --    TR.ID_UBICACION IN
                          --    (SELECT DISTINCT ID_UBICACION
                          --        FROM IPJ.T_GRUPOS_TRAB_UBICACION
                          --       WHERE CUIL = P_CUIL_USUARIO))
                          ) C
               WHERE SIMIL > 0
                  OR SIMILAR_FON > 0
               GROUP BY C.ID_LEGAJO,
                        C.DENOMINACION_SIA,
                        C.CUIT,
                        C.TIPO,
                        C.Id_Tramite_Ipj,
                        C.ID_TRAMITEIPJ_ACCION,
                        FEC_VENCE) TMP
       ORDER BY OBSERVACION  DESC, SIMILAR DESC, PALABRAS ASC, OBSERVACION DESC, DENOMINACION_SIA ASC;

  END SP_TRAER_CONSULTA_NOMBRE_COMP;

  PROCEDURE SP_GUARDAR_INTEGRANTES(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_integrante out number,
    p_id_sexo in varchar2,
    p_nro_documento in varchar2,
    p_pai_cod_pais in varchar2,
    p_id_numero in number,
    p_cuil in varchar2,
    p_detalle in varchar2,
    p_nombre in varchar2,
    p_apellido in varchar2,
    p_error_dato in varchar2,
    p_n_tipo_documento in varchar2)
  IS
    v_pai_cod_pais varchar2(10);
  BEGIN
    IPJ.ENTIDAD_PERSJUR.SP_GUARDAR_INTEGRANTES(
      o_rdo => o_rdo,
      o_tipo_mensaje => o_tipo_mensaje,
      o_id_integrante => o_id_integrante,
      o_pai_cod_pais => v_pai_cod_pais,
      p_id_sexo => p_id_sexo,
      p_nro_documento => p_nro_documento,
      p_pai_cod_pais => p_pai_cod_pais,
      p_id_numero => p_id_numero,
      p_cuil => p_cuil,
      p_detalle => p_detalle,
      p_nombre => p_nombre,
      p_apellido => p_apellido,
      p_error_dato => p_error_dato,
      p_n_tipo_documento => p_n_tipo_documento,
      p_fec_nac => null,
      p_id_sintys => 0
    );

  END SP_GUARDAR_INTEGRANTES;


  PROCEDURE SP_GUARDAR_ENTIDAD_ADMIN(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_integrante out number,
    o_id_admin out number,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number,
    p_id_tipo_integrante in number,
    p_fecha_inicio in varchar2,-- Es Fecha
    p_fecha_fin in varchar2, -- Es Fecha
    p_id_integrante in number,
    p_id_sexo in varchar2,
    p_nro_documento in varchar2,
    p_pai_cod_pais in varchar2,
    p_id_numero in number,
    p_cuil in varchar2,
    p_detalle in varchar2,
    p_nombre in varchar2,
    p_apellido in varchar2,
    p_Id_Vin in number,
    p_Id_Tipo_Organismo in number,
    p_error_dato in varchar2,
    p_eliminar in number,  -- 1 Elimina
    p_n_tipo_documento in varchar2,
    p_Id_Tramite_Ipj_entidad in number,
    p_id_entidades_accion in number,
    p_id_motivo_baja in number,
    p_porc_garantia in varchar2,
    p_monto_garantia in varchar2,
    p_habilitado_present in char,
    p_persona_expuesta in char,
    p_id_moneda in varchar2,
    p_id_admin in number,
    p_cuit_empresa in varchar2,
    p_n_empresa in varchar2,
    p_matricula in varchar2,
    p_fec_acta in varchar2, -- es fecha
    p_id_legajo_empresa in number,
    p_folio in varchar2,
    p_anio in number,
    p_Id_Tipo_Entidad in NUMBER
    )
  IS
  BEGIN
    SP_GUARDAR_ENTIDAD_ADMIN(
      o_rdo => o_rdo,
      o_tipo_mensaje => o_tipo_mensaje,
      o_id_integrante => o_id_integrante,
      o_id_admin => o_id_admin,
      p_Id_Tramite_Ipj => p_Id_Tramite_Ipj,
      p_id_legajo => p_id_legajo,
      p_id_tipo_integrante => p_id_tipo_integrante,
      p_fecha_inicio => p_fecha_inicio,
      p_fecha_fin => p_fecha_fin,
      p_id_integrante => p_id_integrante,
      p_id_sexo => p_id_sexo,
      p_nro_documento => p_nro_documento,
      p_pai_cod_pais => p_pai_cod_pais,
      p_id_numero => p_id_numero,
      p_cuil => p_cuil,
      p_detalle => p_detalle,
      p_nombre => p_nombre,
      p_apellido => p_apellido,
      p_Id_Vin => p_Id_Vin,
      p_Id_Tipo_Organismo => p_Id_Tipo_Organismo,
      p_error_dato => p_error_dato,
      p_eliminar => p_eliminar,
      p_n_tipo_documento => p_n_tipo_documento,
      p_Id_Tramite_Ipj_entidad => p_Id_Tramite_Ipj_entidad,
      p_id_entidades_accion => p_id_entidades_accion,
      p_id_motivo_baja => p_id_motivo_baja,
      p_porc_garantia => p_porc_garantia,
      p_monto_garantia => p_monto_garantia,
      p_habilitado_present => p_habilitado_present,
      p_persona_expuesta => p_persona_expuesta,
      p_id_moneda => p_id_moneda,
      p_id_admin => p_id_admin,
      p_cuit_empresa => p_cuit_empresa,
      p_n_empresa => p_n_empresa,
      p_matricula => p_matricula,
      p_fec_acta => p_fec_acta,
      p_id_legajo_empresa => p_id_legajo_empresa,
      p_folio => p_folio,
      p_anio => p_anio,
      p_Id_Tipo_Entidad => p_Id_Tipo_Entidad,
      p_es_admin_afip => NULL
    );

  END SP_GUARDAR_ENTIDAD_ADMIN;

end Entidad_PersJur;
/

