create or replace package ipj.Varios is

  FUNCTION FC_Buscar_Codigo_AFIP(p_id_localidad IN number, p_id_barrio in number) return number;
  FUNCTION FC_Buscar_Codigo_Postal(p_id_localidad IN number, p_id_barrio in number) return number;
  FUNCTION FC_Quitar_Acentos(p_Texto IN VARCHAR2) return varchar2;
  FUNCTION FC_Quitar_Tipo_Empresa(p_denominacion IN varchar2) return varchar2;
  FUNCTION FC_Dias_Laborables (p_fecha_inicio IN DATE, p_fecha_fin IN DATE, p_enero IN VARCHAR2) RETURN NUMBER;
  FUNCTION FC_Buscar_Dom_Persona(p_id_sexo in varchar2, p_nro_documento in varchar2,
    p_pai_cod_pais in varchar2, p_id_numero in number, p_tipo_dom in number) return number;
  FUNCTION FC_Buscar_Variable_Config (p_id_variable in varchar2) return varchar2;
  FUNCTION FV_Valida_Entero(p_Entero IN VARCHAR2) return NUMBER;
  FUNCTION FC_Ultimo_Expediente(p_id_legajo IN number) return varchar2;
  FUNCTION FC_Conteo_Palabras(p_Texto IN varchar2) return number;
  FUNCTION FC_Reseva_Valida(p_sticker IN varchar) return varchar2;
  FUNCTION FC_Buscar_Te_Nro(p_entidad IN VARCHAR2, p_id_aplicacion in number) return varchar2;
  FUNCTION FC_Buscar_Te_Caract(p_entidad IN VARCHAR2, p_id_aplicacion in number) return varchar2;
  FUNCTION FC_Buscar_Telefono(p_entidad IN VARCHAR2, p_id_aplicacion in number) return varchar2;
  FUNCTION FC_Buscar_Mail(p_entidad IN VARCHAR2, p_id_aplicacion in number) return varchar2;
  FUNCTION FC_Buscar_Profesion(p_id_sexo in varchar2, p_nro_documento in varchar2, p_pai_cod_pais in varchar2, p_id_numero in varchar2) return varchar2;
  FUNCTION FC_Buscar_ID_Profesion(p_id_sexo in varchar2, p_nro_documento in varchar2, p_pai_cod_pais in varchar2, p_id_numero in varchar2) return varchar2;
  FUNCTION FC_Buscar_Tipo_Profesion(p_id_sexo in varchar2, p_nro_documento in varchar2, p_pai_cod_pais in varchar2, p_id_numero in varchar2) return varchar2;
  FUNCTION FC_Armar_Calle_Dom_Id (p_id_vin in number) return varchar2;
  FUNCTION FC_Armar_Calle_Dom(p_n_calle in varchar2, p_altura in number,
    p_piso in varchar2, p_depto in varchar2, p_torre in varchar2, p_mzna in varchar2,
    p_lote in varchar2, p_barrio in varchar2, p_km varchar2, p_tipo_calle in varchar2) return varchar2;
  FUNCTION FC_Generar_Entidad_Dom (
      p_id_integrante in number,
      p_id_legajo in number,
      p_cuit_empresa in varchar2,
      p_id_fondo_comercio in number,
      p_id_entidad_acta in number,
      p_es_comerciante in varchar2, -- S / N
      p_es_admin_entidad in varchar2, -- S / N
      p_id_admin in number
    ) return varchar2;
  FUNCTION MENSAJE_ERROR(p_Codigo IN VARCHAR2) return varchar2;
  FUNCTION FC_ES_FIN_SEMANA(dia in varchar2, mes in varchar2, anio in number) return number;
  FUNCTION TONUMBER(DOC IN VARCHAR2) return NUMBER;
  FUNCTION FC_Numero_Matricula(p_matricula IN VARCHAR2) return varchar2;
  FUNCTION FC_Letra_Matricula(p_matricula IN VARCHAR2) return varchar2;
  FUNCTION FC_Formatear_Matricula(p_matricula IN VARCHAR2) return varchar2;
  FUNCTION VALIDA_ENTIDAD(p_id_tramite_ipj IN number, p_id_legajo in number) return Number;
  FUNCTION VALIDA_ESTADO(Estado IN Number) return Number;
  FUNCTION VALIDA_TIPO_ORGANISMO(p_id_Tipo_Organismo IN Number) return Number;
  FUNCTION VALIDA_FECHA(Fec IN VARCHAR2) return char;
  FUNCTION VALIDA_FECHA(Fec IN VARCHAR2, Masc in VARCHAR2) return char;
  FUNCTION VALIDA_INTEGRANTE(Integrante IN number) return Number;
  FUNCTION VALIDA_TIPO_ACTA(p_Tipo_acta IN Number) return Number;
  FUNCTION VALIDA_TIPO_ORDENDIA(p_Tipo_Orden IN Number) return Number;
  FUNCTION VALIDA_ARCH_ACTA(p_Archivo_Acta IN Number) return Number;
  FUNCTION VALIDA_TIPO_INS_LEGAL(p_Tipo_Ins_Legal IN Number) return Number;
  FUNCTION VALIDA_TIPO_INTEGRANTE(p_Tipo_Integrante IN Number) return Number;
  FUNCTION VALIDA_TIPO_ACCION(p_Tipo_Accion IN Number) return Number;
  FUNCTION VALIDA_TIPO_CLASIFICACION(p_Tipo_Clasif IN Number) return Number;
  FUNCTION VALIDA_USUARIO(Cuil IN varchar2) return Number;
  FUNCTION VALIDA_LIBRO_ENTIDAD(p_id_legajo IN number,
    p_TRAMITE_IPJ in number, p_TIPO_LIBRO in number) return Number;
  FUNCTION VALIDA_TRAMITE_SUAC(p_id_tramite IN number, p_id_tramite_ipj IN number) return Number;
  FUNCTION FC_Formatear_Denominacion(p_Texto IN VARCHAR2) return VARCHAR2;

  PROCEDURE Sp_Traer_Tipos_Dictamen(
    o_Cursor OUT types.cursorType);

  PROCEDURE SP_Traer_Tipos_Actas(
    p_Cursor OUT types.cursorType,
    p_Id_Tipo_Accion in number);

  PROCEDURE SP_Traer_Tipos_Integrantes(
    p_Cursor OUT types.cursorType,
    p_id_tipo_accion in number);

  PROCEDURE SP_TRAER_TIPOS_ACCIONESIPJ(
    p_id_clasif in number,
    p_tipo_persona in number := 1, -- 1= Juridica / 2 = Fisica
    p_Cursor OUT types.cursorType);

  PROCEDURE SP_TRAER_TIPOS_CLASIF_IPJ(
    p_ubicacion in number,
    p_ubicacion_original in number,
    p_clasif_actual in number,
    o_Cursor OUT types.cursorType);

  PROCEDURE SP_Traer_Estados(
    p_Cursor OUT types.cursorType);

  PROCEDURE SP_Traer_Monedas(
    o_Cursor OUT types.cursorType);

  PROCEDURE SP_Traer_Est_Civil(
    o_Cursor OUT types.cursorType);

  PROCEDURE SP_Traer_Meses(
    o_Cursor OUT types.cursorType);

  PROCEDURE SP_Traer_Sexo(
    o_Cursor OUT types.cursorType);

  PROCEDURE SP_Traer_Juzgados(
    o_Cursor OUT types.cursorType);

  PROCEDURE SP_BUSCAR_SAGRADA_FLIA(
    p_ingreso in number := 0,
    p_numero in number := 0,
    p_denominacion in number := 0,
    p_responsable in number := 0,
    p_tipo in number := 0,
    p_hecho in number := 0,
    p_observacion in number := 0,
    P_tipo_busqueda in number, -- 0 = contiene / 1 = finaliza / 2 = inicia / 3 = exacta
    p_cadena_busqueda in varchar2,
    p_Cursor OUT types.cursorType);

  PROCEDURE SP_Buscar_Actividad(
    o_Cursor OUT types.cursorType,
    o_rdo out number,
    p_id_actividad in varchar2 );

  PROCEDURE SP_Traer_Tipo_Libros_Rubricas(
    o_Cursor OUT types.cursorType,
    p_mecanizados in varchar2 );  -- Valores S / N

 PROCEDURE SP_Traer_Informe(
    o_Cursor OUT types.cursorType,
    o_rdo OUT varchar2,
    p_id_tipo_accion in number,
    p_id_informe in number);

  PROCEDURE SP_Traer_Informe_Cabecera(
    o_Cursor OUT types.cursorType,
    p_id_tipo_accion in number,
    p_id_informe in number);

  PROCEDURE SP_Traer_Informe_Borrador(
    o_Cursor OUT types.cursorType,
    o_rdo OUT varchar2,
    p_id_tramite_ipj in number,
    p_id_tramiteipj_accion in number,
    p_id_tipo_accion in number);

  PROCEDURE SP_Eliminar_Informe_Borrador(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in number,
    p_id_tramiteipj_accion in number,
    p_id_tipo_accion in number,
    p_quitar_doc char
  );

  PROCEDURE SP_Guardar_Informe_Borrador(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in number,
    p_id_tramiteipj_accion in number,
    p_id_tipo_accion in number,
    p_texto_informe in clob,
    p_obs_rubrica in varchar);

 PROCEDURE SP_Traer_Tipos_Inst_Legal(
    p_Cursor OUT types.cursorType);

  PROCEDURE SP_Traer_Tipos_Entidades(
    o_Cursor OUT types.cursorType,
    p_id_tramite_ipj in number,
    p_id_tramiteipj_accion in number);

  PROCEDURE SP_Traer_Tipos_Entidades_Ubi(
    o_Cursor OUT types.cursorType,
    p_id_ubicacion in number);

  PROCEDURE SP_Traer_Tipos_Administracion(
    o_Cursor OUT types.cursorType);

  PROCEDURE SP_Traer_Tipo_Gravamenes(
    o_Cursor OUT types.cursorType);

  PROCEDURE SP_Traer_Tipo_Relacion_Empresa(
    o_Cursor OUT types.cursorType);

  PROCEDURE SP_Actualizar_Protocolo(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in number,
    p_id_legajo in number,
    p_id_tramiteipj_accion in number,
    p_matricula in varchar2);

  PROCEDURE SP_Guardar_Matricula(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in number,
    p_id_legajo in number,
    p_id_tramiteipj_accion in number,
    p_cuit in varchar2,
    p_matricula in varchar2);

  PROCEDURE SP_Validar_Matricula(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in number,
    p_id_legajo in number,
    p_id_tramiteipj_accion in number,
    p_cuit in varchar2,
    p_matricula in varchar2);

  PROCEDURE SP_Sugerir_Matricula(
    o_matricula out varchar2,
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in number,
    p_id_legajo in number,
    p_id_tramiteipj_accion in number);

   PROCEDURE SP_Sugerir_Matricula(
    o_matricula out varchar2,
    o_letra out varchar2,
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in number,
    p_id_legajo in number,
    p_id_tramiteipj_accion in number);

FUNCTION FC_Habilitar_Botones (
    p_id_ubicacion number, --4
    p_id_clasif_ipj number, --6
    p_id_tipo_accion number, --0
    p_estado_tramite number,--2
    p_acciones_abiertas number,--0
    p_estado_accion number, --0
    p_area_trabajo number, --2
    p_id_proceso number,
    p_simple_tramite varchar2,
    p_cuil_responsable varchar2,
    p_cuil_logueado varchar2,
    p_id_tramite_ipj number,
    p_id_tramiteipj_accion number) return Number;

  PROCEDURE SP_GUARDAR_LOG(
    p_METODO in VARCHAR2,
    p_NIVEL in VARCHAR2,
    p_ORIGEN in VARCHAR2,
    p_USR_LOG  in VARCHAR2,
    p_USR_WINDOWS in VARCHAR2,
    p_IP_PC  in VARCHAR2,
    p_EXCEPCION in VARCHAR2);

  PROCEDURE SP_GUARDAR_LOG_OL(
    p_METODO in VARCHAR2,
    p_NIVEL in VARCHAR2,
    p_ORIGEN in VARCHAR2,
    p_USR_LOG  in VARCHAR2,
    p_USR_WINDOWS in VARCHAR2,
    p_IP_PC  in VARCHAR2,
    p_EXCEPCION in VARCHAR2);

  PROCEDURE SP_Traer_Estados_Entidad(
    p_Cursor OUT types.cursorType);

  PROCEDURE SP_Traer_Tipo_Organismos (
    o_Cursor OUT types.cursorType,
    p_id_tipo_accion in number);

  PROCEDURE SP_Traer_Tipo_Sindicos (
    o_Cursor OUT types.cursorType,
    p_Id_Tipo_Organismo in number,
    p_id_ubicacion in number );

  PROCEDURE SP_Traer_Tipos_Orden_Dia (
    p_id_entidad_Acta in number,
    o_Cursor OUT types.cursorType);

  PROCEDURE SP_Traer_Archivos_Actas (
    p_ID_TIPO_ACTA in number,
    o_Cursor OUT types.cursorType);

  PROCEDURE SP_Traer_Tipo_Origen (
    o_Cursor OUT types.cursorType,
    p_id_estado_entidad in varchar2);

  PROCEDURE SP_GUARDAR_DOMICILIO(
    o_id_vin out number,
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_vin in number,
    p_usuario in varchar2,
    p_Tipo_Domicilio in number, -- 3 Real //  0 Sin Asignar
    p_id_integrante in number,
    p_id_legajo in number,
    p_cuit_empresa in varchar2,
    p_id_fondo_comercio in number,
    p_id_entidad_acta in number,
    p_es_comerciante in varchar2, -- S / N
    p_es_admin_entidad in varchar2, -- S / N
    P_ID_PROVINCIA in varchar2,
    P_ID_DEPARTAMENTO in number,
    P_ID_LOCALIDAD in number,
    P_BARRIO in varchar2,
    P_CALLE in varchar2,
    P_ALTURA in number,
    P_DEPTO in varchar2,
    P_PISO in varchar2,
    p_torre in varchar2,
    p_manzana in varchar2,
    p_lote in varchar2,
    p_id_barrio in number,
    p_id_calle in number,
    p_id_tipocalle in number,
    p_km in varchar2);

   PROCEDURE SP_Traer_Domicilio(
      p_id_vin in number,
      p_id_integrante in number,
      p_id_legajo in number,
      p_cuit_empresa in varchar2,
      p_id_fondo_comercio in number,
      p_id_entidad_acta in number,
      p_es_comerciante in varchar2, -- S / N
      p_es_admin_entidad in varchar2, -- S / N
      p_Cursor OUT TYPES.cursorType);


  PROCEDURE SP_Guardar_Matricula(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in number,
    p_id_legajo in number,
    p_id_tramiteipj_accion in number,
    p_cuit in varchar2,
    p_matricula in varchar2,
    p_letra in varchar2);

  PROCEDURE SP_Guardar_Tipos_Actas(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_tipo_acta OUT number,
    p_id_tipo_acta in number,
    p_n_tipo_acta in varchar2,
    p_id_ubicacion in number);

  PROCEDURE SP_Guardar_Tipos_Integrantes(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_tipo_integrante OUT number,
    p_id_tipo_integrante in number,
    p_n_tipo_integrante in varchar2,
    p_id_ubicacion in number);

  PROCEDURE SP_Guardar_Tipo_Organismos(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_tipo_organismo OUT number,
    p_id_tipo_organismo in number,
    p_n_tipo_organismo in varchar2,
    p_id_ubicacion in number);

  PROCEDURE SP_Guardar_Tipo_Sindicos(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_tipo_sindico OUT number,
    p_id_tipo_sindico in number,
    p_n_tipo_sindico in varchar2);

  PROCEDURE SP_Guardar_Tipo_Rel_Empresa(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_tipo_relacion_empresa OUT number,
    p_id_tipo_relacion_empresa in number,
    p_n_tipo_relacion_empresa in varchar2);

  PROCEDURE SP_Guardar_Estados_Entidad(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_estado_entidad OUT varchar2,
    p_id_estado_entidad in varchar2,
    p_estado_entidad in varchar2);

  PROCEDURE SP_Guardar_Origen(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_tipo_origen OUT number,
    p_id_tipo_origen in number,
    p_n_tipo_origen in varchar2,
    p_id_estado_entidad in varchar2);

  PROCEDURE SP_Guardar_Orden_Dia(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_tipo_orden_dia OUT number,
    p_id_tipo_orden_dia in number,
    p_n_tipo_orden_dia in varchar2,
    p_siglas in varchar2);


  PROCEDURE SP_Guardar_Archivos_Acta(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_archivo OUT number,
    p_id_archivo in number,
    p_n_archivo in varchar2,
    p_abreviacion in varchar2,
    p_id_tipo_acta in number);

   PROCEDURE SP_Guardar_Tipos_Ins_Legal(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_il_tipo OUT number,
    p_il_tipo in number,
    p_n_il_tipo in varchar2);

  PROCEDURE SP_Guardar_Juzgado(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_juzgado OUT number,
    p_id_juzgado in number,
    p_n_juzgado in varchar2,
    p_activo in varchar2);

  PROCEDURE SP_Guardar_Libros_Rubrica(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_tipo_libro OUT number,
    p_id_tipo_libro in number,
    p_tipo_libro in varchar2,
    p_mecanizado in varchar2);

  PROCEDURE SP_Guardar_Tipo_Gravamen(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_tipo_gravamen OUT number,
    p_id_tipo_gravamen in number,
    p_n_tipo_gravamen in varchar2,
    p_descripcion in varchar2);

   PROCEDURE SP_Guardar_Tipo_Entidad(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_tipo_entidad OUT number,
    p_id_tipo_entidad in number,
    p_tipo_entidad in varchar2,
    p_sigla in varchar2,
    p_contrato in varchar2,
    p_id_ubicacion in number);

   PROCEDURE SP_Guardar_Tipo_Clasif_IPJ(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_clasif_ipj OUT number,
    p_id_clasif_ipj in number,
    p_n_clasif_ipj in varchar2,
    p_id_ubicacion in number);

  PROCEDURE SP_Traer_Variables_Inf(
    o_Cursor OUT types.cursorType);

  PROCEDURE SP_Guardar_Variables_Inf(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_campo in varchar,
    p_n_campo in varchar2,
    p_tipo in varchar2);

  PROCEDURE SP_Traer_Validacion_Inf(
    o_Cursor OUT types.cursorType);

  PROCEDURE SP_Guardar_Validacion_Inf(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_validacion in varchar,
    p_n_validacion in varchar2,
    p_observacion in varchar2);

  PROCEDURE SP_Traer_Mens_Error(
    o_Cursor OUT types.cursorType);

  PROCEDURE SP_Guardar_Mens_Error(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_mensaje_error out number,
    p_id_mensaje_error in number,
    p_n_mensaje_error in varchar2,
    p_comentario in varchar2);

 PROCEDURE SP_Traer_Tipo_Estado_Nota (
    o_Cursor OUT types.cursorType);

  PROCEDURE SP_Guardar_Tipo_Estado_Nota(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_estado_nota OUT number,
    p_id_estado_nota in number,
    p_n_estado_nota in varchar2);

  PROCEDURE SP_Traer_Tipo_Grupos_Acciones (
    o_Cursor OUT types.cursorType);

  PROCEDURE SP_Informes (
    o_Cursor OUT types.cursorType);

  PROCEDURE SP_Guardar_Informe (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_informe out number,
    p_id_informe in number,
    p_n_informe in varchar2,
    p_margen_sup in number,
    p_margen_inf in number,
    p_margen_izq in number,
    p_margen_der in number,
    p_pagina in varchar2,
    p_usa_cabecera in number
    );

    PROCEDURE SP_Buscar_Lineas (
    o_Cursor OUT types.cursorType,
    p_Texto_si in varchar2,
    p_id_campo in varchar2);

  PROCEDURE SP_GUARDAR_INF_LINEAS (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_linea out number,
    p_id_informe in number,
    p_id_linea in number,
    p_id_campo in varchar2,
    p_texto_si in varchar2,
    p_negrita in varchar2,
    p_espacios in number,
    p_tipo_fecha in varchar2,
    p_id_validacion in varchar2,
    p_texto_no in varchar2,
    p_concatenar in varchar2,
    p_tipo_linea in varchar2,
    p_nro_orden in number,
    p_eliminar in number);

  PROCEDURE SP_Reindexar_Informe (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_informe in number);

  PROCEDURE SP_Traer_Estados_Reserva(
    o_Cursor OUT types.cursorType);

  PROCEDURE SP_Traer_Acciones_Archivo(
    o_Cursor OUT types.cursorType);

  PROCEDURE SP_Guardar_Acciones_Archivo(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_accion_archivo OUT number,
    p_id_accion_archivo in number,
    p_n_accion_archivo in varchar2);

  PROCEDURE SP_Traer_Protocolo(
    o_Cursor OUT types.cursorType);

  PROCEDURE SP_Guardar_Protocolo(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_protocolo OUT number,
    p_id_protocolo in number,
    p_n_protocolo in varchar2,
    p_sigla in varchar2,
    p_numerador in number);

  PROCEDURE SP_Traer_Pagina(
    o_Cursor OUT types.cursorType);

  PROCEDURE SP_Guardar_Pagina(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_pagina OUT number,
    p_id_pagina in number,
    p_n_pagina in varchar2);

  PROCEDURE SP_Traer_Workflow(
    o_Cursor OUT types.cursorType);

  PROCEDURE SP_Guardar_Workflow(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_ubicacion_origen in number,
    p_id_clasif_ipj_actual in number,
    p_id_clasif_ipj_proxima in number,
    p_eliminar in number);

  PROCEDURE SP_Traer_Tipos_Documento(
    o_Cursor OUT types.cursorType);

   PROCEDURE SP_Guardar_Tipos_Grupo_Acc(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_grupo_accion OUT number,
    p_id_grupo_accion in number,
    p_n_grupo_accion in varchar2,
    p_eliminar in number); -- 1 elimina

  PROCEDURE SP_Traer_Grupo_Acc_Det(
    o_Cursor OUT types.cursorType,
    p_id_grupo_accion in number);

  PROCEDURE SP_Guardar_Grupo_Acc_Det(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_grupo_accion in number,
    p_id_tipo_accion in number,
    p_eliminar in number); -- 1 elimina

  PROCEDURE SP_Traer_Tipos_Bloque(
    o_Cursor OUT types.cursorType) ;

  PROCEDURE SP_Traer_Bloque_Accion(
    o_Cursor OUT types.cursorType,
    p_id_tipo_accion in number);

  PROCEDURE SP_Traer_Tipo_Pers(
    o_Cursor OUT types.cursorType);

  PROCEDURE Sp_Traer_Tipos_Accion_Id(
    o_Cursor OUT types.cursorType,
    p_id_tipo_accion in number
    );

  PROCEDURE SP_Traer_Tasas(
    o_Cursor OUT types.cursorType);

  PROCEDURE SP_Traer_Acciones_Tasas(
    o_Cursor OUT types.cursorType,
    p_id_tipo_accion in number);

  PROCEDURE Guardar_Acciones_Tasas(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tipo_Accion in number,
    p_id_tasa in number,
    p_importe in number,
    p_eliminar in number); -- 1 elimina

  PROCEDURE SP_Guardar_Tipo_Accion (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_tipo_Accion out number,
    p_id_tipo_Accion in number,
    p_n_tipo_accion in varchar2,
    p_id_conf_accion in number,
    p_id_tipo_persona in number,
    p_id_protocolo in number,
    p_id_clasif_ipj in number,
    p_id_pagina in number,
    p_id_accion_archivo in number,
    p_informar_boletin in varchar2,
    p_tiempo_demora in varchar2,
    p_tiempo_demora_urg in varchar2,
    p_codigo_habilitacion in number,
    p_id_informe number);

  PROCEDURE Sp_Traer_Areas(
    o_Cursor OUT types.cursorType,
    p_ubicacion in number);

  PROCEDURE Sp_Traer_Tipos_Veedor(
    o_Cursor OUT types.cursorType);

  PROCEDURE SP_Traer_Datos(
    o_Dato OUT varchar2,
    p_dato_solicitado in number);

  PROCEDURE Sp_Guardar_Tipos_Veedor(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_rol_veedor OUT number,
    p_id_rol_veedor in number,
    p_n_rol_veedor in varchar2);

  PROCEDURE SP_Buscar_Ayuda (
    o_Cursor OUT types.cursorType,
    p_id_ayuda in number);

  PROCEDURE SP_Guardar_Ayuda (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_ayuda in number,
    p_n_ayuda in varchar2,
    p_descripcion in varchar2,
    p_texto1 in varchar2,
    p_imagen1 in varchar2,
    p_texto2 in varchar2,
    p_imagen2 in varchar2,
    p_texto3 in varchar2,
    p_imagen3 in varchar2,
    p_texto4 in varchar2,
    p_imagen4 in varchar2,
    p_texto5 in varchar2,
    p_imagen5 in varchar2,
    p_texto6 in varchar2,
    p_imagen6 in varchar2,
    p_texto7 in varchar2,
    p_imagen7 in varchar2,
    p_texto8 in varchar2,
    p_imagen8 in varchar2,
    p_id_ubicacion in number);

  PROCEDURE SP_Traer_Tipos_Socio(
    o_Cursor OUT types.cursorType);

  PROCEDURE SP_Traer_Rubros (
    o_Cursor OUT types.cursorType);

  PROCEDURE SP_Traer_Tipos_Activ (
    o_Cursor OUT types.cursorType,
    p_id_rubro in varchar2);

  PROCEDURE SP_Traer_Actividades (
    o_Cursor OUT types.cursorType,
    p_id_rubro in varchar2,
    p_id_tipo_actividad in varchar2);

  PROCEDURE Sp_Traer_Provincias(
    o_Cursor OUT types.cursorType,
    p_id_departamento in number,
    p_id_localidad in number);

  PROCEDURE Sp_Traer_Depto_Arg(
    o_Cursor OUT types.cursorType,
    p_id_provincia in varchar2,
    p_id_localidad in number);

  PROCEDURE Sp_Traer_Local_Arg(
    o_Cursor OUT types.cursorType,
    p_id_provincia in varchar2,
    p_id_departamento in number);

  PROCEDURE Sp_Traer_Departamentos(
    o_Cursor OUT types.cursorType,
    p_id_localidad in number);

  PROCEDURE Sp_Traer_Localidades(
    o_Cursor OUT types.cursorType,
    p_id_departamento in number);

  PROCEDURE Sp_Traer_Formas_Accion(
    o_Cursor OUT types.cursorType);

  PROCEDURE Sp_Traer_Tipos_Accion(
    o_Cursor OUT types.cursorType);

  PROCEDURE Sp_Traer_Tipos_Capital(
    o_Cursor OUT types.cursorType);

  PROCEDURE Sp_Traer_Tipos_Modo_Part(
    o_Cursor OUT types.cursorType);

  PROCEDURE Sp_Traer_Persona_RCivil(
    o_Cursor OUT types.cursorType,
    p_nro_documento in varchar2,
    p_clave varchar2
  );

  PROCEDURE Sp_Traer_Datos_Persona_RCivil(
    o_Cursor OUT types.cursorType,
    p_nro_documento in varchar2,
    p_id_sexo in varchar2,
    p_pai_cod_pais in varchar2,
    p_id_numero in number
  );

  PROCEDURE SP_Traer_Tipo_Organis_Fiscal (
    o_Cursor OUT types.cursorType,
    p_id_ubicacion in number
  );

  PROCEDURE SP_Traer_Tipo_Organismos_Admin  (
    o_Cursor OUT types.cursorType,
    p_id_ubicacion in number
  );

  PROCEDURE SP_Guardar_Telefono_Mail(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_mail in varchar2,
    p_telefono in varchar2,
    p_id_sexo in varchar2,
    p_nro_documento in varchar2,
    p_pai_cod_pais in varchar2,
    p_id_numero in number,
    p_cuit_empresa in varchar2,
    p_sede_empresa in varchar2,
    p_tramite in varchar2,
    p_te_caract in varchar2
  );

  PROCEDURE Sp_Traer_Profesiones(
    o_Cursor OUT types.cursorType
  );

  PROCEDURE Sp_Traer_Profesiones_Sindico(
    o_Cursor OUT types.cursorType);

  PROCEDURE SP_Buscar_Profesion_RCivil(
    o_id_caracteristica out varchar2,
    o_n_caracteristica out varchar2,
    o_id_tipo_caracteristica out varchar2,
    p_id_sexo in varchar2,
    p_nro_documento in varchar2,
    p_pai_cod_pais in varchar2,
    p_id_numero in varchar2
  );

  PROCEDURE SP_Guardar_Profesion_RCivil(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_profesion in varchar2,
    p_id_tipo_caracteristica in varchar2,
    p_id_sexo in varchar2,
    p_nro_documento in varchar2,
    p_pai_cod_pais in varchar2,
    p_id_numero in number
  );

  PROCEDURE SP_Guardar_CUIL_RCivil(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_cuil in varchar2,
    p_id_sexo in varchar2,
    p_nro_documento in varchar2,
    p_pai_cod_pais in varchar2,
    p_id_numero in number
  );

  PROCEDURE Sp_Traer_Tipos_Duracion(
    o_Cursor OUT types.cursorType
  );

  PROCEDURE SP_Traer_Tipos_Integ_Organ(
    o_Cursor OUT types.cursorType,
    p_id_tipo_organismo in number,
    p_codigo_online in number
  );

  PROCEDURE SP_Traer_Clasif_Ipj (
    o_Cursor OUT types.cursorType,
    p_codigo_online in number,
    p_id_ubicacion in number);

  PROCEDURE Sp_Traer_Provicias(
    o_Cursor OUT types.cursorType);

  PROCEDURE Sp_Traer_Tipos_Repres(
    o_Cursor OUT types.cursorType,
    p_tipo_representado in number);

  PROCEDURE Sp_Traer_Mesas_Suac(
    o_Cursor OUT types.cursorType);

  PROCEDURE Sp_Traer_Tipos_Mot_Baja(
    o_Cursor OUT types.cursorType);

  PROCEDURE SP_Traer_Tipos_Fiscal(
    o_Cursor OUT types.cursorType);

  PROCEDURE SP_Traer_Tipos_Est_Prestamo(
    o_Cursor OUT types.cursorType);

  PROCEDURE SP_Buscar_Variable_Config (
    o_valor_variable OUT varchar2,
    p_id_variable in varchar2);

  PROCEDURE SP_Validar_Feriado (
    o_Respuesta OUT number, -- 0 = No es Feriado / 1 = Es Feriado / -1 No es Fecha
    p_Fecha in varchar2);

  PROCEDURE SP_Traer_Tipos_Docs_CDD(
    o_Cursor OUT types.cursorType,
    p_id_aplicacion_cdd in number);

  PROCEDURE SP_Traer_Sede_Origen(
    p_Cursor OUT types.cursorType);

   PROCEDURE SP_Guardar_Valor_Config(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_variable in varchar2,
    p_valor_variable in varchar2
  );

  PROCEDURE SP_Buscar_Cuil_RCivil (
    o_Cuil OUT varchar2,
    p_id_sexo in varchar2,
    p_nro_documento in varchar2,
    p_pai_cod_pais in varchar2,
    p_id_numero in number
  );

  PROCEDURE SP_Validar_Tasa (
    o_Cursor OUT types.cursorType,
    p_tasa in varchar2
  );

  PROCEDURE SP_Traer_Tipos_Societarios (
    o_Cursor OUT types.cursorType,p_id_tipo_entidad in number,
    p_id_ubicacion in number);

  PROCEDURE SP_Traer_Prot_Jur(
    o_Cursor OUT types.cursorType
   );

  PROCEDURE SP_Mail_Log_Monitor;

  PROCEDURE SP_Traer_Barrios_Localidad(
    o_Cursor OUT types.cursorType,
    p_id_localidad in number
  );

  PROCEDURE SP_Traer_Tipos_Calle_Localidad(
    o_Cursor OUT types.cursorType,
    p_id_localidad in number
  );

  PROCEDURE SP_Traer_Tipos_Calle_Todos(
    o_Cursor OUT types.cursorType
  );

  PROCEDURE SP_Traer_Calles_Tipo_Localidad(
    o_Cursor OUT types.cursorType,
    p_id_localidad in number,
    p_id_tipocalle in number
  );

  PROCEDURE SP_Buscar_Cuil_CIDI (
    o_Cursor OUT types.cursorType,
    p_id_sexo in varchar2,
    p_nro_documento in varchar2
  );

  PROCEDURE Sp_Traer_Empresas_Gobierno(
    o_Cursor OUT types.cursorType,
    p_cuit in varchar2
  );

  PROCEDURE SP_Traer_Objetos_Sociales(
    o_Cursor OUT types.cursorType,
    p_id_tipo_entidad number
  );

  PROCEDURE SP_ejecutar_sentencia_job;

  PROCEDURE Sp_Traer_Tipos_Notif(
    o_Cursor OUT types.cursorType,
    p_ubicacion in number
  );

  PROCEDURE SP_Pasar_Fecha_Reserva(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_fec_vence OUT VARCHAR2,
    p_fecha in VARCHAR2
  );

  PROCEDURE SP_TRAER_TOKEN_AFIP (
    o_Cursor OUT types.cursorType,
    p_id_aplicacion in number,
    p_servicio_afip in varchar2
  );

  PROCEDURE SP_GUARDAR_TOKEN_AFIP (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_fecha_caducidad in varchar2,
    p_id_aplicacion in number,
    p_cuit_token in varchar2,
    p_token_hash in varchar2,
    p_sign_hash in varchar2,
    p_servicio_afip in varchar2
  );

  PROCEDURE SP_Traer_Tipo_Est_Desarch (
    o_Cursor OUT types.cursorType,
    p_id_estado_entidad in varchar2
  );

  PROCEDURE SP_Pasar_Fecha_Habil(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_fec_habil OUT VARCHAR2,
    p_fecha in VARCHAR2,
    p_cant_dias IN NUMBER,
    p_cuenta_enero IN VARCHAR2,
    p_dias_habiles IN VARCHAR2
  );

  PROCEDURE SP_Traer_Est_Doc_Adj(
    o_Cursor OUT types.cursorType
  );

  PROCEDURE SP_Traer_Tipos_Tarea(
    o_Cursor OUT types.cursorType
  );

  PROCEDURE SP_Traer_Tipos_Seccion_Inf(
    o_Cursor OUT types.cursorType
  );

  PROCEDURE SP_Traer_Tipo_Intervencion(
    o_Cursor OUT types.cursorType,
    p_id_ubicacion in number
  );

  PROCEDURE Sp_Traer_Tipos_Mot_Vista(
    o_Cursor OUT types.cursorType,
    p_Motivo IN ipj.t_tipos_motivos_vista.motivo%TYPE
  );

  PROCEDURE SP_Traer_Prot_Dig(
    o_Cursor OUT types.cursorType
  );

  PROCEDURE Sp_Traer_Tipos_Denuncia(
    o_Cursor OUT types.cursorType
  );

  PROCEDURE Sp_Traer_Tipos_Solic_CN(
    o_Cursor OUT types.cursorType
  );

  PROCEDURE Sp_Traer_Tipos_Comisiones(
    o_Cursor OUT types.cursorType
  );

  PROCEDURE Sp_Traer_Tipos_Normalizador(
    o_Cursor OUT types.cursorType
  );

  PROCEDURE Sp_Traer_Tipos_Est_Norm(
    o_Cursor OUT types.cursorType
  );

  PROCEDURE SP_Traer_Prot_Dig_Area(
    o_Cursor OUT types.cursorType,
    p_id_ubicacion in number
  );

  /*********************************************************
  **********************************************************
                   PROCEDIMINETO CON SOBRECARGA DE PARAMETROS
  **********************************************************
  **********************************************************/
  PROCEDURE SP_Traer_Tipo_Sindicos (
    o_Cursor OUT types.cursorType);

  FUNCTION F_DATE(v_date IN VARCHAR2) RETURN NUMBER;


end Varios;
/

create or replace package body ipj.Varios is

  FUNCTION FC_Buscar_Codigo_AFIP(p_id_localidad IN number, p_id_barrio in number) return number is
  /**************************************************
    Dado una localidad y un barrio, devuelvo el c�digo de AFIP asociado
  ***************************************************/
    v_CP_Loc number;
    v_CP_Barrio number;
  BEGIN
    -- Busco el CP de la localidad
    begin
      select l.id_afip into v_CP_Loc
      from dom_manager.vt_localidades l
      where
        l.id_localidad = p_id_localidad;
    exception
      when OTHERS then
        v_CP_Loc := null;
    end;

    -- Busca el CP del Barrio
    begin
      select l.id_afip into v_CP_Barrio
      from dom_manager.vt_barrios l
      where
        l.id_localidad = p_id_localidad
        AND l.ID_BARRIO = p_id_barrio;
    exception
      when OTHERS then
        v_CP_Barrio := null;
    end;

    --if p_id_localidad = 1 then
    --  Return v_CP_Loc;
    --else
      Return nvl(v_CP_Barrio, v_CP_Loc);
    --end if;
  Exception
    When Others Then
      Return null;
  End FC_Buscar_Codigo_AFIP;

  FUNCTION FC_Buscar_Codigo_Postal(p_id_localidad IN number, p_id_barrio in number) return number is
  /**************************************************
    Dado una localidad y un barrio, devuelvo el c�digo postal asociado
  ***************************************************/
    v_CP_Loc number;
    v_CP_Barrio number;
  BEGIN
    -- Busco el CP de la localidad
    begin
      select l.cpa into v_CP_Loc
      from dom_manager.vt_localidades l
      where
        l.id_localidad = p_id_localidad;
    exception
      when OTHERS then
        v_CP_Loc := null;
    end;

    -- Busca el CP del Barrio
    begin
      select l.cpa into v_CP_Barrio
      from dom_manager.vt_barrios l
      where
        l.id_barrio = p_id_barrio;
    exception
      when OTHERS then
        v_CP_Barrio := null;
    end;

    -- Para todo lo que es localidad 1 (Cba) AFIP lo tiene como CP 5000
--    IF p_id_localidad = 1 THEN
--      RETURN v_CP_Loc;
--    ELSE
      Return nvl(v_CP_Barrio, v_CP_Loc);
--    END IF;
  Exception
    When Others Then
      Return null;
  End FC_Buscar_Codigo_Postal;

  FUNCTION FC_Quitar_Acentos(p_Texto IN VARCHAR2) return varchar2
  IS
  /**************************************************
    Reemplaza las letras acentuadas, por las mismas sin acentos, y en mayuscula
  ***************************************************/
  BEGIN
    Return (TRANSLATE(upper(p_Texto), '�����','AEIOU'));
  Exception
    When Others Then
      Return NULL;
  End FC_Quitar_Acentos;

  FUNCTION FC_Quitar_Tipo_Empresa(p_denominacion IN varchar2) return varchar2 is
    v_res varchar2(250);
  BEGIN

    v_res :=  trim(
      replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(/*replace(*/upper(p_denominacion), '   ', ' '), '  ', ' '),
      'SAPEM', ''), 'SOCIEDAD ANONIMA CON PARTICIPACION ESTATAL MAYORITARIA', ''),
      'S.A.S.', ''), 'S.A.S', ''), 'SOCIEDAD POR ACCIONES SIMPLIFICADA', ''),
      'S.A.C.I.F.', ''), 'SOCIEDAD ANONIMA, COMERCIAL, INDUSTRIAL, FINANCIERA', ''), 'SOCIEDAD ANONIMA, COMERCIAL, INDUSTRIAL Y FINANCIERA', ''), 'SOCIEDAD ANONIMA COMERCIAL INDUSTRIAL Y FINANCIERA', ''), 'SOCIEDAD ANONIMA COMERCIAL, INDUSTRIAL Y FINANCIERA', ''),
      'S.A.C.I.I.F.', ''), 'SOCIEDAD ANONIMA COMERCIAL, INMOBILIARIA, INDUSTRIAL Y FINANCIERA', ''), 'SOCIEDAD ANONIMA COMERCIAL,INMOBILIARIA,INDUSTRIAL Y FINANCIERA', ''),
      'S.A.C.I.', ''), 'SOCIEDAD ANONIMA COMERCIAL E INDUSTRIAL', ''), 'SOCIEDAD ANONIMA INDUSTRIAL Y COMERCIAL', ''), 'SOCIEDAD ANONIMA, INDUSTRIAL Y COMERCIAL', ''),
      'S.A.C.I.F.A.', ''), 'S.A.C.I.F Y A.', ''), 'SOCIEDAD ANONIMA COMERCIAL INDUSTRIAL FINANCIERA Y AGROPECUARIA', ''), 'SOCIEDAD ANONIMA, COMERCIAL, INDUSTRIAL, FINANCIERA Y AGROPECUARIA', ''), 'SOCIEDAD ANONIMA, COMERCIAL, INDUSTRIAL, FINANCIERA Y AGROPECUARIA', ''),
      'S.A.C.I.I', ''), 'S.A.C.I YI', ''), 'SOCIEDAD ANONIMA COMERCIAL, INDUSTRIAL E INMOBILIARIA', ''), 'SOCIEDAD ANONIMA, COMERCIAL, INDUSTRIAL, INMOBILIARIA', ''),
      'S.A.A.C.I.', ''), 'S.A.C.I.A.', ''),
      'S.A.A.I.F.I Y A.', ''), 'S.A.A.I.F.I.A.', ''),
      'S.C.A.', ''),
      'SACIFIA', ''), 'S.A.C.I.F.I.A.', ''), 'SOCIEDAD ANONIMA COMERCIAL INDUSTRIAL FINANCIERA INMOBILIARIA Y AGROPECUARIA', ''), 'SOCIEDAD ANONIMA, COMERCIAL, INDUSTRIAL, FINANCIERA, INMOBILIARIA Y AGROPECUARIA', ''), 'SOCIEDAD ANONIMA COMERCIAL, INDUSTRIAL, FINANCIERA, INMOBILIARIA Y AGROPECUARIA', ''), 'SOCIEDAD ANONIMA,COMERCIAL, INDUSTRIAL,FINANCIERA,INMOBILIRIA Y AGROPECUARIA', ''),
      'S.A.I.', ''),
      'S.A.I.F.', ''),
      'S.A.C.I.F.I.', ''), 'S.A.C.I.F. I.', ''), 'S.A.C.I.F.E I.', ''), 'SOCIEDAD ANONIMA COM., IND., FINANC. E INMOB.', ''), 'SOCIEDAD ANONIMA COMERCIAL, INDUSTRIAL, FINANCIERA E INMOBILIARIA', ''),
      'S.A.I.Y C.', ''), 'S.A.I.C.', ''), 'SOCIEDAD ANONIMA, INDUSTRIAL Y COMERCIAL', ''),
      'S.A.C.I E .I', ''), 'S.A.C.I.I', ''), 'SOCIEDAD ANONIMA COMERCIAL INDUSTRIAL E INMOBILIARIA', ''),
      'S.A.C.', ''), 'SOCIEDAD ANONIMA COMERCIAL', ''),
      'S.C.A', ''), 'SOCIEDAD EN COMANDITA POR ACCIONES', ''),
      'S.A.I.C.A', ''), 'S.A.I.C Y A', ''), 'SOCIEDAD ANONIMA, INDUSTRIAL, COMERCIAL Y AGROPECUARIA', ''),
      'S.A.C.I.F.I.G', ''), 'SOCIEDAD ANONIMA COM., IND., FIN., INMB., Y AGROP.', ''), 'SOCIEDAD ANONIMA COMERCIAL, INDUSTRIAL, FINANCIERA, INMOBILIARIA, Y AGROPECUARIA', ''),
      'S.A.C.I.A.I', ''), 'SOCIEDAD ANONIMA COMERCIAL, INDUSTRIAL, AGROPECUARIA E INMOBILIARIA', ''), 'SOCIEDAD ANONIMA, COMERCIAL, INDUSTRIAL, AGROPECUARIA E INMOBILIARIA', ''), 'SOCIEDAD ANONIMA COMERCIAL, INDUSTRIAL, AGROPECUARIA, E  INMOBILIARIA', ''), 'S.A.COMERCIAL, INDUSTRIAL, AGROPECUARIA, E  INMOBILIARIA', ''),
      'S.A.C.I.F.I.C.', ''), 'SOCIEDAD ANONIMA CONSTRUCTORA, INDUSTRIAL, FINANCIERA INMOBILIARIA Y COMERCIAL', ''),
      'S.A.F.A.G', ''), 'SOCIEDAD ANONIMA FORESTAL, AGRICOLA GANADERA', ''),
      'S.A.M.C.I', ''), 'SOCIEDAD ANONIMA MINERA COMERCIAL E INDUSTRIAL', ''), 'SOCIEDAD ANONIMA, MINERA, COMERCIAL E INDUSTRIAL', ''),
      'S.A.C.I.F.I.T.Y.A', ''),'S A C I F I T Y A', ''), 'SOCIEDAD ANONIMA, COMERCIAL, INDUSTRIAL, FINANCIERA, INMOBILIARIA, TRANSPORTISTAS Y AGROPECUARIA', ''),
      'SOCIEDAD DEL ESTADO', ''),
      'SOCIEDAD DE ECONOMIA MIXTA', ''),
      'SOCIEDAD DE HECHO', ''),
      'SOC COLECT', ''),  'SOCIEDAD COLECTIVA', ''),
      'SOCIEDAD ANONIMA UNIPERSONAL', ''),
      'S.A,', ''), 'S.A.', ''), ' SA.', ''), 'S.A', ''), 'S. A.', ''), 'S,A,', ''),'SOCIEDAD ANONIMA', ''), 'SOCIEDAD ANONMA', ''), 'SOCIEDAD AN�NIMA', ''),
      'SRL.', ''), 'S. R. L.', ''), 'S R L', ''), 'S.R.L.', ''), /*'SRL', ''),*/'S.R.L', ''), 'SOCIEDAD DE RESPONSABILIDAD LIMITADA', ''), 'SOCIEDA DE RESPONSABILIDAD LIMITADA', ''),
      '(E.F)', ''), '(E/F)', ''), '( EF )', ''), '(E.F.)', ''), 'E.F.', ''), '(EN FORMACION)', ''), '( EN FORMACION)', ''),
      'ASOCIACION CIVIL', ''), 'ASOCIACIONES CIVILES', ''), 'FUNDACION', ''), 'FUNDACIONES', ''),
      'SGR', ''), 'SOCIEDAD DE GARANTIA RECIPROCA', '')
     );

     if v_res like '% SAS' then
       v_res := substr(v_res, 1, length(v_res)-4);
     end if;

     if v_res like '% SRL' then
       v_res := substr(v_res, 1, length(v_res)-4);
     end if;

     if v_res like '% SCS' then
       v_res := substr(v_res, 1, length(v_res)-4);
     end if;

     if v_res like '% SCI' then
       v_res := substr(v_res, 1, length(v_res)-4);
     end if;

     if v_res like '% SC' then
       v_res := substr(v_res, 1, length(v_res)-3);
     end if;

     if v_res like '% SA' then
       v_res := substr(v_res, 1, length(v_res)-3);
     end if;

     if v_res like '% S A' then
       v_res := substr(v_res, 1, length(v_res)-4);
     end if;

     if v_res like '% SE' then
       v_res := substr(v_res, 1, length(v_res)-3);
     end if;

     if v_res like '% SEM' then
       v_res := substr(v_res, 1, length(v_res)-4);
     end if;

     if v_res like '% SAU' then
       v_res := substr(v_res, 1, length(v_res)-4);
     end if;

     if v_res like '% SCA' then
       v_res := substr(v_res, 1, length(v_res)-4);
     end if;

     Return v_res;
  Exception
    When Others Then
      Return 0;
  End FC_Quitar_Tipo_Empresa;

  FUNCTION FC_Dias_Laborables (
    p_fecha_inicio IN DATE,
    p_fecha_fin IN DATE,
    p_enero IN VARCHAR2 --S --> Enero es laborable - N --> Enero no es laborable
    ) RETURN NUMBER IS
  /*****************************************
  Retorna la cantidad de d�as laborables
  ******************************************/
    v_vacaciones NUMBER;
    v_numero_dias NUMBER := 0;
    v_fecha_actual DATE;
  BEGIN
    -- Si el rango no es v�lido, devuelve 0
    if p_fecha_fin >= p_fecha_inicio then
      IF p_enero = 'N' THEN
        -- Cuenta cuantos d�as habiles hay , sin contar ENERO (vacaciones IPJ)
        v_fecha_actual := p_fecha_inicio;
        WHILE v_fecha_actual <= p_fecha_fin LOOP
          if to_char(v_fecha_actual,'D') NOT IN ('6','7') and to_char(v_fecha_actual,'mm') <> '01' THEN
            v_numero_dias := v_numero_dias + 1;
          end if;
          v_fecha_actual := v_fecha_actual + 1;
        END LOOP;

        -- Cuenta la cantidad de Feriados en el rango de fecha, que no caigan en Sabado o Domingo o Enero
        select count (fecha) into v_vacaciones
        from T_COMUNES.VT_FERIADOS
        where
          fecha between p_fecha_inicio and p_fecha_fin and
          to_char(fecha,'D') NOT IN ('6','7') and
          to_char(fecha,'mm') <> '01' and
          id_Feriado not in ('025') and -- Feriado por Cuarenteva Coronavirus
          id_localidad IN (0,1);

      ELSE
        -- Cuenta cuantos d�as habiles hay
        v_fecha_actual := p_fecha_inicio;
        WHILE v_fecha_actual <= p_fecha_fin LOOP
          if to_char(v_fecha_actual,'D') NOT IN ('6','7') THEN
            v_numero_dias := v_numero_dias + 1;
          end if;
          v_fecha_actual := v_fecha_actual + 1;
        END LOOP;

        -- Cuenta la cantidad de Feriados en el rango de fecha, que no caigan en Sabado o Domingo
        select count (fecha) into v_vacaciones
        from T_COMUNES.VT_FERIADOS
        where
          fecha between p_fecha_inicio and p_fecha_fin and
          to_char(fecha,'D') NOT IN ('6','7') and
          id_Feriado not in ('025') and -- Feriado por Cuarenteva Coronavirus
          id_localidad IN (0,1);
      END IF;

      return v_numero_dias - v_vacaciones;
    else
      return 0;
    end if;
  END FC_Dias_Laborables;

  FUNCTION FC_Buscar_Dom_Persona(p_id_sexo in varchar2, p_nro_documento in varchar2,
    p_pai_cod_pais in varchar2, p_id_numero in number, p_tipo_dom in number) return number
  IS
    /**************************************************
      Busca el ID_VIN de una persona f�sica, con las siguientes prioridades:
      1-Busca si tiene un domicilio unico cargado
      2- Busca un domicilio domicilio viejo, segun la entidad
      3- Busca el domicilio legal de Renaper.
    ***************************************************/
    v_id_vin number;
  BEGIN
    -- Busco el domicilio real unico
    begin
      select id_vin into v_id_vin
      from rcivil.vt_personas_domicilio_unico
      where
        id_sexo = p_id_sexo and
        nro_documento = p_nro_documento and
        pai_cod_pais = p_pai_cod_pais and
        id_numero = p_id_numero and
        id_tipo_domicilio = p_tipo_dom; -- 3 Domicilio Real
    exception
      when NO_DATA_FOUND then
        v_id_vin := 0;
    end;

    -- Si no hay domicilio unico
    if nvl(v_id_vin, 0) = 0 then
      -- Busco el mayor domicilio real de la persona deseada, para IPJ
      select max(id_vin) into v_id_vin
      from DOM_MANAGER.VT_DOMICILIOS_COND
      where
        id_entidad = p_id_sexo || p_nro_documento || p_pai_cod_pais || to_char(p_id_numero) and
        ID_TIPODOM = p_tipo_dom and -- 3 Domicilio Real
        id_app = IPJ.TYPES.C_ID_APLICACION;

      -- si no encuentro un domicilio para mi aplciacion, busco cualquiera
      if nvl(v_id_vin, 0) = 0 then
        select max(id_vin) into v_id_vin
        from DOM_MANAGER.VT_DOMICILIOS_COND
        where
          id_entidad = p_id_sexo || p_nro_documento || p_pai_cod_pais || to_char(p_id_numero) and
          ID_TIPODOM = p_tipo_dom; -- 3 Domicilio Real

        -- Si no hay domicilio para la entidad, busco el legal
        if nvl(v_id_vin, 0) = 0 then
          select id_vin into v_id_vin
          from rcivil.vt_personas_domicilio_unico
          where
            id_sexo = p_id_sexo and
            nro_documento = p_nro_documento and
            pai_cod_pais = p_pai_cod_pais and
            id_numero = p_id_numero and
            id_tipo_domicilio = 2 ; -- Domicilio Legal
        end if;
      end if;
    end if;

    return v_id_vin;
  Exception
    When Others Then
      Return 0;
  End FC_Buscar_Dom_Persona;

  FUNCTION FC_Buscar_Variable_Config (p_id_variable in varchar2) return varchar2
  IS
  /**************************************************
    Busca el valor de una variable indicada
  ***************************************************/
    v_valor_variable varchar2(200);
  BEGIN

    select valor_variable into v_valor_variable
    from IPJ.T_Config
    where
      upper(trim(id_variable)) = upper(trim(p_id_variable));

    return v_valor_variable;
  EXCEPTION
    when OTHERS then
      return null;
  END FC_Buscar_Variable_Config ;

  FUNCTION FC_Conteo_Palabras(p_Texto IN varchar2) return number is
    res number(6);
  /**************************************************
    Cuenta la cantidad de palabras en un texto
  ***************************************************/
  BEGIN
    select (LENGTH(p_Texto) - LENGTH(REPLACE(p_Texto,' '))) + 1 into res
    from dual;

    Return res;
  Exception
    When Others Then
      Return 0;
  End FC_Conteo_Palabras;

  FUNCTION FC_Ultimo_Expediente(p_id_legajo IN number) return varchar2 is
      v_res varchar2(100);
  /**************************************************
    Busca el �ltimo tr�mite no rechazado registrado para una entidad.
  ***************************************************/
    v_id_ubicacion number;
  BEGIN
    -- Busco el �rea de la entidad, para ordenar la b�squeda
    select id_ubicacion into v_id_ubicacion
    from ipj.t_legajos l join ipj.t_tipos_entidades te on l.id_tipo_entidad = te.id_tipo_entidad
    where
      l.id_legajo = p_id_legajo;

    if v_id_ubicacion in (IPJ.TYPES.C_AREA_SRL, IPJ.TYPES.C_AREA_SXA) then
      select nvl(Expediente, 'HISTORICO') into v_res
      from
        ( select Tr.Expediente
          from ipj.t_tramitesipj tr join ipj.t_tramitesipj_persjur pj
              on tr.id_tramite_ipj = pj.id_tramite_ipj
            join ipj.t_entidades e
              on e.id_tramite_ipj = tr.id_tramite_ipj and e.id_legajo = pj.id_legajo
          where
            Tr.Id_Estado_Ult < IPJ.TYPES.C_ESTADOS_RECHAZADO and
            pj.id_legajo = p_id_legajo
          order by e.matricula asc , e.matricula_version desc, nvl(e.anio, 0) desc, e.folio desc, e.Id_Tramite_Ipj desc
        ) tmp
      where
        rownum = 1;
    else
      select nvl(Expediente, 'HISTORICO') into v_res
      from
        ( select Tr.Expediente, IPJ.ENTIDAD_PERSJUR.FC_Buscar_Prim_Acta (tr.id_tramite_ipj) Fecha_Acta
          from ipj.t_tramitesipj tr join ipj.t_tramitesipj_persjur pj
            on tr.id_tramite_ipj = pj.id_tramite_ipj
          where
            Tr.Id_Estado_Ult < IPJ.TYPES.C_ESTADOS_RECHAZADO and
            pj.id_legajo = p_id_legajo
          order by Fecha_Acta desc , tr.Id_Tramite_Ipj desc
        ) tmp
      where
        rownum = 1;
    end if;

    Return v_res;
  Exception
    when NO_DATA_FOUND Then
      select
        ( case
             when length(sticker) = 14 then substr(sticker, 1, 9) || substr(sticker, 12, 3)
             when length(sticker) = 12 then sticker
             else null
           end
         ) sticker into v_res
      from
        ( select *
          from ipj.t_tramites_legajo
          where
            id_legajo = p_id_legajo
          order by id_tramite_legajo desc
        )tmp
      where
        rownum = 1;

      Return v_res;

    when OTHERS then
      Return '';
  End FC_Ultimo_Expediente;

  FUNCTION FC_Reseva_Valida(p_sticker IN varchar) return varchar2 is
    res varchar2(250);
  /**************************************************
    Valida que el tr�mite de la reseva de nombre sea v�lido y si vencer (0 = no existe)
  ***************************************************/
  BEGIN
    select N_reserva into res
    from IPJ.t_tramitesipj t join IPJ.T_Tramitesipj_Acc_Reserva r
      on t.id_tramite_ipj = r.id_tramite_ipj
    where
      T.STICKER = p_sticker and
      R.ID_ESTADO_RESERVA = 1 and -- Aprobada
      R.FEC_VENCE >= trunc(SYSDATE);

    Return res;
  Exception
    When Others Then
      Return null;
  End FC_Reseva_Valida;

  FUNCTION FC_Buscar_Te_Nro(p_entidad IN VARCHAR2, p_id_aplicacion in number) return varchar2
  IS
  /**************************************************
    Busca el telefono asociado a una entidad
  ***************************************************/
    v_result varchar2(50);
  BEGIN
    select nro_mail into v_result
    from T_COMUNES.VT_COMUNICACIONES
    where
      id_entidad = p_entidad and
      id_tipo_comunicacion = IPJ.Types.c_comunic_te and
      (p_id_aplicacion is null or id_aplicacion = p_id_aplicacion);

    Return v_result;
  Exception
    When Others Then
      Return NULL;
  End FC_Buscar_Te_Nro;

  FUNCTION FC_Buscar_Te_Caract(p_entidad IN VARCHAR2, p_id_aplicacion in number) return varchar2
  IS
  /**************************************************
    Busca el telefono asociado a una entidad
  ***************************************************/
    v_result varchar2(50);
  BEGIN
    select cod_area into v_result
    from T_COMUNES.VT_COMUNICACIONES
    where
      id_entidad = p_entidad and
      id_tipo_comunicacion = IPJ.Types.c_comunic_te and
      (p_id_aplicacion is null or id_aplicacion = p_id_aplicacion);

    Return v_result;
  Exception
    When Others Then
      Return NULL;
  End FC_Buscar_Te_Caract;

  FUNCTION FC_Buscar_Telefono(p_entidad IN VARCHAR2, p_id_aplicacion in number) return varchar2
  IS
  /**************************************************
    Busca el telefono asociado a una entidad
  ***************************************************/
    v_result varchar2(50);
  BEGIN
    select cod_area || nro_mail into v_result
    from T_COMUNES.VT_COMUNICACIONES
    where
      id_entidad = p_entidad and
      id_tipo_comunicacion = IPJ.Types.c_comunic_te and
      (p_id_aplicacion is null or id_aplicacion = p_id_aplicacion);

    Return v_result;
  Exception
    When Others Then
      Return NULL;
  End FC_Buscar_Telefono;

  FUNCTION FC_Buscar_Mail(p_entidad IN VARCHAR2, p_id_aplicacion in number) return varchar2
  IS
  /**************************************************
    Busca el telefono asociado a una entidad
  ***************************************************/
    v_result varchar2(50);
  BEGIN
    select cod_area || nro_mail into v_result
    from T_COMUNES.VT_COMUNICACIONES
    where
      id_entidad = p_entidad and
      id_tipo_comunicacion = IPJ.Types.c_comunic_Mail and
      (p_id_aplicacion is null or id_aplicacion = p_id_aplicacion);

    Return v_result;
  Exception
    When OTHERS Then
      Return NULL;
  End FC_Buscar_Mail;

  FUNCTION FC_Buscar_Profesion(p_id_sexo in varchar2, p_nro_documento in varchar2, p_pai_cod_pais in varchar2, p_id_numero in varchar2) return varchar2
  IS
  /**************************************************
    Busca la profesion asociada a una persona f�sica
  ***************************************************/
    v_profesion varchar2(50);
  BEGIN
    begin
      select n_caracteristica into v_profesion
      from
        ( select *
          from RCIVIL.VT_CARACTERISTICA_PERS
          where
            id_tipo_caracteristica in ('2021', '2009') and -- Profesiones y Oficios IPJ.TYPES.c_caract_profesion and
            id_sexo = p_id_sexo and
            nro_documento = p_nro_documento and
            pai_cod_pais = p_pai_cod_pais and
            id_numero = p_id_numero
          order by fec_inicio desc
        ) t
       where rownum = 1;
    exception
      when NO_DATA_FOUND then
        v_profesion := null;
    end;

    Return v_profesion;
  Exception
    When Others Then
      Return NULL;
  End FC_Buscar_Profesion;

  FUNCTION FC_Buscar_ID_Profesion(p_id_sexo in varchar2, p_nro_documento in varchar2, p_pai_cod_pais in varchar2, p_id_numero in varchar2) return varchar2
  IS
  /**************************************************
    Busca la profesion asociada a una persona f�sica
  ***************************************************/
    v_profesion varchar2(50);
  BEGIN
    -- Busco la profesion
    begin
      select id_caracteristica into v_profesion
      from
        ( select *
          from RCIVIL.VT_CARACTERISTICA_PERS
          where
            id_tipo_caracteristica in ('2021', '2009') and -- Profesiones y Oficios IPJ.TYPES.c_caract_profesion and
            id_sexo = p_id_sexo and
            nro_documento = p_nro_documento and
            pai_cod_pais = p_pai_cod_pais and
            id_numero = p_id_numero
          order by fec_inicio desc
        ) t
       where rownum = 1;
    exception
      when NO_DATA_FOUND then
        v_profesion := null;
    end;

    Return v_profesion;
  Exception
    When Others Then
      Return NULL;
  End FC_Buscar_ID_Profesion;

  FUNCTION FC_Buscar_Tipo_Profesion(p_id_sexo in varchar2, p_nro_documento in varchar2, p_pai_cod_pais in varchar2, p_id_numero in varchar2) return varchar2
  IS
  /**************************************************
    Busca la profesion asociada a una persona f�sica
  ***************************************************/
    v_id_caracteristica varchar2(10);
  BEGIN
    -- Busco la profesion
    begin
      select id_tipo_caracteristica into v_id_caracteristica
      from
        ( select *
          from RCIVIL.VT_CARACTERISTICA_PERS
          where
            id_tipo_caracteristica in ('2021', '2009') and -- Profesiones y Oficios IPJ.TYPES.c_caract_profesion and
            id_sexo = p_id_sexo and
            nro_documento = p_nro_documento and
            pai_cod_pais = p_pai_cod_pais and
            id_numero = p_id_numero
          order by fec_inicio desc
        ) t
       where rownum = 1;
    exception
      when NO_DATA_FOUND then
        v_id_caracteristica := null;
    end;

    return v_id_caracteristica;
  Exception
    When Others Then
      Return NULL;
  End FC_Buscar_Tipo_Profesion;

  FUNCTION FC_Armar_Calle_Dom_Id (p_id_vin in number) return varchar2
  IS
   v_row_dom dom_manager.vt_domicilios_cond%rowtype;
  BEGIN
    -- Busco el domicilio
    select * into v_row_dom
    from dom_manager.vt_domicilios_cond
    where
      id_vin = p_id_vin;

    return IPJ.VARIOS.FC_Armar_Calle_Dom(v_row_dom.n_calle, v_row_dom.altura, v_row_dom.piso, v_row_dom.depto, v_row_dom.torre, v_row_dom.mzna, v_row_dom.lote, v_row_dom.n_barrio, v_row_dom.km, v_row_dom.n_tipocalle);
  END FC_Armar_Calle_Dom_Id;

  FUNCTION FC_Armar_Calle_Dom(p_n_calle in varchar2, p_altura in number,
    p_piso in varchar2, p_depto in varchar2, p_torre in varchar2, p_mzna in varchar2,
    p_lote in varchar2, p_barrio in varchar2, p_km varchar2,
    p_tipo_calle varchar2) return varchar2
  IS
  /**************************************************
    Dado todos los datos de una posible ubicacion de un domicilio, arma la
    calle concatenando los valores v�lidos
  ***************************************************/
  BEGIN
    Return
      (case when nvl(upper(p_tipo_calle), 'SIN ASIGNAR') = 'SIN ASIGNAR' then 'Calle' else initcap(p_tipo_calle) end) || ' ' ||
      (case when nvl(UPPER(p_n_calle), 'SIN NOMBRE') = 'SIN NOMBRE' then 'Sin Nombre' else initcap(p_n_calle) end) ||
      (case when p_km is null then '' else ' Km. ' || to_char(p_km) end) ||
      (case when nvl(p_altura, 0) = 0 then '' else ' ' || to_char(p_altura) end) ||
      (case when p_piso is null then '' else  ', piso ' || p_piso end) ||
      (case when p_depto is null then '' else  ', departamento ' || p_depto end) ||
      (case when p_torre is null then '' else  ', torre/local ' || p_torre end) ||
      (case when p_mzna is null then '' else
        (case when p_n_calle is not null then ', ' else '' end) || 'manzana ' || p_mzna
       end) ||
      (case when p_lote is null then '' else
        (case when p_n_calle is not null or p_mzna is not null then ', ' else '' end) || 'lote ' || p_lote
      end)  ||
      (case
        when p_barrio is null then ''
        when p_barrio = 'S/A' then ''
        when p_barrio = 'S/D' then ''
        else ', barrio ' || initcap(p_barrio)
      end);
  Exception
    When Others Then
      Return NULL;
  End FC_Armar_Calle_Dom;

  FUNCTION FV_Valida_Entero(p_Entero IN VARCHAR2) return NUMBER is
    X NUMBER;
  /**************************************************
    Valida que sea un numero
  ***************************************************/
  BEGIN
    x := To_NUMBER(p_entero, '9999999999999999');
    Return 1;
  Exception
    When Others Then Return 0;
  End FV_Valida_Entero;

  FUNCTION FC_DOMICILIO_MODIFICADO(
    p_id_vin in number,
    p_id_integrante in number,
    p_id_legajo in number,
    p_cuit_empresa in varchar2,
    p_id_fondo_comercio in number,
    p_id_entidad_acta in number,
    p_es_comerciante in varchar2, -- S / N
    p_es_admin_entidad in varchar2,  -- S / N
    P_ID_PROVINCIA in varchar2,
    P_ID_DEPARTAMENTO in number,
    P_ID_LOCALIDAD in number,
    P_BARRIO in varchar2,
    P_CALLE in varchar2,
    P_ALTURA in number,
    P_DEPTO in varchar2,
    P_PISO in varchar2,
    p_torre in varchar2,
    p_manzana in varchar2,
    p_lote in varchar2,
    p_id_tipocalle in number,
    p_km in varchar2
    ) return char
 IS
   c_Dom sys_refcursor;
   v_id_tipodom number(4);
   v_n_tipodom varchar2(100);
   v_id_tipocalle number(2);
   v_n_tipocalle varchar2(30);
   v_id_calle number(10);
   v_n_calle varchar2(100);
   v_altura number(5);
   v_depto varchar2(4);
   v_piso varchar2(4);
   v_torre varchar2(20);
   v_id_barrio number(10);
   v_n_barrio varchar2(1000);
   v_manzana varchar2(5);
   v_lote varchar2(5);
   v_km varchar2(10);
   v_id_localidad number(6);
   v_n_localidad varchar2(100);
   v_id_departamento number(6);
   v_n_departamento varchar2(100);
   v_id_provincia varchar2(2);
   v_n_provincia varchar2(60);
   v_cpa varchar2(8);
   v_id_vin number(20);
   v_calle_inf varchar2(3000);
   v_id_afip number;
 BEGIN

   -- Busco la direccion anterior
   SP_Traer_Domicilio(
      p_id_vin => p_id_vin,
      p_id_integrante => p_id_integrante,
      p_id_legajo => p_id_legajo,
      p_cuit_empresa => p_cuit_empresa,
      p_id_fondo_comercio => p_id_fondo_comercio,
      p_id_entidad_acta => p_id_entidad_acta,
      p_es_comerciante => p_es_comerciante, -- S / N
      p_es_admin_entidad => p_es_admin_entidad, -- S / N
      p_Cursor => c_Dom);

   fetch c_Dom into
     v_calle_inf,
     v_id_tipodom,
     v_n_tipodom,
     v_id_tipocalle,
     v_n_tipocalle,
     v_id_calle,
     v_n_calle,
     v_altura,
     v_depto,
     v_piso,
     v_torre,
     v_id_barrio,
     v_n_barrio,
     v_id_localidad,
     v_n_localidad,
     v_id_departamento,
     v_n_departamento,
     v_id_provincia,
     v_n_provincia,
     v_cpa,
     v_id_vin,
     v_manzana,
     v_lote,
     v_km,
     v_id_afip;

     --Comparo la anterior con la nueva para ver si se modifico algo
     if upper(P_ID_PROVINCIA)  <> upper(v_id_provincia) or
       P_ID_DEPARTAMENTO <> nvl(v_id_departamento, 0) or
       P_ID_LOCALIDAD <> nvl(v_id_localidad, 0) or
       upper(substr(nvl(P_BARRIO,'@'), 1, 1000)) <> upper(nvl(v_n_barrio, '@')) or
       upper(substr(nvl(P_CALLE,'@'), 1, 70)) <> upper(nvl(v_n_calle, '@')) or
       nvl(P_ALTURA, 0) <> nvl(v_altura, 0) or
       upper(substr(nvl(P_DEPTO, '@'), 1, 4)) <> upper(nvl(v_depto, '@')) or
       upper(substr(nvl(P_PISO, '@'), 1, 4)) <> upper(nvl(v_piso, '@')) or
       upper(substr(nvl(P_TORRE, '@'), 1, 29)) <> upper(nvl(v_torre, '@')) or
       upper(substr(nvl(P_MANZANA, '@'), 1, 29)) <> upper(nvl(v_manzana, '@')) or
       upper(substr(nvl(P_LOTE, '@'), 1, 29)) <> upper(nvl(v_lote, '@')) or
       upper(substr(nvl(P_km, '@'), 1, 10)) <> upper(nvl(v_km, '@')) or
       nvl(p_id_tipocalle, 0) <> nvl(v_id_tipocalle, 0)   then

       return 'S';
     else
       return 'N';
     end if;
 END FC_DOMICILIO_MODIFICADO;

  FUNCTION FC_Generar_Entidad_Dom (
      p_id_integrante in number,
      p_id_legajo in number,
      p_cuit_empresa in varchar2,
      p_id_fondo_comercio in number,
      p_id_entidad_acta in number,
      p_es_comerciante in varchar2, -- S / N
      p_es_admin_entidad in varchar2, -- S / N
      p_id_admin in number
    ) return varchar2
  IS
    v_entidad_dom varchar2(20);
    v_tipo varchar2(2);
    v_nro_documento varchar2(20);
    v_row_integrante IPJ.T_INTEGRANTES%rowtype;
  BEGIN
    /*********************************************************
      Esta funcion arma una mascara para el campo ENTIDAD de los Domicilios de DOM_MANAGER
    *********************************************************/
    --Agrego una letra para ditinguir matriculas de personas fisicas (comerciantes, admin o integrante)
    if UPPER(p_es_comerciante) = 'S' then
      v_tipo := 'C';
    else
      if UPPER(p_es_admin_entidad) = 'S' then
        v_tipo := 'A';
        --v_entidad_dom := to_char(IPJ.TYPES.C_ID_APLICACION) || v_tipo || lpad(to_char(p_id_admin), 15, 0) ;
      else
        v_tipo := 'I';
      end if;
    end if;

    -- controlo si la entidad ya tiene una direccion
    if p_id_legajo >  0 then
      v_tipo := 'E';
      v_entidad_dom := to_char(IPJ.TYPES.C_ID_APLICACION) || v_tipo || lpad(to_char(p_id_legajo), 15, 0) ;
    end if;

     if p_cuit_empresa is not null then
      v_entidad_dom := p_cuit_empresa || '00';
    end if;

    if p_id_integrante >  0 then
       select * into v_row_integrante
       from IPJ.t_integrantes i
       where i.id_integrante = p_id_integrante;

       if v_tipo <> 'I' then
         v_entidad_dom := to_char(IPJ.TYPES.C_ID_APLICACION) || v_tipo || lpad(v_row_integrante.nro_documento, 15, 0) ;
       else
         v_entidad_dom := v_row_integrante.id_sexo || v_row_integrante.nro_documento || v_row_integrante.pai_cod_pais || to_char(v_row_integrante.id_numero) ;
       end if;
    end if;

    if p_id_fondo_comercio > 0 then
      v_tipo := 'F';
      v_entidad_dom := to_char(IPJ.TYPES.C_ID_APLICACION) || v_tipo || lpad(to_char(p_id_fondo_comercio), 15, 0) ;
    end if;

    if p_id_entidad_acta > 0 then
      v_tipo := 'EA';
      v_entidad_dom := to_char(IPJ.TYPES.C_ID_APLICACION) || v_tipo || lpad(to_char(p_id_fondo_comercio), 15, 0) ;
    end if;

    return  (case when v_entidad_dom is null then to_char(IPJ.TYPES.C_ID_APLICACION) || v_tipo else v_entidad_dom end);
  END FC_Generar_Entidad_Dom;

  FUNCTION MENSAJE_ERROR(p_Codigo IN VARCHAR2) return varchar2 is
    res varchar2(4000);
   /**************************************************
    Informa el mensaje de error solicitado
  ***************************************************/
  BEGIN
    select n_mensaje_error into res
    from IPJ.T_mensajes_error
    where
      UPPER(comentario) = upper(p_Codigo);

    Return res;
  Exception
    When Others Then
      Return 'ERROR: No se encontr� el mensaje solicitado (' || p_Codigo || ').';
  End MENSAJE_ERROR;

  FUNCTION FC_ES_FIN_SEMANA(dia in varchar2, mes in varchar2, anio in number) return number is
    fecha date;
    res varchar2(2);
  /**************************************************
    Devuele 1 si la fecha es fin de semana, 2 si es invalido y 0 en caso contrario
  ***************************************************/
  BEGIN
    fecha := to_date(dia || '/' || mes || '/' || to_char(anio), 'dd/mm/rrrr');
    res := to_char(fecha, 'd');
    if res in (1, 7) then
      Return 1;
    else
      Return 0;
    end if;
  Exception
    When Others Then Return 2;
  End FC_ES_FIN_SEMANA;

  FUNCTION TONUMBER(DOC IN VARCHAR2) return NUMBER is
    X NUMBER;
  /**************************************************
    Valida que sea un numero
  ***************************************************/
  BEGIN
    x := To_NUMBER(replace(DOC, ',', '.'), '99999999999999999999.9999'); --Bug 8746
    Return x;
  Exception
    When Others Then Return NULL;
  End TONUMBER;

  FUNCTION FC_Letra_Matricula(p_matricula IN VARCHAR2) return varchar2
  IS
  /**************************************************
    Obtiene la Letra de una Matricula
  ***************************************************/
  BEGIN
    Return trim(TRANSLATE(upper(replace(p_matricula, '.', '')), '-0123456789','           '));
  Exception
    When Others Then
      Return NULL;
  End FC_Letra_Matricula;

  FUNCTION FC_Numero_Matricula(p_matricula IN VARCHAR2) return varchar2
  IS
  /**************************************************
    Obtiene los Numeros de una Matricula
  ***************************************************/
  BEGIN
    Return trim(TRANSLATE(upper(replace(p_matricula, '.', '')), '-ABCDEFGH#','          '));
  Exception
    When Others Then
      Return NULL;
  End FC_Numero_Matricula;

  FUNCTION FC_Formatear_Matricula(p_matricula IN VARCHAR2) return varchar2
  IS
  /**************************************************
    La retorna en formato LNNNNNN, en donde L es la letra y NNNNNN los numeros.
  ***************************************************/
  BEGIN
    Return FC_Letra_Matricula(p_matricula) || FC_Numero_Matricula(p_matricula);
  Exception
    When Others Then
      Return NULL;
  End FC_Formatear_Matricula;

  FUNCTION FC_Formatear_Denominacion(p_Texto IN VARCHAR2) return varchar2
  IS
  /***********************************************************
    Formatea el texto para la validaci�n de t�rminos exclu�dos
  ************************************************************/
  v_texto VARCHAR2(1000);
  BEGIN
    --Quito los m�ltiples espacios en blanco
    v_texto := REGEXP_REPLACE(p_texto, '  *', ' ');
    --Quito los acentos
    v_texto := varios.FC_Quitar_Acentos(v_texto);
    --Quito otros tipos de acentos
    v_texto := TRANSLATE(TRANSLATE(v_texto, '�����','AEIOU'), '�����', 'AEIOU');
    --Quito los tipos societarios
    --v_texto := varios.FC_Quitar_Tipo_Empresa(v_texto);
    --Quito los puntos, guiones, par�ntesis, comillas dobles, signo +
    v_texto := REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(v_Texto,'.',''),'-',''),'_',''),'(',''),')',''),'"',''),'+',''),'@','A'),'�',''),'''',''),'`',''),'&',' '),']',''),'[',''),'{',''),'}',''), ',', '');
    --Quito los m�ltiples espacios en blanco
    v_texto := REGEXP_REPLACE(v_texto, '  *', ' ');
    --Cuando termino quito los espacios al principio y al final
    v_texto := TRIM(v_texto);

    Return v_texto;
  Exception
    When Others Then
      Return NULL;
  End FC_Formatear_Denominacion;

  FUNCTION VALIDA_SIGLA_TIPO_ENTIDAD(p_Sigla in varchar2, p_id_tipo_entidad IN Number) return Number is
    res Number;
  /**************************************************
    Valida que Tipo de Accion Exista (0 = no existe)
  ***************************************************/
  BEGIN
    select count(*) into res
    from IPJ.t_tipos_entidades
    where
      sigla = p_sigla and
      id_tipo_entidad = nvl(p_id_tipo_entidad, 0);

    Return res;
  Exception
    When Others Then Return 0;
  End VALIDA_SIGLA_TIPO_ENTIDAD;

  FUNCTION VALIDA_ENTIDAD(p_id_tramite_ipj IN number, p_id_legajo in number) return Number is
    res Number;
  /**************************************************
    Valida que la entidad Exista (0 = no existe)
  ***************************************************/
  BEGIN
    select count(*) into res
    from IPJ.t_entidades
    where
      ID_TRAMITE_IPJ = p_id_tramite_ipj and
      ID_LEGAJO = p_id_legajo;

    Return res;
  Exception
    When Others Then Return 0;
  End VALIDA_ENTIDAD;

  FUNCTION VALIDA_ESTADO(Estado IN Number) return Number is
    res Number;
  /**************************************************
    Valida que el estado de una transaccion Exista (0 = no existe)
  ***************************************************/
  BEGIN
    select count(*) into res
    from IPJ.t_estados
    where id_estado = Estado;

    Return res;
  Exception
    When Others Then Return 0;
  End VALIDA_ESTADO;

  FUNCTION VALIDA_FECHA(Fec IN VARCHAR2) return char is
    res date;
  /**************************************************
    Valida que la fecha sea valida (S/N)
  ***************************************************/
  BEGIN
    res := to_date(Fec, 'dd/mm/rrrr');
    if Fec is null then
      Return 'N';
    else
      Return 'S';
    end if;
  Exception
    When Others Then Return 'N';
  End VALIDA_FECHA;

  FUNCTION VALIDA_FECHA(Fec IN VARCHAR2, Masc in VARCHAR2) return char is
    res date;
  /**************************************************
    Valida que la fecha sea valida (S/N)
  ***************************************************/
  BEGIN
    res := to_date(Fec, Masc);
    if Fec is null then
      Return 'N';
    else
      Return 'S';
    end if;
  Exception
    When Others Then Return 'N';
  End VALIDA_FECHA;

  FUNCTION VALIDA_INTEGRANTE(Integrante IN number) return Number is
    res Number;
  /**************************************************
    Valida que el integrante Exista (0 = no existe)
  ***************************************************/
  BEGIN
    select count(*) into res
    from IPJ.t_integrantes
    where id_integrante = nvl(integrante, 0);

    Return res;
  Exception
    When Others Then Return 0;
  End VALIDA_INTEGRANTE;

  FUNCTION VALIDA_TIPO_ACTA(p_Tipo_acta IN Number) return Number is
    res Number;
  /**************************************************
    Valida que el Tipo de Acta de las entidades Exista (0 = no existe)
  ***************************************************/
  BEGIN
    select count(*) into res
    from IPJ.t_Tipos_Acta
    where id_tipo_acta = nvl(p_Tipo_acta, 0);

    Return res;
  Exception
    When Others Then Return 0;
  End VALIDA_TIPO_ACTA;

  FUNCTION VALIDA_TIPO_ORDENDIA(p_Tipo_Orden IN Number) return Number is
    res Number;
  /**************************************************
    Valida que el Tipo de Orden del D�a de las actas Exista (0 = no existe)
  ***************************************************/
  BEGIN
    select count(*) into res
    from IPJ.t_Tipos_Orden_Dia
    where id_tipo_orden_dia = nvl(p_Tipo_orden, 0);

    Return res;
  Exception
    When Others Then Return 0;
  End VALIDA_TIPO_ORDENDIA;

  FUNCTION VALIDA_ARCH_ACTA(p_Archivo_Acta IN Number) return Number is
    res Number;
  /**************************************************
    Valida que el Archivo sea v�lido para las actas(0 = no existe)
  ***************************************************/
  BEGIN
    select count(*) into res
    from IPJ.t_Archivos_Acta
    where id_archivo = nvl(p_Archivo_Acta, 0);

    Return res;
  Exception
    When Others Then Return 0;
  End VALIDA_ARCH_ACTA;

  FUNCTION VALIDA_TIPO_INS_LEGAL(p_Tipo_Ins_Legal IN Number) return Number is
    res Number;
  /**************************************************
    Valida que el Tipo de Instrumento Legal de las entidades Exista (0 = no existe)
  ***************************************************/
  BEGIN
    select count(*) into res
    from IPJ.t_Tipos_Ins_Legal
    where il_tipo = nvl(p_Tipo_Ins_Legal, 0);

    Return res;
  Exception
    When Others Then Return 0;
  End VALIDA_TIPO_INS_LEGAL;

  FUNCTION VALIDA_TIPO_INTEGRANTE(p_Tipo_Integrante IN Number) return Number is
    res Number;
  /**************************************************
    Valida que el Tipo de Integrante Exista (0 = no existe)
  ***************************************************/
  BEGIN
    select count(*) into res
    from IPJ.t_Tipos_Integrante
    where id_tipo_integrante = nvl(p_Tipo_Integrante, 0);

    Return res;
  Exception
    When Others Then Return 0;
  End VALIDA_TIPO_INTEGRANTE;

  FUNCTION VALIDA_TIPO_ACCION(p_Tipo_Accion IN Number) return Number is
    res Number;
  /**************************************************
    Valida que Tipo de Accion Exista (0 = no existe)
  ***************************************************/
  BEGIN
    select count(*) into res
    from IPJ.t_Tipos_AccionesIpj
    where id_tipo_accion = nvl(p_Tipo_Accion, 0);

    Return res;
  Exception
    When Others Then Return 0;
  End VALIDA_TIPO_ACCION;

  FUNCTION VALIDA_TIPO_ORGANISMO(p_id_Tipo_Organismo IN Number) return Number is
    res Number;
  /**************************************************
    Valida que Tipo de Organismo Exista (0 = no existe)
  ***************************************************/
  BEGIN
    select count(*) into res
    from IPJ.t_Tipos_Organismo
    where id_tipo_organismo = nvl(p_id_Tipo_organismo, 0);

    Return res;
  Exception
    When Others Then Return 0;
  End VALIDA_TIPO_ORGANISMO;

  FUNCTION VALIDA_TIPO_CLASIFICACION(p_Tipo_Clasif IN Number) return Number is
    res Number;
  /**************************************************
    Valida que Tipo de Accion Exista (0 = no existe)
  ***************************************************/
  BEGIN
    select count(*) into res
    from IPJ.t_Tipos_Clasif_Ipj
    where id_Clasif_Ipj = nvl(p_Tipo_Clasif, 0);

    Return res;
  Exception
    When Others Then Return 0;
  End VALIDA_TIPO_CLASIFICACION;


  FUNCTION VALIDA_USUARIO(Cuil IN varchar2) return Number is
    res Number;
  /**************************************************
    Valida que el Usuario de Cidi exista como usuario (0 = no existe)
  ***************************************************/
  BEGIN
    select count(*) into res
    from IPJ.t_usuarios
    where cuil_usuario = Cuil;

    Return res;
  Exception
    When Others Then Return 0;
  End VALIDA_USUARIO;


  FUNCTION VALIDA_LIBRO_ENTIDAD(p_id_legajo IN number,
    p_TRAMITE_IPJ in number, p_TIPO_LIBRO in number) return Number is
    res Number;
    v_cuit varchar2(11);
    v_max number;
    v_min number;
    v_total number;
    v_es_mecanizado char(1);
  /**************************************************
    Valida que los libros rubricados de una entidad sean consecutivos y sin saltos
    0 = OK y cualquier otro es error
  ***************************************************/
  begin
    select mecanizado into v_es_mecanizado
    from ipj.t_tipos_libros
    where
      id_tipo_libro = p_tipo_libro;

   -- Si es mecanizado, no controlo saltos en los libros
   if v_es_mecanizado = 'S' then
     res := 0;
   else
      begin
        select max(IR.NRO_LIBRO), min(IR.NRO_LIBRO), count(IR.NRO_LIBRO) into v_max, v_min, v_total
        from IPJ.T_ENTIDADES_RUBRICA ir
        where
          IR.ID_LEGAJO = p_id_legajo and
          IR.ID_TIPO_LIBRO = p_tipo_libro;

        res := nvl((v_max - v_min + 1) - v_total, 0);
      exception
        when OTHERS then res := 0;
      end;
    end if;

    return res;
  exception
    when OTHERS then return 0;
  end VALIDA_LIBRO_ENTIDAD;

  PROCEDURE SP_Traer_Tipos_Actas(
    p_Cursor OUT types.cursorType,
    p_Id_Tipo_Accion in number)
  IS
    v_id_ubicacion number(10);
  /**************************************************
    Lista los Tipos de Actas disponibles
  ***************************************************/
  BEGIN
    if p_Id_Tipo_Accion <> 0 then
      select tClas.id_ubicacion into v_id_ubicacion
      from IPJ.t_Tipos_AccionesIPJ tacc join IPJ.T_Tipos_Clasif_Ipj tClas
          on TACC.ID_CLASIF_IPJ = TCLAS.ID_CLASIF_IPJ
      where
        tacc.id_tipo_accion = p_id_tipo_accion;
    else
      v_id_ubicacion := 0;
    end if;

    OPEN p_Cursor FOR
      select t.id_tipo_acta, t.n_tipo_acta, t.id_ubicacion, u.n_ubicacion
      from ipj.t_tipos_acta t join ipj.t_ubicaciones u
          on t.id_ubicacion = u.id_ubicacion
      where
        v_id_ubicacion = 0 or t.id_ubicacion = v_id_ubicacion;

  END SP_Traer_Tipos_Actas;

  PROCEDURE SP_Traer_Tipos_Integrantes(
    p_Cursor OUT types.cursorType,
    p_id_tipo_accion in number)
  IS
    v_id_ubicacion number(10);
  /**************************************************
    Lista los Tipos de Integrantes disponibles
  ***************************************************/
  BEGIN
    if p_id_tipo_Accion <> 0 then
      select tClas.id_ubicacion into v_id_ubicacion
      from IPJ.t_Tipos_AccionesIPJ tacc join IPJ.T_Tipos_Clasif_Ipj tClas
          on TACC.ID_CLASIF_IPJ = TCLAS.ID_CLASIF_IPJ
      where
        tacc.id_tipo_accion = p_id_tipo_accion;
    else
      v_id_ubicacion := 0;
    end if;

    OPEN p_Cursor FOR
      select t.id_tipo_integrante, t.n_tipo_integrante, t.id_ubicacion, u.n_ubicacion,
        T.Id_Tipo_Organismo, t.Es_Suplente, t.Repetible
      from ipj.t_tipos_integrante t join ipj.t_ubicaciones u
          on t.id_ubicacion = u.id_ubicacion
      where
        t.id_tipo_integrante > IPJ.TYPES.C_ID_TIPO_SOCIO and  -- Distintos de Socios
        (v_id_ubicacion = 0 or t.id_ubicacion = v_id_ubicacion);
  END SP_Traer_Tipos_Integrantes;

  PROCEDURE SP_TRAER_TIPOS_ACCIONESIPJ(
    p_id_clasif in number,
    p_tipo_persona in number := 1, -- 1= Juridica / 2 = Fisica / 4 = Sin Persona
    p_Cursor OUT types.cursorType)
  IS
  /**************************************************
    Lista los Tipos de Tr�mites disponibles
  ***************************************************/
  BEGIN
    OPEN p_Cursor FOR
      select
        tt.id_tipo_accion, tt.n_tipo_accion, tt.id_conf_accion, tt.id_tipo_persona, tt.id_protocolo,
        tt.id_clasif_ipj, tt.id_pagina, tt.id_accion_archivo, tt.informar_boletin, trim(To_Char(nvl(tt.tiempo_demora, '0'), '9999990.99')) tiempo_demora,
        trim(To_Char(nvl(tt.tiempo_demora_urg, '0'), '9999990.99')) tiempo_demora_urg, tt.codigo_habilitacion,
        tp.n_tipo_persona, tc.n_clasif_ipj, cp.n_protocolo, p.N_Pagina,
        U.Id_Ubicacion, U.N_Ubicacion,I.Id_Informe,I.N_Informe, nvl(tt.req_reemp, 'N') req_reemp
      from ipj.t_tipos_accionesipj tt left join IPJ.T_TIPOS_CLASIF_PERSONAS tp
          on tt.id_tipo_persona = tp.id_tipo_persona
        join IPJ.T_TIPOS_CLASIF_IPJ tc
          on tt.id_clasif_ipj = tc.id_clasif_ipj
        left join ipj.t_acciones_archivo aa
          on tt.id_accion_archivo = aa.id_accion_archivo
        left join ipj.t_config_protocolo cp
          on tt.id_protocolo = cp.id_protocolo
        join ipj.t_paginas p
          on tt.id_pagina = p.id_pagina
        join ipj.t_ubicaciones u
          on u.id_ubicacion = tc.id_ubicacion
        left join IPJ.T_INFORMES_TRAMITE it
          on Tt.Id_Tipo_Accion = It.Id_Tipo_Accion
        left Join Ipj.T_Informes I
          On It.Id_Informe = I.Id_Informe
      where
        (p_id_clasif = 0 or TT.ID_CLASIF_IPJ = p_id_clasif) and
        (p_tipo_persona = 0 or bitand(tt.id_tipo_persona, p_tipo_persona)=p_tipo_persona)
      order by tt.n_tipo_accion;

  END SP_TRAER_TIPOS_ACCIONESIPJ;

  PROCEDURE SP_TRAER_TIPOS_CLASIF_IPJ(
    p_ubicacion in number,
    p_ubicacion_original in number,
    p_clasif_actual in number,
    o_Cursor OUT types.cursorType)
  IS
  /**************************************************
    Lista Clasificaciones de IPJ disponibles.
    Si se pasa una Ubicacion, lista solo esas clasificaciones, sino lista todas
  ***************************************************/
  BEGIN
    OPEN o_Cursor FOR
    select distinct c.ID_CLASIF_IPJ, c.ID_TIPO_PERSONA, c.ID_UBICACION,
      c.N_CLASIF_IPJ, U.N_UBICACION
    from  ipj.t_ubicaciones u join ipj.t_tipos_clasif_ipj c
        on u.id_ubicacion = c.id_ubicacion
      left join ipj.t_workflow w
       on W.ID_CLASIF_IPJ_PROXIMA = C.ID_CLASIF_IPJ
    where
      (nvl(p_ubicacion_original, 0) = 0 or W.ID_UBICACION_ORIGEN = p_ubicacion_original) and
      (p_clasif_actual = 0 or W.ID_CLASIF_IPJ_ACTUAL = p_clasif_actual) and
      (nvl(p_ubicacion, 0) = 0 or c.id_ubicacion = p_ubicacion)
    order by c.N_CLASIF_IPJ;

  END SP_TRAER_TIPOS_CLASIF_IPJ;

  PROCEDURE SP_Traer_Estados(
    p_Cursor OUT types.cursorType)
  IS
  /**************************************************
    Lista los Estados de Tr�mites disponibles
  ***************************************************/
  BEGIN
    OPEN p_Cursor FOR
    select t.id_estado, t.n_estado
    from ipj.t_estados t;
  END SP_Traer_Estados;


  PROCEDURE SP_Traer_Monedas(
    o_Cursor OUT types.cursorType)
  IS
  /**************************************************
    Lista los Tipos de Monedas disponibles
  ***************************************************/
  BEGIN
    OPEN o_Cursor FOR
      select M.ID_MONEDA, initcap(M.N_MONEDA) N_MONEDA, Conv_Peso
      from T_COMUNES.T_MONEDAS m;
  END SP_Traer_Monedas;

  PROCEDURE SP_Traer_Est_Civil(
    o_Cursor OUT types.cursorType)
  IS
    /**************************************************
    Lista los estados civiles definidos para las personas
  ***************************************************/
  BEGIN
    OPEN o_Cursor FOR
      select S.ID_ESTADO_CIVIL, S.N_ESTADO_CIVIL
      from RCIVIL.VT_ESTADOS_CIVIL s
      where
        s.id_estado_civil not in ('Z', 'O');

  END SP_Traer_Est_Civil;

  PROCEDURE SP_Traer_Meses(
    o_Cursor OUT types.cursorType)
  IS
    /**************************************************
    Lista los meses definidos en gobierno
  ***************************************************/
  BEGIN
    OPEN o_Cursor FOR
      select Id_Mes, N_Mes, Cant_Dias
      from t_comunes.t_meses;

  END SP_Traer_Meses;

  PROCEDURE SP_Traer_Sexo(
    o_Cursor OUT types.cursorType)
  IS
    /**************************************************
    Lista los generod definidos para las personas
  ***************************************************/
  BEGIN
    OPEN o_Cursor FOR
      select S.ID_SEXO, S.TIPO_SEXO
      from T_COMUNES.VT_SEXOS s;

  END SP_Traer_Sexo;

  PROCEDURE SP_Traer_Juzgados(
    o_Cursor OUT types.cursorType)
  IS
  /**************************************************
    Lista los Juzgados de la Provincia de C�rdoba
  ***************************************************/
  BEGIN
    OPEN o_Cursor FOR
      select J.ID_JUZGADO, J.N_JUZGADO, J.ACTIVO
      from IPJ.T_JUZGADOS j
      where
        J.ACTIVO = 'S';
  END SP_Traer_Juzgados;


  PROCEDURE SP_BUSCAR_SAGRADA_FLIA(
    p_ingreso in number := 0,
    p_numero in number := 0,
    p_denominacion in number := 0,
    p_responsable in number := 0,
    p_tipo in number := 0,
    p_hecho in number := 0,
    p_observacion in number := 0,
    P_tipo_busqueda in number, -- 0 = contiene / 1 = finaliza / 2 = inicia / 3 = exacta
    p_cadena_busqueda in varchar2,
    p_Cursor OUT types.cursorType)
  IS
  /**************************************************
    Busqueda de los datos de la planilla, por cualquier campo y de cualquier forma
  ***************************************************/
  BEGIN
    /* Este procedimiento realiza una busqueda sobre cualquier columna de la tabla.
       Posee los siguientes par�metro :
       - 1 par�metro para habilitar o no cada columna: 0 = no habilitado / 1 = habilitado
       - P_tipo_busqueda define el tipo de busqueda: 0 = contiene / 1 = finaliza / 2 = inicia / 3 = exacta
       - p_cadena_busqueda: cadena a buscar.
    */
    OPEN p_Cursor FOR
    select s.ingreso, s.numero, s.denominacion, s.responsable, s.tipo, s.hecho, s.observacion
    from ipj.t_sagrada_flia_xls s
    where
      (p_ingreso > 0 and
        ( (p_tipo_busqueda = 0 and upper(s.ingreso) like '%'|| upper(p_cadena_busqueda) || '%') or
          (p_tipo_busqueda = 1 and upper(s.ingreso) like '%'|| upper(p_cadena_busqueda)) or
          (p_tipo_busqueda = 2 and upper(s.ingreso) like upper(p_cadena_busqueda) || '%') or
          (p_tipo_busqueda = 3 and upper(s.ingreso) = upper(p_cadena_busqueda))
         )) or
      (p_numero > 0 and
        ( (p_tipo_busqueda = 0 and upper(s.numero) like '%'|| upper(p_cadena_busqueda) || '%') or
          (p_tipo_busqueda = 1 and upper(s.numero) like '%'|| upper(p_cadena_busqueda)) or
          (p_tipo_busqueda = 2 and upper(s.numero) like upper(p_cadena_busqueda) || '%') or
          (p_tipo_busqueda = 3 and upper(s.numero) = upper(p_cadena_busqueda))
         )) or
      (p_denominacion > 0 and
        ( (p_tipo_busqueda = 0 and upper(s.denominacion) like '%'|| upper(p_cadena_busqueda) || '%') or
          (p_tipo_busqueda = 1 and upper(s.denominacion) like '%'|| upper(p_cadena_busqueda)) or
          (p_tipo_busqueda = 2 and upper(s.denominacion) like upper(p_cadena_busqueda) || '%') or
          (p_tipo_busqueda = 3 and upper(s.denominacion) = upper(p_cadena_busqueda))
         )) or
      (p_responsable > 0 and
        ( (p_tipo_busqueda = 0 and upper(s.responsable) like '%'|| upper(p_cadena_busqueda) || '%') or
          (p_tipo_busqueda = 1 and upper(s.responsable) like '%'|| upper(p_cadena_busqueda)) or
          (p_tipo_busqueda = 2 and upper(s.responsable) like upper(p_cadena_busqueda) || '%') or
          (p_tipo_busqueda = 3 and upper(s.responsable) = upper(p_cadena_busqueda))
         )) or
      (p_tipo > 0 and
        ( (p_tipo_busqueda = 0 and upper(s.tipo) like '%'|| upper(p_cadena_busqueda) || '%') or
          (p_tipo_busqueda = 1 and upper(s.tipo) like '%'|| upper(p_cadena_busqueda)) or
          (p_tipo_busqueda = 2 and upper(s.tipo) like upper(p_cadena_busqueda) || '%') or
          (p_tipo_busqueda = 3 and upper(s.tipo) = upper(p_cadena_busqueda))
         )) or
      (p_hecho > 0 and
        ( (p_tipo_busqueda = 0 and upper(s.hecho) like '%'|| upper(p_cadena_busqueda) || '%') or
          (p_tipo_busqueda = 1 and upper(s.hecho) like '%'|| upper(p_cadena_busqueda)) or
          (p_tipo_busqueda = 2 and upper(s.hecho) like upper(p_cadena_busqueda) || '%') or
          (p_tipo_busqueda = 3 and upper(s.hecho) = upper(p_cadena_busqueda))
         )) or
      (p_observacion > 0 and
        ( (p_tipo_busqueda = 0 and upper(s.observacion) like '%'|| upper(p_cadena_busqueda) || '%') or
          (p_tipo_busqueda = 1 and upper(s.observacion) like '%'|| upper(p_cadena_busqueda)) or
          (p_tipo_busqueda = 2 and upper(s.observacion) like upper(p_cadena_busqueda) || '%') or
          (p_tipo_busqueda = 3 and upper(s.observacion) = upper(p_cadena_busqueda))
         )) ;
  END SP_BUSCAR_SAGRADA_FLIA;


  PROCEDURE SP_Buscar_Actividad(
    o_Cursor OUT types.cursorType,
    o_rdo out number,
    p_id_actividad in varchar2
    )
  IS
  /**************************************************
    Busca el nombre de una actividad asociada al indice indicado por la afip
  ***************************************************/
  BEGIN
    select count(*) into o_rdo
    from T_COMUNES.VT_ACTIVIDADES a
    where
      lpad(A.ID_ACTIVIDAD, 10, '0') = lpad(p_id_actividad, 10, '0') and
      a.fecha_fin_act is null and
      a.id_afip is not null;

    if o_rdo > 0 then
      OPEN o_Cursor FOR
        select a.n_actividad, a.id_actividad, a.n_rubro, a.id_rubro, a.id_tipo_actividad, ta.n_tipo_actividad
        from T_COMUNES.VT_ACTIVIDADES a join T_COMUNES.VT_TIPOS_ACTIVIDAD ta
          on A.ID_TIPO_ACTIVIDAD = TA.ID_TIPO_ACTIVIDAD and a.id_rubro = ta.id_rubro
        where
          lpad(A.ID_ACTIVIDAD, 10, '0') = lpad(p_id_actividad, 10, '0') and
          a.fecha_fin_act is null and
          a.id_afip is not null;
    else
       OPEN o_Cursor FOR
       select 'Actividad Inv�lida' n_actividad, -1 id_actividad, null n_rubro, -1 id_rubro, -1 id_tipo_actividad
       from dual;
    end if;

  END SP_Buscar_Actividad;


  PROCEDURE SP_Traer_Tipo_Libros_Rubricas(
    o_Cursor OUT types.cursorType,
    p_mecanizados in varchar2   -- Valores S / N
    )
  IS
  /**************************************************
    Lista los libros de Tipos para Rubricas,  filtrando mecanizados o no
  ***************************************************/
  BEGIN
    OPEN o_Cursor FOR
      select T.ID_TIPO_LIBRO, T.TIPO_LIBRO, T.MECANIZADO, t.HABILITA_TITULO
      from IPJ.T_TIPOS_LIBROS t
      where
        p_mecanizados is null or T.MECANIZADO = p_mecanizados
      order by T.TIPO_LIBRO asc;

  END SP_Traer_Tipo_Libros_Rubricas;


   PROCEDURE SP_Traer_Informe(
    o_Cursor OUT types.cursorType,
    o_rdo OUT varchar2,
    p_id_tipo_accion in number,
    p_id_informe in number)
  IS
  /**************************************************
    Lista los libros de Tipos para Rubricas,  filtrando mecanizados o no
  ***************************************************/
  v_id_informe number;
  v_cant_lineas number;
  v_archivo varchar(50);
  v_n_informe varchar(100);
  BEGIN
    --busco el informe asociado al tramite
    begin
      if nvl(p_id_informe, 0) = 0 then
        select it.id_informe, replace(n_informe, ' '), n_informe into v_id_informe, v_archivo, v_n_informe
        from IPJ.T_INFORMES_TRAMITE it join IPJ.T_informes i
          on IT.ID_INFORME = I.ID_INFORME
        where
          id_tipo_accion = p_id_tipo_accion and
          rownum = 1;
      else
        select i.id_informe, replace(n_informe, ' '), n_informe into v_id_informe, v_archivo, v_n_informe
        from IPJ.T_INFORMES i
        where
          id_informe = p_id_informe;
      end if;
    exception
      when NO_DATA_FOUND then
        o_rdo := IPJ.VARIOS.MENSAJE_ERROR('REPORTE_NOT');
        return;
    end;

    -- obtengo la cantidad de lineas, para informarla en el cursor
    select count(*) into  v_cant_lineas
    from IPJ.T_INFORMES_LINEA il
    where
      il.id_informe = v_id_informe;

    OPEN o_Cursor FOR
      select il.id_informe, il.nro_orden, il.id_linea, TC.TIPO, l.TIPO_LINEA ,
        l.id_campo, l.id_validacion, l.negrita, l.espacios, l.texto_si, l.texto_no, l.tipo_fecha, L.CONCATENAR,
        v_cant_lineas Cant_Lineas, case when l.id_campo is null then 'N' else 'S' end Hay_campo,
        v_n_informe n_informe, l.Formato_Parrafo, nvl(l.separa_campo, 'S') separa_campo
      from IPJ.T_INFORMES_LINEA il join IPJ.T_LINEAS l
          on il.id_linea = l.id_linea
        left join IPJ.t_tipos_campo tc
          on l.id_Campo = tc.id_campo
      where
        il.id_informe = v_id_informe
      order by il.nro_orden;

    o_rdo := v_archivo;

  END SP_Traer_Informe;

  PROCEDURE SP_Traer_Informe_Cabecera(
    o_Cursor OUT types.cursorType,
    p_id_tipo_accion in number,
    p_id_informe in number)
  IS
  /**************************************************
    Dada una accion, devuelve la configuracion del Informe asociado
  ***************************************************/
  BEGIN
    --busco el informe asociado al tramite
    OPEN o_Cursor FOR
      select i.id_informe, replace(n_informe, ' ') n_informe, nvl(margen_sup, 0) margen_sup,
        nvl(margen_inf, 0) margen_inf, nvl(margen_izq, 0) margen_izq, nvl(margen_der, 0) margen_der,
        nvl(pagina, '') pagina, nvl(i.usa_cabecera, 0) usa_cabecera,  Marca_Agua,
        i.pie_pagina
      from IPJ.T_informes i left join IPJ.T_INFORMES_TRAMITE it
        on IT.ID_INFORME = I.ID_INFORME
      where
        ( ( nvl(p_id_informe, 0) = 0 and id_tipo_accion = p_id_tipo_accion) or
          i.id_informe = p_id_informe) and
        rownum = 1;

  END SP_Traer_Informe_Cabecera;

  PROCEDURE SP_Eliminar_Informe_Borrador(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in number,
    p_id_tramiteipj_accion in number,
    p_id_tipo_accion in number,
    p_quitar_doc char
  )
  IS
  /**************************************************
    Elimina el borrador de la accion indicada
  ***************************************************/
  BEGIN
    delete IPJ.T_INFORMES_BORRADOR
    where
      id_tramite_ipj = p_id_tramite_ipj and
      id_tramiteipj_accion = p_id_tramiteipj_accion and
      id_tipo_accion = p_id_tipo_accion;

    -- Si se elimina por firmas, no se quita el documento
    if nvl(p_quitar_doc, 'S') = 'S' then
      update ipj.t_tramitesipj_acciones
      set
        id_documento = null,
        n_documento = null
      where
        id_tramite_ipj = p_id_tramite_ipj and
        id_tramiteipj_accion = p_id_tramiteipj_accion;
    end if;

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     when OTHERS then
        o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Eliminar_Informe_Borrador;

  PROCEDURE SP_Traer_Informe_Borrador(
    o_Cursor OUT types.cursorType,
    o_rdo OUT varchar2,
    p_id_tramite_ipj in number,
    p_id_tramiteipj_accion in number,
    p_id_tipo_accion in number)
  IS
  /**************************************************
    Devuelve el borrador, si existe
  ***************************************************/
    v_id_tramiteipj_accion number;
    v_id_tipo_accion number;
  BEGIN
    -- Si no viene acci�n, busco la mayor que tenga un informe
     if nvl(p_id_tramiteipj_accion, 0) = 0 then
       begin
          select id_tramiteipj_accion, id_tipo_accion into  v_id_tramiteipj_accion, v_id_tipo_accion
          from IPJ.T_INFORMES_BORRADOR
          where
            id_tramite_ipj = p_id_tramite_ipj and
            rownum = 1;
       exception
         when NO_DATA_FOUND then
           v_id_tramiteipj_accion := 0;
           v_id_tipo_accion := 0;
       end;
     else
       v_id_tramiteipj_accion := p_id_tramiteipj_accion;
       v_id_tipo_accion := p_id_tipo_accion;
     end if;

      -- busca el nombre del informe para el archivo
     begin
       select replace(n_informe, ' ') into o_rdo
       from IPJ.T_INFORMES_TRAMITE it join IPJ.T_informes i
          on IT.ID_INFORME = I.ID_INFORME
       where
         id_tipo_accion = v_id_tipo_accion and
         rownum = 1;
     exception
       when NO_DATA_FOUND then
         o_rdo := '';
     end;

    --busco el informe asociado al tramite
    OPEN o_Cursor FOR
      select id_tramite_ipj, id_tramiteipj_accion, id_tipo_accion, texto_informe, to_char(fecha_informe, 'dd/mm/rrrr') Fecha_Emision
      from IPJ.T_INFORMES_BORRADOR
      where
        id_tramite_ipj = p_id_tramite_ipj and
        id_tramiteipj_accion = v_id_tramiteipj_accion and
        id_tipo_accion = v_id_tipo_accion;

  END SP_Traer_Informe_Borrador;

  PROCEDURE SP_Guardar_Informe_Borrador(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in number,
    p_id_tramiteipj_accion in number,
    p_id_tipo_accion in number,
    p_texto_informe in clob,
    p_obs_rubrica in varchar)
  IS
    v_id_tipo_accion number;
  /**************************************************
    Guarda el informe borrador de agun tramite y accion de informe
  ***************************************************/
  BEGIN
    if IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('HAB_LOG_VARIOS') = '1' then
      IPJ.VARIOS.SP_GUARDAR_LOG(
        p_METODO => 'SP_Guardar_Informe_Borrador',
        p_NIVEL => 'Base de Datos',
        p_ORIGEN => 'DB',
        p_USR_LOG  => 'DB',
        p_USR_WINDOWS => 'Sistema',
        p_IP_PC  => null,
        p_EXCEPCION =>
          'Tr�mite IPJ = ' ||  to_char(p_id_tramite_ipj)
          || ' / Id Tramite Accion = ' || to_char(p_id_tramiteipj_accion)
          || ' / Id Tipo Accion = ' || to_char(p_id_tipo_accion)
      );
    end if;

    -- Log para analizar probleas de informes vacios o en acciones erroneas
    -- Si no se guarda para un informe digital, lo logueo para analizar
    if p_id_tipo_accion not in (151, 150, 147, 146, 137, 136, 135, 134, 133, 132, 131) then
      IPJ.VARIOS.SP_GUARDAR_LOG(
        p_METODO => 'SP_Guardar_Informe_Borrador',
        p_NIVEL => 'Error Tipo de Accion',
        p_ORIGEN => 'DB',
        p_USR_LOG  => 'DB',
        p_USR_WINDOWS => 'Sistema',
        p_IP_PC  => null,
        p_EXCEPCION =>
          'Tr�mite IPJ = ' ||  to_char(p_id_tramite_ipj)
          || ' / Id Tramite Accion = ' || to_char(p_id_tramiteipj_accion)
          || ' / Id Tipo Accion = ' || to_char(p_id_tipo_accion)
      );
    end if;

     if p_id_tipo_accion = 0 then
       select id_tipo_accion into v_id_tipo_accion
       from IPJ.t_tramitesipj_acciones
       where
         id_tramite_ipj = p_id_tramite_ipj and
         id_tramiteipj_accion = p_id_tramiteipj_accion;
    else
      v_id_tipo_accion := p_id_tipo_accion;
    end if;

    -- Actualizo la Observaci�n, para recuperar la info en las consultas.
    if p_obs_rubrica is not null then
      update IPJ.t_tramitesipj_acciones
      set
        obs_rubrica = p_obs_rubrica
      where
        id_tramite_ipj = p_id_tramite_ipj and
        id_tramiteipj_accion = p_id_tramiteipj_accion;
    end if;

    -- Actualizo el Informe
    update IPJ.T_INFORMES_BORRADOR
    set
      texto_informe = p_texto_informe,
      fecha_informe = sysdate
    where
      id_tramite_ipj = p_id_tramite_ipj and
      id_tramiteipj_accion = p_id_tramiteipj_accion and
      id_tipo_accion = v_id_tipo_accion;

     -- si no se actualizo a andie, lo agrego como borrador
     if sql%rowcount = 0 then
       insert into ipj.t_informes_borrador (id_tramite_ipj, id_tramiteipj_accion, id_tipo_accion, texto_informe, fecha_informe)
       values (p_id_tramite_ipj, p_id_tramiteipj_accion, v_id_tipo_accion, p_texto_informe, sysdate);
     end if;

     o_rdo := IPJ.TYPES.C_RESP_OK;
     o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     when OTHERS then
        o_rdo := To_Char(SQLCODE) || '-' || SQLERRM || ' // Par�metros: ' || 'Tr�mite IPJ = ' ||  to_char(p_id_tramite_ipj)
          || ' / Id Tramite Accion = ' || to_char(p_id_tramiteipj_accion) || ' / Id Tipo Accion = ' || to_char(p_id_tipo_accion);
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Guardar_Informe_Borrador;

  PROCEDURE SP_Traer_Tipos_Inst_Legal(
    p_Cursor OUT types.cursorType)
  IS
  /**************************************************
    Lista los Tipos de Actas disponibles
  ***************************************************/
  BEGIN
    OPEN p_Cursor FOR
      select T.IL_TIPO, T.N_IL_TIPO
      from ipj.t_Tipos_Ins_Legal t;
  END SP_Traer_Tipos_Inst_Legal;

  PROCEDURE SP_Traer_Tipos_Entidades(
    o_Cursor OUT types.cursorType,
    p_id_tramite_ipj in number,
    p_id_tramiteipj_accion in number)
  IS
  /**************************************************
    Lista los Tipos de Entidades Disponibles para un Area
  ***************************************************/
    v_area number;
  BEGIN
    begin
      select T.ID_UBICACION_ORIGEN into v_area
      from IPJ.T_TRAMITESIPJ t join IPJ.T_TRAMITESIPJ_ACCIONES tacc
        on t.id_tramite_ipj = tacc.id_tramite_ipj
      where
        t.id_tramite_ipj = p_id_tramite_ipj and
        tacc.id_tramiteipj_accion = p_id_tramiteipj_accion;
    exception
      when NO_DATA_FOUND then
        v_area:= 0;
    end;

    OPEN o_Cursor FOR
    select T.CONTRATO, T.ID_TIPO_ENTIDAD, T.ID_UBICACION, T.SIGLA,
      T.TIPO_ENTIDAD, U.N_UBICACION
    from ipj.t_tipos_entidades t left join ipj.t_ubicaciones u
      on t.id_ubicacion = u.id_ubicacion
    where
       v_area = 0 or t.id_ubicacion = v_area;

  END SP_Traer_Tipos_Entidades;

  PROCEDURE SP_Traer_Tipos_Entidades_Ubi(
    o_Cursor OUT types.cursorType,
    p_id_ubicacion in number)
  IS
  /**************************************************
    Lista los Tipos de Entidades Disponibles para un Area
  ***************************************************/
    v_area number;
  BEGIN
    OPEN o_Cursor FOR
      select T.CONTRATO, T.ID_TIPO_ENTIDAD, T.ID_UBICACION, T.SIGLA,
        T.TIPO_ENTIDAD, U.N_UBICACION
      from ipj.t_tipos_entidades t left join ipj.t_ubicaciones u
        on t.id_ubicacion = u.id_ubicacion
      where
        t.contrato <> 'R' and
         (nvl(p_id_ubicacion, 0) = 0  or t.id_ubicacion = p_id_ubicacion)
      order by T.TIPO_ENTIDAD asc;

  END SP_Traer_Tipos_Entidades_Ubi;

  PROCEDURE SP_Traer_Tipos_Administracion(
    o_Cursor OUT types.cursorType)
  IS
  /**************************************************
    Lista los Tipos de Admministracion de Entidades
  ***************************************************/
  BEGIN
    OPEN o_Cursor FOR
      select T.ID_TIPO_ADMINISTRACION, T.N_TIPO_ADMINISTRACION
      from ipj.t_tipos_administracion t;
  END SP_Traer_Tipos_Administracion;

  PROCEDURE SP_Traer_Tipo_Gravamenes(
    o_Cursor OUT types.cursorType)
  IS
  /**************************************************
    Lista los Tipos de Gravamentes definidos
  ***************************************************/
  BEGIN
    OPEN o_Cursor FOR
    select G.ID_TIPO_GRAVAMEN, G.N_TIPO_GRAVAMEN, G.DESCRIPCION
    from ipj.t_tipos_gravamenes g
    where
      g.id_tipo_gravamen not in (4, 6, 16, 21, 8, 14, 10, 2, 19); -- Quito los tipo FIN (se dejan de usar)

  END SP_Traer_Tipo_Gravamenes;


  PROCEDURE SP_Traer_Tipo_Relacion_Empresa(
    o_Cursor OUT types.cursorType)
  IS
  /**************************************************
    Lista los Tipos de Relaciones entre Empresas
  ***************************************************/
  BEGIN
    OPEN o_Cursor FOR
      select id_tipo_relacion_empresa, n_tipo_relacion_empresa
      from ipj.t_tipos_relacion_empresa;

  END SP_Traer_Tipo_Relacion_Empresa;


  PROCEDURE SP_Actualizar_Protocolo(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in number,
    p_id_legajo in number,
    p_id_tramiteipj_accion in number,
    p_matricula in varchar2)
  IS
    v_matricula_exists number;
    v_Numero_Matricula varchar2(20);
    v_letra_protocolo varchar2(10);
    v_row_protocolo IPJ.T_CONFIG_PROTOCOLO%ROWTYPE;
  BEGIN
  /***********************************************************
    Verifica que el numerador del protocolo sea mayor que la matricula guardada en un tramite,
    en caso contrario lo actualiza.
    La transaccion la maneja quien lo llama.
  ************************************************************/
    --Busco el protocolo para actualizar el numerador
    select  p.* into v_row_protocolo  --p.sigla || to_char(P.NUMERADOR) into o_rdo
    from IPJ.T_TRAMITESIPJ_ACCIONES t join IPJ.T_TIPOS_ACCIONESIPJ tt
        on T.ID_TIPO_ACCION = TT.ID_TIPO_ACCION
      join IPJ.T_CONFIG_PROTOCOLO p
        on TT.ID_PROTOCOLO = P.ID_PROTOCOLO
    where
      T.ID_TRAMITE_IPJ = p_id_tramite_ipj and
      t.id_tramiteipj_accion = p_id_tramiteipj_accion;

    -- Si a matricula guardada es mayor que el numerador del protoolo, lo actualizo
    begin
      v_Numero_Matricula := FC_Numero_Matricula(p_matricula);
      v_letra_protocolo := trim(replace(FC_Letra_Matricula(p_matricula), '#', ''));
      if v_letra_protocolo != v_row_protocolo.SIGLA then
        o_rdo := IPJ.VARIOS.MENSAJE_ERROR('MATRICULA_NOT') || ' "' || p_matricula || '", protocolo '|| v_row_protocolo.SIGLA;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
        return;
      end if;

      if to_Number(v_Numero_Matricula) >= v_row_protocolo.NUMERADOR then
        update IPJ.T_CONFIG_PROTOCOLO
        set NUMERADOR = to_Number(v_Numero_Matricula) +1
        where
          id_protocolo = v_row_protocolo.ID_Protocolo;
      end if;

      o_rdo := TYPES.C_RESP_OK;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    EXCEPTION
      when OTHERS then
        o_rdo := IPJ.VARIOS.MENSAJE_ERROR('MATRICULA_NOT')|| ' "' || p_matricula || '" (' ||To_Char(SQLCODE) || '-' || SQLERRM || ')';
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
    end;
  EXCEPTION
    when NO_DATA_FOUND then
      o_rdo := TYPES.C_RESP_OK;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    when OTHERS then
      o_rdo := IPJ.VARIOS.MENSAJE_ERROR('MATRICULA_NOT') || ' ' || p_matricula || ' (' ||To_Char(SQLCODE) || '-' || SQLERRM || ')';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Actualizar_Protocolo;

  PROCEDURE SP_Guardar_Matricula(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in number,
    p_id_legajo in number,
    p_id_tramiteipj_accion in number,
    p_cuit in varchar2,
    p_matricula in varchar2)
  IS
    v_id_fondo_comercio number;
    v_matricula_exists number;
    v_matricula_anterior varchar2(20);
    v_Numero_Matricula number;
    v_letra_protocolo varchar2(5);
    v_id_integrante number;
    v_row_protocolo IPJ.T_CONFIG_PROTOCOLO%ROWTYPE;
  BEGIN
  /***********************************************************
    Guarda solo la matricula de la entidad, controlando que no este duplicada.
  ************************************************************/
    /*  Verifico que tipo de matricula es:
        - E = Gravamenes
        - B = SRL
        - C = Comerciantes
        - D = Mandatos
        - F = Fondos de Comercio
        - A = SxA
        - G = Sociedad Extranjera
        - H = Fideicomisos
    */
    IPJ.VARIOS.SP_Validar_Matricula(
      o_rdo => o_rdo,
      o_tipo_mensaje => o_tipo_mensaje,
      p_id_tramite_ipj => p_id_tramite_ipj,
      p_id_legajo => p_id_legajo,
      p_id_tramiteipj_accion => p_id_tramiteipj_accion,
      p_cuit => p_cuit,
      p_matricula => upper(p_matricula));

    if o_rdo != IPJ.TYPES.C_RESP_OK then
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    /***************************************************
      Actualizo la matricula en el registro del tramite, para reservarla
      y actualizo el valor del protocolo
    ***************************************************/
    v_letra_protocolo := replace(IPJ.VARIOS.Fc_Letra_Matricula(p_matricula), '#','');

    if v_letra_protocolo = 'A' then --SxA
      update IPJ.T_ENTIDADES
      set MATRICULA = IPJ.VARIOS.Fc_Formatear_Matricula(p_matricula)
      where
        ID_TRAMITE_IPJ = p_id_tramite_ipj and
        id_legajo = p_id_legajo;
    end if;

    if v_letra_protocolo = 'B' then --SRL
      update IPJ.T_ENTIDADES
      set MATRICULA = IPJ.VARIOS.Fc_Formatear_Matricula(p_matricula)
      where
        ID_TRAMITE_IPJ = p_id_tramite_ipj and
        id_legajo = p_id_legajo;
    end if;

    if v_letra_protocolo = 'E' then -- GRAVAMENES
      update IPJ.T_GRAVAMENES
      set MATRICULA = IPJ.VARIOS.Fc_Formatear_Matricula(p_matricula)
      where
        ID_TRAMITE_IPJ = p_id_tramite_ipj and
        id_tramiteIpj_Accion = p_id_tramiteipj_accion;
    end if;

    if v_letra_protocolo = 'C' then -- COMERCIANTES
      update IPJ.T_COMERCIANTES
      set MATRICULA = IPJ.VARIOS.Fc_Formatear_Matricula(p_matricula)
      where
        ID_TRAMITE_IPJ = p_id_tramite_ipj and
        id_tramiteIpj_Accion = p_id_tramiteipj_accion;
    end if;

    if v_letra_protocolo = 'D' then --MANDATOS
      update IPJ.T_MANDATOS
      set MATRICULA = IPJ.VARIOS.Fc_Formatear_Matricula(p_matricula)
      where
        ID_TRAMITE_IPJ = p_id_tramite_ipj and
        id_tramiteIpj_Accion = p_id_tramiteipj_accion;
    end if;

    if v_letra_protocolo = 'F' then -- FONDOS DE COMERCIO
      select id_fondo_comercio into v_id_fondo_comercio
      from IPJ.T_TramitesIpj_Acciones
      where
        ID_TRAMITE_IPJ = p_id_tramite_ipj and
        id_tramiteIpj_Accion = p_id_tramiteipj_accion;

      update IPJ.T_FONDOS_COMERCIO
      set MATRICULA = IPJ.VARIOS.Fc_Formatear_Matricula(p_matricula)
      where
        id_fondo_comercio = v_id_fondo_comercio;
    end if;

    if v_letra_protocolo = 'G' then --SRL
      update IPJ.T_ENTIDADES
      set MATRICULA = IPJ.VARIOS.Fc_Formatear_Matricula(p_matricula)
      where
        ID_TRAMITE_IPJ = p_id_tramite_ipj and
        id_legajo = p_id_legajo;
    end if;

    if v_letra_protocolo = 'H' then --fideicomisos
      update IPJ.T_ENTIDADES
      set MATRICULA = IPJ.VARIOS.Fc_Formatear_Matricula(p_matricula)
      where
        ID_TRAMITE_IPJ = p_id_tramite_ipj and
        id_legajo = p_id_legajo;
    end if;

    IPJ.VARIOS.SP_Actualizar_Protocolo(
      o_rdo => o_rdo,
      o_tipo_mensaje => o_tipo_mensaje,
      p_id_tramite_ipj => p_id_tramite_ipj,
      p_id_legajo => p_id_legajo,
      p_id_tramiteipj_accion => p_id_tramiteipj_accion,
      p_matricula => upper(p_matricula) );

     if o_rdo = TYPES.C_RESP_OK then
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
       commit;
    else
       rollback;
    end if;
  EXCEPTION
      when OTHERS then
        o_rdo := 'SP_Guardar_Matricula - ' || To_Char(SQLCODE) || '-' || SQLERRM;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      rollback;
  END SP_Guardar_Matricula;

  PROCEDURE SP_Validar_Matricula(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in number,
    p_id_legajo in number,
    p_id_tramiteipj_accion in number,
    p_cuit in varchar2,
    p_matricula in varchar2)
  IS
    v_id_fondo_comercio number;
    v_matricula_exists number;
    v_matricula_anterior varchar2(20);
    v_Numero_Matricula number;
    v_letra_protocolo varchar2(5);
    v_letra_matricula varchar2(20);
    v_id_integrante number;
    v_row_protocolo IPJ.T_CONFIG_PROTOCOLO%ROWTYPE;
    v_id_ubicacion number(6);
    v_id_legajo number(15);
  BEGIN
  /***********************************************************
    Guarda solo la matricula de la entidad, controlando que no este duplicada.
  ************************************************************/
    /*  Verifico que tipo de matricula es:
        - E = Gravamenes
        - B = Entidades
        - C = Comerciantes
        - D = Mandatos
        - F = Fondos de Comercio
        - A = SxA
        - G = Sociedades Extranjeras
        - H = Fideicomisos
    */
    v_letra_matricula := IPJ.VARIOS.Fc_Letra_Matricula(p_matricula);
    if length(trim(Replace(v_letra_matricula, '#', ''))) <> 1 then
      o_rdo := 'Formato de Matr�cula inv�lido.';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    /***********************************************
      Controlo que la matricula no este utilizada por otro usuario,
      y que el mismo usuario no tenga otra matricula
    ***********************************************/
    v_letra_protocolo := trim(Replace(v_letra_matricula, '#', ''));

    -- Para protocolos de empresas, valido que venga bien definida
    if v_letra_protocolo IN ('B', 'D', 'A', 'G', 'H') then
      -- Busco el �rea actual del legajo, para soportar cambios de SA y SRL
      begin
        select id_ubicacion into v_id_ubicacion
        from ipj.t_legajos l join ipj.t_tipos_entidades te
          on l.id_tipo_entidad = te.id_tipo_entidad
        where
          l.id_legajo = p_id_legajo;
      exception
        when NO_DATA_FOUND then
          v_id_ubicacion := 0;
      end;

      if v_id_ubicacion = 0 then
        begin
          select id_ubicacion into v_id_ubicacion
          from ipj.t_entidades e join ipj.t_tipos_entidades te
            on e.id_tipo_entidad = te.id_tipo_entidad
          where
            e.id_legajo = p_id_legajo and
            e.id_tramite_ipj = p_id_tramite_ipj;
        exception
          when NO_DATA_FOUND then
            o_rdo := 'La entidad no posee tipo, no se puede validar su matr�cula.';
            o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
            return;
        end;
      end if;
    end if;

    -- PROTOCOLO A: SxA
    if v_letra_protocolo = 'A' then
      select count(*) into v_matricula_exists
      from IPJ.t_entidades e join ipj.t_tramitesipj_acciones acc
          on E.ID_TRAMITE_IPJ = ACC.ID_TRAMITE_IPJ and E.ID_LEGAJO = acc.id_legajo
      where
        e.id_legajo != p_id_legajo and
        FC_Formatear_Matricula(E.MATRICULA) = FC_Formatear_Matricula(p_matricula) and
        ACC.ID_ESTADO < IPJ.TYPES.C_ESTADOS_RECHAZADO ;

      select nvl(max(matricula), '0') into v_matricula_anterior
      from IPJ.t_entidades e join ipj.t_tramitesipj_acciones acc
          on E.ID_TRAMITE_IPJ = ACC.ID_TRAMITE_IPJ and E.ID_LEGAJO = acc.id_legajo
      where
        e.id_legajo = p_id_legajo and
        e.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_id_ubicacion) and
        FC_Formatear_Matricula(E.MATRICULA) != FC_Formatear_Matricula(p_matricula) and
        E.MATRICULA is not null and
        ACC.ID_ESTADO < IPJ.TYPES.C_ESTADOS_RECHAZADO;
    end if;

    -- PROTOCOLO B: SRL
    if v_letra_protocolo = 'B' then
      select count(*) into v_matricula_exists
      from IPJ.t_entidades e join ipj.t_tramitesipj_acciones acc
          on E.ID_TRAMITE_IPJ = ACC.ID_TRAMITE_IPJ and E.ID_LEGAJO = acc.id_legajo
      where
        e.id_legajo != p_id_legajo and
        FC_Formatear_Matricula(E.MATRICULA) = FC_Formatear_Matricula(p_matricula) and
        ACC.ID_ESTADO < IPJ.TYPES.C_ESTADOS_RECHAZADO ;

      select nvl(max(matricula), '0') into v_matricula_anterior
      from IPJ.t_entidades e join ipj.t_tramitesipj_acciones acc
          on E.ID_TRAMITE_IPJ = ACC.ID_TRAMITE_IPJ and E.ID_LEGAJO = acc.id_legajo
      where
        e.id_legajo = p_id_legajo and
        e.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_id_ubicacion) and
        FC_Formatear_Matricula(E.MATRICULA) != FC_Formatear_Matricula(p_matricula) and
        E.MATRICULA is not null and
        ACC.ID_ESTADO < IPJ.TYPES.C_ESTADOS_RECHAZADO;
    end if;

   -- PROTOCOLO E: GRAVAMENES
    if v_letra_protocolo = 'E' then
      -- Busco la acci�n, a ver si es de persona o empresa
      select id_integrante, id_legajo into v_id_integrante, v_id_legajo
      from IPJ.T_TramitesIPJ_Acciones a
      where
        id_tramite_ipj = p_id_tramite_ipj and
        id_tramiteipj_accion = p_id_tramiteipj_accion;

      -- Verifico si esa matr�cula ya esta utilizada por alg�n otro gravamen.
      select count(*) into v_matricula_exists
      from IPJ.t_Gravamenes g join ipj.t_tramitesipj tr
          on g.ID_TRAMITE_IPJ = tr.ID_TRAMITE_IPJ
      where
        ( nvl(v_id_integrante, 0) > 0 and (nvl(g.id_integrante, 0) <> v_id_integrante or nvl(g.id_legajo, 0) > 0) or
          nvl(v_id_legajo, 0) > 0 and (nvl(g.id_legajo, 0) <> v_id_legajo or nvl(g.id_integrante, 0) > 0)
        ) and
        IPJ.VARIOS.FC_Formatear_Matricula(g.MATRICULA) = IPJ.VARIOS.FC_Formatear_Matricula(p_matricula) and
        tr.ID_ESTADO_ult < IPJ.TYPES.C_ESTADOS_RECHAZADO;

      --Verifico que la persona o empresa no tenga otra matr�cula distinta
      select nvl(max(matricula), '0') into v_matricula_anterior
      from IPJ.t_Gravamenes g join ipj.t_tramitesipj tr
          on g.ID_TRAMITE_IPJ = tr.ID_TRAMITE_IPJ
      where
        ( (nvl(v_id_integrante, 0) > 0 and nvl(g.id_integrante, 0) = v_id_integrante) or
          (nvl(v_id_legajo, 0) > 0 and nvl(g.id_legajo, 0) = v_id_legajo )
        ) and
        FC_Formatear_Matricula(g.MATRICULA) != FC_Formatear_Matricula(p_matricula) and
        g.MATRICULA is not null and
        tr.id_estado_ult < IPJ.TYPES.C_ESTADOS_RECHAZADO;
    end if;

   -- PROTOCOLO C: COMERCIANTES
    if v_letra_protocolo = 'C' then
      select id_integrante into v_id_integrante
      from IPJ.T_TramitesIpj_Acciones a
      where
        id_tramite_ipj = p_id_tramite_ipj and
        id_tramiteipj_accion = p_id_tramiteipj_accion;

      select count(*) into v_matricula_exists
      from IPJ.t_comerciantes c join ipj.t_tramitesipj_acciones acc
          on c.ID_TRAMITE_IPJ = ACC.ID_TRAMITE_IPJ and c.ID_INTEGRANTE = acc.id_INTEGRANTE
      where
        c.id_integrante != v_id_integrante and
        FC_Formatear_Matricula(c.MATRICULA) = FC_Formatear_Matricula(p_matricula) and
        ACC.ID_ESTADO < IPJ.TYPES.C_ESTADOS_RECHAZADO;

      select nvl(max(matricula), '0') into v_matricula_anterior
      from IPJ.t_comerciantes c join ipj.t_tramitesipj_acciones acc
          on c.ID_TRAMITE_IPJ = ACC.ID_TRAMITE_IPJ and c.ID_INTEGRANTE = acc.id_INTEGRANTE
      where
        c.id_integrante = v_id_integrante and
        FC_Formatear_Matricula(c.MATRICULA) != FC_Formatear_Matricula(p_matricula) and
        c.MATRICULA is not null and
        ACC.ID_ESTADO < IPJ.TYPES.C_ESTADOS_RECHAZADO;
    end if;

    -- PROTOCOLO D: MANDATOS
    if v_letra_protocolo = 'D' then -- MANDATOS
      if p_cuit is not null then
        select count(*) into v_matricula_exists
        from IPJ.t_mandatos m join ipj.t_tramitesipj_acciones acc
            on m.ID_TRAMITE_IPJ = ACC.ID_TRAMITE_IPJ and m.id_legajo = ACC.ID_LEGAJO
        where
          m.id_legajo != p_id_legajo and
          FC_Formatear_Matricula(m.MATRICULA) = FC_Formatear_Matricula(p_matricula) and
          ACC.ID_ESTADO < IPJ.TYPES.C_ESTADOS_RECHAZADO;

        select nvl(max(matricula), '0') into v_matricula_anterior
        from IPJ.t_mandatos m join ipj.t_tramitesipj_acciones acc
            on m.ID_TRAMITE_IPJ = ACC.ID_TRAMITE_IPJ and m.id_legajo = ACC.ID_LEGAJO
        where
          m.id_legajo = p_id_legajo and
          FC_Formatear_Matricula(m.MATRICULA) != FC_Formatear_Matricula(p_matricula) and
          m.MATRICULA is not null and
          ACC.ID_ESTADO < IPJ.TYPES.C_ESTADOS_RECHAZADO;
      else
        select id_integrante into v_id_integrante
        from IPJ.T_TramitesIPJ_Acciones a
        where
          id_tramite_ipj = p_id_tramite_ipj and
          id_tramiteipj_accion = p_id_tramiteipj_accion;

        select count(*) into v_matricula_exists
        from IPJ.t_mandatos m join ipj.t_tramitesipj_acciones acc
            on m.ID_TRAMITE_IPJ = ACC.ID_TRAMITE_IPJ and m.id_integrante = ACC.ID_INTEGRANTE
        where
          m.id_integrante != v_id_integrante and
          FC_Formatear_Matricula(m.MATRICULA) = FC_Formatear_Matricula(p_matricula) and
          ACC.ID_ESTADO < IPJ.TYPES.C_ESTADOS_RECHAZADO;

        select nvl(max(matricula), '0') into v_matricula_anterior
        from IPJ.t_mandatos m join ipj.t_tramitesipj_acciones acc
            on m.ID_TRAMITE_IPJ = ACC.ID_TRAMITE_IPJ and m.id_integrante = ACC.ID_INTEGRANTE
        where
          m.id_integrante = v_id_integrante and
          FC_Formatear_Matricula(m.MATRICULA) != FC_Formatear_Matricula(p_matricula) and
          m.MATRICULA is not null and
          ACC.ID_ESTADO < IPJ.TYPES.C_ESTADOS_RECHAZADO;
      end if;
    end if;

   -- PROTOCOLO F: FONDO DE COMERCIOS
    if v_letra_protocolo = 'F' then -- FONDO DE COMERCIO
      select id_fondo_comercio into v_id_fondo_comercio
      from IPJ.T_TramitesIpj_Acciones
      where
        id_tramite_ipj = p_id_tramite_ipj and
        id_tramiteipj_accion = p_id_tramiteipj_accion;

      select count(*) into v_matricula_exists
      from IPJ.t_fondos_comercio f join ipj.t_tramitesipj_acciones acc
          on f.id_fondo_comercio = ACC.id_fondo_comercio
      where
        f.id_fondo_comercio != v_id_fondo_comercio and
        FC_Formatear_Matricula(f.MATRICULA) = FC_Formatear_Matricula(p_matricula) and
        ACC.ID_ESTADO < IPJ.TYPES.C_ESTADOS_RECHAZADO;

      select nvl(max(matricula), '0') into v_matricula_anterior
      from IPJ.t_fondos_comercio f join ipj.t_tramitesipj_acciones acc
          on f.id_fondo_comercio = ACC.id_fondo_comercio
      where
        f.id_fondo_comercio = v_id_fondo_comercio and
        FC_Formatear_Matricula(f.MATRICULA) != FC_Formatear_Matricula(p_matricula) and
        f.MATRICULA is not null and
        ACC.ID_ESTADO < IPJ.TYPES.C_ESTADOS_RECHAZADO;
    end if;

    -- PROTOCOLO G: SOCIEDAD EXTRANGERA
    if v_letra_protocolo = 'G' then -- Sociedad Extranjera
      select count(*) into v_matricula_exists
      from IPJ.t_entidades e join ipj.t_tramitesipj_acciones acc
          on E.ID_TRAMITE_IPJ = ACC.ID_TRAMITE_IPJ and E.ID_LEGAJO = acc.id_legajo
      where
        e.id_legajo != p_id_legajo and
        FC_Formatear_Matricula(E.MATRICULA) = FC_Formatear_Matricula(p_matricula) and
        ACC.ID_ESTADO < IPJ.TYPES.C_ESTADOS_RECHAZADO ;

      select nvl(max(matricula), '0') into v_matricula_anterior
      from IPJ.t_entidades e join ipj.t_tramitesipj_acciones acc
          on E.ID_TRAMITE_IPJ = ACC.ID_TRAMITE_IPJ and E.ID_LEGAJO = acc.id_legajo
      where
        e.id_legajo = p_id_legajo and
        e.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_id_ubicacion) and
        FC_Formatear_Matricula(E.MATRICULA) != FC_Formatear_Matricula(p_matricula) and
        E.MATRICULA is not null and
        ACC.ID_ESTADO < IPJ.TYPES.C_ESTADOS_RECHAZADO;
    end if;

    -- PROTOCOLO H: FIDEICOMISOS
    if v_letra_protocolo = 'H' then -- Fideicomisos
      select count(*) into v_matricula_exists
      from IPJ.t_entidades e join ipj.t_tramitesipj_acciones acc
          on E.ID_TRAMITE_IPJ = ACC.ID_TRAMITE_IPJ and E.ID_LEGAJO = acc.id_legajo
      where
        e.id_legajo != p_id_legajo and
        FC_Formatear_Matricula(E.MATRICULA) = FC_Formatear_Matricula(p_matricula) and
        ACC.ID_ESTADO < IPJ.TYPES.C_ESTADOS_RECHAZADO ;

      select nvl(max(matricula), '0') into v_matricula_anterior
      from IPJ.t_entidades e join ipj.t_tramitesipj_acciones acc
          on E.ID_TRAMITE_IPJ = ACC.ID_TRAMITE_IPJ and E.ID_LEGAJO = acc.id_legajo
      where
        e.id_legajo = p_id_legajo and
        e.id_tipo_entidad in (select id_tipo_entidad from ipj.t_tipos_entidades where id_ubicacion = v_id_ubicacion) and
        FC_Formatear_Matricula(E.MATRICULA) != FC_Formatear_Matricula(p_matricula) and
        E.MATRICULA is not null and
        ACC.ID_ESTADO < IPJ.TYPES.C_ESTADOS_RECHAZADO;
    end if;

    if v_letra_protocolo is null then
      o_rdo := IPJ.VARIOS.MENSAJE_ERROR('PROT_NOT');
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    if v_matricula_exists > 0 then
      o_rdo := IPJ.VARIOS.MENSAJE_ERROR('MATRICULA_NOT');
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    if v_matricula_anterior <> '0' then
      o_rdo := IPJ.VARIOS.MENSAJE_ERROR('MATRICULA_NOT') || ' (Matr�cula anterior ' || v_matricula_anterior || ')';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    o_rdo := TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
    when OTHERS then
      o_rdo := 'SP_Validar_Matricula - ' || To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Validar_Matricula;


  PROCEDURE SP_Sugerir_Matricula(
    o_matricula out varchar2,
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in number,
    p_id_legajo in number,
    p_id_tramiteipj_accion in number)
  IS
  BEGIN
  /***********************************************************
  Gerena el proximo numero de matricula segun el protocolo asociado al tipo de tramite
  ************************************************************/
    begin
      select  upper(p.sigla) || to_char(P.NUMERADOR) into o_matricula
      from IPJ.T_TRAMITESIPJ_ACCIONES t join IPJ.T_TIPOS_ACCIONESIPJ tt
          on T.ID_TIPO_ACCION = TT.ID_TIPO_ACCION
        join IPJ.T_CONFIG_PROTOCOLO p
          on TT.ID_PROTOCOLO = P.ID_PROTOCOLO
      where
        t.id_tramite_ipj = p_id_tramite_ipj and
        T.ID_TRAMITEIPJ_accion = p_id_tramiteipj_accion;

        o_rdo := IPJ.TYPES.C_RESP_OK;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    exception
       when OTHERS then
         o_rdo := IPJ.VARIOS.MENSAJE_ERROR('PROT_NOT');
         o_matricula :=  IPJ.VARIOS.MENSAJE_ERROR('PROT_NOT');
         o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
    end;
  END SP_Sugerir_Matricula;

  PROCEDURE SP_Sugerir_Matricula(
    o_matricula out varchar2,
    o_letra out varchar2,
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in number,
    p_id_legajo in number,
    p_id_tramiteipj_accion in number)
  IS
  BEGIN
  /***********************************************************
  Gerena el proximo numero de matricula segun el protocolo asociado al tipo de tramite
  ************************************************************/
    begin
      select  upper(p.sigla), to_char(P.NUMERADOR) into o_letra, o_matricula
      from IPJ.T_TRAMITESIPJ_ACCIONES t join IPJ.T_TIPOS_ACCIONESIPJ tt
          on T.ID_TIPO_ACCION = TT.ID_TIPO_ACCION
        join IPJ.T_CONFIG_PROTOCOLO p
          on TT.ID_PROTOCOLO = P.ID_PROTOCOLO
      where
        t.id_tramite_ipj = p_id_tramite_ipj and
        T.ID_TRAMITEIPJ_accion = p_id_tramiteipj_accion;

        o_rdo := IPJ.TYPES.C_RESP_OK;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    exception
       when OTHERS then
         o_rdo := IPJ.VARIOS.MENSAJE_ERROR('PROT_NOT');
         o_matricula :=  IPJ.VARIOS.MENSAJE_ERROR('PROT_NOT');
         o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
    end;

  END SP_Sugerir_Matricula;

  FUNCTION FC_Habilitar_Botones (
    p_id_ubicacion number, --4
    p_id_clasif_ipj number, --6
    p_id_tipo_accion number, --0
    p_estado_tramite number,--2
    p_acciones_abiertas number,--0
    p_estado_accion number, --0
    p_area_trabajo number, --2
    p_id_proceso number,
    p_simple_tramite varchar2,
    p_cuil_responsable varchar2,
    p_cuil_logueado varchar2,
    p_id_tramite_ipj number,
    p_id_tramiteipj_accion number) return Number --0
  IS
    v_boton_abrir number := 1; --                00000000000001
    v_boton_reasignar number := 2;--          00000000000010
    v_boton_rechazar number := 4; --          00000000000100
    v_boton_cerrar number := 8;--              00000000001000
    v_boton_observar number := 16; --        00000000010000
    v_boton_archivo number := 32; --          00000000100000
    v_boton_mesa number := 64; --            00000001000000
    v_boton_RPC number := 128; --            00000010000000
    v_boton_Firmar number := 256; --        00000100000000
    v_boton_ACyF number := 512;--           00001000000000
    v_boton_JUR number := 1024;--           00010000000000
    v_boton_SxA number := 2048;--           00100000000000
    v_boton_Inactivo number := 4096;--     01000000000000
    v_boton_Perimido number := 8192;--   10000000000000
    v_botones_habilitados number := 0;
    v_es_nota char;
    v_tiene_informe number;
    v_hab_SxA number;
    v_hab_RPC number;
    v_hab_ACyF number;
    v_hab_JUR number;
    v_hab_ARCH number;
    v_ubicacion_origen number;
    v_es_admin number;
    v_acc_firma_dig ipj.t_config.valor_variable%TYPE;
    v_res VARCHAR2(100):=0;
    v_Sql VARCHAR2(1000);
  BEGIN
  /***********************************************************
  En base a los datos de un tramite o accion, indica cuales botones se deben habilitar.
  ************************************************************/
    --Verifico si el tr�mite de SUAC es nota o expediente
    select (select decode(vtSuac.Nota_expediente, 'NOTA', 'S', 'N') from NUEVOSUAC.VT_TRAMITES_IPJ_DESA vtSuac where tr.id_tramite = vtSuac.id_tramite) Es_Nota,
      tr.id_ubicacion_origen into v_es_nota, v_ubicacion_origen
    from ipj.t_tramitesipj tr
    where
      tr.id_tramite_ipj = p_id_tramite_ipj;

    -- Habilita Jur�dico
    select count(1) into v_hab_JUR
    from ipj.t_workflow
    where
      id_ubicacion_origen = v_ubicacion_origen and
      id_clasif_ipj_actual = p_id_clasif_ipj and
      id_clasif_ipj_proxima in (select id_clasif_ipj from ipj.t_tipos_clasif_ipj where id_ubicacion = IPJ.TYPES.C_AREA_JURIDICO);

    -- Habilita SxA
    select count(1) into v_hab_SxA
    from ipj.t_workflow
    where
      id_ubicacion_origen = v_ubicacion_origen and
      id_clasif_ipj_actual = p_id_clasif_ipj and
      id_clasif_ipj_proxima in (select id_clasif_ipj from ipj.t_tipos_clasif_ipj where id_ubicacion = IPJ.TYPES.C_AREA_SXA);

    -- Habilita RPC
    select count(1) into v_hab_RPC
    from ipj.t_workflow
    where
      id_ubicacion_origen = v_ubicacion_origen and
      id_clasif_ipj_actual = p_id_clasif_ipj and
      id_clasif_ipj_proxima in (select id_clasif_ipj from ipj.t_tipos_clasif_ipj where id_ubicacion = IPJ.TYPES.C_AREA_SRL);

    -- Habilita ACyF
    select count(1) into v_hab_ACyF
    from ipj.t_workflow
    where
      id_ubicacion_origen = v_ubicacion_origen and
      id_clasif_ipj_actual = p_id_clasif_ipj and
      id_clasif_ipj_proxima in (select id_clasif_ipj from ipj.t_tipos_clasif_ipj where id_ubicacion = IPJ.TYPES.C_AREA_CYF );

    -- Habilita ARCH
    select count(1) into v_hab_ARCH
    from ipj.t_workflow
    where
      id_ubicacion_origen = v_ubicacion_origen and
      id_clasif_ipj_actual = p_id_clasif_ipj and
      id_clasif_ipj_proxima in (select id_clasif_ipj from ipj.t_tipos_clasif_ipj where id_ubicacion = IPJ.TYPES.C_AREA_ARCHIVO );

    -- Si es una accion, busco si tiene generado alg�n informe para habilitar la firma
    if p_id_tramiteipj_accion <> 0 then
      /*select count(1) into v_tiene_informe
      from IPJ.T_INFORMES_BORRADOR
      where
        id_tramite_ipj = p_id_tramite_ipj and
        id_tramiteipj_accion = p_id_tramiteipj_accion and
        id_tipo_accion = p_id_tipo_accion;*/

      -- Busco las acciones que llevan firma digital
      v_acc_firma_dig := ipj.varios.FC_Buscar_Variable_Config('FIRMA_DIG_ACCIONES');

      v_sql:='select count(1) '||
             ' from IPJ.T_INFORMES_BORRADOR '||
             'WHERE ' ||
             ' id_tramite_ipj = ''' || p_id_tramite_ipj || '''' ||
             ' AND id_tramiteipj_accion = ''' || p_id_tramiteipj_accion || '''' ||
             ' AND id_tipo_accion IN ('|| v_acc_firma_dig ||')';

      EXECUTE IMMEDIATE v_sql INTO v_tiene_informe;

    else
      v_tiene_informe := 0;
    end if;

    -- Veo si es un administrador
    select count(1) into v_Es_Admin
    from IPJ.T_GRUPOS_TRAB_UBICACION
    where
      cuil = p_cuil_responsable and
      id_ubicacion =  p_id_ubicacion and
      id_grupo_trabajo = 1; -- Administrador

    -- ************* SRL ******************
    if p_id_ubicacion = ipj.types.c_area_SRL then
      if p_estado_accion > 0 then
        -- Permisos para Acciones
        case p_estado_accion
          when 1 /* Est. Pendiente*/ then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_rechazar + v_boton_cerrar + (case when v_tiene_informe > 0 then v_boton_Firmar else 0 end);
          when 2 /* Est. Asignado*/ then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_rechazar + v_boton_cerrar + (case when v_tiene_informe > 0 then v_boton_Firmar else 0 end);
          when 3 /* Est. En Proceso*/ then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_rechazar + v_boton_cerrar + (case when v_tiene_informe > 0 then v_boton_Firmar else 0 end);
          when 4 /* Est. En Revision*/ then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_rechazar + v_boton_cerrar + (case when v_tiene_informe > 0 then v_boton_Firmar else 0 end);
          when 5 /* Est. Observado*/ then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_rechazar + v_boton_cerrar;
          when 10 /* Est. Firma Pend*/ then v_botones_habilitados:= v_botones_habilitados;
          when 15 /* Est. Pend BOE*/ then v_botones_habilitados:= v_botones_habilitados;
          when 18 /* Est. Pend BOE IPJ*/ then v_botones_habilitados:= v_boton_abrir;
          when 16 /* Est. Publicado BOE*/ then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_cerrar;
          when 17 /* Est. Cancelado BOE*/ then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_rechazar;
          when 50 /* Est. Anexado*/ then v_botones_habilitados:= v_boton_abrir;
          else v_botones_habilitados:= v_botones_habilitados + 0;
        end case;
      else
        -- Permisos para Tr�mites
        if p_estado_tramite = 50 then --Anexado
          v_botones_habilitados:= 0;
        else
          case
            -- no completo, sin abiertas, hist�rico, (Admin o Responsable) -> Abrir, Reasignar, Rechazar, Cerrar
            when p_estado_tramite < IPJ.TYPES.c_Estados_Completado and p_acciones_abiertas = 0 and nvl(p_id_proceso, 0) = Ipj.Types.C_Proceso_Hist and (p_area_trabajo = IPJ.TYPES.C_Rol_Administrador or  p_cuil_responsable = p_cuil_logueado)
                then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_reasignar+ v_boton_rechazar + v_boton_cerrar;
            -- no completo, sin abiertas, NO hist�rico, (Admin o Responsable) -> Abrir, Reasignar, Rechazar, Cerrar, Mesa SUAC, ??Juridico, ??SxA
            when p_estado_tramite < IPJ.TYPES.c_Estados_Completado and p_acciones_abiertas = 0 and nvl(p_id_proceso, 0) <> Ipj.Types.C_Proceso_Hist and (p_area_trabajo = IPJ.TYPES.C_Rol_Administrador or  p_cuil_responsable = p_cuil_logueado)
                then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_reasignar+ v_boton_rechazar + v_boton_cerrar + v_boton_mesa + (case when v_hab_JUR > 0 then v_boton_JUR else 0 end) + (case when v_hab_SxA > 0 then v_boton_SxA else 0 end) + (case when v_hab_ARCH > 0 then v_boton_archivo else 0 end);
            -- no completo, CON abiertas, (Admin o Responsable) -> Abrir, Reasignar, ??Juridico, ??SxA
            when p_estado_tramite < IPJ.TYPES.c_Estados_Completado and p_acciones_abiertas > 0 and (p_area_trabajo = IPJ.TYPES.C_Rol_Administrador or  p_cuil_responsable = p_cuil_logueado)
                then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_reasignar + (case when v_hab_JUR > 0 then v_boton_JUR else 0 end) + (case when v_hab_SxA > 0 then v_boton_SxA else 0 end);
            else v_botones_habilitados:= v_botones_habilitados + 0;
          end case;
        end if;
      end if;
    end if;

    -- ************* SOCIEDADES por ACCION ******************
    if p_id_ubicacion = ipj.types.c_area_SxA then
      if p_estado_accion > 0 then
        -- Permisos para Acciones
        case p_estado_accion
          when 1 /* Est. Pendiente*/ then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_rechazar + v_boton_cerrar + (case when v_tiene_informe > 0 then v_boton_Firmar else 0 end);
          when 2 /* Est. Asignado*/ then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_rechazar + v_boton_cerrar + (case when v_tiene_informe > 0 then v_boton_Firmar else 0 end);
          when 3 /* Est. En Proceso*/ then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_rechazar + v_boton_cerrar + (case when v_tiene_informe > 0 then v_boton_Firmar else 0 end);
          when 4 /* Est. En Revision*/ then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_rechazar + v_boton_cerrar + (case when v_tiene_informe > 0 then v_boton_Firmar else 0 end);
          when 5 /* Est. Observado*/ then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_rechazar + v_boton_cerrar;
          when 10 /* Est. Firma Pend*/ then v_botones_habilitados:= v_botones_habilitados;
          when 15 /* Est. Pend BOE*/ then v_botones_habilitados:= v_botones_habilitados;
          when 18 /* Est. Pend BOE IPJ*/ then v_botones_habilitados:= v_boton_abrir;
          when 16 /* Est. Publicado BOE*/ then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_cerrar;
          when 17 /* Est. Cancelado BOE*/ then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_rechazar;
          when 50 /* Est. Observado*/ then v_botones_habilitados:= v_boton_abrir;
          else v_botones_habilitados:= v_botones_habilitados + 0;
        end case;
      else
        -- Permisos para Tr�mites
        if p_estado_tramite = 50 then --Anexado
          v_botones_habilitados:= 0;
        else
          case
            -- No Completo, Sin Abiertas, Historico, (Admin o Responsable) -> Abrir, Reasignar, Rechazar, Cerrar
            when p_estado_tramite < IPJ.TYPES.c_Estados_Completado and p_acciones_abiertas = 0 and nvl(p_id_proceso, 0) = Ipj.Types.C_Proceso_Hist and (p_area_trabajo = IPJ.TYPES.C_Rol_Administrador or  p_cuil_responsable = p_cuil_logueado)
                then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_reasignar+ v_boton_rechazar + v_boton_cerrar;
            -- No Completo, Sin Abiertas, NO Historico, (Admin o Responsable) -> Abrir, Reasignar, Rechazar, Archivo, Mesa SUAC + ??RPC + ??Juridico
            when p_estado_tramite < IPJ.TYPES.c_Estados_Completado and p_acciones_abiertas = 0 and nvl(p_id_proceso, 0) <> Ipj.Types.C_Proceso_Hist and (p_area_trabajo = IPJ.TYPES.C_Rol_Administrador or  p_cuil_responsable = p_cuil_logueado)
                then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_reasignar+ v_boton_rechazar + v_boton_archivo + v_boton_mesa + (case when v_hab_RPC > 0 then v_boton_RPC else 0 end) + (case when v_hab_JUR > 0 then v_boton_JUR else 0 end);
            -- No Completo, Con Abiertas,  (Admin o Responsable) -> Abrir, Reasignar + ??RPC + ??Juridico
            when p_estado_tramite < IPJ.TYPES.c_Estados_Completado and p_acciones_abiertas > 0 and (p_area_trabajo = IPJ.TYPES.C_Rol_Administrador or  p_cuil_responsable = p_cuil_logueado)
                then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_reasignar + (case when v_hab_RPC > 0 then v_boton_RPC else 0 end) + (case when v_hab_JUR > 0 then v_boton_JUR else 0 end);
            else v_botones_habilitados:= v_botones_habilitados + 0;
          end case;

          -- Habilito el bot�n de Inactivos, solo para Administradores y tramites reales y pendiente
          if v_Es_Admin > 0 and nvl(p_id_proceso, 0) <> Ipj.Types.C_Proceso_Hist and p_estado_tramite < IPJ.TYPES.c_Estados_Completado then
            v_botones_habilitados:= v_botones_habilitados; -- + v_boton_Inactivo
          end if;

        end if;

      end if;
    end if;

     -- ************* CIVILES y FUNDACIONES ******************
    if p_id_ubicacion = ipj.types.c_area_CyF then

      if p_estado_accion > 0 then --
        -- Permisos para Acciones
        case p_estado_accion
          when 1 /* Est. Pendiente*/ then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_rechazar + v_boton_cerrar + (case when v_tiene_informe > 0 then v_boton_Firmar else 0 end);
          when 2 /* Est. Asignado*/ then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_rechazar + v_boton_cerrar + (case when v_tiene_informe > 0 then v_boton_Firmar else 0 end);
          when 3 /* Est. En Proceso*/ then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_rechazar + v_boton_cerrar + (case when v_tiene_informe > 0 then v_boton_Firmar else 0 end);
          when 4 /* Est. En Revision*/ then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_rechazar + v_boton_cerrar + (case when v_tiene_informe > 0 then v_boton_Firmar else 0 end);
          when 5 /* Est. Observado*/ then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_rechazar + v_boton_cerrar;
          when 10 /* Est. Firma Pend*/ then v_botones_habilitados:= v_botones_habilitados;
          when 15 /* Est. Pend BOE*/ then v_botones_habilitados:= v_botones_habilitados;
          when 18 /* Est. Pend BOE IPJ*/ then v_botones_habilitados:= v_boton_abrir;
          when 16 /* Est. Publicado BOE*/ then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_cerrar;
          when 17 /* Est. Cancelado BOE*/ then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_rechazar;
          when 50 /* Est. Observado*/ then v_botones_habilitados:= v_boton_abrir;
          else v_botones_habilitados:= v_botones_habilitados + 0;
        end case;

      else
        -- Permisos del Tramite
        if p_estado_tramite = 50 then --Anexado
          v_botones_habilitados:= 0;
        else
          case
            --No Completo, Sin Abiertas, Hist�rico, (Admin o Responsable) -> Abrir, Reasignar, Rechazar, Cerrar
            when p_estado_tramite < IPJ.TYPES.c_Estados_Completado and p_acciones_abiertas = 0 and nvl(p_id_proceso, 0) = Ipj.Types.C_Proceso_Hist and (p_area_trabajo = IPJ.TYPES.C_Rol_Administrador or  p_cuil_responsable = p_cuil_logueado)
                then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_reasignar+ v_boton_rechazar + v_boton_cerrar;
            --No Completo, Sin Abiertas, NO Hist�rico, (Admin o Responsable) -> Abrir, Reasignar, Rechazar, Archivo, Mesa SUAC + ??Juridico
            when p_estado_tramite < IPJ.TYPES.c_Estados_Completado and p_acciones_abiertas = 0 and nvl(p_id_proceso, 0) <> Ipj.Types.C_Proceso_Hist and (p_area_trabajo = IPJ.TYPES.C_Rol_Administrador or  p_cuil_responsable = p_cuil_logueado)
                then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_reasignar+ v_boton_rechazar + v_boton_archivo + v_boton_mesa + (case when v_hab_JUR > 0 then v_boton_JUR else 0 end);
            --No Completo, CON Abiertas, (Admin o Responsable) -> Abrir, Reasignar + ??Juridico
            when p_estado_tramite < IPJ.TYPES.c_Estados_Completado and p_acciones_abiertas > 0 and (p_area_trabajo = IPJ.TYPES.C_Rol_Administrador or  p_cuil_responsable = p_cuil_logueado)
                then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_reasignar + (case when v_hab_JUR > 0 then v_boton_JUR else 0 end);
            else v_botones_habilitados:= v_botones_habilitados + 0;
          end case;

          --No Completo, SIN Abiertas, (Admin o Responsable), Es Nota -> Cerrar
          If p_estado_tramite < IPJ.TYPES.c_Estados_Completado and p_acciones_abiertas = 0 and (p_area_trabajo = IPJ.TYPES.C_Rol_Administrador or  p_cuil_responsable = p_cuil_logueado) and v_es_nota = 'S' then
            v_botones_habilitados:= v_botones_habilitados + v_boton_cerrar;
          end if;
        end if;

      end if;
    end if;


    -- ************* JURIDICO y DESPACHO ******************
    if p_id_ubicacion = IPJ.TYPES.C_AREA_JURIDICO then
       --Segun el area de trabajo
      if p_area_trabajo = 1 then -- AREA ADMINISTRACION
        case
          when p_estado_tramite < IPJ.TYPES.c_Estados_Completado and p_acciones_abiertas = 0
            then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_reasignar + v_boton_mesa + (case when v_hab_ARCH > 0 then v_boton_archivo else 0 end);
          when p_estado_tramite < IPJ.TYPES.c_Estados_Completado and p_acciones_abiertas > 0
            then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_reasignar;
          else v_botones_habilitados:= v_botones_habilitados + 0;
        end case;

      else --AREA TECNICA
        case p_estado_accion
          when 1 /* Est. Pendiente*/ then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_rechazar + v_boton_cerrar + (case when v_tiene_informe > 0 then v_boton_Firmar else 0 end);
          when 2 /* Est. Asignado*/ then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_rechazar + v_boton_cerrar + (case when v_tiene_informe > 0 then v_boton_Firmar else 0 end);
          when 3 /* Est. En Proceso*/ then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_rechazar + v_boton_cerrar + (case when v_tiene_informe > 0 then v_boton_Firmar else 0 end);
          when 4 /* Est. En Revision*/ then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_rechazar + v_boton_cerrar + (case when v_tiene_informe > 0 then v_boton_Firmar else 0 end);
          when 10 /* Est. Firma Pend*/ then v_botones_habilitados:= v_botones_habilitados;
          when 15 /* Est. Pend BOE*/ then v_botones_habilitados:= v_botones_habilitados;
          when 18 /* Est. Pend BOE IPJ*/ then v_botones_habilitados:= v_boton_abrir;
          when 16 /* Est. Publicado BOE*/ then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_cerrar;
          when 17 /* Est. Cancelado BOE*/ then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_rechazar;
          else v_botones_habilitados:= v_botones_habilitados + 0;
        end case;

        case
          -- No Completo, Sin Abiertas, Historico -> Abrir + Resignar
          when p_estado_tramite < IPJ.TYPES.c_Estados_Completado and p_acciones_abiertas = 0  and nvl(p_id_proceso, 0) = Ipj.Types.C_Proceso_Hist
            then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_reasignar;
          -- No Completo, Sin Abiertas, NO Historico -> Abrir + Resignar + Mesa SUAC + ??RPC + ??SxA + ??ACyF
          when p_estado_tramite < IPJ.TYPES.c_Estados_Completado and p_acciones_abiertas = 0  and nvl(p_id_proceso, 0) <> Ipj.Types.C_Proceso_Hist
            then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_reasignar + v_boton_mesa + (case when v_hab_RPC > 0 then v_boton_RPC else 0 end) + (case when v_hab_SxA > 0 then v_boton_SxA else 0 end) + (case when v_hab_ACyF > 0 then v_boton_ACyF else 0 end) + v_boton_Perimido + (case when v_hab_ARCH > 0 then v_boton_archivo else 0 end);
          -- No Completo, CON Abiertas -> Abrir + Resignar + ??RPC + ??SxA + ??ACyF
          when p_estado_tramite < IPJ.TYPES.c_Estados_Completado and p_acciones_abiertas > 0
            then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_reasignar + (case when v_hab_RPC > 0 then v_boton_RPC else 0 end) + (case when v_hab_SxA > 0 then v_boton_SxA else 0 end) + (case when v_hab_ACyF > 0 then v_boton_ACyF else 0 end);
          else v_botones_habilitados:= v_botones_habilitados + 0;
        end case;
      end if;
    end if;

   -- ******************       ARCHIVO       ******************
    if p_id_ubicacion = IPJ.TYPES.C_AREA_ARCHIVO then
       --Segun el area de trabajo
      if p_area_trabajo = 1 then -- AREA ADMINISTRACION
        case
          when p_estado_tramite >= IPJ.TYPES.c_Estados_Completado and p_acciones_abiertas = 0 then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_reasignar+ v_boton_rechazar + v_boton_cerrar;
          when p_estado_tramite >= IPJ.TYPES.c_Estados_Completado and p_acciones_abiertas > 0 then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_reasignar;
          else v_botones_habilitados:= v_botones_habilitados + 0;
        end case;

      else --AREA TECNICA
        case p_estado_accion
          when 1 /* Est. Pendiente*/ then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_rechazar + v_boton_cerrar;
          when 2 /* Est. Asignado*/ then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_rechazar + v_boton_cerrar;
          when 3 /* Est. En Proceso*/ then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_rechazar + v_boton_cerrar;
          when 4 /* Est. En Revision*/ then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_rechazar + v_boton_cerrar;
          else v_botones_habilitados:= v_botones_habilitados + 0;
        end case;

        case
          when p_estado_tramite >= IPJ.TYPES.c_Estados_Completado and p_acciones_abiertas = 0  then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_reasignar + v_boton_cerrar;
          when p_estado_tramite >= IPJ.TYPES.c_Estados_Completado and p_acciones_abiertas > 0 then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_reasignar;
          else v_botones_habilitados:= v_botones_habilitados + 0;
        end case;
      end if;
    end if;

    -- ******************       MESA SUAC       ******************
    if p_id_ubicacion = IPJ.TYPES.C_AREA_MESASUAC then
       --Segun el area de trabajo
      if p_area_trabajo = 1 then -- AREA ADMINISTRACION
        case
          when p_estado_tramite >= IPJ.TYPES.c_Estados_Completado and p_acciones_abiertas = 0 then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_reasignar+ v_boton_rechazar + v_boton_cerrar + v_boton_archivo ;
          when p_estado_tramite >= IPJ.TYPES.c_Estados_Completado and p_acciones_abiertas > 0 then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_reasignar;
          else v_botones_habilitados:= v_botones_habilitados + 0;
        end case;

      else --AREA TECNICA
        case p_estado_accion
          when 1 /* Est. Pendiente*/ then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_rechazar + v_boton_cerrar;
          when 2 /* Est. Asignado*/ then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_rechazar + v_boton_cerrar;
          when 3 /* Est. En Proceso*/ then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_rechazar + v_boton_cerrar;
          when 4 /* Est. En Revision*/ then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_rechazar + v_boton_cerrar;
          else v_botones_habilitados:= v_botones_habilitados + 0;
        end case;

        case
          when p_estado_tramite >= IPJ.TYPES.c_Estados_Completado and p_acciones_abiertas = 0  then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_reasignar + v_boton_cerrar + v_boton_archivo ;
          when p_estado_tramite >= IPJ.TYPES.c_Estados_Completado and p_acciones_abiertas > 0 then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_reasignar;
          else v_botones_habilitados:= v_botones_habilitados + 0;
        end case;
      end if;
    end if;

    -- ************* RECURSOS HUMANOS ******************
    if p_id_ubicacion = IPJ.TYPES.C_AREA_RRHH then
       --Segun el area de trabajo
      if p_area_trabajo = 1 then -- AREA ADMINISTRACION
        case
          when p_estado_tramite < IPJ.TYPES.c_Estados_Completado and p_acciones_abiertas = 0 then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_reasignar+ v_boton_rechazar + v_boton_cerrar;
          when p_estado_tramite < IPJ.TYPES.c_Estados_Completado and p_acciones_abiertas > 0 then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_reasignar;
          else v_botones_habilitados:= v_botones_habilitados + 0;
        end case;

      else --AREA TECNICA
        case p_estado_accion
          when 1 /* Est. Pendiente*/ then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_rechazar + v_boton_cerrar;
          when 2 /* Est. Asignado*/ then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_rechazar + v_boton_cerrar;
          when 3 /* Est. En Proceso*/ then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_rechazar + v_boton_cerrar;
          when 4 /* Est. En Revision*/ then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_rechazar + v_boton_cerrar;
          else v_botones_habilitados:= v_botones_habilitados + 0;
        end case;

        case
          when p_estado_tramite < IPJ.TYPES.c_Estados_Completado and p_acciones_abiertas = 0  then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_reasignar + v_boton_cerrar;
          when p_estado_tramite < IPJ.TYPES.c_Estados_Completado and p_acciones_abiertas > 0 then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_reasignar;
          else v_botones_habilitados:= v_botones_habilitados + 0;
        end case;
      end if;
    end if;

    -- ************* DIRECCION ******************
    if p_id_ubicacion = IPJ.TYPES.C_AREA_DIRECCION then
       --Segun el area de trabajo
      if p_area_trabajo = 1 then -- AREA ADMINISTRACION
        case
          when p_estado_tramite < IPJ.TYPES.c_Estados_Completado and p_acciones_abiertas = 0 then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_reasignar+ v_boton_rechazar + v_boton_cerrar;
          when p_estado_tramite < IPJ.TYPES.c_Estados_Completado and p_acciones_abiertas > 0 then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_reasignar;
          else v_botones_habilitados:= v_botones_habilitados + 0;
        end case;

      else --AREA TECNICA
        case p_estado_accion
          when 1 /* Est. Pendiente*/ then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_rechazar + v_boton_cerrar;
          when 2 /* Est. Asignado*/ then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_rechazar + v_boton_cerrar;
          when 3 /* Est. En Proceso*/ then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_rechazar + v_boton_cerrar;
          when 4 /* Est. En Revision*/ then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_rechazar + v_boton_cerrar;
          else v_botones_habilitados:= v_botones_habilitados + 0;
        end case;

        case
          when p_estado_tramite < IPJ.TYPES.c_Estados_Completado and p_acciones_abiertas = 0  then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_reasignar + v_boton_cerrar;
          when p_estado_tramite < IPJ.TYPES.c_Estados_Completado and p_acciones_abiertas > 0 then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_reasignar;
          else v_botones_habilitados:= v_botones_habilitados + 0;
        end case;
      end if;
    end if;

    -- ************* Delegacion Rio Cuarto ******************
    if p_id_ubicacion = 123  then
      --Segun el area de trabajo
      if p_area_trabajo = 1 then -- AREA ADMINISTRACION
        case
          when p_estado_tramite < IPJ.TYPES.c_Estados_Completado and p_acciones_abiertas = 0 then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_reasignar+ v_boton_rechazar + v_boton_cerrar + v_boton_mesa;
          when p_estado_tramite < IPJ.TYPES.c_Estados_Completado and p_acciones_abiertas > 0 then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_reasignar;
          else v_botones_habilitados:= v_botones_habilitados + 0;
        end case;

      else --AREA TECNICA
        case p_estado_accion
          when 1 /* Est. Pendiente*/ then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_rechazar + v_boton_cerrar;
          when 2 /* Est. Asignado*/ then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_rechazar + v_boton_cerrar;
          when 3 /* Est. En Proceso*/ then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_rechazar + v_boton_cerrar;
          when 4 /* Est. En Revision*/ then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_rechazar + v_boton_cerrar;
          else v_botones_habilitados:= v_botones_habilitados + 0;
        end case;

        case
          when p_estado_tramite < IPJ.TYPES.c_Estados_Completado and p_acciones_abiertas = 0  and nvl(p_id_proceso, 0) = Ipj.Types.C_Proceso_Hist then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_reasignar + v_boton_cerrar;
          when p_estado_tramite < IPJ.TYPES.c_Estados_Completado and p_acciones_abiertas = 0  and nvl(p_id_proceso, 0) <> Ipj.Types.C_Proceso_Hist then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_reasignar;
          when p_estado_tramite < IPJ.TYPES.c_Estados_Completado and p_acciones_abiertas > 0 then v_botones_habilitados:= v_botones_habilitados + v_boton_abrir + v_boton_reasignar;
          else v_botones_habilitados:= v_botones_habilitados + 0;
        end case;
      end if;
    end if;


    return  v_botones_habilitados;
  END FC_Habilitar_Botones;

  PROCEDURE SP_GUARDAR_LOG(
    p_METODO in VARCHAR2,
    p_NIVEL in VARCHAR2,
    p_ORIGEN in VARCHAR2,
    p_USR_LOG  in VARCHAR2,
    p_USR_WINDOWS in VARCHAR2,
    p_IP_PC  in VARCHAR2,
    p_EXCEPCION in VARCHAR2)
  IS
    v_id_Log_App number;
    v_error_message varchar2(4000);
    v_error_Code number;
  BEGIN
  /***********************************************************
  Inserta un nuevoo registro en la tabla T_LOG_APP
  ************************************************************/
    SELECT IPJ.SEQ_LOGS_APP.nextval INTO v_id_Log_App FROM dual;

    insert into IPJ.T_LOGS_APP (ID_LOG_APP, METODO, NIVEL, FEC_EVENT, ORIGEN, USR_LOG, USR_WINDOWS, IP_PC, EXCEPCION)
    values (v_id_Log_App, SubStr(p_METODO, 1, 200), SubStr(p_NIVEL, 1, 100), sysdate,
      SubStr(p_ORIGEN, 1, 500), SubStr(p_USR_LOG, 1, 20), SubStr(p_USR_WINDOWS, 1, 100),
      SubStr(p_IP_PC, 1, 15), SubStr(p_EXCEPCION, 1, 4000));

    Commit;
  exception
    when OTHERS then
      v_error_message :=  SubStr(SQLERRM, 1, 4000);
      v_error_Code := SQLCODE;
      rollback;

    SELECT IPJ.SEQ_LOGS_APP.nextval INTO v_id_Log_App FROM dual;
    insert into IPJ.T_LOGS_APP (ID_LOG_APP, METODO, NIVEL, FEC_EVENT, ORIGEN, USR_LOG, USR_WINDOWS, IP_PC, EXCEPCION)
    values (v_id_Log_App, 'SP_GUARDAR_LOG', '', sysdate, 'ORA-' || to_char(v_error_Code), 'DB', '',  '', v_error_message);

    commit;
  END SP_GUARDAR_LOG;

  PROCEDURE SP_GUARDAR_LOG_OL(
    p_METODO in VARCHAR2,
    p_NIVEL in VARCHAR2,
    p_ORIGEN in VARCHAR2,
    p_USR_LOG  in VARCHAR2,
    p_USR_WINDOWS in VARCHAR2,
    p_IP_PC  in VARCHAR2,
    p_EXCEPCION in VARCHAR2)
  IS
    v_id_Log_App number;
    v_error_message varchar2(4000);
    v_error_Code number;
  BEGIN
  /***********************************************************
  Inserta un nuevoo registro en la tabla T_LOG_APP
  ************************************************************/
    SELECT IPJ.SEQ_LOGS_APP.nextval INTO v_id_Log_App FROM dual;

    insert into IPJ.T_LOGS_APP (ID_LOG_APP, METODO, NIVEL, FEC_EVENT, ORIGEN, USR_LOG, USR_WINDOWS, IP_PC, EXCEPCION)
    values (v_id_Log_App, SubStr('PORTAL OL: ' || p_METODO, 1, 200), SubStr(p_NIVEL, 1, 100), sysdate,
      SubStr(p_ORIGEN, 1, 500), SubStr(p_USR_LOG, 1, 20), SubStr(p_USR_WINDOWS, 1, 100),
      SubStr(p_IP_PC, 1, 15), SubStr(p_EXCEPCION, 1, 4000));

    Commit;
  exception
    when OTHERS then
      v_error_message :=  SubStr(SQLERRM, 1, 4000);
      v_error_Code := SQLCODE;
      rollback;

    SELECT IPJ.SEQ_LOGS_APP.nextval INTO v_id_Log_App FROM dual;
    insert into IPJ.T_LOGS_APP (ID_LOG_APP, METODO, NIVEL, FEC_EVENT, ORIGEN, USR_LOG, USR_WINDOWS, IP_PC, EXCEPCION)
    values (v_id_Log_App, 'SP_GUARDAR_LOG_OL', '', sysdate, 'ORA-' || to_char(v_error_Code), 'DB', '',  '', v_error_message);

    commit;
  END SP_GUARDAR_LOG_OL;


  FUNCTION VALIDA_TRAMITE_SUAC(p_id_tramite IN number, p_id_tramite_ipj IN number) return Number is
    v_res number;
  BEGIN
    select count(*) into v_res
    from IPJ.T_tramitesipj
    where
      id_tramite = p_id_tramite and
      id_tramite_ipj != p_id_tramite_ipj;

    Return v_res;
  Exception
    When Others Then
      Return 0;
  End VALIDA_TRAMITE_SUAC;

  PROCEDURE SP_Traer_Estados_Entidad(
    p_Cursor OUT types.cursorType)
  IS
  /**************************************************
    Lista los Estados de Entidades disponibles
  ***************************************************/
  BEGIN
    OPEN p_Cursor FOR
    select T.ESTADO_ENTIDAD, T.ID_ESTADO_ENTIDAD
    from IPJ.T_ESTADOS_ENTIDADES t;
  END SP_Traer_Estados_Entidad;

  PROCEDURE SP_Traer_Tipo_Organismos (
    o_Cursor OUT types.cursorType,
    p_id_tipo_accion in number)
  IS
    v_id_ubicacion number(10);
  /**************************************************
    Lista los Estados de Entidades disponibles
  ***************************************************/
  BEGIN
    if p_id_tipo_accion <> 0 then
      select tClas.id_ubicacion into v_id_ubicacion
      from IPJ.t_Tipos_AccionesIPJ tacc join IPJ.T_Tipos_Clasif_Ipj tClas
          on TACC.ID_CLASIF_IPJ = TCLAS.ID_CLASIF_IPJ
      where
        tacc.id_tipo_accion = p_id_tipo_accion;
    else
      v_id_ubicacion := 0;
    end if;

    OPEN o_Cursor FOR
      select T.ID_TIPO_ORGANISMO, T.N_TIPO_ORGANISMO, t.id_ubicacion,
        u.n_ubicacion, T.ES_ADMIN
      from IPJ.T_TIPOS_ORGANISMO t join ipj.t_ubicaciones u
          on t.id_ubicacion = u.id_ubicacion
      where
        v_id_ubicacion = 0 or t.id_ubicacion = v_id_ubicacion;

  END SP_Traer_Tipo_Organismos ;

  PROCEDURE SP_Traer_Tipo_Sindicos (
    o_Cursor OUT types.cursorType,
    p_Id_Tipo_Organismo in number,
    p_id_ubicacion in number)
  IS
  /**************************************************
    Lista los Cargos del organo de fiscalizacion indicado
  ***************************************************/
  BEGIN
    OPEN o_Cursor FOR
      select T.id_tipo_integrante ID_TIPO_SINDICO, T.id_tipo_integrante,
        t.n_tipo_integrante N_TIPO_SINDICO, t.n_tipo_integrante,
        t.id_tipo_organismo, t.id_ubicacion, t.Es_Suplente, t.Repetible
      from IPJ.t_tipos_integrante t
      where
        (nvl(p_id_tipo_organismo, 0) = 0 or id_tipo_organismo = p_id_tipo_organismo) and
        id_ubicacion = p_id_ubicacion;

  END SP_Traer_Tipo_Sindicos ;

  PROCEDURE SP_Traer_Tipos_Orden_Dia (
    p_id_entidad_Acta in number,
    o_Cursor OUT types.cursorType)
  IS
  /**************************************************
    Lista los Estados de Entidades disponibles
  ***************************************************/
  BEGIN
    OPEN o_Cursor FOR
      select T.ID_TIPO_ORDEN_DIA, T.N_TIPO_ORDEN_DIA, T.SIGLAS
      from IPJ.T_TIPOS_ORDEN_DIA t
      where
        id_tipo_orden_dia not in
          (select id_tipo_orden_dia from IPJ.T_ENTIDADES_ACTA_ORDEN a where A.ID_ENTIDAD_ACTA = p_id_entidad_Acta);

  END SP_Traer_Tipos_Orden_Dia;


  PROCEDURE SP_Traer_Archivos_Actas (
    p_ID_TIPO_ACTA in number,
    o_Cursor OUT types.cursorType)
  IS
  /**************************************************
    Lista los archivos asociados a un tipo de acta
  ***************************************************/
  BEGIN
    OPEN o_Cursor FOR
    select AA.ID_ARCHIVO, AA.ID_TIPO_ACTA, AA.ABREVIACION, AA.N_ARCHIVO,
      A.N_TIPO_ACTA
    from IPJ.T_ARCHIVOS_ACTA aa join IPJ.T_TIPOS_ACTA a
      on aa.id_tipo_acta = a.id_tipo_acta
    where
      p_id_tipo_acta = 0 or aa.id_tipo_acta = p_id_tipo_acta;

  END SP_Traer_Archivos_Actas ;

  PROCEDURE SP_Traer_Tipo_Origen (
    o_Cursor OUT types.cursorType,
    p_id_estado_entidad in varchar2)
  IS
  /**************************************************
    Lista los Origenes posible para un estado determinado
  ***************************************************/
  BEGIN
    OPEN o_Cursor FOR
      select T.id_tipo_origen, t.n_tipo_origen, T.ID_ESTADO_ENTIDAD, E.ESTADO_ENTIDAD
      from IPJ.T_TIPOS_ORIGEN t join ipj.t_estados_entidades e
        on t.id_estado_entidad = e.id_estado_entidad
      where
        p_id_estado_entidad is null or t.id_estado_entidad = p_id_estado_entidad;
  END SP_Traer_Tipo_Origen ;

  PROCEDURE SP_GUARDAR_DOMICILIO(
    o_id_vin out number,
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_vin in number,
    p_usuario in varchar2,
    p_Tipo_Domicilio in number, -- 3 Real //  0 Sin Asignar
    p_id_integrante in number,
    p_id_legajo in number,
    p_cuit_empresa in varchar2,
    p_id_fondo_comercio in number,
    p_id_entidad_acta in number,
    p_es_comerciante in varchar2, -- S / N
    p_es_admin_entidad in varchar2,  -- S / N
    P_ID_PROVINCIA in varchar2,
    P_ID_DEPARTAMENTO in number,
    P_ID_LOCALIDAD in number,
    P_BARRIO in varchar2,
    P_CALLE in varchar2,
    P_ALTURA in number,
    P_DEPTO in varchar2,
    P_PISO in varchar2,
    p_torre in varchar2,
    p_manzana in varchar2,
    p_lote in varchar2,
    p_id_barrio in number,
    p_id_calle in number,
    p_id_tipocalle in number,
    p_km in varchar2
    )
  IS
    v_valid_parametros varchar2(500);
    v_TIPO_DOM  NUMBER;
    v_N_TIPO_DOM  VARCHAR2(150);
    v_bloque varchar2(200);
    v_entidad varchar2(20);
    v_nro_documento varchar2(20);
    v_tipo varchar2(1); -- C = Comerciante / A = Administrador / I = Integrante / E = Entidad / F = Fondo Comercio
    v_integrante ipj.t_integrantes%rowtype;
    v_existe_dom_unico number;
  BEGIN
  /*************************************************************
    Inserta o Actualiza un domicilio para un integrante en DOM_MANAGER
    No maneja transaccion, ya lo hace DOM_MANAGER
  *************************************************************/
    -- VALIDACION DE PARAMETROS
    if P_ID_PROVINCIA is null then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('PROV_NOT');
    end if;
    if p_manzana is null and p_lote is null and P_CALLE is null then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('CALLE_NOT');
    end if;
    if P_ID_LOCALIDAD is null then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('LOCAL_NOT');
    end if;
    if P_ID_DEPARTAMENTO is null then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('DEPTO_NOT');
    end if;
    --if nvl(p_Id_Integrante, 0) = 0 and p_id_legajo = 0 and p_id_fondo_comercio = 0 and p_id_entidad_acta = 0 then
    --  v_valid_parametros := v_valid_parametros || 'Persona Inv�lida';
    --end if;
    if length(p_manzana) > 5 then
      v_valid_parametros := v_valid_parametros || 'La manzana no puede tener m�s de 5 caracteres.';
    end if;
    if length(p_lote) > 5 then
      v_valid_parametros := v_valid_parametros || 'El lote no puede tener m�s de 5 caracteres.';
    end if;

    if v_valid_parametros is not null then
      o_rdo := 'Parametros : ' || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    -- CUERPO PROCEDIMIENTO
    v_entidad :=  IPJ.VARIOS.FC_Generar_Entidad_Dom (
        p_id_integrante => p_id_integrante,
        p_id_legajo => p_id_legajo,
        p_cuit_empresa => p_cuit_empresa,
        p_id_fondo_comercio => p_id_fondo_comercio,
        p_id_entidad_acta => p_id_entidad_acta,
        p_es_comerciante => p_es_comerciante,
        p_es_admin_entidad => p_es_admin_entidad,
        p_id_admin => null);

    if (Nvl(p_id_vin, 0) = 0) or ('S' = FC_DOMICILIO_MODIFICADO (p_id_vin,  p_id_integrante, p_id_legajo,
        p_cuit_empresa, p_id_fondo_comercio,
        p_id_entidad_acta, p_es_comerciante, p_es_admin_entidad, P_ID_PROVINCIA, P_ID_DEPARTAMENTO,
        P_ID_LOCALIDAD, P_BARRIO, P_CALLE, P_ALTURA, P_DEPTO, P_PISO, p_torre,
        p_manzana, p_lote, p_id_tipocalle, p_km))
    then

      if nvl(p_id_vin, 0) = 0 then
        v_bloque := 'Crear Domicilio';
        /*
        DOM_MANAGER.DOM_DOMICILIO.INSERT_DOMICILIO(
          P_ID_APLICACION => Types.c_id_Aplicacion,
          P_ID_TIPODOM => 3, --Domicilio REAL
          P_ID_ENTIDAD => v_entidad,
          P_ID_PROVINCIA => p_id_provincia,
          P_ID_DEPARTAMENTO => p_id_departamento,
          P_ID_LOCALIDAD => p_id_localidad,
          P_LOCALIDAD => null,
          P_ID_TIPOCALLE => 9, -- Tipo CALLE
          P_TIPO_CALLE => null,
          P_ID_CALLE => null,
          P_CALLE => substr(p_calle, 1, 70),
          P_ID_BARRIO => null,
          P_BARRIO  => substr(p_barrio, 1, 1000),
          P_ID_PRECINTO => null,
          P_ALTURA => p_altura,
          P_PISO => substr(p_piso, 1, 4),
          P_MZNA => p_manzana,
          P_LOTE => p_lote,
          P_DEPTO => substr(p_depto, 1, 4),
          P_TORRE => substr(p_torre, 1, 20),
          O_ID_VIN => o_id_vin,
          O_TIPO_DOM => v_TIPO_DOM,
          O_N_TIPO_DOM => v_N_TIPO_DOM,
          O_MENSAJE => o_rdo
          );
      */
          DOM_MANAGER.DOM_DOMICILIO.INSERT_DOMICILIO_NUEVO(
            P_ID_APLICACION => Types.c_id_Aplicacion,
            P_ID_TIPODOM => 3, --Domicilio REAL
            P_ID_ENTIDAD => v_entidad,
            P_ID_PROVINCIA => p_id_provincia,
            P_ID_DEPARTAMENTO => p_id_departamento,
            P_ID_LOCALIDAD => p_id_localidad,
            P_LOCALIDAD => null,
            P_ID_TIPOCALLE => (case when nvl(p_id_tipocalle, 0) = 0 then 9 else p_id_tipocalle end), -- 9 Tipo CALLE
            P_TIPO_CALLE => null,
            P_ID_CALLE => (case when p_id_calle = 0 then null else p_id_calle end),
            P_CALLE => substr(p_calle, 1, 70),
            P_ID_BARRIO => (case when p_id_barrio = 0 then null else p_id_barrio end),
            P_BARRIO => substr(p_barrio, 1, 1000),
            P_ID_PRECINTO => null,
            P_CPA => null,
            P_ALTURA => (case when p_altura = 0 then null else p_altura end),
            P_PISO => substr(p_piso, 1, 4),
            P_DEPTO => substr(p_depto, 1, 4),
            P_TORRE => substr(p_torre, 1, 20),
            P_MZNA => p_manzana,
            P_LOTE => p_lote,
            P_LATITUD => null,
            P_LONGITUD => null,
            P_NOMEN_CATASTRAL => null,
            P_ID_COMPLEJO => null,
            P_KM => substr(p_km, 1, 10),
            O_ID_VIN => o_id_vin,
            O_TIPO_DOM => v_TIPO_DOM,
            O_MENSAJE => o_rdo
          );
      else
        -- Si la entidad tenia un domicilio, lo actualizo
        v_bloque := 'Actualizar Domicilio';
        /*
        DOM_MANAGER.DOM_DOMICILIO.MODIFICA_DOMICILIO(
          P_ID_APLICACION => Types.c_id_Aplicacion,
          P_ID_DOMICILIO => p_id_vin,
          P_ID_TIPO_DOMICILIO => 3, --Domicilio REAL
          P_ID_ENTIDAD =>  v_entidad,
          P_ID_PROVINCIA => p_id_provincia,
          P_ID_DEPARTAMENTO => p_id_departamento,
          P_ID_LOCALIDAD => p_id_localidad,
          P_LOCALIDAD => null,
          P_ID_TIPOCALLE => 9, -- Tipo CALLE
          P_TIPO_CALLE => null,
          P_ID_CALLE => null,
          P_CALLE => substr(p_calle, 1, 70),
          P_ID_BARRIO => null,
          P_BARRIO  => substr(p_barrio, 1, 1000),
          P_ID_PRECINTO => null,
          P_ALTURA => p_altura,
          P_PISO => substr(p_piso, 1, 4),
          P_DEPTO => substr(p_depto, 1, 4),
          P_TORRE=> substr(p_torre, 1 , 20),
          P_MZNA => p_manzana,
          P_LOTE => p_lote,
          O_MENSAJE => o_rdo,
          O_ID_VIN => o_id_vin
        );
        */
         DOM_MANAGER.DOM_DOMICILIO.MODIF_DOMICILIO_NUEVO(
           P_ID_APLICACION => Types.c_id_Aplicacion,
           P_ID_DOMICILIO => p_id_vin,
           P_ID_TIPO_DOMICILIO => 3, --Domicilio REAL
           P_ID_ENTIDAD =>  v_entidad,
           P_ID_PROVINCIA => p_id_provincia,
           P_ID_DEPARTAMENTO => p_id_departamento,
           P_ID_LOCALIDAD => p_id_localidad,
           P_LOCALIDAD => null,
           P_ID_TIPOCALLE => (case when nvl(p_id_tipocalle, 0) = 0 then 9 else p_id_tipocalle end), -- 9 Tipo CALLE
           P_TIPO_CALLE => null,
           P_ID_CALLE => (case when p_id_calle = 0 then null else p_id_calle end),
           P_CALLE => substr(p_calle, 1, 70),
           P_ID_BARRIO => (case when p_id_barrio = 0 then null else p_id_barrio end),
           P_BARRIO => substr(p_barrio, 1, 1000),
           P_ID_PRECINTO => null,
           P_ALTURA =>(case when p_altura = 0 then null else p_altura end),
           P_PISO => substr(p_piso, 1, 4),
           P_DEPTO => substr(p_depto, 1, 4),
           P_TORRE => substr(p_torre, 1 , 20),
           P_MZNA => p_manzana,
           P_LOTE => p_lote,
           P_CPA => null,
           P_LATITUD => null,
           P_LONGITUD => null,
           P_NOMEN_CATASTRAL => null,
           P_ID_COMPLEJO => null,
           P_KM => substr(p_km, 1, 10),
           O_MENSAJE => o_rdo,
           O_ID_VIN => o_id_vin
         );
      end if;
    else -- No hay cambios, devuelvo el mismo ID_VIN y OK
      o_id_vin := p_id_vin;
      o_rdo := IPJ.TYPES.C_RESP_OK;
    end if;

    -- Si no fallo el domicilio, y es para una persona, lo asocio a la misma
    if nvl(p_id_integrante, 0) > 0 and p_Tipo_Domicilio = 3 then
      -- Busco los datos de la persona
      select * into v_integrante
      from ipj.t_integrantes i
      where
        id_integrante = p_id_integrante;

      -- Verifico si ya existe un domicilio unico real
      select count(1) into v_existe_dom_unico
      from rcivil.vt_personas_domicilio_unico
      where
        id_sexo = v_integrante.id_sexo and
        nro_documento = v_integrante.nro_documento and
        pai_cod_pais = v_integrante.pai_cod_pais and
        id_numero = v_integrante.id_numero and
        id_tipo_domicilio = p_Tipo_Domicilio;--Domicilio REAL

      -- Si no existe un domicilio real unico
      if nvl(v_existe_dom_unico, 0) = 0 then
        RCIVIL.pack_persona.INSERTA_DOMICILIO_PERSONA(
          p_id_aplicacion => IPJ.TYPES.C_ID_APLICACION,
          p_id_sexo => v_integrante.id_sexo,
          p_nro_documento => v_integrante.nro_documento,
          p_pai_cod_pais => v_integrante.pai_cod_pais,
          p_id_numero => v_integrante.id_numero,
          p_id_vin => o_id_vin,
          p_id_tipo_domicilio => p_Tipo_Domicilio,
          p_usr => p_usuario,
          o_resultado => o_rdo
        );
      else
        RCIVIL.pack_persona.MODIFICA_DOMICILIO_PERSONA(
          p_id_aplicacion => IPJ.TYPES.C_ID_APLICACION,
          p_id_sexo => v_integrante.id_sexo,
          p_nro_documento => v_integrante.nro_documento,
          p_pai_cod_pais => v_integrante.pai_cod_pais,
          p_id_numero => v_integrante.id_numero,
          p_id_vin => o_id_vin,
          p_id_tipo_domicilio => p_Tipo_Domicilio,
          p_usr => p_usuario,
          o_resultado => o_rdo
        );
      end if;
    end if;

    if Upper(o_rdo) = IPJ.TYPES.C_RESP_OK then
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    else
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
    end if;
  EXCEPTION
     WHEN OTHERS THEN
        o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;

  END SP_GUARDAR_DOMICILIO;


  PROCEDURE SP_Traer_Domicilio(
      p_id_vin in number,
      p_id_integrante in number,
      p_id_legajo in number,
      p_cuit_empresa in varchar2,
      p_id_fondo_comercio in number,
      p_id_entidad_acta in number,
      p_es_comerciante in varchar2, -- S / N
      p_es_admin_entidad in varchar2, -- S / N
      p_Cursor OUT TYPES.cursorType)
  IS
    v_entidad varchar2(20);
    v_nro_documento varchar2(20);
    v_tipo varchar2(1); -- C = Comerciante / A = Administrador / I = Integrante / E = Entidad / F = Fondo Comercio
  BEGIN
    /********************************************************
      Este procemiento devuelve la direccion la persona indidaca (uno de los 3):
            - Integrante
            - Entidad
            - Fondo de Comercio
      En el caso de los integrantes, se debe indicar si es una Persona Fisica, un Vendedor o
      un Administrador de una Entidad.
    *********************************************************/
IPJ.VARIOS.SP_GUARDAR_LOG(
        p_METODO => 'SP_Traer_Domicilio',
        p_NIVEL => 'Base de Datos',
        p_ORIGEN => 'DB',
        p_USR_LOG  => 'DB',
        p_USR_WINDOWS => 'Sistema',
        p_IP_PC  => null,
        p_EXCEPCION =>
          'id_vin = ' ||  to_char(p_id_vin)
          || ' / p_id_integrante = ' || to_char(p_id_integrante)
          || ' / p_id_legajo = ' || to_char(p_id_legajo)
          || ' / p_cuit_empresa = ' || p_cuit_empresa
          || ' / p_id_fondo_comercio = ' || to_char(p_id_fondo_comercio)
          || ' / p_id_entidad_acta = ' || to_char(p_id_entidad_acta)
          || ' / p_es_comerciante = ' || p_es_comerciante
          || ' / p_es_admin_entidad = ' || p_es_admin_entidad
      );

     v_entidad :=  IPJ.VARIOS.FC_Generar_Entidad_Dom (
        p_id_integrante => p_id_integrante,
        p_id_legajo => p_id_legajo,
        p_cuit_empresa => p_cuit_empresa,
        p_id_fondo_comercio => p_id_fondo_comercio,
        p_id_entidad_acta => p_id_entidad_Acta,
        p_es_comerciante => p_es_comerciante,
        p_es_admin_entidad => p_es_admin_entidad,
        p_id_admin => null);

    IF p_id_vin = 0 THEN
      OPEN p_Cursor FOR
        select
          '' calle_inf,
          '' id_tipodom, '' n_tipodom, '' id_tipocalle, '' n_tipocalle, '' id_calle,
          '' n_calle, '' altura, '' depto, '' piso, '' torre, '' id_barrio,
          '' n_barrio, /*d.nro_mail, d.cod_area,*/
          '' id_localidad, '' n_localidad, '' id_departamento,
          '' n_departamento, '' id_provincia,
          '' n_provincia,
          '' cpa,
          '' id_vin, '' Manzana, '' Lote, '' Km,
          '' id_afip
        from dom_manager.VT_DOMICILIOS_COND d
        where
          id_vin = p_id_vin;
    ELSE
      OPEN p_Cursor FOR
        select
          IPJ.VARIOS.FC_ARMAR_CALLE_DOM(d.n_calle, d.altura, d.piso, d.depto, d.torre, d.mzna, d.lote, d.n_barrio, d.km, d.n_tipocalle) calle_inf,
          d.id_tipodom, d.n_tipodom, nvl(d.id_tipocalle, 9) id_tipocalle, nvl(d.n_tipocalle, 'CALLE') n_tipocalle, d.id_calle,
          initcap(d.n_calle) n_calle, d.altura, d.depto, d.piso, d.torre, d.id_barrio,
          initcap(d.n_barrio) n_barrio, /*d.nro_mail, d.cod_area,*/
          d.id_localidad, initcap(d.n_localidad) n_localidad, d.id_departamento,
          initcap(d.n_departamento) n_departamento, d.id_provincia,
          initcap(d.n_provincia) n_provincia,
          IPJ.VARIOS.FC_Buscar_Codigo_Postal(d.id_localidad, d.id_barrio) cpa,
          d.id_vin, D.mzna Manzana, D.Lote, d.km Km,
          IPJ.VARIOS.FC_Buscar_Codigo_AFIP(d.id_localidad, d.id_barrio) id_afip
        from dom_manager.VT_DOMICILIOS_COND d
        where
          id_vin = p_id_vin;
    END IF;

 END SP_Traer_Domicilio;


 PROCEDURE SP_Guardar_Matricula(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in number,
    p_id_legajo in number,
    p_id_tramiteipj_accion in number,
    p_cuit in varchar2,
    p_matricula in varchar2,
    p_letra in varchar2)
  IS
  BEGIN
    IPJ.VARIOS.SP_GUARDAR_MATRICULA(
      o_rdo => o_rdo,
      o_tipo_mensaje => o_tipo_mensaje,
      p_id_tramite_ipj => p_id_tramite_ipj,
      p_id_legajo => p_id_legajo,
      p_id_tramiteipj_accion => p_id_tramiteipj_accion,
      p_cuit => p_cuit,
      p_matricula => (upper(p_letra) || p_matricula)
    );
  END SP_Guardar_Matricula;

   PROCEDURE SP_Guardar_Tipos_Actas(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_tipo_acta OUT number,
    p_id_tipo_acta in number,
    p_n_tipo_acta in varchar2,
    p_id_ubicacion in number)
  IS
  v_valid_parametros varchar(2000);
  /**************************************************
    Guarda o modifica un Tipo de Acta o Asamblea
  ***************************************************/
  BEGIN
    -- Valido que venga Descripcion y Ubicacion
    if nvl(p_id_ubicacion, 0) = 0 then
      v_valid_parametros := v_valid_parametros || 'Ubicaci�n Inv�lida';
    end if;
    if p_n_tipo_acta is null then
      v_valid_parametros := v_valid_parametros || 'Nombre Inv�lido';
    end if;

    if v_valid_parametros is not null then
      o_rdo := 'Parametros : ' || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    --Si estan bien los datos, actualizo, si no hay registro lo agrege y se devuelve el valor insertado.
    update IPJ.T_TIPOS_ACTA
    set
      id_ubicacion = p_id_ubicacion,
      n_tipo_acta = p_n_tipo_acta
    where
      id_tipo_acta = p_id_tipo_acta;

    if sql%rowcount = 0 then
      select max(id_tipo_acta) +1 into o_id_tipo_acta
      from IPJ.T_TIPOS_ACTA;

      insert into IPJ.T_TIPOS_ACTA (id_tipo_acta, n_tipo_acta, id_ubicacion)
      values (o_id_tipo_acta, p_n_tipo_acta, p_id_ubicacion);

    else
      o_id_tipo_acta := p_id_tipo_Acta;
    end if;

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     when OTHERS then
        o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Guardar_Tipos_Actas;

  PROCEDURE SP_Guardar_Tipos_Integrantes(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_tipo_integrante OUT number,
    p_id_tipo_integrante in number,
    p_n_tipo_integrante in varchar2,
    p_id_ubicacion in number)
  IS
  v_valid_parametros varchar(2000);
  /**************************************************
    Guarda o modifica un Tipo de Integrante
  ***************************************************/
  BEGIN
    -- Valido que venga Descripcion y Ubicacion
    if nvl(p_id_ubicacion, 0) = 0 then
      v_valid_parametros := v_valid_parametros || 'Ubicaci�n Inv�lida';
    end if;
    if p_n_tipo_integrante is null then
      v_valid_parametros := v_valid_parametros || 'Nombre Inv�lido';
    end if;

    if v_valid_parametros is not null then
      o_rdo := 'Parametros : ' || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    --Si estan bien los datos, actualizo, si no hay registro lo agrege y se devuelve el valor insertado.
    update IPJ.T_TIPOS_INTEGRANTE
    set
      id_ubicacion = p_id_ubicacion,
      n_tipo_integrante = p_n_tipo_integrante
    where
      id_tipo_integrante = p_id_tipo_integrante;

    if sql%rowcount = 0 then
      select max(id_tipo_integrante) +1 into o_id_tipo_integrante
      from IPJ.T_TIPOS_INTEGRANTE;

      insert into IPJ.T_TIPOS_INTEGRANTE (id_tipo_integrante, n_tipo_integrante, id_ubicacion)
      values (o_id_tipo_integrante, p_n_tipo_integrante, p_id_ubicacion);

    else
      o_id_tipo_integrante := p_id_tipo_integrante;
    end if;

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     when OTHERS then
        o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Guardar_Tipos_Integrantes;

   PROCEDURE SP_Guardar_Tipo_Organismos(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_tipo_organismo OUT number,
    p_id_tipo_organismo in number,
    p_n_tipo_organismo in varchar2,
    p_id_ubicacion in number)
  IS
  v_valid_parametros varchar(2000);
  /**************************************************
    Guarda o modifica un Tipo de Organismo
  ***************************************************/
  BEGIN
    -- Valido que venga Descripcion y Ubicacion
    if nvl(p_id_ubicacion, 0) = 0 then
      v_valid_parametros := v_valid_parametros || 'Ubicaci�n Inv�lida';
    end if;
    if p_n_tipo_organismo is null then
      v_valid_parametros := v_valid_parametros || 'Nombre Inv�lido';
    end if;

    if v_valid_parametros is not null then
      o_rdo := 'Parametros : ' || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    --Si estan bien los datos, actualizo, si no hay registro lo agrege y se devuelve el valor insertado.
    update IPJ.T_TIPOS_ORGANISMO
    set
      id_ubicacion = p_id_ubicacion,
      n_tipo_organismo = p_n_tipo_organismo
    where
      id_tipo_organismo = p_id_tipo_organismo;

    if sql%rowcount = 0 then
      select max(id_tipo_organismo) +1 into o_id_tipo_organismo
      from IPJ.T_TIPOS_ORGANISMO;

      insert into IPJ.T_TIPOS_ORGANISMO (id_tipo_organismo, n_tipo_organismo, id_ubicacion)
      values (o_id_tipo_organismo, p_n_tipo_organismo, p_id_ubicacion);

    else
      o_id_tipo_organismo := p_id_tipo_organismo;
    end if;

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     when OTHERS then
        o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Guardar_Tipo_Organismos;

  PROCEDURE SP_Guardar_Tipo_Sindicos(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_tipo_sindico OUT number,
    p_id_tipo_sindico in number,
    p_n_tipo_sindico in varchar2)
  IS
  v_valid_parametros varchar(2000);
  /**************************************************
    Guarda o modifica un Tipo de Sindico
  ***************************************************/
  BEGIN
    -- Valido que venga Descripcion
    if p_n_tipo_sindico is null then
      v_valid_parametros := v_valid_parametros || 'Nombre Inv�lido';
    end if;

    if v_valid_parametros is not null then
      o_rdo := 'Parametros : ' || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    --Si estan bien los datos, actualizo, si no hay registro lo agrege y se devuelve el valor insertado.
    update IPJ.T_TIPOS_SINDICO
    set
      n_tipo_sindico = p_n_tipo_sindico
    where
      id_tipo_sindico = p_id_tipo_sindico;

    if sql%rowcount = 0 then
      select max(id_tipo_sindico) +1 into o_id_tipo_sindico
      from IPJ.T_TIPOS_SINDICO;

      insert into IPJ.T_TIPOS_SINDICO (id_tipo_sindico, n_tipo_sindico)
      values (o_id_tipo_sindico, p_n_tipo_sindico);

    else
      o_id_tipo_sindico := p_id_tipo_sindico;
    end if;

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     when OTHERS then
        o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Guardar_Tipo_Sindicos;

  PROCEDURE SP_Guardar_Tipo_Rel_Empresa(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_tipo_relacion_empresa OUT number,
    p_id_tipo_relacion_empresa in number,
    p_n_tipo_relacion_empresa in varchar2)
  IS
  v_valid_parametros varchar(2000);
  /**************************************************
    Guarda o modifica una Relacion entre Empresas
  ***************************************************/
  BEGIN
    -- Valido que venga Descripcion
    if p_n_tipo_relacion_empresa is null then
      v_valid_parametros := v_valid_parametros || 'Nombre Inv�lido';
    end if;

    if v_valid_parametros is not null then
      o_rdo := 'Parametros : ' || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    --Si estan bien los datos, actualizo, si no hay registro lo agrege y se devuelve el valor insertado.
    update IPJ.T_TIPOS_RELACION_EMPRESA
    set
      n_tipo_relacion_empresa = p_n_tipo_relacion_empresa
    where
      id_tipo_relacion_empresa = p_id_tipo_relacion_empresa;

    if sql%rowcount = 0 then
      select max(id_tipo_relacion_empresa) +1 into o_id_tipo_relacion_empresa
      from IPJ.T_TIPOS_RELACION_EMPRESA;

      insert into IPJ.T_TIPOS_RELACION_EMPRESA (id_tipo_relacion_empresa, n_tipo_relacion_empresa)
      values (o_id_tipo_relacion_empresa, p_n_tipo_relacion_empresa);

    else
      o_id_tipo_relacion_empresa := p_id_tipo_relacion_empresa;
    end if;

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     when OTHERS then
        o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Guardar_Tipo_Rel_Empresa;

  PROCEDURE SP_Guardar_Estados_Entidad(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_estado_entidad OUT varchar2,
    p_id_estado_entidad in varchar2,
    p_estado_entidad in varchar2)
  IS
  v_valid_parametros varchar(2000);
  /**************************************************
    Guarda o modifica un Estado de Entidades
  ***************************************************/
  BEGIN
    -- Valido que venga Descripcion
    if p_estado_entidad is null then
      v_valid_parametros := v_valid_parametros || 'Nombre Inv�lido';
    end if;

    if v_valid_parametros is not null then
      o_rdo := 'Parametros : ' || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    --Si estan bien los datos, actualizo, si no hay registro lo agrege y se devuelve el valor insertado.
    update IPJ.T_ESTADOS_ENTIDADES
    set
      estado_entidad = p_estado_entidad
    where
      id_estado_entidad = p_id_estado_entidad;

    if sql%rowcount = 0 then
      o_id_estado_entidad := substr(p_estado_entidad, 1, 1);
      insert into IPJ.T_ESTADOS_ENTIDADES (id_estado_entidad, estado_entidad)
      values (p_id_estado_entidad, p_estado_entidad);

    else
      o_id_estado_entidad := p_id_estado_entidad;
    end if;

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     when OTHERS then
        o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Guardar_Estados_Entidad;


  PROCEDURE SP_Guardar_Origen(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_tipo_origen OUT number,
    p_id_tipo_origen in number,
    p_n_tipo_origen in varchar2,
    p_id_estado_entidad in varchar2)
  IS
    v_valid_parametros varchar(2000);
  /**************************************************
    Guarda o modifica un Origen relaionado a un Estado de Entidades
  ***************************************************/
  BEGIN
    -- Valido que venga Descripcion
    if p_n_tipo_origen is null then
      v_valid_parametros := v_valid_parametros || 'Nombre Inv�lido';
    end if;

    if v_valid_parametros is not null then
      o_rdo := 'Parametros : ' || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    --Si estan bien los datos, actualizo, si no hay registro lo agrege y se devuelve el valor insertado.
    update IPJ.T_TIPOS_ORIGEN
    set
      n_tipo_origen = p_n_tipo_origen
    where
      id_tipo_origen = p_id_tipo_origen;

    if sql%rowcount = 0 then
      select max(id_tipo_origen) +1 into o_id_tipo_origen
      from IPJ.T_TIPOS_ORIGEN;

      insert into IPJ.T_TIPOS_ORIGEN (id_tipo_origen, n_tipo_origen, id_estado_entidad)
      values (o_id_tipo_origen, p_n_tipo_origen, p_id_estado_entidad);

    else
      o_id_tipo_origen := p_id_tipo_origen;
    end if;

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     when OTHERS then
        o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Guardar_Origen;


  PROCEDURE SP_Guardar_Orden_Dia(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_tipo_orden_dia OUT number,
    p_id_tipo_orden_dia in number,
    p_n_tipo_orden_dia in varchar2,
    p_siglas in varchar2)
  IS
    v_valid_parametros varchar(2000);
  /**************************************************
    Guarda o modifica un Origen relaionado a un Estado de Entidades
  ***************************************************/
  BEGIN
    -- Valido que venga Descripcion
    if p_n_tipo_orden_dia is null then
      v_valid_parametros := v_valid_parametros || 'Nombre Inv�lido';
    end if;

    if v_valid_parametros is not null then
      o_rdo := 'Parametros : ' || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    --Si estan bien los datos, actualizo, si no hay registro lo agrege y se devuelve el valor insertado.
    update IPJ.T_TIPOS_ORDEN_DIA
    set
      n_tipo_orden_dia = p_n_tipo_orden_dia,
      siglas = p_siglas
    where
      id_tipo_orden_dia = p_id_tipo_orden_dia;

    if sql%rowcount = 0 then
      select max(id_tipo_orden_dia) +1 into o_id_tipo_orden_dia
      from IPJ.T_TIPOS_ORDEN_DIA;

      insert into IPJ.T_TIPOS_ORDEN_DIA (id_tipo_orden_dia, n_tipo_orden_dia, siglas)
      values (o_id_tipo_orden_dia, p_n_tipo_orden_dia, p_siglas);

    else
      o_id_tipo_orden_dia := p_id_tipo_orden_dia;
    end if;

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     when OTHERS then
        o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Guardar_Orden_Dia;

  PROCEDURE SP_Guardar_Archivos_Acta(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_archivo OUT number,
    p_id_archivo in number,
    p_n_archivo in varchar2,
    p_abreviacion in varchar2,
    p_id_tipo_acta in number)
  IS
    v_valid_parametros varchar(2000);
  /**************************************************
    Guarda o modifica un Origen relaionado a un Estado de Entidades
  ***************************************************/
  BEGIN
    -- Valido que venga Descripcion
    if p_n_archivo is null then
      v_valid_parametros := v_valid_parametros || 'Nombre Inv�lido';
    end if;

    if v_valid_parametros is not null then
      o_rdo := 'Parametros : ' || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    --Si estan bien los datos, actualizo, si no hay registro lo agrege y se devuelve el valor insertado.
    update IPJ.T_ARCHIVOS_ACTA
    set
      n_archivo = p_n_archivo
    where
      id_archivo = p_id_archivo;

    if sql%rowcount = 0 then
      select max(id_archivo) +1 into o_id_archivo
      from IPJ.T_ARCHIVOS_ACTA;

      insert into IPJ.T_ARCHIVOS_ACTA (id_archivo, n_archivo, abreviacion, id_tipo_acta)
      values (o_id_archivo, p_n_archivo, p_abreviacion, p_id_tipo_acta);

    else
      o_id_archivo := p_id_archivo;
    end if;

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     when OTHERS then
        o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Guardar_Archivos_Acta;

  PROCEDURE SP_Guardar_Tipos_Ins_Legal(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_il_tipo OUT number,
    p_il_tipo in number,
    p_n_il_tipo in varchar2)
  IS
  v_valid_parametros varchar(2000);
  /**************************************************
    Guarda o modifica un Estado de Entidades
  ***************************************************/
  BEGIN
    -- Valido que venga Descripcion
    if p_n_il_tipo is null then
      v_valid_parametros := v_valid_parametros || 'Nombre Inv�lido';
    end if;

    if v_valid_parametros is not null then
      o_rdo := 'Parametros : ' || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    --Si estan bien los datos, actualizo, si no hay registro lo agrege y se devuelve el valor insertado.
    update IPJ.T_TIPOS_INS_LEGAL
    set
      n_il_tipo = p_n_il_tipo
    where
      il_tipo = p_il_tipo;

    if sql%rowcount = 0 then
      select max(il_tipo) +1 into o_il_tipo
      from IPJ.T_TIPOS_INS_LEGAL;

      insert into IPJ.T_TIPOS_INS_LEGAL (il_tipo, n_il_tipo)
      values (o_il_tipo, p_n_il_tipo);

    else
      o_il_tipo := p_il_tipo;
    end if;

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     when OTHERS then
        o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Guardar_Tipos_Ins_Legal;

  PROCEDURE SP_Guardar_Juzgado(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_juzgado OUT number,
    p_id_juzgado in number,
    p_n_juzgado in varchar2,
    p_activo in varchar2)
  IS
  v_valid_parametros varchar(2000);
  /**************************************************
    Guarda o modifica un Estado de Entidades
  ***************************************************/
  BEGIN
    -- Valido que venga Descripcion
    if p_n_juzgado is null then
      v_valid_parametros := v_valid_parametros || 'Nombre Inv�lido';
    end if;

    if v_valid_parametros is not null then
      o_rdo := 'Parametros : ' || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    --Si estan bien los datos, actualizo, si no hay registro lo agrege y se devuelve el valor insertado.
    update IPJ.T_JUZGADOS
    set
      n_juzgado = p_n_juzgado,
      activo = p_Activo
    where
      id_juzgado = p_id_juzgado;

    if sql%rowcount = 0 then
      select max(id_juzgado) +1 into o_id_juzgado
      from IPJ.T_JUZGADOS;

      insert into IPJ.T_JUZGADOS (id_juzgado, n_juzgado, activo)
      values (o_id_juzgado, p_n_juzgado, nvl(p_activo, 'S'));

    else
      o_id_juzgado := p_id_juzgado;
    end if;

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     when OTHERS then
        o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Guardar_Juzgado;

  PROCEDURE SP_Guardar_Libros_Rubrica(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_tipo_libro OUT number,
    p_id_tipo_libro in number,
    p_tipo_libro in varchar2,
    p_mecanizado in varchar2)
  IS
  v_valid_parametros varchar(2000);
  /**************************************************
    Guarda o modifica un Estado de Entidades
  ***************************************************/
  BEGIN
    -- Valido que venga Descripcion
    if p_tipo_libro is null then
      v_valid_parametros := v_valid_parametros || 'Nombre Inv�lido';
    end if;

    if v_valid_parametros is not null then
      o_rdo := 'Parametros : ' || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    --Si estan bien los datos, actualizo, si no hay registro lo agrege y se devuelve el valor insertado.
    update IPJ.T_TIPOS_LIBROS
    set
      tipo_libro = p_tipo_libro,
      mecanizado = p_mecanizado
    where
      id_tipo_libro = p_id_tipo_libro;

    if sql%rowcount = 0 then
      select max(id_tipo_libro) +1 into o_id_tipo_libro
      from IPJ.T_TIPOS_LIBROS;

      insert into IPJ.T_TIPOS_LIBROS (id_tipo_libro, tipo_libro, mecanizado)
      values (o_id_tipo_libro, p_tipo_libro, nvl(p_mecanizado, 'N'));

    else
      o_id_tipo_libro := p_id_tipo_libro;
    end if;

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     when OTHERS then
        o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Guardar_Libros_Rubrica;

  PROCEDURE SP_Guardar_Tipo_Gravamen(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_tipo_gravamen OUT number,
    p_id_tipo_gravamen in number,
    p_n_tipo_gravamen in varchar2,
    p_descripcion in varchar2)
  IS
  v_valid_parametros varchar(2000);
  /**************************************************
    Guarda o modifica un Tipo de Gravamen
  ***************************************************/
  BEGIN
    -- Valido que venga Descripcion
    if p_n_tipo_gravamen is null then
      v_valid_parametros := v_valid_parametros || 'Nombre Inv�lido';
    end if;

    if v_valid_parametros is not null then
      o_rdo := 'Parametros : ' || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    --Si estan bien los datos, actualizo, si no hay registro lo agrege y se devuelve el valor insertado.
    update IPJ.T_TIPOS_GRAVAMENES
    set
      n_tipo_gravamen = p_n_tipo_gravamen,
      descripcion = p_descripcion
    where
      id_tipo_gravamen = p_id_tipo_gravamen;

    if sql%rowcount = 0 then
      select max(id_tipo_gravamen) +1 into o_id_tipo_gravamen
      from IPJ.T_TIPOS_GRAVAMENES;

      insert into IPJ.T_TIPOS_GRAVAMENES (id_tipo_gravamen, n_tipo_gravamen, descripcion)
      values (o_id_tipo_gravamen, p_n_tipo_gravamen, p_descripcion);

    else
      o_id_tipo_gravamen := p_id_tipo_gravamen;
    end if;

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     when OTHERS then
        o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Guardar_Tipo_Gravamen;


 PROCEDURE SP_Guardar_Tipo_Entidad(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_tipo_entidad OUT number,
    p_id_tipo_entidad in number,
    p_tipo_entidad in varchar2,
    p_sigla in varchar2,
    p_contrato in varchar2,
    p_id_ubicacion in number)
  IS
  v_valid_parametros varchar(2000);
  v_id_ubicacion number(5);
  /**************************************************
    Guarda o modifica un Tipo de Entidad
  ***************************************************/
  BEGIN
    -- Valido que venga Descripcion
    if p_tipo_entidad is null then
      v_valid_parametros := v_valid_parametros || 'Nombre Inv�lido';
    end if;
    if nvl(p_id_tipo_entidad, 0) < 0 and (p_sigla is null  or VALIDA_SIGLA_TIPO_ENTIDAD(p_sigla, p_id_tipo_entidad) > 0) then
      v_valid_parametros := v_valid_parametros || 'Siglas Inv�lida';
    end if;
    if p_contrato is null then
      v_valid_parametros := v_valid_parametros || 'Contrato Inv�lido';
    end if;

    if v_valid_parametros is not null then
      o_rdo := 'Parametros : ' || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    -- SI viene 0, guardo NULL por la FK
    if nvl(p_id_ubicacion, 0) = 0 then
      v_id_ubicacion := null;
    else
      v_id_ubicacion := p_id_ubicacion;
    end if;

    --Si estan bien los datos, actualizo, si no hay registro lo agrege y se devuelve el valor insertado.
    update IPJ.T_TIPOS_ENTIDADES
    set
      tipo_entidad = p_tipo_entidad,
      id_ubicacion = v_id_ubicacion
    where
      id_tipo_entidad = p_id_tipo_entidad;

    if sql%rowcount = 0 then
      select max(id_tipo_entidad) +1 into o_id_tipo_entidad
      from IPJ.T_TIPOS_ENTIDADES;

      insert into IPJ.T_TIPOS_ENTIDADES (id_tipo_entidad, tipo_entidad, sigla, contrato, id_ubicacion)
      values (o_id_tipo_entidad, p_tipo_entidad, p_sigla, p_contrato, v_id_ubicacion);

    else
      o_id_tipo_entidad := p_id_tipo_entidad;
    end if;

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     when OTHERS then
        o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Guardar_Tipo_Entidad;

  PROCEDURE SP_Guardar_Tipo_Clasif_IPJ(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_clasif_ipj OUT number,
    p_id_clasif_ipj in number,
    p_n_clasif_ipj in varchar2,
    p_id_ubicacion in number)
  IS
  v_valid_parametros varchar(2000);
  /**************************************************
    Guarda o modifica un Tipo de Clasificaciones IPJ (Tipos de Tramites)
  ***************************************************/
  BEGIN
    -- Valido que venga Descripcion y Ubicacion
    if nvl(p_id_ubicacion, 0) = 0 then
      v_valid_parametros := v_valid_parametros || 'Ubicaci�n Inv�lida';
    end if;
    if p_n_clasif_ipj is null then
      v_valid_parametros := v_valid_parametros || 'Nombre Inv�lido';
    end if;

    if v_valid_parametros is not null then
      o_rdo := 'Parametros : ' || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    --Si estan bien los datos, actualizo, si no hay registro lo agrege y se devuelve el valor insertado.
    update IPJ.T_TIPOS_CLASIF_IPJ
    set
      id_ubicacion = p_id_ubicacion,
      n_clasif_ipj = p_n_clasif_ipj
    where
      id_clasif_ipj = p_id_clasif_ipj;

    if sql%rowcount = 0 then
      select max(id_clasif_ipj) +1 into o_id_clasif_ipj
      from IPJ.T_TIPOS_CLASIF_IPJ;

      insert into IPJ.T_TIPOS_CLASIF_IPJ (id_clasif_ipj, n_clasif_ipj, id_ubicacion, id_tipo_persona)
      values (o_id_clasif_ipj, p_n_clasif_ipj, p_id_ubicacion, 1);

    else
      o_id_clasif_ipj := p_id_clasif_ipj;
    end if;

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     when OTHERS then
        o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Guardar_Tipo_Clasif_IPJ;

  PROCEDURE SP_Traer_Variables_Inf(
    o_Cursor OUT types.cursorType)
  IS
    /**************************************************
    Lista las variables definidas para los informes
  ***************************************************/
  BEGIN
    OPEN o_Cursor FOR
      select C.ID_CAMPO, C.N_CAMPO, C.TIPO, rownum ID
      from IPJ.t_tipos_campo c
      order by c.id_campo asc;

  END SP_Traer_Variables_Inf;

  PROCEDURE SP_Guardar_Variables_Inf(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_campo in varchar,
    p_n_campo in varchar2,
    p_tipo in varchar2)
  IS
  v_valid_parametros varchar(2000);
  /**************************************************
    Guarda o modifica un Tipo de Campo para Informes
  ***************************************************/
  BEGIN
    -- Valido que venga Descripcion y Ubicacion
    if p_id_campo is null then
      v_valid_parametros := v_valid_parametros || 'C�digo Inv�lido';
    end if;
    if p_tipo is null then
      v_valid_parametros := v_valid_parametros || 'Tipo Inv�lido';
    end if;
    if p_n_campo is null then
      v_valid_parametros := v_valid_parametros || 'Nombre Inv�lido';
    end if;

    if v_valid_parametros is not null then
      o_rdo := 'Parametros : ' || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    --Si estan bien los datos, actualizo, si no hay registro lo agrege y se devuelve el valor insertado.
    update IPJ.T_TIPOS_CAMPO
    set
      tipo = p_tipo,
      n_campo = p_n_campo
    where
      id_campo = p_id_campo;

    if sql%rowcount = 0 then
      insert into IPJ.T_TIPOS_CAMPO (id_campo, n_campo, tipo)
      values (p_id_campo, p_n_campo, p_tipo);

    end if;

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     when OTHERS then
        o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Guardar_Variables_Inf;

  PROCEDURE SP_Traer_Validacion_Inf(
    o_Cursor OUT types.cursorType)
  IS
    /**************************************************
    Lista las funciones de validaci�n definidas para los informes
  ***************************************************/
  BEGIN
    OPEN o_Cursor FOR
      select C.ID_VALIDACION, C.N_VALIDACION, C.OBSERVACION, rownum ID
      from IPJ.t_tipos_validacion c
      order by C.ID_VALIDACION asc;

  END SP_Traer_Validacion_Inf;

  PROCEDURE SP_Guardar_Validacion_Inf(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_validacion in varchar,
    p_n_validacion in varchar2,
    p_observacion in varchar2)
  IS
  v_valid_parametros varchar(2000);
  /**************************************************
    Guarda o modifica un Tipo de Validacion para Informes
  ***************************************************/
  BEGIN
    -- Valido que venga Descripcion y Ubicacion
    if p_id_validacion is null then
      v_valid_parametros := v_valid_parametros || 'C�digo Inv�lido';
    end if;
    if p_n_validacion is null then
      v_valid_parametros := v_valid_parametros || 'Nombre Inv�lido';
    end if;

    if v_valid_parametros is not null then
      o_rdo := 'Parametros : ' || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    --Si estan bien los datos, actualizo, si no hay registro lo agrege y se devuelve el valor insertado.
    update IPJ.T_TIPOS_VALIDACION
    set
      observacion = p_observacion,
      n_validacion = p_n_validacion
    where
      id_validacion = p_id_validacion;

    if sql%rowcount = 0 then
      insert into IPJ.T_TIPOS_VALIDACION (id_validacion, n_validacion, observacion)
      values (p_id_validacion, p_n_validacion, p_observacion);

    end if;

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     when OTHERS then
        o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Guardar_Validacion_Inf;

  PROCEDURE SP_Traer_Mens_Error(
    o_Cursor OUT types.cursorType)
  IS
    /**************************************************
    Lista los mensajes de error del sistema
  ***************************************************/
  BEGIN
    OPEN o_Cursor FOR
      select M.COMENTARIO, M.ID_MENSAJE_ERROR, M.N_MENSAJE_ERROR
      from IPJ.t_mensajes_error m;

  END SP_Traer_Mens_Error;

  PROCEDURE SP_Guardar_Mens_Error(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_mensaje_error out number,
    p_id_mensaje_error in number,
    p_n_mensaje_error in varchar2,
    p_comentario in varchar2)
  IS
  v_valid_parametros varchar(2000);
  /**************************************************
    Guarda o modifica un un mensaje de error
  ***************************************************/
  BEGIN
    -- Valido que venga Descripcion y Ubicacion
    if p_n_mensaje_error is null then
      v_valid_parametros := v_valid_parametros || 'Mensaje Inv�lido';
    end if;

    if v_valid_parametros is not null then
      o_rdo := 'Parametros : ' || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    --Si estan bien los datos, actualizo, si no hay registro lo agrege y se devuelve el valor insertado.
    update IPJ.T_MENSAJES_ERROR
    set
      comentario = p_comentario,
      N_MENSAJE_ERROR = p_n_mensaje_error
    where
      id_mensaje_error = p_id_mensaje_error;

    if sql%rowcount = 0 then
      select max(id_mensaje_error) +1 into o_id_mensaje_error
      from IPJ.T_MENSAJES_ERROR;

      insert into IPJ.T_MENSAJES_ERROR (id_mensaje_error, N_MENSAJE_ERROR, comentario)
      values (o_id_mensaje_error, p_n_mensaje_error, p_comentario);

    else
      o_id_mensaje_error := p_id_mensaje_error;
    end if;

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     when OTHERS then
        o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Guardar_Mens_Error;

  PROCEDURE SP_Traer_Tipo_Estado_Nota (
    o_Cursor OUT types.cursorType)
  IS
  /**************************************************
    Lista los Estados de las Notificaciones
  ***************************************************/
  BEGIN
    OPEN o_Cursor FOR
      select ID_ESTADO_NOTA, N_ESTADO_NOTA
      from IPJ.T_TIPOS_ESTADO_NOTA
      order by id_estado_nota;

  END SP_Traer_Tipo_Estado_Nota ;

   PROCEDURE SP_Guardar_Tipo_Estado_Nota(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_estado_nota OUT number,
    p_id_estado_nota in number,
    p_n_estado_nota in varchar2)
  IS
  v_valid_parametros varchar(2000);
  /**************************************************
    Guarda o modifica una pagina
  ***************************************************/
  BEGIN
    -- Valido que venga Descripcion
    if p_n_estado_nota is null then
      v_valid_parametros := v_valid_parametros || 'Nombre Inv�lido';
    end if;

    if v_valid_parametros is not null then
      o_rdo := 'Parametros : ' || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    --Si estan bien los datos, actualizo, si no hay registro lo agrege y se devuelve el valor insertado.
    update IPJ.T_TIPOS_ESTADO_NOTA
    set
      n_estado_nota = p_n_estado_nota
    where
      id_estado_nota = p_id_estado_nota;

    if sql%rowcount = 0 then
      select max(id_estado_nota) +1 into o_id_estado_nota
      from IPJ.T_TIPOS_ESTADO_NOTA;

      insert into IPJ.T_TIPOS_ESTADO_NOTA (id_estado_nota, n_estado_nota)
      values (o_id_estado_nota, p_n_estado_nota);

    else
      o_id_estado_nota := p_id_estado_nota;
    end if;

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     when OTHERS then
        o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Guardar_Tipo_Estado_Nota;


  PROCEDURE SP_Traer_Tipo_Grupos_Acciones (
    o_Cursor OUT types.cursorType)
   IS
  /**************************************************
    Lista los Grupos de Acciones definidos
  ***************************************************/
  BEGIN
    OPEN o_Cursor FOR
      select TG.ID_GRUPO_ACCION, TG.N_GRUPO_ACCION
      from IPJ.T_TIPOS_GRUPO_ACCION tg;

  END SP_Traer_Tipo_Grupos_Acciones;

  PROCEDURE SP_Informes (
    o_Cursor OUT types.cursorType)
  IS
  /**************************************************
    Lista los Informes Disponibles
  ***************************************************/
  BEGIN

    OPEN o_Cursor FOR
      select
        I.ID_INFORME, I.MARGEN_DER, I.MARGEN_INF, I.MARGEN_IZQ, I.MARGEN_SUP,
        I.N_INFORME, I.PAGINA, i.pie_pagina
      from IPJ.t_informes i
      order by i.id_informe asc;

  END SP_Informes ;

  PROCEDURE SP_Guardar_Informe (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_informe out number,
    p_id_informe in number,
    p_n_informe in varchar2,
    p_margen_sup in number,
    p_margen_inf in number,
    p_margen_izq in number,
    p_margen_der in number,
    p_pagina in varchar2,
    p_usa_cabecera in number
    )
  IS
  /**************************************************
    Actualiza los datos de un informe, o los agrega si no existe
  ***************************************************/
  BEGIN

    --Si estan bien los datos, actualizo, si no hay registro lo agrege y se devuelve el valor insertado.
    update IPJ.T_INFORMES
    set
      n_informe = p_n_informe,
      margen_sup = p_margen_sup,
      margen_inf = p_margen_inf,
      margen_izq = p_margen_izq,
      margen_der = p_margen_der,
      pagina = p_pagina,
      usa_cabecera = p_usa_cabecera
    where
      id_informe = p_id_informe;

    if sql%rowcount = 0 then
      select max(id_informe) +1 into o_id_informe
      from IPJ.T_INFORMES;

      insert into IPJ.T_INFORMES (id_informe, n_informe, margen_sup, margen_inf, margen_izq, margen_der, pagina, usa_cabecera)
      values (o_id_informe, p_n_informe, p_margen_sup, p_margen_inf, p_margen_izq, p_margen_der, p_pagina, nvl(p_usa_cabecera, 0));

    else
      o_id_informe := p_id_informe;
    end if;

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     when OTHERS then
        o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;

  END SP_Guardar_Informe ;

   PROCEDURE SP_Buscar_Lineas (
    o_Cursor OUT types.cursorType,
    p_Texto_si in varchar2,
    p_id_campo in varchar2)
  IS
  /**************************************************
    Lista las lineas que contengan el texto y el campo indicado
  ***************************************************/
  BEGIN

    OPEN o_Cursor FOR
      select
        L.CONCATENAR, L.ESPACIOS, L.ID_CAMPO, L.ID_LINEA, L.ID_VALIDACION,
        L.NEGRITA, L.TEXTO_NO, L.TEXTO_SI, L.TIPO_FECHA, L.TIPO_LINEA,
        TC.N_CAMPO, TV.N_VALIDACION
      from IPJ.t_lineas l left join ipj.t_tipos_campo tc
          on l.id_campo = tc.id_campo
        left join ipj.t_tipos_validacion tv
          on l.id_validacion = tv.id_validacion
      where
        (p_texto_si is null or upper(l.texto_si) like '%' || upper(p_texto_si) || '%' ) or
        (p_id_campo is null or upper(l.id_campo) = upper(p_id_campo))
      order by id_linea asc;

  END SP_Buscar_Lineas ;


  PROCEDURE SP_GUARDAR_INF_LINEAS (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_linea out number,
    p_id_informe in number,
    p_id_linea in number,
    p_id_campo in varchar2,
    p_texto_si in varchar2,
    p_negrita in varchar2,
    p_espacios in number,
    p_tipo_fecha in varchar2,
    p_id_validacion in varchar2,
    p_texto_no in varchar2,
    p_concatenar in varchar2,
    p_tipo_linea in varchar2,
    p_nro_orden in number,
    p_eliminar in number) -- 1 Elimina
  IS
    v_existe_linea number(5);
  /**************************************************
    Si la linea existe la actualiza, sino la crea.
    Luego agrega la lina al inform en el orden indicado.
  ***************************************************/
  BEGIN
    -- Si viene ID_LINEA, la actualizo, sino la agrego
    if nvl(p_id_linea, 0) > 0 then
      -- actualizo los datos de la linea
      o_id_linea := p_id_linea;

      update IPJ.t_lineas
      set
        id_campo = p_id_campo,
        texto_si = p_texto_si,
        negrita = p_negrita,
        espacios = nvl(p_espacios, 0),
        tipo_fecha = nvl(p_tipo_fecha, 'N'),
        id_validacion = p_id_validacion,
        texto_no = p_texto_no,
        concatenar = p_concatenar,
        tipo_linea = p_tipo_linea
      where
        id_linea = p_id_linea;
    else
      -- No tiene ID_LINE, creo la linea
      select max(id_linea) +1 into o_id_linea
      from IPJ.T_LINEAS;

      insert into IPJ.t_lineas (id_linea, id_campo, texto_si, negrita, espacios, tipo_fecha, id_validacion, texto_no, concatenar, tipo_linea)
      values (o_id_linea, p_id_campo, p_texto_si, p_negrita, nvl(p_espacios, 0), nvl(p_tipo_fecha, 'N'), p_id_validacion, p_texto_no, p_concatenar, p_tipo_linea);
    end if;

    -- Argego la nueva linea en las lineas del informe, si no existe.
    if nvl(p_id_linea, 0) > 0 then
      -- Si la linea existia, y ya esta cargada en el informe con ese orden, no hago nada.
      select count(*) into v_existe_linea
      from ipj.t_informes_linea il
      where
        il.id_informe = p_id_informe and
        il.id_linea = p_id_linea and
        il.nro_orden = p_nro_orden;

      if v_existe_linea = 0 then
        if p_eliminar <> 1 then
          insert into ipj.t_informes_linea (id_informe, id_linea, nro_orden)
          values (p_id_informe, o_id_linea, p_nro_orden);
        end if;
      else
        if p_eliminar = 1 then
          delete ipj.t_informes_linea
          where
            id_informe = p_id_informe and
            id_linea = p_id_linea and
            nro_orden = p_nro_orden;
        end if;
      end if;
    else
      -- es una linea nueva, la agrego al informe
      if p_eliminar <> 1 then
        insert into ipj.t_informes_linea (id_informe, id_linea, nro_orden)
        values (p_id_informe, o_id_linea, p_nro_orden);
      end if;
    end if;

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    commit;
  EXCEPTION
     when OTHERS then
        o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
        rollback;
  END SP_GUARDAR_INF_LINEAS ;

  PROCEDURE SP_Reindexar_Informe (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_informe in number)
  IS
    v_Cursor_Informe  types.cursorType;
    v_Col_Inf_Linea IPJ.T_INFORMES_LINEA%ROWTYPE;
    v_nro_linea number(5);
  /**************************************************
    Reordena las lineas de un informe de 10 en 10
  ***************************************************/
  BEGIN

    -- Armo un cursor con las lineas ordenas del informe
    OPEN v_Cursor_Informe FOR
      select *
      from IPJ.T_INFORMES_LINEA
      where id_informe = p_id_informe
      order by nro_orden;

    -- elimino las lineas del informe
    delete IPJ.T_INFORMES_LINEA where id_informe = p_id_informe;

    v_nro_linea := 1;
    LOOP
       fetch v_Cursor_Informe into v_Col_Inf_Linea;
       EXIT WHEN v_Cursor_Informe%NOTFOUND or v_Cursor_Informe%NOTFOUND is null;

       -- Agrego nuevamente las lineas con el nuevo orden
       insert into IPJ.T_INFORMES_LINEA  (id_informe, id_linea, nro_orden)
       values (v_Col_Inf_Linea.id_informe, v_Col_Inf_Linea.id_linea, v_nro_linea * 10);

      v_nro_linea := v_nro_linea + 1;
    end loop;
    CLOSE v_Cursor_Informe;

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    commit;

  EXCEPTION
     when OTHERS then
        o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
        Rollback;
  END SP_Reindexar_Informe ;

  PROCEDURE SP_Traer_Estados_Reserva(
    o_Cursor OUT types.cursorType)
  IS
    /**************************************************
    Lista los estados disponibles de una reserva de nombre
  ***************************************************/
  BEGIN
    OPEN o_Cursor FOR
      select er.id_estado_reserva, er.n_estado_reserva
      from IPJ.T_TIPOS_ESTADO_RESERVA er
      order by er.id_estado_reserva asc;

  END SP_Traer_Estados_Reserva;

  PROCEDURE SP_Traer_Acciones_Archivo(
    o_Cursor OUT types.cursorType)
  IS
    /**************************************************
    Lista las distintas tipos de acciones que utiliza archivo
  ***************************************************/
  BEGIN
    OPEN o_Cursor FOR
      select AA.ID_ACCION_ARCHIVO, AA.N_ACCION_ARCHIVO
      from IPJ.T_ACCIONES_ARCHIVO aa
      order by AA.ID_ACCION_ARCHIVO asc;

  END SP_Traer_Acciones_Archivo;

  PROCEDURE SP_Guardar_Acciones_Archivo(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_accion_archivo OUT number,
    p_id_accion_archivo in number,
    p_n_accion_archivo in varchar2)
  IS
  v_valid_parametros varchar(2000);
  /**************************************************
    Guarda o modifica un tipo de accion de archivo
  ***************************************************/
  BEGIN
    -- Valido que venga Descripcion
    if p_n_accion_archivo is null then
      v_valid_parametros := v_valid_parametros || 'Nombre Inv�lido';
    end if;

    if v_valid_parametros is not null then
      o_rdo := 'Parametros : ' || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    --Si estan bien los datos, actualizo, si no hay registro lo agrege y se devuelve el valor insertado.
    update IPJ.T_ACCIONES_ARCHIVO
    set
      n_accion_archivo = p_n_accion_archivo
    where
      id_accion_archivo = p_id_accion_archivo;

    if sql%rowcount = 0 then
      select max(id_accion_archivo) +1 into o_id_accion_archivo
      from IPJ.T_ACCIONES_ARCHIVO;

      insert into IPJ.T_ACCIONES_ARCHIVO (id_accion_archivo, n_accion_archivo)
      values (o_id_accion_archivo, p_n_accion_archivo);

    else
      o_id_accion_archivo := p_id_accion_archivo;
    end if;

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     when OTHERS then
        o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Guardar_Acciones_Archivo;

  PROCEDURE SP_Traer_Protocolo(
    o_Cursor OUT types.cursorType)
  IS
    /**************************************************
    Lista los distintos protocolos
  ***************************************************/
  BEGIN
    OPEN o_Cursor FOR
      select CP.ID_PROTOCOLO, CP.N_PROTOCOLO, CP.NUMERADOR, CP.SIGLA
      from IPJ.t_config_protocolo cp
      order by CP.ID_PROTOCOLO asc;

  END SP_Traer_Protocolo;

  PROCEDURE SP_Guardar_Protocolo(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_protocolo OUT number,
    p_id_protocolo in number,
    p_n_protocolo in varchar2,
    p_sigla in varchar2,
    p_numerador in number)
  IS
  v_valid_parametros varchar(2000);
  /**************************************************
    Guarda o modifica un protocolo
  ***************************************************/
  BEGIN
    -- Valido que venga Descripcion
    if p_n_protocolo is null then
      v_valid_parametros := v_valid_parametros || 'Nombre Inv�lido';
    end if;
    if p_sigla is null then
      v_valid_parametros := v_valid_parametros || 'Sigla Inv�lida';
    end if;

    if v_valid_parametros is not null then
      o_rdo := 'Parametros : ' || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    --Si estan bien los datos, actualizo, si no hay registro lo agrege y se devuelve el valor insertado.
    update IPJ.t_config_protocolo
    set
      n_protocolo = p_n_protocolo,
      numerador = p_numerador
    where
      id_protocolo = p_id_protocolo;

    if sql%rowcount = 0 then
      select max(id_protocolo) +1 into o_id_protocolo
      from IPJ.t_config_protocolo;

      insert into IPJ.t_config_protocolo (id_protocolo, n_protocolo, sigla, numerador)
      values (o_id_protocolo, p_n_protocolo, p_sigla, p_numerador);

    else
      o_id_protocolo := p_id_protocolo;
    end if;

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     when OTHERS then
        o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Guardar_Protocolo;

  PROCEDURE SP_Traer_Pagina(
    o_Cursor OUT types.cursorType)
  IS
    /**************************************************
    Lista las distintas paginas
  ***************************************************/
  BEGIN
    OPEN o_Cursor FOR
      select P.ID_PAGINA, P.N_PAGINA, p.url, p.es_externa
      from IPJ.t_paginas p
      order by P.ID_PAGINA asc;

  END SP_Traer_Pagina;

  PROCEDURE SP_Guardar_Pagina(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_pagina OUT number,
    p_id_pagina in number,
    p_n_pagina in varchar2)
  IS
  v_valid_parametros varchar(2000);
  /**************************************************
    Guarda o modifica una pagina
  ***************************************************/
  BEGIN
    -- Valido que venga Descripcion
    if p_n_pagina is null then
      v_valid_parametros := v_valid_parametros || 'Nombre Inv�lido';
    end if;

    if v_valid_parametros is not null then
      o_rdo := 'Parametros : ' || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    --Si estan bien los datos, actualizo, si no hay registro lo agrege y se devuelve el valor insertado.
    update IPJ.t_paginas
    set
      n_pagina = p_n_pagina
    where
      id_pagina = p_id_pagina;

    if sql%rowcount = 0 then
      select max(id_pagina) +1 into o_id_pagina
      from IPJ.t_paginas;

      insert into IPJ.t_paginas (id_pagina, n_pagina)
      values (o_id_pagina, p_n_pagina);

    else
      o_id_pagina := p_id_pagina;
    end if;

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     when OTHERS then
        o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Guardar_Pagina;

 PROCEDURE SP_Traer_Workflow(
    o_Cursor OUT types.cursorType)
  IS
    /**************************************************
    Trae el workflo definido
  ***************************************************/
  BEGIN
    OPEN o_Cursor FOR
      select
        W.ID_CLASIF_IPJ_ACTUAL, W.ID_CLASIF_IPJ_PROXIMA, W.ID_TIPO_ACCION_ACTUAL,
        W.ID_TIPO_ACCION_PROXIMA, W.ID_UBICACION_ORIGEN,
        U.N_UBICACION,
        (select C.N_CLASIF_IPJ from ipj.t_tipos_clasif_ipj c where C.ID_CLASIF_IPJ = W.ID_CLASIF_IPJ_PROXIMA) N_CLASIF_IPJ_PROXIMA,
        (select C.N_CLASIF_IPJ from ipj.t_tipos_clasif_ipj c where C.ID_CLASIF_IPJ = W.ID_CLASIF_IPJ_ACTUAL) N_CLASIF_IPJ_ACTUAL,
        (select u.n_ubicacion from ipj.t_ubicaciones u join IPJ.T_tipos_Clasif_ipj c on u.id_ubicacion = c.id_ubicacion
          where C.ID_CLASIF_IPJ = W.ID_CLASIF_IPJ_PROXIMA) N_UBICACION_DESTINO,
        (select u.n_ubicacion from ipj.t_ubicaciones u join IPJ.T_tipos_Clasif_ipj c on u.id_ubicacion = c.id_ubicacion
          where C.ID_CLASIF_IPJ = W.ID_CLASIF_IPJ_ACTUAL) N_UBICACION_ORIGEN
      from IPJ.t_workflow w join ipj.t_ubicaciones u
        on W.ID_UBICACION_ORIGEN = u.id_ubicacion
      order by W.ID_UBICACION_ORIGEN asc;

  END SP_Traer_Workflow;

  PROCEDURE SP_Guardar_Workflow(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_ubicacion_origen in number,
    p_id_clasif_ipj_actual in number,
    p_id_clasif_ipj_proxima in number,
    p_eliminar in number)
  IS
  v_valid_parametros varchar(2000);
  v_existe number(5);
  /**************************************************
    Guarda o elimina una linea de workflow
  ***************************************************/
  BEGIN
    -- Valido que venga Descripcion
    if nvl(p_id_ubicacion_origen, 0) = 0 then
      v_valid_parametros := v_valid_parametros || 'Area Inv�lida';
    end if;
    if nvl(p_id_clasif_ipj_actual, 0) = 0 then
      v_valid_parametros := v_valid_parametros || 'Tr�mite Actual Inv�lido';
    end if;
    if nvl(p_id_clasif_ipj_proxima, 0) = 0 then
      v_valid_parametros := v_valid_parametros || 'Tr�mite Pr�ximo Inv�lido';
    end if;

    if v_valid_parametros is not null then
      o_rdo := 'Parametros : ' || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    -- CUERPO DEL SP
    if p_eliminar = 1 then
      delete IPJ.t_workflow
      where
        id_ubicacion_origen = p_id_ubicacion_origen and
        id_clasif_ipj_actual = p_id_clasif_ipj_actual and
        id_clasif_ipj_proxima = p_id_clasif_ipj_proxima;
    else
      --Si ya existe no hago nada
      select count(*) into v_existe
      from IPJ.t_workflow
      where
        id_ubicacion_origen = p_id_ubicacion_origen and
        id_clasif_ipj_actual = p_id_clasif_ipj_actual and
        id_clasif_ipj_proxima = p_id_clasif_ipj_proxima;

      if v_existe = 0 then
        insert into IPJ.t_workflow (id_ubicacion_origen, id_clasif_ipj_actual, id_clasif_ipj_proxima, id_tipo_accion_actual, id_tipo_accion_proxima)
        values (p_id_ubicacion_origen, p_id_clasif_ipj_actual, p_id_clasif_ipj_proxima, 0, 0);

      end if;
    end if;

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     when OTHERS then
        o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Guardar_Workflow;

  PROCEDURE SP_Traer_Tipos_Documento(
    o_Cursor OUT types.cursorType)
  IS
    /**************************************************
    Trae los distintos tipos de documentos de RCivil
  ***************************************************/
  BEGIN
    OPEN o_Cursor FOR
      select id_tipo_documento, n_tipo_documento
      from
        ( select 1 id_tipo_documento, 'Documento Nacional de Identidad' n_tipo_documento from dual
           UNION
          select rownum+1 id_tipo_documento, n_tipo_documento
          from RCIVIL.VT_TIPOS_DOCUMENTOS
        )
      order by id_tipo_documento asc;

  END SP_Traer_Tipos_Documento;

  PROCEDURE SP_Guardar_Tipos_Grupo_Acc(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_grupo_accion OUT number,
    p_id_grupo_accion in number,
    p_n_grupo_accion in varchar2,
    p_eliminar in number) -- 1 elimina
  IS
  v_valid_parametros varchar(2000);
  /**************************************************
    Guarda o modifica  un grupo de acciones
  ***************************************************/
  BEGIN
    -- Valido que venga Descripcion
    if p_n_grupo_accion is null then
      v_valid_parametros := v_valid_parametros || 'Nombre Inv�lido';
    end if;

    if v_valid_parametros is not null then
      o_rdo := 'Parametros : ' || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    if p_eliminar = 1 then
      -- borro el detalle
      delete IPJ.T_GRUPOS_ACCIONES
      where
        id_grupo_accion = p_id_grupo_accion;

      -- borro la cabecera
      delete IPJ.T_TIPOS_GRUPO_ACCION
      where
        id_grupo_accion = p_id_grupo_accion;

        o_id_grupo_accion := 0;
    else
      --Si estan bien los datos, actualizo, si no hay registro lo agrege y se devuelve el valor insertado.
      update IPJ.T_TIPOS_GRUPO_ACCION
      set
        n_grupo_accion = p_n_grupo_accion
      where
        id_grupo_accion = p_id_grupo_accion;

      if sql%rowcount = 0 then
        select max(id_grupo_accion) +1 into o_id_grupo_accion
        from IPJ.T_TIPOS_GRUPO_ACCION;

        insert into IPJ.T_TIPOS_GRUPO_ACCION (id_grupo_accion, n_grupo_accion)
        values (o_id_grupo_accion, p_n_grupo_accion);

      else
        o_id_grupo_accion := p_id_grupo_accion;
      end if;
    end if;
    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     when OTHERS then
        o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Guardar_Tipos_Grupo_Acc;

 PROCEDURE SP_Traer_Grupo_Acc_Det(
    o_Cursor OUT types.cursorType,
    p_id_grupo_accion in number)
  IS
  /**************************************************
    Lista las accciones asociadas a un grupo determinado
  ***************************************************/
  BEGIN
    OPEN o_Cursor FOR
      select gacc.id_grupo_Accion, gacc.id_tipo_accion, tt.n_tipo_accion
      from ipj.t_grupos_acciones gacc join ipj.t_tipos_accionesipj tt
        on gacc.id_tipo_accion = tt.id_tipo_accion
      where
        gacc.id_grupo_accion = p_id_grupo_accion;

  END SP_Traer_Grupo_Acc_Det;

  PROCEDURE SP_Guardar_Grupo_Acc_Det(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_grupo_accion in number,
    p_id_tipo_accion in number,
    p_eliminar in number) -- 1 elimina
  IS
    v_existe number(5);
  /**************************************************
    Guarda o elimina una accion del grupo
  ***************************************************/
  BEGIN

    if p_eliminar  = 1 then
      delete ipj.t_grupos_acciones
      where
        id_grupo_accion = p_id_grupo_accion and
        id_tipo_accion = p_id_tipo_accion;
    else
      --Si ya existe, no se hace nada
      select count(*) into v_existe
      from IPJ.t_grupos_acciones
      where
        id_grupo_accion = p_id_grupo_accion and
        id_tipo_accion = p_id_tipo_Accion;

      if v_existe = 0 then
        insert into IPJ.t_grupos_acciones (id_grupo_accion, id_tipo_accion)
        values (p_id_grupo_accion, p_id_tipo_accion);
      end if;
    end if;
    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     when OTHERS then
        o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Guardar_Grupo_Acc_Det;


 PROCEDURE SP_Traer_Tipos_Bloque(
    o_Cursor OUT types.cursorType)
  IS
    /**************************************************
    Trae los distintos tipos de bloques de las entidades
  ***************************************************/
  BEGIN
    OPEN o_Cursor FOR
      select id_bloque_entidad, n_bloque_entidad
      from IPJ.T_TIPOS_BLOQUE_ENTIDAD
      order by n_bloque_entidad asc;

  END SP_Traer_Tipos_Bloque;

  PROCEDURE SP_Traer_Bloque_Accion(
    o_Cursor OUT types.cursorType,
    p_id_tipo_accion in number)
  IS
    v_codigo_habilitacion number;
    /**************************************************
    Trae los distintos tipos de bloques de las entidades asociados a un tipo de accion
  ***************************************************/
  BEGIN
    begin
      select codigo_habilitacion into v_codigo_habilitacion
      from IPJ.T_TIPOS_accionesipj
      where
        id_tipo_accion = p_id_tipo_accion;
    exception
       when NO_DATA_FOUND then
         v_codigo_habilitacion := 0;
    end;

    OPEN o_Cursor FOR
      select id_bloque_entidad, n_bloque_entidad
      from T_TIPOS_BLOQUE_ENTIDAD
      where
        bitand(id_bloque_entidad, v_codigo_habilitacion) = id_bloque_entidad;
  END SP_Traer_Bloque_Accion;

  PROCEDURE SP_Traer_Tipo_Pers(
    o_Cursor OUT types.cursorType)
  IS
    /**************************************************
    Lista los tipos de personas que pueden utilizar las acciones
  ***************************************************/
  BEGIN

    OPEN o_Cursor FOR
      select id_tipo_persona, n_tipo_persona
      from IPJ.T_TIPOS_CLASIF_PERSONAS
      order by n_tipo_persona;
  END SP_Traer_Tipo_Pers;

  PROCEDURE Sp_Traer_Tipos_Accion_Id(
    o_Cursor OUT types.cursorType,
    p_id_tipo_accion in number
    )
  IS
  v_existe number;
  /**************************************************
    Lista los Tipos de Tr�mites disponibles
  ***************************************************/
  BEGIN
    select count(*) into v_existe
    from ipj.t_tipos_accionesipj
    where
      id_tipo_Accion = p_id_tipo_accion;

    -- Si no existe devuelvo un registro vacio
    if v_existe = 0 then
      OPEN o_Cursor FOR
        select
          null id_tipo_accion, '' n_tipo_accion, null id_conf_accion, 0 id_tipo_persona, null id_protocolo,
          null id_clasif_ipj, null id_pagina, null id_accion_archivo, 'N' informar_boletin, 0 tiempo_demora,
          0 tiempo_demora_urg, 0 codigo_habilitacion,
          '' n_tipo_persona, '' n_clasif_ipj, '' n_protocolo, '' n_pagina
        from dual;
    else
      OPEN o_Cursor FOR
        select
          tt.id_tipo_accion, tt.n_tipo_accion, tt.id_conf_accion, tt.id_tipo_persona, tt.id_protocolo,
          tt.id_clasif_ipj, tt.id_pagina, tt.id_accion_archivo, tt.informar_boletin, tt.tiempo_demora,
          tt.tiempo_demora_urg, tt.codigo_habilitacion,
          tp.n_tipo_persona, tc.n_clasif_ipj, cp.n_protocolo, p.n_pagina,
          u.id_ubicacion, u.n_ubicacion,I.Id_Informe,I.N_Informe
        from ipj.t_tipos_accionesipj tt left join IPJ.T_TIPOS_CLASIF_PERSONAS tp
            on tt.id_tipo_persona = tp.id_tipo_persona
          join IPJ.T_TIPOS_CLASIF_IPJ tc
            on tt.id_clasif_ipj = tc.id_clasif_ipj
          left join ipj.t_acciones_archivo aa
            on tt.id_accion_archivo = aa.id_accion_archivo
          left join ipj.t_config_protocolo cp
            on tt.id_protocolo = cp.id_protocolo
          join ipj.t_paginas p
            on tt.id_pagina = p.id_pagina
          join ipj.t_ubicaciones u
            on u.id_ubicacion = tc.id_ubicacion
          left join IPJ.T_INFORMES_TRAMITE it
              on Tt.Id_Tipo_Accion = It.Id_Tipo_Accion
          left Join Ipj.T_Informes I
              On It.Id_Informe = I.Id_Informe
        where
          tt.id_tipo_Accion = p_id_tipo_accion
        order by tt.n_tipo_accion;

    end if;

  END Sp_Traer_Tipos_Accion_Id;

  PROCEDURE SP_Traer_Tasas(
    o_Cursor OUT types.cursorType)
  IS
    /**************************************************
    Lista los tipos de tasas existentes
  ***************************************************/
  BEGIN
    OPEN o_Cursor FOR
      -- solo para testing, no existe la vista
      -- select 1 id_tasa, 'Tasa 1' n_tasa from dual; -- esto es proque en testing no existe la vista
      select *
      from
        ( select Id_Concepto, Fecha_Desde, N_Concepto, Precio_Base, Cantidad_Base, Precio_Extra,
            Item, Id_Concepto_Padre, Fec_Desde_Padre, Id_Unidad, to_char(Fec_Desde, 'dd/mm/rrrr') fec_Desde, Observaciones,
            Cantidad_Desde, Cantidad_Hasta, Fecha_Hasta, '032' Cod_Ente, Lia_Impuesto, Lia_Articulo,
             Lia_Concepto, Aplicable, Requiere_Cantidad, Requiere_Importe
          from TASAS_SERVICIO.VT_CONCEPTOS_GENERALES
          UNION
          select Id_Concepto, Fecha_Desde, N_Concepto, Precio_Base, Cantidad_Base, Precio_Extra,
            Item, Id_Concepto_Padre, Fec_Desde_Padre, Id_Unidad, to_char(Fec_Desde, 'dd/mm/rrrr') fec_Desde, Observaciones,
            Cantidad_Desde, Cantidad_Hasta, Fecha_Hasta, Cod_Ente, Lia_Impuesto, Lia_Articulo,
            Lia_Concepto, Aplicable, Requiere_Cantidad, Requiere_Importe
          from TASAS_SERVICIO.VT_CONCEPTOS_MJ
          where
            cod_ente = '032' -- es IPJ
         ) tmp
       order by cod_ente, id_concepto;
  END SP_Traer_Tasas;

  PROCEDURE SP_Traer_Acciones_Tasas(
    o_Cursor OUT types.cursorType,
    p_id_tipo_accion in number)
  IS
    /**************************************************
    Lista los tipos de tasas asociadas a una accion
  ***************************************************/
  BEGIN

    OPEN o_Cursor FOR
      select at.id_tipo_accion, at.id_tasa, at.importe, 'Tasa ' ||  to_char(at.id_tasa) N_Tasa
      from IPJ.T_ACCIONES_TASAS at
      where
        id_tipo_accion = p_id_tipo_accion;
  END SP_Traer_Acciones_Tasas;

  PROCEDURE Guardar_Acciones_Tasas(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tipo_Accion in number,
    p_id_tasa in number,
    p_importe in number,
    p_eliminar in number) -- 1 elimina
  IS
    v_existe number(5);
  /**************************************************
    Guarda o elimina una accion del grupo
  ***************************************************/
  BEGIN

    if p_eliminar  = 1 then
      delete ipj.T_ACCIONES_TASAS
      where
        id_tipo_Accion = p_id_tipo_accion and
        id_tasa = p_id_tasa;
    else
      --Si ya existe, no se hace nada
      select count(*) into v_existe
      from IPJ.T_ACCIONES_TASAS
      where
        id_tipo_accion = p_id_tipo_accion and
        id_tasa = p_id_tasa;

      if v_existe = 0 then
        insert into IPJ.T_ACCIONES_TASAS (id_tipo_accion, id_tasa, importe)
        values (p_id_tipo_accion, p_id_tasa, p_importe);
      end if;
    end if;
    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     when OTHERS then
        o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END Guardar_Acciones_Tasas;

  PROCEDURE SP_Guardar_Tipo_Accion (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_tipo_Accion out number,
    p_id_tipo_Accion in number,
    p_n_tipo_accion in varchar2,
    p_id_conf_accion in number,
    p_id_tipo_persona in number,
    p_id_protocolo in number,
    p_id_clasif_ipj in number,
    p_id_pagina in number,
    p_id_accion_archivo in number,
    p_informar_boletin in varchar2,
    p_tiempo_demora in varchar2,
    p_tiempo_demora_urg in varchar2,
    p_codigo_habilitacion in number,
    p_id_informe number)
  IS
    v_valid_parametros varchar2(200);
    v_bloque varchar2(100);
    v_id_protocolo number;
    v_id_accion_archivo  number;
    v_id_conf_accion number;
    v_codigo_habilitacion number;
    v_id_tipo_persona number;
    v_id_informe number;
  /**************************************************
    Actualiza o agrega un tipo de accion
  ***************************************************/
  BEGIN
    v_bloque := 'GUARDAR TIPO ACCION - Parametros : ';
    if p_n_tipo_accion is null then
      v_valid_parametros := v_valid_parametros || 'Nombre Inv�lido';
    end if;
    if nvl(p_id_clasif_ipj, 0) = 0 then
      v_valid_parametros := v_valid_parametros || 'Tipo de Tr�mite Inv�lido';
    end if;
    if nvl(p_id_pagina, 0) = 0 then
      v_valid_parametros := v_valid_parametros || 'Tipo de P�gina Inv�lida';
    end if;
    if p_tiempo_demora is not null and nvl(IPJ.VARIOS.ToNumber(p_tiempo_demora), -1) < 0 then
      v_valid_parametros := v_valid_parametros || 'Tiempo de Demora Inv�lido';
    end if;
    if p_tiempo_demora_urg is not null and nvl(IPJ.VARIOS.ToNumber(p_tiempo_demora_urg), -1) < 0 then
      v_valid_parametros := v_valid_parametros || 'Tiempo de Demora Urgente Inv�lido';
    end if;

    -- Si los parametros no estan OK, no continuo
    if v_valid_parametros is not null then
      o_rdo := v_bloque || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    -- Si vienen en 0 porngo NULL para respetar la FK
    v_id_protocolo := (case when p_id_protocolo = 0 then null else p_id_protocolo end);
    v_id_accion_archivo := (case when p_id_accion_archivo = 0 then null else p_id_accion_archivo end);
    v_id_conf_accion := (case when p_id_conf_accion = 0 then null else p_id_conf_accion end);
    v_codigo_habilitacion := (case when p_codigo_habilitacion = 0 then null else p_codigo_habilitacion end);
    v_id_tipo_persona := (case when p_id_tipo_persona = 0 then null else p_id_tipo_persona end);
    v_id_informe := (case when p_id_informe = 0 then null else p_id_informe end);

    v_bloque := 'GUARDAR TIPO ACCION - Actualizo Datos ';
    o_id_tipo_accion := p_id_tipo_accion;
    --actualizo, si no hay nada se agrega
    update IPJ.t_tipos_AccionesIPJ
    set
      n_tipo_accion = p_n_tipo_accion,
      id_conf_accion = v_id_conf_accion,
      id_tipo_persona = v_id_tipo_persona,
      id_protocolo = v_id_protocolo,
      id_clasif_ipj = p_id_clasif_ipj,
      id_pagina = p_id_pagina,
      id_accion_archivo = v_id_accion_archivo,
      informar_boletin = p_informar_boletin,
      tiempo_demora = IPJ.VARIOS.ToNumber(nvl(p_tiempo_demora, '0')),
      tiempo_demora_urg = IPJ.VARIOS.ToNumber(nvl(p_tiempo_demora_urg, '0')),
      codigo_habilitacion = v_codigo_habilitacion
    where
      id_tipo_accion = p_id_tipo_accion;

    if sql%rowcount = 0 then
      select max(id_tipo_accion) +1 into o_id_tipo_accion
      from IPJ.t_tipos_AccionesIPJ;


      insert into IPJ.t_tipos_AccionesIPJ (id_tipo_accion, n_tipo_accion, id_conf_accion, id_tipo_persona,
        id_protocolo, id_clasif_ipj, id_pagina, id_accion_archivo, informar_boletin,
        tiempo_demora, tiempo_demora_urg, codigo_habilitacion)
      values (o_id_tipo_accion, p_n_tipo_accion, v_id_conf_accion, v_id_tipo_persona,
        v_id_protocolo, p_id_clasif_ipj, p_id_pagina, v_id_accion_archivo, nvl(p_informar_boletin, 'N'),
        IPJ.VARIOS.ToNumber(nvl(p_tiempo_demora, '0')), IPJ.VARIOS.ToNumber(nvl(p_tiempo_demora_urg, '0')), v_codigo_habilitacion);
    else
        o_id_tipo_accion := p_id_tipo_accion;
    end if;

    v_bloque := 'GUARDAR INFORME ASOCIADO - Actualizo datos';
    --Agregado de campo informe en el alta de acciones
    --Si id_informe viene null, debo desasociar el informe de la accion
    if v_id_informe is null then
        delete from ipj.t_informes_tramite where id_tipo_accion = o_id_tipo_accion;
    else
        --Si no es null actualizo o inserto segun sea el caso
        update IPJ.T_INFORMES_TRAMITE  T_INFORMES
        set id_informe = v_id_informe
        where id_tipo_accion = o_id_tipo_accion;

     if sql%rowcount = 0 then
              insert into ipj.t_informes_tramite (id_informe,id_tipo_accion)
              values(v_id_informe,o_id_tipo_accion);
        end if;

    end if;


    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     when OTHERS then
        o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Guardar_Tipo_Accion ;

  PROCEDURE Sp_Traer_Areas(
    o_Cursor OUT types.cursorType,
    p_ubicacion in number)
  IS
  BEGIN
  /**************************************************
    Lista las Ubicaciones o Areas de IPJ
  ***************************************************/
    OPEN o_Cursor FOR
      select u.id_ubicacion, u.n_ubicacion, U.OBSERVACION, U.ORDEN,U.CUIL_USUARIO,U.ID_GRUPO,U.CODIGO_SUAC
      from  IPJ.t_ubicaciones u
      where
        nvl(p_ubicacion, 0) = 0 or u.id_ubicacion = p_ubicacion;

  END Sp_Traer_Areas;

  PROCEDURE Sp_Traer_Tipos_Dictamen(
    o_Cursor OUT types.cursorType)
  IS
  BEGIN
  /**************************************************
    Lista los tipos de dictamenes definidos
  ***************************************************/
    OPEN o_Cursor FOR
      select id_tipo_dictamen, n_tipo_dictamen
      from  ipj.t_tipos_dictamen;

  END Sp_Traer_Tipos_Dictamen;

  PROCEDURE Sp_Traer_Tipos_Veedor(
    o_Cursor OUT types.cursorType)
  IS
  BEGIN
  /**************************************************
    Lista las Ubicaciones o Areas de IPJ
  ***************************************************/
    OPEN o_Cursor FOR
      select id_rol_veedor, n_rol_veedor
      from IPJ.T_TIPOS_VEEDOR
      order by n_rol_veedor;

  END Sp_Traer_Tipos_Veedor;

  PROCEDURE SP_Traer_Datos(
    o_Dato OUT varchar2,
    p_dato_solicitado in number)
  IS
    v_Cursor types.cursorType;
    v_Col_Notif IPJ.T_NOTIFICACION%ROWTYPE;
  BEGIN
  /**************************************************
    Lista las Ubicaciones o Areas de IPJ
  ***************************************************/
    o_Dato := null;

    OPEN v_Cursor FOR
      select *
      from IPJ.T_NOTIFICACION n
      where
        n.ID_TIPO_NOTIFICACION = p_dato_solicitado;

    LOOP
       fetch v_Cursor into v_Col_Notif;
       EXIT WHEN v_Cursor%NOTFOUND or v_Cursor%NOTFOUND is null;

       if p_dato_solicitado = 9 then -- Pie de Informe de Notificaciones SxA
         if o_Dato is null then
           o_dato := nvl(v_Col_Notif.n_notificacion, ' ');
         else
           o_dato := o_dato || '<br/>' || nvl(v_Col_Notif.n_notificacion, ' ');
         end if;
       end if;

    end loop;
    CLOSE v_Cursor;

  END SP_Traer_Datos;

  PROCEDURE Sp_Guardar_Tipos_Veedor(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_rol_veedor OUT number,
    p_id_rol_veedor in number,
    p_n_rol_veedor in varchar2)
  IS
  v_valid_parametros varchar(2000);
  /**************************************************
    Guarda o modifica un Tipo de Veedor / Interventor
  ***************************************************/
  BEGIN
    -- Valido que venga Descripcion
    if p_n_rol_veedor is null then
      v_valid_parametros := v_valid_parametros || 'Nombre Inv�lido';
    end if;

    if v_valid_parametros is not null then
      o_rdo := 'Parametros : ' || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    --Si estan bien los datos, actualizo, si no hay registro lo agrege y se devuelve el valor insertado.
    update IPJ.T_TIPOS_VEEDOR
    set
      n_rol_veedor = p_n_rol_veedor
    where
      id_rol_veedor = p_id_rol_veedor;

    if sql%rowcount = 0 then
      select max(id_rol_veedor) +1 into o_id_rol_veedor
      from IPJ.T_TIPOS_VEEDOR;

      insert into IPJ.T_TIPOS_VEEDOR (id_rol_veedor, n_rol_veedor)
      values (o_id_rol_veedor, p_n_rol_veedor);

    else
      o_id_rol_veedor := p_id_rol_veedor;
    end if;

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     when OTHERS then
        o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END Sp_Guardar_Tipos_Veedor;

  PROCEDURE SP_Buscar_Ayuda (
    o_Cursor OUT types.cursorType,
    p_id_ayuda in number)
  IS
  /**************************************************
    Lista la ayuda para el �ndice definido
  ***************************************************/
  BEGIN

    OPEN o_Cursor FOR
      select  id_ayuda, n_ayuda, descripcion, texto1, imagen1, texto2, imagen2,
         texto3, imagen3, texto4, imagen4, texto5, imagen5, texto6, imagen6,
         texto7, imagen7, texto8, imagen8, a.id_ubicacion,
         U.N_UBICACION
      from IPJ.T_Ayuda a left join IPJ.T_ubicaciones u
        on a.id_ubicacion = u.id_ubicacion
      where
        nvl(p_id_ayuda, 0) = 0 or id_ayuda = p_id_ayuda
      order by n_ayuda;

  END SP_Buscar_Ayuda ;

  PROCEDURE SP_Guardar_Ayuda (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_ayuda in number,
    p_n_ayuda in varchar2,
    p_descripcion in varchar2,
    p_texto1 in varchar2,
    p_imagen1 in varchar2,
    p_texto2 in varchar2,
    p_imagen2 in varchar2,
    p_texto3 in varchar2,
    p_imagen3 in varchar2,
    p_texto4 in varchar2,
    p_imagen4 in varchar2,
    p_texto5 in varchar2,
    p_imagen5 in varchar2,
    p_texto6 in varchar2,
    p_imagen6 in varchar2,
    p_texto7 in varchar2,
    p_imagen7 in varchar2,
    p_texto8 in varchar2,
    p_imagen8 in varchar2,
    p_id_ubicacion in number)
  IS
    v_id_ubicacion number;
  /**************************************************
    Lista la ayuda para el �ndice definido
  ***************************************************/
  BEGIN
    if IPJ.Types.c_habilitar_log_SP = 'S' then
      IPJ.VARIOS.SP_GUARDAR_LOG(
        p_METODO => 'SP_Guardar_Ayuda',
        p_NIVEL => 'Gestion',
        p_ORIGEN => 'DB',
        p_USR_LOG  => 'DB',
        p_USR_WINDOWS => 'Sistema',
        p_IP_PC  => null,
        p_EXCEPCION =>
           'Id Ayuda = ' || to_char(p_id_ayuda)
           || ' / Nombre Ayuda = ' || p_n_ayuda
           || ' / Descripcion = ' || p_descripcion
           || ' / Texto 1 = ' || p_texto1
           || ' / Imagen 1 = ' || p_imagen1
           || ' / Texto 2 = ' || p_texto2
           || ' / Imagen 2 = ' || p_imagen2
           || ' / Texto 3 = ' || p_texto3
           || ' / Imagen 3 = ' || p_imagen3
           || ' / Texto 4 = ' || p_texto4
           || ' / Imagen 4 = ' || p_imagen4
           || ' / Texto 5 = ' || p_texto5
           || ' / Imagen 5 = ' || p_imagen5
           || ' / Texto 6 = ' || p_texto6
           || ' / Imagen 6 = ' || p_imagen6
           || ' / Texto 7 = ' || p_texto7
           || ' / Imagen 7 = ' || p_imagen7
           || ' / Texto 8 = ' || p_texto8
           || ' / Imagen 8 = ' || p_imagen8
           || ' / Id Ubicacion = ' || to_char(p_id_ubicacion)
      );
    end if;
    -- Paso el 0 a null por las FK
    v_id_ubicacion := (case when p_id_ubicacion = 0 then null else p_id_ubicacion end);

    update IPJ.T_AYUDA
    set
      n_ayuda = p_n_ayuda,
      descripcion = p_descripcion,
      texto1 = p_texto1,
      imagen1 = p_imagen1,
      texto2 = p_texto2,
      imagen2 = p_imagen2,
      texto3 = p_texto3,
      imagen3 = p_imagen3,
      texto4 = p_texto4,
      imagen4 = p_imagen4,
      texto5 = p_texto5,
      imagen5 = p_imagen5,
      texto6 = p_texto6,
      imagen6 = p_imagen6,
      texto7 = p_texto7,
      imagen7 = p_imagen7,
      texto8 = p_texto8,
      imagen8 = p_imagen8,
      id_ubicacion = p_id_ubicacion
    where
      id_ayuda = p_id_ayuda;

    if sql%rowcount = 0 then
      insert into IPJ.T_AYUDA (id_ayuda, n_ayuda, descripcion, texto1, imagen1,
        texto2, imagen2, texto3, imagen3, texto4, imagen4, texto5, imagen5,
        texto6, imagen6, texto7, imagen7, texto8, imagen8, id_ubicacion)
      values
        (p_id_ayuda, p_n_ayuda, p_descripcion, p_texto1, p_imagen1,
        p_texto2, p_imagen2, p_texto3, p_imagen3, p_texto4, p_imagen4, p_texto5, p_imagen5,
        p_texto6, p_imagen6, p_texto7, p_imagen7, p_texto8, p_imagen8, p_id_ubicacion);
    end if;

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     when OTHERS then
        o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;

  END SP_Guardar_Ayuda ;

  PROCEDURE SP_Traer_Tipos_Socio(
    o_Cursor OUT types.cursorType)
  IS
    /**************************************************
    Lista los tipos de socios existentes
  ***************************************************/
  BEGIN

    OPEN o_Cursor FOR
      select id_tipo_socio, n_tipo_socio
      from ipj.t_tipos_socio
      where
        id_tipo_socio <= 2 -- Para mostrar solo los 2 de SRL
      order by n_tipo_socio;

  END SP_Traer_Tipos_Socio;

  PROCEDURE SP_Traer_Rubros (
    o_Cursor OUT types.cursorType)
  IS
  /**************************************************
    Lista los Rubros habilitados en Grobierno
  ***************************************************/
  BEGIN
    OPEN o_Cursor FOR
      select ID_RUBRO, initcap(N_RUBRO) N_RUBRO
      from T_COMUNES.VT_RUBROS
      where fecha_fin_rubro is null
      order by n_rubro;

  END SP_Traer_Rubros ;

  PROCEDURE SP_Traer_Tipos_Activ (
    o_Cursor OUT types.cursorType,
    p_id_rubro in varchar2)
  IS
  /**************************************************
    Lista los Tipos de Actividades asociados a un Rubro de Grobierno
  ***************************************************/
  BEGIN
    OPEN o_Cursor FOR
      select ID_RUBRO, ID_TIPO_ACTIVIDAD, initcap(N_TIPO_ACTIVIDAD) N_TIPO_ACTIVIDAD
      from T_COMUNES.VT_TIPOS_ACTIVIDAD
      where id_rubro = p_id_rubro
      order by N_TIPO_ACTIVIDAD;

  END SP_Traer_Tipos_Activ ;

  PROCEDURE SP_Traer_Actividades (
    o_Cursor OUT types.cursorType,
    p_id_rubro in varchar2,
    p_id_tipo_actividad in varchar2)
  IS
  /**************************************************
    Lista los Tipos de Actividades asociados a un Rubro de Grobierno
  ***************************************************/
  BEGIN
    OPEN o_Cursor FOR
      select ID_RUBRO, ID_TIPO_ACTIVIDAD, ID_ACTIVIDAD, initcap(N_ACTIVIDAD) N_ACTIVIDAD
      from T_COMUNES.VT_ACTIVIDADES
      where
        fecha_fin_act is null and
        id_afip is not null and
        id_rubro = p_id_rubro and
        id_tipo_actividad = p_id_tipo_actividad
      order by N_ACTIVIDAD;

  END SP_Traer_Actividades ;

  PROCEDURE Sp_Traer_Provincias(
    o_Cursor OUT types.cursorType,
    p_id_departamento in number,
    p_id_localidad in number)
  IS
  BEGIN
  /**************************************************
    Lista las provincias argentinas
    Si no se pasan departamentos o localidades, se listan todas las provincias
  ***************************************************/
    OPEN o_Cursor FOR
      select distinct p.id_provincia, p.n_provincia, p.id_pais
      from DOM_MANAGER.VT_PROVINCIAS p join DOM_MANAGER.VT_LOCALIDADES_TODAS L
          on l.id_provincia = p.id_provincia
      where
        id_pais = 'ARG' and
        (nvl(p_id_departamento, 0) = 0 or l.id_departamento = p_id_departamento) and
        (nvl(p_id_localidad, 0) = 0 or l.id_localidad = p_id_localidad)
      order by n_provincia;

  END Sp_Traer_Provincias;

  PROCEDURE Sp_Traer_Depto_Arg(
    o_Cursor OUT types.cursorType,
    p_id_provincia in varchar2,
    p_id_localidad in number)
  IS
  BEGIN
  /**************************************************
    Lista los departametos de la provincia elegida
    Si la localidad es 0, muestra todos los departamentos.
  ***************************************************/
    OPEN o_Cursor FOR
      select distinct d.id_departamento, d.n_departamento, d.id_provincia
      from  DOM_MANAGER.VT_DEPARTAMENTOS_TODOS D join DOM_MANAGER.VT_LOCALIDADES_TODAS l
          on  d.id_departamento = l.id_departamento
      where
        (p_id_provincia is null or d.id_provincia = p_id_provincia) and
        (nvl(p_id_localidad, 0) = 0 or l.id_localidad = p_id_localidad)
      order by n_departamento;

  END Sp_Traer_Depto_Arg;

  PROCEDURE Sp_Traer_Local_Arg(
    o_Cursor OUT types.cursorType,
    p_id_provincia in varchar2,
    p_id_departamento in number)
  IS
  BEGIN
  /**************************************************
    Lista los las localidades de la provincia elegida.
    Si el departamento es 0, muestra todas las localidades
  ***************************************************/
    OPEN o_Cursor FOR
      select distinct l.id_localidad, l.n_localidad, l.id_departamento, l.id_provincia
      from  DOM_MANAGER.VT_DEPARTAMENTOS_TODOS D join DOM_MANAGER.VT_LOCALIDADES_TODAS l
          on  d.id_departamento = l.id_departamento
      where
        (p_id_provincia is null or l.id_provincia = p_id_provincia) and
        (nvl(p_id_departamento, 0) = 0 or l.id_departamento = p_id_departamento) and
        n_localidad <> 'SIN ASIGNAR'
      order by n_localidad;

  END Sp_Traer_Local_Arg;

  PROCEDURE Sp_Traer_Departamentos(
    o_Cursor OUT types.cursorType,
    p_id_localidad in number)
  IS
  BEGIN
  /**************************************************
    Lista los departametos asociados a la localidad.
    Si la localidad es 0, muestra todos los departamentos.
  ***************************************************/
    OPEN o_Cursor FOR
      select distinct d.id_departamento, d.n_departamento
      from  DOM_MANAGER.VT_DEPARTAMENTOS_CBA D join DOM_MANAGER.VT_LOCALIDADES l
          on  d.id_departamento = l.id_departamento
      where
        l.id_provincia = 'X' and
        nvl(p_id_localidad, 0) = 0 or l.id_localidad = p_id_localidad
      order by n_departamento;

  END Sp_Traer_Departamentos;

  PROCEDURE Sp_Traer_Localidades(
    o_Cursor OUT types.cursorType,
    p_id_departamento in number)
  IS
  BEGIN
  /**************************************************
    Lista los departametos asociados a la localidad.
    Si la localidad es 0, muestra todos los departamentos.
  ***************************************************/
    OPEN o_Cursor FOR
      select distinct l.id_localidad, l.n_localidad, l.id_departamento
      from  DOM_MANAGER.VT_DEPARTAMENTOS_CBA D join DOM_MANAGER.VT_LOCALIDADES l
          on  d.id_departamento = l.id_departamento
      where
        l.id_provincia = 'X' and
        nvl(p_id_departamento, 0) = 0 or l.id_departamento = p_id_departamento and
        n_localidad <> 'SIN ASIGNAR'
      order by n_localidad;

  END Sp_Traer_Localidades;

  PROCEDURE Sp_Traer_Formas_Accion(
    o_Cursor OUT types.cursorType)
  IS
  BEGIN
  /**************************************************
    Lista las distitas formas que pueden tener las acciones de una emrpesa.
  ***************************************************/
    OPEN o_Cursor FOR
      select FA.ID_FORMA_ACCION, FA.N_FORMA_ACCION
      from  IPJ.T_TIPOS_FORMAS_ACCION fa;

  END Sp_Traer_Formas_Accion;

  PROCEDURE Sp_Traer_Tipos_Accion(
    o_Cursor OUT types.cursorType)
  IS
  BEGIN
  /**************************************************
    Lista los distintos tipos de acciones  que pueden tener las acciones de una emrpesa.
  ***************************************************/
    OPEN o_Cursor FOR
      select TA.ID_ACCION, TA.N_ACCION
      from  IPJ.T_TIPOS_ACCIONES ta;

  END Sp_Traer_Tipos_Accion;

  PROCEDURE Sp_Traer_Tipos_Capital(
    o_Cursor OUT types.cursorType)
  IS
  BEGIN
  /**************************************************
    Lista los distintos tipos de capital para la integracion de los socios
  ***************************************************/
    OPEN o_Cursor FOR
      select TC.ID_CAPITAL, TC.N_CAPITAL
      from  IPJ.T_TIPOS_CAPITAL tc;

  END Sp_Traer_Tipos_Capital;

  PROCEDURE Sp_Traer_Tipos_Modo_Part(
    o_Cursor OUT types.cursorType)
  IS
  BEGIN
  /**************************************************
    Lista los modo de participacion de los socios
  ***************************************************/
    OPEN o_Cursor FOR
      select MP.ID_MODO_PART, MP.N_MODO_PART
      from  IPJ.T_TIPOS_MODO_PARTICIPACION mp ;

  END Sp_Traer_Tipos_Modo_Part;

  PROCEDURE Sp_Traer_Persona_RCivil(
    o_Cursor OUT types.cursorType,
    p_nro_documento in varchar2,
    p_clave in varchar2)
  IS
   /**********************************************************
    Busca una persona en Registro Civil por DNI, y devuelve la lista de personas con sus campos claves.
  **********************************************************/
    v_id_sexo varchar2(2);
    v_nro_documento varchar2(20);
    v_pai_cod_pais varchar2(5);
    v_id_numero number;
  BEGIN
    -- Descompongo la clave si viene
    if p_clave is not null then
      select
        substr(p_clave, 1, 2) id_sexo,
        substr(p_clave, length(substr(p_clave, 1, 2))+1, INSTR(p_clave, trim(translate(p_clave, '0987654321', '          ')))-length(substr(p_clave, 1, 2))-1) nro_documento,
        trim(translate(p_clave, '0987654321', '          ')) pai_cod_pais,
        to_number(substr(p_clave, INSTR(p_clave, trim(translate(p_clave, '0987654321', '          '))) + length(trim(translate(p_clave, '0987654321', '          '))), 2)) id_Numero
        into v_id_sexo, v_nro_documento, v_pai_cod_pais, v_id_numero
      from dual;
    end if;

    -- Busca por DNI o Clave
    if p_clave is null then
      OPEN o_Cursor FOR
        select p.id_sexo, p.nro_documento,  p.pai_cod_pais, p.id_numero, p.NOV_NOMBRE Nombre, p.NOV_APELLIDO Apellido,
          p.NOV_NOMBRE || ' ' ||p.NOV_APELLIDO Detalle,
          p.id_sexo || p.nro_documento || p.pai_cod_pais || to_char(p.id_numero) Clave
        from rcivil.vt_pk_persona  p
        where
          p.id_tipo_documento <> 'PSP' and -- No busca Pasaportes (PBI 8595)
          (p_clave is null and p.nro_documento = to_char(to_number(p_nro_documento)));

    else
      OPEN o_Cursor FOR
        select p.id_sexo, p.nro_documento,  p.pai_cod_pais, p.id_numero, p.NOV_NOMBRE Nombre, p.NOV_APELLIDO Apellido,
          p.NOV_NOMBRE || ' ' ||p.NOV_APELLIDO Detalle,
          p.id_sexo || p.nro_documento || p.pai_cod_pais || to_char(p.id_numero) Clave
        from rcivil.vt_pk_persona  p
        where
          p.id_tipo_documento <> 'PSP' and -- No busca Pasaportes (PBI 8595)
          (p_clave is not null and
            p.id_sexo = v_id_sexo and
            p.nro_documento = v_nro_documento and
            p.pai_cod_pais = v_pai_cod_pais and
            p.id_numero = v_id_numero
          );
    end if;

  END Sp_Traer_Persona_RCivil;

  PROCEDURE Sp_Traer_Datos_Persona_RCivil(
    o_Cursor OUT types.cursorType,
    p_nro_documento in varchar2,
    p_id_sexo in varchar2,
    p_pai_cod_pais in varchar2,
    p_id_numero in number)
  IS
  /**********************************************************
    Busca una persona en Registro Civil por DNI, y devuelve la lista de personas con sus campos claves.
  **********************************************************/
    v_row_dom DOM_MANAGER.VT_DOMICILIOS_COND%ROWTYPE;
    v_id_vin number;
  BEGIN
    -- Busco el domicilio real de la persona
    v_id_vin := IPJ.VARIOS.FC_Buscar_Dom_Persona(p_id_sexo, p_nro_documento, p_pai_cod_pais, p_id_numero, 3);

    begin
      select * into v_row_dom
      from DOM_MANAGER.VT_DOMICILIOS_COND
      where
        id_vin = v_id_vin;
    exception
      when NO_DATA_FOUND then
        v_row_dom := null;
    end;

    OPEN o_Cursor FOR
      select C.Id_Sexo, C.Nro_Documento, C.Pai_Cod_Pais, C.Id_Numero,
        C.Nov_Apellido, C.Nov_Nombre, C.Cuil, to_char(C.Fecha_Nacimiento, 'dd/mm/rrrr') Fecha_Nacimiento,
        C.Id_Tipo_Documento,
        (case
           when i.id_estado_civil is not null then I.Id_Estado_Civil
           else C.Id_Estado_Civil
        end) Id_Estado_Civil,
        C.Pai_Cod_Pais_Nacionalidad,
        v_row_dom.Id_Vin id_vin_real, v_row_dom.Id_TipoDom Id_TipoDom, v_row_dom.N_Tipodom N_Tipodom,
        v_row_dom.Id_TipoCalle Id_TipoCalle, v_row_dom.N_TipoCalle N_TipoCalle,
        v_row_dom.Id_Calle Id_Calle, v_row_dom.N_Calle N_Calle, v_row_dom.Altura Altura, v_row_dom.Piso Piso,
        v_row_dom.Depto Depto, v_row_dom.Torre Torre, v_row_dom.Id_Barrio Id_Barrio, v_row_dom.n_barrio n_barrio,
        v_row_dom.mzna Manzana, v_row_dom.lote Lote, v_row_dom.Id_Precinto Id_Precinto,
        v_row_dom.N_Precinto N_Precinto, v_row_dom.Id_Localidad Id_Localidad, v_row_dom.N_Localidad N_Localidad,
        v_row_dom.CPA CPA, v_row_dom.Id_Departamento Id_Departamento, v_row_dom.N_Departamento N_Departamento,
        v_row_dom.Id_Provincia Id_Provincia, v_row_dom.N_Provincia N_Provincia, v_row_dom.km Km,
        C.Nov_Nombre || ' ' || C.Nov_Apellido Detalle,
        IPJ.VARIOS.FC_Buscar_Mail(c.Id_Sexo || c.Nro_Documento || c.Pai_Cod_Pais || to_char(c.Id_Numero), IPJ.Types.c_id_Aplicacion) EMail,
        IPJ.VARIOS.FC_Buscar_Telefono(c.Id_Sexo || c.Nro_Documento || c.Pai_Cod_Pais || to_char(c.Id_Numero), IPJ.Types.c_id_Aplicacion) Telefono,
        IPJ.VARIOS.FC_Buscar_id_Profesion(c.id_sexo, c.nro_documento, c.pai_cod_pais, c.id_numero) id_profesion,
        IPJ.VARIOS.FC_Buscar_Profesion(c.id_sexo, c.nro_documento, c.pai_cod_pais, c.id_numero) n_profesion,
        IPJ.VARIOS.FC_Buscar_Tipo_Profesion(c.id_sexo, c.nro_documento, c.pai_cod_pais, c.id_numero) id_tipo_caracteristica,
        i.id_integrante,
        (case
           when i.id_estado_civil is not null then (select n_estado_civil from RCIVIL.VT_ESTADOS_CIVIL where id_estado_civil = i.id_estado_civil)
           else EC.N_ESTADO_CIVIL
        end) N_ESTADO_CIVIL,
        decode(pais.id_pais, 'DES', 'Argentina', initCap(pais.pai_nacionalidad)) pais_nacionalidad,
        decode(c.pai_cod_pais, 'DES', 'Argentina', (select initCap(n_pais) from DOM_MANAGER.VT_PAISES p where p.id_pais = c.pai_cod_pais )) N_NACIONALIDAD,
        S.TIPO_SEXO N_Sexo, TD.N_TIPO_DOCUMENTO,
        c.id_sexo || c.nro_documento || c.pai_cod_pais || to_char(c.id_numero) Clave,
        c.NOV_APELLIDO Apellido, c.NOV_nombre Nombre
      from RCIVIL.VT_PERSONAS_CUIL c left join RCIVIL.VT_ESTADOS_CIVIL ec
          on C.ID_ESTADO_CIVIL = EC.ID_ESTADO_CIVIL
        join RCIVIL.VT_PAISES pais
          on PAIS.ID_PAIS = C.PAI_COD_PAIS_NACIONALIDAD
        join T_COMUNES.VT_SEXOS s
          on C.ID_SEXO = S.ID_SEXO
        join RCIVIL.VT_TIPOS_DOCUMENTOS td
          on TD.ID_TIPO_DOCUMENTO = C.ID_TIPO_DOCUMENTO and TD.COD_PAIS_ORIGEN = C.PAI_COD_PAIS_TD
        left join IPJ.T_Integrantes i
          on I.ID_SEXO = C.ID_SEXO and
            i.nro_documento = c.nro_documento and
            i.pai_cod_pais = c.pai_cod_pais and
            i.id_numero = c.id_numero
      where
        c.id_sexo = p_id_sexo and
        C.NRO_DOCUMENTO = p_nro_documento and
        c.pai_cod_pais = p_pai_cod_pais and
        c.id_numero = p_id_numero and
        rownum = 1 --si hay varios, tomo el �timo
      order by id_vin_real desc;

  END Sp_Traer_Datos_Persona_RCivil;


  PROCEDURE SP_Traer_Tipo_Organis_Fiscal (
    o_Cursor OUT types.cursorType,
    p_id_ubicacion in number)
  IS
  /**************************************************
    Lista los organismos de Fiscalizaci�n de un area indicada
  ***************************************************/
  BEGIN

    OPEN o_Cursor FOR
      select T.ID_TIPO_ORGANISMO, T.N_TIPO_ORGANISMO, t.id_ubicacion,
        u.n_ubicacion
      from IPJ.T_TIPOS_ORGANISMO t join ipj.t_ubicaciones u
          on t.id_ubicacion = u.id_ubicacion
      where
        t.id_ubicacion = p_id_ubicacion and
        T.ES_ADMIN = 'N';

  END SP_Traer_Tipo_Organis_Fiscal;

  PROCEDURE SP_Traer_Tipo_Organismos_Admin  (
    o_Cursor OUT types.cursorType,
    p_id_ubicacion in number)
  IS
  /**************************************************
    Lista los organismos de Administraci�n de un area indicada
  ***************************************************/
  BEGIN

    OPEN o_Cursor FOR
      select T.ID_TIPO_ORGANISMO, T.N_TIPO_ORGANISMO, t.id_ubicacion,
        u.n_ubicacion
      from IPJ.T_TIPOS_ORGANISMO t join ipj.t_ubicaciones u
          on t.id_ubicacion = u.id_ubicacion
      where
        t.id_ubicacion = p_id_ubicacion and
        T.ES_ADMIN = 'S';

  END SP_Traer_Tipo_Organismos_Admin;

  PROCEDURE SP_Guardar_Telefono_Mail(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_mail in varchar2,
    p_telefono in varchar2,
    p_id_sexo in varchar2,
    p_nro_documento in varchar2,
    p_pai_cod_pais in varchar2,
    p_id_numero in number,
    p_cuit_empresa in varchar2,
    p_sede_empresa in varchar2,
    p_tramite in varchar2,
    p_te_caract in varchar2)
  IS
  /**************************************************
    Guarda un mail o telefono asociado a una persona o empresa.
    La entidad relacionada se maneja de la siguiente manera:
    - Personas: Se concatena sexo, DNI, Pais y numero; y no se pasa id aplicaci�n.
    - Empresas: se concatena cuit y sede; y no se ppasa id aplicaci�n.
    - Otros: se pasa un id_entidad que lo identifique y el id aplicaci�n.
  ***************************************************/
    v_valid_parametros varchar(2000);
    v_id_entidad varchar2(100);
    v_id_aplicacion number;
    v_id_comunicacion varchar2(5);
    v_origen_tabla varchar2(50);
    v_existe number;
  BEGIN
    -- Valido que venga Telefono o mail
    if p_mail is null and p_telefono is null then
      v_valid_parametros := v_valid_parametros || 'Datos Inv�lidos';
    end if;

    if v_valid_parametros is not null then
      o_rdo := 'Parametros : ' || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    -- ARMO LA ENTIDAD, SEGUN LOS DATOS INGRESADOS
    if p_tramite is not null then
      v_id_entidad := p_tramite;
      v_id_aplicacion := IPJ.TYPES.C_ID_APLICACION;
      v_origen_tabla := 'IPJ.T_TRAMITESIPJ';
    else
      if p_cuit_empresa is not null then
        v_id_entidad := trim(p_cuit_empresa) || p_sede_empresa;
        v_id_aplicacion := IPJ.TYPES.C_ID_APLICACION;
        v_origen_tabla := 'IPJ.T_ENTIDADES';
      else
        v_id_entidad := p_id_sexo || p_nro_documento || p_pai_cod_pais || to_char(p_id_numero);
        v_id_aplicacion := IPJ.TYPES.C_ID_APLICACION;
        v_origen_tabla := 'IPJ.T_INTEGRANTES';
      end if;
    end if;

    -- Depende que guardo, elijo el c�digo
    if p_mail is not null then
      v_id_comunicacion := IPJ.TYPES.c_comunic_mail; -- CORREO ELECTRONICO
    else
      v_id_comunicacion := IPJ.TYPES.c_comunic_te; -- TELEFONO PRINCIPAL
    end if;

    -- Verifico si ya existe o no
    select count(*) into v_existe
    from T_COMUNES.VT_COMUNICACIONES
    where
      id_entidad = v_id_entidad and
      id_tipo_comunicacion = v_id_comunicacion and
      (v_id_aplicacion is null or id_aplicacion = v_id_aplicacion);

    -- no existe una comunicacion, la agrega; si ya existe la modifica
    if v_existe = 0 then
      T_COMUNES.PACK_COMUNICACIONES.INSERTA_COMUNICACIONES_VERT(
        P_ID_TIPO_COMUNICACION => v_id_comunicacion,
        P_ENTIDAD => v_id_entidad,
        P_NRO_MAIL => trim(nvl(p_mail, p_telefono)),
        P_COD_AREA => p_te_caract,
        P_ID_APLICACION => v_id_aplicacion,
        P_ORIGEN_TABLA => v_origen_tabla,
        O_RESULTADO => o_rdo);
    else
      T_COMUNES.PACK_COMUNICACIONES.MODIFICA_COMUNICACIONES_VERT(
        P_ID_TIPO_COMUNICACION => v_id_comunicacion,
        P_ENTIDAD => v_id_entidad,
        P_NRO_MAIL => trim(nvl(p_mail, p_telefono)),
        P_COD_AREA => p_te_caract,
        P_ID_APLICACION => v_id_aplicacion,
        P_ORIGEN_TABLA => v_origen_tabla,
        O_RESULTADO => o_rdo);
    end if;


    if o_rdo like '%' || IPJ.TYPES.C_RESP_OK || '%' then
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    else
      if p_mail is null then
        o_rdo := replace(o_rdo, 'LA COLUMNA NRO_MAIL', 'TELEFONO');
      else
        o_rdo := replace(o_rdo, 'LA COLUMNA NRO_MAIL', 'E-MAIL');
      end if;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
    end if;
  EXCEPTION
     when OTHERS then
        o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Guardar_Telefono_Mail;

  PROCEDURE Sp_Traer_Profesiones(
    o_Cursor OUT types.cursorType)
  IS
  BEGIN
  /**************************************************
    Lista las profesiones definidas en Gobierno
  ***************************************************/
    OPEN o_Cursor FOR
      /*select id_caracteristica id_profesion, initcap(n_caracteristica) n_profesion, id_tipo_caracteristica
      from RCIVIL.VT_CARACTERISTICAS
      where
        id_tipo_caracteristica in ('2021', '2009') and -- Profesiones y Oficios
        trim(upper(n_caracteristica)) <> 'OTRO'
      order by n_caracteristica asc;*/

     SELECT id_caracteristica id_profesion,
            REPLACE(initcap(n_caracteristica)
                    ,substr(n_caracteristica, instr(n_caracteristica, '/'), 2)
                    ,lower(substr(n_caracteristica, instr(n_caracteristica, '/'), 2))) AS n_profesion,
            id_tipo_caracteristica
       FROM rcivil.vt_caracteristicas
      WHERE id_tipo_caracteristica IN ('2021', '2009')
        AND -- Profesiones y Oficios
            TRIM(upper(n_caracteristica)) <> 'OTRO'
      ORDER BY n_caracteristica ASC;

  END Sp_Traer_Profesiones;

  PROCEDURE Sp_Traer_Profesiones_Sindico(
    o_Cursor OUT types.cursorType)
  IS
  BEGIN
  /**************************************************
    Lista las profesiones definidas en Gobierno
  ***************************************************/
    OPEN o_Cursor FOR
      select id_caracteristica id_profesion, initcap(n_caracteristica) n_profesion, id_tipo_caracteristica
      from RCIVIL.VT_CARACTERISTICAS
      where
        id_tipo_caracteristica = IPJ.TYPES.c_caract_profesion and
        id_caracteristica in ('001', '007')
      order by n_caracteristica asc;

  END Sp_Traer_Profesiones_Sindico;

  PROCEDURE SP_Buscar_Profesion_RCivil(
    o_id_caracteristica out varchar2,
    o_n_caracteristica out varchar2,
    o_id_tipo_caracteristica out varchar2,
    p_id_sexo in varchar2,
    p_nro_documento in varchar2,
    p_pai_cod_pais in varchar2,
    p_id_numero in varchar2)
  IS
  /**************************************************
    Busca la profesion asociada a una persona f�sica
  ***************************************************/
    v_result varchar2(50);
  BEGIN
    begin
      select id_caracteristica, n_caracteristica, id_tipo_caracteristica into o_id_caracteristica, o_n_caracteristica, o_id_tipo_caracteristica
      from
        ( select *
          from RCIVIL.VT_CARACTERISTICA_PERS
          where
            id_tipo_caracteristica in ('2021', '2009') and -- Profesiones y Oficios IPJ.TYPES.c_caract_profesion and
            id_sexo = p_id_sexo and
            nro_documento = p_nro_documento and
            pai_cod_pais = p_pai_cod_pais and
            id_numero = p_id_numero
          order by fec_inicio desc
        ) t
       where rownum = 1;
    exception
      when NO_DATA_FOUND then
        o_id_caracteristica := null;
        o_n_caracteristica := null;
        o_id_tipo_caracteristica := null;
    end;

  Exception
    When Others Then
      o_id_caracteristica := null;
      o_n_caracteristica := null;
      o_id_tipo_caracteristica := null;
  End SP_Buscar_Profesion_RCivil;

  PROCEDURE SP_Guardar_Profesion_RCivil(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_profesion in varchar2,
    p_id_tipo_caracteristica in varchar2,
    p_id_sexo in varchar2,
    p_nro_documento in varchar2,
    p_pai_cod_pais in varchar2,
    p_id_numero in number)
  IS
  /**************************************************
    Guarda la profesion asociada a una persona f�sica.
  ***************************************************/
    v_valid_parametros varchar(2000);
    v_existe number;
    v_id_caracteristica varchar2(10);
    v_id_tipo_caracteristica varchar2(10);
    v_profesion varchar2(10);
    v_oficio varchar2(10);
  BEGIN
    -- Valido que venga Telefono o mail
    if nvl(p_id_profesion, '0') = '0' then
      v_valid_parametros := v_valid_parametros || 'Profesi�n Inv�lida. ';
    end if;
    if nvl(p_id_tipo_caracteristica, '0') = '0' then
      v_valid_parametros := v_valid_parametros || 'Caracter�stica Inv�lida. ';
    end if;

    if v_valid_parametros is not null then
      o_rdo := 'Parametros : ' || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    -- Busco si tiene registrado profesion
    begin
      select id_caracteristica into v_profesion
      from RCIVIL.VT_CARACTERISTICA_PERS
      where
        id_tipo_caracteristica in (IPJ.TYPES.c_caract_profesion) and -- Profesiones
        id_sexo = p_id_sexo and
        nro_documento = p_nro_documento and
        pai_cod_pais = p_pai_cod_pais and
        id_numero = p_id_numero;
    exception
      when NO_DATA_FOUND then
        v_profesion := null;
    end;

    -- Busco si tiene registrado oficio
    begin
      select id_caracteristica into v_oficio
      from RCIVIL.VT_CARACTERISTICA_PERS
      where
        id_tipo_caracteristica in ('2009') and -- Oficios
        id_sexo = p_id_sexo and
        nro_documento = p_nro_documento and
        pai_cod_pais = p_pai_cod_pais and
        id_numero = p_id_numero;
    exception
      when NO_DATA_FOUND then
        v_oficio := null;
    end;

    -- no existe profesi�n, la agrega
    if (p_id_tipo_caracteristica = '2021' and v_profesion is null) or (p_id_tipo_caracteristica = '2009' and v_oficio is null) then
      RCIVIL.PACK_PERSONA.INSERT_CARACT_PERSONA(
        P_ID_APLICACION => IPJ.TYPES.C_ID_APLICACION ,
        P_ID_SEXO => p_id_sexo,
        P_NRO_DOCUMENTO => p_nro_documento,
        P_PAI_COD_PAIS => p_pai_cod_pais,
        P_ID_NUMERO => p_id_numero,
        P_ID_TIPO_CARACTERISTICA => p_id_tipo_caracteristica,
        P_ID_CARACTERISTICA => p_id_profesion,
        P_COMENTARIO => NULL,
        P_FECHA_INICIO => NULL,
        O_ID_RETORNO => o_tipo_mensaje,
        O_RETORNO => o_rdo);

    else
      if (p_id_tipo_caracteristica = '2021' and v_profesion <> p_id_profesion) or
         (p_id_tipo_caracteristica = '2009' and v_oficio <> p_id_profesion) then
        RCIVIL.PACK_PERSONA.MODIFICA_CARACT_PERSONA(
          P_ID_APLICACION => IPJ.TYPES.C_ID_APLICACION ,
          P_ID_SEXO => p_id_sexo,
          P_NRO_DOCUMENTO => p_nro_documento,
          P_PAI_COD_PAIS => p_pai_cod_pais,
          P_ID_NUMERO => p_id_numero,
          P_ID_TIPO_CARACTERISTICA => p_id_tipo_caracteristica,
          P_NUEVA_CARACTERISTICA => p_id_profesion,
          P_COMENTARIO => NULL,
          O_ID_RETORNO => o_tipo_mensaje,
          O_RETORNO => o_rdo);
      else
        o_rdo := IPJ.TYPES.C_RESP_OK;
      end if;
    end if;

    if o_rdo like '%'|| IPJ.TYPES.C_RESP_OK ||'%' then
      o_rdo := IPJ.TYPES.C_RESP_OK;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    else
      o_rdo := o_rdo || ' (' || to_char(o_tipo_mensaje) || ')';
      o_tipo_mensaje := IPJ.TYPES.c_Tipo_Mens_ERROR;
    end if;
  EXCEPTION
     when OTHERS then
        o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Guardar_Profesion_RCivil;

  PROCEDURE SP_Guardar_CUIL_RCivil(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_cuil in varchar2,
    p_id_sexo in varchar2,
    p_nro_documento in varchar2,
    p_pai_cod_pais in varchar2,
    p_id_numero in number)
  IS
  /**************************************************
    Guarda el cuil asociado a una persona f�sica.
  ***************************************************/
    v_valid_parametros varchar(2000);
    v_existe number;
    v_cuil varchar2(12);
  BEGIN

    if v_valid_parametros is not null then
      o_rdo := 'Parametros : ' || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    -- Verifico si ya existe o no
    select cuil into v_cuil
    from RCIVIL.VT_PERSONAS_CUIL
    where
      id_sexo = p_id_sexo and
      nro_documento = p_nro_documento and
      pai_cod_pais = p_pai_cod_pais and
      id_numero = p_id_numero;

    -- no existe CUIL, la agrega
    if v_cuil is null then
      RCIVIL.PR_INSERTA_CUIL (
        p_cuil => p_cuil,
        p_id_aplicacion => IPJ.TYPES.C_Id_Aplicacion,
        p_id_sexo  => p_id_sexo,
        p_nro_documento => p_nro_documento,
        p_pais => p_pai_cod_pais,
        p_id_numero => p_id_numero,
        o_mensaje => o_rdo
      );

      /*
      RCIVIL.PACK_PERSONA.PR_INSERTA_CUIL(
        p_cuil => p_cuil,
        p_id_aplicacion => IPJ.TYPES.C_Id_Aplicacion ,
        p_id_sexo => p_id_sexo,
        p_nro_documento => p_nro_documento,
        p_pais => p_pai_cod_pais,
        p_id_numero => p_id_numero,
        o_mensaje => o_rdo);
      */
      v_cuil := p_cuil;
    else
      if replace(p_cuil, '-', '') = replace(v_cuil, '-', '') then
        o_rdo := IPJ.TYPES.C_RESP_OK;
      else
        o_rdo := 'CUIL no permitido, actuamente posee cuil ' || v_cuil || '.';
      end if;
    end if;

    if nvl(o_rdo, IPJ.TYPES.C_RESP_OK) like '%'|| IPJ.TYPES.C_RESP_OK ||'%' then
      -- Pongo el CUIL en el integrante
      update ipj.t_integrantes
      set cuil = v_cuil
      where
        id_sexo = p_id_sexo and
        nro_documento = p_nro_documento and
        pai_cod_pais = p_pai_cod_pais and
        id_numero = p_id_numero;

      o_rdo := IPJ.TYPES.C_RESP_OK;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    else
      --o_rdo := 'CUIL inv�lido o no permitido.';
      o_tipo_mensaje := IPJ.TYPES.c_Tipo_Mens_ERROR;
    end if;
  EXCEPTION
     when OTHERS then
        o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Guardar_CUIL_RCivil;

  PROCEDURE Sp_Traer_Tipos_Duracion(
    o_Cursor OUT types.cursorType)
  IS
  BEGIN
  /**************************************************
    Lista los tipos de duracion para los organismos
  ***************************************************/
    OPEN o_Cursor FOR
      select Id_Tipo_Duracion, N_Tipo_Duracion
      from T_TIPOS_DURACION;

  END Sp_Traer_Tipos_Duracion;

  PROCEDURE Sp_Traer_Tipos_Repres(
    o_Cursor OUT types.cursorType,
    p_tipo_representado in number)
  IS
  BEGIN
  /**************************************************
    Lista los tipos de representantes de un socio empresa
    p_tipo_representado: 1 = Persona // 2 = Empresa
  ***************************************************/
    if p_tipo_representado = 1 then
      OPEN o_Cursor FOR
        select ID_TIPO_REPRES, N_TIPO_REPRES
        from IPJ.T_TIPOS_REPRES
        where id_tipo_repres in (3);
    else
      OPEN o_Cursor FOR
        select ID_TIPO_REPRES, N_TIPO_REPRES
        from IPJ.T_TIPOS_REPRES
        where id_tipo_repres in (1, 2, 3, 4, 6);
    end if;

  END Sp_Traer_Tipos_Repres;

  PROCEDURE SP_Traer_Tipos_Integ_Organ(
    o_Cursor OUT types.cursorType,
    p_id_tipo_organismo in number,
    p_codigo_online in number)
  IS
    v_id_tipo_tramite_ol ipj.t_ol_entidades.id_tipo_tramite_ol%TYPE;
    v_id_sub_tramite_ol  ipj.t_ol_entidades.id_sub_tramite_ol%TYPE;
  /**************************************************
    Lista los Tipos de Integrantes disponibles
  ***************************************************/
  BEGIN

    IF p_codigo_online = 0 THEN
      OPEN o_Cursor FOR
        select t.id_tipo_integrante, t.n_tipo_integrante, t.id_ubicacion, u.n_ubicacion,
          t.Es_Suplente, t.Repetible
          from ipj.t_tipos_integrante t join ipj.t_ubicaciones u
            on t.id_ubicacion = u.id_ubicacion
         where t.Id_Tipo_Organismo = p_id_tipo_organismo
         order by t.nro_orden asc;
    --10971:[Constituci�n AC] - Modelo IPJ - �rgano de Administraci�n - Alta PF - Agregar cargos
    ELSE

      SELECT e.id_tipo_tramite_ol, e.id_sub_tramite_ol
        INTO v_id_tipo_tramite_ol, v_id_sub_tramite_ol
        FROM ipj.t_ol_entidades e
       WHERE e.codigo_online = p_codigo_online;

      --Const. AC modelo libre
      IF v_id_tipo_tramite_ol = 17 AND v_id_sub_tramite_ol = 8 THEN
        OPEN o_Cursor FOR
          select t.id_tipo_integrante, t.n_tipo_integrante, t.id_ubicacion, u.n_ubicacion,
            t.Es_Suplente, t.Repetible
          from ipj.t_tipos_integrante t join ipj.t_ubicaciones u
              on t.id_ubicacion = u.id_ubicacion
          where
            t.Id_Tipo_Organismo = p_id_tipo_organismo
            AND t.id_tipo_integrante IN (11, 20, 14, 13, 15, 16, 21, 22, 36, 37)
          order by t.nro_orden asc;

      --Const. AC modelo aprobado
      --14201:[Constituci�n AC] - Modelo aprobado - Solapa �rgano de Gobierno - Quitar autoridades obligatoria (se sacan 53 y 54. Se agrega 15)
      ELSIF v_id_tipo_tramite_ol = 17 AND v_id_sub_tramite_ol = 7 THEN
        OPEN o_Cursor FOR
          select t.id_tipo_integrante, t.n_tipo_integrante, t.id_ubicacion, u.n_ubicacion,
            t.Es_Suplente, t.Repetible
          from ipj.t_tipos_integrante t join ipj.t_ubicaciones u
              on t.id_ubicacion = u.id_ubicacion
          where
            t.Id_Tipo_Organismo = p_id_tipo_organismo
            AND t.id_tipo_integrante IN (11, 14, 13/*, 53, 54*/, 16, 15)
          order by t.nro_orden asc;
      END IF;

      --(PBI 12271) Const. Fundaciones
      --Const. FUND modelo libre
      IF v_id_tipo_tramite_ol = 23 AND v_id_sub_tramite_ol = 10 THEN
        OPEN o_Cursor FOR
          select t.id_tipo_integrante, t.n_tipo_integrante, t.id_ubicacion, u.n_ubicacion,
            t.Es_Suplente, t.Repetible
          from ipj.t_tipos_integrante t join ipj.t_ubicaciones u
              on t.id_ubicacion = u.id_ubicacion
          where
            t.Id_Tipo_Organismo = p_id_tipo_organismo
            AND t.id_tipo_integrante IN (38, 39, 40, 42, 44, 45, 41, 43, 57, 58)
          order by t.nro_orden asc;

      --Const. FUND modelo aprobado
      ELSIF v_id_tipo_tramite_ol = 23 AND v_id_sub_tramite_ol = 9 THEN
        OPEN o_Cursor FOR
          select t.id_tipo_integrante, t.n_tipo_integrante, t.id_ubicacion, u.n_ubicacion,
            t.Es_Suplente, t.Repetible
          from ipj.t_tipos_integrante t join ipj.t_ubicaciones u
              on t.id_ubicacion = u.id_ubicacion
          where
            t.Id_Tipo_Organismo = p_id_tipo_organismo
            AND t.id_tipo_integrante IN (38, 40, 42)
          order by t.nro_orden asc;
      END IF;

      --16434:[Asamblea Asociaci�n Civil] - Elecci�n de autoridades - �rgano de Gobierno
      IF v_id_tipo_tramite_ol in (30) AND v_id_sub_tramite_ol = 0 THEN
        OPEN o_Cursor FOR
          select t.id_tipo_integrante, t.n_tipo_integrante, t.id_ubicacion, u.n_ubicacion,
            t.Es_Suplente, t.Repetible
          from ipj.t_tipos_integrante t join ipj.t_ubicaciones u
              on t.id_ubicacion = u.id_ubicacion
          where
            t.Id_Tipo_Organismo = p_id_tipo_organismo
            AND t.id_tipo_integrante IN (11, 20, 14, 13, 15, 16, 55, 56, 36, 37, 21, 22)
          order by t.nro_orden asc;
      END IF;

      --[Reunion F] - Elecci�n de autoridades - �rgano de Gobierno
      IF v_id_tipo_tramite_ol in (31) AND v_id_sub_tramite_ol = 0 THEN
        OPEN o_Cursor FOR
          select t.id_tipo_integrante, t.n_tipo_integrante, t.id_ubicacion, u.n_ubicacion,
            t.Es_Suplente, t.Repetible
          from ipj.t_tipos_integrante t join ipj.t_ubicaciones u
              on t.id_ubicacion = u.id_ubicacion
          where
            t.Id_Tipo_Organismo = p_id_tipo_organismo
            AND t.id_tipo_integrante IN (38, 39, 42, 40, 44, 45, 57, 58, 41, 43)
          order by t.nro_orden asc;
      END IF;

    END IF;

  END SP_Traer_Tipos_Integ_Organ;

  PROCEDURE SP_Traer_Clasif_Ipj (
    o_Cursor OUT types.cursorType,
    p_codigo_online in number,
    p_id_ubicacion in number)
  IS
  /**************************************************
    Lista los Rubros definidos por IPJ
    - Si se pasa codigo online, elimina los ya cargados
    - Si se pasa un �rea, se muestran esas, sin todas
  ***************************************************/
  BEGIN
    OPEN o_Cursor FOR
      select id_tipo_rubro_ipj ID_RUBRO_IPJ, initcap(N_Tipo_RUBRO_IPJ) N_RUBRO_IPJ
      from IPJ.T_TIPOS_RUBROS_IPJ
      where
        (nvl(p_id_ubicacion, 0) = 0 or p_id_ubicacion = id_ubicacion) and
        id_tipo_rubro_ipj not in (select id_rubro_ipj id_tipo_rubro_ipj from Ipj.T_Ol_Entidad_Rubro_Ipj where codigo_online = p_codigo_online)
      order by n_rubro_ipj asc;

  END SP_Traer_Clasif_Ipj ;

  PROCEDURE Sp_Traer_Provicias(
    o_Cursor OUT types.cursorType)
  IS
  BEGIN
  /**************************************************
    Lista las provincias Argentinas
  ***************************************************/
    OPEN o_Cursor FOR
      select id_provincia, n_provincia, id_pais
      from DOM_MANAGER.VT_PROVINCIAS
      where
        id_PAIS = 'ARG';

  END Sp_Traer_Provicias;

  PROCEDURE Sp_Traer_Mesas_Suac(
    o_Cursor OUT types.cursorType)
  IS
  BEGIN
  /**************************************************
    Lista las mesas suac de IPJ (IPJ y Delegaciones)
  ***************************************************/
    OPEN o_Cursor FOR
      -- solo para testing, no existe la vista
      --select 3252 id_unidad, 'DIPJ01' CODIGO_MESA_SUAC, 'DIRECCION DE INSPECCION DE PERSONAS JURIDICAS' n_unidad from dual;

      select id_reparticion id_unidad, codigo_reparticion CODIGO_MESA_SUAC, nombre_reparticion n_unidad
      from nuevosuac.VT_GET_REP_IPJ
      where codigo_reparticion = 'DIPJ01'
      order by n_unidad;

  END Sp_Traer_Mesas_Suac;

  PROCEDURE Sp_Traer_Tipos_Mot_Baja(
    o_Cursor OUT types.cursorType)
  IS
  BEGIN
  /**************************************************
    Lista los motivo de bajas de las Autoridades
  ***************************************************/
    OPEN o_Cursor FOR
      select mb.id_motivo_baja, mb.n_motivo_baja
      from  IPJ.T_TIPOS_MOTIVOS_BAJA mb;

  END Sp_Traer_Tipos_Mot_Baja;

  PROCEDURE SP_Traer_Tipos_Fiscal(
    o_Cursor OUT types.cursorType)
  IS
  /**************************************************
    Lista los Tipos de Organos de Fiscalizacion que se pueden definir
  ***************************************************/
  BEGIN
    OPEN o_Cursor FOR
      SELECT ID_TIPO_FISCAL, N_TIPO_FISCAL
      FROM IPJ.T_TIPOS_FISCALIZACION;

  END SP_Traer_Tipos_Fiscal;

  PROCEDURE SP_Traer_Tipos_Est_Prestamo(
    o_Cursor OUT types.cursorType)
  IS
    /**************************************************
    Lista los estados de los prestamos de archivos
  ***************************************************/
  BEGIN
    OPEN o_Cursor FOR
      select id_estado_prestamo, n_estado_prestamo, mensaje
      from IPJ.T_TIPOS_ESTADO_PREST;

  END SP_Traer_Tipos_Est_Prestamo;

  PROCEDURE SP_Buscar_Variable_Config (
    o_valor_variable OUT varchar2,
    p_id_variable in varchar2)
  IS
  /**************************************************
    Busca el valor de una variable indicada
  ***************************************************/
  BEGIN

    select  valor_variable into o_valor_variable
    from IPJ.T_Config
    where
      upper(trim(id_variable)) = upper(trim(p_id_variable));
  EXCEPTION
    when OTHERS then
      o_valor_variable := null;
  END SP_Buscar_Variable_Config ;

  PROCEDURE SP_Validar_Feriado (
    o_Respuesta OUT number, -- 0 = No es Feriado / 1 = Es Feriado / -1 No es Fecha
    p_Fecha in varchar2)
  IS
  v_no_laboral NUMBER;
  /**************************************************
    Indica si a fecha informada es feriado o no
  ***************************************************/
  BEGIN
    if IPJ.Types.c_habilitar_log_SP = 'S' then
      IPJ.VARIOS.SP_GUARDAR_LOG_OL(
        p_METODO => 'SP_Validar_Feriado',
        p_NIVEL => 'Tramites Online',
        p_ORIGEN => 'DB',
        p_USR_LOG  => 'DB',
        p_USR_WINDOWS => 'Sistema',
        p_IP_PC  => null,
        p_EXCEPCION =>
         'Fecha = ' || p_Fecha
      );
    end if;

    select  count(1) into o_Respuesta
    from T_COMUNES.VT_FERIADOS
    where
      fecha = to_date(p_fecha, 'dd/mm/rrrr') and
      id_Feriado not in ('025') and -- Feriado por Cuarenteva Coronavirus
      id_localidad IN (0,1);

    --13439:[DEV] - Agregar listado de feriados/asuetos/per�odos(enero) que no est�n en la base de gobierno
    SELECT count(1) into v_no_laboral
      FROM ipj.t_config t
     WHERE t.id_variable = 'DIAS_NO_LABORABLES'
       AND To_date(SYSDATE,'dd/mm/rrrr') BETWEEN to_date(substr(t.valor_variable,1,10),'dd/mm/rrrr') AND nvl(to_date(substr(t.valor_variable,12,10),'dd/mm/rrrr'),to_date(substr(t.valor_variable,1,10),'dd/mm/rrrr'));

    o_Respuesta := o_Respuesta + v_no_laboral;
    --o_Respuesta := 1; -- 0 = No es Feriado / 1 = Es Feriado / -1 No es Fecha

  EXCEPTION
    when NO_DATA_FOUND then
      o_Respuesta := 0;
    when OTHERS then
      o_Respuesta := -1;
  END SP_Validar_Feriado ;


  PROCEDURE SP_Traer_Tipos_Docs_CDD(
    o_Cursor OUT types.cursorType,
    p_id_aplicacion_cdd in number)
  IS
  /**************************************************
    Lista los tipos de documentos habilitados en CDD.
    Cada aplicaci�n ve los suyos
  ***************************************************/
  BEGIN
    OPEN o_Cursor FOR
      select id_tipo_documento, n_tipo_documento
      from cdd.vt_tipos_documento_ipj
      where
        id_aplicacion = p_id_aplicacion_cdd;

  END SP_Traer_Tipos_Docs_CDD;

  PROCEDURE SP_Traer_Sede_Origen(
    p_Cursor OUT types.cursorType)
  IS
  /**************************************************
    Lista los tipos de Origen de Sedes
  ***************************************************/
  BEGIN
    OPEN p_Cursor FOR
      select t.id_Tipo_Sede, t.n_tipo_sede
      from ipj.T_TIPOS_SEDE_ORIGEN t
      order by n_tipo_sede;

  END SP_Traer_Sede_Origen;

  PROCEDURE SP_Guardar_Valor_Config(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_variable in varchar2,
    p_valor_variable in varchar2
  )
  IS
  /**************************************************
    Actualiza el valor de una variable en la tabla config
  ***************************************************/
  BEGIN
    --Si estan bien los datos, actualizo, si no hay registro lo agrege y se devuelve el valor insertado.
    update IPJ.T_CONFIG
    set valor_variable = p_valor_variable
    where
      upper(id_variable) = upper(p_id_variable);

    commit;
    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
    when OTHERS then
      rollback;
      o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Guardar_Valor_Config;

  PROCEDURE SP_Buscar_Cuil_RCivil (
    o_Cuil OUT varchar2,
    p_id_sexo in varchar2,
    p_nro_documento in varchar2,
    p_pai_cod_pais in varchar2,
    p_id_numero in number
  )
  IS
  /**************************************************
    Busca el cuil de un usuario, para ver si tiene Cidi Nivel 2
  ***************************************************/
  BEGIN

      select cuil into o_Cuil
      from RCIVIL.VT_PERSONAS_CUIL
      where
        id_sexo = p_id_sexo and
        nro_documento = p_nro_documento and
        pai_cod_pais = p_pai_cod_pais and
        id_numero = p_id_numero;

  EXCEPTION
    when OTHERS then
      o_Cuil := null;
  END SP_Buscar_Cuil_RCivil ;

  PROCEDURE SP_Buscar_Cuil_CIDI (
    o_Cursor OUT types.cursorType,
    p_id_sexo in varchar2,
    p_nro_documento in varchar2
  )
  IS
    v_cuil ipj.t_integrantes.cuil%TYPE;
  /**************************************************
    Busca si el usuario existe en CIDI
  ***************************************************/
  BEGIN
    --13911:[SG] - [Constituci�n AC] - Agregar PF - Validaci�n vs CIDI
    BEGIN
      SELECT i.cuil
        INTO v_cuil
        FROM ipj.t_integrantes i
       WHERE i.id_sexo = p_id_sexo
         AND i.nro_documento = p_nro_documento
         and i.cuil is not null;
    EXCEPTION
      WHEN no_data_found THEN
        v_cuil := NULL;
    END;

    IF v_cuil IS NOT NULL THEN
      open o_cursor for
        select cuil, apellido, nombre, id_Sexo, nro_documento, id_estado, n_estado,
          empleado, constatado, nivel
        from gestion_ciudadanos.vt_usuarios_cidi
        where
          id_sexo = p_id_sexo and
          nro_documento = p_nro_documento AND
          cuil = v_cuil AND
          nivel > 0; -- Nivel 0 es que no confirmo por su MAIL
    ELSE
      open o_cursor for
        select cuil, apellido, nombre, id_Sexo, nro_documento, id_estado, n_estado,
          empleado, constatado, nivel
        from gestion_ciudadanos.vt_usuarios_cidi
        where
          id_sexo = p_id_sexo and
          nro_documento = p_nro_documento AND
          nivel > 0; -- Nivel 0 es que no confirmo por su MAIL
    END IF;

  END SP_Buscar_Cuil_CIDI ;

  PROCEDURE SP_Validar_Tasa (
    o_Cursor OUT types.cursorType,
    p_tasa in varchar2)
  IS
  /**************************************************
    Muestra los datos de una tasa retributiva
  ***************************************************/
  BEGIN
    open o_cursor for
      select t.n_concepto concepto, t.importe_total, t.pagado, t.medio_de_pago
      from trs.vt_transacciones_verticales t
      where
        t.NroLiquidacionOriginal = p_tasa;

  END SP_Validar_Tasa;

  PROCEDURE SP_Traer_Tipos_Societarios (
    o_Cursor OUT types.cursorType,p_id_tipo_entidad in number,
    p_id_ubicacion in number)
  IS
  /**************************************************
    Lista los Estados de las Notificaciones
  ***************************************************/
  BEGIN
    OPEN o_Cursor FOR
      SELECT t.id_ubicacion, t.id_tipo_entidad, t.n_tipo_sociedad
        FROM ipj.t_tipos_societarios t
       WHERE t.id_ubicacion = p_id_ubicacion
         AND (t.id_tipo_entidad = p_id_tipo_entidad or nvl(p_id_tipo_entidad,0) = 0)
    ORDER BY t.id_tipo_entidad;

  END SP_Traer_Tipos_Societarios ;

  PROCEDURE SP_Traer_Prot_Jur(
    o_Cursor OUT types.cursorType)
  IS
    /**************************************************
    Lista los protocolos de Juridico
  ***************************************************/
  BEGIN
    OPEN o_Cursor FOR
      select id_prot_jur, n_prot_jur, numerador
      from ipj.t_protocolos_juridico p;

  END SP_Traer_Prot_Jur;


  PROCEDURE SP_Mail_Log_Monitor
  /********************************************************
    Lista los errores de Gesti�n y portal y env�a un mail.
    Es llamado por el job MAILMONITOR
  *********************************************************/
  IS
    --Listado de entidades_acciones con borrador en S
    CURSOR c_entAcc IS
      SELECT e.id_tramite_ipj, e.id_legajo, e.borrador BORRADOR_ENT, ea.borrador BORRADOR_EA
        FROM ipj.t_entidades_acciones ea
           , ipj.t_entidades e
       WHERE e.id_legajo = ea.id_legajo
         AND e.id_tramite_ipj = ea.id_tramite_ipj
         AND ea.borrador = 'S'
         AND e.borrador = 'N';

    --Listado de sedes sin domicilio
    CURSOR c_sedeSinDom IS
      SELECT tr.codigo_online, tr.id_tramite_ipj
        FROM IPJ.T_ENTIDADES e
        JOIN ipj.t_tramitesipj tr
          ON e.id_tramite_ipj = tr.id_tramite_ipj
        JOIN ipj.t_ol_entidades o
          ON tr.codigo_online = o.codigo_online
       WHERE o.id_tipo_tramite_ol IN (1, 20)
         AND E.ID_VIN_REAL IS NULL;

    --Listado de errores del log, agrupado por cantidad de apariciones
    CURSOR c_logApp IS
      SELECT metodo, COUNT(1) Cantidad
        FROM ipj.t_logs_app
       WHERE to_date(fec_event, 'dd/mm/rrrr') = to_date(SYSDATE-1, 'dd/mm/rrrr')
    GROUP BY metodo
    ORDER BY cantidad DESC;

    --Listado de nombres de entidades con mas de 1 espacio en blanco
    CURSOR c_denomEspacio IS
      SELECT e.id_legajo LEGAJO, e.denominacion_sia DENOMINACION
        FROM ipj.t_legajos e
       WHERE UPPER(denominacion_sia) LIKE '%  %' AND NVL(eliminado, '0') <> '1'
       UNION ALL
      SELECT e.id_legajo, e.denominacion_1
        FROM ipj.t_entidades e
       WHERE UPPER(denominacion_1) LIKE '%  %';

    --Listado de errores de LIBRO DIGITAL - ASOCIAR DOC. A CUIT
    CURSOR c_logLibros IS
      SELECT l.metodo, l.excepcion
        FROM ipj.t_logs_app l
       WHERE l.excepcion like 'ERROR: LIBRO DIGITAL - ASOCIAR DOC. A CUIT - %'
         AND to_date(fec_event, 'dd/mm/rrrr') = to_date(SYSDATE-1, 'dd/mm/rrrr');

    --Listado de errores de ENTIDADES BLINDADAS
    CURSOR c_Ent_Blindaje IS
      SELECT l.fec_event, l.excepcion
        FROM ipj.t_logs_app l
       WHERE l.metodo like 'PORTAL OL: SP_Traer_Pend_Gestion%'
         AND to_date(fec_event, 'dd/mm/rrrr') = to_date(SYSDATE-1, 'dd/mm/rrrr')
       ORDER BY l.excepcion, l.fec_event;

    --Listado de ENTIDADES AUDITADAS
   /* CURSOR c_Ent_Auditada IS
      select o.codigo_online, o.cuit, o.denominacion, o.fecha_creacion,
        (select t.n_tipo_tramite_ol from ipj.t_tipos_tramites_ol t where t.id_tipo_tramite_ol = o.id_tipo_tramite_ol) Tipo_Tramite,
        decode(o.id_estado , 1, 'Pendiente', 'Completado') Estado,
        (select sticker from ipj.t_tramitesipj tr where tr.codigo_online = o.codigo_online) Sticker
      from ipj.t_ol_entidades o
      where
        o.id_tipo_tramite_ol in (30, 33, 35, 31, 28, 29 ) and   --Libros Digitales, Asamblea , Reunion, Subsanados
        o.cuit in ('30530790130','30534699111','30545264575','30553728513','30582061846','30600032360','30619715817','30631264537','30634479305',
          '30668259991','30668271622','30669098355','30669377882','30669406270','30677616969','30680994575','30682251790','30683954876',
          '30683963433','30683985232','30685363417','30688897358','30689750121','30689816394','30692969940','30692987302','30698567496',
          '30700256266','30703354994','30707064958','30707078118','30707114238','30707136444','30707704450','30707745203','30707814469',
          '30707897798','30708006633','30708054042','30708057149','30708065893','30708172649','30708225939','30708241535','30708411228',
          '30708415630','30708530065','30708595663','30708678879','30708782064','30708807466','30708842024','30708909862','30708950625',
          '30708954000','30708983655','30709090875','30709090875','30709129771','30709191280','30709218278','30709316962','30709324019',
          '30709341231','30709468533','30709472727','30709561479','30709619663','30709687510','30709927651','30709971391','30710009437',
          '30710430736','30710816766','30710964455','30711539693','30711657416','30711669937','30711751013','30712074449','30712197834',
          '30712197834','30712535977','30713746602','30714716413','30714834904','30715388444','33560432939','33637617449','33638271899',
          '33669167909','33693450239','33707783619','33708057989','33709449619','33710092309','33710401069','33711962889','33712340369',
          '33714500029') and
        o.fecha_creacion >= to_date('01/08/2019', 'dd/mm/rrrr')
      order by  o.fecha_creacion desc;*/

    --Listado de errores de BANCOR SI DEVOLUCION
    CURSOR c_Bancor_Dev IS
      select tr.id_tramite_ipj, tr.id_estado_ult, TR.FECHA_INI_SUAC, tr.expediente, o.codigo_online, o.cod_seguridad, b.cbu_cuenta,
        (select max(h.fecha_pase) from ipj.t_tramites_ipj_estado h where h.id_tramite_ipj = tr.id_tramite_ipj) Fecha_Fin,
        (select l.denominacion_sia from ipj.t_tramitesipj_persjur p join ipj.t_legajos l on p.id_legajo = l.id_legajo where p.id_tramite_ipj = tr.id_tramite_ipj) entidad
      from ipj.t_tramitesipj tr join ipj.t_ol_cuentas_bancor b
          on tr.codigo_online = b.codigo_online
        join ipj.t_ol_entidades o
          on tr.codigo_online = o.codigo_online
      where
        tr.id_estado_ult >= 100 and tr.id_estado_ult <> 210 and
        b.fec_devolucion is null
      order by Fecha_Fin asc;

    -- Lista de Tr�mites Online finalizado que no pasaron a Gesti�n
    CURSOR c_OL_Gest IS
      select
         t.id_tramite_ipj, t.expediente, t.codigo_online, to_char(t.fecha_inicio, 'dd/mm/rrrr') Fecha_inicio,
         (select n_tipo_tramite_ol from ipj.t_tipos_tramites_ol tt where tt.id_tipo_tramite_ol = o.id_tipo_tramite_ol) Tramite
      from ipj.t_tramitesipj t join ipj.t_ol_entidades o
          on t.codigo_online = o.codigo_online
      where
        nvl(t.codigo_online, 0) > 0 and
        id_estado_ult = 1 and
        not exists (select id_tramite_ipj from ipj.t_entidades e where e.id_tramite_ipj = t.id_tramite_ipj)
        and o.id_tipo_tramite_ol in ( 30, 21, 23, 17, 1, 20, 19);


    v_From      VARCHAR2(80) := 'gestionipj@cba.gov.ar';
    v_Subject_Sistema   VARCHAR2(80) := ipj.varios.FC_Buscar_Variable_Config('ENTORNO')||' IPJ - Sistema de Gesti�n - Monitoreo General de Aplicaciones';
    v_Subject_Informe   VARCHAR2(80) := ipj.varios.FC_Buscar_Variable_Config('ENTORNO')||' IPJ - An�lisis de Informaci�n';
    --PRODUCCION
    v_Recipient VARCHAR2(80) := 'gestionipj@cba.gov.ar';
    v_Mail_Host VARCHAR2(30) := '10.250.1.239';
    --DESARROLLO
    --v_Recipient VARCHAR2(80) := 'pablo.viano@cba.gov.ar';
    --v_Mail_Host VARCHAR2(30) := '10.250.1.239';--'10.250.5.13';--'d250lx03.gobiernocba.gov.ar';--

    v_Mail_Conn utl_smtp.Connection;
    crlf        VARCHAR2(2)  := chr(13)||chr(10);

    v_cantidad NUMBER:=0;
    v_texto VARCHAR2(10000);
    v_texto2 VARCHAR2(10000);
    v_texto3 VARCHAR2(10000);
    v_texto4 VARCHAR2(10000);
    v_texto5 VARCHAR2(20000);
    v_texto6 VARCHAR2(10000);
    v_texto7 VARCHAR2(20000);
    --v_texto8 VARCHAR2(30000);
    v_texto9 VARCHAR2(20000);
    v_texto10 VARCHAR2(20000);
    v_cabecera VARCHAR2(1000);
    v_cabecera2 VARCHAR2(1000);
    v_cabecera3 VARCHAR2(1000);
    v_cabecera4 VARCHAR2(1000);
    v_cabecera5 VARCHAR2(1000);
    v_cabecera6 VARCHAR2(1000);
    v_cabecera7 VARCHAR2(1000);
    --v_cabecera8 VARCHAR2(1000);
    v_cabecera9 VARCHAR2(1000);
    v_cabecera10 VARCHAR2(1000);
    v_titulo VARCHAR2(1000);
    v_titulo2 VARCHAR2(1000);
    v_titulo3 VARCHAR2(1000);
    v_titulo4 VARCHAR2(1000);
    v_titulo5 VARCHAR2(1000);
    v_titulo6 VARCHAR2(1000);
    v_titulo7 VARCHAR2(1000);
    --v_titulo8 VARCHAR2(1000);
    v_titulo9 VARCHAR2(1000);
    v_titulo10 VARCHAR2(1000);
    v_separador_titu VARCHAR2(100)  := '---------------------------------------------------------------------------------------------';
    v_separador_titu2 VARCHAR2(100) := '---------------------------------------------------------------------------------------------';
    v_separador_titu3 VARCHAR2(100) := '---------------------------------------------------------------------------------------------';
    v_separador_titu4 VARCHAR2(100) := '---------------------------------------------------------------------------------------------';
    v_separador_titu5 VARCHAR2(100) := '---------------------------------------------------------------------------------------------';
    v_separador_titu6 VARCHAR2(100);
    v_separador_titu7 VARCHAR2(100):= '---------------------------------------------------------------------------------------------';
    --v_separador_titu8 VARCHAR2(100):= '---------------------------------------------------------------------------------------------';
    v_separador_titu9 VARCHAR2(100):= '---------------------------------------------------------------------------------------------';
    v_separador_titu10 VARCHAR2(100):= '---------------------------------------------------------------------------------------------';
    v_separador VARCHAR2(100)  := '-----------------------------------------------------------';
    v_separador2 VARCHAR2(100) := '-----------------------------------------------------------';
    v_separador3 VARCHAR2(100) := '-----------------------------------------------------------';
    v_separador4 VARCHAR2(100) := '-----------------------------------------------------------';
    v_separador5 VARCHAR2(100) := '-----------------------------------------------------------';
    v_separador6 VARCHAR2(100);
    v_separador7 VARCHAR2(100);
    --v_separador8 VARCHAR2(100):= '-----------------------------------------------------------';
    v_separador9 VARCHAR2(100):= '-----------------------------------------------------------';
    v_separador10 VARCHAR2(100):= '-----------------------------------------------------------';
    v_fecha_delete VARCHAR2(2);
    v_tit_logs_borrados VARCHAR2(100);
    v_logs_borrados NUMBER;
    v_tit_tasas_borrados VARCHAR2(100);
    v_tasas_borrados NUMBER;
    v_mon_valor_variable VARCHAR2(1000);
    v_mon_id_variable VARCHAR2(1000);
    v_mon_valor_variable2 VARCHAR2(1000);
    v_mon_id_variable2 VARCHAR2(1000);
    v_mon_valor_variable3 VARCHAR2(1000);
    v_mon_id_variable3 VARCHAR2(1000);
  BEGIN

    /*****Bloque de actualizaci�n de variables*********************************************************
    Para que funcione correctamente se deben configurar las siguientes variables:
    MON_FECHA_VARIABLE: Fecha del d�a en que se tienen que ejecutar formato dd/mm/rrrr
    MON_NOMBRE_VARIABLE: Nombre de la variable que se va a actualizar
    MON_VALOR_VARIABLE: Valor con el que se va a actualizar la variable indicada en MON_NOMBRE_VARIABLE
    ***************************************************************************************************/
    IF to_char(SYSDATE,'dd/mm/rrrr') = FC_Buscar_Variable_Config('MON_FECHA_VARIABLE') THEN
      BEGIN

        v_mon_valor_variable := FC_Buscar_Variable_Config('MON_VALOR_VARIABLE');
        v_mon_id_variable := FC_Buscar_Variable_Config('MON_ID_VARIABLE');
        v_mon_valor_variable2 := FC_Buscar_Variable_Config('MON_VALOR_VARIABLE2');
        v_mon_id_variable2 := FC_Buscar_Variable_Config('MON_ID_VARIABLE2');
        v_mon_valor_variable3 := FC_Buscar_Variable_Config('MON_VALOR_VARIABLE3');
        v_mon_id_variable3 := FC_Buscar_Variable_Config('MON_ID_VARIABLE3');

        UPDATE ipj.t_config c
           SET c.valor_variable = v_mon_valor_variable
         WHERE c.id_variable = v_mon_id_variable;

        UPDATE ipj.t_config c
           SET c.valor_variable = v_mon_valor_variable2
         WHERE c.id_variable = v_mon_id_variable2;

        UPDATE ipj.t_config c
           SET c.valor_variable = v_mon_valor_variable3
         WHERE c.id_variable = v_mon_id_variable3;

        COMMIT;

        v_titulo6 := 'ACTUALIZACI�N DE VARIABLES AUTOM�TICAS';
        v_cabecera6 := 'Variable - Valor';
        v_texto6:=v_texto6 || crlf ||v_mon_id_variable||' - '||v_mon_valor_variable;
        v_texto6:=v_texto6 || crlf ||v_mon_id_variable2||' - '||v_mon_valor_variable2;
        v_texto6:=v_texto6 || crlf ||v_mon_id_variable3||' - '||v_mon_valor_variable3;
        v_separador_titu6 := '---------------------------------------------------------------------------------------------';
        v_separador6 := '-----------------------------------------------------------';

      EXCEPTION
        WHEN OTHERS THEN
          NULL;

      END;
    END IF;

    --Bloque de ejecuci�n de limpieza de logs
    BEGIN
      SELECT substr(to_date(SYSDATE,'dd/mm/rrrr'),1,2)
        INTO v_fecha_delete
        FROM dual;

      --Todos los primeros de cada mes borro los logs mas viejos que un a�o
      IF v_fecha_delete = '01' THEN
        DELETE ipj.t_logs_app l
         WHERE to_date(l.fec_event,'dd/mm/rrrr') < to_date(SYSDATE,'dd/mm/rrrr')-365;
         v_tit_logs_borrados := 'LOGS ELIMINADOS POR ANTIG�EDAD: ';
         v_logs_borrados := SQL%ROWCOUNT;
        COMMIT;
      ELSE
        v_logs_borrados := NULL;
        v_tit_logs_borrados := NULL;
      END IF;

    EXCEPTION
      WHEN OTHERS THEN
        NULL;
    END;

    --Bloque de ejecuci�n de Eliminaci�n de tasas Viejas
    BEGIN
      --Elimino las tasas que ya no estan vigentes
      delete ipj.t_ol_entidad_tasas
      where
        codigo_online in (select codigo_online from ipj.t_ol_entidades o where o.id_estado = 1) and
        (id_concepto > 0 or not exists (select * from TRS.VT_CONCEPTOS_VERTICALES t where t.id_concepto = id_concepto_trs  and sysdate between t.fecha_desde and nvl(t.fecha_hasta, sysdate)));

       v_tit_tasas_borrados := 'TASAS VENCIDAS ELIMINADAS: ';
       v_tasas_borrados := SQL%ROWCOUNT;
      COMMIT;
    EXCEPTION
      WHEN OTHERS THEN
        NULL;
    END;

    --Bloque de armado y env�o de mails
    BEGIN
      v_titulo := 'ENTIDADES ACCIONES CON BORRADOR EN S';
      v_cabecera := 'Tramite - Legajo - Borrador ent - Borrador ea';
      FOR r1 IN c_entAcc LOOP
        v_texto:= v_texto || crlf ||r1.id_tramite_ipj||' - '||r1.id_legajo||' - '||r1.borrador_ent||' - '||r1.borrador_ea;
      END LOOP;
      IF v_texto IS NULL THEN
        v_cabecera := '';
        v_separador := '';
        v_separador_titu := '';
        v_titulo := '';
      END IF;

      v_titulo2 := 'SEDE SOCIAL SIN DOMICILIO';
      v_cabecera2 := 'Cod ol - Tramite Ipj';
      FOR r2 IN c_sedeSinDom LOOP
        v_texto2:=v_texto2 || crlf ||r2.codigo_online||' - '||r2.id_tramite_ipj;
      END LOOP;
      IF v_texto2 IS NULL THEN
        v_cabecera2 := '';
        v_separador2 := '';
        v_separador_titu2 := '';
        v_titulo2 := '';
      END IF;

      v_titulo3 := 'LOGS DE LA APLICACI�N';
      v_cabecera3 := 'M�todo - Cantidad';
      FOR r3 IN c_logApp LOOP
        IF r3.metodo IN ('PORTAL OL: GobCba.TramitesOnlineIPJ.Cidi.CidiNotificacionService','PORTAL OL: Domicilios inv�lidos AFIP',
                         'SetFinFirmaTramite - ComunicarReunionSAS','SetFinFirmaTramite - ComunicarResolucionConstitucionSAS') THEN
          v_texto3:=v_texto3 || crlf ||'********VALIDAR******** - '||r3.metodo||' - '||r3.cantidad;
        ELSE
          v_texto3:=v_texto3 || crlf ||r3.metodo||' - '||r3.cantidad;
        END IF;
      END LOOP;
      IF v_texto3 IS NULL THEN
        v_cabecera3 := '';
        v_separador3 := '';
        v_separador_titu3 := '';
        v_titulo3 := '';
      END IF;

      v_titulo4 := 'NOMBRES DE ENTIDADES CON ESPACIOS';
      v_cabecera4 := 'Legajo - Denominaci�n';
      FOR r4 IN c_denomEspacio LOOP
        v_texto4:=v_texto4 || crlf ||r4.legajo||' - '||r4.denominacion;
      END LOOP;
      IF v_texto4 IS NULL THEN
        v_cabecera4 := '';
        v_separador4 := '';
        v_separador_titu4 := '';
        v_titulo4 := '';
      END IF;

      v_titulo5 := 'LOGS DE LIBROS DIGITALES';
      v_cabecera5 := 'M�todo - Excepci�n';
      FOR r5 IN c_logLibros LOOP
        v_texto5:=v_texto5 || crlf ||r5.metodo||' - '||r5.excepcion;
      END LOOP;
      IF v_texto5 IS NULL THEN
        v_cabecera5 := '';
        v_separador5 := '';
        v_separador_titu5 := '';
        v_titulo5 := '';
      END IF;

      v_titulo7 := 'ENTIDADES RECHAZADAS EN BLINDAJE';
      v_cabecera7 := '';
      FOR r7 IN c_Ent_Blindaje LOOP
        v_texto7:= v_texto7 || crlf ||' - ' || to_char(r7.fec_event, 'dd/mm/rrrr HH:MI:SS AM')||' - '||r7.excepcion;
      END LOOP;

      /*v_titulo8 := 'ENTIDADES AUDITADAS';
      v_cabecera8 := 'Entidades notificadas por Carta Documento para regularizar desde 01/08/2019';
      FOR r8 IN c_Ent_Auditada LOOP
        v_texto8:= v_texto8 || crlf ||' - ' ||r8.denominacion || '(Cuit ' || r8.cuit || ') - Fecha ' || to_char(r8.fecha_creacion) ||
          ' - Tr�mite: ' || r8.tipo_tramite || ' - Estado: ' || r8.estado || ' - Sticker: ' || r8.sticker;
      END LOOP;*/

      v_titulo9 := 'TRAMITES BANCOR SIN DEVOLUCIONES';
      v_cabecera9 := 'Tramites finalizados sin ejecutar la devoluci�n.';
      FOR r9 IN c_Bancor_Dev LOOP
        v_texto9:= v_texto9 || crlf ||' - Tr�mite: ' || to_char(r9.id_tramite_ipj) || ' - Estado: ' || to_char(r9.id_estado_ult) || ' - Exp.: ' || r9.expediente ||
          ' - Cod. OL: ' || r9.codigo_online || ' - Seguridad: ' || r9.cod_seguridad || ' - CBU: ' || r9.cbu_cuenta || ' - Fecha Fin: ' || to_char(r9.fecha_fin, 'dd/mm/rrrr') ||
          ' - Entidad: ' || r9.entidad ;
      END LOOP;

      v_titulo10 := 'TRAMITES OL QUE NO PASARON A GESTION';
      v_cabecera10 := 'TramiteIPJ - Expediete - Cod. Online - Fec. Inicio - Tramite';
      FOR r10 IN c_OL_Gest LOOP
        v_texto10:=v_texto10 || crlf ||to_char(r10.id_tramite_ipj)||' - '||r10.expediente||' - '||to_char(r10.codigo_online)||' - '||r10.Fecha_inicio||' - '||r10.tramite ;
      END LOOP;
      IF v_texto10 IS NULL THEN
        v_cabecera10 := '';
        v_separador10 := '';
        v_separador_titu10 := '';
        v_titulo10 := '';
      END IF;


      --*******************************************************
      -- ARMO EL MAIL DE SISTEMAS
      -- *******************************************************
      v_Mail_Conn := utl_smtp.Open_Connection(v_Mail_Host, 25);
      utl_smtp.Helo(v_Mail_Conn, v_Mail_Host);
      utl_smtp.Mail(v_Mail_Conn, v_From);
      utl_smtp.Rcpt(v_Mail_Conn, v_Recipient);
      utl_smtp.Data(v_Mail_Conn,
        'Date: '   || to_char(sysdate, 'Dy, DD Mon YYYY hh24:mi:ss') || crlf ||
        'From: '   || v_From || crlf ||
        'Subject: '|| v_Subject_Sistema || crlf ||
        'To: '     || v_Recipient || crlf ||
        crlf ||
        v_titulo || crlf || v_separador_titu|| crlf || crlf || v_cabecera || crlf || v_separador|| crlf || v_texto || crlf || crlf ||
        v_titulo2 || crlf || v_separador_titu2 || crlf || crlf || v_cabecera2 || crlf || v_separador2 || crlf || v_texto2 || crlf || crlf ||
        v_titulo3 || crlf || v_separador_titu3 || crlf || crlf || v_cabecera3 || crlf || v_separador3 || crlf || v_texto3 || crlf || crlf ||
        v_tit_logs_borrados || v_logs_borrados|| crlf ||crlf ||
        v_tit_tasas_borrados || v_tasas_borrados|| crlf ||crlf ||
        v_titulo4 || crlf || v_separador_titu4 || crlf || crlf || v_cabecera4 || crlf || v_separador4 || crlf ||  v_texto4 || crlf || crlf ||
        v_titulo5 || crlf || v_separador_titu5 || crlf || crlf || v_cabecera5 || crlf || v_separador5 || crlf ||  v_texto5 || crlf || crlf ||
        v_titulo6 || crlf || v_separador_titu6 || crlf || crlf || v_cabecera6 || crlf || v_separador6 || crlf ||  v_texto6 || crlf || crlf ||
        v_titulo9 || crlf || v_separador_titu9 || crlf || crlf || v_cabecera9 || crlf || v_separador9 || crlf ||  v_texto9 || crlf || crlf ||
        v_titulo10 || crlf || v_separador_titu10 || crlf || crlf || v_cabecera10 || crlf || v_separador10 || crlf ||  v_texto10 || crlf
      );
      utl_smtp.Quit(v_mail_conn);

      --*******************************************************
      -- ARMO EL MAIL INFORMACION DE ANALISIS
      -- *******************************************************
      v_Mail_Conn := utl_smtp.Open_Connection(v_Mail_Host, 25);
      utl_smtp.Helo(v_Mail_Conn, v_Mail_Host);
      utl_smtp.Mail(v_Mail_Conn, v_From);
      utl_smtp.Rcpt(v_Mail_Conn, v_Recipient);
      utl_smtp.Data(v_Mail_Conn,
        'Date: '   || to_char(sysdate, 'Dy, DD Mon YYYY hh24:mi:ss') || crlf ||
        'From: '   || v_From || crlf ||
        'Subject: '|| v_Subject_Informe || crlf ||
        'To: '     || v_Recipient || crlf ||
        crlf ||
        v_titulo7 || crlf || v_separador_titu7 || crlf || crlf || v_cabecera7 || crlf || v_separador7 || crlf ||  v_texto7 || crlf || crlf --||
        --v_titulo8 || crlf || v_separador_titu8 || crlf || crlf || v_cabecera8 || crlf || v_separador8 || crlf ||  v_texto8 || crlf
      );
      utl_smtp.Quit(v_mail_conn);

    EXCEPTION
      WHEN utl_smtp.Transient_Error OR utl_smtp.Permanent_Error then
        raise_application_error(-20000, 'Unable to send mail', TRUE);
    END;

  END SP_Mail_Log_Monitor;

  PROCEDURE SP_Traer_Barrios_Localidad(
    o_Cursor OUT types.cursorType,
    p_id_localidad in number
  )
  IS
    /**************************************************
    Lista los barrios configurados para una localidad determinada
  ***************************************************/
  BEGIN
    OPEN o_Cursor FOR
      select b.id_barrio, b.n_barrio
      from DOM_MANAGER.VT_BARRIOS b
      where
        b.id_localidad = p_id_localidad
      order by b.n_barrio asc;

  END SP_Traer_Barrios_Localidad;

  PROCEDURE SP_Traer_Tipos_Calle_Localidad(
    o_Cursor OUT types.cursorType,
    p_id_localidad in number
  )
  IS
  v_Existe NUMBER;
    /**************************************************
    Lista los tipos de calles existentes para las calles configuradas en una localidad
  ***************************************************/
  BEGIN
    --Bug #11394: [Domicilio] - Al registrar un domicilio especial se deber�a visualizar el bot�n " Agregar mi Calle "
    SELECT COUNT(1)
      INTO v_Existe
      FROM DOM_MANAGER.VT_CALLES c JOIN DOM_MANAGER.VT_TIPOCALLES tc
        ON c.id_tipocalle = tc.id_tipocalle
     WHERE c.id_localidad = p_id_localidad
  ORDER BY tc.n_tipocalle ASC;

    IF v_Existe <> 0 THEN
      OPEN o_Cursor FOR
        select distinct tc.id_tipocalle, tc.n_tipocalle
        from DOM_MANAGER.VT_CALLES c join DOM_MANAGER.VT_TIPOCALLES tc
          on c.id_tipocalle = tc.id_tipocalle
        where
          c.id_localidad = p_id_localidad
        order by tc.n_tipocalle asc;
    ELSE
      OPEN o_Cursor FOR
        select tc.id_tipocalle, tc.n_tipocalle
        from DOM_MANAGER.VT_TIPOCALLES tc
        order by tc.n_tipocalle asc;
    END IF;

  END SP_Traer_Tipos_Calle_Localidad;

  PROCEDURE SP_Traer_Tipos_Calle_Todos(
    o_Cursor OUT types.cursorType
  )
  IS
    /**************************************************
    Lista los todos los tipos de calles existentes en gobierno
  ***************************************************/
  BEGIN
    OPEN o_Cursor FOR
      select tc.id_tipocalle, tc.n_tipocalle
      from DOM_MANAGER.VT_TIPOCALLES tc
      order by tc.n_tipocalle asc;

  END SP_Traer_Tipos_Calle_Todos;

  PROCEDURE SP_Traer_Calles_Tipo_Localidad(
    o_Cursor OUT types.cursorType,
    p_id_localidad in number,
    p_id_tipocalle in number
  )
  IS
    /**************************************************
    Lista las calles existentes para una localidad y tipo definidos.
    Si no viene tipo, lista todas
  ***************************************************/
  BEGIN
    OPEN o_Cursor FOR
      select c.id_calle, c.id_tipocalle, c.n_calle
      from DOM_MANAGER.VT_CALLES c
      where
        c.id_localidad = p_id_localidad and
        (p_id_tipocalle = 0 or c.id_tipocalle = p_id_tipocalle)
      order by c.n_calle asc;

  END SP_Traer_Calles_Tipo_Localidad;

  PROCEDURE Sp_Traer_Empresas_Gobierno(
    o_Cursor OUT types.cursorType,
    p_cuit in varchar2)
  IS
  BEGIN
    /**************************************************
      Busca una empresa registrada en gobierno
    ***************************************************/
    OPEN o_Cursor FOR
      select cuit, razon_social denominacion_sia
      from T_COMUNES.VT_PERS_JURIDICAS_UNICA
      where
        cuit = replace(p_cuit, '-', '');

  END Sp_Traer_Empresas_Gobierno;

  PROCEDURE SP_Traer_Objetos_Sociales(
    o_Cursor OUT types.cursorType,
    p_id_tipo_entidad number)
  IS
  /**********************************************************
    Dado un tipo de empresa, lista los objetos sociales habilitados para la misma
  **********************************************************/
  BEGIN
    OPEN o_Cursor FOR
      select id_obj_social, n_obj_social, desc_obj_social, id_tipo_entidad
      from IPJ.T_TIPOS_OBJ_SOCIAL t
      where
        t.id_tipo_entidad = p_id_tipo_entidad;
  END SP_Traer_Objetos_Sociales;

  PROCEDURE SP_ejecutar_sentencia_job IS
    v_sql VARCHAR2(2000);
  /**********************************************************
    Toma el valor configurado y lo ejecuta. Se instancia desde el job mailmonitor
  **********************************************************/
  BEGIN
    SELECT NVL(c.valor_variable,'SELECT 1 FROM DUAL')
      INTO v_sql
      FROM ipj.t_config c
     WHERE c.id_variable = 'DYNAMIC_SQL';
    EXECUTE IMMEDIATE v_sql;
    COMMIT;
  EXCEPTION
    WHEN OTHERS THEN
      raise_application_error(-20000, 'Error al ejecutar la sentencia din�mica '||v_sql, TRUE);
  END SP_ejecutar_sentencia_job;

  PROCEDURE Sp_Traer_Tipos_Notif(
    o_Cursor OUT types.cursorType,
    p_ubicacion in number)
  IS
  /**************************************************
    Lista los Tipos de Notificaciones de SUAC habilitados para cada area
  ***************************************************/
  BEGIN

    OPEN o_Cursor FOR
      select n.id_tipo_notificacion, n.n_tipo_notificacion, N_Notificacion,
        nvl(n.editable, 'N') editable, n.texto_extra
      from IPJ.T_TIPOS_NOTIFICACION n
      where
         n.id_ubicacion = p_ubicacion
      ORDER BY n.n_notificacion;

  END Sp_Traer_Tipos_Notif;

  PROCEDURE SP_Pasar_Fecha_Reserva(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_fec_vence OUT VARCHAR2,
    p_fecha in VARCHAR2)
  IS
   /**********************************************************
    Calcula la fecha de vencimiento de una Reserva/Pr�rroga
  **********************************************************/
  BEGIN

    o_fec_vence := to_date(to_date(p_fecha,'dd/mm/rrrr') + ipj.varios.FC_Buscar_Variable_Config('DIAS_VENC_RESERVA'), 'dd/mm/rrrr');

    --Si la fecha es en enero, le sumo 31 d�as y valido que caiga un d�a h�bil
    IF to_char(to_date(o_fec_vence,'dd/mm/rrrr'),'mm') = '01' THEN
      o_fec_vence := to_char(to_date(o_fec_vence,'dd/mm/rrrr') + 31,'dd/mm/rrrr');
    END IF;
    WHILE ipj.varios.FC_Dias_Laborables(o_fec_vence, o_fec_vence, 'N') = 0 LOOP
      o_fec_vence := to_char(to_date(o_fec_vence,'dd/mm/rrrr') + 1,'dd/mm/rrrr');
    END LOOP;

    o_fec_vence:=to_char(to_date(o_fec_vence,'dd/mm/rrrr'),'dd/mm/rrrr');
    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
    when OTHERS then
       o_rdo := 'SP_Pasar_Fecha_Reserva: ' || To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;

  END SP_Pasar_Fecha_Reserva;

  PROCEDURE SP_TRAER_TOKEN_AFIP (
    o_Cursor OUT types.cursorType,
    p_id_aplicacion in number,
    p_servicio_afip in varchar2)
  IS
  /*********************************************************
     BUSCA el token indicado para la coneccion con AFIP
  *********************************************************/
  BEGIN
    OPEN o_Cursor FOR
      select fecha_caducidad, id_aplicacion, cuit_token, token_hash, sign_hash, servicio_afip
      from IPJ.T_TOKEN_AFIP
      where
         id_aplicacion = p_id_aplicacion and
         servicio_afip = p_servicio_afip;

  END SP_TRAER_TOKEN_AFIP;

  PROCEDURE SP_GUARDAR_TOKEN_AFIP (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_fecha_caducidad in varchar2,
    p_id_aplicacion in number,
    p_cuit_token in varchar2,
    p_token_hash in varchar2,
    p_sign_hash in varchar2,
    p_servicio_afip in varchar2)
  IS
  /*********************************************************
     BUSCA el token indicado para la coneccion con AFIP
  *********************************************************/
  BEGIN
    -- Actualizo el registro
    update IPJ.T_TOKEN_AFIP
    set
      fecha_caducidad = p_fecha_caducidad,
      cuit_token = p_cuit_token,
      token_hash = p_token_hash,
      sign_hash = p_sign_hash
    where
      id_aplicacion = p_id_aplicacion and
      servicio_afip = p_servicio_afip;

    -- Si no existia, lo creo
    if sql%rowcount = 0 then
      insert into IPJ.T_TOKEN_AFIP (fecha_caducidad, id_aplicacion, cuit_token, token_hash, sign_hash, servicio_afip)
      values (p_fecha_caducidad, p_id_aplicacion, p_cuit_token, p_token_hash, p_sign_hash, p_servicio_afip);
    end if;

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    commit;
  EXCEPTION
    when OTHERS then
      o_rdo := 'SP_GUARDAR_TOKEN_AFIP: ' || To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;

  END SP_GUARDAR_TOKEN_AFIP;

  PROCEDURE SP_Pasar_Fecha_Habil(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_fec_habil OUT VARCHAR2,
    p_fecha in VARCHAR2,
    p_cant_dias IN NUMBER,
    p_cuenta_enero IN VARCHAR2,--puede devolver fecha en enero S/N
    p_dias_habiles IN VARCHAR2)--suma d�as h�biles S/N
  IS
   /************************************************************************************
    Suma la cantidad de d�as a la fecha pasada por par�metro, y devuelve una fecha h�bil.
    **************************************************************************************/
  v_fecha_habil VARCHAR2(10);
  v_cant NUMBER:=0;
  v_dias_enero NUMBER:=0;
  BEGIN

    IF p_dias_habiles = 'S' THEN
      v_fecha_habil := to_char(to_date(p_fecha,'dd/mm/rrrr'),'dd/mm/rrrr');
      --Si no devuelve fecha en el mes de enero
      IF p_cuenta_enero = 'N' THEN
        --Si la fecha es en enero, le sumo 31 d�as
        IF to_char(to_date(v_fecha_habil,'dd/mm/rrrr'),'mm') = '01' THEN
          v_fecha_habil := to_char(to_date(v_fecha_habil,'dd/mm/rrrr') + 31,'dd/mm/rrrr');
        END IF;
      END IF;
      WHILE v_cant < p_cant_dias LOOP
        --Cuento solo si es laborable
        IF ipj.varios.FC_Dias_Laborables(to_date(v_fecha_habil,'dd/mm/rrrr'), to_date(v_fecha_habil,'dd/mm/rrrr'), p_cuenta_enero) <> 0 THEN
          v_cant := v_cant + 1;
        END IF;
        v_fecha_habil := to_date(to_date(v_fecha_habil,'dd/mm/rrrr') + 1, 'dd/mm/rrrr');
      END LOOP;
      --Valido que la fecha caiga un d�a h�bil
      WHILE ipj.varios.FC_Dias_Laborables(v_fecha_habil, v_fecha_habil, p_cuenta_enero) = 0 LOOP
        v_fecha_habil := to_char(to_date(v_fecha_habil,'dd/mm/rrrr') + 1,'dd/mm/rrrr');
      END LOOP;

      o_fec_habil := to_date(v_fecha_habil,'dd/mm/rrrr');

    ELSE
      v_fecha_habil := to_char(to_date(p_fecha,'dd/mm/rrrr'),'dd/mm/rrrr');
      --Si no devuelve fecha en el mes de enero
      IF p_cuenta_enero = 'N' THEN
        --Si la fecha es en enero, le sumo 31 d�as
        IF to_char(to_date(p_fecha,'dd/mm/rrrr'),'mm') = '01' THEN
          v_fecha_habil := to_char(to_date(p_fecha,'dd/mm/rrrr') + 31,'dd/mm/rrrr');
        END IF;

        --Sumo los d�as
        v_fecha_habil := to_char(to_date(v_fecha_habil,'dd/mm/rrrr') + p_cant_dias, 'dd/mm/rrrr');
        --Si cae en enero, veo cu�ntos d�as de enero abarca para contarlos en febrero
        IF to_char(to_date(v_fecha_habil,'dd/mm/rrrr'),'mm') = '01' THEN
          v_dias_enero := to_char(to_date(v_fecha_habil,'dd/mm/rrrr'),'dd');
          v_cant := p_cant_dias - v_dias_enero;
        END IF;

      ELSE
        v_fecha_habil := to_char(to_date(v_fecha_habil,'dd/mm/rrrr') + p_cant_dias, 'dd/mm/rrrr');
      END IF;

      --Valido que la fecha caiga un d�a h�bil
      WHILE ipj.varios.FC_Dias_Laborables(to_date(v_fecha_habil,'dd/mm/rrrr'), to_date(v_fecha_habil,'dd/mm/rrrr'), p_cuenta_enero) = 0 LOOP
        v_fecha_habil := to_date(to_date(v_fecha_habil,'dd/mm/rrrr') + 1,'dd/mm/rrrr');

      END LOOP;

      --Sumo la cant de d�as calculados
      v_fecha_habil := to_char(to_date(v_fecha_habil,'dd/mm/rrrr') + v_cant,'dd/mm/rrrr');
      o_fec_habil := to_date(v_fecha_habil,'dd/mm/rrrr');
    END IF;

    o_fec_habil:=to_char(to_date(o_fec_habil,'dd/mm/rrrr'),'dd/mm/rrrr');
    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;


  EXCEPTION
    WHEN OTHERS THEN
       o_rdo := 'SP_Pasar_Fecha_Habil: ' || To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;

  END SP_Pasar_Fecha_Habil;

  PROCEDURE SP_Traer_Est_Doc_Adj(
    o_Cursor OUT types.cursorType)
  IS
    /**************************************************
    Lista los estados para los documentos adjuntos a un tr�mite
  ***************************************************/
  BEGIN
    OPEN o_Cursor FOR
      select id_estado_doc, n_estado_doc
      from IPJ.T_TIPOS_ESTADOS_DOC
      order by id_estado_doc;

  END SP_Traer_Est_Doc_Adj;

  PROCEDURE SP_Traer_Tipos_Tarea(
    o_Cursor OUT types.cursorType)
  IS
    /**************************************************
      Lista los tipos de tareas que se pueden definir en el Portal Web
  ***************************************************/
  BEGIN
    OPEN o_Cursor FOR
      select id_tarea, n_tarea
      from IPJ.T_TIPOS_TAREAS
      order by n_tarea;

  END SP_Traer_Tipos_Tarea;

  PROCEDURE SP_Traer_Tipos_Seccion_Inf(
    o_Cursor OUT types.cursorType)
  IS
   /**********************************************************
    Lista los tipos de secci�n de Informes.
  **********************************************************/
  BEGIN

    OPEN o_Cursor FOR
      select ts.n_secc_inf_rpc, ts.orden
        from ipj.t_tipos_seccion_inf_rpc ts
        order by ts.orden asc;

  END SP_Traer_Tipos_Seccion_Inf;

  PROCEDURE SP_Traer_Tipo_Intervencion(
    o_Cursor OUT types.cursorType,
    p_id_ubicacion in number)
  IS
  /**************************************************
    Lista los Tipos de Intervenciones definidos
  ***************************************************/
  BEGIN
    -- Muestra los que generales (sin ubicacion) o los propios de la ubicacion indicada
    OPEN o_Cursor FOR
      select id_tipo_intervencion, n_tipo_intervencion, observacion, informar_libros
      from ipj.t_tipos_intervencion i
      where
        (id_ubicacion is null or nvl(id_ubicacion, 0) = p_id_ubicacion);

  END SP_Traer_Tipo_Intervencion;

  PROCEDURE Sp_Traer_Tipos_Mot_Vista(
    o_Cursor OUT types.cursorType,
    p_Motivo IN ipj.t_tipos_motivos_vista.motivo%TYPE)
  IS
  BEGIN
  /**************************************************
    Lista los motivos de solicitud de Vista
  ***************************************************/
    OPEN o_Cursor FOR
      SELECT mv.id_motivo_vista, mv.n_motivo_vista, mv.motivo
        FROM IPJ.T_TIPOS_MOTIVOS_VISTA mv
       WHERE mv.motivo = nvl(p_Motivo, mv.motivo);

  END Sp_Traer_Tipos_Mot_Vista;

  PROCEDURE SP_Traer_Prot_Dig(
    o_Cursor OUT types.cursorType)
  IS
    /**************************************************
    Lista los protocolos digitales
  ***************************************************/
  BEGIN
    OPEN o_Cursor FOR
      select id_prot_dig, n_prot_dig, numerador
      from ipj.t_protocolos_digital p;

  END SP_Traer_Prot_Dig;

  PROCEDURE SP_Traer_Prot_Dig_Area(
    o_Cursor OUT types.cursorType,
    p_id_ubicacion in number)
  IS
    /**************************************************
    Lista los protocolos digitales habilitados para un �rea + Protocolo A para SA
  ***************************************************/
  BEGIN
    -- Muestro los protocolos de cada �rea, pero Jur�dico ve todos
    OPEN o_Cursor FOR
      select distinct id_prot_dig, n_prot_dig, numerador
      from
        (SELECT p.id_prot_dig, n_prot_dig, (CASE WHEN compartido = 'S' THEN pa.numerador ELSE p.numerador END)numerador, pa.id_ubicacion
           FROM ipj.t_protocolos_digital p, t_prot_dig_area pa
          WHERE p.id_prot_dig = pa.id_prot_dig
          UNION ALL
          select sigla id_prot_dig, n_protocolo n_prot_dig, numerador, 4 id_ubicacion
          from ipj.t_config_protocolo cp
          where
            cp.sigla = 'A'
          UNION ALL
          select sigla id_prot_dig, n_protocolo n_prot_dig, numerador, 1 id_ubicacion
          from ipj.t_config_protocolo cp
          where
            cp.sigla = 'A'
         ) tmp
      where
        (p_id_ubicacion = IPJ.TYPES.C_AREA_JURIDICO or id_ubicacion = p_id_ubicacion);

  END SP_Traer_Prot_Dig_Area;

  PROCEDURE Sp_Traer_Tipos_Denuncia(
    o_Cursor OUT types.cursorType)
  IS
  BEGIN
  /**************************************************
    Lista los tipos de denuncias habilitados
  ***************************************************/
    OPEN o_Cursor FOR
      select td.id_motivo_denuncia, td.n_motivo_denuncia
      from  IPJ.T_TIPOS_MOTIVOS_DENUNCIA td
      order by td.n_motivo_denuncia asc;

  END Sp_Traer_Tipos_Denuncia;

  PROCEDURE Sp_Traer_Tipos_Solic_CN(
    o_Cursor OUT types.cursorType)
  IS
  BEGIN
  /**************************************************
    Lista los tipos de Solicitantes para Comisiones Normalizadoras
  ***************************************************/
    OPEN o_Cursor FOR
      select ts.id_solicitante_cn, ts.n_solicitante_cn
      from  IPJ.T_TIPOS_SOLIC_CN ts
      order by ts.n_solicitante_cn asc;

  END Sp_Traer_Tipos_Solic_CN;

  PROCEDURE Sp_Traer_Tipos_Comisiones(
    o_Cursor OUT types.cursorType)
  IS
  BEGIN
  /**************************************************
    Lista los tipos de Solicitantes para Comisiones Normalizadoras
  ***************************************************/
    OPEN o_Cursor FOR
      select mc.id_motivo_comision, mc.n_motivo_comision
      from  IPJ.T_TIPOS_MOTIVOS_COMISION mc
      order by mc.n_motivo_comision asc;

  END Sp_Traer_Tipos_Comisiones;

  PROCEDURE Sp_Traer_Tipos_Normalizador(
    o_Cursor OUT types.cursorType)
  IS
  BEGIN
  /**************************************************
    Lista los tipos de Normalizacores para Comisiones Normalizadoras
  ***************************************************/
    OPEN o_Cursor FOR
      select tn.id_normalizador, tn.n_normalizador
      from  IPJ.T_TIPOS_NORMALIZADOR tn
      order by tn.n_normalizador asc;

  END Sp_Traer_Tipos_Normalizador;

  PROCEDURE Sp_Traer_Tipos_Est_Norm(
    o_Cursor OUT types.cursorType)
  IS
  BEGIN
  /**************************************************
    Lista los tipos de Normalizacores para Comisiones Normalizadoras
  ***************************************************/
    OPEN o_Cursor FOR
      select te.id_estado_normalizador, te.n_estado_normalizador
      from  IPJ.T_TIPOS_ESTADO_NORM te
      order by te.n_estado_normalizador asc;

  END Sp_Traer_Tipos_Est_Norm;

  /*********************************************************
  **********************************************************
                   PROCEDIMINETO CON SOBRECARGA DE PARAMETROS
  **********************************************************
  **********************************************************/

  PROCEDURE SP_Traer_Tipo_Sindicos (
    o_Cursor OUT types.cursorType)
  IS
  BEGIN

    IPJ.VARIOS.SP_Traer_Tipo_Sindicos(
      o_cursor => o_cursor,
      p_id_tipo_organismo => 8,
      p_id_ubicacion => 1 );

  END SP_Traer_Tipo_Sindicos ;


  FUNCTION F_DATE(v_date IN VARCHAR2) RETURN NUMBER IS
    v_date1 DATE;
  BEGIN
    select to_date(v_date) into v_date1 from dual;
        RETURN 1;
    Exception WHEN Others THEN
        RETURN 0;
  END;



  PROCEDURE SP_Traer_Tipo_Est_Desarch (
    o_Cursor OUT types.cursorType,
    p_id_estado_entidad in varchar2)
  IS
  /**************************************************
    Lista los estados de los desarchivos electr�nicos
  ***************************************************/
  BEGIN
    OPEN o_Cursor FOR
      select d.id_estado_desarchivo, d.n_estado_desarchivo
      from IPJ.T_TIPOS_ESTADO_DESARCH d;

  END SP_Traer_Tipo_Est_Desarch;
end Varios;
/

