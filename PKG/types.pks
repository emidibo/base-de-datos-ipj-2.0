create or replace package ipj.types
as
  function fc_numero_a_letras(numero in number) return varchar2;
  FUNCTION FC_Nombre_Mes(p_mes IN varchar) return varchar2;
   function fc_numero_a_letras_comp(numero in number) return varchar2;
  type cursorType is ref cursor;

  c_id_Aplicacion constant number(3) := 126;
  c_comunic_mail constant varchar2(2) := '11'; -- CORREO ELECTRONICO
  c_comunic_te constant varchar2(2) := '01'; -- TELEFONO PRINCIPAL
  c_caract_profesion constant  varchar2(4) := '2021'; -- PROFESIONES
  c_habilitar_log_SP constant varchar2(1) := 'N'; -- Habilita o no el logueo de paramtros dentro de los SP

  c_id_Tipo_Socio constant number(1) := 1;
  c_Separador_SUAC constant varchar2(1):= '~';
  c_UnidadRPC constant varchar2(50) := 'REGISTRO PUBLICO DE COMERCIO-RPCDIPJ01';

  c_accion_reserva_SxA constant number(2) := 50;
  c_accion_reserva_ACyF constant number(3) := 84;
  c_accion_reserva_RP constant number(3) := 172;

  -- Areas Nuevo IPJ (para filtrar Tramites en bandeja de entrada)
  c_area_sistema constant number(1) := 6;
  c_area_juridico constant number(1) := 7;
  c_area_archivo constant number(1) := 8;
  c_area_rrhh constant number(2) := 19;
  c_area_direccion constant number(2) := 20;
  c_area_mesaSuac constant number(2) := 24;
  c_area_SRL constant number(1) := 1;
  c_area_SxA constant number(1) := 4;
  c_area_CyF constant number(1) := 5;

  -- Roles
  c_rol_administrador constant number(1) := 1;

  -- Respuestas SP
  c_Reserva_OK constant number(1) := 1;
  c_Resp_OK constant varchar2(2) := 'OK';
  c_Tipo_Mens_ERROR constant number := 0;
  c_Tipo_Mens_WARNING constant number := 1;
  c_Tipo_Mens_OK constant number := 2;

  -- Estados Tramites y Acciones
  c_Estados_Archivo_Inicial constant number(1) := 1;
  c_Estado_Tramites constant varchar2(2) := 'TR';
  c_Estados_Asignado constant number(3) := 2;
  c_Estados_Proceso constant number(3) := 3;
  c_Estados_Revision constant number(3) := 4;
  c_Estados_Observado constant number(3) := 5;
  c_Estados_Firma constant number(3) := 10;
  c_Estados_Boe constant number(3) := 15;
  c_Estados_Anexado constant number(3) := 50;
  c_Estados_Completado constant number(3) := 100;
  c_Estados_Rechazado constant number(3) := 200;
  c_Estados_Perimido constant number(3) := 201;
  c_Estados_Cerrado constant number(3) := 110;

  -- Carga de Datos Hist�ricas
  c_Proceso_Gen_Hist constant number(1) := 4;
  c_Proceso_Hist constant number(1) := 5;

  -- Acciones para informes particulares o controles
  c_Acc_Grav_TractoRegistral constant number(3) := 20;
  c_Tipo_Acc_Interv_ACyF constant number(2) := 72;
  c_Tipo_Acc_Norm_ACyF constant number(3) := 163;
  c_Tipo_Acc_Interv_SxA constant number(2) := 70;
  c_Tipo_Acc_Norm_SxA constant number(2) := 71;
  c_Tipo_Acc_Const_ACyF constant number(2) := 37;
  c_Tipo_Acc_Const_SxA constant number(2) := 31;
  c_Tipo_Acc_Const_SRL constant number(2) := 1;
  c_Tipo_Acc_Retiro constant number(2) := 92;
  c_Tipo_Acc_Archivar_Expediente constant number(2) := 40;

  -- Constantes para NOTAS SUAC
  c_Separador_Nota constant varchar2(1) := '~';
  c_Formulario constant varchar2(10) := 'SFDIPJ01';
  c_Campo_Nota constant varchar2(25) := 'grilla_observaciones_IPJ';

  -- Constantes manejo de Archivo
  c_Max_Tablas constant number(2) := 25;
  c_Tipo_Arch_Constituciones number(1) := 1;
  c_Tipo_Arch_Asambleas number(1) := 2;
  c_Tipo_Arch_Reformas number(1) := 3;
  c_Tipo_Arch_SistMecanizados number(1) := 4;
  c_Tipo_Arch_Perimidas number(1) := 6;
  c_Tipo_Arch_Varios number(1) := 7;


end;
/

create or replace package body ipj.types is

  function numero_menor_mil(numero in number) return varchar2;

  function fc_numero_a_letras_comp(numero in number) return varchar2 is
    v_devolucion varchar2(200);
  begin
    if numero < 0 then
      v_devolucion := 'p�rdida de pesos ' || fc_numero_a_letras(abs(numero));
    else
      v_devolucion := fc_numero_a_letras(numero);
    end if;
    return v_devolucion;
  exception
    when others then
      return 'error ' || sqlerrm;
  end;


  -- implementacion
  function fc_numero_a_letras(numero in number) return varchar2 is
     fuera_de_rango EXCEPTION;
     millares_de_millon number;
     millones number;
     millares number;
     centenas number;
     centimos number;
     en_letras varchar2(200);
     entero number;
     aux varchar2(15);
  begin
      if numero < 0 or numero > 999999999999.99 then
          raise fuera_de_rango;
      end if;

      entero := trunc(numero);
      millares_de_millon := trunc(entero / 1000000000);
      millones := trunc((entero mod 1000000000) / 1000000);
      millares := trunc((entero mod 1000000) / 1000);
      centenas := entero mod 1000;
      centimos := (round(numero,2) * 100) mod 100;

      -- MILLARES DE MILLON
      if millares_de_millon = 1 then
          if millones = 0 then
              en_letras := 'mil millones ';
          else
              en_letras := 'mil ';
          end if;
      elsif millares_de_millon > 1 then

          en_letras := numero_menor_mil(millares_de_millon);

          if millones = 0 then
              en_letras := en_letras || 'mil millones ';
          else
                en_letras := en_letras || 'mil ';
          end if;
      end if;

      -- MILLONES
      if millones = 1 and  millares_de_millon = 0 then
          en_letras := 'un mill�n ';
      elsif millones > 0 then
          en_letras := en_letras || numero_menor_mil(millones) || 'millones ';
      end if;

      -- MILLARES
      if millares = 1 and millares_de_millon = 0 and millones = 0 then
          en_letras := 'mil ';
      elsif millares > 0 then
          en_letras := en_letras || numero_menor_mil(millares) || 'mil ';
      end if;

      -- CENTENAS
      if centenas > 0 or (entero = 0 and centimos = 0) then
          en_letras := en_letras || numero_menor_mil(centenas);
      end if;

      if centimos > 0 then
          if centimos = 1 then
              aux := 'c�ntimo';
          else
              aux := 'c�ntimos';
          end if;
          if entero > 0 then
              en_letras := en_letras || 'con ' || replace(numero_menor_mil(centimos),'uno ','un ') || aux;
          else
              en_letras := en_letras || replace(numero_menor_mil(centimos),'uno','un') || aux;
            end if;
      end if;

      return InitCap(en_letras);

  EXCEPTION
      when fuera_de_rango then
          return('Error: entrada fuera de rango');
      when others then
          raise;
  end;

  function numero_menor_mil(numero in number) return varchar2 is
     fuera_de_rango EXCEPTION;
     no_entero EXCEPTION;
     centenas number;
     decenas number;
     unidades number;
     en_letras varchar2(100);
     unir varchar2(2);
  begin

      if trunc(numero) <> numero then
          raise no_entero;
      end if;

      if numero < 0 or numero > 999 then
          raise fuera_de_rango;
      end if;


      if numero = 100 then
          return ('cien ');
      elsif numero = 0 then
          return ('cero ');
      elsif numero = 1 then
          return ('uno ');
      else
          centenas := trunc(numero / 100);
          decenas  := trunc((numero mod 100)/10);
          unidades := numero mod 10;
          unir := 'y ';

          -- CENTENAS
          if centenas = 1 then
              en_letras := 'ciento ';
          elsif centenas = 2 then
              en_letras := 'doscientos ';
          elsif centenas = 3 then
              en_letras := 'trescientos ';
          elsif centenas = 4 then
              en_letras := 'cuatrocientos ';
          elsif centenas = 5 then
              en_letras := 'quinientos ';
          elsif centenas = 6 then
              en_letras := 'seiscientos ';
          elsif centenas = 7 then
              en_letras := 'setecientos ';
          elsif centenas = 8 then
              en_letras := 'ochocientos ';
          elsif centenas = 9 then
              en_letras := 'novecientos ';
          end if;



          -- DECENAS
          if decenas = 3 then
              en_letras := en_letras || 'treinta ';
          elsif decenas = 4 then
              en_letras := en_letras || 'cuarenta ';
          elsif decenas = 5 then
              en_letras := en_letras || 'cincuenta ';
          elsif decenas = 6 then
              en_letras := en_letras || 'sesenta ';
          elsif decenas = 7 then
              en_letras := en_letras || 'setenta ';
          elsif decenas = 8 then
              en_letras := en_letras || 'ochenta ';
          elsif decenas = 9 then
              en_letras := en_letras || 'noventa ';
          elsif decenas = 1 then
              if unidades < 6 then
                  if unidades = 0 then
                      en_letras := en_letras || 'diez ';
                  elsif unidades = 1 then
                      en_letras := en_letras || 'once ';
                  elsif unidades = 2 then
                      en_letras := en_letras || 'doce ';
                  elsif unidades = 3 then
                      en_letras := en_letras || 'trece ';
                  elsif unidades = 4 then
                      en_letras := en_letras || 'catorce ';
                  elsif unidades = 5 then
                      en_letras := en_letras || 'quince ';
                  end if;
                  unidades := 0;
              else
                  en_letras := en_letras || 'dieci';
                  unir := null;
              end if;
          elsif decenas = 2 then
              if unidades = 0 then
                  en_letras := en_letras || 'veinte ';
              else
                  en_letras := en_letras || 'veinti';
              end if;
              unir := null;
          elsif decenas = 0 then
              unir := null;
          end if;

          -- UNIDADES
          if unidades = 1 then
              en_letras := en_letras || unir || 'uno ';
          elsif unidades = 2 then
              en_letras := en_letras || unir || 'dos ';
          elsif unidades = 3 then
              en_letras := en_letras || unir || 'tres ';
          elsif unidades = 4 then
              en_letras := en_letras || unir || 'cuatro ';
          elsif unidades = 5 then
              en_letras := en_letras || unir || 'cinco ';
          elsif unidades = 6 then
              en_letras := en_letras || unir || 'seis ';
          elsif unidades = 7 then
              en_letras := en_letras || unir || 'siete ';
          elsif unidades = 8 then
              en_letras := en_letras || unir || 'ocho ';
          elsif unidades = 9 then
              en_letras := en_letras || unir || 'nueve ';
          end if;
      end if;

      return(en_letras);

  EXCEPTION
      when no_entero then
          return('Error: entrada no es un n�mero entero');
      when fuera_de_rango then
          return('Error: entrada fuera de rango');
      when others then
          raise;
  end;

   FUNCTION FC_Nombre_Mes(p_mes IN varchar) return varchar2 is
      res varchar2(50);
    /**************************************************
      Obtiene el nombre del mes indicado como MM, en espa�ol
    ***************************************************/
  BEGIN
    select N_MES into res
    from t_comunes.t_meses
    where
      id_mes = p_mes;

    Return initcap(res);
  Exception
    When Others Then
      Return null;
  End FC_Nombre_Mes;

end types;
/

