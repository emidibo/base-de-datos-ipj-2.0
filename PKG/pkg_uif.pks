CREATE OR REPLACE PACKAGE IPJ.PKG_UIF AS
  /*
   ORA-02292 integrity constraint (string.string) violated - child record found
   Cause: An attempt was made to delete a row that is referenced by a foreign key.
  */

  /* invalid number*/
  V_ERROR_01722 EXCEPTION;
  PRAGMA EXCEPTION_INIT(V_ERROR_01722, -01722);

  V_ERROR_2292 EXCEPTION;
  PRAGMA EXCEPTION_INIT(V_ERROR_2292, -2292);

  /*
  ORA-00001 unique constraint (string.string) violated
  Cause: An UPDATE or INSERT statement attempted to insert a duplicate key.
  */
  V_ERROR_00001 EXCEPTION;
  PRAGMA EXCEPTION_INIT(V_ERROR_00001, -00001);

  /*
  ORA-01407 cannot update (string) to NULL
  Cause: An attempt was made to update a table column USER.TABLE.COLUMN with a NULL val
  */
  V_ERROR_01407 EXCEPTION;
  PRAGMA EXCEPTION_INIT(V_ERROR_01407, -01407);

  TYPE TY_CURSOR IS REF CURSOR;

  PROCEDURE BUSCAR_CONSTITUCION_SOCIEDADES(p_T_CONSULTA  OUT TY_CURSOR,
                                           P_FECHA_DESDE IN IPJ.t_Entidades.FEC_INSCRIPCION%TYPE DEFAULT NULL,
                                           P_FECHA_HASTA IN IPJ.t_Entidades.FEC_INSCRIPCION%TYPE DEFAULT NULL);

  PROCEDURE SOCIO_PERSONA_FISICA(p_T_CONSULTA  OUT TY_CURSOR,
                                 P_FECHA_DESDE IN IPJ.t_Entidades.FEC_INSCRIPCION%TYPE DEFAULT NULL,
                                 P_FECHA_HASTA IN IPJ.t_Entidades.FEC_INSCRIPCION%TYPE DEFAULT NULL,
                                 P_ID_LEGAJO   IN IPJ.t_Entidades.ID_LEGAJO%TYPE DEFAULT NULL);

  PROCEDURE SOCIO_PERSONA_JURIDICA(p_T_CONSULTA  OUT TY_CURSOR,
                                   P_FECHA_DESDE IN IPJ.t_Entidades.FEC_INSCRIPCION%TYPE DEFAULT NULL,
                                   P_FECHA_HASTA IN IPJ.t_Entidades.FEC_INSCRIPCION%TYPE DEFAULT NULL,
                                   P_ID_LEGAJO   IN IPJ.t_Entidades.ID_LEGAJO%TYPE DEFAULT NULL);

  PROCEDURE ENTIDADES_SIN_FINES_DE_LUCRO(p_T_CONSULTA  OUT TY_CURSOR,
                                         P_FECHA_DESDE IN IPJ.t_Entidades.FEC_INSCRIPCION%TYPE DEFAULT NULL,
                                         P_FECHA_HASTA IN IPJ.t_Entidades.FEC_INSCRIPCION%TYPE DEFAULT NULL);

  PROCEDURE PERSONAS_FISICAS_MAS_DE5_JURI(p_T_CONSULTA  OUT TY_CURSOR,
                                          P_FECHA_DESDE IN IPJ.t_Entidades.FEC_INSCRIPCION%TYPE DEFAULT NULL,
                                          P_FECHA_HASTA IN IPJ.t_Entidades.FEC_INSCRIPCION%TYPE DEFAULT NULL);

  PROCEDURE MAS_DE5_PERSONAS_JURIDICAS(p_T_CONSULTA  OUT TY_CURSOR,
                                       P_FECHA_DESDE IN IPJ.t_Entidades.FEC_INSCRIPCION%TYPE DEFAULT NULL,
                                       P_FECHA_HASTA IN IPJ.t_Entidades.FEC_INSCRIPCION%TYPE DEFAULT NULL,
                                       P_DOCUMENTO   IN IPJ.t_integrantes.NRO_DOCUMENTO%TYPE DEFAULT NULL);

  PROCEDURE BUSCAR_ENTIDADES_MISMO_DOMI(p_T_CONSULTA  OUT TY_CURSOR,
                                        P_FECHA_DESDE IN IPJ.t_Entidades.FEC_INSCRIPCION%TYPE DEFAULT NULL,
                                        P_FECHA_HASTA IN IPJ.t_Entidades.FEC_INSCRIPCION%TYPE DEFAULT NULL,
                                        P_CALLE       IN dom_manager.vt_domicilios_cond.n_calle%TYPE DEFAULT NULL,
                                        P_NRO         IN dom_manager.vt_domicilios_cond.altura%TYPE DEFAULT NULL);

  PROCEDURE BUSCAR_MISMO_DOMICILIO_PF(p_T_CONSULTA   OUT TY_CURSOR,
                                      P_FECHA_DESDE  IN IPJ.t_Entidades.FEC_INSCRIPCION%TYPE DEFAULT NULL,
                                      P_FECHA_HASTA  IN IPJ.t_Entidades.FEC_INSCRIPCION%TYPE DEFAULT NULL,
                                      P_ID_DOMICILIO IN IPJ.t_Entidades.ID_LEGAJO%TYPE DEFAULT NULL);

  PROCEDURE BUSCAR_MISMO_DOMICILIO_PJ(p_T_CONSULTA   OUT TY_CURSOR,
                                      P_FECHA_DESDE  IN IPJ.t_Entidades.FEC_INSCRIPCION%TYPE DEFAULT NULL,
                                      P_FECHA_HASTA  IN IPJ.t_Entidades.FEC_INSCRIPCION%TYPE DEFAULT NULL,
                                      P_ID_DOMICILIO IN IPJ.t_Entidades.ID_LEGAJO%TYPE DEFAULT NULL);

  PROCEDURE LLENAR_TABLA_DOMICILIO;

END PKG_UIF;
/

CREATE OR REPLACE PACKAGE BODY IPJ.PKG_UIF AS

  PROCEDURE BUSCAR_CONSTITUCION_SOCIEDADES(p_T_CONSULTA  OUT TY_CURSOR,
                                           P_FECHA_DESDE IN IPJ.t_Entidades.FEC_INSCRIPCION%TYPE DEFAULT NULL,
                                           P_FECHA_HASTA IN IPJ.t_Entidades.FEC_INSCRIPCION%TYPE DEFAULT NULL) IS
    V_MSG         VARCHAR2(300);
    V_FECHA_DESDE DATE;
    V_FECHA_HASTA DATE;
    V_SENTENCIA   VARCHAR2(50);

  BEGIN

    V_FECHA_DESDE := P_FECHA_DESDE;
    V_FECHA_HASTA := P_FECHA_HASTA;

    V_SENTENCIA := '  ALTER SESSION SET NLS_DATE_FORMAT=' || '''' ||
                   'DD/MM/YYYY' || '''';

    EXECUTE IMMEDIATE V_SENTENCIA;

    OPEN p_T_CONSULTA FOR

    ------CONSTITUCIONES TODAS DESDE OCTUBRE 2020 y socios BEA

      SELECT distinct REPLACE((upper(substr(e.denominacion_1, 1, 1)) ||
                              lower(SUBSTR(e.denominacion_1, 2))),
                              '\&',
                              '\&amp;') AS "Denominaci93n",

                      CASE translate(UPPER(te.tipo_entidad),
                                 '����������',
                                 'aeiouAEIOU')

                        WHEN UPPER('Sociedad de bolsa') THEN
                         'Sociedad de bolsa' --

                        WHEN UPPER('Sociedad colectiva') THEN
                         'Sociedad colectiva' --

                        WHEN UPPER('Sociedades en comandita simple') THEN
                         'Sociedad en comandita simple' --

                        WHEN UPPER('Sociedad de capital e industria') THEN
                         'Sociedad de capital e industria' --

                        WHEN UPPER('Sociedad de responsabilidad limitada') THEN
                         'Sociedad de responsabilidad limitada' --

                        WHEN UPPER('Sociedad anonima') THEN
                         'Sociedad an�nima' --

                        WHEN UPPER('Sociedades del estado') THEN
                         'Sociedad del estado' --

                        WHEN UPPER('Sociedad en comandita por acciones') THEN
                         'Sociedad en comandita por acciones' --

                        WHEN UPPER('Sociedad de hecho') THEN
                         'Sociedad de hecho' --

                        WHEN UPPER('Sociedad constituida en el extranjero') THEN
                         'Sociedad constituida en el extranjero' --

                        WHEN
                         UPPER('Sociedad binacional fuera de jurisdiccion') THEN
                         'Sociedad binacional fuera de jurisdicci�n' --

                        WHEN UPPER('Asociacion civil') THEN
                         'Asociaci�n civil' --

                        WHEN UPPER('Entidad extranjera sin fines de lucro') THEN
                         'Entidad extranjera sin fines de lucro' --

                        WHEN UPPER('Fundacion') THEN
                         'Fundacion' --

                        WHEN UPPER('Simples asociaciones') THEN
                         'Simples asociaciones' --

                        WHEN UPPER('Federacion') THEN
                         'Federaci�n' --

                        WHEN UPPER('Confederacion') THEN
                         'Confederaci�n' --

                        WHEN UPPER('C�mara') THEN
                         'C�mara' --

                        WHEN UPPER('Contrato de colaboracion empresaria') THEN
                         'Contrato de colaboraci�n empresaria' --

                        WHEN UPPER('Union transitoria') THEN
                         'Uni�n transitoria de empresas' --

                        WHEN UPPER('Sociedad de garantia reciproca') THEN
                         'Sociedad de garant�a rec�iproca' --

                        WHEN UPPER('Soc. capitalizacion y ahorro') THEN
                         'Soc. capitalizacion y ahorro"' --

                        WHEN UPPER('Cooperativas') THEN
                         'Cooperativa' --

                        WHEN UPPER('Mutuales') THEN
                         'Mutual' --

                        WHEN UPPER('Fideicomisos') THEN
                         'Fideicomiso' --

                        ELSE
                         'Otros'
                      END AS Tipo_Sociedad,

                      e.cuit      "CUIT_CUIL",
                      E.ID_LEGAJO ID_LEGAJO,

                      --  TO_NUMBER ( trim(REPLACE (translate (upper (e.nro_resolucion),'AB / " - ','*') ,'*','')))"Nro_de_denominaci93n" ,--JP

                      to_char(to_date(e.acta_contitutiva, 'dd/mm/yyyy'),
                              'yyyy-MM-dd') || 'T00:00:00-03:00' /*E.ACTA_CONTITUTIVA*/ "Fecha_de_Constituci93n",

                      /*DBMS_LOB.substr(e.objeto_social,
                      DBMS_LOB.getlength(e.objeto_social),
                      1)*/
                      null as Objeto_Social,

                      /* (select decode((upper(substr(m.n_moneda, 1, 1)) ||
                                   lower(SUBSTR(m.n_moneda, 2))),
                                   'Peso',
                                   'Peso Argentino',
                                   (upper(substr(m.n_moneda, 1, 1)) ||
                                   lower(SUBSTR(m.n_moneda, 2))))
                       from t_comunes.t_monedas m
                      where m.id_moneda = e.id_moneda) "Tipo_de_Moneda",
                      */
                      --se cambio el 9/11/2021
                      decode((select decode((upper(substr(m.n_moneda, 1, 1)) ||
                                           lower(SUBSTR(m.n_moneda, 2))),
                                           'Peso',
                                           'Peso Argentino',
                                           (upper(substr(m.n_moneda, 1, 1)) ||
                                           lower(SUBSTR(m.n_moneda, 2))))
                               from t_comunes.t_monedas m
                              where m.id_moneda = e.id_moneda),
                             null,
                             'Peso Argentino',
                             (select decode((upper(substr(m.n_moneda, 1, 1)) ||
                                            lower(SUBSTR(m.n_moneda, 2))),
                                            'Peso',
                                            'Peso Argentino',
                                            (upper(substr(m.n_moneda, 1, 1)) ||
                                            lower(SUBSTR(m.n_moneda, 2))))
                                from t_comunes.t_monedas m
                               where m.id_moneda = e.id_moneda)) "Tipo_de_Moneda",

                      e.monto "Valor_del_Capital_Social",

                      e.monto "Valor_del_Capital_Social_en_Pesos",

                      e.monto "Monto_en_Efectivo",

                      0 Valor_Estimado_de_Bienes,
                      /*e.cuotas,
                      e.valor_cuota,*/

                      (select (upper(substr(d.n_calle, 1, 1)) ||
                              lower(SUBSTR(d.n_calle, 2)))
                         from dom_manager.vt_domicilios_cond d
                        where d.id_vin =
                              IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(l.id_legajo,
                                                                    'S')) "Calle",

                      (select decode(d.altura, null, 0, d.altura) --agregado 11/08/2021
                         from dom_manager.vt_domicilios_cond d
                        where d.id_vin =
                              IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(l.id_legajo,
                                                                    'S')) "Nro",

                      (select d.piso
                         from dom_manager.vt_domicilios_cond d
                        where d.id_vin =
                              IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(l.id_legajo,
                                                                    'S')) "Piso",

                      (select d.depto
                         from dom_manager.vt_domicilios_cond d
                        where d.id_vin =
                              IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(l.id_legajo,
                                                                    'S')) "Departamento",

                      (select (upper(substr(d.N_Localidad, 1, 1)) ||
                              lower(SUBSTR(d.N_Localidad, 2)))
                         from dom_manager.vt_domicilios_cond d
                        where d.id_vin =
                              IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(l.id_legajo,
                                                                    'S')) "Localidad",

                      (select d.cpa
                         from dom_manager.vt_domicilios_cond d
                        where d.id_vin =
                              IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(l.id_legajo,
                                                                    'S')) "C93digo_Postal",

                      (select (upper(substr(d.n_provincia, 1, 1)) ||
                              lower(SUBSTR(d.n_provincia, 2)))
                         from dom_manager.vt_domicilios_cond d
                        where d.id_vin =
                              IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(l.id_legajo,
                                                                    'S')) "Provincia",

                      (select (upper(substr(pa.n_pais, 1, 1)) ||
                              lower(SUBSTR(pa.n_pais, 2)))
                         from dom_manager.vt_paises pa
                        where pa.ID_PAIS = 'ARG') "Pa92s",

                      '' as Prefijo,
                      '' as Tel91fono,
                      '' as Email

        from ipj.t_tipos_entidades         te,
             ipj.t_tramitesipj             tr,
             nuevosuac.vt_tramites_id      suactr,
             nuevosuac.vt_subtipos_tramite subtip,
             ipj.t_legajos                 l
        join ipj.t_entidades e
          on l.id_legajo = e.id_legajo

        join (select distinct i.id_integrante,
                              i.id_legajo,
                              i.id_tramite_ipj,
                              i.id_tipo_integrante,
                              i.fecha_fin,
                              i.persona_expuesta

                from ipj.t_entidades_admin i
                join ipj.t_tramitesipj tr
                  on i.id_tramite_ipj = tr.id_tramite_ipj
                join ipj.t_legajos l
                  on l.id_legajo = i.id_legajo
               where nvl(i.id_integrante, 0) > 0
                 and -- Personas F�sicas
                     nvl(i.fecha_fin, sysdate) >= trunc(sysdate)
                 and -- Vigentes
                     nvl(l.eliminado, 0) <> 1
                 and -- Entidades no eliminadas
                     l.fecha_baja_entidad is null
                 and -- Entidades no dadas de baja
                     tr.id_estado_ult between 100 and 199 -- En tr�mites inscriptos
              UNION
              select distinct i.id_integrante,
                              i.id_legajo,
                              i.id_tramite_ipj,
                              i.id_tipo_integrante,
                              i.fecha_baja,
                              i.persona_expuesta
                from ipj.t_entidades_sindico i
                join ipj.t_tramitesipj tr
                  on i.id_tramite_ipj = tr.id_tramite_ipj
                join ipj.t_legajos l
                  on l.id_legajo = i.id_legajo
               where nvl(i.id_integrante, 0) > 0
                 and -- Personas F�sicas
                     nvl(i.fecha_baja, sysdate) >= trunc(sysdate)
                 and -- Vigentes
                     nvl(l.eliminado, 0) <> 1
                 and -- Entidades no eliminadas
                     l.fecha_baja_entidad is null
                 and -- Entidades no dadas de baja
                     tr.id_estado_ult between 100 and 199 -- En tr�mites inscriptos
              UNION
              select distinct i.id_integrante,
                              i.id_legajo,
                              i.id_tramite_ipj,
                              51,
                              i.fecha_baja,
                              i.persona_expuesta
                from ipj.t_entidades_representante i
                join ipj.t_tramitesipj tr
                  on i.id_tramite_ipj = tr.id_tramite_ipj
                join ipj.t_legajos l
                  on l.id_legajo = i.id_legajo
               where nvl(i.id_integrante, 0) > 0
                 and -- Personas F�sicas
                     nvl(i.fecha_baja, sysdate) >= trunc(sysdate)
                 and -- Vigentes
                     nvl(l.eliminado, 0) <> 1
                 and -- Entidades no eliminadas
                     l.fecha_baja_entidad is null
                 and -- Entidades no dadas de baja
                     tr.id_estado_ult between 100 and 199 -- En tr�mites inscriptos
              UNION
              select distinct i.id_integrante,
                              i.id_legajo,
                              i.id_tramite_ipj,
                              46,
                              i.fecha_fin,
                              i.persona_expuesta
                from ipj.t_entidades_benef i
                join ipj.t_tramitesipj tr
                  on i.id_tramite_ipj = tr.id_tramite_ipj
                join ipj.t_legajos l
                  on l.id_legajo = i.id_legajo
               where nvl(i.id_integrante, 0) > 0
                 and -- Personas F�sicas
                     nvl(i.fecha_fin, sysdate) >= trunc(sysdate)
                 and -- Vigentes
                     nvl(l.eliminado, 0) <> 1
                 and -- Entidades no eliminadas
                     l.fecha_baja_entidad is null
                 and -- Entidades no dadas de baja
                     tr.id_estado_ult between 100 and 199 -- En tr�mites inscriptos
              UNION
              select distinct i.id_integrante,
                              i.id_legajo,
                              i.id_tramite_ipj,
                              46,
                              i.fecha_fin,
                              i.persona_expuesta
                from ipj.t_entidades_fideicom i
                join ipj.t_tramitesipj tr
                  on i.id_tramite_ipj = tr.id_tramite_ipj
                join ipj.t_legajos l
                  on l.id_legajo = i.id_legajo
               where nvl(i.id_integrante, 0) > 0
                 and -- Personas F�sicas
                     nvl(i.fecha_fin, sysdate) >= trunc(sysdate)
                 and -- Vigentes
                     nvl(l.eliminado, 0) <> 1
                 and -- Entidades no eliminadas
                     l.fecha_baja_entidad is null
                 and -- Entidades no dadas de baja
                     tr.id_estado_ult between 100 and 199 -- En tr�mites inscriptos
              UNION
              select distinct i.id_integrante,
                              i.id_legajo,
                              i.id_tramite_ipj,
                              46,
                              i.fecha_fin,
                              i.persona_expuesta
                from ipj.t_entidades_fiduciantes i
                join ipj.t_tramitesipj tr
                  on i.id_tramite_ipj = tr.id_tramite_ipj
                join ipj.t_legajos l
                  on l.id_legajo = i.id_legajo
               where nvl(i.id_integrante, 0) > 0
                 and -- Personas F�sicas
                     nvl(i.fecha_fin, sysdate) >= trunc(sysdate)
                 and -- Vigentes
                     nvl(l.eliminado, 0) <> 1
                 and -- Entidades no eliminadas
                     l.fecha_baja_entidad is null
                 and -- Entidades no dadas de baja
                     tr.id_estado_ult between 100 and 199 -- En tr�mites inscriptos
              UNION
              select distinct i.id_integrante,
                              i.id_legajo,
                              i.id_tramite_ipj,
                              46,
                              i.fecha_fin,
                              i.persona_expuesta
                from ipj.t_entidades_fiduciarios i
                join ipj.t_tramitesipj tr
                  on i.id_tramite_ipj = tr.id_tramite_ipj
                join ipj.t_legajos l
                  on l.id_legajo = i.id_legajo
               where nvl(i.id_integrante, 0) > 0
                 and -- Personas F�sicas
                     nvl(i.fecha_fin, sysdate) >= trunc(sysdate)
                 and -- Vigentes
                     nvl(l.eliminado, 0) <> 1
                 and -- Entidades no eliminadas
                     l.fecha_baja_entidad is null
                 and -- Entidades no dadas de baja
                     tr.id_estado_ult between 100 and 199 -- En tr�mites inscriptos
              UNION
              select distinct i.id_integrante,
                              i.id_legajo,
                              i.id_tramite_ipj,
                              i.id_tipo_integrante,
                              i.fecha_fin,
                              i.persona_expuesta
                from ipj.t_entidades_socios i
                join ipj.t_tramitesipj tr
                  on i.id_tramite_ipj = tr.id_tramite_ipj
                join ipj.t_legajos l
                  on l.id_legajo = i.id_legajo
               where nvl(i.id_integrante, 0) > 0
                 and -- Personas F�sicas
                     nvl(i.fecha_fin, sysdate) >= trunc(sysdate)
                 and -- Vigentes
                     nvl(l.eliminado, 0) <> 1
                 and -- Entidades no eliminadas
                     l.fecha_baja_entidad is null
                 and -- Entidades no dadas de baja
                     tr.id_estado_ult between 100 and 199 -- En tr�mites inscriptos
              UNION
              select distinct s.id_integrante,
                              s.id_legajo,
                              s.id_tramite_ipj,
                              s.id_tipo_integrante,
                              s.fecha_fin,
                              s.persona_expuesta
                from ipj.t_entidades_socios_copro i
                join ipj.t_entidades_socios s
                  on i.id_entidad_socio = s.id_entidad_socio
                join ipj.t_tramitesipj tr
                  on s.id_tramite_ipj = tr.id_tramite_ipj
                join ipj.t_legajos l
                  on l.id_legajo = s.id_legajo
               where nvl(i.id_integrante, 0) > 0
                 and -- Personas F�sicas
                     nvl(i.fec_baja, sysdate) >= trunc(sysdate)
                 and -- Vigentes
                     nvl(l.eliminado, 0) <> 1
                 and -- Entidades no eliminadas
                     l.fecha_baja_entidad is null
                 and -- Entidades no dadas de baja
                     tr.id_estado_ult between 100 and 199 -- En tr�mites inscriptos
              UNION
              select distinct s.id_integrante,
                              s.id_legajo,
                              s.id_tramite_ipj,
                              s.id_tipo_integrante,
                              s.fecha_fin,
                              s.persona_expuesta
                from ipj.t_entidades_socios_usuf i
                join ipj.t_entidades_socios s
                  on i.id_entidad_socio = s.id_entidad_socio
                join ipj.t_tramitesipj tr
                  on s.id_tramite_ipj = tr.id_tramite_ipj
                join ipj.t_legajos l
                  on l.id_legajo = s.id_legajo
               where nvl(i.id_integrante, 0) > 0
                 and -- Personas F�sicas
                     nvl(i.fec_baja, sysdate) >= trunc(sysdate)
                 and -- Vigentes
                     nvl(l.eliminado, 0) <> 1
                 and -- Entidades no eliminadas
                     l.fecha_baja_entidad is null
                 and -- Entidades no dadas de baja
                     tr.id_estado_ult between 100 and 199 -- En tr�mites inscriptos
              ) ad

          on e.id_tramite_ipj = ad.id_tramite_ipj

         and e.id_legajo = ad.id_legajo
        join ipj.t_integrantes i
          on i.id_integrante = ad.id_integrante
        join ipj.t_tipos_integrante ti
          on ti.id_tipo_integrante = ad.id_tipo_integrante
      /* join ipj.t_tipos_organismo o
      on o.id_tipo_organismo = ad.id_tipo_organismo*/ --22/10/2021 comentado con fer,lu y bea

       where te.id_tipo_entidad = e.id_tipo_entidad
         and tr.id_tramite_ipj = e.id_tramite_ipj
         and suactr.id_tramite = tr.id_tramite
         and subtip.id_subtipo_tramite = suactr.id_subtipo_tramite

         and te.id_ubicacion in (1, 4)
            -- excluir las entidades que tienen persona juridica
            /*  and not exists
            (select 1
                     from ipj.t_entidades_socios es1
                    where (es1.cuit_empresa is not null or
                          es1.n_empresa is not null)
                      and es1.borrador = 'N'
                      and e.id_legajo = es1.id_legajo
                      and e.id_tramite_ipj = es1.id_tramite_ipj
                      and es1.fecha_fin is null)*/ --22/10/2021 comentado con fer,lu y bea

            --INICIO sector valores null
         and e.denominacion_1 is not null
         and te.tipo_entidad is not null
         and length(e.acta_contitutiva) = 10 --el requerimiento dice que no puede estar vacio, y el formato es de 10 digitos (10/01/2012)
            -- por lo tanto debe cumplir eso para su correcta transformacion.

         and e.monto is not null
         and (select d.n_calle
                from dom_manager.vt_domicilios_cond d
               where d.id_vin =
                     IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(l.id_legajo, 'S')) is not null

         and (select decode(d.altura, null, 0, d.altura) --agregado 11/08/2021
                from dom_manager.vt_domicilios_cond d
               where d.id_vin =
                     IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(l.id_legajo, 'S')) is not null

         and (select (upper(substr(d.N_Localidad, 1, 1)) ||
                     lower(SUBSTR(d.N_Localidad, 2)))
                from dom_manager.vt_domicilios_cond d
               where d.id_vin =
                     IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(l.id_legajo, 'S')) is not null

         and (select (upper(substr(d.n_provincia, 1, 1)) ||
                     lower(SUBSTR(d.n_provincia, 2)))
                from dom_manager.vt_domicilios_cond d
               where d.id_vin =
                     IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(l.id_legajo, 'S')) is not null
         and (select (upper(substr(pa.n_pais, 1, 1)) ||
                     lower(SUBSTR(pa.n_pais, 2)))
                from dom_manager.vt_paises pa
               where pa.ID_PAIS = 'ARG') is not null

         and (select (upper(substr(vp.APELLIDO, 1, 1)) ||
                     lower(SUBSTR(vp.APELLIDO, 2)))
                from rcivil.vt_pk_persona vp
               where vp.id_sexo = i.id_sexo
                 and vp.nro_documento = i.nro_documento
                 and vp.pai_cod_pais = i.pai_cod_pais
                 and vp.id_numero = i.id_numero) is not null

         and (select initcap(vp.nombre)
                from rcivil.vt_pk_persona vp
               where vp.id_sexo = i.id_sexo
                 and vp.nro_documento = i.nro_documento
                 and vp.pai_cod_pais = i.pai_cod_pais
                 and vp.id_numero = i.id_numero) is not null

         and decode(ad.persona_expuesta, 'S', 'true', 'false') is not null
            -- FIN sector valores null

         and subtip.id_subtipo_tramite in ('39451',
                                           '39452',
                                           '39453',
                                           '39480',
                                           '39547',
                                           '39549',
                                           '39586',
                                           '39587',
                                           '189',
                                           '1396',
                                           '1397',
                                           '1398',
                                           '1399',
                                           '1486',
                                           '48344',
                                           '39582',
                                           '51060',
                                           '54853', --agregado el 11/08/2021
                                           '39556', --agregado el 11/08/2021
                                           '54862', --agregado el 11/08/2021
                                           '54868', --agregado el 11/08/2021
                                           '39549', --agregado el 11/08/2021
                                           '54649' --agregado el 11/08/2021
                                           )
         and tr.id_estado_ult between 100 and 199

         and E.BORRADOR = 'N'

         and ti.id_tipo_integrante IN ('1', --de aca
                                       '2',
                                       '3',
                                       '4',
                                       '5',
                                       '6',
                                       '7',
                                       '8',
                                       '9',
                                       '10',
                                       '11',
                                       '13',
                                       '14',
                                       '15',
                                       '16',
                                       '18',
                                       '19',
                                       '20',
                                       '21',
                                       '22',
                                       '23',
                                       '24',
                                       '25',
                                       '28',
                                       '29',
                                       '30',
                                       '31',
                                       '32',
                                       '33',
                                       '34',
                                       '35',
                                       '36',
                                       '37',
                                       '38',
                                       '39',
                                       '40',
                                       '41',
                                       '42',
                                       '43',
                                       '44',
                                       '45',
                                       '46',
                                       '47',
                                       '48',
                                       '49',
                                       '50',
                                       '51',
                                       '52',
                                       '53',
                                       '54',
                                       '55',
                                       '56',
                                       '57',
                                       '58',
                                       '59',
                                       '60',
                                       '61',
                                       '62',
                                       '63',
                                       '64',
                                       --hasta aca agregado el 22/10/2021 comentado con fer,lu y bea
                                       '5',
                                       '3',
                                       '24',
                                       '2',
                                       '9',
                                       '11',
                                       --'29',--
                                       '38',
                                       '51',
                                       '52',
                                       '28',
                                       '30',
                                       '34',
                                       '1',
                                       '14',
                                       '42',
                                       '49',
                                       '39',
                                       '20',
                                       '23',
                                       '10')

         and te.id_tipo_entidad NOT IN ('5',
                                        '9',
                                        '11',
                                        '12',
                                        '16',
                                        '20',
                                        '21',
                                        '22',
                                        '29', --hasta que se vea bien lo de fideicomiso
                                        '31',
                                        '32',
                                        '33',
                                        '34',
                                        '35',
                                        '36',
                                        '37',
                                        '38',
                                        '39',
                                        -- '40',--comentado el 11/08/2021
                                        '41',
                                        '42',
                                        '43',
                                        '44')
            --comentado el 11/08/2021
            /* and (ad.fecha_fin >= to_date(sysdate, 'dd/mm/rrrr') or
            ad.fecha_fin =
            (select max(adm.fecha_fin)
                from ipj.t_tramitesipj tr
                join ipj.t_entidades_admin adm
                  on tr.id_tramite_ipj = adm.id_tramite_ipj
               where adm.id_legajo = l.id_legajo
                 and tr.id_estado_ult between 100 and 199))*/ --22/10/2021 comentado con fer,lu y bea

         and nvl(e.fec_inscripcion, e.fec_resolucion) >= V_FECHA_DESDE
         and nvl(e.fec_inscripcion, e.fec_resolucion) <= V_FECHA_HASTA
      --order by te.tipo_entidad
      ;

  EXCEPTION
    WHEN V_ERROR_00001 THEN
      V_MSG := 'Mal.';
  END BUSCAR_CONSTITUCION_SOCIEDADES;

  PROCEDURE SOCIO_PERSONA_FISICA(p_T_CONSULTA  OUT TY_CURSOR,
                                 P_FECHA_DESDE IN IPJ.t_Entidades.FEC_INSCRIPCION%TYPE DEFAULT NULL,
                                 P_FECHA_HASTA IN IPJ.t_Entidades.FEC_INSCRIPCION%TYPE DEFAULT NULL,
                                 P_ID_LEGAJO   IN IPJ.t_Entidades.ID_LEGAJO%TYPE DEFAULT NULL) IS
    V_MSG         VARCHAR2(300);
    V_FECHA_DESDE DATE;
    V_FECHA_HASTA DATE;
    V_SENTENCIA   VARCHAR2(50);
    v_ID_LEGAJO   number;

  BEGIN

    V_FECHA_DESDE := P_FECHA_DESDE;
    V_FECHA_HASTA := P_FECHA_HASTA;
    v_ID_LEGAJO   := P_ID_LEGAJO;

    V_SENTENCIA := '  ALTER SESSION SET NLS_DATE_FORMAT=' || '''' ||
                   'DD/MM/YYYY' || ''''; -- modificado por E.J 2010-04-22 para que tenga en cuenta hh:mm_ss
    --dd/mm/yyyy hh24:mi:ss

    EXECUTE IMMEDIATE V_SENTENCIA;

    OPEN p_T_CONSULTA FOR

    ------CONSTITUCIONES TODAS DESDE OCTUBRE 2020 y socios BEA

      select
      --)Socio_Persona_F92sica
       (select (upper(substr(vp.APELLIDO, 1, 1)) ||
               lower(SUBSTR(vp.APELLIDO, 2)))
          from rcivil.vt_pk_persona vp
         where vp.id_sexo = i.id_sexo
           and vp.nro_documento = i.nro_documento
           and vp.pai_cod_pais = i.pai_cod_pais
           and vp.id_numero = i.id_numero) "Apellido88PerFisica",

       '' as Segundo_Apellido88PerFisica,

       (select initcap(vp.nombre)
          from rcivil.vt_pk_persona vp
         where vp.id_sexo = i.id_sexo
           and vp.nro_documento = i.nro_documento
           and vp.pai_cod_pais = i.pai_cod_pais
           and vp.id_numero = i.id_numero) "Nombre88PerFisica",

       '' as Segundo_Nombre88PerFisica,

       (select vp.n_tipo_documento
          from rcivil.vt_pk_persona vp
         where vp.id_sexo = i.id_sexo
           and vp.nro_documento = i.nro_documento
           and vp.pai_cod_pais = i.pai_cod_pais
           and vp.id_numero = i.id_numero) "Tipo_Documento88PerFisica",

       i.nro_documento "N94mero_documento88PerFisica",

       (select vp.cuil
          from rcivil.vt_pk_persona vp
         where vp.id_sexo = i.id_sexo
           and vp.nro_documento = i.nro_documento
           and vp.pai_cod_pais = i.pai_cod_pais
           and vp.id_numero = i.id_numero) "Nro_de_CUIT_CUIL88PerFisica",

       'false' as Radicada_en_el_Exterior88PerFisica,
       'false' as Radicada_en_Paraiso_Fiscal88PerFisica,

       decode(ad.persona_expuesta, 'S', 'true', 'false') "PEP88PerFisica",

       /*       (select (upper(substr(d.n_calle, 1, 1)) ||
             lower(SUBSTR(d.n_calle, 2)))
        from DOM_MANAGER.VT_DOMICILIOS_COND d
       where d.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo,
                                                         i.nro_documento,
                                                         i.pai_cod_pais,
                                                         to_char(i.id_numero),
                                                         3)) "Calle88PerFisica",*/
       --actualizado el 9/11/2021
       (select decode((upper(substr(d.n_calle, 1, 1)) ||
                      lower(SUBSTR(d.n_calle, 2))),
                      null,
                      'Sin Calle',
                      (upper(substr(d.n_calle, 1, 1)) ||
                      lower(SUBSTR(d.n_calle, 2))))
          from DOM_MANAGER.VT_DOMICILIOS_COND d
         where d.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo,
                                                           i.nro_documento,
                                                           i.pai_cod_pais,
                                                           to_char(i.id_numero),
                                                           3)) "Calle88PerFisica",

       (select decode(d.altura,
                      null,
                      '0',
                      decode(UPPER(d.altura), 'S/N', '0', d.altura))
          from DOM_MANAGER.VT_DOMICILIOS_COND d
         where d.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo,
                                                           i.nro_documento,
                                                           i.pai_cod_pais,
                                                           to_char(i.id_numero),
                                                           3)) "Nro88PerFisica",
       (select d.piso
          from DOM_MANAGER.VT_DOMICILIOS_COND d
         where d.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo,
                                                           i.nro_documento,
                                                           i.pai_cod_pais,
                                                           to_char(i.id_numero),
                                                           3)) "Piso88PerFisica",
       (select d.depto
          from DOM_MANAGER.VT_DOMICILIOS_COND d
         where d.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo,
                                                           i.nro_documento,
                                                           i.pai_cod_pais,
                                                           to_char(i.id_numero),
                                                           3)) "Departamento88PerFisica",
       (select (upper(substr(d.n_localidad, 1, 1)) ||
               lower(SUBSTR(d.n_localidad, 2)))
          from DOM_MANAGER.VT_DOMICILIOS_COND d
         where d.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo,
                                                           i.nro_documento,
                                                           i.pai_cod_pais,
                                                           to_char(i.id_numero),
                                                           3)) "Localidad88PerFisica",
       (select d.cpa
          from DOM_MANAGER.VT_DOMICILIOS_COND d
         where d.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo,
                                                           i.nro_documento,
                                                           i.pai_cod_pais,
                                                           to_char(i.id_numero),
                                                           3)) "C93digo_Postal88PerFisica",
       (select (upper(substr(d.n_provincia, 1, 1)) ||
               lower(SUBSTR(d.n_provincia, 2)))
          from dom_manager.vt_domicilios_cond d
         where d.id_vin =
               IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(l.id_legajo, 'S')) "Provincia88PerFisica",

       (select INITCAP(pa.n_pais)
          from dom_manager.vt_paises pa
         where pa.ID_PAIS = i.pai_cod_pais) "Pa92s88PerFisica",

       /*IPJ.VARIOS.FC_Buscar_Te_Caract(i.id_sexo || i.nro_documento ||
       i.pai_cod_pais ||
       to_char(i.id_numero),
       126)*/

       '' "Prefijo88PerFisica",

       /*IPJ.VARIOS.FC_Buscar_Telefono(i.id_sexo || i.nro_documento ||
       i.pai_cod_pais || to_char(i.id_numero),
       126) */

       '' "Tel91fono88PerFisica",

       IPJ.VARIOS.FC_BUSCAR_MAIL(i.id_sexo || i.nro_documento ||
                                 i.pai_cod_pais || to_char(i.id_numero),
                                 126) "Email88PerFisica",

       /*  (select decode(vp.FECHA_NACIMIENTO,
                    null,
                    '',
                    to_char(vp.FECHA_NACIMIENTO, 'yyyy-MM-dd') ||
                    'T00:00:00-03:00')
        from rcivil.vt_pk_persona vp
       where vp.id_sexo = i.id_sexo
         and vp.nro_documento = i.nro_documento
         and vp.pai_cod_pais = i.pai_cod_pais
         and vp.id_numero = i.id_numero) "Fecha_de_Nacimiento88PerFisica",*/

       --actualizado el dia 09/11/2021

       (select decode(vp.FECHA_NACIMIENTO,
                      null,
                      '',
                      to_char((select decode(edad.si_no,
                                            1,
                                            vp.FECHA_NACIMIENTO,
                                            '')
                                from (select count(*) si_no
                                        from dual
                                       where TO_NUMBER(TO_CHAR(vp.FECHA_NACIMIENTO,
                                                               'YYYY')) >
                                             TO_NUMBER(TO_CHAR(sysdate, 'YYYY')) - 100) edad),
                              'yyyy-MM-dd') || 'T00:00:00-03:00')
          from rcivil.vt_pk_persona vp
         where vp.id_sexo = i.id_sexo
           and vp.nro_documento = i.nro_documento
           and vp.pai_cod_pais = i.pai_cod_pais
           and vp.id_numero = i.id_numero) "Fecha_de_Nacimiento88PerFisica",

       CASE UPPER(ti.n_tipo_integrante)

         WHEN UPPER('Socio/a') THEN
          'Socio' --

         WHEN UPPER('Gerente/a') THEN
          'Gerente' --

         WHEN UPPER('S�ndico/a Titular') THEN
          'S�ndico' --

         WHEN UPPER('Tesorero/a') THEN
          'Tesorero' --

         WHEN UPPER('Presidente/a') THEN
          'Presidente' --

         WHEN UPPER('Director/a') THEN
          'Director' --

         WHEN UPPER('Director/a Titular') THEN
          'Director' --

         WHEN UPPER('Vice-Presidente/a') THEN
          'Vicepresidente' --

         WHEN UPPER('Vicepresidente/a') THEN
          'Vicepresidente' --

         WHEN UPPER('Titular') THEN
          'Titular' --

         WHEN UPPER('Representante') THEN
          'Representante Legal' --

         ELSE
          'Otros'
       END AS "Caracter_Invocado88PerFisica"

        from ipj.t_tipos_entidades         te,
             ipj.t_tramitesipj             tr,
             nuevosuac.vt_tramites_id      suactr,
             nuevosuac.vt_subtipos_tramite subtip,
             ipj.t_legajos                 l
        join ipj.t_entidades e
          on l.id_legajo = e.id_legajo
      -- join ipj.t_entidades_admin ad

        join (select distinct i.id_integrante,
                              i.id_legajo,
                              i.id_tramite_ipj,
                              i.id_tipo_integrante,
                              i.fecha_fin,
                              i.persona_expuesta

                from ipj.t_entidades_admin i
                join ipj.t_tramitesipj tr
                  on i.id_tramite_ipj = tr.id_tramite_ipj
                join ipj.t_legajos l
                  on l.id_legajo = i.id_legajo
               where nvl(i.id_integrante, 0) > 0
                 and -- Personas F�sicas
                     nvl(i.fecha_fin, sysdate) >= trunc(sysdate)
                 and -- Vigentes
                     nvl(l.eliminado, 0) <> 1
                 and -- Entidades no eliminadas
                     l.fecha_baja_entidad is null
                 and -- Entidades no dadas de baja
                     tr.id_estado_ult between 100 and 199 -- En tr�mites inscriptos
              UNION
              select distinct i.id_integrante,
                              i.id_legajo,
                              i.id_tramite_ipj,
                              i.id_tipo_integrante,
                              i.fecha_baja,
                              i.persona_expuesta
                from ipj.t_entidades_sindico i
                join ipj.t_tramitesipj tr
                  on i.id_tramite_ipj = tr.id_tramite_ipj
                join ipj.t_legajos l
                  on l.id_legajo = i.id_legajo
               where nvl(i.id_integrante, 0) > 0
                 and -- Personas F�sicas
                     nvl(i.fecha_baja, sysdate) >= trunc(sysdate)
                 and -- Vigentes
                     nvl(l.eliminado, 0) <> 1
                 and -- Entidades no eliminadas
                     l.fecha_baja_entidad is null
                 and -- Entidades no dadas de baja
                     tr.id_estado_ult between 100 and 199 -- En tr�mites inscriptos
              UNION
              select distinct i.id_integrante,
                              i.id_legajo,
                              i.id_tramite_ipj,
                              51,
                              i.fecha_baja,
                              i.persona_expuesta
                from ipj.t_entidades_representante i
                join ipj.t_tramitesipj tr
                  on i.id_tramite_ipj = tr.id_tramite_ipj
                join ipj.t_legajos l
                  on l.id_legajo = i.id_legajo
               where nvl(i.id_integrante, 0) > 0
                 and -- Personas F�sicas
                     nvl(i.fecha_baja, sysdate) >= trunc(sysdate)
                 and -- Vigentes
                     nvl(l.eliminado, 0) <> 1
                 and -- Entidades no eliminadas
                     l.fecha_baja_entidad is null
                 and -- Entidades no dadas de baja
                     tr.id_estado_ult between 100 and 199 -- En tr�mites inscriptos
              UNION
              select distinct i.id_integrante,
                              i.id_legajo,
                              i.id_tramite_ipj,
                              46,
                              i.fecha_fin,
                              i.persona_expuesta
                from ipj.t_entidades_benef i
                join ipj.t_tramitesipj tr
                  on i.id_tramite_ipj = tr.id_tramite_ipj
                join ipj.t_legajos l
                  on l.id_legajo = i.id_legajo
               where nvl(i.id_integrante, 0) > 0
                 and -- Personas F�sicas
                     nvl(i.fecha_fin, sysdate) >= trunc(sysdate)
                 and -- Vigentes
                     nvl(l.eliminado, 0) <> 1
                 and -- Entidades no eliminadas
                     l.fecha_baja_entidad is null
                 and -- Entidades no dadas de baja
                     tr.id_estado_ult between 100 and 199 -- En tr�mites inscriptos
              UNION
              select distinct i.id_integrante,
                              i.id_legajo,
                              i.id_tramite_ipj,
                              46,
                              i.fecha_fin,
                              i.persona_expuesta
                from ipj.t_entidades_fideicom i
                join ipj.t_tramitesipj tr
                  on i.id_tramite_ipj = tr.id_tramite_ipj
                join ipj.t_legajos l
                  on l.id_legajo = i.id_legajo
               where nvl(i.id_integrante, 0) > 0
                 and -- Personas F�sicas
                     nvl(i.fecha_fin, sysdate) >= trunc(sysdate)
                 and -- Vigentes
                     nvl(l.eliminado, 0) <> 1
                 and -- Entidades no eliminadas
                     l.fecha_baja_entidad is null
                 and -- Entidades no dadas de baja
                     tr.id_estado_ult between 100 and 199 -- En tr�mites inscriptos
              UNION
              select distinct i.id_integrante,
                              i.id_legajo,
                              i.id_tramite_ipj,
                              46,
                              i.fecha_fin,
                              i.persona_expuesta
                from ipj.t_entidades_fiduciantes i
                join ipj.t_tramitesipj tr
                  on i.id_tramite_ipj = tr.id_tramite_ipj
                join ipj.t_legajos l
                  on l.id_legajo = i.id_legajo
               where nvl(i.id_integrante, 0) > 0
                 and -- Personas F�sicas
                     nvl(i.fecha_fin, sysdate) >= trunc(sysdate)
                 and -- Vigentes
                     nvl(l.eliminado, 0) <> 1
                 and -- Entidades no eliminadas
                     l.fecha_baja_entidad is null
                 and -- Entidades no dadas de baja
                     tr.id_estado_ult between 100 and 199 -- En tr�mites inscriptos
              UNION
              select distinct i.id_integrante,
                              i.id_legajo,
                              i.id_tramite_ipj,
                              46,
                              i.fecha_fin,
                              i.persona_expuesta
                from ipj.t_entidades_fiduciarios i
                join ipj.t_tramitesipj tr
                  on i.id_tramite_ipj = tr.id_tramite_ipj
                join ipj.t_legajos l
                  on l.id_legajo = i.id_legajo
               where nvl(i.id_integrante, 0) > 0
                 and -- Personas F�sicas
                     nvl(i.fecha_fin, sysdate) >= trunc(sysdate)
                 and -- Vigentes
                     nvl(l.eliminado, 0) <> 1
                 and -- Entidades no eliminadas
                     l.fecha_baja_entidad is null
                 and -- Entidades no dadas de baja
                     tr.id_estado_ult between 100 and 199 -- En tr�mites inscriptos
              UNION
              select distinct i.id_integrante,
                              i.id_legajo,
                              i.id_tramite_ipj,
                              i.id_tipo_integrante,
                              i.fecha_fin,
                              i.persona_expuesta
                from ipj.t_entidades_socios i
                join ipj.t_tramitesipj tr
                  on i.id_tramite_ipj = tr.id_tramite_ipj
                join ipj.t_legajos l
                  on l.id_legajo = i.id_legajo
               where nvl(i.id_integrante, 0) > 0
                 and -- Personas F�sicas
                     nvl(i.fecha_fin, sysdate) >= trunc(sysdate)
                 and -- Vigentes
                     nvl(l.eliminado, 0) <> 1
                 and -- Entidades no eliminadas
                     l.fecha_baja_entidad is null
                 and -- Entidades no dadas de baja
                     tr.id_estado_ult between 100 and 199 -- En tr�mites inscriptos
              UNION
              select distinct s.id_integrante,
                              s.id_legajo,
                              s.id_tramite_ipj,
                              s.id_tipo_integrante,
                              s.fecha_fin,
                              s.persona_expuesta
                from ipj.t_entidades_socios_copro i
                join ipj.t_entidades_socios s
                  on i.id_entidad_socio = s.id_entidad_socio
                join ipj.t_tramitesipj tr
                  on s.id_tramite_ipj = tr.id_tramite_ipj
                join ipj.t_legajos l
                  on l.id_legajo = s.id_legajo
               where nvl(i.id_integrante, 0) > 0
                 and -- Personas F�sicas
                     nvl(i.fec_baja, sysdate) >= trunc(sysdate)
                 and -- Vigentes
                     nvl(l.eliminado, 0) <> 1
                 and -- Entidades no eliminadas
                     l.fecha_baja_entidad is null
                 and -- Entidades no dadas de baja
                     tr.id_estado_ult between 100 and 199 -- En tr�mites inscriptos
              UNION
              select distinct s.id_integrante,
                              s.id_legajo,
                              s.id_tramite_ipj,
                              s.id_tipo_integrante,
                              s.fecha_fin,
                              s.persona_expuesta
                from ipj.t_entidades_socios_usuf i
                join ipj.t_entidades_socios s
                  on i.id_entidad_socio = s.id_entidad_socio
                join ipj.t_tramitesipj tr
                  on s.id_tramite_ipj = tr.id_tramite_ipj
                join ipj.t_legajos l
                  on l.id_legajo = s.id_legajo
               where nvl(i.id_integrante, 0) > 0
                 and -- Personas F�sicas
                     nvl(i.fec_baja, sysdate) >= trunc(sysdate)
                 and -- Vigentes
                     nvl(l.eliminado, 0) <> 1
                 and -- Entidades no eliminadas
                     l.fecha_baja_entidad is null
                 and -- Entidades no dadas de baja
                     tr.id_estado_ult between 100 and 199 -- En tr�mites inscriptos
              ) ad

          on e.id_tramite_ipj = ad.id_tramite_ipj
         and e.id_legajo = ad.id_legajo
        join ipj.t_integrantes i
          on i.id_integrante = ad.id_integrante
        join ipj.t_tipos_integrante ti
          on ti.id_tipo_integrante = ad.id_tipo_integrante
      /*  join ipj.t_tipos_organismo o
      on o.id_tipo_organismo = ad.id_tipo_organismo*/

       where te.id_tipo_entidad = e.id_tipo_entidad
         and tr.id_tramite_ipj = e.id_tramite_ipj
         and suactr.id_tramite = tr.id_tramite
         and subtip.id_subtipo_tramite = suactr.id_subtipo_tramite

            --INICIO sector valores null
         and e.denominacion_1 is not null
         and te.tipo_entidad is not null
         and length(e.acta_contitutiva) = 10 --el requerimiento dice que no puede estar vacio, y el formato es de 10 digitos (10/01/2012)
            -- por lo tanto debe cumplir eso para su correcta transformacion.

         and e.monto is not null
         and (select d.n_calle
                from dom_manager.vt_domicilios_cond d
               where d.id_vin =
                     IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(l.id_legajo, 'S')) is not null

         and (select decode(d.altura, null, 0, d.altura) --agregado 11/08/2021
                from dom_manager.vt_domicilios_cond d
               where d.id_vin =
                     IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(l.id_legajo, 'S')) is not null

         and (select (upper(substr(d.N_Localidad, 1, 1)) ||
                     lower(SUBSTR(d.N_Localidad, 2)))
                from dom_manager.vt_domicilios_cond d
               where d.id_vin =
                     IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(l.id_legajo, 'S')) is not null

         and (select (upper(substr(d.n_provincia, 1, 1)) ||
                     lower(SUBSTR(d.n_provincia, 2)))
                from dom_manager.vt_domicilios_cond d
               where d.id_vin =
                     IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(l.id_legajo, 'S')) is not null
         and (select (upper(substr(pa.n_pais, 1, 1)) ||
                     lower(SUBSTR(pa.n_pais, 2)))
                from dom_manager.vt_paises pa
               where pa.ID_PAIS = 'ARG') is not null

         and (select (upper(substr(vp.APELLIDO, 1, 1)) ||
                     lower(SUBSTR(vp.APELLIDO, 2)))
                from rcivil.vt_pk_persona vp
               where vp.id_sexo = i.id_sexo
                 and vp.nro_documento = i.nro_documento
                 and vp.pai_cod_pais = i.pai_cod_pais
                 and vp.id_numero = i.id_numero) is not null

         and (select initcap(vp.nombre)
                from rcivil.vt_pk_persona vp
               where vp.id_sexo = i.id_sexo
                 and vp.nro_documento = i.nro_documento
                 and vp.pai_cod_pais = i.pai_cod_pais
                 and vp.id_numero = i.id_numero) is not null

         and decode(ad.persona_expuesta, 'S', 'true', 'false') is not null
            -- FIN sector valores null

         and subtip.id_subtipo_tramite in ('39451',
                                           '39452',
                                           '39453',
                                           '39480',
                                           '39547',
                                           '39549',
                                           '39586',
                                           '39587',
                                           '189',
                                           '1396',
                                           '1397',
                                           '1398',
                                           '1399',
                                           '1486',
                                           '48344',
                                           '39582',
                                           '51060',
                                           '54853', --agregado el 11/08/2021
                                           '39556', --agregado el 11/08/2021
                                           '54862', --agregado el 11/08/2021
                                           '54868', --agregado el 11/08/2021
                                           '39549', --agregado el 11/08/2021
                                           '54649' --agregado el 11/08/2021
                                           )
         and tr.id_estado_ult between 100 and 199

         and E.BORRADOR = 'N'

         and ti.id_tipo_integrante IN ('1', --de aca
                                       '2',
                                       '3',
                                       '4',
                                       '5',
                                       '6',
                                       '7',
                                       '8',
                                       '9',
                                       '10',
                                       '11',
                                       '13',
                                       '14',
                                       '15',
                                       '16',
                                       '18',
                                       '19',
                                       '20',
                                       '21',
                                       '22',
                                       '23',
                                       '24',
                                       '25',
                                       '28',
                                       '29',
                                       '30',
                                       '31',
                                       '32',
                                       '33',
                                       '34',
                                       '35',
                                       '36',
                                       '37',
                                       '38',
                                       '39',
                                       '40',
                                       '41',
                                       '42',
                                       '43',
                                       '44',
                                       '45',
                                       '46',
                                       '47',
                                       '48',
                                       '49',
                                       '50',
                                       '51',
                                       '52',
                                       '53',
                                       '54',
                                       '55',
                                       '56',
                                       '57',
                                       '58',
                                       '59',
                                       '60',
                                       '61',
                                       '62',
                                       '63',
                                       '64',
                                       --hasta aca agregado el 22/10/2021 comentado con fer,lu y bea
                                       '5',
                                       '3',
                                       '24',
                                       '2',
                                       '9',
                                       '11',
                                       --'29',--
                                       '38',
                                       '51',
                                       '52',
                                       '28',
                                       '30',
                                       '34',
                                       '1',
                                       '14',
                                       '42',
                                       '49',
                                       '39',
                                       '20',
                                       '23',
                                       '10')

         and te.id_tipo_entidad NOT IN ('5',
                                        '9',
                                        '11',
                                        '12',
                                        '16',
                                        '20',
                                        '21',
                                        '22',
                                        '31',
                                        '32',
                                        '33',
                                        '34',
                                        '35',
                                        '36',
                                        '37',
                                        '38',
                                        '39',
                                        -- '40',--comentado el 11/08/2021
                                        '41',
                                        '42',
                                        '43',
                                        '44')
            /*  --comentado el 11/08/2021
            and (ad.fecha_fin >= to_date(sysdate, 'dd/mm/rrrr') or
                ad.fecha_fin =
                (select max(adm.fecha_fin)
                    from ipj.t_tramitesipj tr
                    join ipj.t_entidades_admin adm
                      on tr.id_tramite_ipj = adm.id_tramite_ipj
                   where adm.id_legajo = l.id_legajo
                     and tr.id_estado_ult between 100 and 199))*/

         and CASE UPPER(ti.n_tipo_integrante)

               WHEN UPPER('Socio/a') THEN
                'Socio' --

               WHEN UPPER('Gerente/a') THEN
                'Gerente' --

               WHEN UPPER('S�ndico/a Titular') THEN
                'S�ndico' --

               WHEN UPPER('Tesorero/a') THEN
                'Tesorero' --

               WHEN UPPER('Presidente/a') THEN
                'Presidente' --

               WHEN UPPER('Director/a') THEN
                'Director' --

               WHEN UPPER('Director/a Titular') THEN
                'Director' --

               WHEN UPPER('Vice-Presidente/a') THEN
                'Vicepresidente' --

               WHEN UPPER('Vicepresidente/a') THEN
                'Vicepresidente' --

               WHEN UPPER('Titular') THEN
                'Titular' --

               WHEN UPPER('Representante') THEN
                'Representante Legal' --
             /*
              WHEN UPPER('Otros/as') THEN
             'xxxxxxxxxx' --    para fideicomiso */

               ELSE
                'Otros'
             END <> 'Otros'

         and nvl(e.fec_inscripcion, e.fec_resolucion) >= V_FECHA_DESDE
         and nvl(e.fec_inscripcion, e.fec_resolucion) <= V_FECHA_HASTA

         and e.ID_LEGAJO = v_ID_LEGAJO

       order by te.tipo_entidad;

  EXCEPTION
    WHEN V_ERROR_00001 THEN

      V_MSG := 'Mal.';

  END SOCIO_PERSONA_FISICA;

  PROCEDURE SOCIO_PERSONA_JURIDICA(p_T_CONSULTA  OUT TY_CURSOR,
                                   P_FECHA_DESDE IN IPJ.t_Entidades.FEC_INSCRIPCION%TYPE DEFAULT NULL,
                                   P_FECHA_HASTA IN IPJ.t_Entidades.FEC_INSCRIPCION%TYPE DEFAULT NULL,
                                   P_ID_LEGAJO   IN IPJ.t_Entidades.ID_LEGAJO%TYPE DEFAULT NULL) IS
    V_MSG         VARCHAR2(300);
    V_FECHA_DESDE DATE;
    V_FECHA_HASTA DATE;
    V_SENTENCIA   VARCHAR2(50);
    v_ID_LEGAJO   number;

  BEGIN

    V_FECHA_DESDE := P_FECHA_DESDE;
    V_FECHA_HASTA := P_FECHA_HASTA;
    v_ID_LEGAJO   := P_ID_LEGAJO;

    V_SENTENCIA := '  ALTER SESSION SET NLS_DATE_FORMAT=' || '''' ||
                   'DD/MM/YYYY' || ''''; -- modificado por E.J 2010-04-22 para que tenga en cuenta hh:mm_ss
    --dd/mm/yyyy hh24:mi:ss

    EXECUTE IMMEDIATE V_SENTENCIA;

    OPEN p_T_CONSULTA FOR
      select ---Persona Jur�dica

       (select PJU.RAZON_SOCIAL
          from t_comunes.vt_pers_juridicas_unica PJU
         where PJU.CUIT = jur.cuit) "Denominaci93n88PerJuridica",

       CASE
        translate(UPPER(te_jur.tipo_entidad), '����������', 'aeiouAEIOU')

         WHEN UPPER('Sociedad de bolsa') THEN
          'Sociedad de bolsa' --

         WHEN UPPER('Sociedad colectiva') THEN
          'Sociedad colectiva' --

         WHEN UPPER('Sociedades en comandita simple') THEN
          'Sociedad en comandita simple' --

         WHEN UPPER('Sociedad de capital e industria') THEN
          'Sociedad de capital e industria' --

         WHEN UPPER('Sociedad de responsabilidad limitada') THEN
          'Sociedad de responsabilidad limitada' --

         WHEN UPPER('Sociedad anonima') THEN
          'Sociedad an�nima' --

         WHEN UPPER('Sociedades del estado') THEN
          'Sociedad del estado' --

         WHEN UPPER('Sociedad en comandita por acciones') THEN
          'Sociedad en comandita por acciones' --

         WHEN UPPER('Sociedad de hecho') THEN
          'Sociedad de hecho' --

         WHEN UPPER('Sociedad constituida en el extranjero') THEN
          'Sociedad constituida en el extranjero' --

         WHEN UPPER('Sociedad binacional fuera de jurisdiccion') THEN
          'Sociedad binacional fuera de jurisdicci�n' --

         WHEN UPPER('Asociacion civil') THEN
          'Asociaci�n civil' --

         WHEN UPPER('Entidad extranjera sin fines de lucro') THEN
          'Entidad extranjera sin fines de lucro' --

         WHEN UPPER('Fundacion') THEN
          'Fundacion' --

         WHEN UPPER('Simples asociaciones') THEN
          'Simples asociaciones' --

         WHEN UPPER('Federacion') THEN
          'Federaci�n' --

         WHEN UPPER('Confederacion') THEN
          'Confederaci�n' --

         WHEN UPPER('C�mara') THEN
          'C�mara' --

         WHEN UPPER('Contrato de colaboracion empresaria') THEN
          'Contrato de colaboraci�n empresaria' --

         WHEN UPPER('Union transitoria') THEN
          'Uni�n transitoria de empresas' --

         WHEN UPPER('Sociedad de garantia reciproca') THEN
          'Sociedad de garant�a rec�iproca' --

         WHEN UPPER('Soc. capitalizacion y ahorro') THEN
          'Soc. capitalizacion y ahorro"' --

         WHEN UPPER('Cooperativas') THEN
          'Cooperativa' --

         WHEN UPPER('Mutuales') THEN
          'Mutual' --

         WHEN UPPER('Fideicomisos') THEN
          'Fideicomiso' --

         ELSE
          'Otros'
       END as "Forma_Jur92dica88PerJuridica",
       --   (select FJ.N_FORMA_JURIDICA from t_comunes.vt_formas_juridica FJ where FJ.ID_FORMA_JURIDICA=PJu.ID_FORMA_JURIDICA)"Forma_Jur92dica88Per",
       (select PJ.CUIT
          from t_comunes.vt_pers_juridicas_unica pj
         where pj.CUIT = jur.CUIT) "Nro_CUIT88PerJuridica",

       (select (upper(substr(d.n_calle, 1, 1)) ||
               lower(SUBSTR(d.n_calle, 2)))
          from dom_manager.vt_domicilios_cond d
         where d.id_vin =
               IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(l.id_legajo, 'S')) as "Calle88PerJuridica",
       (select decode(d.altura, null, 0, d.altura) --agregado 11/08/2021
          from dom_manager.vt_domicilios_cond d
         where d.id_vin =
               IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(l.id_legajo, 'S')) as "Nro88PerJuridica",
       (select d.piso
          from dom_manager.vt_domicilios_cond d
         where d.id_vin =
               IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(l.id_legajo, 'S')) as "Piso88PerJuridica",
       (select d.depto
          from dom_manager.vt_domicilios_cond d
         where d.id_vin =
               IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(l.id_legajo, 'S')) as "Departamento88PerJuridica",
       (select (upper(substr(d.n_localidad, 1, 1)) ||
               lower(SUBSTR(d.n_localidad, 2)))
          from dom_manager.vt_domicilios_cond d
         where d.id_vin =
               IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(l.id_legajo, 'S')) as "Localidad88PerJuridica",
       (select d.cpa
          from dom_manager.vt_domicilios_cond d
         where d.id_vin =
               IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(l.id_legajo, 'S')) as "C93digo_Postal88PerJuridica",
       (select (upper(substr(d.n_provincia, 1, 1)) ||
               lower(SUBSTR(d.n_provincia, 2)))
          from dom_manager.vt_domicilios_cond d
         where d.id_vin =
               IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(l.id_legajo, 'S')) as "Provincia88PerJuridica",
       (select pa.n_pais
          from dom_manager.vt_domicilios_cond d
          join DOM_MANAGER.VT_PROVINCIAS pr
            on pr.id_provincia = d.id_provincia
          join DOM_MANAGER.VT_PAISES pa
            on pr.id_pais = pa.id_pais
         where d.id_vin =
               IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(l.id_legajo, 'S')) as "Pa92s88PerJuridica",
       IPJ.VARIOS.FC_Buscar_Mail(L.cuit || '00', 126) as "Email88PerJuridica",
       IPJ.VARIOS.FC_Buscar_Te_Caract(L.cuit || '00', 126) as "Prefijo88PerJuridica",
       IPJ.VARIOS.FC_Buscar_Telefono(L.cuit || '00', 126) as "Tel91fono88PerJuridica",
       -- '' as "Actividad_que_Desarrolla88PerJuridica",

       (select max(apju.n_actividad_uif)
          from t_actividad_persona_juri_uif    apju, /* t_comunes.t_actividades a,*/
               t_comunes.vt_actividades_perjur ap
         where apju.id_actividad_uif = ap.id_actividad
              /*a.id_actividad=ap.id_actividad and*/
           and ap.cuit = e.cuit) "Actividad_que_Desarrolla88PerJuridica"

        from ipj.t_tipos_entidades         te,
             ipj.t_tramitesipj             tr,
             nuevosuac.vt_tramites_id      suactr,
             nuevosuac.vt_subtipos_tramite subtip,
             ipj.t_legajos                 l
        join ipj.t_entidades e
          on l.id_legajo = e.id_legajo
        join ipj.t_entidades_SOCIOS ad
          on e.id_tramite_ipj = ad.id_tramite_ipj
         and e.id_legajo = ad.id_legajo
        join ipj.t_Entidades JUR
          on JUR.ID_LEGAJO = AD.ID_LEGAJO_SOCIO
        join ipj.t_tipos_entidades te_jur
          on te_jur.id_tipo_entidad = jur.id_tipo_entidad

       where te.id_tipo_entidad = e.id_tipo_entidad
         and tr.id_tramite_ipj = e.id_tramite_ipj
         and suactr.id_tramite = tr.id_tramite
         and subtip.id_subtipo_tramite = suactr.id_subtipo_tramite

            --INICIO sector valores null
         and e.denominacion_1 is not null
         and te.tipo_entidad is not null
         and length(e.acta_contitutiva) = 10 --el requerimiento dice que no puede estar vacio, y el formato es de 10 digitos (10/01/2012)
            -- por lo tanto debe cumplir eso para su correcta transformacion.

         and decode(ad.persona_expuesta, 'S', 'true', 'false') is not null
            -- FIN sector valores null

         and subtip.id_subtipo_tramite in ('39451',
                                           '39452',
                                           '39453',
                                           '39480',
                                           '39547',
                                           '39549',
                                           '39586',
                                           '39587',
                                           '189',
                                           '1396',
                                           '1397',
                                           '1398',
                                           '1399',
                                           '1486',
                                           '48344',
                                           '39582',
                                           '51060',
                                           '54853', --agregado el 11/08/2021
                                           '39556', --agregado el 11/08/2021
                                           '54862', --agregado el 11/08/2021
                                           '54868', --agregado el 11/08/2021
                                           '39549', --agregado el 11/08/2021
                                           '54649' --agregado el 11/08/2021
                                           )

         and tr.id_estado_ult between 100 and 199

         and E.BORRADOR = 'N'

         and te.id_tipo_entidad NOT IN ('5',
                                        '9',
                                        '11',
                                        '12',
                                        '16',
                                        '20',
                                        '21',
                                        '22',
                                        '31',
                                        '32',
                                        '33',
                                        '34',
                                        '35',
                                        '36',
                                        '37',
                                        '38',
                                        '39',
                                        -- '40',--comentado el 11/08/2021
                                        '41',
                                        '42',
                                        '43',
                                        '44')

         and nvl(e.fec_inscripcion, e.fec_resolucion) >= V_FECHA_DESDE
         and nvl(e.fec_inscripcion, e.fec_resolucion) <= V_FECHA_HASTA

         and e.ID_LEGAJO = v_ID_LEGAJO

       order by te.tipo_entidad;

  EXCEPTION
    WHEN V_ERROR_00001 THEN
      V_MSG := 'Mal.';

  END SOCIO_PERSONA_JURIDICA;

  PROCEDURE ENTIDADES_SIN_FINES_DE_LUCRO(p_T_CONSULTA  OUT TY_CURSOR,
                                         P_FECHA_DESDE IN IPJ.t_Entidades.FEC_INSCRIPCION%TYPE DEFAULT NULL,
                                         P_FECHA_HASTA IN IPJ.t_Entidades.FEC_INSCRIPCION%TYPE DEFAULT NULL) IS
    V_MSG         VARCHAR2(300);
    V_FECHA_DESDE DATE;
    V_FECHA_HASTA DATE;
    V_SENTENCIA   VARCHAR2(50);
    v_cuil        number;

  BEGIN

    V_FECHA_DESDE := P_FECHA_DESDE;
    V_FECHA_HASTA := P_FECHA_HASTA;

    V_SENTENCIA := '  ALTER SESSION SET NLS_DATE_FORMAT=' || '''' ||
                   'DD/MM/YYYY' || ''''; -- modificado por E.J 2010-04-22 para que tenga en cuenta hh:mm_ss
    --dd/mm/yyyy hh24:mi:ss

    EXECUTE IMMEDIATE V_SENTENCIA;

    OPEN p_T_CONSULTA FOR

      select distinct (upper(substr(e.denominacion_1, 1, 1)) ||
                      lower(SUBSTR(e.denominacion_1, 2))) AS "Denominaci93n",

                      e.cuit "Nro_de_CUIT",

                      to_char(to_date(e.acta_contitutiva, 'dd/mm/yyyy'),
                              'yyyy-MM-dd') || 'T00:00:00-03:00' /*E.ACTA_CONTITUTIVA*/ "Fecha_de_Constituci93n",

                      (select (upper(substr(d.n_calle, 1, 1)) ||
                              lower(SUBSTR(d.n_calle, 2)))
                         from dom_manager.vt_domicilios_cond d
                        where d.id_vin =
                              IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(l.id_legajo,
                                                                    'S')) "Calle",

                      (select decode(d.altura, null, 0, d.altura) --agregado 21/10/2021
                         from dom_manager.vt_domicilios_cond d
                        where d.id_vin =
                              IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(l.id_legajo,
                                                                    'S')) "Nro",

                      (select d.piso
                         from dom_manager.vt_domicilios_cond d
                        where d.id_vin =
                              IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(l.id_legajo,
                                                                    'S')) "Piso",

                      (select d.depto
                         from dom_manager.vt_domicilios_cond d
                        where d.id_vin =
                              IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(l.id_legajo,
                                                                    'S')) "Departamento",

                      (select (upper(substr(d.N_Localidad, 1, 1)) ||
                              lower(SUBSTR(d.N_Localidad, 2)))
                         from dom_manager.vt_domicilios_cond d
                        where d.id_vin =
                              IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(l.id_legajo,
                                                                    'S')) "Localidad",

                      (select (upper(substr(d.n_provincia, 1, 1)) ||
                              lower(SUBSTR(d.n_provincia, 2)))
                         from dom_manager.vt_domicilios_cond d
                        where d.id_vin =
                              IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(l.id_legajo,
                                                                    'S')) "Provincia",

                      (select initcap(upper(substr(pa.n_pais, 1, 1)) ||
                                      lower(SUBSTR(pa.n_pais, 2)))
                         from dom_manager.vt_paises pa
                        where pa.ID_PAIS = 'ARG') "Pa92s",

                      (select d.cpa
                         from dom_manager.vt_domicilios_cond d
                        where d.id_vin =
                              IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(l.id_legajo,
                                                                    'S')) "C93digo_Postal",

                      '' as Prefijo_Tel,
                      '' as Nro_Tel,
                      '' as Email,

                      --)REPRESENTANTE

                      (select (upper(substr(vp.APELLIDO, 1, 1)) ||
                              lower(SUBSTR(vp.APELLIDO, 2)))
                         from rcivil.vt_pk_persona vp
                        where vp.id_sexo = i.id_sexo
                          and vp.nro_documento = i.nro_documento
                          and vp.pai_cod_pais = i.pai_cod_pais
                          and vp.id_numero = i.id_numero) "Apellido88Representante",

                      '' as Segundo_Apellido88Representante,

                      (select initcap(vp.nombre)
                         from rcivil.vt_pk_persona vp
                        where vp.id_sexo = i.id_sexo
                          and vp.nro_documento = i.nro_documento
                          and vp.pai_cod_pais = i.pai_cod_pais
                          and vp.id_numero = i.id_numero) "Nombre88Representante",

                      '' as Segundo_Nombre88Representante,

                      (select vp.n_tipo_documento
                         from rcivil.vt_pk_persona vp
                        where vp.id_sexo = i.id_sexo
                          and vp.nro_documento = i.nro_documento
                          and vp.pai_cod_pais = i.pai_cod_pais
                          and vp.id_numero = i.id_numero) "Tipo_Documento88Representante",

                      i.nro_documento "N94mero_Documento",

                      (select vp.cuil
                         from rcivil.vt_pk_persona vp
                        where vp.id_sexo = i.id_sexo
                          and vp.nro_documento = i.nro_documento
                          and vp.pai_cod_pais = i.pai_cod_pais
                          and vp.id_numero = i.id_numero) "Nro_de_CUIT_CUIL88Representante",

                      'false' as Radicada_en_el_Exterior,
                      'false' as Radicada_en_Para92so_Fiscal,

                      decode(ad.persona_expuesta, 'S', 'true', 'false') "PEP",

                      (select (upper(substr(d.n_calle, 1, 1)) ||
                              lower(SUBSTR(d.n_calle, 2)))
                         from DOM_MANAGER.VT_DOMICILIOS_COND d
                        where d.id_vin =
                              IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo,
                                                               i.nro_documento,
                                                               i.pai_cod_pais,
                                                               to_char(i.id_numero),
                                                               3)) "Calle88Representante",

                      (select decode(d.altura, null, 0, d.altura) --agregado 21/10/2021
                         from DOM_MANAGER.VT_DOMICILIOS_COND d
                        where d.id_vin =
                              IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo,
                                                               i.nro_documento,
                                                               i.pai_cod_pais,
                                                               to_char(i.id_numero),
                                                               3)) "Nro88Representante",

                      (select d.piso
                         from DOM_MANAGER.VT_DOMICILIOS_COND d
                        where d.id_vin =
                              IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo,
                                                               i.nro_documento,
                                                               i.pai_cod_pais,
                                                               to_char(i.id_numero),
                                                               3)) "Piso88Representante",

                      (select d.depto
                         from DOM_MANAGER.VT_DOMICILIOS_COND d
                        where d.id_vin =
                              IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo,
                                                               i.nro_documento,
                                                               i.pai_cod_pais,
                                                               to_char(i.id_numero),
                                                               3)) "Departamento88Representante",

                      (select (upper(substr(d.n_localidad, 1, 1)) ||
                              lower(SUBSTR(d.n_localidad, 2)))
                         from DOM_MANAGER.VT_DOMICILIOS_COND d
                        where d.id_vin =
                              IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo,
                                                               i.nro_documento,
                                                               i.pai_cod_pais,
                                                               to_char(i.id_numero),
                                                               3)) "Localidad88Representante",

                      (select d.cpa
                         from DOM_MANAGER.VT_DOMICILIOS_COND d
                        where d.id_vin =
                              IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo,
                                                               i.nro_documento,
                                                               i.pai_cod_pais,
                                                               to_char(i.id_numero),
                                                               3)) "C93digo_Postal88Representante",

                      (select (upper(substr(d.n_provincia, 1, 1)) ||
                              lower(SUBSTR(d.n_provincia, 2)))
                         from dom_manager.vt_domicilios_cond d
                        where d.id_vin =
                              IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(l.id_legajo,
                                                                    'S')) "Provincia88Representante",

                      (select INITCAP(pa.n_pais)
                         from dom_manager.vt_paises pa
                        where pa.ID_PAIS = i.pai_cod_pais) "Pa92s88Representante",

                      IPJ.VARIOS.FC_Buscar_Te_Caract(i.id_sexo ||
                                                     i.nro_documento ||
                                                     i.pai_cod_pais ||
                                                     to_char(i.id_numero),
                                                     126) "Prefijo_Tel88Representante",

                      IPJ.VARIOS.FC_Buscar_Telefono(i.id_sexo ||
                                                    i.nro_documento ||
                                                    i.pai_cod_pais ||
                                                    to_char(i.id_numero),
                                                    126) "Nro_Tel88Representante",

                      IPJ.VARIOS.FC_BUSCAR_MAIL(i.id_sexo ||
                                                i.nro_documento ||
                                                i.pai_cod_pais ||
                                                to_char(i.id_numero),
                                                126) "Email88Representante",

                      CASE UPPER(ti.n_tipo_integrante)

                        WHEN UPPER('Presidente/a') THEN
                         'Presidente' --

                      /*WHEN UPPER('Representante') THEN
                         'Representante Legal' --
                      */
                        ELSE
                         'otros'
                      END AS "Car90cter"

        from ipj.t_tipos_entidades         te,
             ipj.t_tramitesipj             tr,
             nuevosuac.vt_tramites_id      suactr,
             nuevosuac.vt_subtipos_tramite subtip,
             ipj.t_legajos                 l
        join ipj.t_entidades e
          on l.id_legajo = e.id_legajo
      -- join ipj.t_entidades_admin ad

        join (select distinct i.id_integrante,
                              i.id_legajo,
                              i.id_tramite_ipj,
                              i.id_tipo_integrante,
                              i.fecha_fin,
                              i.persona_expuesta

                from ipj.t_entidades_admin i
                join ipj.t_tramitesipj tr
                  on i.id_tramite_ipj = tr.id_tramite_ipj
                join ipj.t_legajos l
                  on l.id_legajo = i.id_legajo
               where nvl(i.id_integrante, 0) > 0
                 and -- Personas F�sicas
                     nvl(i.fecha_fin, sysdate) >= trunc(sysdate)
                 and -- Vigentes
                     nvl(l.eliminado, 0) <> 1
                 and -- Entidades no eliminadas
                     l.fecha_baja_entidad is null
                 and -- Entidades no dadas de baja
                     tr.id_estado_ult between 100 and 199 -- En tr�mites inscriptos
              ) ad

          on e.id_tramite_ipj = ad.id_tramite_ipj
         and e.id_legajo = ad.id_legajo
        join ipj.t_integrantes i
          on i.id_integrante = ad.id_integrante
        join ipj.t_tipos_integrante ti
          on ti.id_tipo_integrante = ad.id_tipo_integrante
      /*  join ipj.t_tipos_organismo o
      on o.id_tipo_organismo = ad.id_tipo_organismo*/

       where te.id_tipo_entidad = e.id_tipo_entidad
         and tr.id_tramite_ipj = e.id_tramite_ipj
         and suactr.id_tramite = tr.id_tramite
         and subtip.id_subtipo_tramite = suactr.id_subtipo_tramite
         and subtip.id_subtipo_tramite in
             ('39451',
              '39452',
              '39453',
              '39480',
              '39547',
              '39549',
              '39586',
              '39587',
              '189',
              '1396',
              '1397',
              '1398',
              '1399',
              '1486',
              '48344',
              '39582',
              '51060')

         and te.id_ubicacion = 5 --CIVILES Y FUNDACIONES
            -- excluir las entidades que tienen persona juridica
            /* and not exists
            (select 1
                     from ipj.t_entidades_socios es1
                    where (es1.cuit_empresa is not null or
                          es1.n_empresa is not null)
                      and es1.borrador = 'N'
                      and e.id_legajo = es1.id_legajo
                      and e.id_tramite_ipj = es1.id_tramite_ipj
                      and es1.fecha_fin is null)*/ --22/10/2021 comentado con fer,lu y bea

         and tr.id_estado_ult between 100 and 199

         and E.BORRADOR = 'N'

         and ti.id_tipo_integrante IN ('11', '38')

         and (ad.fecha_fin >= to_date(sysdate, 'dd/mm/rrrr') or
             ad.fecha_fin =
             (select max(adm.fecha_fin)
                 from ipj.t_tramitesipj tr
                 join ipj.t_entidades_admin adm
                   on tr.id_tramite_ipj = adm.id_tramite_ipj
                where adm.id_legajo = l.id_legajo
                  and tr.id_estado_ult between 100 and 199))

         AND ((nvl(e.fec_inscripcion, e.fec_resolucion) >= V_FECHA_DESDE) AND
             (nvl(e.fec_inscripcion, e.fec_resolucion) <= V_FECHA_HASTA));

  EXCEPTION
    WHEN V_ERROR_00001 THEN
      V_MSG := 'Mal.';

  END ENTIDADES_SIN_FINES_DE_LUCRO;

  PROCEDURE PERSONAS_FISICAS_MAS_DE5_JURI(p_T_CONSULTA  OUT TY_CURSOR,
                                          P_FECHA_DESDE IN IPJ.t_Entidades.FEC_INSCRIPCION%TYPE DEFAULT NULL,
                                          P_FECHA_HASTA IN IPJ.t_Entidades.FEC_INSCRIPCION%TYPE DEFAULT NULL) IS
    V_MSG         VARCHAR2(300);
    V_FECHA_DESDE DATE;
    V_FECHA_HASTA DATE;
    V_SENTENCIA   VARCHAR2(50);

  BEGIN

    V_FECHA_DESDE := P_FECHA_DESDE;
    V_FECHA_HASTA := P_FECHA_HASTA;

    V_SENTENCIA := '  ALTER SESSION SET NLS_DATE_FORMAT=' || '''' ||
                   'DD/MM/YYYY' || ''''; -- modificado por E.J 2010-04-22 para que tenga en cuenta hh:mm_ss
    --dd/mm/yyyy hh24:mi:ss

    EXECUTE IMMEDIATE V_SENTENCIA;

    OPEN p_T_CONSULTA FOR
      select distinct --Socio_Persona_F92sica
                      (select (upper(substr(vp.APELLIDO, 1, 1)) ||
                              lower(SUBSTR(vp.APELLIDO, 2)))
                         from rcivil.vt_pk_persona vp
                        where vp.id_sexo = i.id_sexo
                          and vp.nro_documento = i.nro_documento
                          and vp.pai_cod_pais = i.pai_cod_pais
                          and vp.id_numero = i.id_numero) as Apellido,

                      '' as Segundo_Apellido,

                      (select initcap(vp.nombre)
                         from rcivil.vt_pk_persona vp
                        where vp.id_sexo = i.id_sexo
                          and vp.nro_documento = i.nro_documento
                          and vp.pai_cod_pais = i.pai_cod_pais
                          and vp.id_numero = i.id_numero) as Nombre,

                      '' as Segundo_Nombre,

                      (select vp.n_tipo_documento
                         from rcivil.vt_pk_persona vp
                        where vp.id_sexo = i.id_sexo
                          and vp.nro_documento = i.nro_documento
                          and vp.pai_cod_pais = i.pai_cod_pais
                          and vp.id_numero = i.id_numero) as Tipo_Documento,

                      i.nro_documento as N94mero_documento,

                      (select vp.cuil
                         from rcivil.vt_pk_persona vp
                        where vp.id_sexo = i.id_sexo
                          and vp.nro_documento = i.nro_documento
                          and vp.pai_cod_pais = i.pai_cod_pais
                          and vp.id_numero = i.id_numero) as Nro_de_CUIT_CUIL,

                      (select (upper(substr(d.n_calle, 1, 1)) ||
                              lower(SUBSTR(d.n_calle, 2)))
                         from DOM_MANAGER.VT_DOMICILIOS_COND d
                        where d.id_vin =
                              IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo,
                                                               i.nro_documento,
                                                               i.pai_cod_pais,
                                                               to_char(i.id_numero),
                                                               3)) as Calle,
                      (select d.altura
                         from DOM_MANAGER.VT_DOMICILIOS_COND d
                        where d.id_vin =
                              IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo,
                                                               i.nro_documento,
                                                               i.pai_cod_pais,
                                                               to_char(i.id_numero),
                                                               3)) as Nro,
                      (select d.piso
                         from DOM_MANAGER.VT_DOMICILIOS_COND d
                        where d.id_vin =
                              IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo,
                                                               i.nro_documento,
                                                               i.pai_cod_pais,
                                                               to_char(i.id_numero),
                                                               3)) as Piso,

                      (select d.depto
                         from DOM_MANAGER.VT_DOMICILIOS_COND d
                        where d.id_vin =
                              IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo,
                                                               i.nro_documento,
                                                               i.pai_cod_pais,
                                                               to_char(i.id_numero),
                                                               3)) as Departamento,

                      (select (upper(substr(d.n_localidad, 1, 1)) ||
                              lower(SUBSTR(d.n_localidad, 2)))
                         from DOM_MANAGER.VT_DOMICILIOS_COND d
                        where d.id_vin =
                              IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo,
                                                               i.nro_documento,
                                                               i.pai_cod_pais,
                                                               to_char(i.id_numero),
                                                               3)) as Localidad,
                      (select d.cpa
                         from DOM_MANAGER.VT_DOMICILIOS_COND d
                        where d.id_vin =
                              IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo,
                                                               i.nro_documento,
                                                               i.pai_cod_pais,
                                                               to_char(i.id_numero),
                                                               3)) as C93digo_Postal,

                      /*(select (upper(substr(d.n_provincia, 1, 1)) ||
                            lower(SUBSTR(d.n_provincia, 2)))
                       from dom_manager.vt_domicilios_cond d
                      where d.id_vin =
                            IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(t2.id_legajo,
                                                                  'S'))*/
                      'poner �ca' as Provincia,

                      (select initcap(pa.n_pais)
                         from dom_manager.vt_paises pa
                        where pa.ID_PAIS = i.pai_cod_pais) as Pa92s,

                      (select max(decode(ad1.persona_expuesta,
                                         'S',
                                         'true',
                                         'false'))

                         from (select distinct i.id_integrante, --administrador
                                               i.id_legajo,
                                               i.id_tramite_ipj,
                                               i.id_tipo_integrante,
                                               i.fecha_fin,
                                               i.persona_expuesta

                                 from ipj.t_entidades_admin i
                                 join ipj.t_tramitesipj tr
                                   on i.id_tramite_ipj = tr.id_tramite_ipj
                                 join ipj.t_legajos l
                                   on l.id_legajo = i.id_legajo
                                where nvl(i.id_integrante, 0) > 0
                                  and -- Personas F�sicas
                                      nvl(i.fecha_fin, sysdate) >=
                                      trunc(sysdate)
                                  and -- Vigentes
                                      nvl(l.eliminado, 0) <> 1
                                  and -- Entidades no eliminadas
                                      l.fecha_baja_entidad is null
                                  and -- Entidades no dadas de baja
                                      tr.id_estado_ult between 100 and 199 -- En tr�mites inscriptos
                               UNION
                               select distinct i.id_integrante, ---sindico
                                               i.id_legajo,
                                               i.id_tramite_ipj,
                                               i.id_tipo_integrante,
                                               i.fecha_baja,
                                               i.persona_expuesta
                                 from ipj.t_entidades_sindico i
                                 join ipj.t_tramitesipj tr
                                   on i.id_tramite_ipj = tr.id_tramite_ipj
                                 join ipj.t_legajos l
                                   on l.id_legajo = i.id_legajo
                                where nvl(i.id_integrante, 0) > 0
                                  and -- Personas F�sicas
                                      nvl(i.fecha_baja, sysdate) >=
                                      trunc(sysdate)
                                  and -- Vigentes
                                      nvl(l.eliminado, 0) <> 1
                                  and -- Entidades no eliminadas
                                      l.fecha_baja_entidad is null
                                  and -- Entidades no dadas de baja
                                      tr.id_estado_ult between 100 and 199 -- En tr�mites inscriptos
                               UNION
                               select distinct i.id_integrante, ---representante
                                               i.id_legajo,
                                               i.id_tramite_ipj,
                                               51,
                                               i.fecha_baja,
                                               i.persona_expuesta
                                 from ipj.t_entidades_representante i
                                 join ipj.t_tramitesipj tr
                                   on i.id_tramite_ipj = tr.id_tramite_ipj
                                 join ipj.t_legajos l
                                   on l.id_legajo = i.id_legajo
                                where nvl(i.id_integrante, 0) > 0
                                  and -- Personas F�sicas
                                      nvl(i.fecha_baja, sysdate) >=
                                      trunc(sysdate)
                                  and -- Vigentes
                                      nvl(l.eliminado, 0) <> 1
                                  and -- Entidades no eliminadas
                                      l.fecha_baja_entidad is null
                                  and -- Entidades no dadas de baja
                                      tr.id_estado_ult between 100 and 199 -- En tr�mites inscriptos

                               UNION
                               select distinct i.id_integrante, --socio
                                               i.id_legajo,
                                               i.id_tramite_ipj,
                                               i.id_tipo_integrante,
                                               i.fecha_fin,
                                               i.persona_expuesta
                                 from ipj.t_entidades_socios i
                                 join ipj.t_tramitesipj tr
                                   on i.id_tramite_ipj = tr.id_tramite_ipj
                                 join ipj.t_legajos l
                                   on l.id_legajo = i.id_legajo
                                where nvl(i.id_integrante, 0) > 0
                                  and -- Personas F�sicas
                                      nvl(i.fecha_fin, sysdate) >=
                                      trunc(sysdate)
                                  and -- Vigentes
                                      nvl(l.eliminado, 0) <> 1
                                  and -- Entidades no eliminadas
                                      l.fecha_baja_entidad is null
                                  and -- Entidades no dadas de baja
                                      tr.id_estado_ult between 100 and 199 -- En tr�mites inscriptos

                               ) ad1

                        where /* e.id_tramite_ipj = ad1.id_tramite_ipj
                                                                                                                                           and t2.id_legajo = ad1.id_legajo*/
                        id_integrante = tt.id_integrante) as PEP,

                      IPJ.VARIOS.FC_Buscar_Te_Caract(i.id_sexo ||
                                                     i.nro_documento ||
                                                     i.pai_cod_pais ||
                                                     to_char(i.id_numero),
                                                     126) as Prefijo_Tel,

                      IPJ.VARIOS.FC_Buscar_Telefono(i.id_sexo ||
                                                    i.nro_documento ||
                                                    i.pai_cod_pais ||
                                                    to_char(i.id_numero),
                                                    126) as Nro_Tel,

                      IPJ.VARIOS.FC_BUSCAR_MAIL(i.id_sexo ||
                                                i.nro_documento ||
                                                i.pai_cod_pais ||
                                                to_char(i.id_numero),
                                                126) as Email,

                      (select --vp.FECHA_NACIMIENTO
                        decode(vp.FECHA_NACIMIENTO,
                               null,
                               '',
                               to_char(vp.FECHA_NACIMIENTO, 'yyyy-MM-dd') ||
                               'T00:00:00-03:00')
                         from rcivil.vt_pk_persona vp
                        where vp.id_sexo = i.id_sexo
                          and vp.nro_documento = i.nro_documento
                          and vp.pai_cod_pais = i.pai_cod_pais
                          and vp.id_numero = i.id_numero) as Fecha_de_Nacimiento
        from (select distinct t.id_integrante
                from (select distinct i.id_integrante,
                                      i.id_legajo,
                                      1               Admin,
                                      null            Sindico,
                                      null            Repres,
                                      null            Benef_Fidei,
                                      null            Fideicomisario,
                                      null            Fiduciante,
                                      null            Fiduciario,
                                      null            Socio,
                                      null            Socio_Cop,
                                      null            Socio_Usuf
                        from ipj.t_entidades_admin i
                        join ipj.t_tramitesipj tr
                          on i.id_tramite_ipj = tr.id_tramite_ipj
                        join ipj.t_legajos l
                          on l.id_legajo = i.id_legajo
                       where nvl(i.id_integrante, 0) > 0
                         and -- Personas F�sicas
                             nvl(i.fecha_fin, sysdate) >= trunc(sysdate)
                         and -- Vigentes
                             nvl(l.eliminado, 0) <> 1
                         and -- Entidades no eliminadas
                             l.fecha_baja_entidad is null
                         and -- Entidades no dadas de baja
                             tr.id_estado_ult between 100 and 199 -- En tr�mites inscriptos
                      UNION
                      select distinct i.id_integrante,
                                      i.id_legajo,
                                      null            Admin,
                                      1               Sindico,
                                      null            Repres,
                                      null            Benef_Fidei,
                                      null            Fideicomisario,
                                      null            Fiduciante,
                                      null            Fiduciario,
                                      null            Socio,
                                      null            Socio_Cop,
                                      null            Socio_Usuf
                        from ipj.t_entidades_sindico i
                        join ipj.t_tramitesipj tr
                          on i.id_tramite_ipj = tr.id_tramite_ipj
                        join ipj.t_legajos l
                          on l.id_legajo = i.id_legajo
                       where nvl(i.id_integrante, 0) > 0
                         and -- Personas F�sicas
                             nvl(i.fecha_baja, sysdate) >= trunc(sysdate)
                         and -- Vigentes
                             nvl(l.eliminado, 0) <> 1
                         and -- Entidades no eliminadas
                             l.fecha_baja_entidad is null
                         and -- Entidades no dadas de baja
                             tr.id_estado_ult between 100 and 199 -- En tr�mites inscriptos
                      UNION
                      select distinct i.id_integrante,
                                      i.id_legajo,
                                      null            Admin,
                                      null            Sindico,
                                      1               Repres,
                                      null            Benef_Fidei,
                                      null            Fideicomisario,
                                      null            Fiduciante,
                                      null            Fiduciario,
                                      null            Socio,
                                      null            Socio_Cop,
                                      null            Socio_Usuf
                        from ipj.t_entidades_representante i
                        join ipj.t_tramitesipj tr
                          on i.id_tramite_ipj = tr.id_tramite_ipj
                        join ipj.t_legajos l
                          on l.id_legajo = i.id_legajo
                       where nvl(i.id_integrante, 0) > 0
                         and -- Personas F�sicas
                             nvl(i.fecha_baja, sysdate) >= trunc(sysdate)
                         and -- Vigentes
                             nvl(l.eliminado, 0) <> 1
                         and -- Entidades no eliminadas
                             l.fecha_baja_entidad is null
                         and -- Entidades no dadas de baja
                             tr.id_estado_ult between 100 and 199 -- En tr�mites inscriptos
                      UNION
                      select distinct i.id_integrante,
                                      i.id_legajo,
                                      null            Admin,
                                      null            Sindico,
                                      null            Repres,
                                      1               Benef_Fidei,
                                      null            Fideicomisario,
                                      null            Fiduciante,
                                      null            Fiduciario,
                                      null            Socio,
                                      null            Socio_Cop,
                                      null            Socio_Usuf
                        from ipj.t_entidades_benef i
                        join ipj.t_tramitesipj tr
                          on i.id_tramite_ipj = tr.id_tramite_ipj
                        join ipj.t_legajos l
                          on l.id_legajo = i.id_legajo
                       where nvl(i.id_integrante, 0) > 0
                         and -- Personas F�sicas
                             nvl(i.fecha_fin, sysdate) >= trunc(sysdate)
                         and -- Vigentes
                             nvl(l.eliminado, 0) <> 1
                         and -- Entidades no eliminadas
                             l.fecha_baja_entidad is null
                         and -- Entidades no dadas de baja
                             tr.id_estado_ult between 100 and 199 -- En tr�mites inscriptos
                      UNION
                      select distinct i.id_integrante,
                                      i.id_legajo,
                                      null            Admin,
                                      null            Sindico,
                                      null            Repres,
                                      null            Benef_Fidei,
                                      1               Fideicomisario,
                                      null            Fiduciante,
                                      null            Fiduciario,
                                      null            Socio,
                                      null            Socio_Cop,
                                      null            Socio_Usuf
                        from ipj.t_entidades_fideicom i
                        join ipj.t_tramitesipj tr
                          on i.id_tramite_ipj = tr.id_tramite_ipj
                        join ipj.t_legajos l
                          on l.id_legajo = i.id_legajo
                       where nvl(i.id_integrante, 0) > 0
                         and -- Personas F�sicas
                             nvl(i.fecha_fin, sysdate) >= trunc(sysdate)
                         and -- Vigentes
                             nvl(l.eliminado, 0) <> 1
                         and -- Entidades no eliminadas
                             l.fecha_baja_entidad is null
                         and -- Entidades no dadas de baja
                             tr.id_estado_ult between 100 and 199 -- En tr�mites inscriptos
                      UNION
                      select distinct i.id_integrante,
                                      i.id_legajo,
                                      null            Admin,
                                      null            Sindico,
                                      null            Repres,
                                      null            Benef_Fidei,
                                      null            Fideicomisario,
                                      1               Fiduciante,
                                      null            Fiduciario,
                                      null            Socio,
                                      null            Socio_Cop,
                                      null            Socio_Usuf
                        from ipj.t_entidades_fiduciantes i
                        join ipj.t_tramitesipj tr
                          on i.id_tramite_ipj = tr.id_tramite_ipj
                        join ipj.t_legajos l
                          on l.id_legajo = i.id_legajo
                       where nvl(i.id_integrante, 0) > 0
                         and -- Personas F�sicas
                             nvl(i.fecha_fin, sysdate) >= trunc(sysdate)
                         and -- Vigentes
                             nvl(l.eliminado, 0) <> 1
                         and -- Entidades no eliminadas
                             l.fecha_baja_entidad is null
                         and -- Entidades no dadas de baja
                             tr.id_estado_ult between 100 and 199 -- En tr�mites inscriptos
                      UNION
                      select distinct i.id_integrante,
                                      i.id_legajo,
                                      null            Admin,
                                      null            Sindico,
                                      null            Repres,
                                      null            Benef_Fidei,
                                      null            Fideicomisario,
                                      null            Fiduciante,
                                      1               Fiduciario,
                                      null            Socio,
                                      null            Socio_Cop,
                                      null            Socio_Usuf
                        from ipj.t_entidades_fiduciarios i
                        join ipj.t_tramitesipj tr
                          on i.id_tramite_ipj = tr.id_tramite_ipj
                        join ipj.t_legajos l
                          on l.id_legajo = i.id_legajo
                       where nvl(i.id_integrante, 0) > 0
                         and -- Personas F�sicas
                             nvl(i.fecha_fin, sysdate) >= trunc(sysdate)
                         and -- Vigentes
                             nvl(l.eliminado, 0) <> 1
                         and -- Entidades no eliminadas
                             l.fecha_baja_entidad is null
                         and -- Entidades no dadas de baja
                             tr.id_estado_ult between 100 and 199 -- En tr�mites inscriptos
                      UNION
                      select distinct i.id_integrante,
                                      i.id_legajo,
                                      null            Admin,
                                      null            Sindico,
                                      null            Repres,
                                      null            Benef_Fidei,
                                      null            Fideicomisario,
                                      null            Fiduciante,
                                      null            Fiduciario,
                                      1               Socio,
                                      null            Socio_Cop,
                                      null            Socio_Usuf
                        from ipj.t_entidades_socios i
                        join ipj.t_tramitesipj tr
                          on i.id_tramite_ipj = tr.id_tramite_ipj
                        join ipj.t_legajos l
                          on l.id_legajo = i.id_legajo
                       where nvl(i.id_integrante, 0) > 0
                         and -- Personas F�sicas
                             nvl(i.fecha_fin, sysdate) >= trunc(sysdate)
                         and -- Vigentes
                             nvl(l.eliminado, 0) <> 1
                         and -- Entidades no eliminadas
                             l.fecha_baja_entidad is null
                         and -- Entidades no dadas de baja
                             tr.id_estado_ult between 100 and 199 -- En tr�mites inscriptos
                      UNION
                      select distinct i.id_integrante,
                                      s.id_legajo,
                                      null            Admin,
                                      null            Sindico,
                                      null            Repres,
                                      null            Benef_Fidei,
                                      null            Fideicomisario,
                                      null            Fiduciante,
                                      null            Fiduciario,
                                      null            Socio,
                                      1               Socio_Cop,
                                      null            Socio_Usuf
                        from ipj.t_entidades_socios_copro i
                        join ipj.t_entidades_socios s
                          on i.id_entidad_socio = s.id_entidad_socio
                        join ipj.t_tramitesipj tr
                          on s.id_tramite_ipj = tr.id_tramite_ipj
                        join ipj.t_legajos l
                          on l.id_legajo = s.id_legajo
                       where nvl(i.id_integrante, 0) > 0
                         and -- Personas F�sicas
                             nvl(i.fec_baja, sysdate) >= trunc(sysdate)
                         and -- Vigentes
                             nvl(l.eliminado, 0) <> 1
                         and -- Entidades no eliminadas
                             l.fecha_baja_entidad is null
                         and -- Entidades no dadas de baja
                             tr.id_estado_ult between 100 and 199 -- En tr�mites inscriptos
                      UNION
                      select distinct i.id_integrante,
                                      s.id_legajo,
                                      null            Admin,
                                      null            Sindico,
                                      null            Repres,
                                      null            Benef_Fidei,
                                      null            Fideicomisario,
                                      null            Fiduciante,
                                      null            Fiduciario,
                                      null            Socio,
                                      null            Socio_Cop,
                                      1               Socio_Usuf
                        from ipj.t_entidades_socios_usuf i
                        join ipj.t_entidades_socios s
                          on i.id_entidad_socio = s.id_entidad_socio
                        join ipj.t_tramitesipj tr
                          on s.id_tramite_ipj = tr.id_tramite_ipj
                        join ipj.t_legajos l
                          on l.id_legajo = s.id_legajo
                       where nvl(i.id_integrante, 0) > 0
                         and -- Personas F�sicas
                             nvl(i.fec_baja, sysdate) >= trunc(sysdate)
                         and -- Vigentes
                             nvl(l.eliminado, 0) <> 1
                         and -- Entidades no eliminadas
                             l.fecha_baja_entidad is null
                         and -- Entidades no dadas de baja
                             tr.id_estado_ult between 100 and 199 -- En tr�mites inscriptos
                      ) t
                join ipj.t_integrantes i
                  on t.id_integrante = i.id_integrante
               group by t.id_integrante,
                        i.id_sexo,
                        i.nro_documento,
                        i.pai_cod_pais,
                        i.id_numero
              having count(distinct id_legajo) > 4) tt
        JOIN (select distinct i.id_integrante,
                              i.id_legajo,
                              1               Admin,
                              null            Sindico,
                              null            Repres,
                              null            Benef_Fidei,
                              null            Fideicomisario,
                              null            Fiduciante,
                              null            Fiduciario,
                              null            Socio,
                              null            Socio_Cop,
                              null            Socio_Usuf
                from ipj.t_entidades_admin i
                join ipj.t_tramitesipj tr
                  on i.id_tramite_ipj = tr.id_tramite_ipj
                join ipj.t_legajos l
                  on l.id_legajo = i.id_legajo
               where nvl(i.id_integrante, 0) > 0
                 and -- Personas F�sicas
                     nvl(i.fecha_fin, sysdate) >= trunc(sysdate)
                 and -- Vigentes
                     nvl(l.eliminado, 0) <> 1
                 and -- Entidades no eliminadas
                     l.fecha_baja_entidad is null
                 and -- Entidades no dadas de baja
                     tr.id_estado_ult between 100 and 199 -- En tr�mites inscriptos
              UNION
              select distinct i.id_integrante,
                              i.id_legajo,
                              null            Admin,
                              1               Sindico,
                              null            Repres,
                              null            Benef_Fidei,
                              null            Fideicomisario,
                              null            Fiduciante,
                              null            Fiduciario,
                              null            Socio,
                              null            Socio_Cop,
                              null            Socio_Usuf
                from ipj.t_entidades_sindico i
                join ipj.t_tramitesipj tr
                  on i.id_tramite_ipj = tr.id_tramite_ipj
                join ipj.t_legajos l
                  on l.id_legajo = i.id_legajo
               where nvl(i.id_integrante, 0) > 0
                 and -- Personas F�sicas
                     nvl(i.fecha_baja, sysdate) >= trunc(sysdate)
                 and -- Vigentes
                     nvl(l.eliminado, 0) <> 1
                 and -- Entidades no eliminadas
                     l.fecha_baja_entidad is null
                 and -- Entidades no dadas de baja
                     tr.id_estado_ult between 100 and 199 -- En tr�mites inscriptos
              UNION
              select distinct i.id_integrante,
                              i.id_legajo,
                              null            Admin,
                              null            Sindico,
                              1               Repres,
                              null            Benef_Fidei,
                              null            Fideicomisario,
                              null            Fiduciante,
                              null            Fiduciario,
                              null            Socio,
                              null            Socio_Cop,
                              null            Socio_Usuf
                from ipj.t_entidades_representante i
                join ipj.t_tramitesipj tr
                  on i.id_tramite_ipj = tr.id_tramite_ipj
                join ipj.t_legajos l
                  on l.id_legajo = i.id_legajo
               where nvl(i.id_integrante, 0) > 0
                 and -- Personas F�sicas
                     nvl(i.fecha_baja, sysdate) >= trunc(sysdate)
                 and -- Vigentes
                     nvl(l.eliminado, 0) <> 1
                 and -- Entidades no eliminadas
                     l.fecha_baja_entidad is null
                 and -- Entidades no dadas de baja
                     tr.id_estado_ult between 100 and 199 -- En tr�mites inscriptos
              UNION
              select distinct i.id_integrante,
                              i.id_legajo,
                              null            Admin,
                              null            Sindico,
                              null            Repres,
                              1               Benef_Fidei,
                              null            Fideicomisario,
                              null            Fiduciante,
                              null            Fiduciario,
                              null            Socio,
                              null            Socio_Cop,
                              null            Socio_Usuf
                from ipj.t_entidades_benef i
                join ipj.t_tramitesipj tr
                  on i.id_tramite_ipj = tr.id_tramite_ipj
                join ipj.t_legajos l
                  on l.id_legajo = i.id_legajo
               where nvl(i.id_integrante, 0) > 0
                 and -- Personas F�sicas
                     nvl(i.fecha_fin, sysdate) >= trunc(sysdate)
                 and -- Vigentes
                     nvl(l.eliminado, 0) <> 1
                 and -- Entidades no eliminadas
                     l.fecha_baja_entidad is null
                 and -- Entidades no dadas de baja
                     tr.id_estado_ult between 100 and 199 -- En tr�mites inscriptos
              UNION
              select distinct i.id_integrante,
                              i.id_legajo,
                              null            Admin,
                              null            Sindico,
                              null            Repres,
                              null            Benef_Fidei,
                              1               Fideicomisario,
                              null            Fiduciante,
                              null            Fiduciario,
                              null            Socio,
                              null            Socio_Cop,
                              null            Socio_Usuf
                from ipj.t_entidades_fideicom i
                join ipj.t_tramitesipj tr
                  on i.id_tramite_ipj = tr.id_tramite_ipj
                join ipj.t_legajos l
                  on l.id_legajo = i.id_legajo
               where nvl(i.id_integrante, 0) > 0
                 and -- Personas F�sicas
                     nvl(i.fecha_fin, sysdate) >= trunc(sysdate)
                 and -- Vigentes
                     nvl(l.eliminado, 0) <> 1
                 and -- Entidades no eliminadas
                     l.fecha_baja_entidad is null
                 and -- Entidades no dadas de baja
                     tr.id_estado_ult between 100 and 199 -- En tr�mites inscriptos
              UNION
              select distinct i.id_integrante,
                              i.id_legajo,
                              null            Admin,
                              null            Sindico,
                              null            Repres,
                              null            Benef_Fidei,
                              null            Fideicomisario,
                              1               Fiduciante,
                              null            Fiduciario,
                              null            Socio,
                              null            Socio_Cop,
                              null            Socio_Usuf
                from ipj.t_entidades_fiduciantes i
                join ipj.t_tramitesipj tr
                  on i.id_tramite_ipj = tr.id_tramite_ipj
                join ipj.t_legajos l
                  on l.id_legajo = i.id_legajo
               where nvl(i.id_integrante, 0) > 0
                 and -- Personas F�sicas
                     nvl(i.fecha_fin, sysdate) >= trunc(sysdate)
                 and -- Vigentes
                     nvl(l.eliminado, 0) <> 1
                 and -- Entidades no eliminadas
                     l.fecha_baja_entidad is null
                 and -- Entidades no dadas de baja
                     tr.id_estado_ult between 100 and 199 -- En tr�mites inscriptos
              UNION
              select distinct i.id_integrante,
                              i.id_legajo,
                              null            Admin,
                              null            Sindico,
                              null            Repres,
                              null            Benef_Fidei,
                              null            Fideicomisario,
                              null            Fiduciante,
                              1               Fiduciario,
                              null            Socio,
                              null            Socio_Cop,
                              null            Socio_Usuf
                from ipj.t_entidades_fiduciarios i
                join ipj.t_tramitesipj tr
                  on i.id_tramite_ipj = tr.id_tramite_ipj
                join ipj.t_legajos l
                  on l.id_legajo = i.id_legajo
               where nvl(i.id_integrante, 0) > 0
                 and -- Personas F�sicas
                     nvl(i.fecha_fin, sysdate) >= trunc(sysdate)
                 and -- Vigentes
                     nvl(l.eliminado, 0) <> 1
                 and -- Entidades no eliminadas
                     l.fecha_baja_entidad is null
                 and -- Entidades no dadas de baja
                     tr.id_estado_ult between 100 and 199 -- En tr�mites inscriptos
              UNION
              select distinct i.id_integrante,
                              i.id_legajo,
                              null            Admin,
                              null            Sindico,
                              null            Repres,
                              null            Benef_Fidei,
                              null            Fideicomisario,
                              null            Fiduciante,
                              null            Fiduciario,
                              1               Socio,
                              null            Socio_Cop,
                              null            Socio_Usuf
                from ipj.t_entidades_socios i
                join ipj.t_tramitesipj tr
                  on i.id_tramite_ipj = tr.id_tramite_ipj
                join ipj.t_legajos l
                  on l.id_legajo = i.id_legajo
               where nvl(i.id_integrante, 0) > 0
                 and -- Personas F�sicas
                     nvl(i.fecha_fin, sysdate) >= trunc(sysdate)
                 and -- Vigentes
                     nvl(l.eliminado, 0) <> 1
                 and -- Entidades no eliminadas
                     l.fecha_baja_entidad is null
                 and -- Entidades no dadas de baja
                     tr.id_estado_ult between 100 and 199 -- En tr�mites inscriptos
              UNION
              select distinct i.id_integrante,
                              s.id_legajo,
                              null            Admin,
                              null            Sindico,
                              null            Repres,
                              null            Benef_Fidei,
                              null            Fideicomisario,
                              null            Fiduciante,
                              null            Fiduciario,
                              null            Socio,
                              1               Socio_Cop,
                              null            Socio_Usuf
                from ipj.t_entidades_socios_copro i
                join ipj.t_entidades_socios s
                  on i.id_entidad_socio = s.id_entidad_socio
                join ipj.t_tramitesipj tr
                  on s.id_tramite_ipj = tr.id_tramite_ipj
                join ipj.t_legajos l
                  on l.id_legajo = s.id_legajo
               where nvl(i.id_integrante, 0) > 0
                 and -- Personas F�sicas
                     nvl(i.fec_baja, sysdate) >= trunc(sysdate)
                 and -- Vigentes
                     nvl(l.eliminado, 0) <> 1
                 and -- Entidades no eliminadas
                     l.fecha_baja_entidad is null
                 and -- Entidades no dadas de baja
                     tr.id_estado_ult between 100 and 199 -- En tr�mites inscriptos
              UNION
              select distinct i.id_integrante,
                              s.id_legajo,
                              null            Admin,
                              null            Sindico,
                              null            Repres,
                              null            Benef_Fidei,
                              null            Fideicomisario,
                              null            Fiduciante,
                              null            Fiduciario,
                              null            Socio,
                              null            Socio_Cop,
                              1               Socio_Usuf
                from ipj.t_entidades_socios_usuf i
                join ipj.t_entidades_socios s
                  on i.id_entidad_socio = s.id_entidad_socio
                join ipj.t_tramitesipj tr
                  on s.id_tramite_ipj = tr.id_tramite_ipj
                join ipj.t_legajos l
                  on l.id_legajo = s.id_legajo
               where nvl(i.id_integrante, 0) > 0
                 and -- Personas F�sicas
                     nvl(i.fec_baja, sysdate) >= trunc(sysdate)
                 and -- Vigentes
                     nvl(l.eliminado, 0) <> 1
                 and -- Entidades no eliminadas
                     l.fecha_baja_entidad is null
                 and -- Entidades no dadas de baja
                     tr.id_estado_ult between 100 and 199 -- En tr�mites inscriptos
              ) t2
          on tt.id_integrante = t2.id_integrante
        join ipj.t_integrantes i
          on tt.id_integrante = i.id_integrante, ipj.t_entidades_admin ad,
       ipj.t_legajos l, ipj.t_entidades e, ipj.t_tipos_organismo o,
       ipj.t_tipos_integrante ti
       where tt.id_integrante = i.id_integrante
         and l.id_legajo = e.id_legajo
         and e.id_legajo = ad.id_legajo
         and i.id_integrante = ad.id_integrante
         and ti.id_tipo_integrante = ad.id_tipo_integrante
         and o.id_tipo_organismo = ad.id_tipo_organismo
            --and tt.id_integrante='65945'--Solis, cant_empresas=5
         and ad.persona_expuesta is not null
            --    and nvl(e.fec_inscripcion, e.fec_resolucion) >= to_date('01/01/2021', 'dd/mm/rrrr')
            /*  AND ((nvl(e.fec_inscripcion, e.fec_resolucion) >= V_FECHA_DESDE) AND
            (nvl(e.fec_inscripcion, e.fec_resolucion) <= V_FECHA_HASTA))*/

         and (ad.fecha_fin >= to_date(sysdate, 'dd/mm/rrrr') or
             ad.fecha_fin =
             (select max(adm.fecha_fin)
                 from ipj.t_tramitesipj tr
                 join ipj.t_entidades_admin adm
                   on tr.id_tramite_ipj = adm.id_tramite_ipj
                where adm.id_legajo = l.id_legajo
                  and tr.id_estado_ult between 100 and 199))
       group by tt.id_integrante,
                t2.id_legajo,
                i.id_sexo,
                i.nro_documento,
                i.pai_cod_pais,
                i.id_numero,
                ad.persona_expuesta,
                ti.n_tipo_integrante;

  EXCEPTION
    WHEN V_ERROR_00001 THEN
      V_MSG := 'Mal.';

  END PERSONAS_FISICAS_MAS_DE5_JURI;

  PROCEDURE MAS_DE5_PERSONAS_JURIDICAS(p_T_CONSULTA  OUT TY_CURSOR,
                                       P_FECHA_DESDE IN IPJ.t_Entidades.FEC_INSCRIPCION%TYPE DEFAULT NULL,
                                       P_FECHA_HASTA IN IPJ.t_Entidades.FEC_INSCRIPCION%TYPE DEFAULT NULL,
                                       P_DOCUMENTO   IN IPJ.t_integrantes.NRO_DOCUMENTO%TYPE DEFAULT NULL) IS

    V_MSG         VARCHAR2(300);
    V_FECHA_DESDE DATE;
    V_FECHA_HASTA DATE;
    V_SENTENCIA   VARCHAR2(50);
    V_DOCUMENTO   VARCHAR2(50);

  BEGIN

    V_FECHA_DESDE := P_FECHA_DESDE;
    V_FECHA_HASTA := P_FECHA_HASTA;
    V_DOCUMENTO   := to_char(P_DOCUMENTO);

    V_SENTENCIA := '  ALTER SESSION SET NLS_DATE_FORMAT=' || '''' ||
                   'DD/MM/YYYY' || ''''; -- modificado por E.J 2010-04-22 para que tenga en cuenta hh:mm_ss
    --dd/mm/yyyy hh24:mi:ss

    EXECUTE IMMEDIATE V_SENTENCIA;

    OPEN p_T_CONSULTA FOR

      select distinct --Personas Jur�dicas
                      (select denominacion_sia
                         from ipj.t_legajos l
                        where l.id_legajo = ad.id_legajo) as Denominaci93n88Sociedad,
                      (select cuit
                         from ipj.t_legajos l
                        where l.id_legajo = ad.id_legajo) as Nro_de_CUIT88Sociedad,

                      CASE UPPER(ti.n_tipo_integrante)
                        WHEN UPPER('Socio/a') THEN
                         'Socio' --
                        WHEN UPPER('Gerente/a') THEN
                         'Gerente' --
                        WHEN UPPER('S�ndico/a Titular') THEN
                         'S�ndico' --
                        WHEN UPPER('Tesorero/a') THEN
                         'Tesorero' --
                        WHEN UPPER('Presidente/a') THEN
                         'Presidente' --
                        WHEN UPPER('Director/a') THEN
                         'Director' --
                        WHEN UPPER('Director/a Titular') THEN
                         'Director' --
                        WHEN UPPER('Vice-Presidente/a') THEN
                         'Vicepresidente' --
                        WHEN UPPER('Vicepresidente/a') THEN
                         'Vicepresidente' --
                        WHEN UPPER('Titular') THEN
                         'Titular' --
                        WHEN UPPER('Representante') THEN
                         'Representante Legal' --
                        ELSE
                         'Otros'
                      END AS Car90cter

        from ipj.t_tipos_entidades         te,
             ipj.t_tramitesipj             tr,
             nuevosuac.vt_tramites_id      suactr,
             nuevosuac.vt_subtipos_tramite subtip,
             ipj.t_legajos                 l
        join ipj.t_entidades e
          on l.id_legajo = e.id_legajo

        join (select distinct i.id_integrante, --administrador
                              i.id_legajo,
                              i.id_tramite_ipj,
                              i.id_tipo_integrante,
                              i.fecha_fin,
                              i.persona_expuesta

                from ipj.t_entidades_admin i
                join ipj.t_tramitesipj tr
                  on i.id_tramite_ipj = tr.id_tramite_ipj
                join ipj.t_legajos l
                  on l.id_legajo = i.id_legajo
               where nvl(i.id_integrante, 0) > 0
                 and -- Personas F�sicas
                     nvl(i.fecha_fin, sysdate) >= trunc(sysdate)
                 and -- Vigentes
                     nvl(l.eliminado, 0) <> 1
                 and -- Entidades no eliminadas
                     l.fecha_baja_entidad is null
                 and -- Entidades no dadas de baja
                     tr.id_estado_ult between 100 and 199 -- En tr�mites inscriptos
              UNION
              select distinct i.id_integrante, ---sindico
                              i.id_legajo,
                              i.id_tramite_ipj,
                              i.id_tipo_integrante,
                              i.fecha_baja,
                              i.persona_expuesta
                from ipj.t_entidades_sindico i
                join ipj.t_tramitesipj tr
                  on i.id_tramite_ipj = tr.id_tramite_ipj
                join ipj.t_legajos l
                  on l.id_legajo = i.id_legajo
               where nvl(i.id_integrante, 0) > 0
                 and -- Personas F�sicas
                     nvl(i.fecha_baja, sysdate) >= trunc(sysdate)
                 and -- Vigentes
                     nvl(l.eliminado, 0) <> 1
                 and -- Entidades no eliminadas
                     l.fecha_baja_entidad is null
                 and -- Entidades no dadas de baja
                     tr.id_estado_ult between 100 and 199 -- En tr�mites inscriptos
              UNION
              select distinct i.id_integrante, ---representante
                              i.id_legajo,
                              i.id_tramite_ipj,
                              51,
                              i.fecha_baja,
                              i.persona_expuesta
                from ipj.t_entidades_representante i
                join ipj.t_tramitesipj tr
                  on i.id_tramite_ipj = tr.id_tramite_ipj
                join ipj.t_legajos l
                  on l.id_legajo = i.id_legajo
               where nvl(i.id_integrante, 0) > 0
                 and -- Personas F�sicas
                     nvl(i.fecha_baja, sysdate) >= trunc(sysdate)
                 and -- Vigentes
                     nvl(l.eliminado, 0) <> 1
                 and -- Entidades no eliminadas
                     l.fecha_baja_entidad is null
                 and -- Entidades no dadas de baja
                     tr.id_estado_ult between 100 and 199 -- En tr�mites inscriptos

              UNION
              select distinct i.id_integrante, --socio
                              i.id_legajo,
                              i.id_tramite_ipj,
                              i.id_tipo_integrante,
                              i.fecha_fin,
                              i.persona_expuesta
                from ipj.t_entidades_socios i
                join ipj.t_tramitesipj tr
                  on i.id_tramite_ipj = tr.id_tramite_ipj
                join ipj.t_legajos l
                  on l.id_legajo = i.id_legajo
               where nvl(i.id_integrante, 0) > 0
                 and -- Personas F�sicas
                     nvl(i.fecha_fin, sysdate) >= trunc(sysdate)
                 and -- Vigentes
                     nvl(l.eliminado, 0) <> 1
                 and -- Entidades no eliminadas
                     l.fecha_baja_entidad is null
                 and -- Entidades no dadas de baja
                     tr.id_estado_ult between 100 and 199 -- En tr�mites inscriptos

              ) ad

          on e.id_tramite_ipj = ad.id_tramite_ipj
         and e.id_legajo = ad.id_legajo
        join ipj.t_integrantes i
          on i.id_integrante = ad.id_integrante
        join ipj.t_tipos_integrante ti
          on ti.id_tipo_integrante = ad.id_tipo_integrante

      /* join ipj.t_integrantes i
         on tt.id_integrante = i.id_integrante,

      ipj.t_entidades_admin ad,
       ipj.t_legajos l,
       ipj.t_entidades e,
       ipj.t_tipos_organismo o,
       ipj.t_tipos_integrante ti*/

       where te.id_tipo_entidad = e.id_tipo_entidad
         and tr.id_tramite_ipj = e.id_tramite_ipj
         and suactr.id_tramite = tr.id_tramite
         and subtip.id_subtipo_tramite = suactr.id_subtipo_tramite

         and E.BORRADOR = 'N'
         and (ad.fecha_fin >= to_date(sysdate, 'dd/mm/rrrr') or
             ad.fecha_fin =
             (select max(adm.fecha_fin)
                 from ipj.t_tramitesipj tr
                 join ipj.t_entidades_admin adm
                   on tr.id_tramite_ipj = adm.id_tramite_ipj
                where adm.id_legajo = l.id_legajo
                  and tr.id_estado_ult between 100 and 199))

         and ti.id_tipo_integrante IN ('5',
                                       '3',
                                       '24',
                                       '2',
                                       '9',
                                       '11',
                                       '38',
                                       '51',
                                       '52',
                                       '28',
                                       '30',
                                       '34',
                                       '1',
                                       '14',
                                       '42',
                                       '49',
                                       '39',
                                       '20',
                                       '23',
                                       '10')
            -------------

         and ad.persona_expuesta is not null
            --and nvl(e.fec_inscripcion, e.fec_resolucion) >= '01-oct-2020'

            /*and nvl(e.fec_inscripcion, e.fec_resolucion) >= V_FECHA_DESDE
            and nvl(e.fec_inscripcion, e.fec_resolucion) <= V_FECHA_HASTA*/

         and i.nro_documento = V_DOCUMENTO;

  EXCEPTION
    WHEN V_ERROR_00001 THEN
      V_MSG := 'Mal.';

  END MAS_DE5_PERSONAS_JURIDICAS;
  
  PROCEDURE BUSCAR_ENTIDADES_MISMO_DOMI(p_T_CONSULTA  OUT TY_CURSOR,
                                        P_FECHA_DESDE IN IPJ.t_Entidades.FEC_INSCRIPCION%TYPE DEFAULT NULL,
                                        P_FECHA_HASTA IN IPJ.t_Entidades.FEC_INSCRIPCION%TYPE DEFAULT NULL,
                                        P_CALLE       IN dom_manager.vt_domicilios_cond.n_calle%TYPE DEFAULT NULL,
                                        P_NRO         IN dom_manager.vt_domicilios_cond.altura%TYPE DEFAULT NULL) IS
    V_MSG         VARCHAR2(300);
    V_FECHA_DESDE DATE;
    V_FECHA_HASTA DATE;
    V_SENTENCIA   VARCHAR2(50);
    V_CALLE       VARCHAR2(300);
    V_NRO         number;
  
  BEGIN
  
    V_FECHA_DESDE := P_FECHA_DESDE;
    V_FECHA_HASTA := P_FECHA_HASTA;
    V_CALLE       := P_CALLE;
    V_NRO         := P_NRO;
  
    V_SENTENCIA := '  ALTER SESSION SET NLS_DATE_FORMAT=' || '''' ||
                   'DD/MM/YYYY' || '''';
  
    EXECUTE IMMEDIATE V_SENTENCIA;
    /*    delete ipj.t_uif_legajo_domicilio ;
    commit;
    insert into ipj.t_uif_legajo_domicilio ui
      (select le.id_legajo,
              ipj.entidad_persjur.fc_buscar_ult_dom(le.id_legajo, 'S'),
              user,
              sysdate
    
         FROM ipj.t_legajos le);
    commit;*/
  
    OPEN p_T_CONSULTA FOR
    
    /*  -- ENTIDADES CON EL MISMO DOMICILIO ws60488
                        **********************************************************\*/
    /*select
                        ----------------------------cabecera--------------------
                        distinct ipj.entidad_persjur.fc_buscar_ult_dom(le.id_legajo, 'S') as id_domicilio,
                                 LE.ID_LEGAJO,
                                 le.denominacion_sia "Denominaci93n", ---*novacia
                                 e.cuit              "Nro_de_CUIT", ---* sivacia
                                 to_char(to_date(E.ACTA_CONTITUTIVA, 'dd/mm/yyyy'),'yyyy-MM-dd')
                                 || 'T00:00:00-03:00' "Fecha_de_Constituci93n", ---*novacia
    
                                 (select (upper(substr(d.n_calle, 1, 1)) ||
                                         lower(SUBSTR(d.n_calle, 2)))
                                    from dom_manager.vt_domicilios_cond d
                                   where d.id_vin =
                                         IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(le.id_legajo, 'S')) "Calle", ---*novacia
    
    
                                 (select d.altura
                                    from dom_manager.vt_domicilios_cond d
                                   where d.id_vin =
                                         IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(le.id_legajo, 'S')) "Nro", ---*novacia
    
                                 (select d.piso
                                    from dom_manager.vt_domicilios_cond d
                                   where d.id_vin =
                                         IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(le.id_legajo, 'S')) "Piso", ---*sivacio
    
                                 (select d.depto
                                    from dom_manager.vt_domicilios_cond d
                                   where d.id_vin =
                                         IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(le.id_legajo, 'S')) "Departamento", ---* sivacio
    
                                 (select d.n_localidad
                                    from dom_manager.vt_domicilios_cond d
                                   where d.id_vin =
                                         ipj.entidad_persjur.fc_buscar_ult_dom(le.id_legajo, 'N')) "Localidad", ---*novacia
    
                                 (select d.cpa
                                    from dom_manager.vt_domicilios_cond d
                                   where d.id_vin =
                                         IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(le.id_legajo, 'S')) "C93digo_Postal", ---*sivacia
    
                                 (select (upper(substr(d.n_provincia, 1, 1)) ||
                                         lower(SUBSTR(d.n_provincia, 2)))
                                    from dom_manager.vt_domicilios_cond d
                                   where d.id_vin =
                                         IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(le.id_legajo, 'S')) "Provincia", ---*novacio
    
                                 (select (upper(substr(pa.n_pais, 1, 1)) ||
                                         lower(SUBSTR(pa.n_pais, 2)))
                                    from dom_manager.vt_paises pa
                                   where pa.ID_PAIS = 'ARG') "Pa92s", ---*novacia
    
                                 '' as Prefijo, --sivacia
                                 '' as Nro_Tel, --sivacia
                                 '' as Email --sivacia
    
                        ----------------------------cabecera--------------------
    
    
                          from ipj.t_tipos_entidades         te,
                               ipj.t_tramitesipj             tr,
                               nuevosuac.vt_tramites_id      suactr,
                               nuevosuac.vt_subtipos_tramite subtip,
                               ipj.t_legajos                 le
                          join ipj.t_entidades e
                            on le.id_legajo = e.id_legajo
                          join ipj.t_entidades_admin ad
                            on e.id_tramite_ipj = ad.id_tramite_ipj
                           and e.id_legajo = ad.id_legajo
                          join ipj.t_integrantes i
                            on i.id_integrante = ad.id_integrante
                          join ipj.t_tipos_integrante ti
                            on ti.id_tipo_integrante = ad.id_tipo_integrante
                          join ipj.t_tipos_organismo o
                            on o.id_tipo_organismo = ad.id_tipo_organismo
    
                         where nvl(le.eliminado, 0) <> 1
    
                           and le.fecha_baja_entidad is null
    
                           and ipj.entidad_persjur.fc_buscar_ult_dom(le.id_legajo, 'S') in
                               (select domicilio
                                  from (select le.id_legajo,
                                               ipj.entidad_persjur.fc_buscar_ult_dom(le.id_legajo,
                                                                                     'S') domicilio
                                          from ipj.t_tipos_entidades         te,
                                               ipj.t_tramitesipj             tr,
                                               nuevosuac.vt_tramites_id      suactr,
                                               nuevosuac.vt_subtipos_tramite subtip,
                                               ipj.t_legajos                 le
                                          join ipj.t_entidades e
                                            on le.id_legajo = e.id_legajo
                                          join ipj.t_entidades_admin ad
                                            on e.id_tramite_ipj = ad.id_tramite_ipj
                                           and e.id_legajo = ad.id_legajo
                                          join ipj.t_integrantes i
                                            on i.id_integrante = ad.id_integrante
                                          join ipj.t_tipos_integrante ti
                                            on ti.id_tipo_integrante = ad.id_tipo_integrante
                                          join ipj.t_tipos_organismo o
                                            on o.id_tipo_organismo = ad.id_tipo_organismo
    
                                         where nvl(le.eliminado, 0) <> 1
    
                                           and le.fecha_baja_entidad is null
    
                                           and te.id_tipo_entidad = e.id_tipo_entidad
                                           and tr.id_tramite_ipj = e.id_tramite_ipj
                                           and suactr.id_tramite = tr.id_tramite
                                           and subtip.id_subtipo_tramite = suactr.id_subtipo_tramite
                                           and tr.id_estado_ult between 100 and 199 -- Tramites OK
                                           --and nvl(e.fec_inscripcion, e.fec_resolucion) <= '01-oct-2021'
                                            and nvl(e.fec_inscripcion, e.fec_resolucion) between V_FECHA_DESDE and  V_FECHA_HASTA
    
                                           ) t
                                 where nvl(domicilio, 0) > 0
    
                                 group by domicilio
                                having count(DISTINCT id_legajo) > 1)
    
                           and te.id_tipo_entidad = e.id_tipo_entidad
                           and tr.id_tramite_ipj = e.id_tramite_ipj
                           and suactr.id_tramite = tr.id_tramite
                           and subtip.id_subtipo_tramite = suactr.id_subtipo_tramite
                           and tr.id_estado_ult between 100 and 199 -- Tramites OK
    
                        --   and nvl(e.fec_inscripcion, e.fec_resolucion) <= '01-oct-2021'
                          and nvl(e.fec_inscripcion, e.fec_resolucion) between V_FECHA_DESDE and  V_FECHA_HASTA
                          --and IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(le.id_legajo, 'S') IN (6736506,6739913)
    
                        \*and le.id_legajo in (43902
                        ,44038
                        ,45721
                        ,45057
                        ,45719
                        )*\
    
                        order by IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(le.id_legajo, 'S');*/
select distinct uifld.id_vin_real as id_domicilio,
                LE.ID_LEGAJO,
                le.denominacion_sia "Denominaci93n", ---*novacia
                e.cuit "Nro_de_CUIT", ---* sivacia
                to_char(to_date(E.ACTA_CONTITUTIVA, 'dd/mm/yyyy'),
                        'yyyy-MM-dd') || 'T00:00:00-03:00' "Fecha_de_Constituci93n", ---*novacia
                
                (select (upper(substr(d.n_calle, 1, 1)) ||
                        lower(SUBSTR(d.n_calle, 2)))
                   from dom_manager.vt_domicilios_cond d
                  where d.id_vin = uifld.id_vin_real) "Calle", ---*novacia
                
                (select decode(d.altura, null, 0, d.altura)
                   from dom_manager.vt_domicilios_cond d
                  where d.id_vin = uifld.id_vin_real) "Nro", ---*novacia
                
                (select d.piso
                   from dom_manager.vt_domicilios_cond d
                  where d.id_vin = uifld.id_vin_real) "Piso", ---*sivacio
                
                (select d.depto
                   from dom_manager.vt_domicilios_cond d
                  where d.id_vin = uifld.id_vin_real) "Departamento", ---* sivacio
                
                (select d.n_localidad
                   from dom_manager.vt_domicilios_cond d
                  where d.id_vin = uifld.id_vin_real) "Localidad", ---*novacia
                
                (select d.cpa
                   from dom_manager.vt_domicilios_cond d
                  where d.id_vin = uifld.id_vin_real) "C93digo_Postal", ---*sivacia
                
                (select (upper(substr(d.n_provincia, 1, 1)) ||
                        lower(SUBSTR(d.n_provincia, 2)))
                   from dom_manager.vt_domicilios_cond d
                  where d.id_vin = uifld.id_vin_real) "Provincia", ---*novacio
                
                (select (upper(substr(pa.n_pais, 1, 1)) ||
                        lower(SUBSTR(pa.n_pais, 2)))
                   from dom_manager.vt_paises pa
                  where pa.ID_PAIS = 'ARG') "Pa92s", ---*novacia
                
                '' as Prefijo, --sivacia
                '' as Nro_Tel, --sivacia
                '' as Email --sivacia
  FROM ipj.t_tramitesipj             tr,
       nuevosuac.vt_tramites_id      suactr,
       nuevosuac.vt_subtipos_tramite subtip,
       ipj.t_legajos                 le,
       ipj.t_entidades               e,
       ipj.t_uif_legajo_domicilio    uifld

 WHERE tr.id_tramite_ipj = e.id_tramite_ipj
   and suactr.id_tramite = tr.id_tramite
   and subtip.id_subtipo_tramite = suactr.id_subtipo_tramite
   and le.id_legajo = e.id_legajo
      
   and uifld.id_legajo = le.id_legajo
   and uifld.id_vin_real = e.id_vin_real
   and uifld.id_tramite_ipj = e.id_tramite_ipj
   and nvl(le.eliminado, 0) = 0
      
   and le.fecha_baja_entidad is null
   AND uifld.id_vin_real in
       (SELECT id_domicilio
          FROM (select distinct (LE1.ID_LEGAJO),
                                uifld1.id_vin_real as id_domicilio
                  FROM ipj.t_tramitesipj             tr1,
                       ipj.t_legajos                 le1,
                       nuevosuac.vt_tramites_id      suactr1,
                       nuevosuac.vt_subtipos_tramite subtip1,
                       ipj.t_entidades               e1,
                       ipj.t_uif_legajo_domicilio    uifld1
                
                 where nvl(le1.eliminado, 0) = 0
                   and le1.fecha_baja_entidad is NULL
                   and tr1.id_tramite_ipj = e1.id_tramite_ipj
                   and le1.id_legajo = e1.id_legajo
                   and suactr1.id_tramite = tr1.id_tramite
                   and subtip1.id_subtipo_tramite =
                       suactr1.id_subtipo_tramite
                   and uifld1.id_legajo = le1.id_legajo
                   and uifld1.id_vin_real = e1.id_vin_real
                   and uifld1.id_tramite_ipj = e1.id_tramite_ipj
                   and tr1.id_estado_ult between 100 and 199 -- Tramites OK
                      
                   and le1.fecha_baja_entidad is null)
         group by id_domicilio
        having count(id_domicilio) >= 2)
      
   and tr.id_estado_ult between 100 and 199 -- Tramites OK

 order by 1;
  
  EXCEPTION
    WHEN V_ERROR_00001 THEN
      V_MSG := 'Mal.';
  END BUSCAR_ENTIDADES_MISMO_DOMI;

  PROCEDURE BUSCAR_MISMO_DOMICILIO_PF(p_T_CONSULTA   OUT TY_CURSOR,
                                      P_FECHA_DESDE  IN IPJ.t_Entidades.FEC_INSCRIPCION%TYPE DEFAULT NULL,
                                      P_FECHA_HASTA  IN IPJ.t_Entidades.FEC_INSCRIPCION%TYPE DEFAULT NULL,
                                      P_ID_DOMICILIO IN IPJ.t_Entidades.ID_LEGAJO%TYPE DEFAULT NULL) IS
    V_MSG          VARCHAR2(300);
    V_FECHA_DESDE  DATE;
    V_FECHA_HASTA  DATE;
    V_SENTENCIA    VARCHAR2(50);
    V_ID_DOMICILIO number;

  BEGIN

    V_FECHA_DESDE  := P_FECHA_DESDE;
    V_FECHA_HASTA  := P_FECHA_HASTA;
    V_ID_DOMICILIO := P_ID_DOMICILIO;

    V_SENTENCIA := '  ALTER SESSION SET NLS_DATE_FORMAT=' || '''' ||
                   'DD/MM/YYYY' || '''';

    EXECUTE IMMEDIATE V_SENTENCIA;

    OPEN p_T_CONSULTA FOR

    --PERSONA FISICA

      select distinct uifld.id_vin_real as id_domicilio,

                      (select (upper(substr(vp.APELLIDO, 1, 1)) ||
                              lower(SUBSTR(vp.APELLIDO, 2)))
                         from rcivil.vt_pk_persona vp
                        where vp.id_sexo = i.id_sexo
                          and vp.nro_documento = i.nro_documento
                          and vp.pai_cod_pais = i.pai_cod_pais
                          and vp.id_numero = i.id_numero) "Apellido88PerFisica", ---*--novacia

                      '' as Segundo_Apellido88PerFisica, --sivacia

                      (select initcap(vp.nombre)
                         from rcivil.vt_pk_persona vp
                        where vp.id_sexo = i.id_sexo
                          and vp.nro_documento = i.nro_documento
                          and vp.pai_cod_pais = i.pai_cod_pais
                          and vp.id_numero = i.id_numero) "Nombre88PerFisica", ---*--novacia

                      '' as Segundo_Nombre88PerFisica, --sivacia

                      (select vp.n_tipo_documento
                         from rcivil.vt_pk_persona vp
                        where vp.id_sexo = i.id_sexo
                          and vp.nro_documento = i.nro_documento
                          and vp.pai_cod_pais = i.pai_cod_pais
                          and vp.id_numero = i.id_numero) "Tipo_Documento88PerFisica", ---*--novacia

                      i.nro_documento "N94mero_documento88PerFisica", ---*--novacia

                      (select vp.cuil
                         from rcivil.vt_pk_persona vp
                        where vp.id_sexo = i.id_sexo
                          and vp.nro_documento = i.nro_documento
                          and vp.pai_cod_pais = i.pai_cod_pais
                          and vp.id_numero = i.id_numero) "Nro_de_CUIT_CUIL88PerFisica", ---*--sivacia

                      ad.persona_expuesta "Radicada_en_el_Exterior88PerFisica", ---*--novacia

                      ad.persona_expuesta "Radicada_en_Paraiso_Fiscal88PerFisica", ---*--novacia

                      ad.persona_expuesta "PEP88PerFisica", ---* --novacia

                      (select (upper(substr(d.n_calle, 1, 1)) ||
                              lower(SUBSTR(d.n_calle, 2)))
                         from DOM_MANAGER.VT_DOMICILIOS_COND d
                        where d.id_vin =
                              IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo,
                                                               i.nro_documento,
                                                               i.pai_cod_pais,
                                                               to_char(i.id_numero),
                                                               3)) "Calle88PerFisica", ---*--novacia
                      (select d.altura
                         from DOM_MANAGER.VT_DOMICILIOS_COND d
                        where d.id_vin =
                              IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo,
                                                               i.nro_documento,
                                                               i.pai_cod_pais,
                                                               to_char(i.id_numero),
                                                               3)) "Nro88PerFisica", ---*--novacia
                      (select d.piso
                         from DOM_MANAGER.VT_DOMICILIOS_COND d
                        where d.id_vin =
                              IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo,
                                                               i.nro_documento,
                                                               i.pai_cod_pais,
                                                               to_char(i.id_numero),
                                                               3)) "Piso88PerFisica", ---*si--novacia
                      (select d.depto
                         from DOM_MANAGER.VT_DOMICILIOS_COND d
                        where d.id_vin =
                              IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo,
                                                               i.nro_documento,
                                                               i.pai_cod_pais,
                                                               to_char(i.id_numero),
                                                               3)) "Departamento88PerFisica", ---*si--novacia
                      (select (upper(substr(d.n_localidad, 1, 1)) ||
                              lower(SUBSTR(d.n_localidad, 2)))
                         from DOM_MANAGER.VT_DOMICILIOS_COND d
                        where d.id_vin =
                              IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo,
                                                               i.nro_documento,
                                                               i.pai_cod_pais,
                                                               to_char(i.id_numero),
                                                               3)) "Localidad88PerFisica", ---*--novacia
                      (select d.cpa
                         from DOM_MANAGER.VT_DOMICILIOS_COND d
                        where d.id_vin =
                              IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo,
                                                               i.nro_documento,
                                                               i.pai_cod_pais,
                                                               to_char(i.id_numero),
                                                               3)) "C93digo_Postal88PerFisica", ---*si--novacia
                      (select (upper(substr(d.n_provincia, 1, 1)) ||
                              lower(SUBSTR(d.n_provincia, 2)))
                         from dom_manager.vt_domicilios_cond d
                        where d.id_vin = uifld.id_vin_real) "Provincia88PerFisica", ---*novacio

                      (select (upper(substr(pa.n_pais, 1, 1)) ||
                              lower(SUBSTR(pa.n_pais, 2)))
                         from dom_manager.vt_paises pa
                        where pa.ID_PAIS = 'ARG') "Pa92s88PerFisica", ---*--novacia

                      IPJ.VARIOS.FC_Buscar_Te_Caract(i.id_sexo ||
                                                     i.nro_documento ||
                                                     i.pai_cod_pais ||
                                                     to_char(i.id_numero),
                                                     126) "Prefijo88PerFisica", ---*si--novacia

                      IPJ.VARIOS.FC_Buscar_Telefono(i.id_sexo ||
                                                    i.nro_documento ||
                                                    i.pai_cod_pais ||
                                                    to_char(i.id_numero),
                                                    126) "Tel91fono88PerFisica", ---*si--novacia

                      IPJ.VARIOS.FC_BUSCAR_MAIL(i.id_sexo ||
                                                i.nro_documento ||
                                                i.pai_cod_pais ||
                                                to_char(i.id_numero),
                                                126) "Email88PerFisica", ---*si--novacia

                      (select vp.FECHA_NACIMIENTO
                         from rcivil.vt_pk_persona vp
                        where vp.id_sexo = i.id_sexo
                          and vp.nro_documento = i.nro_documento
                          and vp.pai_cod_pais = i.pai_cod_pais
                          and vp.id_numero = i.id_numero) "Fecha_de_Nacimiento88PerFisica", ---*si--novacia

                      CASE UPPER(ti.n_tipo_integrante)

                        WHEN UPPER('Socio/a') THEN
                         'Socio' --

                        WHEN UPPER('Gerente/a') THEN
                         'Gerente' --

                        WHEN UPPER('S�ndico/a Titular') THEN
                         'S�ndico' --

                        WHEN UPPER('Tesorero/a') THEN
                         'Tesorero' --

                        WHEN UPPER('Presidente/a') THEN
                         'Presidente' --

                        WHEN UPPER('Director/a') THEN
                         'Director' --

                        WHEN UPPER('Director/a Titular') THEN
                         'Director' --

                        WHEN UPPER('Vice-Presidente/a') THEN
                         'Vicepresidente' --

                        WHEN UPPER('Vicepresidente/a') THEN
                         'Vicepresidente' --

                        WHEN UPPER('Titular') THEN
                         'Titular' --

                        WHEN UPPER('Representante') THEN
                         'Representante Legal' --

                        ELSE
                         'Otros'
                      END AS "Caracter_Invocado88PerFisica", ---* --novacia

                      '' as Sociedad_Vinculada --sivacia

        from ipj.t_tipos_entidades         te,
             ipj.t_tramitesipj             tr,
             nuevosuac.vt_tramites_id      suactr,
             nuevosuac.vt_subtipos_tramite subtip,
             ipj.t_uif_legajo_domicilio    uifld,
             ipj.t_legajos                 le
        join ipj.t_entidades e
          on le.id_legajo = e.id_legajo
        join ipj.t_entidades_admin ad
          on e.id_tramite_ipj = ad.id_tramite_ipj
         and e.id_legajo = ad.id_legajo
        join ipj.t_integrantes i
          on i.id_integrante = ad.id_integrante
        join ipj.t_tipos_integrante ti
          on ti.id_tipo_integrante = ad.id_tipo_integrante
        join ipj.t_tipos_organismo o
          on o.id_tipo_organismo = ad.id_tipo_organismo

       where nvl(le.eliminado, 0) <> 1

         and le.fecha_baja_entidad is null

         and uifld.id_vin_real in
             (select domicilio
                from (select le.id_legajo, uifld1.id_vin_real domicilio
                        from ipj.t_tipos_entidades         te,
                             ipj.t_tramitesipj             tr,
                             nuevosuac.vt_tramites_id      suactr,
                             nuevosuac.vt_subtipos_tramite subtip,
                             ipj.t_legajos                 le,
                             ipj.t_uif_legajo_domicilio    uifld1
                        join ipj.t_entidades e
                          on le.id_legajo = e.id_legajo
                        join ipj.t_entidades_admin ad
                          on e.id_tramite_ipj = ad.id_tramite_ipj
                         and e.id_legajo = ad.id_legajo
                        join ipj.t_integrantes i
                          on i.id_integrante = ad.id_integrante
                        join ipj.t_tipos_integrante ti
                          on ti.id_tipo_integrante = ad.id_tipo_integrante
                        join ipj.t_tipos_organismo o
                          on o.id_tipo_organismo = ad.id_tipo_organismo

                       where nvl(le.eliminado, 0) <> 1

                         and le.fecha_baja_entidad is null
                         and uifld1.id_legajo = le.id_legajo
                         and te.id_tipo_entidad = e.id_tipo_entidad
                         and tr.id_tramite_ipj = e.id_tramite_ipj
                         and suactr.id_tramite = tr.id_tramite
                         and subtip.id_subtipo_tramite =
                             suactr.id_subtipo_tramite
                         and tr.id_estado_ult between 100 and 199 -- Tramites OK
                      ) t
               where nvl(domicilio, 0) > 0

               group by domicilio
              having count(DISTINCT id_legajo) > 1)

         and te.id_tipo_entidad = e.id_tipo_entidad
         and tr.id_tramite_ipj = e.id_tramite_ipj
         and suactr.id_tramite = tr.id_tramite
         and subtip.id_subtipo_tramite = suactr.id_subtipo_tramite
         and uifld.id_legajo = le.id_legajo
         and tr.id_estado_ult between 100 and 199 -- Tramites OK
         and uifld.id_vin_real IN (V_ID_DOMICILIO)

       ORDER BY 1;

  EXCEPTION
    WHEN V_ERROR_00001 THEN
      V_MSG := 'Mal.';
  END BUSCAR_MISMO_DOMICILIO_PF;

  PROCEDURE BUSCAR_MISMO_DOMICILIO_PJ(p_T_CONSULTA   OUT TY_CURSOR,
                                      P_FECHA_DESDE  IN IPJ.t_Entidades.FEC_INSCRIPCION%TYPE DEFAULT NULL,
                                      P_FECHA_HASTA  IN IPJ.t_Entidades.FEC_INSCRIPCION%TYPE DEFAULT NULL,
                                      P_ID_DOMICILIO IN IPJ.t_Entidades.ID_LEGAJO%TYPE DEFAULT NULL) IS
    V_MSG          VARCHAR2(300);
    V_FECHA_DESDE  DATE;
    V_FECHA_HASTA  DATE;
    V_SENTENCIA    VARCHAR2(50);
    V_ID_DOMICILIO number;

  BEGIN

    V_FECHA_DESDE  := P_FECHA_DESDE;
    V_FECHA_HASTA  := P_FECHA_HASTA;
    V_ID_DOMICILIO := P_ID_DOMICILIO;

    V_SENTENCIA := '  ALTER SESSION SET NLS_DATE_FORMAT=' || '''' ||
                   'DD/MM/YYYY' || '''';

    EXECUTE IMMEDIATE V_SENTENCIA;

    OPEN p_T_CONSULTA FOR
    ---Persona Jur�dica
      select distinct uifld.id_vin_real as id_domicilio,

                      (select PJU.RAZON_SOCIAL
                         from t_comunes.vt_pers_juridicas_unica PJU
                        where PJU.CUIT = e.cuit) "Denominaci93n88PerJuridica", ---*--novacia

                      (select FJ.N_FORMA_JURIDICA
                         from t_comunes.vt_formas_juridica      FJ,
                              t_comunes.vt_pers_juridicas_unica PJU
                        where PJU.CUIT = e.cuit
                          and FJ.ID_FORMA_JURIDICA = PJu.ID_FORMA_JURIDICA) "Forma_Jur92dica88PerJuridica", ---*--novacia

                      (select PJ.CUIT
                         from t_comunes.vt_pers_juridicas_unica pj
                        where pj.CUIT = E.CUIT) "Nro_CUIT88PerJuridica", ---*--novacia

                      (select (upper(substr(d.n_calle, 1, 1)) ||
                              lower(SUBSTR(d.n_calle, 2)))
                         from dom_manager.vt_domicilios_cond d
                        where d.id_vin = uifld.id_vin_real) as "Calle88PerJuridica", --novacia
                      (select decode(d.altura, null, 0, d.altura) --agregado 11/08/2021
                         from dom_manager.vt_domicilios_cond d
                        where d.id_vin = uifld.id_vin_real) as "Nro88PerJuridica", --novacia
                      (select d.piso
                         from dom_manager.vt_domicilios_cond d
                        where d.id_vin = uifld.id_vin_real) as "Piso88PerJuridica", --si--novacia
                      (select d.depto
                         from dom_manager.vt_domicilios_cond d
                        where d.id_vin = uifld.id_vin_real) as "Departamento88PerJuridica", --si--novacia
                      (select (upper(substr(d.n_localidad, 1, 1)) ||
                              lower(SUBSTR(d.n_localidad, 2)))
                         from dom_manager.vt_domicilios_cond d
                        where d.id_vin = uifld.id_vin_real) as "Localidad88PerJuridica", ----novacia
                      (select d.cpa
                         from dom_manager.vt_domicilios_cond d
                        where d.id_vin = uifld.id_vin_real) as "C93digo_Postal88PerJuridica", --si--novacia
                      (select (upper(substr(d.n_provincia, 1, 1)) ||
                              lower(SUBSTR(d.n_provincia, 2)))
                         from dom_manager.vt_domicilios_cond d
                        where d.id_vin = uifld.id_vin_real) as "Provincia88PerJuridica", --novacio
                      (select pa.n_pais
                         from dom_manager.vt_domicilios_cond d
                         join DOM_MANAGER.VT_PROVINCIAS pr
                           on pr.id_provincia = d.id_provincia
                         join DOM_MANAGER.VT_PAISES pa
                           on pr.id_pais = pa.id_pais
                        where d.id_vin = uifld.id_vin_real) as "Pa92s88PerJuridica", --novacia
                      IPJ.VARIOS.FC_Buscar_Mail(le.cuit || '00', 126) as "Email88PerJuridica", --si--novacia
                      IPJ.VARIOS.FC_Buscar_Te_Caract(le.cuit || '00', 126) as "Prefijo88PerJuridica", --si--novacia
                      IPJ.VARIOS.FC_Buscar_Telefono(le.cuit || '00', 126) as "Tel91fono88PerJuridica", --si--novacia
                      -- '' as "Actividad_que_Desarrolla88PerJuridica",

                      (select max(apju.n_actividad_uif)
                         from t_actividad_persona_juri_uif    apju, /* t_comunes.t_actividades a,*/
                              t_comunes.vt_actividades_perjur ap
                        where apju.id_actividad_uif = ap.id_actividad
                             /*a.id_actividad=ap.id_actividad and*/
                          and ap.cuit = e.cuit) "Actividad_que_Desarrolla88PerJuridica", --novacia

                      '' as Sociedad_Vinculada --si vacia

        from ipj.t_tipos_entidades         te,
             ipj.t_tramitesipj             tr,
             nuevosuac.vt_tramites_id      suactr,
             nuevosuac.vt_subtipos_tramite subtip,
             ipj.t_uif_legajo_domicilio    uifld,
             ipj.t_legajos                 le
        join ipj.t_entidades e
          on le.id_legajo = e.id_legajo
        join ipj.t_entidades_admin ad
          on e.id_tramite_ipj = ad.id_tramite_ipj
         and e.id_legajo = ad.id_legajo
        join ipj.t_integrantes i
          on i.id_integrante = ad.id_integrante
        join ipj.t_tipos_integrante ti
          on ti.id_tipo_integrante = ad.id_tipo_integrante
        join ipj.t_tipos_organismo o
          on o.id_tipo_organismo = ad.id_tipo_organismo

       where nvl(le.eliminado, 0) <> 1

         and le.fecha_baja_entidad is null

         and uifld.id_vin_real in
             (select domicilio
                from (select le.id_legajo, uifld1.id_vin_real domicilio
                        from ipj.t_tipos_entidades         te,
                             ipj.t_tramitesipj             tr,
                             nuevosuac.vt_tramites_id      suactr,
                             nuevosuac.vt_subtipos_tramite subtip,
                             ipj.t_uif_legajo_domicilio    uifld1,
                             ipj.t_legajos                 le
                        join ipj.t_entidades e
                          on le.id_legajo = e.id_legajo
                        join ipj.t_entidades_admin ad
                          on e.id_tramite_ipj = ad.id_tramite_ipj
                         and e.id_legajo = ad.id_legajo
                        join ipj.t_integrantes i
                          on i.id_integrante = ad.id_integrante
                        join ipj.t_tipos_integrante ti
                          on ti.id_tipo_integrante = ad.id_tipo_integrante
                        join ipj.t_tipos_organismo o
                          on o.id_tipo_organismo = ad.id_tipo_organismo

                       where nvl(le.eliminado, 0) <> 1

                         and le.fecha_baja_entidad is null

                         and te.id_tipo_entidad = e.id_tipo_entidad
                         and tr.id_tramite_ipj = e.id_tramite_ipj
                         and suactr.id_tramite = tr.id_tramite
                         and uifld1.id_legajo = le.id_legajo
                         and subtip.id_subtipo_tramite =
                             suactr.id_subtipo_tramite
                         and tr.id_estado_ult between 100 and 199 -- Tramites OK
                         and nvl(e.fec_inscripcion, e.fec_resolucion) between
                             '19/08/2010' and '19/08/2018') t
               where nvl(domicilio, 0) > 0

               group by domicilio
              having count(DISTINCT id_legajo) > 1)

         and te.id_tipo_entidad = e.id_tipo_entidad
         and tr.id_tramite_ipj = e.id_tramite_ipj
         and suactr.id_tramite = tr.id_tramite
         and subtip.id_subtipo_tramite = suactr.id_subtipo_tramite
         and uifld.id_legajo = le.id_legajo
         and tr.id_estado_ult between 100 and 199 -- Tramites OK
         and uifld.id_vin_real IN (V_ID_DOMICILIO)

       ORDER BY 1;

  EXCEPTION
    WHEN V_ERROR_00001 THEN
      V_MSG := 'Mal.';
  END BUSCAR_MISMO_DOMICILIO_PJ;

  /*
    PROCEDURE BUSCAR_ENTIDADES_MISMO_DOMI(p_T_CONSULTA  OUT TY_CURSOR,
                                          P_FECHA_DESDE IN IPJ.t_Entidades.FEC_INSCRIPCION%TYPE DEFAULT NULL,
                                          P_FECHA_HASTA IN IPJ.t_Entidades.FEC_INSCRIPCION%TYPE DEFAULT NULL,
                                          P_CALLE       IN dom_manager.vt_domicilios_cond.n_calle%TYPE DEFAULT NULL,
                                          P_NRO         IN dom_manager.vt_domicilios_cond.altura%TYPE DEFAULT NULL) IS
      V_MSG         VARCHAR2(300);
      V_FECHA_DESDE DATE;
      V_FECHA_HASTA DATE;
      V_SENTENCIA   VARCHAR2(50);
      V_CALLE       VARCHAR2(300);
      V_NRO         number;

    BEGIN

      V_FECHA_DESDE := P_FECHA_DESDE;
      V_FECHA_HASTA := P_FECHA_HASTA;
      V_CALLE       := P_CALLE;
      V_NRO         := P_NRO;

      V_SENTENCIA := '  ALTER SESSION SET NLS_DATE_FORMAT=' || '''' ||
                     'DD/MM/YYYY' || '''';

      EXECUTE IMMEDIATE V_SENTENCIA;

      OPEN p_T_CONSULTA FOR

      \*  -- ENTIDADES CON EL MISMO DOMICILIO ws60488
          **********************************************************\*\
      \*select
          ----------------------------cabecera--------------------
          distinct ipj.entidad_persjur.fc_buscar_ult_dom(le.id_legajo, 'S') as id_domicilio,
                   LE.ID_LEGAJO,
                   le.denominacion_sia "Denominaci93n", ---*novacia
                   e.cuit              "Nro_de_CUIT", ---* sivacia
                   to_char(to_date(E.ACTA_CONTITUTIVA, 'dd/mm/yyyy'),'yyyy-MM-dd')
                   || 'T00:00:00-03:00' "Fecha_de_Constituci93n", ---*novacia

                   (select (upper(substr(d.n_calle, 1, 1)) ||
                           lower(SUBSTR(d.n_calle, 2)))
                      from dom_manager.vt_domicilios_cond d
                     where d.id_vin =
                           IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(le.id_legajo, 'S')) "Calle", ---*novacia


                   (select d.altura
                      from dom_manager.vt_domicilios_cond d
                     where d.id_vin =
                           IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(le.id_legajo, 'S')) "Nro", ---*novacia

                   (select d.piso
                      from dom_manager.vt_domicilios_cond d
                     where d.id_vin =
                           IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(le.id_legajo, 'S')) "Piso", ---*sivacio

                   (select d.depto
                      from dom_manager.vt_domicilios_cond d
                     where d.id_vin =
                           IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(le.id_legajo, 'S')) "Departamento", ---* sivacio

                   (select d.n_localidad
                      from dom_manager.vt_domicilios_cond d
                     where d.id_vin =
                           ipj.entidad_persjur.fc_buscar_ult_dom(le.id_legajo, 'N')) "Localidad", ---*novacia

                   (select d.cpa
                      from dom_manager.vt_domicilios_cond d
                     where d.id_vin =
                           IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(le.id_legajo, 'S')) "C93digo_Postal", ---*sivacia

                   (select (upper(substr(d.n_provincia, 1, 1)) ||
                           lower(SUBSTR(d.n_provincia, 2)))
                      from dom_manager.vt_domicilios_cond d
                     where d.id_vin =
                           IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(le.id_legajo, 'S')) "Provincia", ---*novacio

                   (select (upper(substr(pa.n_pais, 1, 1)) ||
                           lower(SUBSTR(pa.n_pais, 2)))
                      from dom_manager.vt_paises pa
                     where pa.ID_PAIS = 'ARG') "Pa92s", ---*novacia

                   '' as Prefijo, --sivacia
                   '' as Nro_Tel, --sivacia
                   '' as Email --sivacia

          ----------------------------cabecera--------------------


            from ipj.t_tipos_entidades         te,
                 ipj.t_tramitesipj             tr,
                 nuevosuac.vt_tramites_id      suactr,
                 nuevosuac.vt_subtipos_tramite subtip,
                 ipj.t_legajos                 le
            join ipj.t_entidades e
              on le.id_legajo = e.id_legajo
            join ipj.t_entidades_admin ad
              on e.id_tramite_ipj = ad.id_tramite_ipj
             and e.id_legajo = ad.id_legajo
            join ipj.t_integrantes i
              on i.id_integrante = ad.id_integrante
            join ipj.t_tipos_integrante ti
              on ti.id_tipo_integrante = ad.id_tipo_integrante
            join ipj.t_tipos_organismo o
              on o.id_tipo_organismo = ad.id_tipo_organismo

           where nvl(le.eliminado, 0) <> 1

             and le.fecha_baja_entidad is null

             and ipj.entidad_persjur.fc_buscar_ult_dom(le.id_legajo, 'S') in
                 (select domicilio
                    from (select le.id_legajo,
                                 ipj.entidad_persjur.fc_buscar_ult_dom(le.id_legajo,
                                                                       'S') domicilio
                            from ipj.t_tipos_entidades         te,
                                 ipj.t_tramitesipj             tr,
                                 nuevosuac.vt_tramites_id      suactr,
                                 nuevosuac.vt_subtipos_tramite subtip,
                                 ipj.t_legajos                 le
                            join ipj.t_entidades e
                              on le.id_legajo = e.id_legajo
                            join ipj.t_entidades_admin ad
                              on e.id_tramite_ipj = ad.id_tramite_ipj
                             and e.id_legajo = ad.id_legajo
                            join ipj.t_integrantes i
                              on i.id_integrante = ad.id_integrante
                            join ipj.t_tipos_integrante ti
                              on ti.id_tipo_integrante = ad.id_tipo_integrante
                            join ipj.t_tipos_organismo o
                              on o.id_tipo_organismo = ad.id_tipo_organismo

                           where nvl(le.eliminado, 0) <> 1

                             and le.fecha_baja_entidad is null

                             and te.id_tipo_entidad = e.id_tipo_entidad
                             and tr.id_tramite_ipj = e.id_tramite_ipj
                             and suactr.id_tramite = tr.id_tramite
                             and subtip.id_subtipo_tramite = suactr.id_subtipo_tramite
                             and tr.id_estado_ult between 100 and 199 -- Tramites OK
                             --and nvl(e.fec_inscripcion, e.fec_resolucion) <= '01-oct-2021'
                              and nvl(e.fec_inscripcion, e.fec_resolucion) between V_FECHA_DESDE and  V_FECHA_HASTA

                             ) t
                   where nvl(domicilio, 0) > 0

                   group by domicilio
                  having count(DISTINCT id_legajo) > 1)

             and te.id_tipo_entidad = e.id_tipo_entidad
             and tr.id_tramite_ipj = e.id_tramite_ipj
             and suactr.id_tramite = tr.id_tramite
             and subtip.id_subtipo_tramite = suactr.id_subtipo_tramite
             and tr.id_estado_ult between 100 and 199 -- Tramites OK

          --   and nvl(e.fec_inscripcion, e.fec_resolucion) <= '01-oct-2021'
            and nvl(e.fec_inscripcion, e.fec_resolucion) between V_FECHA_DESDE and  V_FECHA_HASTA
            --and IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(le.id_legajo, 'S') IN (6736506,6739913)

          \*and le.id_legajo in (43902
          ,44038
          ,45721
          ,45057
          ,45719
          )*\

          order by IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(le.id_legajo, 'S');*\
        select distinct ipj.entidad_persjur.fc_buscar_ult_dom(le.id_legajo,
                                                              'S') as id_domicilio,
                        LE.ID_LEGAJO,
                        le.denominacion_sia "Denominaci93n", ---*novacia
                        e.cuit "Nro_de_CUIT", ---* sivacia
                        to_char(to_date(E.ACTA_CONTITUTIVA, 'dd/mm/yyyy'),
                                'yyyy-MM-dd') || 'T00:00:00-03:00' "Fecha_de_Constituci93n", ---*novacia

                        (select (upper(substr(d.n_calle, 1, 1)) ||
                                lower(SUBSTR(d.n_calle, 2)))
                           from dom_manager.vt_domicilios_cond d
                          where d.id_vin =
                                IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(le.id_legajo,
                                                                      'S')) "Calle", ---*novacia

                        (select d.altura
                           from dom_manager.vt_domicilios_cond d
                          where d.id_vin =
                                IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(le.id_legajo,
                                                                      'S')) "Nro", ---*novacia

                        (select d.piso
                           from dom_manager.vt_domicilios_cond d
                          where d.id_vin =
                                IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(le.id_legajo,
                                                                      'S')) "Piso", ---*sivacio

                        (select d.depto
                           from dom_manager.vt_domicilios_cond d
                          where d.id_vin =
                                IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(le.id_legajo,
                                                                      'S')) "Departamento", ---* sivacio

                        (select d.n_localidad
                           from dom_manager.vt_domicilios_cond d
                          where d.id_vin =
                                ipj.entidad_persjur.fc_buscar_ult_dom(le.id_legajo,
                                                                      'N')) "Localidad", ---*novacia

                        (select d.cpa
                           from dom_manager.vt_domicilios_cond d
                          where d.id_vin =
                                IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(le.id_legajo,
                                                                      'S')) "C93digo_Postal", ---*sivacia

                        (select (upper(substr(d.n_provincia, 1, 1)) ||
                                lower(SUBSTR(d.n_provincia, 2)))
                           from dom_manager.vt_domicilios_cond d
                          where d.id_vin =
                                IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(le.id_legajo,
                                                                      'S')) "Provincia", ---*novacio

                        (select (upper(substr(pa.n_pais, 1, 1)) ||
                                lower(SUBSTR(pa.n_pais, 2)))
                           from dom_manager.vt_paises pa
                          where pa.ID_PAIS = 'ARG') "Pa92s", ---*novacia

                        '' as Prefijo, --sivacia
                        '' as Nro_Tel, --sivacia
                        '' as Email --sivacia
          FROM ipj.t_tramitesipj             tr,
               nuevosuac.vt_tramites_id      suactr,
               nuevosuac.vt_subtipos_tramite subtip,
               ipj.t_legajos                 le
          join ipj.t_entidades e
            on le.id_legajo = e.id_legajo

         WHERE nvl(le.eliminado, 0) = 0

           and le.fecha_baja_entidad is null
           AND ipj.entidad_persjur.fc_buscar_ult_dom(le.id_legajo, 'S') in
               (SELECT id_domicilio
                  FROM (select distinct (LE.ID_LEGAJO),
                                        ipj.entidad_persjur.fc_buscar_ult_dom(le.id_legajo,
                                                                              'S') as id_domicilio
                          FROM ipj.t_tramitesipj tr, ipj.t_legajos le
                          join ipj.t_entidades e
                            on le.id_legajo = e.id_legajo
                         where nvl(le.eliminado, 0) = 0
                           and le.fecha_baja_entidad is NULL
                           and tr.id_tramite_ipj = e.id_tramite_ipj
                           and tr.id_estado_ult between 100 and 199 -- Tramites OK
                           \*and nvl(e.fec_inscripcion, e.fec_resolucion) between
                               &V_FECHA_DESDE and &V_FECHA_HASTA*\)
                 group by id_domicilio
                having count(id_domicilio) > 1)
           and tr.id_tramite_ipj = e.id_tramite_ipj
           and suactr.id_tramite = tr.id_tramite
           and subtip.id_subtipo_tramite = suactr.id_subtipo_tramite
           and tr.id_estado_ult between 100 and 199 -- Tramites OK
           \*and nvl(e.fec_inscripcion, e.fec_resolucion) between
               &V_FECHA_DESDE and &V_FECHA_HASTA*\

         order by 1;

    EXCEPTION
      WHEN V_ERROR_00001 THEN
        V_MSG := 'Mal.';
    END BUSCAR_ENTIDADES_MISMO_DOMI;

    PROCEDURE BUSCAR_MISMO_DOMICILIO_PF(p_T_CONSULTA   OUT TY_CURSOR,
                                        P_FECHA_DESDE  IN IPJ.t_Entidades.FEC_INSCRIPCION%TYPE DEFAULT NULL,
                                        P_FECHA_HASTA  IN IPJ.t_Entidades.FEC_INSCRIPCION%TYPE DEFAULT NULL,
                                        P_ID_DOMICILIO IN IPJ.t_Entidades.ID_LEGAJO%TYPE DEFAULT NULL) IS
      V_MSG          VARCHAR2(300);
      V_FECHA_DESDE  DATE;
      V_FECHA_HASTA  DATE;
      V_SENTENCIA    VARCHAR2(50);
      V_ID_DOMICILIO number;

    BEGIN

      V_FECHA_DESDE  := P_FECHA_DESDE;
      V_FECHA_HASTA  := P_FECHA_HASTA;
      V_ID_DOMICILIO := P_ID_DOMICILIO;

      V_SENTENCIA := '  ALTER SESSION SET NLS_DATE_FORMAT=' || '''' ||
                     'DD/MM/YYYY' || '''';

      EXECUTE IMMEDIATE V_SENTENCIA;

      OPEN p_T_CONSULTA FOR

      --PERSONA FISICA

        select distinct ipj.entidad_persjur.fc_buscar_ult_dom(le.id_legajo,
                                                              'S') as id_domicilio,
                        --)Socio_Persona_F92sica

                        (select (upper(substr(vp.APELLIDO, 1, 1)) ||
                                lower(SUBSTR(vp.APELLIDO, 2)))
                           from rcivil.vt_pk_persona vp
                          where vp.id_sexo = i.id_sexo
                            and vp.nro_documento = i.nro_documento
                            and vp.pai_cod_pais = i.pai_cod_pais
                            and vp.id_numero = i.id_numero) "Apellido88PerFisica", ---*--novacia

                        '' as Segundo_Apellido88PerFisica, --sivacia

                        (select initcap(vp.nombre)
                           from rcivil.vt_pk_persona vp
                          where vp.id_sexo = i.id_sexo
                            and vp.nro_documento = i.nro_documento
                            and vp.pai_cod_pais = i.pai_cod_pais
                            and vp.id_numero = i.id_numero) "Nombre88PerFisica", ---*--novacia

                        '' as Segundo_Nombre88PerFisica, --sivacia

                        (select vp.n_tipo_documento
                           from rcivil.vt_pk_persona vp
                          where vp.id_sexo = i.id_sexo
                            and vp.nro_documento = i.nro_documento
                            and vp.pai_cod_pais = i.pai_cod_pais
                            and vp.id_numero = i.id_numero) "Tipo_Documento88PerFisica", ---*--novacia

                        i.nro_documento "N94mero_documento88PerFisica", ---*--novacia

                        (select vp.cuil
                           from rcivil.vt_pk_persona vp
                          where vp.id_sexo = i.id_sexo
                            and vp.nro_documento = i.nro_documento
                            and vp.pai_cod_pais = i.pai_cod_pais
                            and vp.id_numero = i.id_numero) "Nro_de_CUIT_CUIL88PerFisica", ---*--sivacia

                        ad.persona_expuesta "Radicada_en_el_Exterior88PerFisica", ---*--novacia

                        ad.persona_expuesta "Radicada_en_Paraiso_Fiscal88PerFisica", ---*--novacia

                        ad.persona_expuesta "PEP88PerFisica", ---* --novacia

                        (select (upper(substr(d.n_calle, 1, 1)) ||
                                lower(SUBSTR(d.n_calle, 2)))
                           from DOM_MANAGER.VT_DOMICILIOS_COND d
                          where d.id_vin =
                                IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo,
                                                                 i.nro_documento,
                                                                 i.pai_cod_pais,
                                                                 to_char(i.id_numero),
                                                                 3)) "Calle88PerFisica", ---*--novacia
                        (select d.altura
                           from DOM_MANAGER.VT_DOMICILIOS_COND d
                          where d.id_vin =
                                IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo,
                                                                 i.nro_documento,
                                                                 i.pai_cod_pais,
                                                                 to_char(i.id_numero),
                                                                 3)) "Nro88PerFisica", ---*--novacia
                        (select d.piso
                           from DOM_MANAGER.VT_DOMICILIOS_COND d
                          where d.id_vin =
                                IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo,
                                                                 i.nro_documento,
                                                                 i.pai_cod_pais,
                                                                 to_char(i.id_numero),
                                                                 3)) "Piso88PerFisica", ---*si--novacia
                        (select d.depto
                           from DOM_MANAGER.VT_DOMICILIOS_COND d
                          where d.id_vin =
                                IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo,
                                                                 i.nro_documento,
                                                                 i.pai_cod_pais,
                                                                 to_char(i.id_numero),
                                                                 3)) "Departamento88PerFisica", ---*si--novacia
                        (select (upper(substr(d.n_localidad, 1, 1)) ||
                                lower(SUBSTR(d.n_localidad, 2)))
                           from DOM_MANAGER.VT_DOMICILIOS_COND d
                          where d.id_vin =
                                IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo,
                                                                 i.nro_documento,
                                                                 i.pai_cod_pais,
                                                                 to_char(i.id_numero),
                                                                 3)) "Localidad88PerFisica", ---*--novacia
                        (select d.cpa
                           from DOM_MANAGER.VT_DOMICILIOS_COND d
                          where d.id_vin =
                                IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo,
                                                                 i.nro_documento,
                                                                 i.pai_cod_pais,
                                                                 to_char(i.id_numero),
                                                                 3)) "C93digo_Postal88PerFisica", ---*si--novacia
                        (select (upper(substr(d.n_provincia, 1, 1)) ||
                                lower(SUBSTR(d.n_provincia, 2)))
                           from dom_manager.vt_domicilios_cond d
                          where d.id_vin =
                                IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(le.id_legajo,
                                                                      'S')) "Provincia88PerFisica", ---*novacio

                        (select (upper(substr(pa.n_pais, 1, 1)) ||
                                lower(SUBSTR(pa.n_pais, 2)))
                           from dom_manager.vt_paises pa
                          where pa.ID_PAIS = 'ARG') "Pa92s88PerFisica", ---*--novacia

                        IPJ.VARIOS.FC_Buscar_Te_Caract(i.id_sexo ||
                                                       i.nro_documento ||
                                                       i.pai_cod_pais ||
                                                       to_char(i.id_numero),
                                                       126) "Prefijo88PerFisica", ---*si--novacia

                        IPJ.VARIOS.FC_Buscar_Telefono(i.id_sexo ||
                                                      i.nro_documento ||
                                                      i.pai_cod_pais ||
                                                      to_char(i.id_numero),
                                                      126) "Tel91fono88PerFisica", ---*si--novacia

                        IPJ.VARIOS.FC_BUSCAR_MAIL(i.id_sexo ||
                                                  i.nro_documento ||
                                                  i.pai_cod_pais ||
                                                  to_char(i.id_numero),
                                                  126) "Email88PerFisica", ---*si--novacia

                        (select vp.FECHA_NACIMIENTO
                           from rcivil.vt_pk_persona vp
                          where vp.id_sexo = i.id_sexo
                            and vp.nro_documento = i.nro_documento
                            and vp.pai_cod_pais = i.pai_cod_pais
                            and vp.id_numero = i.id_numero) "Fecha_de_Nacimiento88PerFisica", ---*si--novacia

                        CASE UPPER(ti.n_tipo_integrante)

                          WHEN UPPER('Socio/a') THEN
                           'Socio' --

                          WHEN UPPER('Gerente/a') THEN
                           'Gerente' --

                          WHEN UPPER('S�ndico/a Titular') THEN
                           'S�ndico' --

                          WHEN UPPER('Tesorero/a') THEN
                           'Tesorero' --

                          WHEN UPPER('Presidente/a') THEN
                           'Presidente' --

                          WHEN UPPER('Director/a') THEN
                           'Director' --

                          WHEN UPPER('Director/a Titular') THEN
                           'Director' --

                          WHEN UPPER('Vice-Presidente/a') THEN
                           'Vicepresidente' --

                          WHEN UPPER('Vicepresidente/a') THEN
                           'Vicepresidente' --

                          WHEN UPPER('Titular') THEN
                           'Titular' --

                          WHEN UPPER('Representante') THEN
                           'Representante Legal' --

                          ELSE
                           'Otros'
                        END AS "Caracter_Invocado88PerFisica", ---* --novacia

                        '' as Sociedad_Vinculada --sivacia

          from ipj.t_tipos_entidades         te,
               ipj.t_tramitesipj             tr,
               nuevosuac.vt_tramites_id      suactr,
               nuevosuac.vt_subtipos_tramite subtip,
               ipj.t_legajos                 le
          join ipj.t_entidades e
            on le.id_legajo = e.id_legajo
          join ipj.t_entidades_admin ad
            on e.id_tramite_ipj = ad.id_tramite_ipj
           and e.id_legajo = ad.id_legajo
          join ipj.t_integrantes i
            on i.id_integrante = ad.id_integrante
          join ipj.t_tipos_integrante ti
            on ti.id_tipo_integrante = ad.id_tipo_integrante
          join ipj.t_tipos_organismo o
            on o.id_tipo_organismo = ad.id_tipo_organismo

         where nvl(le.eliminado, 0) <> 1

           and le.fecha_baja_entidad is null

           and ipj.entidad_persjur.fc_buscar_ult_dom(le.id_legajo, 'S') in
               (select domicilio
                  from (select le.id_legajo,
                               ipj.entidad_persjur.fc_buscar_ult_dom(le.id_legajo,
                                                                     'S') domicilio
                          from ipj.t_tipos_entidades         te,
                               ipj.t_tramitesipj             tr,
                               nuevosuac.vt_tramites_id      suactr,
                               nuevosuac.vt_subtipos_tramite subtip,
                               ipj.t_legajos                 le
                          join ipj.t_entidades e
                            on le.id_legajo = e.id_legajo
                          join ipj.t_entidades_admin ad
                            on e.id_tramite_ipj = ad.id_tramite_ipj
                           and e.id_legajo = ad.id_legajo
                          join ipj.t_integrantes i
                            on i.id_integrante = ad.id_integrante
                          join ipj.t_tipos_integrante ti
                            on ti.id_tipo_integrante = ad.id_tipo_integrante
                          join ipj.t_tipos_organismo o
                            on o.id_tipo_organismo = ad.id_tipo_organismo

                         where nvl(le.eliminado, 0) <> 1

                           and le.fecha_baja_entidad is null

                           and te.id_tipo_entidad = e.id_tipo_entidad
                           and tr.id_tramite_ipj = e.id_tramite_ipj
                           and suactr.id_tramite = tr.id_tramite
                           and subtip.id_subtipo_tramite =
                               suactr.id_subtipo_tramite
                           and tr.id_estado_ult between 100 and 199 -- Tramites OK
                              --and nvl(e.fec_inscripcion, e.fec_resolucion) <= '01-oct-2021'
                              --and nvl(e.fec_inscripcion, e.fec_resolucion) between '19/08/2010'and '19/08/2018'
                           \*and nvl(e.fec_inscripcion, e.fec_resolucion) between
                               V_FECHA_DESDE and V_FECHA_HASTA*\) t
                 where nvl(domicilio, 0) > 0

                 group by domicilio
                having count(DISTINCT id_legajo) > 1)

           and te.id_tipo_entidad = e.id_tipo_entidad
           and tr.id_tramite_ipj = e.id_tramite_ipj
           and suactr.id_tramite = tr.id_tramite
           and subtip.id_subtipo_tramite = suactr.id_subtipo_tramite
           and tr.id_estado_ult between 100 and 199 -- Tramites OK
              --and nvl(e.fec_inscripcion, e.fec_resolucion) <= '01-oct-2021'
           \*and nvl(e.fec_inscripcion, e.fec_resolucion) between V_FECHA_DESDE and
               V_FECHA_HASTA*\
           and IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(le.id_legajo, 'S') IN
               (V_ID_DOMICILIO)

         ORDER BY 1;

    EXCEPTION
      WHEN V_ERROR_00001 THEN
        V_MSG := 'Mal.';
    END BUSCAR_MISMO_DOMICILIO_PF;

    PROCEDURE BUSCAR_MISMO_DOMICILIO_PJ(p_T_CONSULTA   OUT TY_CURSOR,
                                        P_FECHA_DESDE  IN IPJ.t_Entidades.FEC_INSCRIPCION%TYPE DEFAULT NULL,
                                        P_FECHA_HASTA  IN IPJ.t_Entidades.FEC_INSCRIPCION%TYPE DEFAULT NULL,
                                        P_ID_DOMICILIO IN IPJ.t_Entidades.ID_LEGAJO%TYPE DEFAULT NULL) IS
      V_MSG          VARCHAR2(300);
      V_FECHA_DESDE  DATE;
      V_FECHA_HASTA  DATE;
      V_SENTENCIA    VARCHAR2(50);
      V_ID_DOMICILIO number;

    BEGIN

      V_FECHA_DESDE  := P_FECHA_DESDE;
      V_FECHA_HASTA  := P_FECHA_HASTA;
      V_ID_DOMICILIO := P_ID_DOMICILIO;

      V_SENTENCIA := '  ALTER SESSION SET NLS_DATE_FORMAT=' || '''' ||
                     'DD/MM/YYYY' || '''';

      EXECUTE IMMEDIATE V_SENTENCIA;

      OPEN p_T_CONSULTA FOR
      ---Persona Jur�dica
        select distinct ipj.entidad_persjur.fc_buscar_ult_dom(le.id_legajo,
                                                              'S') as id_domicilio,

                        (select PJU.RAZON_SOCIAL
                           from t_comunes.vt_pers_juridicas_unica PJU
                          where PJU.CUIT = e.cuit) "Denominaci93n88PerJuridica", ---*--novacia

                        (select FJ.N_FORMA_JURIDICA
                           from t_comunes.vt_formas_juridica      FJ,
                                t_comunes.vt_pers_juridicas_unica PJU
                          where PJU.CUIT = e.cuit
                            and FJ.ID_FORMA_JURIDICA = PJu.ID_FORMA_JURIDICA) "Forma_Jur92dica88PerJuridica", ---*--novacia

                        (select PJ.CUIT
                           from t_comunes.vt_pers_juridicas_unica pj
                          where pj.CUIT = E.CUIT) "Nro_CUIT88PerJuridica", ---*--novacia

                        (select (upper(substr(d.n_calle, 1, 1)) ||
                                lower(SUBSTR(d.n_calle, 2)))
                           from dom_manager.vt_domicilios_cond d
                          where d.id_vin =
                                IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(le.id_legajo,
                                                                      'S')) as "Calle88PerJuridica", --novacia
                        (select decode(d.altura, null, 0, d.altura) --agregado 11/08/2021
                           from dom_manager.vt_domicilios_cond d
                          where d.id_vin =
                                IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(le.id_legajo,
                                                                      'S')) as "Nro88PerJuridica", --novacia
                        (select d.piso
                           from dom_manager.vt_domicilios_cond d
                          where d.id_vin =
                                IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(le.id_legajo,
                                                                      'S')) as "Piso88PerJuridica", --si--novacia
                        (select d.depto
                           from dom_manager.vt_domicilios_cond d
                          where d.id_vin =
                                IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(le.id_legajo,
                                                                      'S')) as "Departamento88PerJuridica", --si--novacia
                        (select (upper(substr(d.n_localidad, 1, 1)) ||
                                lower(SUBSTR(d.n_localidad, 2)))
                           from dom_manager.vt_domicilios_cond d
                          where d.id_vin =
                                IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(le.id_legajo,
                                                                      'S')) as "Localidad88PerJuridica", ----novacia
                        (select d.cpa
                           from dom_manager.vt_domicilios_cond d
                          where d.id_vin =
                                IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(le.id_legajo,
                                                                      'S')) as "C93digo_Postal88PerJuridica", --si--novacia
                        (select (upper(substr(d.n_provincia, 1, 1)) ||
                                lower(SUBSTR(d.n_provincia, 2)))
                           from dom_manager.vt_domicilios_cond d
                          where d.id_vin =
                                IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(le.id_legajo,
                                                                      'S')) as "Provincia88PerJuridica", --novacio
                        (select pa.n_pais
                           from dom_manager.vt_domicilios_cond d
                           join DOM_MANAGER.VT_PROVINCIAS pr
                             on pr.id_provincia = d.id_provincia
                           join DOM_MANAGER.VT_PAISES pa
                             on pr.id_pais = pa.id_pais
                          where d.id_vin =
                                IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(le.id_legajo,
                                                                      'S')) as "Pa92s88PerJuridica", --novacia
                        IPJ.VARIOS.FC_Buscar_Mail(le.cuit || '00', 126) as "Email88PerJuridica", --si--novacia
                        IPJ.VARIOS.FC_Buscar_Te_Caract(le.cuit || '00', 126) as "Prefijo88PerJuridica", --si--novacia
                        IPJ.VARIOS.FC_Buscar_Telefono(le.cuit || '00', 126) as "Tel91fono88PerJuridica", --si--novacia
                        -- '' as "Actividad_que_Desarrolla88PerJuridica",

                        (select max(apju.n_actividad_uif)
                           from t_actividad_persona_juri_uif    apju, \* t_comunes.t_actividades a,*\
                                t_comunes.vt_actividades_perjur ap
                          where apju.id_actividad_uif = ap.id_actividad
                               \*a.id_actividad=ap.id_actividad and*\
                            and ap.cuit = e.cuit) "Actividad_que_Desarrolla88PerJuridica", --novacia

                        '' as Sociedad_Vinculada --si vacia

          from ipj.t_tipos_entidades         te,
               ipj.t_tramitesipj             tr,
               nuevosuac.vt_tramites_id      suactr,
               nuevosuac.vt_subtipos_tramite subtip,
               ipj.t_legajos                 le
          join ipj.t_entidades e
            on le.id_legajo = e.id_legajo
          join ipj.t_entidades_admin ad
            on e.id_tramite_ipj = ad.id_tramite_ipj
           and e.id_legajo = ad.id_legajo
          join ipj.t_integrantes i
            on i.id_integrante = ad.id_integrante
          join ipj.t_tipos_integrante ti
            on ti.id_tipo_integrante = ad.id_tipo_integrante
          join ipj.t_tipos_organismo o
            on o.id_tipo_organismo = ad.id_tipo_organismo

         where nvl(le.eliminado, 0) <> 1

           and le.fecha_baja_entidad is null

           and ipj.entidad_persjur.fc_buscar_ult_dom(le.id_legajo, 'S') in
               (select domicilio
                  from (select le.id_legajo,
                               ipj.entidad_persjur.fc_buscar_ult_dom(le.id_legajo,
                                                                     'S') domicilio
                          from ipj.t_tipos_entidades         te,
                               ipj.t_tramitesipj             tr,
                               nuevosuac.vt_tramites_id      suactr,
                               nuevosuac.vt_subtipos_tramite subtip,
                               ipj.t_legajos                 le
                          join ipj.t_entidades e
                            on le.id_legajo = e.id_legajo
                          join ipj.t_entidades_admin ad
                            on e.id_tramite_ipj = ad.id_tramite_ipj
                           and e.id_legajo = ad.id_legajo
                          join ipj.t_integrantes i
                            on i.id_integrante = ad.id_integrante
                          join ipj.t_tipos_integrante ti
                            on ti.id_tipo_integrante = ad.id_tipo_integrante
                          join ipj.t_tipos_organismo o
                            on o.id_tipo_organismo = ad.id_tipo_organismo

                         where nvl(le.eliminado, 0) <> 1

                           and le.fecha_baja_entidad is null

                           and te.id_tipo_entidad = e.id_tipo_entidad
                           and tr.id_tramite_ipj = e.id_tramite_ipj
                           and suactr.id_tramite = tr.id_tramite
                           and subtip.id_subtipo_tramite =
                               suactr.id_subtipo_tramite
                           and tr.id_estado_ult between 100 and 199 -- Tramites OK
                              --and nvl(e.fec_inscripcion, e.fec_resolucion) <=        '01-oct-2021'
                           and nvl(e.fec_inscripcion, e.fec_resolucion) between
                               '19/08/2010' and '19/08/2018') t
                 where nvl(domicilio, 0) > 0

                 group by domicilio
                having count(DISTINCT id_legajo) > 1)

           and te.id_tipo_entidad = e.id_tipo_entidad
           and tr.id_tramite_ipj = e.id_tramite_ipj
           and suactr.id_tramite = tr.id_tramite
           and subtip.id_subtipo_tramite = suactr.id_subtipo_tramite
           and tr.id_estado_ult between 100 and 199 -- Tramites OK
              --   and nvl(e.fec_inscripcion, e.fec_resolucion) <= '01-oct-2021'
           \*and nvl(e.fec_inscripcion, e.fec_resolucion) between V_FECHA_DESDE and
               V_FECHA_HASTA*\
           and IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(le.id_legajo, 'S') IN
               (V_ID_DOMICILIO)

         ORDER BY 1;

    EXCEPTION
      WHEN V_ERROR_00001 THEN
        V_MSG := 'Mal.';
    END BUSCAR_MISMO_DOMICILIO_PJ;
  */

  PROCEDURE LLENAR_TABLA_DOMICILIO IS

    V_SENTENCIA            VARCHAR2(50);
    v_Id_Tramite_Ipj       number;
    v_id_tramiteipj_accion number;
    v_id_legajo            number;
    v_Abiertos             char;

    CURSOR cur_T_ENTIDADES IS
      SELECT DISTINCT UI.ID_LEGAJO ID_LEGAJO
        FROM IPJ.T_UIF_LEGAJO_DOMICILIO UI;

  BEGIN

    V_SENTENCIA := '  ALTER SESSION SET NLS_DATE_FORMAT=' || '''' ||
                   'DD/MM/YYYY' || '''';

    EXECUTE IMMEDIATE V_SENTENCIA;

    delete ipj.t_uif_legajo_domicilio;
    commit;
    insert into ipj.t_uif_legajo_domicilio ui
      (select le.id_legajo,
              ipj.entidad_persjur.fc_buscar_ult_dom(le.id_legajo, 'S'),
              user,
              sysdate,
              null

         FROM ipj.t_legajos le);
    commit;

    FOR ENT_T_ENTIDADES IN cur_T_ENTIDADES LOOP

      IPJ.ENTIDAD_PERSJUR.SP_Buscar_Entidad_Tramite(V_Id_Tramite_Ipj,
                                                    V_id_tramiteipj_accion,
                                                    ENT_T_ENTIDADES.ID_LEGAJO,
                                                    'S');

      UPDATE IPJ.T_UIF_LEGAJO_DOMICILIO UIF
         SET UIF.id_tramite_ipj = v_id_tramite_ipj
       WHERE UIF.ID_LEGAJO = ENT_T_ENTIDADES.ID_LEGAJO;

    END LOOP;
    commit;
  END LLENAR_TABLA_DOMICILIO;

END PKG_UIF;
/

