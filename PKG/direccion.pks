create or replace package ipj.Direccion
as
 
  PROCEDURE SP_Acciones_x_Estados (
    o_Cursor out IPJ.TYPES.CURSORTYPE ,
    p_fecha_inicio in varchar2,
    p_fecha_fin in varchar2,
    p_incl_historicos in number, -- 0 = No incluye, 1 = Incluye
    p_id_ubicacion in number,
    p_id_clasif_ipj in number,
    p_id_tipo_accion in number
  ) ;  
  
  PROCEDURE SP_Tramites_x_Estados (
    o_Cursor out IPJ.TYPES.CURSORTYPE ,
    p_fecha_inicio in varchar2,
    p_fecha_fin in varchar2,
    p_incl_historicos in number, -- 0 = No incluye, 1 = Incluye
    p_id_ubicacion in number,
    p_id_clasif_ipj in number
  );
  
  PROCEDURE SP_Acciones_IPJ (
    o_Cursor out IPJ.TYPES.CURSORTYPE ,
    p_fecha_inicio in varchar2,
    p_fecha_fin in varchar2,
    p_incl_historicos in number, -- 0 = No incluye, 1 = Incluye
    p_id_ubicacion in number,
    p_id_clasif_ipj in number,
    p_id_tipo_accion in number,
    p_id_grupo_accion in number
  );
  
  PROCEDURE SP_Acciones_Desvios (
    o_Cursor out IPJ.TYPES.CURSORTYPE ,
    p_fecha_inicio in varchar2,
    p_fecha_fin in varchar2,
    p_incl_historicos in number, -- 0 = No incluye, 1 = Incluye
    p_id_ubicacion in number,
    p_id_clasif_ipj in number,
    p_id_tipo_accion in number
  );
  
  PROCEDURE SP_Acciones_Desvios_Det (
    o_Cursor out IPJ.TYPES.CURSORTYPE ,
    p_fecha_inicio in varchar2,
    p_fecha_fin in varchar2,
    p_incl_historicos in number, -- 0 = No incluye, 1 = Incluye
    p_id_ubicacion in number,
    p_id_clasif_ipj in number,
    p_id_tipo_accion in number,
    p_Solo_Desvios in number -- 0 = No incluye, 1 = Incluye
  );
  
  PROCEDURE SP_Buscar_Tramites (
    o_Cursor out IPJ.TYPES.CURSORTYPE ,
    p_fecha_inicio in varchar2,
    p_fecha_fin in varchar2,
    p_tipo_operacion in number, --1- Tr�mite, 2-Acci�n - 3- Ambos
    p_incl_historicos in number, -- 0 = No incluye, 1 = Incluye
    p_id_ubicacion in number,
    p_id_clasif_ipj in number,
    p_id_tipo_accion in number,
    p_sticker in varchar2,
    p_expediente in varchar2,
    p_id_estado in number,
    -- Personas Juridicas
    p_id_legajo in number,
    p_id_tipo_entidad in number,
    p_cuit in varchar2,
    p_razon_social in varchar2,
    p_matricula_ent in number,
    p_matricula_letra_ent in varchar2,
    p_nro_registro in varchar2,
    p_id_estado_entidad in varchar2,
    p_id_tipo_origen in number,
    p_id_rubro in varchar2, -- Rubro Afip
    p_id_tipo_actividad in varchar2, -- Tipo Act. Afip
    p_id_actividad in varchar2, --Actividad Afip
    p_capital in varchar2,
    p_fec_constitucion in varchar2,
    p_obj_social in varchar2,
    -- Personas F�sicas
    p_id_sexo in varchar2,
    p_nro_documento in varchar2,
    p_pai_cod_pais in varchar2,
    p_id_numero in number,
    p_id_integrante in number,
    p_matricula_pers in number,
    p_matricula_letra_pers in varchar2,
    p_buscar_persona in number, -- 1= Comerciante /  2= Comp FC / 4 = Vend FC / 8 = Socios / 16 = Admin / 32 = Repres.
    -- Domicilios 
    p_Id_Departamento In Number,
    p_Id_Localidad In Number,
    p_buscar_domicilio in number, -- 1= Empresa /  2= Asamblea / 4 = Admin / 8 = Comerc / 16 = Fondo Com / 32 = Comrpador FC  / 64 = Vendedor FC
    -- Actuaciones
    p_nro_actuacion in varchar2,
    p_texto_informe in varchar2
  );
  
  PROCEDURE SP_Informar_Tasas (
    o_Cursor out IPJ.TYPES.CURSORTYPE ,
    p_fecha_inicio in varchar2,
    p_fecha_fin in varchar2
  );
  
  PROCEDURE SP_Buscar_Personas (
    o_Cursor out IPJ.TYPES.CURSORTYPE ,
    p_nro_documento in varchar2,
    p_nombre in varchar2,
    p_apellido in varchar2
  );
  
end Direccion;
/

create or replace package body ipj.Direccion is

PROCEDURE SP_Acciones_x_Estados (
    o_Cursor out IPJ.TYPES.CURSORTYPE ,
    p_fecha_inicio in varchar2,
    p_fecha_fin in varchar2,
    p_incl_historicos in number, -- 1 = Carga Normal, 2 = Carga Historica, 3 = Ambas
    p_id_ubicacion in number,
    p_id_clasif_ipj in number,
    p_id_tipo_accion in number
  )
  IS
    v_carga_normal number(1) := 1;
    v_carga_historica number(1) := 2;
  BEGIN

    OPEN o_Cursor FOR
      select
        u.id_ubicacion, u.n_ubicacion,
        sum((case when AccIPJ.id_estado in (Ipj.Types.c_Estados_Archivo_Inicial, Ipj.Types.c_Estados_Asignado, Ipj.Types.c_Estados_Proceso) then 1 else 0 end)) Abiertas,
        sum((case when AccIPJ.id_estado in (Ipj.Types.c_Estados_Completado) then 1 else 0 end)) Completos,
        sum((case when AccIPJ.id_estado = Ipj.Types.c_Estados_Observado then 1 else 0 end)) Observado,
        sum((case when AccIPJ.id_estado in (Ipj.Types.c_Estados_Cerrado) then 1 else 0 end)) Cerradas,
        sum((case when AccIPJ.id_estado = Ipj.Types.C_Estados_Rechazado then 1 else 0 end)) Rechazado,
        sum((case when AccIPJ.id_estado = Ipj.Types.C_Estados_Perimido then 1 else 0 end)) Perimido, -- VER ESTE CAMBIO EN GESTION
        count(*) total
      from ipj.t_tramitesipj_acciones AccIPJ join ipj.t_tramitesIPJ tIPJ
          on AccIPJ.Id_Tramite_IPJ = tIPJ.id_tramite_ipj
        join ipj.t_tipos_accionesipj tacc
          on TACC.ID_TIPO_ACCION = ACCIPJ.ID_TIPO_ACCION
        join ipj.t_tipos_clasif_ipj tClas
          on TCLAS.ID_CLASIF_IPJ = TACC.ID_CLASIF_IPJ
        join ipj.t_ubicaciones u
          on u.id_ubicacion = tClas.id_ubicacion
      where
        TIPJ.FECHA_INICIO between to_date(p_fecha_inicio, 'dd/mm/rrrr') and to_date(p_fecha_fin, 'dd/mm/rrrr') and
        ( (bitand(p_incl_historicos, v_carga_normal) = v_carga_normal and nvl(TIPJ.ID_PROCESO, 0) not in (IPJ.TYPES.C_PROCESO_GEN_HIST, IPJ.TYPES.C_PROCESO_HIST)) or
          (bitand(p_incl_historicos, v_carga_historica) = v_carga_historica and nvl(TIPJ.ID_PROCESO, 0) in (IPJ.TYPES.C_PROCESO_GEN_HIST, IPJ.TYPES.C_PROCESO_HIST))
        ) and
        --(p_incl_historicos = 1 or nvl(TIPJ.ID_PROCESO, 0) not in (IPJ.TYPES.C_PROCESO_GEN_HIST, IPJ.TYPES.C_PROCESO_HIST)) and
        (p_id_ubicacion = 0 or tIPJ.id_ubicacion_origen = p_id_ubicacion) and
        (p_id_clasif_ipj = 0 or exists ( select *
                                                     from ipj.t_tramitesipj_acciones ac join ipj.t_tipos_accionesipj ta
                                                       on ac.id_tipo_accion = ta.id_tipo_accion
                                                     where
                                                       ac.id_tramite_ipj = tIPJ.id_tramite_ipj and
                                                       ta.id_clasif_ipj = p_id_clasif_ipj
                                                   ) ) and
        (p_id_tipo_accion = 0 or tAcc.id_tipo_accion = p_id_tipo_accion)
      group by u.id_ubicacion, u.n_ubicacion
      order by u.id_ubicacion, u.n_ubicacion;

  END SP_Acciones_x_Estados;

  PROCEDURE SP_Tramites_x_Estados (
    o_Cursor out IPJ.TYPES.CURSORTYPE ,
    p_fecha_inicio in varchar2,
    p_fecha_fin in varchar2,
    p_incl_historicos in number, -- 1 = Carga Normal, 2 = Carga Historica, 3 = Ambas
    p_id_ubicacion in number,
    p_id_clasif_ipj in number
  )
  IS
    v_carga_normal number(1) := 1;
    v_carga_historica number(1) := 2;
  BEGIN

    OPEN o_Cursor FOR
      select
        u.id_ubicacion, u.n_ubicacion, tClas.id_clasif_ipj, TClas.N_Clasif_Ipj,
        sum((case when tIPJ.id_estado_ult in (Ipj.Types.c_Estados_Archivo_Inicial, Ipj.Types.c_Estados_Asignado, Ipj.Types.c_Estados_Proceso, Ipj.Types.c_Estados_Observado) then 1 else 0 end)) Abiertas,
        sum((case when tIPJ.id_estado_ult in (Ipj.Types.C_Estados_Completado) then 1 else 0 end)) Completos,
        sum((case when tIPJ.id_estado_ult in (Ipj.Types.C_Estados_Cerrado) then 1 else 0 end)) Cerradas,
        sum((case when tIPJ.id_estado_ult = Ipj.Types.C_Estados_Rechazado then 1 else 0 end)) Rechazado,
        sum((case when tIPJ.id_estado_ult = Ipj.Types.C_Estados_Perimido then 1 else 0 end)) Perimido, -- VER ESTE DATO EN GESTION
        count(*) total
      from ipj.t_tramitesIPJ tIPJ join ipj.t_tipos_clasif_ipj tClas
          on TCLAS.ID_CLASIF_IPJ = TIPJ.ID_CLASIF_IPJ
        join ipj.t_ubicaciones u
          on u.id_ubicacion = tClas.id_ubicacion
      where
        TIPJ.FECHA_INICIO between to_date(p_fecha_inicio, 'dd/mm/rrrr') and to_date(p_fecha_fin, 'dd/mm/rrrr') and
        ( (bitand(p_incl_historicos, v_carga_normal) = v_carga_normal and nvl(TIPJ.ID_PROCESO, 0) not in (IPJ.TYPES.C_PROCESO_GEN_HIST, IPJ.TYPES.C_PROCESO_HIST)) or
          (bitand(p_incl_historicos, v_carga_historica) = v_carga_historica and nvl(TIPJ.ID_PROCESO, 0) in (IPJ.TYPES.C_PROCESO_GEN_HIST, IPJ.TYPES.C_PROCESO_HIST))
        ) and
        --(p_incl_historicos = 1 or nvl(TIPJ.ID_PROCESO, 0) not in (IPJ.TYPES.C_PROCESO_GEN_HIST, IPJ.TYPES.C_PROCESO_HIST)) and
        (p_id_ubicacion = 0 or tIPJ.id_ubicacion_origen = p_id_ubicacion) and
        (p_id_clasif_ipj = 0 or exists ( select *
                                                     from ipj.t_tramitesipj_acciones ac join ipj.t_tipos_accionesipj ta
                                                       on ac.id_tipo_accion = ta.id_tipo_accion
                                                     where
                                                       ac.id_tramite_ipj = tIPJ.id_tramite_ipj and
                                                       ta.id_clasif_ipj = p_id_clasif_ipj
                                                   ) )
      group by u.id_ubicacion, u.n_ubicacion, tClas.id_clasif_ipj, TClas.N_Clasif_Ipj
      order by u.n_ubicacion, TClas.N_Clasif_Ipj;

  END SP_Tramites_x_Estados;

  PROCEDURE SP_Acciones_IPJ (
    o_Cursor out IPJ.TYPES.CURSORTYPE ,
    p_fecha_inicio in varchar2,
    p_fecha_fin in varchar2,
    p_incl_historicos in number, -- 1 = Carga Normal, 2 = Carga Historica, 3 = Ambas
    p_id_ubicacion in number,
    p_id_clasif_ipj in number,
    p_id_tipo_accion in number,
    p_id_grupo_accion in number
  )
  IS
    v_carga_normal number(1) := 1;
    v_carga_historica number(1) := 2;
BEGIN

    OPEN o_Cursor FOR
      select
        u.id_ubicacion, u.n_ubicacion, tClas.id_clasif_ipj, tClas.N_clasif_ipj,
        tacc.id_tipo_accion, tacc.n_tipo_Accion, count(AccIPJ.id_tipo_accion) cantidad
      from ipj.t_tramitesipj_acciones AccIPJ join ipj.t_tramitesIPJ tIPJ
          on AccIPJ.Id_Tramite_IPJ = tIPJ.id_tramite_ipj
        join ipj.t_tipos_accionesipj tacc
          on TACC.ID_TIPO_ACCION = ACCIPJ.ID_TIPO_ACCION
        join ipj.t_tipos_clasif_ipj tClas
          on TCLAS.ID_CLASIF_IPJ = TACC.ID_CLASIF_IPJ
        join ipj.t_ubicaciones u
          on u.id_ubicacion = tClas.id_ubicacion
      where
        TIPJ.FECHA_INICIO between to_date(p_fecha_inicio, 'dd/mm/rrrr') and to_date(p_fecha_fin, 'dd/mm/rrrr') and
        ( (bitand(p_incl_historicos, v_carga_normal) = v_carga_normal and nvl(TIPJ.ID_PROCESO, 0) not in (IPJ.TYPES.C_PROCESO_GEN_HIST, IPJ.TYPES.C_PROCESO_HIST)) or
          (bitand(p_incl_historicos, v_carga_historica) = v_carga_historica and nvl(TIPJ.ID_PROCESO, 0) in (IPJ.TYPES.C_PROCESO_GEN_HIST, IPJ.TYPES.C_PROCESO_HIST))
        ) and
        --(p_incl_historicos = 1 or nvl(TIPJ.ID_PROCESO, 0) not in (IPJ.TYPES.C_PROCESO_GEN_HIST, IPJ.TYPES.C_PROCESO_HIST)) and
        (p_id_ubicacion = 0 or u.id_ubicacion = p_id_ubicacion) and
        (p_id_clasif_ipj = 0 or tClas.id_clasif_ipj = p_id_clasif_ipj) and
        (p_id_tipo_accion = 0 or tAcc.id_tipo_accion = p_id_tipo_accion) and
        (p_id_grupo_accion = 0 or tacc.id_tipo_accion in
            (select id_tipo_Accion from IPJ.t_Grupos_Acciones where id_grupo_accion = p_id_grupo_accion)
        )
      group by u.id_ubicacion, u.n_ubicacion, tClas.id_clasif_ipj, tClas.N_clasif_ipj, tacc.id_tipo_accion, tacc.n_tipo_Accion
      order by u.n_ubicacion, tClas.N_clasif_ipj, tacc.n_tipo_Accion asc;

  END SP_Acciones_IPJ;


  PROCEDURE SP_Acciones_Desvios (
    o_Cursor out IPJ.TYPES.CURSORTYPE ,
    p_fecha_inicio in varchar2,
    p_fecha_fin in varchar2,
    p_incl_historicos in number, -- 0 = No incluye, 1 = Incluye
    p_id_ubicacion in number,
    p_id_clasif_ipj in number,
    p_id_tipo_accion in number
  )
  IS
  BEGIN

    OPEN o_Cursor FOR
      select
        id_ubicacion, n_ubicacion, id_tipo_accion, n_tipo_accion,
        trim(To_Char(decode(sum((case when upper(urgente) = 'N' then 1 else 0 end)), 0, 0, sum((case when upper(urgente) = 'N' then desvio else 0 end)) / sum((case when upper(urgente) = 'N' then 1 else 0 end))) , '9999999999999990.99')) desvio_normal,
        trim(To_Char(decode(sum((case when upper(urgente) = 'S' then 1 else 0 end)), 0, 0, sum((case when upper(urgente) = 'S' then desvio else 0 end))  / sum((case when upper(urgente) = 'S' then 1 else 0 end))) , '9999999999999990.99')) desvio_urgente,
        trim(To_Char(decode(count(*), 0, 0, sum(desvio) / count(*)) , '9999999999999990.99')) desvio_total,
        max(tiempo_demora) demora_normal,
        max(tiempo_demora) demora_urg
      from
          (Select
              u.id_ubicacion, u.n_ubicacion, tacc.id_tipo_accion, tacc.n_tipo_accion, TIPJ.URGENTE,
              TACC.TIEMPO_DEMORA, TACC.TIEMPO_DEMORA_URG,
              to_number(IPJ.TRAMITES.FC_Ultima_Fecha_Accion ( AccIPJ.id_tramite_ipj, AccIPJ.id_tramiteipj_accion) -
                 IPJ.TRAMITES.FC_Primera_Fecha_Accion ( AccIPJ.id_tramite_ipj, AccIPJ.id_tramiteipj_accion)) desvio
            from ipj.t_tramitesipj_acciones AccIPJ join ipj.t_tramitesIPJ tIPJ
                on AccIPJ.Id_Tramite_IPJ = tIPJ.id_tramite_ipj
              join ipj.t_tipos_accionesipj tacc
                on TACC.ID_TIPO_ACCION = ACCIPJ.ID_TIPO_ACCION
              join ipj.t_tipos_clasif_ipj tClas
                on TCLAS.ID_CLASIF_IPJ = TACC.ID_CLASIF_IPJ
              join ipj.t_ubicaciones u
                on u.id_ubicacion = tClas.id_ubicacion
            where
              TIPJ.FECHA_INICIO between to_date(p_fecha_inicio, 'dd/mm/rrrr') and to_date(p_fecha_fin, 'dd/mm/rrrr') and
              (p_incl_historicos = 1 or nvl(TIPJ.ID_PROCESO, 0) not in (IPJ.TYPES.C_PROCESO_GEN_HIST, IPJ.TYPES.C_PROCESO_HIST)) and
              (p_id_ubicacion = 0 or u.id_ubicacion = p_id_ubicacion) and
              (p_id_clasif_ipj = 0 or tClas.id_clasif_ipj = p_id_clasif_ipj) and
              (p_id_tipo_accion = 0 or tAcc.id_tipo_accion = p_id_tipo_accion)
          ) tmp
      group by id_ubicacion, n_ubicacion, id_tipo_accion, n_tipo_accion
      order by id_ubicacion, id_tipo_accion;

  END SP_Acciones_Desvios;

  PROCEDURE SP_Acciones_Desvios_Det (
    o_Cursor out IPJ.TYPES.CURSORTYPE ,
    p_fecha_inicio in varchar2,
    p_fecha_fin in varchar2,
    p_incl_historicos in number, -- 0 = No incluye, 1 = Incluye
    p_id_ubicacion in number,
    p_id_clasif_ipj in number,
    p_id_tipo_accion in number,
    p_Solo_Desvios in number -- 0 = No incluye, 1 = Incluye
  )
  IS
  BEGIN

    OPEN o_Cursor FOR
      select
        id_ubicacion, n_ubicacion, id_tipo_accion, n_tipo_accion, URGENTE,
        Nro_Expediente, Sticker, Estimado, desvio, fecha_fin, fecha_inicio
      from
        ( select
            u.id_ubicacion, u.n_ubicacion, tacc.id_tipo_accion, tacc.n_tipo_accion, TIPJ.URGENTE,
            VT_SUAC.NRO_TRAMITE Nro_Expediente, VT_SUAC.nro_sticker Sticker,
            (case when TIPJ.URGENTE = 'N' then tacc.TIEMPO_DEMORA else TACC.TIEMPO_DEMORA_URG end) Estimado,
            trim(To_Char(to_number(IPJ.TRAMITES.FC_Ultima_Fecha_Accion ( AccIPJ.id_tramite_ipj, AccIPJ.id_tramiteipj_accion) -
               IPJ.TRAMITES.FC_Primera_Fecha_Accion ( AccIPJ.id_tramite_ipj, AccIPJ.id_tramiteipj_accion)) , '9999999999999990.99')) desvio,
            to_char(IPJ.TRAMITES.FC_Ultima_Fecha_Accion ( AccIPJ.id_tramite_ipj, AccIPJ.id_tramiteipj_accion), 'dd/mm/rrrr') fecha_fin,
            to_char(IPJ.TRAMITES.FC_Primera_Fecha_Accion ( AccIPJ.id_tramite_ipj, AccIPJ.id_tramiteipj_accion), 'dd/mm/rrrr') fecha_inicio
          from ipj.t_tramitesipj_acciones AccIPJ join ipj.t_tramitesIPJ tIPJ
              on AccIPJ.Id_Tramite_IPJ = tIPJ.id_tramite_ipj
            left join NUEVOSUAC.VT_TRAMITES_IPJ_DESA vt_suac
               on VT_SUAC.ID_TRAMITE = tIPJ.id_tramite
            join ipj.t_tipos_accionesipj tacc
              on TACC.ID_TIPO_ACCION = ACCIPJ.ID_TIPO_ACCION
            join ipj.t_tipos_clasif_ipj tClas
              on TCLAS.ID_CLASIF_IPJ = TACC.ID_CLASIF_IPJ
            join ipj.t_ubicaciones u
              on u.id_ubicacion = tClas.id_ubicacion
          where
            TIPJ.FECHA_INICIO between to_date(p_fecha_inicio, 'dd/mm/rrrr') and to_date(p_fecha_fin, 'dd/mm/rrrr') and
            (p_incl_historicos = 1 or nvl(TIPJ.ID_PROCESO, 0) not in (IPJ.TYPES.C_PROCESO_GEN_HIST, IPJ.TYPES.C_PROCESO_HIST)) and
            (p_id_ubicacion = 0 or u.id_ubicacion = p_id_ubicacion) and
            (p_id_clasif_ipj = 0 or tClas.id_clasif_ipj = p_id_clasif_ipj) and
            (p_id_tipo_accion = 0 or tAcc.id_tipo_accion = p_id_tipo_accion)
        ) tmp
      where
        (p_Solo_Desvios = 0 or desvio > estimado)
      order by id_ubicacion, id_tipo_accion;

  END SP_Acciones_Desvios_Det;

  PROCEDURE SP_Buscar_Tramites (
    o_Cursor out IPJ.TYPES.CURSORTYPE ,
    p_fecha_inicio in varchar2,
    p_fecha_fin in varchar2,
    p_tipo_operacion in number, --1- Tr�mite, 2-Acci�n - 3- Ambos
    p_incl_historicos in number, -- 0 = No incluye, 1 = Incluye
    p_id_ubicacion in number,
    p_id_clasif_ipj in number,
    p_id_tipo_accion in number,
    p_sticker in varchar2,
    p_expediente in varchar2,
    p_id_estado in number,
    -- Personas Juridicas
    p_id_legajo in number,
    p_id_tipo_entidad in number,
    p_cuit in varchar2,
    p_razon_social in varchar2,
    p_matricula_ent in number,
    p_matricula_letra_ent in varchar2,
    p_nro_registro in varchar2,
    p_id_estado_entidad in varchar2,
    p_id_tipo_origen in number,
    p_id_rubro in varchar2, -- Rubro Afip
    p_id_tipo_actividad in varchar2, -- Tipo Act. Afip
    p_id_actividad in varchar2, --Actividad Afip
    p_capital in varchar2,
    p_fec_constitucion in varchar2,
    p_obj_social in varchar2,
    -- Personas F�sicas
    p_id_sexo in varchar2,
    p_nro_documento in varchar2,
    p_pai_cod_pais in varchar2,
    p_id_numero in number,
    p_id_integrante in number,
    p_matricula_pers in number,
    p_matricula_letra_pers in varchar2,
    p_buscar_persona in number, -- 1= Comerciante /  2= Comp FC / 4 = Vend FC / 8 = Socios / 16 = Admin / 32 = Repres.
    -- Domicilios
    p_Id_Departamento In Number,
    p_Id_Localidad In Number,
    p_buscar_domicilio in number, -- 1= Empresa /  2= Asamblea / 4 = Admin / 8 = Comerc / 16 = Fondo Com / 32 = Comrpador FC  / 64 = Vendedor FC
    -- Actuaciones
    p_nro_actuacion in varchar2,
    p_texto_informe in varchar2
  )
  IS
    v_Sentencia_Tramites varchar2(4000);
    v_Sentencia_Acciones varchar2(4000);
    v_Sentencia_Cabecera varchar2(4000);
    v_Where_Tramites varchar2(4000);
    v_Where_Acciones varchar2(4000);
    v_Where_Tramites_Temp varchar2(4000);
    v_Where_Acciones_Temp varchar2(4000);
    v_Where_Cabecera varchar2(4000);
    v_Cursor_SQL varchar(12000);
    v_tipo_tramites number := 1;
    v_tipo_acciones number := 2;
  BEGIN
   -- Cabecera para unir tr�mites y acciones
    v_Sentencia_Cabecera :=
       'select ' ||
       '  botones, Clase, Fecha_Inicio_Suac, Fecha_recepcion, Sticker, ' ||
       '  Nro_Expediente, Asunto, id_tramite_ipj, ' ||
       '  Id_Tramite_Suac, id_clasif_ipj, Tipo_IPJ, id_tipo_accion, URGENTE, nvl(Error_Dato, ''N'') Error_Dato, ' ||
       '  identificacion, persona, id_legajo, id_estado, id_tramiteipj_accion, id_proceso,  ' ||
       '  case ' ||
       '    when clase = 1 and Acc_Abiertas = 0  then ''Completo'' ' ||
       '    when clase = 1 and Acc_Abiertas <> 0  then ''Pendiente'' ' ||
       '    when clase = 2 then n_estado ' ||
       '  end n_estado, ' ||
       '  to_char(Fecha_Inicio_Ipj, ''dd/mm/rrrr'') Fecha_Asignacion, id_pagina, id_fondo_comercio, ' ||
       '  id_integrante ' ||
       ' from  ' ||
       '   ( ';

     -- sentencia para listar Tramites
     v_Sentencia_Tramites :=
       'select ' ||
       '  IPJ.varios.FC_Habilitar_Botones (tIPJ.id_ubicacion,  tIPJ.id_clasif_ipj, 0, tIPJ.id_estado_ult, IPJ.TRAMITES.FC_Acciones_Abiertas (tIPJ.id_tramite_ipj), 0, 2, tIPJ.id_proceso,tIPJ.simple_tramite, tIPJ.Cuil_Ult_Estado, '' '', tIPJ.id_tramite_ipj, 0) botones, ' ||
       '  1 Clase,  to_char(VT_SUAC.fecha_inicio, ''dd/mm/rrrr'') Fecha_Inicio_Suac, ' ||
       '  to_char(VT_SUAC.fecha_ultima_recepcion, ''dd/mm/rrrr'') Fecha_recepcion,  ' ||
       '  nvl(tIPJ.Sticker, VT_SUAC.nro_sticker) Sticker, nvl(tIPJ.Expediente, VT_SUAC.nro_tramite) Nro_Expediente,  ' ||
       '  VT_SUAC.Asunto,  tIPJ.id_tramite_ipj, tIPJ.id_tramite Id_Tramite_Suac, ' ||
       '  tIPJ.id_clasif_ipj, tc.N_Clasif_Ipj Tipo_IPJ, 0 id_tipo_accion, TIPJ.URGENTE, ' ||
       '  ''N'' Error_Dato, 0 id_legajo, tIPJ.id_estado_ult id_estado, ' ||
       '  e.n_estado, TIPJ.FECHA_INICIO Fecha_Inicio_Ipj,  ' ||
       '  IPJ.TRAMITES.FC_Acciones_Abiertas (tIPJ.id_tramite_ipj) Acc_Abiertas, ' ||
       '  IPJ.TRAMITES.FC_Acciones_Cerradas (tIPJ.id_tramite_ipj) Acc_Cerradas,  ' ||
       '  0 id_tramiteipj_accion,  ' ||
       '  tIPJ.id_proceso, '''' identificacion, '''' persona, 0 id_pagina, 0 id_fondo_comercio, ' ||
       '  0 id_integrante ' ||
       'from ' ||
       '  IPJ.t_tramitesIPJ tIPJ left join NUEVOSUAC.VT_TRAMITES_IPJ_DESA vt_suac ' ||
       '    on VT_SUAC.ID_TRAMITE = tIPJ.id_tramite ' ||
       '  join IPJ.t_estados e ' ||
       '    on tIPJ.id_estado_ult = e.id_estado ' ||
       '  left join IPJ.t_tipos_Clasif_ipj tc ' ||
       '    on TIPJ.Id_Clasif_Ipj = tc.Id_Clasif_Ipj ';

    -- sentencia para listar Acciones
    v_Sentencia_Acciones :=
      'select ' ||
      '  IPJ.varios.FC_Habilitar_Botones (tIPJ.id_ubicacion,  tIPJ.id_clasif_ipj, AccIPJ.id_tipo_Accion, 0, -1, AccIPJ.id_estado, 2, tIPJ.id_proceso,tIPJ.simple_tramite, AccIPJ.Cuil_usuario, '' '', tIPJ.id_tramite_ipj, AccIPJ.id_tramiteipj_accion) botones, ' ||
      '  2 Clase, to_char(VT_SUAC.fecha_inicio, ''dd/mm/rrrr'') Fecha_Inicio_Suac, ' ||
      '  to_char(VT_SUAC.fecha_ultima_recepcion, ''dd/mm/rrrr'') Fecha_recepcion, ' ||
      '  nvl(tIPJ.Sticker, VT_SUAC.nro_sticker) Sticker, nvl(tIPJ.Expediente, VT_SUAC.nro_tramite) Nro_Expediente,  ' ||
      '  VT_SUAC.Asunto,  AccIPJ.id_tramite_ipj,  tIPJ.id_tramite Id_Tramite_Suac, ' ||
      '  0 id_clasif_ipj, ta.N_Tipo_Accion Tipo_IPJ, AccIPJ.id_tipo_accion, tIPJ.URGENTE, ' ||
      '  nvl(TRPJ.ERROR_DATO, trInt.Error_Dato) Error_Dato, AccIPJ.id_legajo, AccIPJ.id_estado, ' ||
      '  e.n_estado, tIPJ.FECHA_INICIO Fecha_Inicio_Ipj, 0 Acc_Abiertas, 0 Acc_Cerradas,  ' ||
      '  AccIPJ.id_tramiteipj_accion, tIPJ.id_proceso,  ' ||
      '  nvl(to_char(AccIPJ.id_fondo_comercio), nvl(I.NRO_DOCUMENTO, L.CUIT)) identificacion, ' ||
      '  nvl(fc.n_fondo_comercio, nvl(I.DETALLE, L.Denominacion_Sia)) persona, ta.id_pagina, AccIPJ.id_fondo_comercio, ' ||
      '  AccIPJ.id_integrante ' ||
      'from ipj.t_tramitesipj_acciones AccIPJ join ipj.t_tramitesIPJ tIPJ  ' ||
      '    on AccIPJ.Id_Tramite_IPJ = tIPJ.id_tramite_ipj ' ||
      '  left join NUEVOSUAC.VT_TRAMITES_IPJ_DESA vt_suac ' ||
      '    on VT_SUAC.ID_TRAMITE = tIPJ.id_tramite ' ||
      '  join IPJ.t_estados e ' ||
      '    on AccIPJ.id_estado = e.id_estado ' ||
      '  join ipj.t_tipos_AccionesIpj ta ' ||
      '     on ACCIPJ.ID_TIPO_ACCION = ta.id_tipo_accion ' ||
      '  left join ipj.t_legajos l ' ||
      '    on ACCIPJ.ID_LEGAJO = l.id_legajo ' ||
      '  left join IPJ.T_TRAMITESIPJ_INTEGRANTE trInt ' ||
      '    on ACCIPJ.ID_INTEGRANTE = TRINT.ID_INTEGRANTE and AccIPJ.Id_tramite_ipj = trInt.id_tramite_ipj ' ||
      '  left join IPJ.T_TRAMITESIPJ_PERSJUR trPJ ' ||
      '    on AccIPJ.Id_tramite_ipj = trPJ.id_tramite_ipj and ACCIPJ.id_legajo = TRPJ.id_legajo ' ||
      '  left join IPJ.T_FONDOS_COMERCIO fc ' ||
      '    on AccIPJ.id_fondo_comercio = fc.id_fondo_comercio ' ||
      '  left join IPJ.T_INTEGRANTES i ' ||
      '    on AccIPJ.ID_INTEGRANTE = i.ID_INTEGRANTE ';

    /*********************************************************
           PARA CADA CONDICION AGREGO EL WHERE QUE HAGA FALTA
    *********************************************************/
    -- *********  Datos generales de tramites  **********************
    if p_fecha_inicio is not null and IPJ.VARIOS.Valida_Fecha(p_fecha_inicio) = 'S' and
        p_fecha_fin is not null and IPJ.VARIOS.Valida_Fecha(p_fecha_fin) = 'S' then -- Agrego Rango de Fechas
      v_Where_Tramites := v_Where_Tramites || ' and tIPJ.FECHA_INICIO between to_date(''' || p_fecha_inicio || ''', ''dd/mm/rrrr'') and to_Date('''|| p_fecha_fin || ''', ''dd/mm/rrrr'')+1';
      v_Where_Acciones := v_Where_Acciones || ' and tIPJ.FECHA_INICIO between to_date(''' || p_fecha_inicio || ''', ''dd/mm/rrrr'') and to_Date('''|| p_fecha_fin || ''', ''dd/mm/rrrr'')+1';
    end if;
    if nvl(p_id_estado, 0) > 0 then -- Agrego ESTADO
      v_Where_Tramites := v_Where_Tramites || ' and tIPJ.id_estado_ult = ' ||to_char(p_id_estado);
      v_Where_Acciones := v_Where_Acciones || ' and AccIPJ.id_estado = ' ||to_char(p_id_estado);
    end if;
    if nvl(p_id_ubicacion, 0) > 0 then -- Agrego UBICACION
      v_Where_Tramites := v_Where_Tramites || ' and TIPJ.ID_UBICACION = ' || to_char(p_id_ubicacion);
      v_Where_Acciones := v_Where_Acciones || ' and TIPJ.ID_UBICACION = ' || to_char(p_id_ubicacion);
    end if;
    if nvl(p_id_clasif_ipj, 0) > 0 then -- Agrego CLASIFICACION
      v_Where_Tramites := v_Where_Tramites || ' and TIPJ.ID_CLASIF_IPJ = ' || to_char(p_id_clasif_ipj);
      v_Where_Acciones := v_Where_Acciones || ' and ta.ID_CLASIF_IPJ = ' || to_char(p_id_clasif_ipj);
    end if;
    if nvl(p_id_tipo_accion, 0) > 0 then -- Agrego TIPO de ACCION
      v_Where_Tramites := v_Where_Tramites || ' and ' ||
         'exists (select * from IPJ.T_tramitesipj_acciones t where T.ID_TRAMITE_IPJ = tIPJ.id_tramite_ipj and T.ID_TIPO_ACCION = ' || to_char(p_id_tipo_accion) ||')';
      v_Where_Acciones := v_Where_Acciones || ' and ta.ID_TIPO_ACCION = ' || to_char(p_id_tipo_accion);
    end if;
    if p_sticker is not null then -- Agrego STICKER
      v_Where_Tramites := v_Where_Tramites || ' and VT_SUAC.nro_sticker like ''%' || trim(p_sticker) || '%''';
      v_Where_Acciones := v_Where_Acciones || ' and VT_SUAC.nro_sticker like ''%' || trim(p_sticker) || '%''';
    end if;
    if p_expediente is not null then -- Agrego EXPEDIENTE
      v_Where_Tramites := v_Where_Tramites || ' and upper(VT_SUAC.nro_tramite) like ''%' || trim(p_expediente) || '%''';
      v_Where_Acciones := v_Where_Acciones || ' and upper(VT_SUAC.nro_tramite) like ''%' || trim(upper(p_expediente)) || '%''';
    end if;
    if nvl(p_incl_historicos, 0) = 0 then -- Agrego INCLUIR HISTORICOS 0 = No incluye, 1 = Incluye
      v_Where_Tramites := v_Where_Tramites || ' and nvl(tIPJ.id_proceso, 0) not in (' || to_char(IPJ.TYPES.C_PROCESO_GEN_HIST) || ', ' || to_char(IPJ.TYPES.C_PROCESO_HIST) || ')';
      v_Where_Acciones := v_Where_Acciones || ' and nvl(tIPJ.id_proceso, 0) not in (' || to_char(IPJ.TYPES.C_PROCESO_GEN_HIST) || ', ' || to_char(IPJ.TYPES.C_PROCESO_HIST) || ')';
    end if;

    -- ***********  Datos de Empresas   ***************************
    if nvl(p_id_legajo, 0) > 0 or
      nvl(p_id_tipo_entidad, 0) > 0 or
      p_cuit is not null or
      p_razon_social is not null or
      (nvl(p_matricula_ent, 0 )> 0 and p_matricula_letra_ent is not null) or
      p_nro_registro is not null or
      p_id_estado_entidad is not null or
      nvl(p_id_tipo_origen, 0) > 0 or
      (p_capital is not null and IPJ.VARIOS.ToNumber(p_capital) > 0) or
      p_fec_constitucion is not null or
      p_obj_social is not null then

     v_Where_Tramites := v_Where_Tramites || ' and exists (select * from IPJ.t_entidades t where T.ID_TRAMITE_IPJ = tIPJ.id_tramite_ipj';
     v_Where_Acciones := v_Where_Acciones || ' and exists (select * from IPJ.t_entidades t where T.ID_TRAMITE_IPJ = tIPJ.id_tramite_ipj and t.id_legajo = AccIPJ.id_legajo';
      if nvl(p_id_legajo, 0) > 0 then -- Agrego LEGAJO
        v_Where_Tramites := v_Where_Tramites || ' and t.id_legajo = ' || to_char(p_id_legajo);
        v_Where_Acciones := v_Where_Acciones || ' and t.id_legajo = ' || to_char(p_id_legajo);
      end if;
      if nvl(p_id_tipo_entidad, 0) > 0 then -- Agrego TIPO de ENTIDAD
        v_Where_Tramites := v_Where_Tramites || ' and t.id_tipo_entidad = ' || to_char(p_id_tipo_entidad);
        v_Where_Acciones := v_Where_Acciones || ' and t.id_tipo_entidad = ' || to_char(p_id_tipo_entidad);
      end if;
      if p_cuit is not null then -- Agrego CUIT
        v_Where_Tramites := v_Where_Tramites || ' and t.cuit = ''' || replace(replace(trim(p_cuit), '-', ''), '/', '') || '''';
        v_Where_Acciones := v_Where_Acciones || ' and t.cuit = ''' || replace(replace(trim(p_cuit), '-', ''), '/', '') || '''';
      end if;
      if p_razon_social is not null then -- Agrego RAZON SOCIAL
        v_Where_Tramites := v_Where_Tramites || ' and upper(t.denominacion_1) like ''%' || upper(trim(p_razon_social)) || '%''';
        v_Where_Acciones := v_Where_Acciones || ' and upper(t.denominacion_1) like ''%' || upper(trim(p_razon_social)) || '%''';
      end if;
      if nvl(p_matricula_ent, 0 )> 0 and p_matricula_letra_ent is not null then -- Agrego MATRICULA
        v_Where_Tramites := v_Where_Tramites || ' and t.matricula = ''' || p_matricula_letra_ent || to_char(p_matricula_ent) ||  '''';
        v_Where_Acciones := v_Where_Acciones || ' and t.matricula = ''' || p_matricula_letra_ent || to_char(p_matricula_ent) ||  '''';
      end if;
      if p_nro_registro is not null then -- Agrego REGISTRO o FICHA
        v_Where_Tramites := v_Where_Tramites || ' and upper(t.nro_registro) = ''' || upper(trim(p_nro_registro)) || '''';
        v_Where_Acciones := v_Where_Acciones || ' and upper(t.nro_registro) = ''' || upper(trim(p_nro_registro)) || '''';
      end if;
      if p_id_estado_entidad is not null then -- Agrego ESTADO DE LA EMPRESA
        v_Where_Tramites := v_Where_Tramites || ' and t.id_estado_entidad = ''' || p_id_estado_entidad || '''';
        v_Where_Acciones := v_Where_Acciones || ' and t.id_estado_entidad = ''' || p_id_estado_entidad || '''';
      end if;
      if nvl(p_id_tipo_origen, 0) > 0 then -- Agrego ORIGEN (del estado)
        v_Where_Tramites := v_Where_Tramites || ' and t.origen = ' || to_char(p_id_tipo_origen);
        v_Where_Acciones := v_Where_Acciones || ' and t.origen = ' || to_char(p_id_tipo_origen);
      end if;
      if p_capital is not null and IPJ.VARIOS.ToNumber(p_capital) > 0 then -- Agrego CAPITAL SOCIAL
        v_Where_Tramites := v_Where_Tramites || ' and t.monto = ' || p_capital ;
        v_Where_Acciones := v_Where_Acciones || ' and t.monto = ' || p_capital ;
      end if;
      if p_fec_constitucion is not null then -- Agrego FECHA CONSTITUCION
        v_Where_Tramites := v_Where_Tramites || ' and t.acta_contitutiva = to_date(''' || p_fec_constitucion || ''', ''dd/mm/rrrr'')';
        v_Where_Acciones := v_Where_Acciones || ' and t.acta_contitutiva = to_date(''' || p_fec_constitucion || ''', ''dd/mm/rrrr'')';
      end if;
      if p_obj_social is not null  then -- Agrego OBSEJTO SOCIAL
        v_Where_Tramites := v_Where_Tramites || ' and upper(t.desc_obj_social) like ''%' || upper(p_obj_social) || '%''';
        v_Where_Acciones := v_Where_Acciones || ' and upper(t.desc_obj_social) like ''%' || upper(p_obj_social) || '%''';
      end if;

      v_Where_Tramites := v_Where_Tramites || ')';
      v_Where_Acciones := v_Where_Acciones || ')';
    end if;

    -- Agrego la clasificacion de AFIP a controla, por Rubro y/o Tipo Act. y/o Actividad
    if p_id_actividad is not null or p_id_rubro is not null or p_id_tipo_actividad is not null then
      v_Where_Tramites := v_Where_Tramites || ' and ' ||
         'exists (select * from IPJ.t_entidades_rubros t where t.ID_TRAMITE_IPJ = tIPJ.id_tramite_ipj ';
      v_Where_Acciones := v_Where_Acciones || ' and ' ||
         'exists (select * from IPJ.t_entidades_rubros t where t.ID_TRAMITE_IPJ = tIPJ.id_tramite_ipj and t.id_legajo = AccIPJ.id_legajo ';

      if p_id_rubro is not null then
        v_Where_Tramites := v_Where_Tramites || 'and t.id_rubro = ''' || p_id_rubro || ''' ';
        v_Where_Acciones := v_Where_Acciones || 'and t.id_rubro = ''' || p_id_rubro || ''' ';
      end if;
      if p_id_tipo_Actividad is not null then
        v_Where_Tramites := v_Where_Tramites || 'and t.id_tipo_actividad = ''' || p_id_tipo_actividad || ''' ';
        v_Where_Acciones := v_Where_Acciones || 'and t.id_tipo_actividad = ''' || p_id_tipo_actividad || ''' ';
      end if;
      if p_id_actividad is not null then
        v_Where_Tramites := v_Where_Tramites || 'and t.id_actividad = ''' || p_id_actividad || ''' ';
        v_Where_Acciones := v_Where_Acciones || 'and t.id_actividad = ''' || p_id_actividad || ''' ';
      end if;

      v_Where_Tramites := v_Where_Tramites || ') ';
      v_Where_Acciones := v_Where_Acciones || ') ';
    end if;

    -- ***********  Datos de Personas   ***************************
    if nvl(p_matricula_pers, 0 )> 0 and p_matricula_letra_pers is not null  then -- Agrego MATRICULA COMERCIANTE
      v_Where_Tramites := v_Where_Tramites || ' and ' ||
         'exists (select * from IPJ.t_comerciantes t where t.ID_TRAMITE_IPJ = tIPJ.id_tramite_ipj and t.matricula = ''' || p_matricula_letra_pers || to_char(p_matricula_pers) ||  ''')';
      v_Where_Acciones := v_Where_Acciones || ' and ' ||
         'exists (select * from IPJ.t_comerciantes t where t.ID_TRAMITE_IPJ = tIPJ.id_tramite_ipj and t.id_tramiteipj_accion = AccIPJ.id_tramiteipj_accion and t.matricula = ''' || p_matricula_letra_pers || to_char(p_matricula_pers) ||  ''')';
    end if;

    -- Agrego los join para buscar en las distintas tablas de personas
    if p_id_sexo is not null and nvl(p_buscar_persona, 0) > 0 then
      v_Where_Tramites := v_Where_Tramites || ' and exists (select * from IPJ.t_integrantes t';
      v_Where_Acciones := v_Where_Acciones || ' and exists (select * from IPJ.t_integrantes t';

      if bitand(p_buscar_persona, 1) = 1 then -- Comerciantes
        v_Where_Tramites := v_Where_Tramites || ' left join IPJ.T_comerciantes c on c.id_integrante = t.id_integrante';
        v_Where_Acciones := v_Where_Acciones || ' left join IPJ.T_comerciantes c on c.id_integrante = t.id_integrante';
      end if;
      if bitand(p_buscar_persona, 2) = 2 then -- Comprador Fondo Com.
        v_Where_Tramites := v_Where_Tramites || ' left join IPJ.t_fondos_comercio_comprador fcc on fcc.id_integrante = t.id_integrante';
        v_Where_Acciones := v_Where_Acciones || ' left join IPJ.t_fondos_comercio_comprador fcc on fcc.id_integrante = t.id_integrante';
      end if;
      if bitand(p_buscar_persona, 4) = 4 then -- Vendedor Fondo Com.
        v_Where_Tramites := v_Where_Tramites || ' left join IPJ.t_fondos_comercio_vendedor fcv on fcv.id_integrante = t.id_integrante';
        v_Where_Acciones := v_Where_Acciones || ' left join IPJ.t_fondos_comercio_vendedor fcv on fcv.id_integrante = t.id_integrante';
      end if;
      if bitand(p_buscar_persona, 8) = 8 then -- Socios Entidad
        v_Where_Tramites := v_Where_Tramites || ' left join IPJ.t_entidades_socios es on es.id_integrante = t.id_integrante';
        v_Where_Acciones := v_Where_Acciones || ' left join IPJ.t_entidades_socios es on es.id_integrante = t.id_integrante';
      end if;
      if bitand(p_buscar_persona, 16) = 16 then -- Administardores Entidad
        v_Where_Tramites := v_Where_Tramites || ' left join IPJ.t_entidades_admin ea on ea.id_integrante = t.id_integrante';
        v_Where_Acciones := v_Where_Acciones || ' left join IPJ.t_entidades_admin ea on ea.id_integrante = t.id_integrante';
      end if;
      if bitand(p_buscar_persona, 32) = 32 then -- Representantes Entidad
        v_Where_Tramites := v_Where_Tramites || ' left join IPJ.t_entidades_representante er on er.id_integrante = t.id_integrante';
        v_Where_Acciones := v_Where_Acciones || ' left join IPJ.t_entidades_representante er on er.id_integrante = t.id_integrante';
      end if;

      -- Una vez cargado los join, agrego el where general con las condiciones
      v_Where_Tramites_Temp := null;
      v_Where_Acciones_Temp := null;
      if bitand(p_buscar_persona, 1) = 1 then -- Comerciantes
        v_Where_Tramites_Temp := v_Where_Tramites_Temp || ' or c.ID_TRAMITE_IPJ = tIPJ.id_tramite_ipj';
        v_Where_Acciones_Temp := v_Where_Acciones_Temp|| ' or (c.ID_TRAMITE_IPJ = tIPJ.id_tramite_ipj and c.id_tramiteipj_accion = AccIPJ.id_tramiteipj_accion) ';
      end if;
      if bitand(p_buscar_persona, 2) = 2 then -- Comprador Fondo Com.
        v_Where_Tramites_Temp := v_Where_Tramites_Temp || ' or fcc.ID_TRAMITE_IPJ = tIPJ.id_tramite_ipj';
        v_Where_Acciones_Temp := v_Where_Acciones_Temp|| ' or (fcc.ID_TRAMITE_IPJ = tIPJ.id_tramite_ipj and fcc.id_tramiteipj_accion = AccIPJ.id_tramiteipj_accion) ';
      end if;
      if bitand(p_buscar_persona, 4) = 4 then -- Vendedor Fondo Com.
        v_Where_Tramites_Temp := v_Where_Tramites_Temp || ' or fcv.ID_TRAMITE_IPJ = tIPJ.id_tramite_ipj';
        v_Where_Acciones_Temp := v_Where_Acciones_Temp|| ' or (fcv.ID_TRAMITE_IPJ = tIPJ.id_tramite_ipj and fcv.id_tramiteipj_accion = AccIPJ.id_tramiteipj_accion) ';
      end if;
      if bitand(p_buscar_persona, 8) = 8 then -- Socios Entidad
        v_Where_Tramites_Temp := v_Where_Tramites_Temp || ' or es.ID_TRAMITE_IPJ = tIPJ.id_tramite_ipj';
        v_Where_Acciones_Temp := v_Where_Acciones_Temp|| ' or (es.ID_TRAMITE_IPJ = tIPJ.id_tramite_ipj and es.id_legajo = AccIPJ.id_legajo) ';
      end if;
      if bitand(p_buscar_persona, 16) = 16 then -- Administardores Entidad
        v_Where_Tramites_Temp := v_Where_Tramites_Temp || ' or ea.ID_TRAMITE_IPJ = tIPJ.id_tramite_ipj';
        v_Where_Acciones_Temp := v_Where_Acciones_Temp|| ' or (ea.ID_TRAMITE_IPJ = tIPJ.id_tramite_ipj and ea.id_legajo = AccIPJ.id_legajo) ';
      end if;
      if bitand(p_buscar_persona, 32) = 32 then -- Representantes Entidad
        v_Where_Tramites_Temp := v_Where_Tramites_Temp || ' or er.ID_TRAMITE_IPJ = tIPJ.id_tramite_ipj';
        v_Where_Acciones_Temp := v_Where_Acciones_Temp|| ' or (er.ID_TRAMITE_IPJ = tIPJ.id_tramite_ipj and ea.id_legajo = AccIPJ.id_legajo) ';
      end if;

      --Agrego la condion del where del subselect para socios
      v_Where_Tramites := v_Where_Tramites || ' where ';
      v_Where_Acciones := v_Where_Acciones || ' where ';
      if v_Where_Tramites_Temp is not null then
        v_Where_Tramites := v_Where_Tramites || ' (' || substr(v_Where_Tramites_Temp, 5, 4000) || ') ' ;
      end if;
      if v_Where_Acciones_Temp is not null then
        v_Where_Acciones := v_Where_Acciones || ' (' || substr(v_Where_Acciones_Temp, 5, 4000) || ') ' ;
      end if;

      -- Agrego ID INTEGRANTE, es por personas y hay sexo
      v_Where_Tramites := v_Where_Tramites || ' and t.id_integrante = ' || to_char(nvl(p_id_integrante, 0));
      v_Where_Acciones := v_Where_Acciones || ' and t.id_integrante = ' || to_char(nvl(p_id_integrante, 0));

      v_Where_Tramites := v_Where_Tramites || ')';
      v_Where_Acciones := v_Where_Acciones || ')';
    end if;


    -- ***********  Domicilios   ***************************
    if (nvl(p_Id_Departamento, 0) > 0 or nvl(p_Id_Localidad, 0) > 0) and nvl(p_buscar_domicilio, 0) > 0 then
      v_Where_Tramites := v_Where_Tramites || ' and exists (select * from DOM_MANAGER.VT_DOMICILIOS_COND t';
      v_Where_Acciones := v_Where_Acciones || ' and exists (select * from DOM_MANAGER.VT_DOMICILIOS_COND t';

      -- armo los join a las distintas tablas con domicilios
      if bitand(p_buscar_domicilio, 1) = 1 then -- Empresas
        v_Where_Tramites := v_Where_Tramites || ' left join IPJ.T_entidades e on e.id_vin_real = t.id_vin'; --c.ID_TRAMITE_IPJ = tIPJ.id_tramite_ipj and
        v_Where_Acciones := v_Where_Acciones || ' left join IPJ.T_entidades e on e.id_vin_real = t.id_vin';
      end if;
      if bitand(p_buscar_domicilio, 2) = 2 then -- Asambleas Empresas
        v_Where_Tramites := v_Where_Tramites || ' left join t_entidades_acta eas on eas.id_vin = t.id_vin'; --c.ID_TRAMITE_IPJ = tIPJ.id_tramite_ipj and
        v_Where_Acciones := v_Where_Acciones || ' left join t_entidades_acta eas on eas.id_vin = t.id_vin';
      end if;
      if bitand(p_buscar_domicilio, 4) = 4 then -- Administradores Empresas
        v_Where_Tramites := v_Where_Tramites || ' left join IPJ.T_entidades_admin ea on ea.id_vin = t.id_vin'; --c.ID_TRAMITE_IPJ = tIPJ.id_tramite_ipj and
        v_Where_Acciones := v_Where_Acciones || ' left join IPJ.T_entidades_admin ea on ea.id_vin = t.id_vin';
      end if;
      if bitand(p_buscar_domicilio, 8) = 8 then -- Comerciantes
        v_Where_Tramites := v_Where_Tramites || ' left join IPJ.T_comerciantes c on c.id_vin_real = t.id_vin or c.id_vin_comercial = t.id_vin'; --c.ID_TRAMITE_IPJ = tIPJ.id_tramite_ipj and
        v_Where_Acciones := v_Where_Acciones || ' left join IPJ.T_comerciantes c on c.id_vin_real = t.id_vin or c.id_vin_comercial = t.id_vin';
      end if;
      if bitand(p_buscar_domicilio, 16) = 16 then -- Fondo Comercio
        v_Where_Tramites := v_Where_Tramites || ' left join IPJ.t_fondos_comercio fc on fc.id_vin = t.id_vin' ||
          ' left join ipj.t_tramitesipj_acciones a on a.Id_Fondo_Comercio = fc.Id_Fondo_Comercio'; --a.ID_TRAMITE_IPJ = tIPJ.id_tramite_ipj and
        v_Where_Acciones := v_Where_Acciones || ' left join IPJ.t_fondos_comercio fc on fc.id_vin = t.id_vin';
      end if;
      if bitand(p_buscar_domicilio, 32) =32 or bitand(p_buscar_domicilio, 64) = 64 then
         v_Where_Tramites := v_Where_Tramites || ' left join ipj.t_integrantes i on  i.id_vin = t.id_vin ';
         v_Where_Acciones := v_Where_Acciones || ' left join ipj.t_integrantes i on  i.id_vin = t.id_vin ';
        if bitand(p_buscar_domicilio, 32) =32 then -- Comprador Fondo Comercio
          v_Where_Tramites := v_Where_Tramites || ' left join IPJ.t_fondos_comercio_comprador fcc on fcc.id_integrante = i.id_integrante'; --c.ID_TRAMITE_IPJ = tIPJ.id_tramite_ipj and
          v_Where_Acciones := v_Where_Acciones || ' left join IPJ.t_fondos_comercio_comprador fcc on fcc.id_integrante = i.id_integrante';
        end if;
        if bitand(p_buscar_domicilio, 64) = 64 then -- Vendedor Fondo Comercio
          v_Where_Tramites := v_Where_Tramites || ' left join IPJ.t_fondos_comercio_vendedor fcv on fcv.id_integrante = i.id_integrante'; --c.ID_TRAMITE_IPJ = tIPJ.id_tramite_ipj and
          v_Where_Acciones := v_Where_Acciones || ' left join IPJ.t_fondos_comercio_vendedor fcv on fcv.id_integrante = i.id_integrante';
        end if;
      end if;

      -- Armo los where temporales de los join
      v_Where_Tramites_Temp := null;
      v_Where_Acciones_Temp := null;
      if bitand(p_buscar_domicilio, 1) = 1 then -- Empresas
        v_Where_Tramites_Temp := v_Where_Tramites_Temp || ' or e.ID_TRAMITE_IPJ = tIPJ.id_tramite_ipj ' ;
        v_Where_Acciones_Temp := v_Where_Acciones_Temp || ' or (e.ID_TRAMITE_IPJ = tIPJ.id_tramite_ipj and e.id_legajo = AccIPJ.id_legajo)';
      end if;
      if bitand(p_buscar_domicilio, 2) = 2 then -- Asambleas Empresas
        v_Where_Tramites_Temp := v_Where_Tramites_Temp || ' or eas.ID_TRAMITE_IPJ = tIPJ.id_tramite_ipj' ;
        v_Where_Acciones_Temp := v_Where_Acciones_Temp || ' or (eas.ID_TRAMITE_IPJ = tIPJ.id_tramite_ipj and eas.id_legajo = AccIPJ.id_legajo)';
      end if;
      if bitand(p_buscar_domicilio, 4) = 4 then -- Administradores Empresas
        v_Where_Tramites_Temp := v_Where_Tramites_Temp || ' or ea.ID_TRAMITE_IPJ = tIPJ.id_tramite_ipj' ;
        v_Where_Acciones_Temp := v_Where_Acciones_Temp || ' or (ea.ID_TRAMITE_IPJ = tIPJ.id_tramite_ipj and ea.id_legajo = AccIPJ.id_legajo)';
      end if;
      if bitand(p_buscar_domicilio, 8) = 8 then -- Comerciantes
        v_Where_Tramites_Temp := v_Where_Tramites_Temp || ' or c.ID_TRAMITE_IPJ = tIPJ.id_tramite_ipj';
        v_Where_Acciones_Temp := v_Where_Acciones_Temp || ' or (c.ID_TRAMITE_IPJ = tIPJ.id_tramite_ipj and c.id_tramiteipj_accion = AccIPJ.id_tramiteipj_accion)';
      end if;
      if bitand(p_buscar_domicilio, 16) = 16 then -- Fondo Comercio
        v_Where_Tramites_Temp := v_Where_Tramites_Temp || ' or a.ID_TRAMITE_IPJ = tIPJ.id_tramite_ipj';
        v_Where_Acciones_Temp := v_Where_Acciones_Temp || ' or fc.Id_Fondo_Comercio = AccIPJ.Id_Fondo_Comercio';
      end if;
      if bitand(p_buscar_domicilio, 32) =32 then -- Comprador Fondo Comercio
        v_Where_Tramites_Temp := v_Where_Tramites_Temp || ' or fcc.ID_TRAMITE_IPJ = tIPJ.id_tramite_ipj';
        v_Where_Acciones_Temp := v_Where_Acciones_Temp || ' or (fcc.ID_TRAMITE_IPJ = tIPJ.id_tramite_ipj and fcc.Id_Fondo_Comercio = AccIPJ.Id_Fondo_Comercio)';
      end if;
      if bitand(p_buscar_domicilio, 64) = 64 then -- Vendedor Fondo Comercio
        v_Where_Tramites_Temp := v_Where_Tramites_Temp || ' or fcv.ID_TRAMITE_IPJ = tIPJ.id_tramite_ipj';
        v_Where_Acciones_Temp := v_Where_Acciones_Temp || ' or (fcv.ID_TRAMITE_IPJ = tIPJ.id_tramite_ipj and fcv.Id_Fondo_Comercio = AccIPJ.Id_Fondo_Comercio)';
      end if;

      -- Agrego los Where de los subconsultas de domicilio
      v_Where_Tramites := v_Where_Tramites || ' where t.id_app = ' || to_char(IPJ.TYPES.C_ID_APLICACION);
      v_Where_Acciones := v_Where_Acciones || ' where t.id_app = ' || to_char(IPJ.TYPES.C_ID_APLICACION);
      if v_Where_Tramites_Temp is not null then
        v_Where_Tramites := v_Where_Tramites || ' and (' || substr(v_Where_Tramites_Temp, 5, 4000) || ') ' ;
      end if;
      if v_Where_Acciones_Temp is not null then
        v_Where_Acciones := v_Where_Acciones || ' and (' || substr(v_Where_Acciones_Temp, 5, 4000) || ') ' ;
      end if;

      if nvl(p_Id_Departamento, 0) > 0  then -- Agrego DEPARTAMENTO
        v_Where_Tramites := v_Where_Tramites || ' and t.id_departamento = ' || to_char(p_Id_Departamento);
        v_Where_Acciones := v_Where_Acciones || ' and t.id_departamento = ' || to_char(p_Id_Departamento);
      end if;

       if nvl(p_Id_Localidad, 0) > 0  then -- Agrego LOCALIDAD
        v_Where_Tramites := v_Where_Tramites || ' and t.id_localidad = ' || to_char(p_Id_Localidad);
        v_Where_Acciones := v_Where_Acciones || ' and t.id_localidad = ' || to_char(p_Id_Localidad);
      end if;
      v_Where_Tramites := v_Where_Tramites || ')';
      v_Where_Acciones := v_Where_Acciones || ')';
    end if;

    -- ***********  Actuaciones  ***************************
    if p_nro_actuacion is not null  then -- Agrego NUMERO de ACTUACION
      v_Where_Tramites := v_Where_Tramites || ' and ' ||
         'exists (select * from IPJ.t_tramitesij_acciones t where t.ID_TRAMITE_IPJ = tIPJ.id_tramite_ipj and t.nro_actuacion = ''' || p_nro_actuacion || ''')';
      v_Where_Acciones := v_Where_Acciones || ' and AccIPJ.nro_actuacion = ''' || p_nro_actuacion || ''')';
    end if;
    if p_texto_informe is not null  then -- Agrego TEXTO INFORME
      v_Where_Tramites := v_Where_Tramites || ' and ' ||
         'exists (select * from IPJ.T_Informes_Borrador t where t.ID_TRAMITE_IPJ = tIPJ.id_tramite_ipj and t.texto_informe like ''%' || p_texto_informe || '%'')';
      v_Where_Acciones := v_Where_Acciones || ' and ' ||
         'exists (select * from IPJ.T_Informes_Borrador t where t.ID_TRAMITE_IPJ = tIPJ.id_tramite_ipj and t.id_tramiteipj_accion = AccIPJ.id_tramiteipj_accion and t.texto_informe like ''%' || p_texto_informe || '%'')';
    end if;


    -- Armo la sentencia del cursor
    v_Cursor_SQL:= v_Sentencia_Cabecera;
    if bitand(p_tipo_operacion, v_tipo_tramites) = v_tipo_tramites then
      v_Cursor_SQL:=  v_Cursor_SQL || v_Sentencia_Tramites;
      if v_Where_Tramites is not null then
        v_Cursor_SQL:=  v_Cursor_SQL || 'where ' || substr(v_Where_Tramites, 6, 4000);
      end if;
    end if;
    if bitand(p_tipo_operacion, v_tipo_acciones) = v_tipo_acciones then
      if bitand(p_tipo_operacion, v_tipo_tramites) = v_tipo_tramites then
        v_Cursor_SQL:=  v_Cursor_SQL || ' UNION ';
      end if;
      v_Cursor_SQL:=  v_Cursor_SQL || v_Sentencia_Acciones;
      if v_Where_Acciones is not null then
        v_Cursor_SQL:=  v_Cursor_SQL || 'where ' || substr(v_Where_Acciones, 6, 4000);
      end if;
    end if;
    v_Cursor_SQL:= v_Cursor_SQL || ') temp ';
    v_Cursor_SQL:= v_Cursor_SQL || 'Order by id_tramite_ipj, clase';

   -- Este bloque se comenta, se habilita solo para debug de la busqueda
    IPJ.VARIOS.SP_GUARDAR_LOG(
         p_METODO => 'DIRECCION.SP_Buscar_Tramites',
         p_NIVEL => 'Tr�mites',
         p_ORIGEN => 'DB',
         p_USR_LOG  => 'DB',
         p_USR_WINDOWS => '',
         p_IP_PC  => '',
         p_EXCEPCION => SubStr(v_Sentencia_Tramites || ' Where ' || substr(v_Where_Tramites, 6, 4000), 1, 4000));

    IPJ.VARIOS.SP_GUARDAR_LOG(
         p_METODO => 'DIRECCION.SP_Buscar_Tramites',
         p_NIVEL => 'Acciones',
         p_ORIGEN => 'DB',
         p_USR_LOG  => 'DB',
         p_USR_WINDOWS => '',
         p_IP_PC  => '',
         p_EXCEPCION => SubStr(v_Sentencia_Acciones || ' Where ' || substr(v_Where_Acciones, 6, 4000), 1, 4000));

    OPEN o_Cursor FOR
      v_Cursor_SQL;



  EXCEPTION
     WHEN OTHERS THEN
       IPJ.VARIOS.SP_GUARDAR_LOG(
         p_METODO => 'DIRECCION.SP_Buscar_Tramites',
         p_NIVEL => 'Tr�mites',
         p_ORIGEN => 'DB',
         p_USR_LOG  => 'DB',
         p_USR_WINDOWS => '',
         p_IP_PC  => '',
         p_EXCEPCION => SubStr(v_Sentencia_Tramites || ' Where ' || substr(v_Where_Tramites, 6, 4000), 1, 4000));

       IPJ.VARIOS.SP_GUARDAR_LOG(
         p_METODO => 'DIRECCION.SP_Buscar_Tramites',
         p_NIVEL => 'Acciones',
         p_ORIGEN => 'DB',
         p_USR_LOG  => 'DB',
         p_USR_WINDOWS => '',
         p_IP_PC  => '',
         p_EXCEPCION => SubStr(v_Sentencia_Acciones || ' Where ' || substr(v_Where_Acciones, 6, 4000), 1, 4000));
  END SP_Buscar_Tramites;

  PROCEDURE SP_Informar_Tasas (
    o_Cursor out IPJ.TYPES.CURSORTYPE ,
    p_fecha_inicio in varchar2,
    p_fecha_fin in varchar2
  )
  IS
  BEGIN

    OPEN o_Cursor FOR
      select
        u.id_ubicacion, u.n_ubicacion, 'Tasa ' || to_char(tt.id_tasa) N_tasa,
        sum(case when tr.urgente = 'N' then 1 else 0 end) Cobrados, trim(To_Char(sum(case when tr.urgente = 'N' then tt.importe else 0 end), '9999999999999990.99')) Imp_Cobrados,
        sum(case when tr.urgente = 'S' then 1 else 0 end) Exentos, trim(To_Char(sum(case when tr.urgente = 'S' then tt.importe else 0 end), '9999999999999990.99')) Imp_Exentos,
        count(tt.importe) Convenio, trim(To_Char(sum(tt.importe), '9999999999999990.99')) Imp_Convenio
      from IPJ.T_TRAMITESIPJ_ACC_TASA tt join ipj.t_tramitesipj tr
          on TT.ID_TRAMITE_IPJ = TR.ID_TRAMITE_IPJ
        join ipj.t_ubicaciones u
          on TR.ID_UBICACION_origen = u.id_ubicacion
      where
        TR.FECHA_INICIO between to_date(p_fecha_inicio, 'dd/mm/rrrr') and to_date(p_fecha_fin, 'dd/mm/rrrr')
      group by u.id_ubicacion, u.n_ubicacion, tt.id_tasa
      order by u.n_ubicacion;

  END SP_Informar_Tasas;

  PROCEDURE SP_Buscar_Personas (
    o_Cursor out IPJ.TYPES.CURSORTYPE ,
    p_nro_documento in varchar2,
    p_nombre in varchar2,
    p_apellido in varchar2
  )
  IS
    v_SQL varchar2(4000);
    v_where varchar2(2000);
  BEGIN

    v_SQL :=
      'select p.id_sexo, p.nro_documento,  p.pai_cod_pais, p.id_numero, p.NOV_NOMBRE || '' '' ||p.NOV_APELLIDO Detalle, ' ||
      '  I.ID_INTEGRANTE, p.n_tipo_documento, i.cuil ' ||
      'from rcivil.vt_pk_persona  p left join IPJ.T_INTEGRANTES i ' ||
      '  on P.ID_SEXO = I.ID_SEXO and  ' ||
      '    p.NRO_DOCUMENTO = I.NRO_DOCUMENTO and ' ||
      '    p.PAI_COD_PAIS = I.PAI_COD_PAIS and  ' ||
      '    P.ID_NUMERO = I.ID_NUMERO  ' ||
      'where  ';

    if p_nro_documento is not null then
      v_where := v_where || 'and lpad(p.nro_documento, 12, ''0'') = lpad(''' || p_nro_documento || ''', 12, ''0'')';
    end if;
    if p_nombre is not null then
        v_where := v_where || 'and upper(p.nov_nombre) like ''' ||upper(p_nombre) || '%''' ;
    end if;
    if p_apellido is not null then
        v_where := v_where || 'and upper(p.nov_apellido) like ''' || upper(p_apellido) || '%''';
    end if;

    v_SQL := v_SQL || substr(v_where, 5, 2000) ||  'order by detalle ';

    OPEN o_Cursor FOR v_SQL;

  EXCEPTION
    when OTHERS then
      IPJ.VARIOS.SP_GUARDAR_LOG(
        p_METODO => 'DIRECCION.SP_Buscar_Personas',
        p_NIVEL => 'Error Query',
        p_ORIGEN => 'DB',
        p_USR_LOG  => 'DB',
        p_USR_WINDOWS => 'Sistema',
        p_IP_PC  => null,
        p_EXCEPCION =>
          'Consula = ' || v_SQL
      );
      Commit;

  END SP_Buscar_Personas;


end Direccion;
/

