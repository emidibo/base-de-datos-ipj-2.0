CREATE OR REPLACE PACKAGE IPJ.Comerciantes AS
/******************************************************************************
   NAME:       Comerciantes
   PURPOSE: Almacenar todas las operaciones relacionas con Comerciantes y Auxiliares de Comercio

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        18/12/2014      crabellini       1. Created this package.
******************************************************************************/

  PROCEDURE SP_Traer_Comerciante(
    p_Cursor OUT types.cursorType,
    p_id_tramite_ipj in number,
    p_id_tramiteipj_accion in number,
    p_id_tipo_accion in number,
    p_id_integrante in number);
  
  PROCEDURE SP_Traer_Comerciante_Rubros(
    p_Cursor OUT types.cursorType,
    p_id_tramite_ipj in number,
    p_id_tramiteipj_accion in number,
    p_id_tipo_accion in number,
    p_id_integrante in number);
    
  PROCEDURE SP_Traer_Comerciante_InsLegal(
    p_Cursor OUT types.cursorType,
    p_id_tramite_ipj in number,
    p_id_tramiteipj_accion in number,
    p_id_tipo_accion in number,
    p_id_integrante in number);
  
   PROCEDURE SP_Traer_Comerciante_Domicilio(
      p_Cursor OUT TYPES.cursorType,
      p_id_tramite_ipj in number,
      p_id_tramiteipj_accion in number,
      p_id_tipo_accion in number,
      p_id_integrante in number,
      p_id_vin in number,
      p_cuil in varchar2);
    
   
   PROCEDURE SP_GUARDAR_COMERCIANTES(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_ID_TRAMITE_IPJ in NUMBER,
    p_ID_TRAMITEIPJ_ACCION  in NUMBER,
    p_ID_TIPO_ACCION  in NUMBER,
    p_ID_INTEGRANTE  in NUMBER,
    p_FEC_INSCRIPCION in varchar2,
    p_MATRICULA  in VARCHAR2,
    p_MATRICULA_VERSION  in NUMBER,
    p_FOLIO in VARCHAR2,
    p_TOMO in VARCHAR2,
    p_ANIO in NUMBER,
    p_PROTOCOLO in VARCHAR2,
    p_ID_VIN_REAL in NUMBER,
    p_ID_VIN_COMERCIAL in NUMBER,
    p_OBJETO_SOCIAL in VARCHAR2,
    p_CUIL in varchar2,
    p_letra in varchar2
  );
      
   PROCEDURE SP_GUARDAR_COMERCIANTES_IL(
     o_rdo out varchar2,
     o_tipo_mensaje out number,
     p_ID_TRAMITE_IPJ in NUMBER,
     p_ID_TRAMITEIPJ_ACCION in NUMBER,
     p_ID_TIPO_ACCION in NUMBER,
     p_ID_INTEGRANTE in NUMBER,
     p_ID_JUZGADO in NUMBER,
     p_IL_TIPO in NUMBER,
     p_IL_NUMERO in NUMBER,
     p_IL_FECHA in varchar2,
     p_TITULO in VARCHAR2,
     p_DESCRIPCION in VARCHAR2,
     p_FECHA in varchar2,
     p_eliminar in number);


   PROCEDURE SP_GUARDAR_COMERCIANTES_RUBROS(
     o_rdo out varchar2,
     o_tipo_mensaje out number,
     p_id_tramite_ipj in number,
     p_id_tramiteipj_accion in number,
     p_id_tipo_accion in number,
     p_id_integrante in number,
     p_id_rubro in varchar2,
     p_id_tipo_actividad in varchar2,
     p_id_actividad in varchar2,
     p_fecha_inicio in varchar2,
     p_fecha_vencimiento in varchar2,
     p_eliminar in number); -- 1 elimina
     
     
   PROCEDURE SP_GUARDAR_COMERCIANTE_DOM(
    o_id_vin out number, 
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in number,
    p_id_tramiteipj_accion in number,
    p_id_integrante in number,
    p_cuit in varchar2, 
    p_id_vin in number,
    p_ID_PROVINCIA in varchar2,
    p_ID_DEPARTAMENTO in number,
    p_ID_LOCALIDAD in number,
    p_BARRIO in varchar2,
    p_CALLE in varchar2,
    p_ALTURA in number,
    p_DEPTO in varchar2,
    p_PISO in varchar2,
    p_torre in varchar2,
    p_manzana in varchar2,
    p_lote in varchar2,
    p_id_barrio in number,
    p_id_calle in number,
    p_id_tipocalle in number,
    p_km in varchar2
  );
    
  PROCEDURE SP_Traer_Comerc_Rub_Mecan(
     o_Cursor OUT TYPES.cursorType,
     p_id_tramite_ipj in number,
     p_id_tramiteipj_accion in number,
     p_id_tipo_accion in number,
     p_id_integrante in number
     );
     
  PROCEDURE SP_Traer_Comerc_Rubricas(
      o_Cursor OUT TYPES.cursorType,
      p_id_tramite_ipj in number,
      p_id_tramiteipj_accion in number,
      p_id_tipo_accion in number,
      p_id_integrante in number
      );
      
  PROCEDURE SP_Traer_Comerc_Rubricas_NO(
     o_Cursor OUT TYPES.cursorType,
     p_id_tramite_ipj in number,
     p_id_tramiteipj_accion in number,
     p_id_tipo_accion in number,
     p_id_integrante in number
     );
     
   PROCEDURE SP_GUARDAR_COMERC_RUBRICAS(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in number,
    p_id_tramiteipj_accion in number,
    p_id_tipo_accion in number,
    p_id_integrante in number,
    p_id_tipo_libro in number, 
    p_nro_libro in number,
    p_titulo in varchar2,
    p_fecha_tramite in varchar2, --es DATE
    p_Fs_Ps in varchar2,
    p_id_juzgado in number,
    p_sentencia in varchar2, 
    p_fecha_sent in varchar2, --es DATE 
    p_eliminar in number); -- 1 eliminar
    
  PROCEDURE SP_Traer_Comerc_Rubros_Jud(
    o_Cursor OUT types.cursorType,
    p_id_integrante in number
  );
  
   PROCEDURE SP_GUARDAR_COMERC_RUBROS_JUD(
     o_rdo out varchar2,
     o_tipo_mensaje out number,
     p_id_tramite_ipj in number,
     p_id_integrante in number,
     p_nro_rubro in number,
     p_rubro in varchar2,
     p_fecha_baja in varchar2,
     p_id_tramite_ipj_baja in number,
     p_eliminar in number -- 1 elimina
   );
   
  PROCEDURE SP_Listar_Comerciantes(
    o_Cursor OUT types.cursorType
  );
     
END Comerciantes;
/

create or replace package body ipj.Comerciantes is

  FUNCTION VALIDA_LIBRO_COMERCIANTE(p_TRAMITE_IPJ in number, p_id_tramiteipj_accion in number,  
    p_id_integrante in number, p_TIPO_LIBRO in number) return Number 
  IS
    res Number;
    v_cuit varchar2(11);
    v_max number;
    v_min number;
    v_total number;
  /**************************************************
    Valida que los libros rubricados de un comerciante sean consecutivos y sin saltos
    0 = OK y cualquier otro es error 
  ***************************************************/ 
  begin
    begin
      select max(CR.NRO_LIBRO), min(CR.NRO_LIBRO), count(CR.NRO_LIBRO) into v_max, v_min, v_total
      from IPJ.T_COMERCIANTES_RUBRICA cr
      where
        cr.id_integrante = p_id_integrante and
        cr.ID_TIPO_LIBRO = p_tipo_libro;
        
      res := nvl((v_max - v_min + 1) - v_total, 0);
    exception
      when OTHERS then res := 0;
    end;
    
    return res;
  exception
    when OTHERS then return 0;
  end VALIDA_LIBRO_COMERCIANTE;

   PROCEDURE SP_Traer_Comerciante(
    p_Cursor OUT types.cursorType,
    p_id_tramite_ipj in number,
    p_id_tramiteipj_accion in number,
    p_id_tipo_accion in number,
    p_id_integrante in number
    )
  is
    v_existe number;
    v_id_tramite_ipj number;
    v_id_tramiteipj_accion number;
    v_id_tipo_accion number;
    v_id_integrante number;
    v_hay_protocolo number;
  BEGIN
    -- busco si la accion usa protocolo, para saber si se incrementa o no la matricula y su versionado
    select nvl(AC.ID_PROTOCOLO, 0) into v_hay_protocolo
    from IPJ.T_TIPOS_ACCIONESIPJ ac
    where id_tipo_accion = p_id_tipo_accion;
    
    select count(*) into v_existe
    from IPJ.T_Comerciantes c
    where
      c.id_tramite_ipj = p_id_tramite_ipj and
      c.id_tramiteipj_accion = p_id_tramiteipj_accion and
      c.id_tipo_accion = p_id_tipo_accion and
      c.id_integrante = p_id_integrante;
      
    if v_existe > 0 then
      -- Si existe un registro para el comerciante lo muestro. 
      OPEN p_Cursor FOR
        select 
          C.A�O ano, To_Char(C.FEC_INSCRIPCION, 'dd/mm/rrrr') FEC_INSCRIPCION, C.FOLIO, C.ID_INTEGRANTE,
          C.ID_TIPO_ACCION, C.ID_TRAMITE_IPJ, C.ID_TRAMITEIPJ_ACCION, C.ID_VIN_COMERCIAL,
          C.ID_VIN_REAL,
          trim(TRANSLATE(C.MATRICULA, 'ABCDEF','      ')) MATRICULA, 
          trim(TRANSLATE(C.MATRICULA, '0123456789','          ')) Letra, 
          C.OBJETO_SOCIAL, C.PROTOCOLO, C.TOMO,
          I.CUIL,
          (select p.nombre||' '||p.apellido from rcivil.vt_pk_persona p where p.id_sexo = i.id_sexo and p.nro_documento = i.nro_documento and p.pai_cod_pais = i.pai_cod_pais and p.id_numero = i.id_numero) detalle,
          I.ID_NUMERO, I.ID_SEXO, I.NRO_DOCUMENTO, I.PAI_COD_PAIS,
          S.TIPO_SEXO,
          C.MATRICULA_VERSION
        from IPJ.t_comerciantes c join IPJ.t_integrantes i
            on C.ID_INTEGRANTE = I.ID_INTEGRANTE
          join T_COMUNES.VT_SEXOS s
            on I.ID_SEXO = S.ID_SEXO
        where
          c.id_tramite_ipj = p_id_tramite_ipj and
          c.id_tramiteipj_accion = p_id_tramiteipj_accion and
          c.id_tipo_accion = p_id_tipo_accion and
          c.id_integrante = p_id_integrante;
          
    else 
      -- No existe un registro para el comerciante, busco el ultimo existente.
      begin
        select id_tramite_ipj, id_tramiteipj_accion, id_tipo_accion, id_integrante 
          into v_id_tramite_ipj, v_id_tramiteipj_accion, v_id_tipo_accion, v_id_integrante
        from
          ( select c.*
            from ipj.t_comerciantes c 
            where
              c.id_integrante = p_id_integrante
            order by c.MATRICULA, c.MATRICULA_VERSION
           ) t
        where
          rownum = 1;
         
        OPEN p_Cursor FOR
          select 
            C.A�O ano, To_Char(C.FEC_INSCRIPCION, 'dd/mm/rrrr') FEC_INSCRIPCION, C.FOLIO, C.ID_INTEGRANTE,
            p_id_tipo_accion ID_TIPO_ACCION, p_id_tramite_ipj ID_TRAMITE_IPJ, p_id_tramiteipj_accion ID_TRAMITEIPJ_ACCION, 
            C.ID_VIN_COMERCIAL, C.ID_VIN_REAL,
            trim(TRANSLATE(C.MATRICULA, 'ABCDEF','      ')) MATRICULA, 
            trim(TRANSLATE(C.MATRICULA, '0123456789','          ')) Letra, 
            C.OBJETO_SOCIAL, C.PROTOCOLO, C.TOMO,
            I.CUIL,
            (select p.nombre||' '||p.apellido from rcivil.vt_pk_persona p where p.id_sexo = i.id_sexo and p.nro_documento = i.nro_documento and p.pai_cod_pais = i.pai_cod_pais and p.id_numero = i.id_numero) detalle,
            I.ID_NUMERO, I.ID_SEXO, I.NRO_DOCUMENTO, I.PAI_COD_PAIS,
            S.TIPO_SEXO,
            (case when v_hay_protocolo = 0 then C.MATRICULA_VERSION else C.MATRICULA_VERSION +1 end) MATRICULA_VERSION
          from IPJ.t_comerciantes c join IPJ.t_integrantes i
              on C.ID_INTEGRANTE = I.ID_INTEGRANTE
            join T_COMUNES.VT_SEXOS s
              on I.ID_SEXO = S.ID_SEXO
          where
            c.id_tramite_ipj = v_id_tramite_ipj and
            c.id_tramiteipj_accion = v_id_tramiteipj_accion and
            c.id_tipo_accion = v_id_tipo_accion and
            c.id_integrante = v_id_integrante;
      exception
        when OTHERS then
          OPEN p_Cursor FOR
            select 
              null ANO, null FEC_INSCRIPCION, null FOLIO, p_id_integrante ID_INTEGRANTE,
              p_id_tipo_accion ID_TIPO_ACCION, p_id_tramite_ipj ID_TRAMITE_IPJ, p_id_tramiteipj_accion ID_TRAMITEIPJ_ACCION, 
              null ID_VIN_COMERCIAL, null ID_VIN_REAL, '' MATRICULA, '' LETRA, 0 MATRICULA_VERSION, '' OBJETO_SOCIAL,
              null PROTOCOLO, null TOMO, I.CUIL,
              (select p.nombre||' '||p.apellido from rcivil.vt_pk_persona p where p.id_sexo = i.id_sexo and p.nro_documento = i.nro_documento and p.pai_cod_pais = i.pai_cod_pais and p.id_numero = i.id_numero) detalle,
              I.ID_NUMERO, I.ID_SEXO, I.NRO_DOCUMENTO, I.PAI_COD_PAIS,
              S.TIPO_SEXO
            from IPJ.t_integrantes i join T_COMUNES.VT_SEXOS s
                on I.ID_SEXO = S.ID_SEXO
            where
              i.id_integrante = p_id_integrante;
      end;
    end if;
    
  END SP_Traer_Comerciante;
  
  
   PROCEDURE SP_Traer_Comerciante_Rubros(
    p_Cursor OUT types.cursorType,
    p_id_tramite_ipj in number,
    p_id_tramiteipj_accion in number,
    p_id_tipo_accion in number,
    p_id_integrante in number)
  IS
  BEGIN
    OPEN p_Cursor FOR
      select R.ID_TRAMITE_IPJ, r.id_tramiteipj_accion, r.id_tipo_accion, r.id_integrante, 
        to_char(r.fecha_desde, 'dd/mm/rrrr') fecha_desde, to_char(r.fecha_hasta, 'dd/mm/rrrr') fecha_hasta, 
        r.id_rubro, ACT.N_RUBRO,  R.ID_TIPO_ACTIVIDAD, '' n_tipo_actividad,
        R.ID_ACTIVIDAD, ACT.N_ACTIVIDAD, r.borrador
      from ipj.t_comerciantes_rubros r join  t_comunes.vt_actividades act
        on r.id_actividad = act.id_actividad and r.id_rubro = act.id_rubro and r.id_tipo_actividad = act.id_tipo_actividad 
      where
        r.id_integrante = p_id_integrante and
        (r.borrador = 'N' or r.ID_TRAMITE_IPJ = p_id_tramite_ipj) and
        (r.fecha_hasta is null or r.fecha_hasta > sysdate or r.borrador = 'S') ;
  
  END SP_Traer_Comerciante_Rubros;
    
  PROCEDURE SP_Traer_Comerciante_InsLegal(
    p_Cursor OUT types.cursorType,
    p_id_tramite_ipj in number,
    p_id_tramiteipj_accion in number,
    p_id_tipo_accion in number,
    p_id_integrante in number)
  IS
  BEGIN
    OPEN p_Cursor FOR
      select
        id_tramite_ipj, id_tramiteipj_accion, id_tipo_accion, 
         id_integrante, id_juzgado, il_tipo, decode(il_numero, 0, null, il_numero) il_numero, il_fecha, 
         titulo, descripcion, borrador, fecha,
         n_juzgado, n_il_tipo,
         (case rownum when 1 then 1 else Imprimir end) imprimir
      from
        ( select 
            cil.ID_TRAMITE_IPJ, cil.ID_TRAMITEIPJ_ACCION, cil.ID_TIPO_ACCION, 
            cil.id_integrante, cil.ID_JUZGADO,
            cil.IL_TIPO, cil.IL_NUMERO, To_Char(cil.IL_FECHA, 'dd/mm/rrrr') IL_FECHA, 
            cil.TITULO, cil.DESCRIPCION, cil.BORRADOR, To_Char(cil.FECHA, 'dd/mm/rrrr') FECHA,
            J.N_JUZGADO, L.N_IL_TIPO, 0 imprimir
         from ipj.t_comerciantes_ins_legal cil join ipj.t_juzgados j
             on cIL.ID_JUZGADO = J.ID_JUZGADO
           join IPJ.T_TIPOS_INS_LEGAL L
             on cIL.IL_TIPO = L.IL_TIPO
         where
           cil.id_integrante = p_id_integrante and
           (cil.borrador = 'N' or cil.ID_TRAMITE_IPJ = p_id_tramite_ipj)
         order by cil.fecha desc, cIL.IL_FECHA desc
        ) t;
      
  END SP_Traer_Comerciante_InsLegal;
  
  PROCEDURE SP_Traer_Comerciante_Domicilio(
      p_Cursor OUT TYPES.cursorType,
      p_id_tramite_ipj in number,
      p_id_tramiteipj_accion in number,
      p_id_tipo_accion in number,
      p_id_integrante in number,      
      p_id_vin in number,
      p_cuil in varchar2)
  IS
   /********************************************************
      Este procemiento devuelve la direccion de la sede una entidad de un tramite
    *********************************************************/
    v_id_vin number;
  BEGIN
    -- Busco el domicilio de la persona
    select IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(id_sexo, nro_documento, pai_cod_pais, id_numero, 3) into v_id_vin
    from ipj.t_integrantes 
    where
      id_integrante = p_id_integrante;
  
    -- Busco el domicilio real de la perosna
    OPEN p_Cursor FOR
      select
        IPJ.VARIOS.FC_ARMAR_CALLE_DOM(d.n_calle, d.altura, d.piso, d.depto, d.torre, d.mzna, d.lote, d.n_barrio, d.km, d.n_tipocalle) calle_inf, 
        d.id_tipodom, d.n_tipodom, nvl(d.id_tipocalle, 9) id_tipocalle, nvl(d.n_tipocalle, 'CALLE') n_tipocalle, d.id_calle, 
        initcap(d.n_calle) n_calle, d.altura, d.depto, d.piso, d.torre, d.id_barrio, 
        initcap(d.n_barrio) n_barrio, /*d.nro_mail, d.cod_area,*/
        d.id_localidad, initcap(d.n_localidad) n_localidad, d.id_departamento, 
        initcap(d.n_departamento) n_departamento, d.id_provincia, 
        initcap(d.n_provincia) n_provincia, 
        IPJ.VARIOS.FC_Buscar_Codigo_Postal(d.id_localidad, d.id_barrio) cpa, 
        d.id_vin, D.mzna Manzana, D.Lote, d.km Km,
        IPJ.VARIOS.FC_Buscar_Codigo_AFIP(d.id_localidad, d.id_barrio) id_afip
      from dom_manager.VT_DOMICILIOS_COND d 
      where
        id_vin = v_id_vin;
      
  END SP_Traer_Comerciante_Domicilio;
  
  
  PROCEDURE SP_GUARDAR_COMERCIANTES(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_ID_TRAMITE_IPJ in NUMBER,
    p_ID_TRAMITEIPJ_ACCION  in NUMBER,
    p_ID_TIPO_ACCION  in NUMBER,
    p_ID_INTEGRANTE  in NUMBER,
    p_FEC_INSCRIPCION in varchar2,
    p_MATRICULA  in VARCHAR2,
    p_MATRICULA_VERSION  in NUMBER,
    p_FOLIO in VARCHAR2,
    p_TOMO in VARCHAR2,
    p_ANIO in NUMBER,
    p_PROTOCOLO in VARCHAR2,
    p_ID_VIN_REAL in NUMBER,
    p_ID_VIN_COMERCIAL in NUMBER,
    p_OBJETO_SOCIAL in VARCHAR2,
    p_CUIL in varchar2,
    p_letra in varchar2
  )
  IS
    v_mensaje varchar2(200);
    v_valid_parametros varchar2(500);
    v_matricula_exists varchar2(500);
    v_id_tramiteipj_accion number;
  BEGIN
  /********************************************************
    Guarda los datos de un Comerciante.
    Si el comerciante ya existe, los actualiza, en caso contrario los ingresa
    No maneja transaccion, ya que se usa en cascada desde la aplicacion y lo administra el codigo.
  *********************************************************/    
    --  VALIDACION DE PARAMETROS
    if p_FEC_INSCRIPCION is not null and IPJ.VARIOS.Valida_Fecha(p_FEC_INSCRIPCION) = 'N' or To_Date(p_FEC_INSCRIPCION, 'dd/mm/rrrr') > sysdate then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT')  || ' (Fecha Inscripci�n)'; 
    end if;
    if IPJ.VARIOS.TONUMBER(p_anio) is null then
      v_valid_parametros := v_valid_parametros || 'A�o Invalido.'; 
    end if;
    if p_matricula is not null then
      if IPJ.VARIOS.TONUMBER(p_matricula) is null then
        v_valid_parametros := 'Matricula Inv�lida.';
      else
        IPJ.VARIOS.SP_Validar_Matricula(
         o_rdo => v_matricula_exists,
         o_tipo_mensaje => o_tipo_mensaje,
         p_id_tramite_ipj => p_id_tramite_ipj,
         p_id_legajo => null,
         p_id_tramiteipj_accion => p_id_tramiteipj_accion,
         p_cuit => null,
         p_matricula => upper(p_letra) || p_matricula);
         
        if v_matricula_exists != IPJ.TYPES.C_RESP_OK then
          v_valid_parametros :=  v_valid_parametros || v_matricula_exists;
        end if;
      end if;
    end if;
    if p_CUIL is not null then
      t_comunes.pack_persona_juridica.VerificarCUIT (p_CUIL, v_mensaje);
      if Upper(trim(v_mensaje)) != IPJ.TYPES.C_RESP_OK then
        v_valid_parametros := v_valid_parametros || v_Mensaje || ' .';
      end if;
    end if;
    
    if nvl(p_id_integrante, 0) = 0then
       v_valid_parametros := v_valid_parametros || 'Persona Inv�lida';
    end if;
  
    -- Si los paramentros no estan OK, no continuo
    if v_valid_parametros is not null then
      o_rdo := 'Parametros : ' || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    --  CUERPO DEL PROCEDIMIETNO
    -- Actualizo el registro (si existe)
    update ipj.t_comerciantes 
    set
      fec_inscripcion = To_Date(p_FEC_INSCRIPCION, 'dd/mm/rrrr'),
      matricula = upper(p_letra) || p_MATRICULA,
      folio = p_FOLIO,
      tomo = p_TOMO,
      a�o = p_ANIO,
      protocolo = p_PROTOCOLO,
      id_vin_real = p_id_vin_real,
      id_vin_comercial = p_id_vin_comercial,
      objeto_social = p_objeto_social
    where 
      ID_TRAMITE_IPJ = p_id_tramite_ipj and
      id_tramiteipj_accion = p_ID_TRAMITEIPJ_ACCION and
      id_tipo_accion = p_ID_TIPO_ACCION and
      id_integrante = p_id_integrante; 
 
    -- Si no se actualizo nada, lo inserto.
    -- NO DEBERIA OCURRIR, VER SI INSERTO O MUESTRO ERROR
    if sql%rowcount = 0 then
      insert into ipj.t_comerciantes
        (ID_TRAMITE_IPJ, ID_TRAMITEIPJ_ACCION, ID_TIPO_ACCION, ID_INTEGRANTE,
          FEC_INSCRIPCION, MATRICULA, MATRICULA_VERSION, FOLIO, TOMO, A�O,
          PROTOCOLO, ID_VIN_REAL, ID_VIN_COMERCIAL, OBJETO_SOCIAL)
      values
        ( p_ID_TRAMITE_IPJ, p_ID_TRAMITEIPJ_ACCION, p_ID_TIPO_ACCION, p_ID_INTEGRANTE,
          to_date(p_FEC_INSCRIPCION, 'dd/mm/rrrr'), upper(p_letra) || p_MATRICULA, p_MATRICULA_VERSION, 
          p_FOLIO, p_TOMO, p_ANIO, p_PROTOCOLO, p_ID_VIN_REAL, p_ID_VIN_COMERCIAL, p_OBJETO_SOCIAL);
    end if;
    
    -- actualizo el CUIT si lo tiene
    update IPJ.T_Integrantes
    set cuil = p_cuil
    where 
      id_integrante = p_id_integrante and nvl(cuil, '-') <> nvl(p_cuil, '-');
        
    --actualizo el domicilio real de la persona si lo tiene
    update IPJ.T_Integrantes
    set id_vin = p_id_vin_real
    where 
      id_integrante = p_id_integrante and nvl(id_vin, 0) <> p_id_vin_real;
    
    if p_matricula is not null then
      IPJ.VARIOS.SP_Actualizar_Protocolo(
        o_rdo => o_rdo,
        o_tipo_mensaje => o_tipo_mensaje,
        p_id_tramite_ipj => p_id_tramite_ipj,
        p_id_legajo => null,
        p_id_tramiteipj_accion => p_id_tramiteipj_accion ,
        p_matricula => p_letra || p_matricula);
    end if;
    
    if nvl(o_rdo, IPJ.TYPES.C_RESP_OK) != IPJ.TYPES.C_RESP_OK then
      o_rdo := 'SP_Actualizar_Protocolo' || o_rdo;
      return;
    end if;
    
    --Una vez creada la entidad, marco la accion como EN PROCESO
    update ipj.t_tramitesipj_acciones
    set id_estado =3
    where
      id_tramite_ipj = p_id_tramite_ipj and
      id_tramiteipj_accion = p_id_tramiteipj_accion;

    o_rdo := IPJ.TYPES.C_RESP_OK;     
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION 
     WHEN OTHERS THEN 
       o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_GUARDAR_COMERCIANTES;
  
  
  PROCEDURE SP_GUARDAR_COMERCIANTES_IL(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_ID_TRAMITE_IPJ in NUMBER,
    p_ID_TRAMITEIPJ_ACCION in NUMBER,
    p_ID_TIPO_ACCION in NUMBER,
    p_ID_INTEGRANTE in NUMBER,
    p_ID_JUZGADO in NUMBER,
    p_IL_TIPO in NUMBER,
    p_IL_NUMERO in NUMBER,
    p_IL_FECHA in varchar2,
    p_TITULO in VARCHAR2,
    p_DESCRIPCION in VARCHAR2,
    p_FECHA in varchar2,
    p_eliminar in number)
  IS
    v_valid_parametros varchar2(500);
  BEGIN
    /*************************************************************
      Guarda los instrumentos legales asociados a un comerciante de un tramite.
      No maneja transaccion, ya que se usa en cascada desde la aplicacion y lo administra el codigo.
  *************************************************************/  
  -- VALIDACION DE PARAMETROS
    if IPJ.VARIOS.Valida_Fecha(p_il_fecha) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT' || '(Fecha Ins. Legal)'); 
    end if;
    if IPJ.VARIOS.Valida_Fecha(p_fecha) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT' || '(Fecha Insc.)'); 
    end if;
    if IPJ.VARIOS.Valida_Tipo_Ins_Legal(nvl(p_il_tipo, 0)) = 0 then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('INS_LEGAL_NOT'); 
    end if;
  
    if v_valid_parametros is not null then
      o_rdo := 'Parametros: '  || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    -- CUERPO PROCEDIMIENTO
    if p_eliminar = 1 then
      delete ipj.t_comerciantes_ins_legal
      where
        id_tramite_ipj = p_id_tramite_ipj and
        id_tramiteipj_accion = p_id_tramiteipj_accion and
        id_tipo_accion = p_id_tipo_accion and
        id_integrante = p_id_integrante and
        id_juzgado = p_id_juzgado and
        il_tipo = p_il_tipo and
        il_numero = p_il_numero and
        borrador = 'S';
        
    else
      -- Si existe un registro igual no hago nada
      update ipj.t_comerciantes_ins_legal
      set 
         titulo = p_TITULO,
         descripcion = p_DESCRIPCION,
         fecha = to_date(p_FECHA, 'dd/mm/rrrr')
      where
        id_tramite_ipj = p_id_tramite_ipj and
        id_tramiteipj_accion = p_id_tramiteipj_accion and
        id_tipo_accion = p_id_tipo_accion and
        id_integrante = p_id_integrante and
        id_juzgado = p_id_juzgado and
        il_tipo = p_il_tipo and
        il_numero = p_il_numero;
    
      if sql%rowcount = 0 then
        insert into ipj.t_comerciantes_ins_legal
          ( ID_TRAMITE_IPJ, ID_TRAMITEIPJ_ACCION, ID_TIPO_ACCION, ID_INTEGRANTE, ID_JUZGADO,
            IL_TIPO, IL_NUMERO, IL_FECHA, TITULO, DESCRIPCION, BORRADOR, FECHA)
        values
          (p_ID_TRAMITE_IPJ, p_ID_TRAMITEIPJ_ACCION, p_ID_TIPO_ACCION, p_ID_INTEGRANTE, p_ID_JUZGADO,
            p_IL_TIPO, p_IL_NUMERO, to_date(p_IL_FECHA, 'dd/mm/rrrr'),
            p_TITULO, p_DESCRIPCION, 'S', to_date(p_FECHA, 'dd/mm/rrrr'));
      end if;
    end if;
         
    o_rdo := 'OK';
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION 
     WHEN OTHERS THEN 
       o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_GUARDAR_COMERCIANTES_IL;
  
  
  PROCEDURE SP_GUARDAR_COMERCIANTES_RUBROS(
     o_rdo out varchar2,
     o_tipo_mensaje out number,
     p_id_tramite_ipj in number,
     p_id_tramiteipj_accion in number,
     p_id_tipo_accion in number,
     p_id_integrante in number,
     p_id_rubro in varchar2,
     p_id_tipo_actividad in varchar2,
     p_id_actividad in varchar2,
     p_fecha_inicio in varchar2,
     p_fecha_vencimiento in varchar2,
     p_eliminar in number) -- 1 elimina
  IS
    v_actividad_existente varchar2(20) := null;
    v_id_actividad varchar2(20) := null;
    v_id_rubro varchar2(3) := null;
    v_id_tipo_actividad varchar2(3) := null;
    v_valid_parametros varchar2(200);
    v_bloque varchar2(100);
    v_cuit varchar2(11);
  BEGIN
  /***********************************************************
    Guarda los rubros y actividades asociados a un comerciante de un tramite
    Si se utiliza solo Rubro y Tipo_Actividades, se guarda la primera actividad de la vista.
    Luego el programa se supone que no muestra ese nivel.
    Si lo utilizan, siempre viene por parametro y se utiliza normalmente.
    No maneja transaccion, ya que se usa en cascada desde la aplicacion y lo administra el codigo.
  ************************************************************/    
    if IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('HAB_LOG_COMERCIANTE') = '1' then
      IPJ.VARIOS.SP_GUARDAR_LOG(
        p_METODO => 'SP_GUARDAR_COMERCIANTES_RUBROS',
        p_NIVEL => 'DB - Gesti�n',
        p_ORIGEN => 'DB',
        p_USR_LOG  => 'DB',
        p_USR_WINDOWS => 'Sistema',
        p_IP_PC  => null,
        p_EXCEPCION =>
          ' Id Tramite IPJ = ' || to_char(p_id_tramite_ipj)
         || ' / Id Tramitesipj Accion = ' || to_char(p_id_tramiteipj_accion)
         || ' / Id Tipo Accion = ' || to_char(p_id_tipo_accion)
         || ' / ID Integrante = ' || to_char(p_id_integrante)
         || ' / Id Rubro = ' || p_id_rubro
         || ' / ID Tipo Actividad = ' || p_id_tipo_actividad
         || ' / Id Actividad = ' || p_id_actividad
         || ' / Fecha Inicio = ' || p_fecha_inicio
         || ' / Fecha Venc. = ' || p_fecha_vencimiento
         || ' / Eliminar = ' || to_char(p_eliminar) 
      );
    end if;
  
    -- VALIDACION DE PARAMETROS
    v_bloque := 'GUARDAR RUBROS - Par�metros: ';
    if IPJ.VARIOS.Valida_Integrante(p_id_integrante) = 0 then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('INT_NOT')|| '(' || to_char(p_id_tramite_ipj) ||' - ' || to_char(p_id_integrante)||')';
    end if;
    if p_id_actividad is null then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('RUBRO_NOT');
    end if;
    if IPJ.VARIOS.Valida_Fecha(p_fecha_inicio) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT');
    end if;
    if p_fecha_vencimiento is not null and IPJ.VARIOS.Valida_Fecha(p_fecha_vencimiento) = 'N' then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT');
    end if;
  
    -- si los parametros no estan OK, no continuo  
    if v_valid_parametros is not null then
      o_rdo := v_bloque || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;

    -- CUERPO DEL PROCEDIMIENTO
    if p_eliminar = 1 then
      delete ipj.t_comerciantes_rubros
      where
        id_tramite_ipj = p_id_tramite_ipj and
        id_tramiteipj_Accion = p_id_tramiteipj_accion and
        id_tipo_Accion = p_id_tipo_accion and
        id_integrante = p_id_integrante and
        id_rubro = p_id_rubro and
        id_tipo_actividad = p_id_tipo_actividad and
        id_actividad = p_id_actividad and
        borrador = 'S';
        
    else
      v_bloque := '(Rubros/Actividades) ';
      -- Verifico si ya esta cargado o no.      
      select nvl(max(id_actividad), '-1') into v_actividad_existente
      from ipj.t_comerciantes_rubros cr 
      where
        cr.id_integrante = p_id_integrante and
        cr.id_rubro = p_id_rubro and
        cr.id_tipo_actividad = p_id_tipo_actividad and
        cr.id_actividad = p_id_actividad;
    
      -- Si solo viene e codigo de Actividad, busco el rubto y tipo asociados
      if p_id_actividad is not null  and p_id_rubro is null and p_id_tipo_actividad is null then 
        select id_rubro into v_id_rubro
        from t_comunes.vt_actividades a
        where
          a.id_actividad = p_id_actividad;
        
        select id_tipo_actividad into v_id_tipo_actividad
        from t_comunes.vt_actividades a
        where
          a.id_actividad = p_id_actividad;
      else
        v_id_rubro := p_id_rubro;
        v_id_tipo_actividad :=   p_id_tipo_actividad;
      end if;
    
      -- si no viene actividad, busco la primera del tipo dado
      if p_id_actividad is null then 
        select id_actividad into v_id_actividad
        from t_comunes.vt_actividades a
        where
          a.ID_RUBRO = p_id_rubro and
          a.ID_TIPO_ACTIVIDAD = p_id_tipo_actividad and
          rownum = 1;
      else
        v_id_actividad := p_id_actividad;
      end if;
 
      -- Actualizo la fecha de baja
      update ipj.t_comerciantes_rubros
      set 
        fecha_hasta = to_date(p_fecha_vencimiento, 'dd/mm/rrrr')
      where
        id_tramite_ipj = p_id_tramite_ipj and
        id_tramiteipj_accion = p_id_tramiteipj_accion and
        id_tipo_accion = p_id_tipo_accion and
        id_integrante = p_id_integrante and
        id_rubro = v_id_rubro and
        id_tipo_actividad = v_id_tipo_actividad and
        id_actividad = nvl(p_id_actividad, v_actividad_existente);
  
      -- si no existe, lo agrego
      if sql%rowcount = 0 then
        insert into ipj.t_comerciantes_rubros
          (fecha_desde, fecha_hasta, id_rubro, id_tipo_actividad, id_actividad, ID_TRAMITE_IPJ, id_tramiteipj_accion,
          id_tipo_accion, id_integrante, borrador)
        values
          (To_Date(p_fecha_inicio, 'dd/mm/rrrr'), to_date(p_fecha_vencimiento, 'dd/mm/rrrr'),
           v_id_rubro, v_id_tipo_actividad, nvl(p_id_actividad, v_id_actividad), p_id_tramite_ipj, 
           p_id_tramiteipj_accion, p_id_tipo_accion, p_id_integrante, 'S');
      end if;
    end if;
          
    if o_rdo is null then
      o_rdo := 'OK';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    end if;
  EXCEPTION 
    WHEN OTHERS THEN 
      o_rdo := v_bloque || To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_GUARDAR_COMERCIANTES_RUBROS;
  
  
  PROCEDURE SP_GUARDAR_COMERCIANTE_DOM(
    o_id_vin out number, 
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in number,
    p_id_tramiteipj_accion in number,
    p_id_integrante in number,
    p_cuit in varchar2, 
    p_id_vin in number,
    p_ID_PROVINCIA in varchar2,
    p_ID_DEPARTAMENTO in number,
    p_ID_LOCALIDAD in number,
    p_BARRIO in varchar2,
    p_CALLE in varchar2,
    p_ALTURA in number,
    p_DEPTO in varchar2,
    p_PISO in varchar2,
    p_torre in varchar2,
    p_manzana in varchar2,
    p_lote in varchar2,
    p_id_barrio in number,
    p_id_calle in number,
    p_id_tipocalle in number,
    p_km in varchar2
   )
  IS
    v_valid_parametros varchar2(500);
    v_mensaje varchar2(500);
    v_TIPO_DOM  NUMBER;
    v_N_TIPO_DOM  VARCHAR2(150);
    v_bloque varchar(200);
    v_usuario varchar2(20);
  BEGIN
  /*************************************************************
    Inserta o Actualiza un domicilio para una empresa en DOM_MANAGER
    No maneja transaccion, ya lo hace DOM_MANAGER
  *************************************************************/
    if IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('HAB_LOG_COMERCIANTE') = '1' then
      IPJ.VARIOS.SP_GUARDAR_LOG(
        p_METODO => 'SP_GUARDAR_COMERCIANTE_DOM',
        p_NIVEL => 'DB - Gesti�n',
        p_ORIGEN => 'DB',
        p_USR_LOG  => 'DB',
        p_USR_WINDOWS => 'Sistema',
        p_IP_PC  => null,
        p_EXCEPCION =>
          ' Id Tramite IPJ = ' || to_char(p_id_tramite_ipj)
         || ' / Id Tramitesipj Accion = ' || to_char(p_id_tramiteipj_accion)
         || ' / Id Integrante = ' || to_char(p_id_integrante)
         || ' / Cuit = ' || p_cuit 
         || ' / Id Vin = ' || to_char(p_id_vin)
         || ' / ID Provincia = ' || to_char(p_ID_PROVINCIA)
         || ' / Id Departamneto = ' || to_char(p_ID_DEPARTAMENTO)
         || ' / Id Localidad = ' || to_char(p_ID_LOCALIDAD)
         || ' / Barrio = ' || p_BARRIO
         || ' / Calle = ' || p_CALLE
         || ' / Altura = ' || to_char(p_ALTURA)
         || ' / Depto = ' || p_DEPTO
         || ' / Piso = ' || p_PISO
         || ' / Torre = ' || p_torre
         || ' / Manzana = ' || p_manzana
         || ' / Lote = ' || p_lote
         || ' / Id Barrio = ' || to_char(p_id_barrio)
         || ' / Id Calle = ' || to_char(p_id_calle)
         || ' / ID Tipo Calle = ' || to_char(p_id_tipocalle)
         || ' / Km = ' || p_km 
      );
    end if;
  
    -- Busco el responsable de la primer accion
    select cuil_usuario into v_usuario 
    from ipj.t_tramitesipj_acciones 
    where 
      id_tramite_ipj = p_id_tramite_ipj and 
      id_estado < 100 and
      rownum = 1;
        
    if nvl(p_id_departamento, 0) <> 0 then 
      IPJ.VARIOS.SP_GUARDAR_DOMICILIO(
        o_id_vin => o_id_vin, 
        o_rdo => o_rdo,
        o_tipo_mensaje => o_tipo_mensaje,
        p_id_vin => p_id_vin,
        p_usuario => v_usuario,
        p_Tipo_Domicilio => 3, -- 3 Real //  0 Sin Asignar
        p_id_integrante => p_id_integrante,
        p_id_legajo => 0,
        p_cuit_empresa => null,
        p_id_fondo_comercio => 0,  
        p_id_entidad_acta => 0,
        p_es_comerciante => 'N',
        p_es_admin_entidad => 'N',
        P_ID_PROVINCIA => p_id_provincia,
        P_ID_DEPARTAMENTO => p_id_departamento,
        P_ID_LOCALIDAD => p_id_localidad,
        P_BARRIO => p_barrio,
        P_CALLE => p_calle,
        P_ALTURA => p_altura,
        P_DEPTO => p_depto,
        P_PISO => p_piso,
        p_torre => p_torre,
        p_manzana => p_manzana,
        p_lote => p_lote,
        p_id_barrio => p_id_barrio,
        p_id_calle => p_id_calle,
        p_id_tipocalle => p_id_tipocalle,
        p_km => p_km
      );
    else
      o_rdo := IPJ.TYPES.C_RESP_OK;
      o_id_vin := 0;
    end if;
     
    if Upper(o_rdo) = IPJ.TYPES.C_RESP_OK then
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    else
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
    end if;
     
  EXCEPTION 
     WHEN OTHERS THEN 
        o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_GUARDAR_COMERCIANTE_DOM;
  
  
   PROCEDURE SP_Traer_Comerc_Rubricas(
      o_Cursor OUT TYPES.cursorType,
      p_id_tramite_ipj in number,
      p_id_tramiteipj_accion in number,
      p_id_tipo_accion in number,
      p_id_integrante in number
      )
  IS
  BEGIN
  /**********************************************************
  Lista las rubricas de un comerciante
  **********************************************************/
    --Muestro primero los ultimos libros de cada tipo, y luego los demas
    OPEN o_Cursor FOR
    select ID_TRAMITE_IPJ, id_tipo_libro, TIPO_LIBRO, nro_libro, titulo, fecha_tramite, 
      FD_PS, id_juzgado, sentencia, fecha_sent, borrador,
      N_JUZGADO, ID_TRAMITEIPJ_ACCION, ID_TIPO_ACCION, ID_INTEGRANTE
    from (
      Select CR.BORRADOR, CR.FD_PS, to_char(CR.FECHA_SENT, 'dd/mm/rrrr') FECHA_SENT, 
        to_char(CR.FECHA_TRAMITE, 'dd/mm/rrrr') FECHA_TRAMITE, 
        CR.ID_JUZGADO, CR.ID_TIPO_LIBRO, CR.ID_TRAMITE_IPJ, 
        CR.ID_TRAMITEIPJ_ACCION, CR.ID_TIPO_ACCION, CR.ID_INTEGRANTE, 
        CR.NRO_LIBRO, CR.SENTENCIA, CR.TITULO , 
        1 orden, TL.TIPO_LIBRO, J.N_JUZGADO
      from IPJ.T_COMERCIANTES_RUBRICA CR join IPJ.T_TIPOS_LIBROS tl
          on CR.ID_TIPO_LIBRO = tl.id_tipo_libro
        join IPJ.T_JUZGADOS j 
          on CR.ID_JUZGADO = J.id_JUZGADO
        join 
          ( select ID_TIPO_libro, max(nro_libro) Maximo 
            from IPJ.T_COMERCIANTES_RUBRICA cr 
            where 
              cr.id_integrante = p_id_integrante
            group by id_tipo_libro
          ) crm
          on cr.id_tipo_libro = crm.id_tipo_libro and cr.nro_libro = crm.maximo
        where
          cr.id_integrante = p_id_integrante and
          TL.MECANIZADO = 'N' 
    union
    Select CR.BORRADOR, CR.FD_PS, to_char(CR.FECHA_SENT, 'dd/mm/rrrr') FECHA_SENT, 
        to_char(CR.FECHA_TRAMITE, 'dd/mm/rrrr') FECHA_TRAMITE, 
        CR.ID_JUZGADO, CR.ID_TIPO_LIBRO, CR.ID_TRAMITE_IPJ, 
        CR.ID_TRAMITEIPJ_ACCION, CR.ID_TIPO_ACCION, CR.ID_INTEGRANTE,
        CR.NRO_LIBRO, CR.SENTENCIA, CR.TITULO ,
         2 orden, TL.TIPO_LIBRO, J.N_JUZGADO
    from IPJ.T_COMERCIANTES_RUBRICA CR join IPJ.T_TIPOS_LIBROS tl
          on CR.ID_TIPO_LIBRO = tl.id_tipo_libro
        join IPJ.T_JUZGADOS j 
          on CR.ID_JUZGADO = J.id_JUZGADO
        join 
          (select ID_TIPO_libro, max(nro_libro) Maximo 
           from IPJ.T_COMERCIANTES_RUBRICA cr 
            where 
              cr.id_integrante = p_id_integrante
           group by id_tipo_libro
        ) crm
          on cr.id_tipo_libro = crm.id_tipo_libro and cr.nro_libro < crm.maximo
    where
      cr.id_integrante = p_id_integrante and
      TL.MECANIZADO = 'N' 
    ) rub
    where
      borrador = 'N' or id_tramite_ipj = p_id_tramite_ipj
    order by orden asc, id_tipo_libro asc;
  END SP_Traer_Comerc_Rubricas;
  
  
  
  PROCEDURE SP_Traer_Comerc_Rub_Mecan(
     o_Cursor OUT TYPES.cursorType,
     p_id_tramite_ipj in number,
     p_id_tramiteipj_accion in number,
     p_id_tipo_accion in number,
     p_id_integrante in number
     )
  IS
  BEGIN
  /*****************************************************************
  Lista los medios mecanizados de un comerciante
  ******************************************************************/    
    --Muestro primero los ultimos libros de cada tipo, y luego los demas
    OPEN o_Cursor FOR
    select ID_TRAMITE_IPJ , id_tipo_libro, TIPO_LIBRO, nro_libro, titulo, fecha_tramite, 
      FD_PS, id_juzgado, sentencia, fecha_sent, borrador,
      N_JUZGADO, ID_TRAMITEIPJ_ACCION, ID_TIPO_ACCION, ID_INTEGRANTE
    from (
      Select CR.BORRADOR, CR.FD_PS, to_char(CR.FECHA_SENT, 'dd/mm/rrrr') FECHA_SENT, 
        to_char(CR.FECHA_TRAMITE, 'dd/mm/rrrr') FECHA_TRAMITE, 
        CR.ID_JUZGADO, CR.ID_TIPO_LIBRO, CR.ID_TRAMITE_IPJ, 
        CR.ID_TRAMITEIPJ_ACCION, CR.ID_TIPO_ACCION, CR.ID_INTEGRANTE, 
        CR.NRO_LIBRO, CR.SENTENCIA, CR.TITULO , 
        1 orden, TL.TIPO_LIBRO, J.N_JUZGADO
      from IPJ.T_COMERCIANTES_RUBRICA CR join IPJ.T_TIPOS_LIBROS tl
          on CR.ID_TIPO_LIBRO = tl.id_tipo_libro
        join IPJ.T_JUZGADOS j 
          on CR.ID_JUZGADO = J.id_JUZGADO
        join 
          ( select ID_TIPO_libro, max(nro_libro) Maximo 
            from IPJ.T_COMERCIANTES_RUBRICA cr
            where 
              cr.id_integrante = p_id_integrante
            group by id_tipo_libro
          ) crm
          on cr.id_tipo_libro = crm.id_tipo_libro and cr.nro_libro = crm.maximo
        where
          cr.id_integrante = p_id_integrante and
          TL.MECANIZADO = 'S' 
    union
    Select CR.BORRADOR, CR.FD_PS, to_char(CR.FECHA_SENT, 'dd/mm/rrrr') FECHA_SENT, 
        to_char(CR.FECHA_TRAMITE, 'dd/mm/rrrr') FECHA_TRAMITE, 
        CR.ID_JUZGADO, CR.ID_TIPO_LIBRO, CR.ID_TRAMITE_IPJ, 
        CR.ID_TRAMITEIPJ_ACCION, CR.ID_TIPO_ACCION, CR.ID_INTEGRANTE, 
        CR.NRO_LIBRO, CR.SENTENCIA, CR.TITULO ,
         2 orden, TL.TIPO_LIBRO, J.N_JUZGADO
    from IPJ.T_COMERCIANTES_RUBRICA CR join IPJ.T_TIPOS_LIBROS tl
          on CR.ID_TIPO_LIBRO = tl.id_tipo_libro
        join IPJ.T_JUZGADOS j 
          on CR.ID_JUZGADO = J.id_JUZGADO
        join 
          (select ID_TIPO_libro, max(nro_libro) Maximo 
           from IPJ.T_COMERCIANTES_RUBRICA cr
            where 
              cr.id_integrante = p_id_integrante
           group by id_tipo_libro
        ) crm
          on cr.id_tipo_libro = crm.id_tipo_libro and cr.nro_libro < crm.maximo
    where
      cr.id_integrante = p_id_integrante and
      TL.MECANIZADO = 'S' 
    ) rub
    where
      borrador = 'N' or id_tramite_ipj = p_id_tramite_ipj
    order by orden asc, id_tipo_libro asc;
  END SP_Traer_Comerc_Rub_Mecan;
  
  
  
  PROCEDURE SP_Traer_Comerc_Rubricas_NO(
     o_Cursor OUT TYPES.cursorType,
     p_id_tramite_ipj in number,
     p_id_tramiteipj_accion in number,
     p_id_tipo_accion in number,
     p_id_integrante in number
     )
  IS
  BEGIN
  /*****************************************************************
  Lista los libros de r�bricas no utilizados por un comerciante
  ******************************************************************/
    OPEN o_Cursor FOR
    select T.ID_TIPO_LIBRO, T.TIPO_LIBRO, '' Titulo
    from IPJ.t_tipos_libros t
    where 
      id_tipo_libro not in (
        select id_tipo_libro
        from IPJ.t_comerciantes_rubrica cr 
        where 
          cr.id_integrante = p_id_integrante);
    
  END SP_Traer_Comerc_Rubricas_NO;




PROCEDURE SP_GUARDAR_COMERC_RUBRICAS(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in number,
    p_id_tramiteipj_accion in number,
    p_id_tipo_accion in number,
    p_id_integrante in number,
    p_id_tipo_libro in number, 
    p_nro_libro in number,
    p_titulo in varchar2,
    p_fecha_tramite in varchar2, --es DATE
    p_Fs_Ps in varchar2,
    p_id_juzgado in number,
    p_sentencia in varchar2, 
    p_fecha_sent in varchar2, --es DATE 
    p_eliminar in number) -- 1 eliminar
  IS
    v_valid_parametros varchar2(500);
  BEGIN
    /*********************************************************
      Guarda las rubricas y medios mecanizados de un Comerciante
      No maneja transaccion, ya que se usa en cascada desde la aplicacion y lo administra el codigo.
  **********************************************************/  
    if IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('HAB_LOG_COMERCIANTE') = '1' then
      IPJ.VARIOS.SP_GUARDAR_LOG(
        p_METODO => 'SP_GUARDAR_COMERC_RUBRICAS',
        p_NIVEL => 'DB - Gesti�n',
        p_ORIGEN => 'DB',
        p_USR_LOG  => 'DB',
        p_USR_WINDOWS => 'Sistema',
        p_IP_PC  => null,
        p_EXCEPCION =>
          ' Id Tramite IPJ = ' || to_char(p_id_tramite_ipj)
         || ' / Id Tramite Accion = ' || to_char(p_id_tramiteipj_accion)
         || ' / Id Tipo Accion = ' || to_char(p_id_tipo_accion)
         || ' / Id Integrante = ' || to_char(p_id_integrante) 
         || ' / Id Tipo Libro = ' || to_char(p_id_tipo_libro) 
         || ' / Nro Libro = ' || to_char(p_nro_libro)
         || ' / Titulo = ' || p_titulo
         || ' / Fecha Tramite = ' || p_fecha_tramite
         || ' / Fs As = ' || p_Fs_Ps
         || ' / Id Juzgado = ' || to_char(p_id_juzgado)
         || ' / Sentencia = ' || p_sentencia 
         || ' / Fecha Sent = ' || p_fecha_sent 
         || ' / Eliminar = ' || to_char(p_eliminar) 
      );
    end if;
  
  -- VALIDACION DE PARAMETROS
    if IPJ.VARIOS.VALIDA_INTEGRANTE(nvl(p_id_integrante, 0)) = 0 then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('INT_NOT'); 
    end if;
    if p_id_tipo_libro is null then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('LIBRO_NOT'); 
    end if;
    if p_nro_libro is null then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('NRO_NOT'); 
    end if;
    if p_Fs_Ps is null then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FOLIO_NOT'); 
    end if;
    if p_fecha_tramite is not null and IPJ.VARIOS.Valida_Fecha(p_fecha_tramite) = 'N'  then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT') || ' (' || p_fecha_tramite || ')'; 
    end if;
    if p_fecha_sent is not null and IPJ.VARIOS.Valida_Fecha(p_fecha_sent) = 'N'  then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT') || ' (' || p_fecha_sent || ')'; 
    end if;
    
    if v_valid_parametros is not null then
      o_rdo := 'GUARDAR RUBRICA - Parametros: '  || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;
    
    -- CUERPO PROCEDIMIENTO
    if p_eliminar = 1 then
      delete ipj.t_comerciantes_rubrica
      where
        id_tramite_ipj = p_id_tramite_ipj and
        id_tramiteipj_accion = p_id_tramiteipj_accion and
        id_tipo_accion = p_id_tipo_accion and
        id_integrante = p_id_integrante and
        id_tipo_libro = p_id_tipo_libro and 
        nro_libro = p_nro_libro and
        borrador = 'S';
    else
      -- Solo permito actualizar algunos datos
      update ipj.t_comerciantes_rubrica
      set 
        fecha_tramite = to_date(p_fecha_tramite, 'dd/mm/rrrr'),
        fd_ps = p_Fs_Ps
      where
        id_tramite_ipj = p_id_tramite_ipj and
        id_tramiteipj_accion = p_id_tramiteipj_accion and
        id_tipo_accion = p_id_tipo_accion and
        id_integrante = p_id_integrante and
        id_tipo_libro = p_id_tipo_libro and 
        nro_libro = p_nro_libro;
    
      -- Si no se actualizo nada, no existia, entonces lo inserto
      if sql%rowcount = 0 then
        insert into ipj.t_comerciantes_rubrica 
          (Id_Tramite_Ipj, Id_Tramiteipj_Accion, Id_Tipo_Accion, Id_Integrante, Id_Tipo_Libro, 
           Nro_Libro, Titulo, Fecha_Tramite, Fd_Ps, Id_Juzgado, Sentencia, Fecha_Sent, Borrador)
        values
          (p_Id_Tramite_Ipj, p_Id_Tramiteipj_Accion, p_Id_Tipo_Accion, p_Id_Integrante, p_Id_Tipo_Libro, 
           p_Nro_Libro, p_Titulo, to_date(p_Fecha_Tramite, 'dd/mm/rrrr'), p_Fs_Ps, p_Id_Juzgado, p_Sentencia, to_date(p_Fecha_Sent, 'dd/mm/rrrr'), 'S');
      end if;
    end if;
   
    if 0 = VALIDA_LIBRO_COMERCIANTE(p_ID_TRAMITE_IPJ, p_id_tramiteipj_accion, p_id_integrante, p_id_tipo_libro) then
      o_rdo := IPJ.TYPES.C_RESP_OK;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
    else
      o_rdo := IPJ.VARIOS.MENSAJE_ERROR('RUBRICA_NOT');
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
    end if;
    
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION 
     WHEN OTHERS THEN 
       o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_GUARDAR_COMERC_RUBRICAS;
  
  PROCEDURE SP_Traer_Comerc_Rubros_Jud(
    o_Cursor OUT types.cursorType,
    p_id_integrante in number)
  IS
    -- Lista los Rubros definidos en la sentencia para el comerciante
  BEGIN
    -- Listo todos los rubos, sin importar si estan dado de baja
    OPEN o_Cursor FOR
      select id_tramite_ipj, id_integrante, nro_rubro, rubro, 
        to_char(fecha_baja, 'dd/mm/rrrr') fecha_baja, id_tramite_ipj_baja
      from T_CORMERC_RUBROS_JUDICIAL rl  
      where
        rl.id_integrante = p_id_integrante;
  
  END SP_Traer_Comerc_Rubros_Jud;
  
  PROCEDURE SP_GUARDAR_COMERC_RUBROS_JUD(
     o_rdo out varchar2,
     o_tipo_mensaje out number,
     p_id_tramite_ipj in number,
     p_id_integrante in number,
     p_nro_rubro in number,
     p_rubro in varchar2,
     p_fecha_baja in varchar2,
     p_id_tramite_ipj_baja in number,
     p_eliminar in number -- 1 elimina
   )
  IS
    v_proximo_rubro number;
    v_id_tramite_ipj_baja number;
  BEGIN
    if IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('HAB_LOG_COMERCIANTE') = '1' then
      IPJ.VARIOS.SP_GUARDAR_LOG(
        p_METODO => 'SP_GUARDAR_COMERC_RUBROS_JUD',
        p_NIVEL => 'DB - Gesti�n',
        p_ORIGEN => 'DB',
        p_USR_LOG  => 'DB',
        p_USR_WINDOWS => 'Sistema',
        p_IP_PC  => null,
        p_EXCEPCION =>
          ' Id Tramite IPJ = ' || to_char(p_id_tramite_ipj)
         || ' / Id Integrante = ' || to_char(p_id_integrante)
         || ' / Nro Rubro = ' || to_char(p_nro_rubro)
         || ' / Rubro = ' || p_rubro
         || ' / Fecha Baja = ' || p_fecha_baja
         || ' / Id Tramite IPJ Baja = ' || to_char(p_id_tramite_ipj_baja)
         || ' / Eliminar = ' || to_char(p_eliminar) 
      );
    end if;
    
    -- CUERPO DEL PROCEDIMIENTO
    if nvl(p_eliminar, 0) = 1 then
      -- Elimino el registro indicado
      delete IPJ.T_CORMERC_RUBROS_JUDICIAL
      where
        id_tramite_ipj = p_id_tramite_ipj and
        id_integrante = p_id_integrante and
        nro_rubro = p_nro_rubro;
        
    else
      if nvl(p_id_tramite_ipj_baja, 0) <> 0 then
        -- Si viene un tr�mite de baja, solo marco la baja
        update IPJ.T_CORMERC_RUBROS_JUDICIAL
        set 
          fecha_baja = to_date(p_fecha_baja, 'dd/mm/rrrr'),
          id_tramite_ipj_baja = p_id_tramite_ipj_baja
        where
          id_tramite_ipj = p_id_tramite_ipj and
          id_integrante = p_id_integrante and
          nro_rubro = p_nro_rubro;
        
      else
        -- Actualizo el rubo si existe
        update IPJ.T_CORMERC_RUBROS_JUDICIAL
        set 
          rubro = p_rubro
        where
          id_tramite_ipj = p_id_tramite_ipj and
          id_integrante = p_id_integrante and
          nro_rubro = p_nro_rubro;
    
        -- si no existe, lo agrego
        if sql%rowcount = 0 then
          -- Busco el proximo numero
          select count(1) + 1 into v_proximo_rubro
          from IPJ.T_CORMERC_RUBROS_JUDICIAL
          where
            id_tramite_ipj = p_id_tramite_ipj and
            id_integrante = p_id_integrante;
         
          -- Inserto un nuevo rubro
          insert into ipj.T_CORMERC_RUBROS_JUDICIAL
            (id_tramite_ipj, id_integrante, nro_rubro, rubro, fecha_baja, id_tramite_ipj_baja)
          values
            (p_id_tramite_ipj, p_id_integrante, v_proximo_rubro, p_rubro, 
            to_date(p_fecha_baja, 'dd/mm/rrrr'), decode(p_id_tramite_ipj_baja, 0, null, p_id_tramite_ipj_baja));
        end if;
      end if;
    end if;
          
    o_rdo := 'OK';
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION 
    WHEN OTHERS THEN 
      o_rdo := 'ERROR  SP_GUARDAR_COMERC_RUBROS_JUD: ' || To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_GUARDAR_COMERC_RUBROS_JUD;
  
  PROCEDURE SP_Listar_Comerciantes(
    o_Cursor OUT types.cursorType
    )
  IS
  /* Este procedimiento lista todos los Comerciantes registrados en tr�mites cerrados OK */
  BEGIN
    OPEN o_Cursor FOR
      select p.nro_documento , p.cuil , p.nombre || ' ' || p.apellido Nombre, 
        IPJ.VARIOS.FC_LETRA_MATRICULA(tmp.matricula) || '-' || IPJ.VARIOS.FC_NUMERO_MATRICULA(tmp.matricula) Matricula, matricula_version,
        ( select count(1) 
          from ipj.t_comerciantes c join ipj.t_tramitesipj tr  on c.id_tramite_ipj = tr.id_tramite_ipj
            join ipj.t_comerciantes_rubros cr
              on cr.id_tramite_ipj = c.id_tramite_ipj
          where
            tr.id_estado_ult between 100 and 199 and
            c.id_integrante = tmp.id_integrante
         ) Cant_Rubros
      from
        ( Select c.id_integrante, i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, max(matricula) matricula, max(matricula_version) matricula_version
          from ipj.t_comerciantes c join ipj.t_tramitesipj tr
               on c.id_tramite_ipj = tr.id_tramite_ipj 
            join ipj.t_integrantes i
            on c.id_integrante = i.id_integrante
          where
            tr.id_estado_ult between 100 and 199
          group by c.id_integrante, i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero
        ) tmp
       join rcivil.vt_pk_persona p
         on p.id_sexo = tmp.id_sexo and p.nro_documento = tmp.nro_documento and p.pai_cod_pais = tmp.pai_cod_pais and p.id_numero = tmp.id_numero;
    
  END SP_Listar_Comerciantes;
  
  
END Comerciantes;
/

