create or replace package ipj.RRHH
as
 
  PROCEDURE SP_Traer_Tipos_Licencias(
    o_Cursor OUT types.cursorType);
    
  PROCEDURE SP_Traer_Usr_Licencias(
    o_Cursor OUT types.cursorType,
    p_cuil_usuario in varchar2,
    p_mes in number,
    p_anio in number);
  
  PROCEDURE SP_Traer_Usr_Lic_Total(
    o_Cursor OUT types.cursorType,
    p_cuil_usuario in varchar2,
    p_mes in number,
    p_anio in number);
    
  PROCEDURE SP_Guardar_Usr_Licencias(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_cuil_usuario in varchar2,
    p_fec_ausencia in varchar2,
    p_id_tipo_ausencia in number,
    p_fec_desde in varchar2,
    p_fec_hasta in varchar2,
    p_id_tipo_ausencia_old in number,
    p_cuil_responsable in varchar2, 
    p_eliminar in number); --1 elimina
    
  PROCEDURE SP_Traer_Usr_Legajo(
    o_Cursor OUT types.cursorType,
    p_cuil_usuario in varchar2,
    p_anio in number);
    
   PROCEDURE SP_Traer_Licencias_Mensual(
    o_Cursor OUT types.cursorType,
    p_anio in number,
    p_mes in number );
 
  PROCEDURE SP_GUARDAR_Tipos_Licencias(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_tipo_ausencia OUT number,
    p_id_tipo_ausencia in number,
    p_n_tipo_ausencia in varchar2,
    p_siglas in varchar2); 
end RRHH;
/

create or replace package body ipj.RRHH is

  PROCEDURE SP_Traer_Tipos_Licencias(
    o_Cursor OUT types.cursorType)
  IS
  /**************************************************
    Lista los Tipos de Licencias Disponibles
  ***************************************************/
  BEGIN
    OPEN o_Cursor FOR
      select T.ID_TIPO_AUSENCIA, T.N_TIPO_AUSENCIA, T.SIGLAS
      from ipj.T_TIPOS_AUSENCIA t
      order by id_tipo_ausencia;
      
  END SP_Traer_Tipos_Licencias;
  
  PROCEDURE SP_Traer_Usr_Licencias(
    o_Cursor OUT types.cursorType,
    p_cuil_usuario in varchar2,
    p_mes in number,
    p_anio in number)
  IS
  /**************************************************
    Lista las licencias de un usuario para un mes de un a�o detereminado
  ***************************************************/
  BEGIN
    OPEN o_Cursor FOR
      select UL.CUIL_USUARIO, to_char(UL.FEC_AUSENCIA, 'dd/mm/rrrr') FEC_AUSENCIA, 
        to_char(UL.FEC_DESDE, 'dd/mm/rrrr') FEC_DESDE, to_char(UL.FEC_HASTA, 'dd/mm/rrrr') FEC_HASTA,
        to_number(UL.FEC_HASTA -UL.FEC_DESDE +1) Dias,
        UL.ID_TIPO_AUSENCIA, TA.N_TIPO_AUSENCIA, UL.ID_TIPO_AUSENCIA ID_TIPO_AUSENCIA_OLD,  
        IPJ.Varios.FC_ES_FIN_SEMANA(to_char(UL.FEC_AUSENCIA, 'dd'), to_char(UL.FEC_AUSENCIA, 'mm'), to_char(UL.FEC_AUSENCIA, 'rrrr')) Dia_Semana, 
        DF.N_FERIADOS, U.DESCRIPCION, 
        (select u.descripcion from ipj.t_usuarios u where u.cuil_usuario = Ul.Cuil_Responsable) N_Responsable 
      from ipj.T_USUARIOS_LEGAJO ul join IPJ.T_TIPOS_AUSENCIA ta
          on ul.id_tipo_ausencia = ta.id_tipo_ausencia
        join IPJ.t_usuarios u
          on u.cuil_usuario = ul.cuil_usuario
        left join T_COMUNES.VT_FERIADOS df
          on DF.FECHA = ul.fec_ausencia
      where
        UL.CUil_Usuario = p_cuil_usuario and
        extract(year from UL.FEC_AUSENCIA) = p_anio and
        extract(month from UL.FEC_AUSENCIA) = p_mes
      order by UL.FEC_AUSENCIA desc;
      
  END SP_Traer_Usr_Licencias;
  
  PROCEDURE SP_Traer_Usr_Lic_Total(
    o_Cursor OUT types.cursorType,
    p_cuil_usuario in varchar2,
    p_mes in number,
    p_anio in number)
  IS
  /**************************************************
    Lista las licencias de un usuario para un mes de un a�o detereminado
  ***************************************************/
  BEGIN
    OPEN o_Cursor FOR
      select UL.ID_TIPO_AUSENCIA, TA.N_TIPO_AUSENCIA, 
      count(*) cant, --suma la cantidad de licencias de cada tipo
      sum (UL.FEC_HASTA - UL.FEC_DESDE + 1) cantidad -- suma la cantidad de dias de licencias de cada tipo
      from ipj.T_USUARIOS_LEGAJO ul join IPJ.T_TIPOS_AUSENCIA ta
        on ul.id_tipo_ausencia = ta.id_tipo_ausencia
      where
        UL.CUil_Usuario = p_cuil_usuario and
        extract(year from UL.FEC_AUSENCIA) = p_anio and
        extract(month from UL.FEC_AUSENCIA) = p_mes
      group by UL.ID_TIPO_AUSENCIA, TA.N_TIPO_AUSENCIA
      order by UL.ID_TIPO_AUSENCIA desc;
      
  END SP_Traer_Usr_Lic_Total;

  PROCEDURE SP_Guardar_Usr_Licencias(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_cuil_usuario in varchar2,
    p_fec_ausencia in varchar2,
    p_id_tipo_ausencia in number,
    p_fec_desde in varchar2,
    p_fec_hasta in varchar2,
    p_id_tipo_ausencia_old in number,
    p_cuil_responsable in varchar2, 
    p_eliminar in number ) --1 = Elimina
  IS
  /**************************************************
    Lista las licencias de un usuario para un mes de un a�o detereminado
  ***************************************************/
    v_valid_parametros varchar(2000);
  BEGIN
  
    -- Valido que venga Descripcion 
    if p_id_Tipo_ausencia is null then
      v_valid_parametros := v_valid_parametros || 'Licencia Inv�lida'; 
    end if;
    if IPJ.VARIOS.Valida_Fecha(p_fec_ausencia) = 'N'  then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT'); 
    end if;
    if p_fec_desde is not null and IPJ.VARIOS.Valida_Fecha(p_fec_desde) = 'N'  then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT') || ' (Fecha Desde)';
    end if;
    if p_fec_hasta is not null and IPJ.VARIOS.Valida_Fecha(p_fec_Hasta) = 'N'  then
      v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT') || ' (Fecha Hasta)';
    end if;
    
    if v_valid_parametros is not null then
      o_rdo := 'Parametros : ' || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;
    
    -- Si los par�metros estan bien, continuo
    if p_eliminar = 1 then
      delete IPJ.T_USUARIOS_LEGAJO
      where
        cuil_usuario = p_cuil_usuario and
        fec_ausencia = to_Date(p_fec_ausencia, 'dd/mm/rrrr') and
        id_tipo_ausencia = p_id_tipo_ausencia;
    
    else
      -- intento actualizar con el p_id_tipo_ausencia_old
      update IPJ.T_USUARIOS_LEGAJO
      set 
        id_tipo_ausencia = p_id_tipo_ausencia,
        fec_desde = to_Date(nvl(p_fec_desde, p_fec_ausencia), 'dd/mm/rrrr'),
        fec_hasta = to_date(nvl(p_fec_hasta, p_fec_ausencia), 'dd/mm/rrrr')
      where
        cuil_usuario = p_cuil_usuario and
        fec_ausencia = to_Date(p_fec_ausencia, 'dd/mm/rrrr') and
        id_tipo_ausencia = p_id_tipo_ausencia_old;
      
      -- si no se actualizo nada, ingreso el nuevo registro 
      if sql%rowcount = 0 then
        insert into IPJ.T_USUARIOS_LEGAJO (CUIL_USUARIO, FEC_AUSENCIA, 
          ID_TIPO_AUSENCIA, FEC_DESDE, FEC_HASTA, cuil_responsable)
        values(p_CUIL_USUARIO, to_date(p_FEC_AUSENCIA, 'dd/mm/rrrr'), 
         p_ID_TIPO_AUSENCIA, to_date(nvl(p_FEC_DESDE, p_fec_ausencia), 'dd/mm/rrrr'), 
         to_date(nvl(p_FEC_HASTA, p_fec_ausencia), 'dd/mm/rrrr'), p_cuil_responsable);
      end if;
    end if;
    
    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;    
  EXCEPTION 
    WHEN OTHERS THEN 
      o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Guardar_Usr_Licencias;
  
  PROCEDURE SP_Traer_Usr_Legajo(
    o_Cursor OUT types.cursorType,
    p_cuil_usuario in varchar2,
    p_anio in number)
  IS
  /**************************************************
    Lista las licencas agrupadas por mes y dia, del a�o indicado, de un usuario
  ***************************************************/
  BEGIN
    OPEN o_Cursor FOR
      select 
        M.N_MES, substr(m.n_mes, 1, 3) MES_ABRV,
        max(dia01) Dia01, max(dia02) Dia02, max(dia03) Dia03, max(dia04) Dia04, max(dia05) Dia05,
        max(dia06) Dia06, max(dia07) Dia07, max(dia08) Dia08, max(dia09) Dia09, max(dia10) Dia10,
        max(dia11) Dia11, max(dia12) Dia12, max(dia13) Dia13, max(dia14) Dia14, max(dia15) Dia15,
        max(dia16) Dia16, max(dia17) Dia17, max(dia18) Dia18, max(dia19) Dia19, max(dia20) Dia20,
        max(dia21) Dia21, max(dia22) Dia22, max(dia23) Dia23, max(dia24) Dia24, max(dia25) Dia25,
        max(dia26) Dia26, max(dia27) Dia27, max(dia28) Dia28, max(dia29) Dia29, max(dia30) Dia30,
        max(dia31) Dia31,
        max(dia01_feriado) Dia01_feriado, max(dia02_feriado) Dia02_feriado, max(dia03_feriado) Dia03_feriado, max(dia04_feriado) Dia04_feriado, max(dia05_feriado) Dia05_feriado,
        max(dia06_feriado) Dia06_feriado, max(dia07_feriado) Dia07_feriado, max(dia08_feriado) Dia08_feriado, max(dia09_feriado) Dia09_feriado, max(dia10_feriado) Dia10_feriado,
        max(dia11_feriado) Dia11_feriado, max(dia12_feriado) Dia12_feriado, max(dia13_feriado) Dia13_feriado, max(dia14_feriado) Dia14_feriado, max(dia15_feriado) Dia15_feriado,
        max(dia16_feriado) Dia16_feriado, max(dia17_feriado) Dia17_feriado, max(dia18_feriado) Dia18_feriado, max(dia19_feriado) Dia19_feriado, max(dia20_feriado) Dia20_feriado,
        max(dia21_feriado) Dia21_feriado, max(dia22_feriado) Dia22_feriado, max(dia23_feriado) Dia23_feriado, max(dia24_feriado) Dia24_feriado, max(dia25_feriado) Dia25_feriado,
        max(dia26_feriado) Dia26_feriado, max(dia27_feriado) Dia27_feriado, max(dia28_feriado) Dia28_feriado, max(dia29_feriado) Dia29_feriado, max(dia30_feriado) Dia30_feriado,
        max(dia31_feriado) Dia31_feriado,
        IPJ.VARIOS.FC_ES_FIN_SEMANA('01', m.id_mes, p_anio) Dia01_finde, IPJ.VARIOS.FC_ES_FIN_SEMANA('02', m.id_mes, p_anio) Dia02_finde, IPJ.VARIOS.FC_ES_FIN_SEMANA('03', m.id_mes, p_anio) Dia03_finde, 
        IPJ.VARIOS.FC_ES_FIN_SEMANA('04', m.id_mes, p_anio) Dia04_finde, IPJ.VARIOS.FC_ES_FIN_SEMANA('05', m.id_mes, p_anio) Dia05_finde,
        IPJ.VARIOS.FC_ES_FIN_SEMANA('06', m.id_mes, p_anio) Dia06_finde, IPJ.VARIOS.FC_ES_FIN_SEMANA('07', m.id_mes, p_anio) Dia07_finde, IPJ.VARIOS.FC_ES_FIN_SEMANA('08', m.id_mes, p_anio) Dia08_finde, 
        IPJ.VARIOS.FC_ES_FIN_SEMANA('09', m.id_mes, p_anio) Dia09_finde, IPJ.VARIOS.FC_ES_FIN_SEMANA('10', m.id_mes, p_anio) Dia10_finde,
        IPJ.VARIOS.FC_ES_FIN_SEMANA('11', m.id_mes, p_anio) Dia11_finde, IPJ.VARIOS.FC_ES_FIN_SEMANA('12', m.id_mes, p_anio) Dia12_finde, IPJ.VARIOS.FC_ES_FIN_SEMANA('13', m.id_mes, p_anio) Dia13_finde, 
        IPJ.VARIOS.FC_ES_FIN_SEMANA('14', m.id_mes, p_anio) Dia14_finde, IPJ.VARIOS.FC_ES_FIN_SEMANA('15', m.id_mes, p_anio) Dia15_finde,
        IPJ.VARIOS.FC_ES_FIN_SEMANA('16', m.id_mes, p_anio) Dia16_finde, IPJ.VARIOS.FC_ES_FIN_SEMANA('17', m.id_mes, p_anio) Dia17_finde, IPJ.VARIOS.FC_ES_FIN_SEMANA('18', m.id_mes, p_anio) Dia18_finde, 
        IPJ.VARIOS.FC_ES_FIN_SEMANA('19', m.id_mes, p_anio) Dia19_finde, IPJ.VARIOS.FC_ES_FIN_SEMANA('20', m.id_mes, p_anio) Dia20_finde,
        IPJ.VARIOS.FC_ES_FIN_SEMANA('21', m.id_mes, p_anio) Dia21_finde, IPJ.VARIOS.FC_ES_FIN_SEMANA('22', m.id_mes, p_anio) Dia22_finde, IPJ.VARIOS.FC_ES_FIN_SEMANA('23', m.id_mes, p_anio) Dia23_finde, 
        IPJ.VARIOS.FC_ES_FIN_SEMANA('24', m.id_mes, p_anio) Dia24_finde, IPJ.VARIOS.FC_ES_FIN_SEMANA('25', m.id_mes, p_anio) Dia25_finde,
        IPJ.VARIOS.FC_ES_FIN_SEMANA('26', m.id_mes, p_anio) Dia26_finde, IPJ.VARIOS.FC_ES_FIN_SEMANA('27', m.id_mes, p_anio) Dia27_finde, IPJ.VARIOS.FC_ES_FIN_SEMANA('28', m.id_mes, p_anio) Dia28_finde, 
        IPJ.VARIOS.FC_ES_FIN_SEMANA('29', m.id_mes, p_anio) Dia29_finde, IPJ.VARIOS.FC_ES_FIN_SEMANA('30', m.id_mes, p_anio) Dia30_finde,
        IPJ.VARIOS.FC_ES_FIN_SEMANA('31', m.id_mes, p_anio) Dia31_finde
      from
        ( select 
            extract(month from UL.FECHA) mes,
            (case when to_char(UL.FECHA, 'dd') = '01' then TA.SIGLAS else null end) Dia01, 
            (case when to_char(UL.FECHA, 'dd') = '02' then TA.SIGLAS else null end) Dia02,
            (case when to_char(UL.FECHA, 'dd') = '03' then TA.SIGLAS else null end) Dia03,
            (case when to_char(UL.FECHA, 'dd') = '04' then TA.SIGLAS else null end) Dia04,
            (case when to_char(UL.FECHA, 'dd') = '05' then TA.SIGLAS else null end) Dia05,
            (case when to_char(UL.FECHA, 'dd') = '06' then TA.SIGLAS else null end) Dia06,
            (case when to_char(UL.FECHA, 'dd') = '07' then TA.SIGLAS else null end) Dia07,
            (case when to_char(UL.FECHA, 'dd') = '08' then TA.SIGLAS else null end) Dia08,
            (case when to_char(UL.FECHA, 'dd') = '09' then TA.SIGLAS else null end) Dia09,
            (case when to_char(UL.FECHA, 'dd') = '10' then TA.SIGLAS else null end) Dia10,
            (case when to_char(UL.FECHA, 'dd') = '11' then TA.SIGLAS else null end) Dia11,
            (case when to_char(UL.FECHA, 'dd') = '12' then TA.SIGLAS else null end) Dia12,
            (case when to_char(UL.FECHA, 'dd') = '13' then TA.SIGLAS else null end) Dia13,
            (case when to_char(UL.FECHA, 'dd') = '14' then TA.SIGLAS else null end) Dia14,
            (case when to_char(UL.FECHA, 'dd') = '15' then TA.SIGLAS else null end) Dia15,
            (case when to_char(UL.FECHA, 'dd') = '16' then TA.SIGLAS else null end) Dia16,
            (case when to_char(UL.FECHA, 'dd') = '17' then TA.SIGLAS else null end) Dia17,
            (case when to_char(UL.FECHA, 'dd') = '18' then TA.SIGLAS else null end) Dia18,
            (case when to_char(UL.FECHA, 'dd') = '19' then TA.SIGLAS else null end) Dia19,
            (case when to_char(UL.FECHA, 'dd') = '20' then TA.SIGLAS else null end) Dia20,
            (case when to_char(UL.FECHA, 'dd') = '21' then TA.SIGLAS else null end) Dia21,
            (case when to_char(UL.FECHA, 'dd') = '22' then TA.SIGLAS else null end) Dia22,
            (case when to_char(UL.FECHA, 'dd') = '23' then TA.SIGLAS else null end) Dia23,
            (case when to_char(UL.FECHA, 'dd') = '24' then TA.SIGLAS else null end) Dia24,
            (case when to_char(UL.FECHA, 'dd') = '25' then TA.SIGLAS else null end) Dia25,
            (case when to_char(UL.FECHA, 'dd') = '26' then TA.SIGLAS else null end) Dia26,
            (case when to_char(UL.FECHA, 'dd') = '27' then TA.SIGLAS else null end) Dia27,
            (case when to_char(UL.FECHA, 'dd') = '28' then TA.SIGLAS else null end) Dia28,
            (case when to_char(UL.FECHA, 'dd') = '29' then TA.SIGLAS else null end) Dia29,
            (case when to_char(UL.FECHA, 'dd') = '30' then TA.SIGLAS else null end) Dia30,
            (case when to_char(UL.FECHA, 'dd') = '31' then TA.SIGLAS else null end) Dia31,
      
            (case when to_char(UL.FECHA, 'dd') = '01' then DF.N_FERIADOS else null end) Dia01_feriado, 
            (case when to_char(UL.FECHA, 'dd') = '02' then DF.N_FERIADOS else null end) Dia02_feriado,
            (case when to_char(UL.FECHA, 'dd') = '03' then DF.N_FERIADOS else null end) Dia03_feriado,
            (case when to_char(UL.FECHA, 'dd') = '04' then DF.N_FERIADOS else null end) Dia04_feriado,
            (case when to_char(UL.FECHA, 'dd') = '05' then DF.N_FERIADOS else null end) Dia05_feriado,
            (case when to_char(UL.FECHA, 'dd') = '06' then DF.N_FERIADOS else null end) Dia06_feriado,
            (case when to_char(UL.FECHA, 'dd') = '07' then DF.N_FERIADOS else null end) Dia07_feriado,
            (case when to_char(UL.FECHA, 'dd') = '08' then DF.N_FERIADOS else null end) Dia08_feriado,
            (case when to_char(UL.FECHA, 'dd') = '09' then DF.N_FERIADOS else null end) Dia09_feriado,
            (case when to_char(UL.FECHA, 'dd') = '10' then DF.N_FERIADOS else null end) Dia10_feriado,
            (case when to_char(UL.FECHA, 'dd') = '11' then DF.N_FERIADOS else null end) Dia11_feriado,
            (case when to_char(UL.FECHA, 'dd') = '12' then DF.N_FERIADOS else null end) Dia12_feriado,
            (case when to_char(UL.FECHA, 'dd') = '13' then DF.N_FERIADOS else null end) Dia13_feriado,
            (case when to_char(UL.FECHA, 'dd') = '14' then DF.N_FERIADOS else null end) Dia14_feriado,
            (case when to_char(UL.FECHA, 'dd') = '15' then DF.N_FERIADOS else null end) Dia15_feriado,
            (case when to_char(UL.FECHA, 'dd') = '16' then DF.N_FERIADOS else null end) Dia16_feriado,
            (case when to_char(UL.FECHA, 'dd') = '17' then DF.N_FERIADOS else null end) Dia17_feriado,
            (case when to_char(UL.FECHA, 'dd') = '18' then DF.N_FERIADOS else null end) Dia18_feriado,
            (case when to_char(UL.FECHA, 'dd') = '19' then DF.N_FERIADOS else null end) Dia19_feriado,
            (case when to_char(UL.FECHA, 'dd') = '20' then DF.N_FERIADOS else null end) Dia20_feriado,
            (case when to_char(UL.FECHA, 'dd') = '21' then DF.N_FERIADOS else null end) Dia21_feriado,
            (case when to_char(UL.FECHA, 'dd') = '22' then DF.N_FERIADOS else null end) Dia22_feriado,
            (case when to_char(UL.FECHA, 'dd') = '23' then DF.N_FERIADOS else null end) Dia23_feriado,
            (case when to_char(UL.FECHA, 'dd') = '24' then DF.N_FERIADOS else null end) Dia24_feriado,
            (case when to_char(UL.FECHA, 'dd') = '25' then DF.N_FERIADOS else null end) Dia25_feriado,
            (case when to_char(UL.FECHA, 'dd') = '26' then DF.N_FERIADOS else null end) Dia26_feriado,
            (case when to_char(UL.FECHA, 'dd') = '27' then DF.N_FERIADOS else null end) Dia27_feriado,
            (case when to_char(UL.FECHA, 'dd') = '28' then DF.N_FERIADOS else null end) Dia28_feriado,
            (case when to_char(UL.FECHA, 'dd') = '29' then DF.N_FERIADOS else null end) Dia29_feriado,
            (case when to_char(UL.FECHA, 'dd') = '30' then DF.N_FERIADOS else null end) Dia30_feriado,
            (case when to_char(UL.FECHA, 'dd') = '31' then DF.N_FERIADOS else null end) Dia31_feriado
          from 
            (  select usl.*, 
                 (case when ipj.varios. VALIDA_FECHA(d.dia ||'/'||m.id_mes||'/'||p_anio) = 'S' and to_date( d.dia|| '/'||m.id_mes||'/'||p_anio, 'dd/mm/rrrr') between usl.fec_desde and usl.fec_hasta then to_date( d.dia||'/'||m.id_mes||'/'||p_anio, 'dd/mm/rrrr') else null end) Fecha
               from T_usuarios_legajo usl , t_comunes.t_meses m, 
                (  select '01' dia from dual    union
                   select '02' dia from dual    union
                   select '03' dia from dual    union
                   select '04' dia from dual    union
                   select '05' dia from dual    union
                   select '06' dia from dual    union
                   select '07' dia from dual    union
                   select '08' dia from dual    union
                   select '09' dia from dual    union
                   select '10' dia from dual    union
                   select '11' dia from dual    union
                   select '12' dia from dual    union
                   select '13' dia from dual    union
                   select '14' dia from dual    union
                   select '15' dia from dual    union
                   select '16' dia from dual    union
                   select '17' dia from dual    union
                   select '18' dia from dual    union
                   select '19' dia from dual    union
                   select '20' dia from dual    union
                   select '21' dia from dual    union
                   select '22' dia from dual    union
                   select '23' dia from dual    union
                   select '24' dia from dual    union
                   select '25' dia from dual    union
                   select '26' dia from dual    union
                   select '27' dia from dual    union
                   select '28' dia from dual    union
                   select '29' dia from dual    union
                   select '30' dia from dual    union
                   select '31' dia from dual 
                 ) D
               where 
                 (extract(year from usl.FEC_DESDE ) = p_anio or extract(year from usl.FEC_HASTA ) = p_anio)
               order by fecha
            ) ul join t_tipos_ausencia ta
              on ul.id_tipo_ausencia = ta.id_tipo_ausencia
            left join T_COMUNES.VT_FERIADOS df
                on DF.FECHA = ul.fecha
          where
            UL.CUil_Usuario = p_cuil_usuario and
            (extract(year from UL.FEC_DESDE ) = p_anio or extract(year from UL.FEC_HASTA ) = p_anio)
          order by UL.FEC_DESDE asc
        ) t right join T_COMUNES.T_MESES m
          on  to_number(m.id_mes) = T.mes
      group by M.N_MES, m.id_mes 
      order by m.id_mes asc;
      
  END SP_Traer_Usr_Legajo;
 
  PROCEDURE SP_Traer_Licencias_Mensual(
    o_Cursor OUT types.cursorType,
    p_anio in number,
    p_mes in number )
  IS
    v_Cursor_Tipo_Ausen  types.cursorType;
    v_Col_Tipo_Ausencia IPJ.T_TIPOS_AUSENCIA%ROWTYPE;
    v_SQL varchar2(8000);
  BEGIN
    OPEN v_Cursor_Tipo_Ausen FOR
      select TA.ID_TIPO_AUSENCIA, TA.N_TIPO_AUSENCIA, TA.SIGLAS
      from IPJ.T_TIPOS_AUSENCIA ta;
    
    --Armo un select dinamico para poner las columnas 
    v_SQL := 
       'select ' || 
       '  u.descripcion, substr(u.cuil_usuario, 3, 8) DNI ';
    
    -- agrego una columna para cada tipo  
    LOOP
       fetch v_Cursor_Tipo_Ausen into v_Col_Tipo_Ausencia;
       EXIT WHEN v_Cursor_Tipo_Ausen%NOTFOUND or v_Cursor_Tipo_Ausen%NOTFOUND is null;
       
       v_SQL := v_SQL ||
         ', (case when UL.ID_TIPO_AUSENCIA = ' || to_char(v_Col_Tipo_Ausencia.id_tipo_ausencia) || ' then to_number(Least(UL.FEC_HASTA, to_date(''01/'' || ' || to_char(p_mes +1) || '|| ''/'' ||' || to_char(p_anio) ||', ''dd/mm/rrrr'')) - Greatest(UL.FEC_DESDE, to_date(''01/'' || ' || to_char(p_mes) || '|| ''/'' || ' || to_char(p_anio) ||', ''dd/mm/rrrr''))) +1 else 0 end) '|| trim(v_Col_Tipo_Ausencia.siglas);
      
    end loop;
    CLOSE v_Cursor_Tipo_Ausen;
    
    --agrego las columnas con la fecha de licencia y total
    v_SQL := v_SQL ||
      '  , to_char(Greatest(UL.FEC_DESDE, to_date(''01/'' || ' || to_char(p_mes) || '|| ''/'' || ' || to_char(p_anio) ||', ''dd/mm/rrrr'')), ''dd/mm/rrrr'') FEC_DESDE '|| 
      '  , to_char(Least(UL.FEC_HASTA, to_date(''01/'' || ' || to_char(p_mes +1) || '|| ''/'' ||' || to_char(p_anio) ||', ''dd/mm/rrrr'')), ''dd/mm/rrrr'') FEC_HASTA ' ||
      '  , to_number(Least(UL.FEC_HASTA, to_date(''01/'' || ' || to_char(p_mes +1) || '|| ''/'' ||' || to_char(p_anio) ||', ''dd/mm/rrrr'')) - Greatest(UL.FEC_DESDE, to_date(''01/'' || ' || to_char(p_mes) || '|| ''/'' || ' || to_char(p_anio) ||', ''dd/mm/rrrr''))) +1 Total_Dias ' ||
      'from ' || 
      '  ipj.T_USUARIOS_LEGAJO ul  join IPJ.t_usuarios u ' ||
      '    on u.cuil_usuario = ul.cuil_usuario ' ||
      'where ' ||
      '  UL.FEC_DESDE between to_date(''01/'' || ' || to_char(p_mes) || '|| ''/'' || ' || to_char(p_anio) ||', ''dd/mm/rrrr'') and to_date(''01/'' || ' || to_char(p_mes +1) || '|| ''/'' ||' || to_char(p_anio) ||', ''dd/mm/rrrr'') or ' ||
      '  UL.FEC_HASTA between to_date(''01/'' || ' || to_char(p_mes) || '|| ''/'' || ' || to_char(p_anio) ||', ''dd/mm/rrrr'') and to_date(''01/'' || ' || to_char(p_mes +1) || '|| ''/'' ||' || to_char(p_anio) ||', ''dd/mm/rrrr'') ' ||
--      '  extract(year from UL.FEC_AUSENCIA) = ' || to_char(p_anio) || ' and ' ||
--      '  extract(month from UL.FEC_AUSENCIA) = ' || to_char(p_mes) ||
      ' order by u.descripcion asc ';
    
    OPEN o_Cursor FOR v_SQL;
    
  EXCEPTION 
    WHEN OTHERS THEN
       IPJ.VARIOS.SP_GUARDAR_LOG(
         p_METODO => 'RRHH.SP_Traer_Licencias_Mensual',
         p_NIVEL => '',
         p_ORIGEN => 'DB',
         p_USR_LOG  => 'DB',
         p_USR_WINDOWS => '',
         p_IP_PC  => '',
         p_EXCEPCION => v_SQL);
  END SP_Traer_Licencias_Mensual;
  
   PROCEDURE SP_GUARDAR_Tipos_Licencias(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_tipo_ausencia OUT number,
    p_id_tipo_ausencia in number,
    p_n_tipo_ausencia in varchar2,
    p_siglas in varchar2)
  IS
  v_valid_parametros varchar(2000);
  /**************************************************
    Guarda o modifica una pagina
  ***************************************************/
  BEGIN
    -- Valido que venga Descripcion 
    if p_n_tipo_ausencia is null then
      v_valid_parametros := v_valid_parametros || 'Nombre Inv�lido'; 
    end if;
    if p_siglas is null then
      v_valid_parametros := v_valid_parametros || 'Siglas Inv�lidas'; 
    end if;
    
    if v_valid_parametros is not null then
      o_rdo := 'Parametros : ' || v_valid_parametros;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      return;
    end if;
    
    --Si estan bien los datos, actualizo, si no hay registro lo agrege y se devuelve el valor insertado.
    update IPJ.T_TIPOS_AUSENCIA
    set
      n_tipo_ausencia = p_n_tipo_ausencia,
      siglas = p_siglas
    where
      id_tipo_ausencia = p_id_tipo_ausencia;
    
    if sql%rowcount = 0 then
      select max(id_tipo_ausencia) +1 into o_id_tipo_ausencia 
      from IPJ.T_TIPOS_AUSENCIA;
      
      insert into IPJ.T_TIPOS_AUSENCIA (id_tipo_ausencia, n_tipo_ausencia, siglas)
      values (o_id_tipo_ausencia, p_n_tipo_ausencia, p_siglas);
      
    else
      o_id_tipo_ausencia := p_id_tipo_ausencia;
    end if;
    
    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
  EXCEPTION
     when OTHERS then
        o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;      
  END SP_GUARDAR_Tipos_Licencias;
 
end RRHH;
/

