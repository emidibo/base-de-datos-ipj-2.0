create or replace package ipj.PCK_DATOS_PF
/*********************************************************** 
  Paquete creado para definir los datos que se comparten con PF
**********************************************************/
as
  -- Tipo registro Sociedades para PF
  TYPE t_RegSociedades IS RECORD(
    c_cuit                 VARCHAR2(11),
    c_nombre               VARCHAR2(250),
    c_id_domicilio         NUMBER(20),
    c_gravamen             VARCHAR2(100),
    c_intervencion         VARCHAR2(1000),
    c_fecha_acta           DATE,
    c_fecha_inscripcion    DATE,
    c_estado               VARCHAR2(50),
    c_vigencia             VARCHAR2(200),
    c_expediente           VARCHAR2(50),
    c_sticker              VARCHAR2(50),
    c_provincia            VARCHAR2(60),
    c_departamento         VARCHAR2(100),
    c_localidad            VARCHAR2(100),
    c_barrio               VARCHAR2(1000),
    c_calle                VARCHAR2(100),
    c_altura               NUMBER(5),
    c_piso                 VARCHAR2(4),
    c_depto                VARCHAR2(4),
    c_torre                VARCHAR2(20),
    c_km                   VARCHAR2(10),
    c_fecha_baja    DATE,
    c_tipo_entidad varchar2(100),
    c_CP varchar2(21)
  );
  
  TYPE t_TabSoc IS TABLE OF t_RegSociedades INDEX BY BINARY_INTEGER;
 
  -- Tipo registro Socios para PF
  TYPE t_RegSocios IS RECORD(
    c_id_sexo              VARCHAR2(2),
    c_id_tipo_documento    VARCHAR2(4),
    c_nro_documento        VARCHAR2(12),
    c_pai_cod_pais         VARCHAR2(5),
    c_id_numero            NUMBER(10),
    c_nombre               VARCHAR2(130),
    c_cuil                 VARCHAR2(20),
    c_fecha_inicio         DATE,
    c_fecha_fin            DATE,
    c_cuota_compartida     VARCHAR2(1),
    c_cuit_empresa         VARCHAR2(20),
    c_n_empresa            VARCHAR2(250),
    c_sticker              VARCHAR2(50),
    c_expediente           VARCHAR2(50),
    c_id_entidad_socio  NUMBER(10)
  );
  
  TYPE t_TabSocios IS TABLE OF t_RegSocios INDEX BY BINARY_INTEGER;
  
  -- Tipo registro Socios Copro para PF
  TYPE t_RegSociosCopro IS RECORD(
    c_id_sexo              VARCHAR2(2),
    c_id_tipo_documento    VARCHAR2(4),
    c_nro_documento        VARCHAR2(12),
    c_pai_cod_pais         VARCHAR2(5),
    c_id_numero            NUMBER(10),
    c_nombre              VARCHAR2(130),
    c_cuil                 VARCHAR2(20),
    c_fecha_inicio         DATE,
    c_fecha_fin            DATE,
    c_cuota_compartida     VARCHAR2(1),
    c_cuit_empresa         VARCHAR2(20),
    c_n_empresa            VARCHAR2(250),
    c_sticker              VARCHAR2(50),
    c_expediente           VARCHAR2(50),
    c_id_entidad_socio  NUMBER(10)
  );
  
  TYPE t_TabSociosCopro IS TABLE OF t_RegSociosCopro INDEX BY BINARY_INTEGER;
  
  -- Tipo registro Socios Usuf para PF
  TYPE t_RegSociosUsuf IS RECORD(
    c_id_sexo              VARCHAR2(2),
    c_id_tipo_documento    VARCHAR2(4),
    c_nro_documento        VARCHAR2(12),
    c_pai_cod_pais         VARCHAR2(5),
    c_id_numero            NUMBER(10),
    c_nombre              VARCHAR2(130),
    c_cuil                 VARCHAR2(20),
    c_fecha_inicio         DATE,
    c_fecha_fin            DATE,
    c_cuota_compartida     VARCHAR2(1),
    c_cuit_empresa         VARCHAR2(20),
    c_n_empresa            VARCHAR2(250),
    c_sticker              VARCHAR2(50),
    c_expediente           VARCHAR2(50),
    c_id_entidad_socio  NUMBER(10)
  );
  
  TYPE t_TabSociosUsuf IS TABLE OF t_RegSociosUsuf INDEX BY BINARY_INTEGER;
  
  -- Tipo registro Administrador para PF
  TYPE t_RegAdmin IS RECORD(
    c_id_sexo              VARCHAR2(2),
    c_id_tipo_documento    VARCHAR2(4),
    c_nro_documento        VARCHAR2(12),
    c_pai_cod_pais         VARCHAR2(5),
    c_id_numero            NUMBER(10),
    c_nombre               VARCHAR2(130),
    c_cuil                 VARCHAR2(20),
    c_cuit_empresa         VARCHAR2(20),
    c_n_empresa            VARCHAR2(250),
    c_n_tipo_integrante    VARCHAR2(50),
    c_n_tipo_organismo     VARCHAR2(50),
    c_fecha_inicio         DATE,
    c_fecha_fin            DATE,
    c_cuota_compartida     VARCHAR2(1),
    c_sticker              VARCHAR2(50),
    c_expediente           VARCHAR2(50)
  );
  
  TYPE t_TabAdmin IS TABLE OF t_RegAdmin INDEX BY BINARY_INTEGER;
  
  -- Tipo registro Rubricas para PF
  TYPE t_RegRubricas IS RECORD(
    c_nro_libro            NUMBER(10),
    c_titulo               VARCHAR2(100),
    c_fecha_tramite        DATE,
    c_hojas                VARCHAR2(100),
    c_n_juzgado            VARCHAR2(4000),
    c_sentencia            VARCHAR2(15),
    c_fecha_sent           DATE,
    c_observacion          VARCHAR2(2000),
    c_hojas_desde          NUMBER(6),
    c_hojas_hasta          NUMBER(6),
    c_es_autorizacion      CHAR(1),
    c_sticker              VARCHAR2(50),
    c_expediente           VARCHAR2(50)
  );
  
  TYPE t_TabRubricas IS TABLE OF t_RegRubricas INDEX BY BINARY_INTEGER;
  
  -- Tipo registro Balance para PF
  TYPE t_RegBalance IS RECORD(
    c_fecha              DATE,
    c_reserva_legal      VARCHAR2(1), 
    c_monto              NUMBER(24,4), 
    c_activos            NUMBER(24,4), 
    c_pasivos            NUMBER(24,4), 
    c_neto               NUMBER(24,4),
    c_ingreso_ejerc      NUMBER(24,4),
    c_costo_ejerc        NUMBER(24,4),
    c_result_ejerc       NUMBER(24,4),
    c_observacion        VARCHAR2(2000),
    c_fec_asamblea       DATE, 
    c_n_tipo_asamblea    VARCHAR2(500),
    c_obs_asamblea       VARCHAR2(2000), 
    c_sticker            VARCHAR2(50),
    c_expediente         VARCHAR2(50)
  );
  
  TYPE t_TabBalance IS TABLE OF t_RegBalance INDEX BY BINARY_INTEGER;
  
  -- Tipo registro Mandatos para PF
  TYPE t_RegMandatos IS RECORD(
    --Empresa
    c_cuit_mandante VARCHAR2(25),
    c_mandante VARCHAR2(1000),
    --Mandatario
    c_id_sexo VARCHAR2(2),
    c_id_tipo_documento VARCHAR2(4),
    c_nro_documento VARCHAR2(12),
    c_pai_cod_pais VARCHAR2(5),
    c_id_numero NUMBER(10),
    c_nombre_mandatario VARCHAR2(130),
    c_cuil VARCHAR2(20),
    c_cuit_mandatario VARCHAR2(25),
    c_mandatario VARCHAR2(1000),
    --Poder
    c_fecha_alta DATE,
    c_fecha_baja DATE,
    c_poder VARCHAR2(50),
    c_sticker VARCHAR2(50),
    c_expediente VARCHAR2(50)
  );
  
  TYPE t_TabMandatos IS TABLE OF t_RegMandatos INDEX BY BINARY_INTEGER;
  
  -- Tipo registro Mandatarios para PF
  TYPE t_RegMandatarios IS RECORD(
    --Mandatario
    c_cuit_mandatario VARCHAR2(25),
    c_mandatario VARCHAR2(1000),
    --Empresa
    c_cuit_mandante VARCHAR2(25),
    c_mandante VARCHAR2(1000),
    c_id_sexo VARCHAR2(2),
    c_id_tipo_documento VARCHAR2(4),
    c_nro_documento VARCHAR2(12),
    c_pai_cod_pais VARCHAR2(5),
    c_id_numero NUMBER(10),
    c_nombre_mandante VARCHAR2(130),
    c_cuil VARCHAR2(20),
    --Poder
    c_fecha_alta DATE,
    c_fecha_baja DATE,
    c_poder VARCHAR2(50),
    c_sticker VARCHAR2(50),
    c_expediente VARCHAR2(50)
  );
  
  TYPE t_TabMandatarios IS TABLE OF t_RegMandatarios INDEX BY BINARY_INTEGER;
  
  -- Tipo registro Cierre de Sociedades para Salas Cunas
  TYPE t_RegCierre IS RECORD(
    c_cierre_ejercicio     DATE,
    c_matricula            VARCHAR2(20),
    c_nro_decreto          VARCHAR2(20),
    c_nro_resolucion       VARCHAR2(20),
    c_fec_resolucion       DATE,
    c_fec_estatuto         DATE
  );
 
  TYPE t_TabCierre IS TABLE OF t_RegCierre INDEX BY BINARY_INTEGER;
  
  -- Registro para las entidades relacionadas con una persona
  TYPE t_RegEnt_x_Pers IS RECORD(
    c_id_legajo number(15),
    c_denominacion varchar2(250),
    c_cuit varchar2(20),
    c_tipo_entidad varchar2(100),
    c_es_administrador char(1),
    c_es_Admin_repres char(1), 
    c_es_Sindico char(1), 
    c_es_Repres char(1),
    c_es_Benef_Fidei char(1),
    c_es_Fideicomisario char(1),
    c_es_Fiduciante char(1),
    c_es_Fiduciario char(1),
    c_es_Socio char(1),
    c_es_Socio_Repres char(1),
    c_es_Socio_Cop char(1),
    c_es_Socio_Usuf char(1),
    c_es_Denunciado char(1),
    c_es_Normalizador char(1)
  );
  
  TYPE t_TabEntPers IS TABLE OF t_RegEnt_x_Pers INDEX BY BINARY_INTEGER;
  
   -- Tipo registro Documentacion Digital
  TYPE t_RegDoc_Digital IS RECORD(
    c_id_documento number(10),
    c_documento varchar2(100),
    c_id_tipo_documento_cdd number(5)
  );
 
  TYPE t_TabDocDigital IS TABLE OF t_RegDoc_Digital INDEX BY BINARY_INTEGER;
  

  /*********************************************************
    Arma el texto para informar la vigencia de la sociedad
  *********************************************************/
  FUNCTION FC_Vigencia_Texto(p_TipoVigencia IN ipj.t_entidades.tipo_vigencia%TYPE
                           , p_Vigencia IN ipj.t_entidades.vigencia%TYPE
                           , p_FecInscripcion IN ipj.t_entidades.fec_inscripcion%TYPE
                           , p_ActaContitutiva IN ipj.t_entidades.acta_contitutiva%TYPE
                           , p_FecVig IN ipj.t_entidades.fec_vig%TYPE
                           , p_FecVigHasta IN ipj.t_entidades.fec_vig_hasta%TYPE
  ) RETURN Varchar2;
  
  /*********************************************************
    Verifica si existe un legajo activo para el cuit
  *********************************************************/
  FUNCTION FC_Existe_Legajo(o_rdo out varchar2,
                            o_tipo_mensaje out number,
                            p_Cuit in VARCHAR2
  ) RETURN NUMBER;
  
  /*********************************************************** 
  SP_Validar_CUIT: valida si la sociedad existe o no en IPJ
  **********************************************************/
  PROCEDURE SP_Validar_CUIT(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_cuit in varchar2
  );
  
  /*************************************************************
  Solicitante: POLICIA FISCAL
  SP_Datos_Sociedad: retorna un registro con los datos 
  de la sociedad
  **************************************************************
  Estructura de o_Tab_Sociedad
  CUIT                 VARCHAR2(11)
  NOMBRE               VARCHAR2(250)
  ID_DOMICILIO         NUMBER(20)
  GRAVAMEN             VARCHAR2(100)
  INTERVENCION         VARCHAR2(1000)
  FECHA_ACTA           VARCHAR2(20)
  FECHA_INSCRIPCION    DATE
  ESTADO               VARCHAR2(50)
  VIGENCIA             VARCHAR(200)
  EXPEDIENTE           VARCHAR2(50)
  STICKER              VARCHAR2(50)
  PROVINCIA            VARCHAR2(60)
  DEPARTAMENTO         VARCHAR2(100)
  LOCALIDAD            VARCHAR2(100)
  BARRIO               VARCHAR2(1000)
  CALLE                VARCHAR2(100)
  ALTURA               NUMBER(5)
  PISO                 VARCHAR2(4)
  DEPTO                VARCHAR2(4)
  TORRE                VARCHAR2(20)
  KM                   VARCHAR2(10)
  **********************************************************/
  
  PROCEDURE SP_Datos_Sociedad(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_Tab_Sociedad OUT t_TabSoc,
    p_cuit in VARCHAR2
  );
    
  /*************************************************************
  Solicitante: POLICIA FISCAL
  SP_Datos_Socios: retorna un registro con los datos 
  de los socios
  **************************************************************
  Estructura de o_Tab_Socios
  c_id_sexo              VARCHAR2(2)
  c_id_tipo_documento    VARCHAR2(4)
  c_nro_documento        VARCHAR2(12)
  c_pai_cod_pais         VARCHAR2(5)
  c_id_numero            NUMBER(10)
  c_nombre              VARCHAR2(130)
  c_cuil                 VARCHAR2(20)
  c_fecha_inicio         DATE
  c_fecha_fin            DATE
  c_cuota_compartida     VARCHAR2(1)
  c_cuit_empresa         VARCHAR2(20)
  c_n_empresa            VARCHAR2(250)
  c_sticker VARCHAR2(50)
  c_expediente VARCHAR2(50)
  **********************************************************/
  
  PROCEDURE SP_Datos_Socios(
    o_rdo OUT VARCHAR2,
    o_tipo_mensaje OUT NUMBER,
    o_Tab_Socios OUT t_TabSocios,
    p_FechaInicio DATE,--Si es null, toma la fecha de inicio como comienzo del per�odo
    p_FechaFin DATE,--Si es null, muestra informaci�n hasta el d�a de hoy
    p_cuit in VARCHAR2
  );
  
  /*************************************************************
  Solicitante: POLICIA FISCAL
  SP_Datos_Socios_Copro: retorna un registro con los datos 
  de los socios Co-Propietarios
  **************************************************************
  Estructura de o_Tab_SociosCopro
  c_id_sexo              VARCHAR2(2)
  c_id_tipo_documento    VARCHAR2(4)
  c_nro_documento        VARCHAR2(12)
  c_pai_cod_pais         VARCHAR2(5)
  c_id_numero            NUMBER(10)
  c_nombre               VARCHAR2(130)
  c_cuil                 VARCHAR2(20)
  c_fecha_inicio         DATE
  c_fecha_fin            DATE
  c_cuota_compartida     VARCHAR2(1)
  c_cuit_empresa         VARCHAR2(20)
  c_n_empresa            VARCHAR2(250)
  c_sticker VARCHAR2(50)
  c_expediente VARCHAR2(50)
  **********************************************************/
  
  PROCEDURE SP_Datos_SociosCopro(
    o_rdo OUT VARCHAR2,
    o_tipo_mensaje OUT NUMBER,
    o_Tab_SociosCopro OUT t_TabSociosCopro,
    p_FechaInicio DATE,--Si es null, toma la fecha de inicio como comienzo del per�odo
    p_FechaFin DATE,--Si es null, muestra informaci�n hasta el d�a de hoy
    p_cuit in VARCHAR2
  );
  
  /*************************************************************
  Solicitante: POLICIA FISCAL
  SP_Datos_Socios_Usuf: retorna un registro con los datos 
  de los socios Usufructuarios
  **************************************************************
  Estructura de o_Tab_SociosUsuf
  c_id_sexo              VARCHAR2(2)
  c_id_tipo_documento    VARCHAR2(4)
  c_nro_documento        VARCHAR2(12)
  c_pai_cod_pais         VARCHAR2(5)
  c_id_numero            NUMBER(10)
  c_nombre               VARCHAR2(130)
  c_cuil                 VARCHAR2(20)
  c_fecha_inicio         DATE
  c_fecha_fin            DATE
  c_cuota_compartida     VARCHAR2(1)
  c_cuit_empresa         VARCHAR2(20)
  c_n_empresa            VARCHAR2(250)
  c_sticker VARCHAR2(50)
  c_expediente VARCHAR2(50)
  **********************************************************/
  
  PROCEDURE SP_Datos_SociosUsuf(
    o_rdo OUT VARCHAR2,
    o_tipo_mensaje OUT NUMBER,
    o_Tab_SociosUsuf OUT t_TabSociosUsuf,
    p_FechaInicio DATE,--Si es null, toma la fecha de inicio como comienzo del per�odo
    p_FechaFin DATE,--Si es null, muestra informaci�n hasta el d�a de hoy
    p_cuit in VARCHAR2
  );
  
  /*************************************************************
  Solicitante: POLICIA FISCAL
  SP_Datos_Admin: retorna un registro con los datos 
  de los administradores
  **************************************************************
  Estructura de o_Tab_Admin
  c_id_sexo              VARCHAR2(2),
  c_id_tipo_documento    VARCHAR2(4),
  c_nro_documento        VARCHAR2(12),
  c_pai_cod_pais         VARCHAR2(5),
  c_id_numero            NUMBER(10),
  c_nombre               VARCHAR2(130),
  c_cuil                 VARCHAR2(20),
  c_cuit_empresa         VARCHAR2(20),
  c_n_empresa            VARCHAR2(250),
  c_n_tipo_integrante    VARCHAR2(50),
  c_n_tipo_organismo     VARCHAR2(50),
  c_fecha_inicio         DATE,
  c_fecha_fin            DATE,
  c_cuota_compartida     VARCHAR2(1),
  c_sticker              VARCHAR2(50),
  c_expediente           VARCHAR2(50)
  **********************************************************/
  
  PROCEDURE SP_Datos_Admin(
    o_rdo OUT VARCHAR2,
    o_tipo_mensaje OUT NUMBER,
    o_Tab_Admin OUT t_TabAdmin,
    p_FechaInicio DATE,--Si es null, toma la fecha de inicio como comienzo del per�odo
    p_FechaFin DATE,--Si es null, muestra informaci�n hasta el d�a de hoy
    p_cuit in VARCHAR2
  );
  
  /*************************************************************
  Solicitante: POLICIA FISCAL
  SP_Datos_Rubricas: retorna un registro con los datos 
  de los libros
  **************************************************************
  Estructura de o_Tab_Rubricas
  c_nro_libro NUMBER(10)
  c_titulo VARCHAR2(100)
  c_fecha_tramite DATE
  c_hojas VARCHAR2(100)
  c_n_juzgado VARCHAR2(4000)
  c_sentencia VARCHAR2(15)
  c_fecha_sent DATE
  c_observacion VARCHAR2(2000)
  c_hojas_desde NUMBER(6)
  c_hojas_hasta NUMBER(6)
  c_es_autorizacion CHAR(1)
  c_sticker VARCHAR2(50)
  c_expediente VARCHAR2(50)
  **********************************************************/
  
  PROCEDURE SP_Datos_Rubricas(
    o_rdo OUT VARCHAR2,
    o_tipo_mensaje OUT NUMBER,
    o_Tab_Rubricas OUT t_TabRubricas,
    p_cuit in VARCHAR2
  );
  
  /*************************************************************
  Solicitante: POLICIA FISCAL
  SP_Datos_Balance: retorna un registro con los datos 
  de los balances
  **************************************************************
  c_fecha              DATE,
  c_reserva_legal      VARCHAR2(1), 
  c_monto              NUMBER(24,4), 
  c_activos            NUMBER(24,4), 
  c_pasivos            NUMBER(24,4), 
  c_neto               NUMBER(24,4),
  c_ingreso_ejerc      NUMBER(24,4),
  c_costo_ejerc        NUMBER(24,4),
  c_result_ejerc       NUMBER(24,4),
  c_observacion        VARCHAR2(2000),
  c_fec_asamblea       DATE, 
  c_n_tipo_asamblea    VARCHAR2(500),
  c_obs_asamblea       VARCHAR2(2000), 
  c_sticker            VARCHAR2(50),
  c_expediente         VARCHAR2(50)
  **********************************************************/
  
  PROCEDURE SP_Datos_Balances(
    o_rdo OUT VARCHAR2,
    o_tipo_mensaje OUT NUMBER,
    o_Tab_Balance OUT t_TabBalance,
    p_FechaInicio DATE,--Si es null, toma la fecha de inicio como comienzo del per�odo
    p_FechaFin DATE,--Si es null, muestra informaci�n hasta el d�a de hoy
    p_cuit in VARCHAR2
  );
  
  /*************************************************************
  Solicitante: POLICIA FISCAL
  SP_Datos_Mandatos: retorna un registro con los datos 
  de la empresa con sus mandatarios
  **************************************************************
  --Empresa
  c_cuit_mandante      VARCHAR2(11),
  c_mandante           VARCHAR2(250),
  --Mandatario
  c_id_sexo            VARCHAR2(2),
  c_id_tipo_documento  VARCHAR2(4),
  c_nro_documento      VARCHAR2(12),
  c_pai_cod_pais       VARCHAR2(5),
  c_id_numero          NUMBER(10),
  c_nombre_mandatario  VARCHAR2(130),
  c_cuil               VARCHAR2(20),
  c_cuit_mandatario    VARCHAR2(25),
  c_mandatario         VARCHAR2(1000),
  --Poder
  c_fecha_alta         DATE,
  c_fecha_baja         DATE,
  c_poder              VARCHAR2(50),
  c_sticker            VARCHAR2(50),
  c_expediente         VARCHAR2(50)
  **********************************************************/
  
  PROCEDURE SP_Datos_Mandatos(
    o_rdo OUT VARCHAR2,
    o_tipo_mensaje OUT NUMBER,
    o_Tab_Mandatos OUT t_TabMandatos,
    p_FechaInicio DATE,--Si es null, toma la fecha de inicio como comienzo del per�odo
    p_FechaFin DATE,--Si es null, muestra informaci�n hasta el d�a de hoy
    p_cuit in VARCHAR2
  );
  
  /*************************************************************
  Solicitante: POLICIA FISCAL
  SP_Datos_Mandatarios: retorna un registro con los datos 
  de los mandatarios y sus mandantes
  **************************************************************
  --Mandatario
  c_cuit_mandatario    VARCHAR2(25),
  c_mandatario         VARCHAR2(1000),
  --Mandante
  c_cuit_mandante      VARCHAR2(25),
  c_mandante   VARCHAR2(1000),
  c_id_sexo            VARCHAR2(2),
  c_id_tipo_documento  VARCHAR2(4),
  c_nro_documento      VARCHAR2(12),
  c_pai_cod_pais       VARCHAR2(5),
  c_id_numero          NUMBER(10),
  c_nombre_mandante    VARCHAR2(130),
  c_cuil               VARCHAR2(20),
  --Poder
  c_fecha_alta         DATE,
  c_fecha_baja         DATE,
  c_poder              VARCHAR2(50),
  c_sticker            VARCHAR2(50),
  c_expediente         VARCHAR2(50)
  **********************************************************/
  
  PROCEDURE SP_Datos_Mandatarios(
    o_rdo OUT VARCHAR2,
    o_tipo_mensaje OUT NUMBER,
    o_Tab_Mandatarios OUT t_TabMandatarios,
    p_FechaInicio DATE,--Si es null, toma la fecha de inicio como comienzo del per�odo
    p_FechaFin DATE,--Si es null, muestra informaci�n hasta el d�a de hoy
    p_cuit in VARCHAR2
  );
  
  /************************************************************* 
  Solicitante: SALAS CUNAS - Marcela Oliva
  SP_Datos_Cierre: Retorna los datos extra (no retornados en 
  SP_Datos_Sociedad) registrados de la sociedad relacionados al 
  cierre de ejercicio
  **************************************************************
  Estructura de o_Tab_Sociedad
  c_cierre_ejercicio     DATE,
  c_matricula            VARCHAR2(20),
  c_nro_decreto          VARCHAR2(20),
  c_nro_resolucion       VARCHAR2(20),
  c_fec_resolucion       DATE,
  c_fec_estatuto         DATE
  **********************************************************/
  
  PROCEDURE SP_Datos_Cierre(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_Tab_Cierre OUT t_TabCierre,
    p_cuit in VARCHAR2
  );
  
  PROCEDURE SP_Entidades_x_Persona(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_Tab_Empresas OUT t_TabEntPers,
    o_Nombre out varchar2,
    p_cuil in VARCHAR2
  );
  
   PROCEDURE SP_Datos_Doc_Digital(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_Tab_DocDigital OUT t_TabDocDigital,
    p_cuit in VARCHAR2
  );
  
end;
/

create or replace package body ipj.PCK_DATOS_PF is

  FUNCTION FC_Vigencia_Texto(p_TipoVigencia IN ipj.t_entidades.tipo_vigencia%TYPE
                                           , p_Vigencia IN ipj.t_entidades.vigencia%TYPE
                                           , p_FecInscripcion IN ipj.t_entidades.fec_inscripcion%TYPE
                                           , p_ActaContitutiva IN ipj.t_entidades.acta_contitutiva%TYPE
                                           , p_FecVig IN ipj.t_entidades.fec_vig%TYPE
                                           , p_FecVigHasta IN ipj.t_entidades.fec_vig_hasta%TYPE) RETURN VARCHAR2 IS
  /*********************************************************
    Arma el texto para informar la vigencia de la sociedad
  *********************************************************/
    v_Texto VARCHAR2(100) := '-';
  BEGIN

    IF p_TipoVigencia = 0 THEN
      v_Texto := 'La sociedad est� vigente por '||p_Vigencia||' a�os desde la fecha '||to_char(p_FecInscripcion,'dd/mm/rrrr');
    ELSIF p_TipoVigencia = 1 THEN
      v_Texto := 'La sociedad est� vigente por '||p_Vigencia||' a�os desde la fecha '||to_char(to_date(p_ActaContitutiva,'dd/mm/rrrr'),'dd/mm/rrrr');
    ELSIF p_TipoVigencia = 2 THEN
      v_Texto := 'La sociedad est� vigente por '||p_Vigencia||' a�os desde la fecha '||to_char(p_FecVig,'dd/mm/rrrr');
    ELSE
      v_Texto := 'La sociedad est� vigente hasta la fecha '||to_char(p_FecVigHasta,'dd/mm/rrrr');
    END IF;

    return(v_Texto);

  END FC_Vigencia_Texto;

  FUNCTION FC_Existe_Legajo(o_rdo out varchar2,
                            o_tipo_mensaje out number,
                            p_Cuit in VARCHAR2) RETURN NUMBER IS
  /*********************************************************
    Verifica si existe un legajo para el cuit
  *********************************************************/
    v_id_legajo ipj.t_legajos.id_legajo%TYPE;
  BEGIN

    SELECT l.id_legajo
      INTO v_id_legajo
      FROM ipj.t_legajos l
     WHERE l.cuit = replace(p_cuit, '-', '')
       AND nvl(l.eliminado,0) = 0;

    return(v_id_legajo);

  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      o_rdo := 'No existe una empresa con CUIT ' || p_cuit || ' inscripta en IPJ.';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      RETURN(v_id_legajo);
    WHEN OTHERS THEN
      o_rdo := 'FC_Existe_Legajo: ' || To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      RETURN(v_id_legajo);
  END FC_Existe_Legajo;

  PROCEDURE SP_Validar_CUIT(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_cuit in varchar2)
  IS
  /**************************************************
    Verifica si una empresa existe o no en IPJ
  ***************************************************/
    v_existe number;
  BEGIN
    -- Busco si existe un legajo activo para es cuit
    SELECT l.id_legajo
      INTO v_existe
      FROM ipj.t_legajos l
     WHERE l.cuit = replace(p_cuit, '-', '')
       AND nvl(l.eliminado,0) = 0;

    if v_existe > 0 then
      o_rdo := IPJ.TYPES.C_RESP_OK;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    else
      o_rdo := 'No existe una empresa con CUIT ' || p_cuit || ' inscripta en IPJ.';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
    end if;

  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      o_rdo := 'No existe una empresa con CUIT ' || p_cuit || ' inscripta en IPJ.';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
    WHEN OTHERS THEN
      o_rdo := 'SP_Validar_CUIT: ' || To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Validar_CUIT;

  PROCEDURE SP_Datos_Sociedad(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_Tab_Sociedad OUT t_TabSoc,
    p_cuit in VARCHAR2)
  IS
  /**************************************************
    Retorna los datos registrados de la sociedad
  ***************************************************/
    v_Id_Tramite_Ipj NUMBER;
    v_id_tramiteipj_accion NUMBER;
    v_id_legajo ipj.t_legajos.id_legajo%TYPE;
    v_Indice Number;
    v_TabSocios t_TabSoc;
    v_Existe NUMBER := 0;

  BEGIN
    v_Indice:= 1;

    v_id_legajo := FC_Existe_Legajo(o_rdo, o_tipo_mensaje, p_Cuit);

    if v_id_legajo IS NOT NULL then
      o_rdo := IPJ.TYPES.C_RESP_OK;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    else
      o_rdo := 'No existe una empresa con CUIT ' || p_cuit || ' inscripta en IPJ.';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      RETURN;
    end if;

    ipj.entidad_persjur.SP_Buscar_Entidad_Tramite(v_Id_Tramite_Ipj,v_id_tramiteipj_accion,v_id_legajo,'N');

    FOR r IN (SELECT e.cuit CUIT, e.denominacion_1 NOMBRE, e.id_vin_real ID_DOMICILIO
                   , IPJ.ENTIDAD_PERSJUR.FC_ESTADO_GRAVAMEN_PERSJUR(e.id_legajo) GRAVAMEN
                   , IPJ.ENTIDAD_PERSJUR.FC_ESTADO_GRAVAMEN_INTERV(e.id_legajo) INTERVENCION
                   , to_date(e.acta_contitutiva, 'dd/mm/rrrr') FECHA_ACTA, e.fec_inscripcion FECHA_INSCRIPCION, ee.estado_entidad ESTADO
                   , FC_Vigencia_Texto(e.tipo_vigencia, e.vigencia, e.fec_inscripcion, e.acta_contitutiva,
                                           e.fec_vig, e.fec_vig_hasta) VIGENCIA
                   , t.expediente EXPEDIENTE, t.sticker STICKER, d.n_provincia PROVINCIA
                   , d.n_departamento DEPARTAMENTO, d.n_localidad LOCALIDAD, d.n_barrio BARRIO, d.n_calle CALLE
                   , d.altura ALTURA, d.piso PISO, d.depto DEPTO, d.torre TORRE, d.km KM
                   , e.baja_temporal Fecha_Baja
                   , (select tipo_entidad from ipj.t_tipos_entidades t where t.id_tipo_entidad = e.id_tipo_entidad) Tipo_Entidad
                   , d.cpa CP
                FROM ipj.t_entidades e
                JOIN ipj.t_tramitesipj t ON e.id_tramite_ipj = t.id_tramite_ipj
                JOIN ipj.t_estados_entidades ee ON e.id_estado_entidad = ee.id_estado_entidad
           LEFT JOIN DOM_MANAGER.VT_DOMICILIOS_COND d ON e.id_vin_real = d.id_vin
               WHERE e.id_legajo = v_id_legajo
                 AND e.id_tramite_ipj = v_Id_Tramite_Ipj
                 AND t.id_estado_ult >= IPJ.TYPES.C_ESTADOS_COMPLETADO
                 AND t.id_estado_ult < IPJ.TYPES.C_ESTADOS_RECHAZADO) LOOP

      v_TabSocios(v_Indice).c_cuit := r.cuit;
      v_TabSocios(v_Indice).c_nombre := r.nombre;
      v_TabSocios(v_Indice).c_id_domicilio := r.id_domicilio;
      v_TabSocios(v_Indice).c_gravamen := r.gravamen;
      v_TabSocios(v_Indice).c_intervencion := r.intervencion;
      v_TabSocios(v_Indice).c_fecha_acta := r.fecha_acta;
      v_TabSocios(v_Indice).c_fecha_inscripcion := r.fecha_inscripcion;
      v_TabSocios(v_Indice).c_estado := r.estado;
      v_TabSocios(v_Indice).c_vigencia := r.vigencia;
      v_TabSocios(v_Indice).c_expediente := r.expediente;
      v_TabSocios(v_Indice).c_sticker := r.sticker;
      v_TabSocios(v_Indice).c_provincia := r.provincia;
      v_TabSocios(v_Indice).c_departamento := r.departamento;
      v_TabSocios(v_Indice).c_localidad := r.localidad;
      v_TabSocios(v_Indice).c_barrio := r.barrio;
      v_TabSocios(v_Indice).c_calle := r.calle;
      v_TabSocios(v_Indice).c_altura := r.altura;
      v_TabSocios(v_Indice).c_piso := r.piso;
      v_TabSocios(v_Indice).c_depto := r.depto;
      v_TabSocios(v_Indice).c_torre := r.torre;
      v_TabSocios(v_Indice).c_km := r.km;
      v_TabSocios(v_Indice).c_fecha_baja := r.fecha_baja;
      v_TabSocios(v_Indice).c_tipo_entidad := r.tipo_entidad;
      v_TabSocios(v_Indice).c_CP := r.CP;

      dbms_output.put_line('CUIT: '||v_TabSocios(v_Indice).c_CUIT);
      dbms_output.put_line('nombre: '||v_TabSocios(v_Indice).c_nombre);
      dbms_output.put_line('id_domicilio: '||v_TabSocios(v_Indice).c_id_domicilio);
      dbms_output.put_line('gravamen: '||v_TabSocios(v_Indice).c_gravamen);
      dbms_output.put_line('intervencion: '||v_TabSocios(v_Indice).c_intervencion);
      dbms_output.put_line('fecha_acta: '||v_TabSocios(v_Indice).c_fecha_acta);
      dbms_output.put_line('fecha_inscripcion: '||v_TabSocios(v_Indice).c_fecha_inscripcion);
      dbms_output.put_line('estado: '||v_TabSocios(v_Indice).c_estado);
      dbms_output.put_line('vigencia: '||v_TabSocios(v_Indice).c_vigencia);
      dbms_output.put_line('expediente: '||v_TabSocios(v_Indice).c_expediente);
      dbms_output.put_line('sticker: '||v_TabSocios(v_Indice).c_sticker);
      dbms_output.put_line('provincia: '||v_TabSocios(v_Indice).c_provincia);
      dbms_output.put_line('departamento: '||v_TabSocios(v_Indice).c_departamento);
      dbms_output.put_line('localidad: '||v_TabSocios(v_Indice).c_localidad);
      dbms_output.put_line('barrio: '||v_TabSocios(v_Indice).c_barrio);
      dbms_output.put_line('calle: '||v_TabSocios(v_Indice).c_calle);
      dbms_output.put_line('altura: '||v_TabSocios(v_Indice).c_altura);
      dbms_output.put_line('piso: '||v_TabSocios(v_Indice).c_piso);
      dbms_output.put_line('depto: '||v_TabSocios(v_Indice).c_depto);
      dbms_output.put_line('torre: '||v_TabSocios(v_Indice).c_torre);
      dbms_output.put_line('km: '||v_TabSocios(v_Indice).c_km);
      dbms_output.put_line('Fecha Baja: '||v_TabSocios(v_Indice).c_fecha_baja);
      dbms_output.put_line('Tipo Entidad: '||v_TabSocios(v_Indice).c_tipo_entidad);
      dbms_output.put_line('CP: '||v_TabSocios(v_Indice).c_CP);

      v_Indice:= v_Indice + 1;
      v_Existe := v_Existe + 1;

    END LOOP;

    if v_Existe > 0 then
      o_Tab_Sociedad:= v_TabSocios;
      o_rdo := IPJ.TYPES.C_RESP_OK;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    else
      o_rdo := 'No existen datos registrados en IPJ para la sociedad con CUIT ' || p_cuit;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      RETURN;
    end if;


  EXCEPTION
    WHEN OTHERS THEN
      o_rdo := 'SP_Datos_Sociedad: ' || To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Datos_Sociedad;

  PROCEDURE SP_Datos_Socios(
    o_rdo OUT VARCHAR2,
    o_tipo_mensaje OUT NUMBER,
    o_Tab_Socios OUT t_TabSocios,
    p_FechaInicio DATE,
    p_FechaFin DATE,
    p_cuit in VARCHAR2)
  IS
  /**************************************************
    Retorna los datos registrados de los socios
  ***************************************************/
    v_id_legajo ipj.t_legajos.id_legajo%TYPE;
    v_Indice Number;
    v_TabSocios t_TabSocios;
    v_Existe NUMBER := 0;

  BEGIN
    v_Indice:= 1;

    v_id_legajo := FC_Existe_Legajo(o_rdo, o_tipo_mensaje, p_Cuit);

    if v_id_legajo IS NOT NULL then
      o_rdo := IPJ.TYPES.C_RESP_OK;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    else
      o_rdo := 'No existe una empresa con CUIT ' || p_cuit || ' inscripta en IPJ.';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      RETURN;
    end if;

    FOR r IN (SELECT i.id_sexo id_sexo, p.id_tipo_documento id_tipo_documento, i.nro_documento nro_documento
                   , i.pai_cod_pais pai_cod_pais, i.id_numero id_numero
                   , (select p.nombre||' '||p.apellido from rcivil.vt_pk_persona p where p.id_sexo = i.id_sexo and p.nro_documento = i.nro_documento and p.pai_cod_pais = i.pai_cod_pais and p.id_numero = i.id_numero) nombre
                   , i.cuil cuil
                   , e.fecha_inicio fecha_inicio, e.fecha_fin fecha_fin, e.cuota_compartida, e.cuit_empresa cuit_empresa
                   , e.n_empresa n_empresa, tr.sticker sticker, tr.expediente expediente
                   , e.id_entidad_socio
                FROM ipj.t_entidades_socios e
                JOIN ipj.t_tramitesipj tr ON e.id_tramite_ipj = tr.id_tramite_ipj
           LEFT JOIN ipj.t_integrantes i ON e.id_integrante = i.id_integrante
           LEFT JOIN RCIVIL.VT_PK_PERSONA p ON i.id_sexo = p.ID_SEXO AND i.nro_documento = p.NRO_DOCUMENTO
                 AND i.pai_cod_pais = p.PAI_COD_PAIS AND i.id_numero = p.ID_NUMERO
               WHERE e.id_legajo = v_id_legajo
                 AND e.fecha_inicio BETWEEN nvl(p_FechaInicio,to_date('01/01/1800','dd/mm/rrrr')) AND nvl(p_FechaFin,trunc(SYSDATE))
               ) LOOP

      v_TabSocios(v_Indice).c_id_sexo := r.id_sexo;
      v_TabSocios(v_Indice).c_id_tipo_documento := r.id_tipo_documento;
      v_TabSocios(v_Indice).c_nro_documento := r.nro_documento;
      v_TabSocios(v_Indice).c_pai_cod_pais := r.pai_cod_pais;
      v_TabSocios(v_Indice).c_id_numero := r.id_numero;
      v_TabSocios(v_Indice).c_nombre := r.nombre;
      v_TabSocios(v_Indice).c_cuil := r.cuil;
      v_TabSocios(v_Indice).c_fecha_inicio := r.fecha_inicio;
      v_TabSocios(v_Indice).c_fecha_fin := r.fecha_fin;
      v_TabSocios(v_Indice).c_cuota_compartida := r.cuota_compartida;
      v_TabSocios(v_Indice).c_cuit_empresa := r.cuit_empresa;
      v_TabSocios(v_Indice).c_n_empresa := r.n_empresa;
      v_TabSocios(v_Indice).c_sticker := r.sticker;
      v_TabSocios(v_Indice).c_expediente := r.expediente;
      v_TabSocios(v_Indice).c_id_entidad_socio := r.id_entidad_socio;

      dbms_output.put_line('Sexo: '||v_TabSocios(v_Indice).c_id_sexo);
      dbms_output.put_line('Tipo Doc: '||v_TabSocios(v_Indice).c_id_tipo_documento);
      dbms_output.put_line('Nro Doc: '||v_TabSocios(v_Indice).c_nro_documento);
      dbms_output.put_line('Pa�s: '||v_TabSocios(v_Indice).c_pai_cod_pais);
      dbms_output.put_line('Id N�mero: '||v_TabSocios(v_Indice).c_id_numero);
      dbms_output.put_line('Nombre: '||v_TabSocios(v_Indice).c_nombre);
      dbms_output.put_line('Cuil: '||v_TabSocios(v_Indice).c_cuil);
      dbms_output.put_line('Fecha Inicio: '||v_TabSocios(v_Indice).c_fecha_inicio);
      dbms_output.put_line('Fecha Fin: '||v_TabSocios(v_Indice).c_fecha_fin);
      dbms_output.put_line('Cuota Compartida: '||v_TabSocios(v_Indice).c_cuota_compartida);
      dbms_output.put_line('Cuit Empresa: '||v_TabSocios(v_Indice).c_cuit_empresa);
      dbms_output.put_line('Empresa: '||v_TabSocios(v_Indice).c_n_empresa);
      dbms_output.put_line('Sticker: '||v_TabSocios(v_Indice).c_sticker);
      dbms_output.put_line('Expediente: '||v_TabSocios(v_Indice).c_expediente);
      dbms_output.put_line('Id Entidad Socio: '||v_TabSocios(v_Indice).c_id_entidad_socio);
      dbms_output.put_line('----------------------------------------------------------');

      v_Indice := v_Indice + 1;
      v_Existe := v_Existe + 1;

    END LOOP;

    if v_Existe > 0 then
      o_Tab_Socios:= v_TabSocios;
      o_rdo := IPJ.TYPES.C_RESP_OK;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    else
      o_rdo := 'No existen socios registrados en IPJ para la sociedad con CUIT ' || p_cuit;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      RETURN;
    end if;

  EXCEPTION
    WHEN OTHERS THEN
      o_rdo := 'SP_Datos_Socios: ' || To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Datos_Socios;

  PROCEDURE SP_Datos_SociosCopro(
    o_rdo OUT VARCHAR2,
    o_tipo_mensaje OUT NUMBER,
    o_Tab_SociosCopro OUT t_TabSociosCopro,
    p_FechaInicio DATE,
    p_FechaFin DATE,
    p_cuit in VARCHAR2)
  IS
  /************************************************************
    Retorna los datos registrados de los socios Co-Propietarios
  *************************************************************/
    v_id_legajo ipj.t_legajos.id_legajo%TYPE;
    v_Indice Number;
    v_TabSociosCopro t_TabSociosCopro;
    v_Existe NUMBER := 0;

  BEGIN
    v_Indice:= 1;

    v_id_legajo := FC_Existe_Legajo(o_rdo, o_tipo_mensaje, p_Cuit);

    if v_id_legajo IS NOT NULL then
      o_rdo := IPJ.TYPES.C_RESP_OK;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    else
      o_rdo := 'No existe una empresa con CUIT ' || p_cuit || ' inscripta en IPJ.';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      RETURN;
    end if;

    FOR r IN (SELECT i.id_sexo id_sexo, p.id_tipo_documento id_tipo_documento, i.nro_documento nro_documento
                   , i.pai_cod_pais pai_cod_pais, i.id_numero id_numero
                   , (select p.nombre||' '||p.apellido from rcivil.vt_pk_persona p where p.id_sexo = i.id_sexo and p.nro_documento = i.nro_documento and p.pai_cod_pais = i.pai_cod_pais and p.id_numero = i.id_numero) nombre
                   , i.cuil cuil
                   , e.fecha_inicio fecha_inicio, e.fecha_fin fecha_fin, e.cuota_compartida, e.cuit_empresa cuit_empresa
                   , e.n_empresa n_empresa, tr.sticker sticker, tr.expediente expediente
                   , e.id_entidad_socio
                FROM ipj.t_entidades_socios e
                JOIN ipj.t_entidades_socios_copro esc ON e.id_entidad_socio = esc.id_entidad_socio
                JOIN ipj.t_tramitesipj tr ON e.id_tramite_ipj = tr.id_tramite_ipj
           LEFT JOIN ipj.t_integrantes i ON esc.id_integrante = i.id_integrante
           LEFT JOIN RCIVIL.VT_PK_PERSONA p ON i.id_sexo = p.ID_SEXO AND i.nro_documento = p.NRO_DOCUMENTO
                 AND i.pai_cod_pais = p.PAI_COD_PAIS AND i.id_numero = p.ID_NUMERO
               WHERE e.id_legajo = v_id_legajo
                 AND e.fecha_inicio BETWEEN nvl(p_FechaInicio,to_date('01/01/1800','dd/mm/rrrr')) AND nvl(p_FechaFin,trunc(SYSDATE))
               ) LOOP

      v_TabSociosCopro(v_Indice).c_id_sexo := r.id_sexo;
      v_TabSociosCopro(v_Indice).c_id_tipo_documento := r.id_tipo_documento;
      v_TabSociosCopro(v_Indice).c_nro_documento := r.nro_documento;
      v_TabSociosCopro(v_Indice).c_pai_cod_pais := r.pai_cod_pais;
      v_TabSociosCopro(v_Indice).c_id_numero := r.id_numero;
      v_TabSociosCopro(v_Indice).c_nombre := r.nombre;
      v_TabSociosCopro(v_Indice).c_cuil := r.cuil;
      v_TabSociosCopro(v_Indice).c_fecha_inicio := r.fecha_inicio;
      v_TabSociosCopro(v_Indice).c_fecha_fin := r.fecha_fin;
      v_TabSociosCopro(v_Indice).c_cuota_compartida := r.cuota_compartida;
      v_TabSociosCopro(v_Indice).c_cuit_empresa := r.cuit_empresa;
      v_TabSociosCopro(v_Indice).c_n_empresa := r.n_empresa;
      v_TabSociosCopro(v_Indice).c_sticker := r.sticker;
      v_TabSociosCopro(v_Indice).c_expediente := r.expediente;
      v_TabSociosCopro(v_Indice).c_id_entidad_socio := r.id_entidad_socio;

      dbms_output.put_line('Sexo: '||v_TabSociosCopro(v_Indice).c_id_sexo);
      dbms_output.put_line('Tipo Doc: '||v_TabSociosCopro(v_Indice).c_id_tipo_documento);
      dbms_output.put_line('Nro Doc: '||v_TabSociosCopro(v_Indice).c_nro_documento);
      dbms_output.put_line('Pa�s: '||v_TabSociosCopro(v_Indice).c_pai_cod_pais);
      dbms_output.put_line('Id N�mero: '||v_TabSociosCopro(v_Indice).c_id_numero);
      dbms_output.put_line('Nombre: '||v_TabSociosCopro(v_Indice).c_nombre);
      dbms_output.put_line('Cuil: '||v_TabSociosCopro(v_Indice).c_cuil);
      dbms_output.put_line('Fecha Inicio: '||v_TabSociosCopro(v_Indice).c_fecha_inicio);
      dbms_output.put_line('Fecha Fin: '||v_TabSociosCopro(v_Indice).c_fecha_fin);
      dbms_output.put_line('Cuota Compartida: '||v_TabSociosCopro(v_Indice).c_cuota_compartida);
      dbms_output.put_line('Cuit Empresa: '||v_TabSociosCopro(v_Indice).c_cuit_empresa);
      dbms_output.put_line('Empresa: '||v_TabSociosCopro(v_Indice).c_n_empresa);
      dbms_output.put_line('Sticker: '||v_TabSociosCopro(v_Indice).c_sticker);
      dbms_output.put_line('Expediente: '||v_TabSociosCopro(v_Indice).c_expediente);
      dbms_output.put_line('Id Entidad Socio: '||v_TabSociosCopro(v_Indice).c_id_entidad_socio);
      dbms_output.put_line('----------------------------------------------------------');

      v_Indice := v_Indice + 1;
      v_Existe := v_Existe + 1;

    END LOOP;

    if v_Existe > 0 then
      o_Tab_SociosCopro:= v_TabSociosCopro;
      o_rdo := IPJ.TYPES.C_RESP_OK;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    else
      o_rdo := 'No existen socios Co-Propietarios registrados en IPJ para la sociedad con CUIT ' || p_cuit;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      RETURN;
    end if;

  EXCEPTION
    WHEN OTHERS THEN
      o_rdo := 'SP_Datos_SociosCopro: ' || To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Datos_SociosCopro;

  PROCEDURE SP_Datos_SociosUsuf(
    o_rdo OUT VARCHAR2,
    o_tipo_mensaje OUT NUMBER,
    o_Tab_SociosUsuf OUT t_TabSociosUsuf,
    p_FechaInicio DATE,
    p_FechaFin DATE,
    p_cuit in VARCHAR2)
  IS
  /************************************************************
    Retorna los datos registrados de los socios Usufructuarios
  *************************************************************/
    v_id_legajo ipj.t_legajos.id_legajo%TYPE;
    v_Indice Number;
    v_TabSociosUsuf t_TabSociosUsuf;
    v_Existe NUMBER := 0;

  BEGIN
    v_Indice:= 1;

    v_id_legajo := FC_Existe_Legajo(o_rdo, o_tipo_mensaje, p_Cuit);

    if v_id_legajo IS NOT NULL then
      o_rdo := IPJ.TYPES.C_RESP_OK;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    else
      o_rdo := 'No existe una empresa con CUIT ' || p_cuit || ' inscripta en IPJ.';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      RETURN;
    end if;

    FOR r IN (SELECT i.id_sexo id_sexo, p.id_tipo_documento id_tipo_documento, i.nro_documento nro_documento
                   , i.pai_cod_pais pai_cod_pais, i.id_numero id_numero
                   , (select p.nombre||' '||p.apellido from rcivil.vt_pk_persona p where p.id_sexo = i.id_sexo and p.nro_documento = i.nro_documento and p.pai_cod_pais = i.pai_cod_pais and p.id_numero = i.id_numero) nombre
                   , i.cuil cuil
                   , e.fecha_inicio fecha_inicio, e.fecha_fin fecha_fin, e.cuota_compartida, e.cuit_empresa cuit_empresa
                   , e.n_empresa n_empresa, tr.sticker sticker, tr.expediente expediente
                   , e.id_entidad_socio
                FROM ipj.t_entidades_socios e
                JOIN ipj.t_entidades_socios_usuf esu ON e.id_entidad_socio = esu.id_entidad_socio
                JOIN ipj.t_tramitesipj tr ON e.id_tramite_ipj = tr.id_tramite_ipj
           LEFT JOIN ipj.t_integrantes i ON esu.id_integrante = i.id_integrante
           LEFT JOIN RCIVIL.VT_PK_PERSONA p ON i.id_sexo = p.ID_SEXO AND i.nro_documento = p.NRO_DOCUMENTO
                 AND i.pai_cod_pais = p.PAI_COD_PAIS AND i.id_numero = p.ID_NUMERO
               WHERE e.id_legajo = v_id_legajo
                 AND e.fecha_inicio BETWEEN nvl(p_FechaInicio,to_date('01/01/1800','dd/mm/rrrr')) AND nvl(p_FechaFin,trunc(SYSDATE))
               ) LOOP

      v_TabSociosUsuf(v_Indice).c_id_sexo := r.id_sexo;
      v_TabSociosUsuf(v_Indice).c_id_tipo_documento := r.id_tipo_documento;
      v_TabSociosUsuf(v_Indice).c_nro_documento := r.nro_documento;
      v_TabSociosUsuf(v_Indice).c_pai_cod_pais := r.pai_cod_pais;
      v_TabSociosUsuf(v_Indice).c_id_numero := r.id_numero;
      v_TabSociosUsuf(v_Indice).c_nombre := r.nombre;
      v_TabSociosUsuf(v_Indice).c_cuil := r.cuil;
      v_TabSociosUsuf(v_Indice).c_fecha_inicio := r.fecha_inicio;
      v_TabSociosUsuf(v_Indice).c_fecha_fin := r.fecha_fin;
      v_TabSociosUsuf(v_Indice).c_cuota_compartida := r.cuota_compartida;
      v_TabSociosUsuf(v_Indice).c_cuit_empresa := r.cuit_empresa;
      v_TabSociosUsuf(v_Indice).c_n_empresa := r.n_empresa;
      v_TabSociosUsuf(v_Indice).c_sticker := r.sticker;
      v_TabSociosUsuf(v_Indice).c_expediente := r.expediente;
      v_TabSociosUsuf(v_Indice).c_id_entidad_socio := r.id_entidad_socio;

      dbms_output.put_line('Sexo: '||v_TabSociosUsuf(v_Indice).c_id_sexo);
      dbms_output.put_line('Tipo Doc: '||v_TabSociosUsuf(v_Indice).c_id_tipo_documento);
      dbms_output.put_line('Nro Doc: '||v_TabSociosUsuf(v_Indice).c_nro_documento);
      dbms_output.put_line('Pa�s: '||v_TabSociosUsuf(v_Indice).c_pai_cod_pais);
      dbms_output.put_line('Id N�mero: '||v_TabSociosUsuf(v_Indice).c_id_numero);
      dbms_output.put_line('Nombre: '||v_TabSociosUsuf(v_Indice).c_nombre);
      dbms_output.put_line('Cuil: '||v_TabSociosUsuf(v_Indice).c_cuil);
      dbms_output.put_line('Fecha Inicio: '||v_TabSociosUsuf(v_Indice).c_fecha_inicio);
      dbms_output.put_line('Fecha Fin: '||v_TabSociosUsuf(v_Indice).c_fecha_fin);
      dbms_output.put_line('Cuota Compartida: '||v_TabSociosUsuf(v_Indice).c_cuota_compartida);
      dbms_output.put_line('Cuit Empresa: '||v_TabSociosUsuf(v_Indice).c_cuit_empresa);
      dbms_output.put_line('Empresa: '||v_TabSociosUsuf(v_Indice).c_n_empresa);
      dbms_output.put_line('Sticker: '||v_TabSociosUsuf(v_Indice).c_sticker);
      dbms_output.put_line('Expediente: '||v_TabSociosUsuf(v_Indice).c_expediente);
      dbms_output.put_line('Id Entidad Socio: '||v_TabSociosUsuf(v_Indice).c_id_entidad_socio);
      dbms_output.put_line('----------------------------------------------------------');

      v_Indice := v_Indice + 1;
      v_Existe := v_Existe + 1;

    END LOOP;

    if v_Existe > 0 then
      o_Tab_SociosUsuf:= v_TabSociosUsuf;
      o_rdo := IPJ.TYPES.C_RESP_OK;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    else
      o_rdo := 'No existen socios Usufructuarios registrados en IPJ para la sociedad con CUIT ' || p_cuit;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      RETURN;
    end if;

  EXCEPTION
    WHEN OTHERS THEN
      o_rdo := 'SP_Datos_SociosUsuf: ' || To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Datos_SociosUsuf;

  PROCEDURE SP_Datos_Admin(
    o_rdo OUT VARCHAR2,
    o_tipo_mensaje OUT NUMBER,
    o_Tab_Admin OUT t_TabAdmin,
    p_FechaInicio DATE,
    p_FechaFin DATE,
    p_cuit in VARCHAR2)
  IS
  /**************************************************
    Retorna los datos registrados de los administradores
  ***************************************************/
    v_id_legajo ipj.t_legajos.id_legajo%TYPE;
    v_Indice Number;
    v_TabAdmin t_TabAdmin;
    v_Existe NUMBER := 0;

  BEGIN
    v_Indice:= 1;

    v_id_legajo := FC_Existe_Legajo(o_rdo, o_tipo_mensaje, p_Cuit);

    if v_id_legajo IS NOT NULL then
      o_rdo := IPJ.TYPES.C_RESP_OK;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    else
      o_rdo := 'No existe una empresa con CUIT ' || p_cuit || ' inscripta en IPJ.';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      RETURN;
    end if;

    FOR r IN (SELECT i.id_sexo id_sexo, p.id_tipo_documento id_tipo_documento, i.nro_documento nro_documento
                   , i.pai_cod_pais pai_cod_pais, i.id_numero id_numero
                   , (select p.nombre||' '||p.apellido from rcivil.vt_pk_persona p where p.id_sexo = i.id_sexo and p.nro_documento = i.nro_documento and p.pai_cod_pais = i.pai_cod_pais and p.id_numero = i.id_numero) nombre
                   , i.cuil cuil
                   , ea.cuit_empresa cuit_empresa, ea.n_empresa n_empresa, ti.n_tipo_integrante n_tipo_integrante
                   , tor.n_tipo_organismo n_tipo_organismo, ea.fecha_inicio fecha_inicio, ea.fecha_fin fecha_fin
                   , tr.sticker sticker, tr.expediente expediente
                FROM ipj.t_entidades_admin ea
                JOIN ipj.t_tramitesipj tr ON ea.id_tramite_ipj = tr.id_tramite_ipj
           LEFT JOIN ipj.t_integrantes i ON ea.id_integrante = i.id_integrante
           LEFT JOIN RCIVIL.VT_PK_PERSONA p ON i.id_sexo = p.ID_SEXO AND i.nro_documento = p.NRO_DOCUMENTO
                 AND i.pai_cod_pais = p.PAI_COD_PAIS AND i.id_numero = p.ID_NUMERO
                JOIN ipj.t_tipos_integrante ti ON ea.id_tipo_integrante = ti.id_tipo_integrante
                LEFT JOIN ipj.t_tipos_organismo tor ON ea.id_tipo_organismo = tor.id_tipo_organismo
               WHERE ea.id_legajo = v_id_legajo
                 AND ea.fecha_inicio BETWEEN nvl(p_FechaInicio,to_date('01/01/1800','dd/mm/rrrr')) AND nvl(p_FechaFin,trunc(SYSDATE))
               ) LOOP

      v_TabAdmin(v_Indice).c_id_sexo := r.id_sexo;
      v_TabAdmin(v_Indice).c_id_tipo_documento := r.id_tipo_documento;
      v_TabAdmin(v_Indice).c_nro_documento := r.nro_documento;
      v_TabAdmin(v_Indice).c_pai_cod_pais := r.pai_cod_pais;
      v_TabAdmin(v_Indice).c_id_numero := r.id_numero;
      v_TabAdmin(v_Indice).c_nombre := r.nombre;
      v_TabAdmin(v_Indice).c_cuil := r.cuil;
      v_TabAdmin(v_Indice).c_cuit_empresa := r.cuit_empresa;
      v_TabAdmin(v_Indice).c_n_empresa := r.n_empresa;
      v_TabAdmin(v_Indice).c_n_tipo_integrante := r.n_tipo_integrante;
      v_TabAdmin(v_Indice).c_n_tipo_organismo := r.n_tipo_organismo;
      v_TabAdmin(v_Indice).c_fecha_inicio := r.fecha_inicio;
      v_TabAdmin(v_Indice).c_fecha_fin := r.fecha_fin;
      v_TabAdmin(v_Indice).c_sticker := r.sticker;
      v_TabAdmin(v_Indice).c_expediente := r.expediente;

      dbms_output.put_line('Sexo: '||v_TabAdmin(v_Indice).c_id_sexo);
      dbms_output.put_line('Tipo Doc: '||v_TabAdmin(v_Indice).c_id_tipo_documento);
      dbms_output.put_line('Nro Doc: '||v_TabAdmin(v_Indice).c_nro_documento);
      dbms_output.put_line('Pa�s: '||v_TabAdmin(v_Indice).c_pai_cod_pais);
      dbms_output.put_line('Id N�mero: '||v_TabAdmin(v_Indice).c_id_numero);
      dbms_output.put_line('Nombre: '||v_TabAdmin(v_Indice).c_nombre);
      dbms_output.put_line('Cuil: '||v_TabAdmin(v_Indice).c_cuil);
      dbms_output.put_line('Cuit Empresa: '||v_TabAdmin(v_Indice).c_cuit_empresa);
      dbms_output.put_line('Empresa: '||v_TabAdmin(v_Indice).c_n_empresa);
      dbms_output.put_line('Tipo Integrante: '||v_TabAdmin(v_Indice).c_n_tipo_integrante);
      dbms_output.put_line('Tipo Organismo: '||v_TabAdmin(v_Indice).c_n_tipo_organismo);
      dbms_output.put_line('Fecha Inicio: '||v_TabAdmin(v_Indice).c_fecha_inicio);
      dbms_output.put_line('Fecha Fin: '||v_TabAdmin(v_Indice).c_fecha_fin);
      dbms_output.put_line('Sticker: '||v_TabAdmin(v_Indice).c_sticker);
      dbms_output.put_line('Expediente: '||v_TabAdmin(v_Indice).c_expediente);
      dbms_output.put_line('----------------------------------------------------------');

      v_Indice := v_Indice + 1;
      v_Existe := v_Existe + 1;

    END LOOP;

    if v_Existe > 0 then
      o_Tab_Admin:= v_TabAdmin;
      o_rdo := IPJ.TYPES.C_RESP_OK;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    else
      o_rdo := 'No existen administradores registrados en IPJ para la sociedad con CUIT ' || p_cuit;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      RETURN;
    end if;

  EXCEPTION
    WHEN OTHERS THEN
      o_rdo := 'SP_Datos_Admin: ' || To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Datos_Admin;

  PROCEDURE SP_Datos_Rubricas(
    o_rdo OUT VARCHAR2,
    o_tipo_mensaje OUT NUMBER,
    o_Tab_Rubricas OUT t_TabRubricas,
    p_cuit in VARCHAR2)
  IS
  /**************************************************
    Retorna los datos registrados de las r�bricas
  ***************************************************/
    v_id_legajo ipj.t_legajos.id_legajo%TYPE;
    v_Indice Number;
    v_TabRubricas t_TabRubricas;
    v_Existe NUMBER := 0;

  BEGIN
    v_Indice:= 1;

    v_id_legajo := FC_Existe_Legajo(o_rdo, o_tipo_mensaje, p_Cuit);

    if v_id_legajo IS NOT NULL then
      o_rdo := IPJ.TYPES.C_RESP_OK;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    else
      o_rdo := 'No existe una empresa con CUIT ' || p_cuit || ' inscripta en IPJ.';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      RETURN;
    end if;

    FOR r IN (SELECT e.nro_libro nro_libro, NVL(e.titulo,l.tipo_libro) titulo, e.fecha_tramite fecha_tramite
                   , e.fd_ps hojas, j.n_juzgado n_juzgado, e.sentencia sentencia, e.fecha_sent fecha_sent
                   , e.observacion observacion, e.hojas_desde hojas_desde, e.hojas_hasta hojas_hasta
                   , e.es_autorizacion es_autorizacion, tr.sticker sticker, tr.expediente expediente
                FROM ipj.t_entidades_rubrica e
                JOIN ipj.t_tramitesipj tr ON e.id_tramite_ipj = tr.id_tramite_ipj
                JOIN ipj.t_tipos_libros l ON e.id_tipo_libro = l.id_tipo_libro
                LEFT JOIN ipj.t_juzgados j ON e.id_juzgado = j.id_juzgado
               WHERE e.id_legajo = v_id_legajo
               ) LOOP

      v_TabRubricas(v_Indice).c_nro_libro := r.nro_libro;
      v_TabRubricas(v_Indice).c_titulo := r.titulo;
      v_TabRubricas(v_Indice).c_fecha_tramite := r.fecha_tramite;
      v_TabRubricas(v_Indice).c_hojas := r.hojas;
      v_TabRubricas(v_Indice).c_n_juzgado := r.n_juzgado;
      v_TabRubricas(v_Indice).c_sentencia := r.sentencia;
      v_TabRubricas(v_Indice).c_fecha_sent := r.fecha_sent;
      v_TabRubricas(v_Indice).c_observacion := r.observacion;
      v_TabRubricas(v_Indice).c_hojas_desde := r.hojas_desde;
      v_TabRubricas(v_Indice).c_hojas_hasta := r.hojas_hasta;
      v_TabRubricas(v_Indice).c_es_autorizacion := r.es_autorizacion;
      v_TabRubricas(v_Indice).c_sticker := r.sticker;
      v_TabRubricas(v_Indice).c_expediente := r.expediente;

      dbms_output.put_line('N�mero libro: '||v_TabRubricas(v_Indice).c_nro_libro);
      dbms_output.put_line('T�tulo: '||v_TabRubricas(v_Indice).c_titulo);
      dbms_output.put_line('Fecha Tr�mite: '||v_TabRubricas(v_Indice).c_fecha_tramite);
      dbms_output.put_line('Cantidad Hojas: '||v_TabRubricas(v_Indice).c_hojas);
      dbms_output.put_line('Juzgado: '||v_TabRubricas(v_Indice).c_n_juzgado);
      dbms_output.put_line('Sentencia: '||v_TabRubricas(v_Indice).c_sentencia);
      dbms_output.put_line('Fecha Sentencia: '||v_TabRubricas(v_Indice).c_fecha_sent);
      dbms_output.put_line('Observacion: '||v_TabRubricas(v_Indice).c_observacion);
      dbms_output.put_line('Hojas Desde: '||v_TabRubricas(v_Indice).c_hojas_desde);
      dbms_output.put_line('Hojas Hasta: '||v_TabRubricas(v_Indice).c_hojas_hasta);
      dbms_output.put_line('Es Autorizaci�n?: '||v_TabRubricas(v_Indice).c_es_autorizacion);
      dbms_output.put_line('Sticker: '||v_TabRubricas(v_Indice).c_sticker);
      dbms_output.put_line('Expediente: '||v_TabRubricas(v_Indice).c_expediente);
      dbms_output.put_line('----------------------------------------------------------');

      v_Indice := v_Indice + 1;
      v_Existe := v_Existe + 1;

    END LOOP;

    if v_Existe > 0 then
      o_Tab_Rubricas:= v_TabRubricas;
      o_rdo := IPJ.TYPES.C_RESP_OK;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    else
      o_rdo := 'No existen r�bricas registradas en IPJ para la sociedad con CUIT ' || p_cuit;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      RETURN;
    end if;

  EXCEPTION
    WHEN OTHERS THEN
      o_rdo := 'SP_Datos_Rubricas: ' || To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Datos_Rubricas;

  PROCEDURE SP_Datos_Balances(
    o_rdo OUT VARCHAR2,
    o_tipo_mensaje OUT NUMBER,
    o_Tab_Balance OUT t_TabBalance,
    p_FechaInicio DATE,
    p_FechaFin DATE,
    p_cuit in VARCHAR2)
  IS
  /**************************************************
    Retorna los datos registrados de los balances
  ***************************************************/
    v_id_legajo ipj.t_legajos.id_legajo%TYPE;
    v_Indice Number;
    v_TabBalance t_TabBalance;
    v_Existe NUMBER := 0;

  BEGIN
    v_Indice:= 1;

    v_id_legajo := FC_Existe_Legajo(o_rdo, o_tipo_mensaje, p_Cuit);

    if v_id_legajo IS NOT NULL then
      o_rdo := IPJ.TYPES.C_RESP_OK;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    else
      o_rdo := 'No existe una empresa con CUIT ' || p_cuit || ' inscripta en IPJ.';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      RETURN;
    end if;

    FOR r IN (SELECT e.fecha fecha, e.reserva_legal reserva_legal, e.monto monto, e.activos activos, e.pasivos pasivos, e.neto neto,
                     e.ingreso_ejerc ingreso_ejerc, e.costo_ejerc costo_ejerc, e.result_ejerc result_ejerc, e.observacion observacion,
                     ea.fec_acta fec_asamblea, ta.n_tipo_acta n_tipo_asamblea, ea.observacion obs_asamblea,
                     tr.sticker sticker, tr.expediente expediente
                FROM ipj.t_entidades_balance e
                JOIN ipj.t_tramitesipj tr ON e.id_tramite_ipj = tr.id_tramite_ipj
                JOIN ipj.t_entidades_acta ea ON e.id_tramite_ipj = ea.id_tramite_ipj
           LEFT JOIN ipj.t_tipos_acta ta ON ea.id_tipo_acta = ta.id_tipo_acta
               WHERE e.id_legajo = v_id_legajo
                 AND e.fecha BETWEEN nvl(p_FechaInicio,to_date('01/01/1800','dd/mm/rrrr')) AND nvl(p_FechaFin,trunc(SYSDATE))
               ) LOOP

      v_TabBalance(v_Indice).c_reserva_legal := r.reserva_legal;
      v_TabBalance(v_Indice).c_monto := r.monto;
      v_TabBalance(v_Indice).c_activos := r.activos;
      v_TabBalance(v_Indice).c_pasivos := r.pasivos;
      v_TabBalance(v_Indice).c_neto := r.neto;
      v_TabBalance(v_Indice).c_ingreso_ejerc := r.ingreso_ejerc;
      v_TabBalance(v_Indice).c_costo_ejerc := r.costo_ejerc;
      v_TabBalance(v_Indice).c_result_ejerc := r.result_ejerc;
      v_TabBalance(v_Indice).c_observacion := r.observacion;
      v_TabBalance(v_Indice).c_fec_asamblea := r.fec_asamblea;
      v_TabBalance(v_Indice).c_n_tipo_asamblea := r.n_tipo_asamblea;
      v_TabBalance(v_Indice).c_obs_asamblea := r.obs_asamblea;
      v_TabBalance(v_Indice).c_sticker := r.sticker;
      v_TabBalance(v_Indice).c_expediente := r.expediente;

      dbms_output.put_line('Reserva Legal: '||v_TabBalance(v_Indice).c_reserva_legal);
      dbms_output.put_line('Monto: '||v_TabBalance(v_Indice).c_monto);
      dbms_output.put_line('Activos: '||v_TabBalance(v_Indice).c_activos);
      dbms_output.put_line('Pasivos: '||v_TabBalance(v_Indice).c_pasivos);
      dbms_output.put_line('Neto: '||v_TabBalance(v_Indice).c_neto);
      dbms_output.put_line('Ingreso Ejerc: '||v_TabBalance(v_Indice).c_ingreso_ejerc);
      dbms_output.put_line('Costo Ejerc: '||v_TabBalance(v_Indice).c_costo_ejerc);
      dbms_output.put_line('Resultado Ejerc: '||v_TabBalance(v_Indice).c_result_ejerc);
      dbms_output.put_line('Observaci�n: '||v_TabBalance(v_Indice).c_observacion);
      dbms_output.put_line('Fecha Asamblea: '||v_TabBalance(v_Indice).c_fec_asamblea);
      dbms_output.put_line('Tipo Asamblea: '||v_TabBalance(v_Indice).c_n_tipo_asamblea);
      dbms_output.put_line('Obs Asamblea: '||v_TabBalance(v_Indice).c_obs_asamblea);
      dbms_output.put_line('Sticker: '||v_TabBalance(v_Indice).c_sticker);
      dbms_output.put_line('Expediente: '||v_TabBalance(v_Indice).c_expediente);
      dbms_output.put_line('----------------------------------------------------------');

      v_Indice := v_Indice + 1;
      v_Existe := v_Existe + 1;

    END LOOP;

    if v_Existe > 0 then
      o_Tab_Balance:= v_TabBalance;
      o_rdo := IPJ.TYPES.C_RESP_OK;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    else
      o_rdo := 'No existen balances registrados en IPJ para la sociedad con CUIT ' || p_cuit;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      RETURN;
    end if;

  EXCEPTION
    WHEN OTHERS THEN
      o_rdo := 'SP_Datos_Balance: ' || To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Datos_Balances;

  PROCEDURE SP_Datos_Mandatos(
    o_rdo OUT VARCHAR2,
    o_tipo_mensaje OUT NUMBER,
    o_Tab_Mandatos OUT t_TabMandatos,
    p_FechaInicio DATE,
    p_FechaFin DATE,
    p_cuit in VARCHAR2)
  IS
  /**************************************************
    Retorna los datos registrados de los mandatos
  ***************************************************/
    v_id_legajo ipj.t_legajos.id_legajo%TYPE;
    v_Indice Number;
    v_TabMandatos t_TabMandatos;
    v_Existe NUMBER := 0;

  BEGIN
    v_Indice:= 1;

    v_id_legajo := FC_Existe_Legajo(o_rdo, o_tipo_mensaje, p_Cuit);

    if v_id_legajo IS NOT NULL then
      o_rdo := IPJ.TYPES.C_RESP_OK;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    else
      o_rdo := 'No existe una empresa con CUIT ' || p_cuit || ' inscripta en IPJ.';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      RETURN;
    end if;

    FOR r IN (
       SELECT --Empresa
           l2.cuit cuit_mandante, l2.denominacion_sia mandante
           --Mandatario
         , i.id_sexo id_sexo, p.id_tipo_documento id_tipo_documento, i.nro_documento nro_documento
         , i.pai_cod_pais pai_cod_pais, i.id_numero id_numero
         , (select p.nombre||' '||p.apellido from rcivil.vt_pk_persona p where p.id_sexo = i.id_sexo and p.nro_documento = i.nro_documento and p.pai_cod_pais = i.pai_cod_pais and p.id_numero = i.id_numero) nombre_mandatario
         , i.cuil cuil
         , l.cuit cuit_mandatario, l.denominacion_sia mandatario
           --Poder
         , nvl(mp.fecha_alta, ma.fecha_alta) fecha_alta, nvl(mp.fecha_baja, ma.fecha_baja) fecha_baja, tp.n_tipo_poder_mandato poder
         , tr.sticker sticker, tr.expediente expediente
      FROM ipj.t_mandatos m JOIN ipj.t_mandatos_mandatarios ma
          ON m.id_tramite_ipj = ma.id_tramite_ipj AND m.id_tramiteipj_accion = ma.id_tramiteipj_accion AND m.id_tipo_accion = ma.id_tipo_accion
        JOIN ipj.t_mandatos_mandatarios_poderes mp
          ON ma.id_mandato_mandatario = mp.id_mandato_mandatario
        JOIN ipj.t_tipo_poderes_mandatos tp
          ON mp.id_tipo_poder_mandato = tp.id_tipo_poder_mandato
        JOIN ipj.t_tramitesipj tr
          ON m.id_tramite_ipj = tr.id_tramite_ipj
        LEFT JOIN ipj.t_integrantes i
          ON ma.id_integrante = i.id_integrante
        LEFT JOIN RCIVIL.VT_PK_PERSONA p
          ON i.id_sexo = p.ID_SEXO AND i.nro_documento = p.NRO_DOCUMENTO AND i.pai_cod_pais = p.PAI_COD_PAIS AND i.id_numero = p.ID_NUMERO
        LEFT JOIN ipj.t_legajos l
          ON ma.id_legajo = l.id_legajo
        JOIN ipj.t_legajos l2
          ON m.id_legajo = l2.id_legajo
      WHERE
        m.id_legajo = v_id_legajo
        AND nvl(mp.fecha_alta, ma.fecha_alta) BETWEEN nvl(p_FechaInicio,to_date('01/01/1800','dd/mm/rrrr')) AND nvl(p_FechaFin,trunc(SYSDATE))
     ) LOOP

      v_TabMandatos(v_Indice).c_cuit_mandante := r.cuit_mandante;
      v_TabMandatos(v_Indice).c_mandante := r.mandante;
      v_TabMandatos(v_Indice).c_id_sexo := r.id_sexo;
      v_TabMandatos(v_Indice).c_id_tipo_documento := r.id_tipo_documento;
      v_TabMandatos(v_Indice).c_nro_documento := r.nro_documento;
      v_TabMandatos(v_Indice).c_pai_cod_pais := r.pai_cod_pais;
      v_TabMandatos(v_Indice).c_id_numero := r.id_numero;
      v_TabMandatos(v_Indice).c_nombre_mandatario := r.nombre_mandatario;
      v_TabMandatos(v_Indice).c_cuil := r.cuil;
      v_TabMandatos(v_Indice).c_cuit_mandatario := r.cuit_mandatario;
      v_TabMandatos(v_Indice).c_mandatario := r.mandatario;
      v_TabMandatos(v_Indice).c_fecha_alta := r.fecha_alta;
      v_TabMandatos(v_Indice).c_fecha_baja := r.fecha_baja;
      v_TabMandatos(v_Indice).c_poder := r.poder;
      v_TabMandatos(v_Indice).c_sticker := r.sticker;
      v_TabMandatos(v_Indice).c_expediente := r.expediente;

      dbms_output.put_line('Cuit: '||v_TabMandatos(v_Indice).c_cuit_mandante);
      dbms_output.put_line('Empresa: '||v_TabMandatos(v_Indice).c_mandante);
      dbms_output.put_line('Sexo: '||v_TabMandatos(v_Indice).c_id_sexo);
      dbms_output.put_line('Tipo Documento: '||v_TabMandatos(v_Indice).c_id_tipo_documento);
      dbms_output.put_line('Nro Documento: '||v_TabMandatos(v_Indice).c_nro_documento);
      dbms_output.put_line('Pa�s: '||v_TabMandatos(v_Indice).c_pai_cod_pais);
      dbms_output.put_line('Id Numero: '||v_TabMandatos(v_Indice).c_id_numero);
      dbms_output.put_line('Nombre Mandatario: '||v_TabMandatos(v_Indice).c_nombre_mandatario);
      dbms_output.put_line('Cuil: '||v_TabMandatos(v_Indice).c_cuil);
      dbms_output.put_line('Cuit Mandatario: '||v_TabMandatos(v_Indice).c_cuit_mandatario);
      dbms_output.put_line('Empresa Mandatario: '||v_TabMandatos(v_Indice).c_mandatario);
      dbms_output.put_line('Fecha Alta: '||v_TabMandatos(v_Indice).c_fecha_alta);
      dbms_output.put_line('Fecha Baja: '||v_TabMandatos(v_Indice).c_fecha_baja);
      dbms_output.put_line('Poder: '||v_TabMandatos(v_Indice).c_poder);
      dbms_output.put_line('Sticker: '||v_TabMandatos(v_Indice).c_sticker);
      dbms_output.put_line('Expediente: '||v_TabMandatos(v_Indice).c_expediente);
      dbms_output.put_line('----------------------------------------------------------');

      v_Indice := v_Indice + 1;
      v_Existe := v_Existe + 1;

    END LOOP;

    if v_Existe > 0 then
      o_Tab_Mandatos:= v_TabMandatos;
      o_rdo := IPJ.TYPES.C_RESP_OK;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    else
      o_rdo := 'No existen mandatos registrados en IPJ para la sociedad con CUIT ' || p_cuit;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      RETURN;
    end if;

  EXCEPTION
    WHEN OTHERS THEN
      o_rdo := 'SP_Datos_Mandatos: ' || To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Datos_Mandatos;

  PROCEDURE SP_Datos_Mandatarios(
    o_rdo OUT VARCHAR2,
    o_tipo_mensaje OUT NUMBER,
    o_Tab_Mandatarios OUT t_TabMandatarios,
    p_FechaInicio DATE,
    p_FechaFin DATE,
    p_cuit in VARCHAR2)
  IS
  /**************************************************
    Retorna los datos registrados de los mandatarios
  ***************************************************/
    v_id_legajo ipj.t_legajos.id_legajo%TYPE;
    v_Indice Number;
    v_TabMandatarios t_TabMandatarios;
    v_Existe NUMBER := 0;

  BEGIN
    v_Indice:= 1;

    v_id_legajo := FC_Existe_Legajo(o_rdo, o_tipo_mensaje, p_Cuit);

    if v_id_legajo IS NOT NULL then
      o_rdo := IPJ.TYPES.C_RESP_OK;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    else
      o_rdo := 'No existe una empresa con CUIT ' || p_cuit || ' inscripta en IPJ.';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      RETURN;
    end if;

    FOR r IN (
       SELECT --Mandatario
         l.cuit cuit_mandatario, l.denominacion_sia mandatario
           --Poder
         , nvl(mp.fecha_alta, ma.fecha_alta) fecha_alta, nvl(mp.fecha_baja, ma.fecha_baja) fecha_baja, tp.n_tipo_poder_mandato poder
         , tr.sticker sticker, tr.expediente expediente
           --Empresa Mandante
         , l2.cuit cuit_mandante, l2.denominacion_sia mandante
         , i.id_sexo id_sexo, p.id_tipo_documento id_tipo_documento, i.nro_documento nro_documento
         , i.pai_cod_pais pai_cod_pais, i.id_numero id_numero
         , (select p.nombre||' '||p.apellido from rcivil.vt_pk_persona p where p.id_sexo = i.id_sexo and p.nro_documento = i.nro_documento and p.pai_cod_pais = i.pai_cod_pais and p.id_numero = i.id_numero) nombre_mandante
         , i.cuil cuil
      FROM ipj.t_mandatos m JOIN ipj.t_mandatos_mandatarios ma
          ON m.id_tramite_ipj = ma.id_tramite_ipj AND m.id_tramiteipj_accion = ma.id_tramiteipj_accion AND m.id_tipo_accion = ma.id_tipo_accion
        JOIN ipj.t_mandatos_mandatarios_poderes mp
          ON ma.id_mandato_mandatario = mp.id_mandato_mandatario
        JOIN ipj.t_tipo_poderes_mandatos tp
          ON mp.id_tipo_poder_mandato = tp.id_tipo_poder_mandato
        JOIN ipj.t_tramitesipj tr
          ON m.id_tramite_ipj = tr.id_tramite_ipj
        LEFT JOIN ipj.t_integrantes i
          ON m.id_integrante = i.id_integrante
        LEFT JOIN RCIVIL.VT_PK_PERSONA p
          ON i.id_sexo = p.ID_SEXO AND i.nro_documento = p.NRO_DOCUMENTO AND i.pai_cod_pais = p.PAI_COD_PAIS AND i.id_numero = p.ID_NUMERO
        JOIN ipj.t_legajos l
          ON ma.id_legajo = l.id_legajo
        LEFT JOIN ipj.t_legajos l2
          ON m.id_legajo = l2.id_legajo
      WHERE
        ma.id_legajo = v_id_legajo
       AND nvl(mp.fecha_alta, ma.fecha_alta) BETWEEN nvl(p_FechaInicio,to_date('01/01/1800','dd/mm/rrrr')) AND nvl(p_FechaFin,trunc(SYSDATE))
     ) LOOP

      v_TabMandatarios(v_Indice).c_cuit_mandatario := r.cuit_mandatario;
      v_TabMandatarios(v_Indice).c_mandatario := r.mandatario;
      v_TabMandatarios(v_Indice).c_fecha_alta := r.fecha_alta;
      v_TabMandatarios(v_Indice).c_fecha_baja := r.fecha_baja;
      v_TabMandatarios(v_Indice).c_poder := r.poder;
      v_TabMandatarios(v_Indice).c_sticker := r.sticker;
      v_TabMandatarios(v_Indice).c_expediente := r.expediente;
      v_TabMandatarios(v_Indice).c_cuit_mandante := r.cuit_mandante;
      v_TabMandatarios(v_Indice).c_mandante := r.mandante;
      v_TabMandatarios(v_Indice).c_id_sexo := r.id_sexo;
      v_TabMandatarios(v_Indice).c_id_tipo_documento := r.id_tipo_documento;
      v_TabMandatarios(v_Indice).c_nro_documento := r.nro_documento;
      v_TabMandatarios(v_Indice).c_pai_cod_pais := r.pai_cod_pais;
      v_TabMandatarios(v_Indice).c_id_numero := r.id_numero;
      v_TabMandatarios(v_Indice).c_nombre_mandante := r.nombre_mandante;
      v_TabMandatarios(v_Indice).c_cuil := r.cuil;

      dbms_output.put_line('Cuit Mandatario: '||v_TabMandatarios(v_Indice).c_cuit_mandatario);
      dbms_output.put_line('Mandatario: '||v_TabMandatarios(v_Indice).c_mandatario);
      dbms_output.put_line('Fecha Alta: '||v_TabMandatarios(v_Indice).c_fecha_alta);
      dbms_output.put_line('Fecha Baja '||v_TabMandatarios(v_Indice).c_fecha_baja);
      dbms_output.put_line('Poder: '||v_TabMandatarios(v_Indice).c_poder);
      dbms_output.put_line('Sticker: '||v_TabMandatarios(v_Indice).c_sticker);
      dbms_output.put_line('Expediente: '||v_TabMandatarios(v_Indice).c_expediente);
      dbms_output.put_line('Cuit: '||v_TabMandatarios(v_Indice).c_cuit_mandante);
      dbms_output.put_line('Empresa: '||v_TabMandatarios(v_Indice).c_mandante);
      dbms_output.put_line('Sexo: '||v_TabMandatarios(v_Indice).c_id_sexo);
      dbms_output.put_line('Tipo Doc: '||v_TabMandatarios(v_Indice).c_id_tipo_documento);
      dbms_output.put_line('Nro Doc: '||v_TabMandatarios(v_Indice).c_nro_documento);
      dbms_output.put_line('Pa�s '||v_TabMandatarios(v_Indice).c_pai_cod_pais);
      dbms_output.put_line('IDNumero: '||v_TabMandatarios(v_Indice).c_id_numero);
      dbms_output.put_line('Nombre Mandante: '||v_TabMandatarios(v_Indice).c_nombre_mandante);
      dbms_output.put_line('Cuil: '||v_TabMandatarios(v_Indice).c_cuil);
      dbms_output.put_line('----------------------------------------------------------');

      v_Indice := v_Indice + 1;
      v_Existe := v_Existe + 1;

    END LOOP;

    if v_Existe > 0 then
      o_Tab_Mandatarios:= v_TabMandatarios;
      o_rdo := IPJ.TYPES.C_RESP_OK;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    else
      o_rdo := 'No existen mandatarios registrados en IPJ para la sociedad con CUIT ' || p_cuit;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      RETURN;
    end if;

  EXCEPTION
    WHEN OTHERS THEN
      o_rdo := 'SP_Datos_Mandatarios: ' || To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Datos_Mandatarios;

  PROCEDURE SP_Datos_Cierre(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_Tab_Cierre OUT t_TabCierre,
    p_cuit in VARCHAR2)
  IS
  /*****************************************************************************************************************************
    Retorna los datos extra (no retornados en SP_Datos_Sociedad) registrados de la sociedad relacionados al cierre de ejercicio
  *****************************************************************************************************************************/
    v_Id_Tramite_Ipj NUMBER;
    v_id_tramiteipj_accion NUMBER;
    v_id_legajo ipj.t_legajos.id_legajo%TYPE;
    v_Indice Number;
    v_TabCierre t_TabCierre;
    v_Existe NUMBER := 0;

  BEGIN
    v_Indice:= 1;

    v_id_legajo := FC_Existe_Legajo(o_rdo, o_tipo_mensaje, p_Cuit);

    if v_id_legajo IS NOT NULL then
      o_rdo := IPJ.TYPES.C_RESP_OK;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    else
      o_rdo := 'No existe una empresa con CUIT ' || p_cuit || ' inscripta en IPJ.';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      RETURN;
    end if;

    ipj.entidad_persjur.SP_Buscar_Entidad_Tramite(v_Id_Tramite_Ipj,v_id_tramiteipj_accion,v_id_legajo,'N');

    FOR r IN (SELECT e.cierre_ejercicio CIERRE_EJERCICIO, e.matricula MATRICULA,
                    decode(te.id_ubicacion, 5, e.folio, null) NRO_DECRETO, e.nro_resolucion NRO_RESOLUCION
                   , e.fec_resolucion FEC_RESOLUCION, e.fec_estatuto FEC_ESTATUTO
                FROM ipj.t_entidades e
                JOIN ipj.t_tramitesipj t ON e.id_tramite_ipj = t.id_tramite_ipj
                JOIN ipj.t_estados_entidades ee ON e.id_estado_entidad = ee.id_estado_entidad
                join ipj.t_tipos_entidades te on e.id_tipo_entidad = te.id_tipo_entidad
           LEFT JOIN DOM_MANAGER.VT_DOMICILIOS_COND d ON e.id_vin_real = d.id_vin
               WHERE e.id_legajo = v_id_legajo
                 AND e.id_tramite_ipj = v_Id_Tramite_Ipj
                 AND t.id_estado_ult >= IPJ.TYPES.C_ESTADOS_COMPLETADO
                 AND t.id_estado_ult < IPJ.TYPES.C_ESTADOS_RECHAZADO) LOOP

      v_TabCierre(v_Indice).c_cierre_ejercicio := r.cierre_ejercicio;
      v_TabCierre(v_Indice).c_matricula := r.matricula;
      v_TabCierre(v_Indice).c_nro_decreto := r.nro_decreto;
      v_TabCierre(v_Indice).c_nro_resolucion := r.nro_resolucion;
      v_TabCierre(v_Indice).c_fec_resolucion := r.fec_resolucion;
      v_TabCierre(v_Indice).c_fec_estatuto := r.fec_estatuto;

      dbms_output.put_line('cierre_ejercicio: '||v_TabCierre(v_Indice).c_cierre_ejercicio);
      dbms_output.put_line('matricula: '||v_TabCierre(v_Indice).c_matricula);
      dbms_output.put_line('nro_decreto: '||v_TabCierre(v_Indice).c_nro_decreto);
      dbms_output.put_line('nro_resolucion: '||v_TabCierre(v_Indice).c_nro_resolucion);
      dbms_output.put_line('fec_resolucion: '||v_TabCierre(v_Indice).c_fec_resolucion);
      dbms_output.put_line('fec_estatuto: '||v_TabCierre(v_Indice).c_fec_estatuto);

      v_Indice:= v_Indice + 1;
      v_Existe := v_Existe + 1;

    END LOOP;

    if v_Existe > 0 then
      o_Tab_Cierre:= v_TabCierre;
      o_rdo := IPJ.TYPES.C_RESP_OK;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    else
      o_rdo := 'No existen datos registrados en IPJ para la sociedad con CUIT ' || p_cuit;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      RETURN;
    end if;


  EXCEPTION
    WHEN OTHERS THEN
      o_rdo := 'SP_Datos_Cierre: ' || To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Datos_Cierre;

  PROCEDURE SP_Entidades_x_Persona(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_Tab_Empresas OUT t_TabEntPers,
    o_Nombre out varchar2,
    p_cuil in VARCHAR2)
  IS
  /**********************************************************************
    Lista todas las entidades sonde la eprsona figura de alguina manera asociada
  ***********************************************************************/
    v_id_integrante ipj.t_integrantes.id_integrante%TYPE;
    v_TabEntPers t_TabEntPers;
    v_Indice number;
    v_Existe NUMBER := 0;
  BEGIN
    -- Busco si existe un integrante en IPJ
    begin
      select id_integrante, nombre || ' ' || apellido into v_id_integrante, o_nombre
      from rcivil.vt_pk_persona p left join ipj.t_integrantes i
          on p.id_sexo = i.id_sexo and p.nro_documento = i.nro_documento and p.pai_cod_pais = i.pai_cod_pais and p.id_numero = i.id_numero
      where
        p.cuil = p_cuil;
    exception
      when NO_DATA_FOUND then
        v_id_integrante := -1;
        o_nombre:= 'Persona no encontrada.';
    end;

    v_Indice:= 1;

    FOR r IN
        (select id_legajo,
            (select denominacion_sia from ipj.t_legajos l where l.id_legajo = t.id_legajo) Denominacion,
            (select cuit from ipj.t_legajos l where l.id_legajo = t.id_legajo) Cuit,
            (select tipo_entidad from ipj.t_legajos l join ipj.t_tipos_entidades t on l.id_tipo_entidad = t.id_tipo_entidad where l.id_legajo = t.id_legajo) Tipo_Entidad,
            max(Admin) Administrador, max(Admin_Repres) Admin_repres, Max(Sindico) Sindico, Max(Repres) Repres, Max(Benef_Fidei) Benef_Fidei, max(Fideicomisario) Fideicomisario, max(Fiduciante) Fiduciante,
            Max(Fiduciario) Fiduciario, Max(Socio) Socio, max(Socio_Repres) Socio_Repres, Max(Socio_Cop) Socio_Cop, max(Socio_Usuf) Socio_Usuf, Max(Denunciado) Denunciado, max(Normalizador) Normalizador
          from (
            select distinct id_legajo, 'S' Admin, null Admin_Repres, null Sindico, null Repres, null Benef_Fidei, null Fideicomisario, null Fiduciante, null Fiduciario, null Socio, null Socio_Repres, null Socio_Cop, null Socio_Usuf, null Denunciado, null Normalizador from ipj.t_entidades_admin i where id_integrante = v_id_integrante union
            select distinct id_legajo, null Admin, 'S' Admin_Repres, null Sindico, null Repres, null Benef_Fidei, null Fideicomisario, null Fiduciante, null Fiduciario, null Socio, null Socio_Repres, null Socio_Cop, null Socio_Usuf, null Denunciado, null Normalizador from ipj.t_entidades_admin i where id_integrante_repres = v_id_integrante union
            select distinct id_legajo, null Admin, null Admin_Repres, 'S' Sindico, null Repres, null Benef_Fidei, null Fideicomisario, null Fiduciante, null Fiduciario, null Socio, null Socio_Repres, null Socio_Cop, null Socio_Usuf, null Denunciado, null Normalizador from ipj.t_entidades_sindico i where id_integrante = v_id_integrante union
            select distinct id_legajo, null Admin, null Admin_Repres, null Sindico, 'S' Repres, null Benef_Fidei, null Fideicomisario, null Fiduciante, null Fiduciario, null Socio, null Socio_Repres, null Socio_Cop, null Socio_Usuf, null Denunciado, null Normalizador from ipj.t_entidades_representante i where id_integrante = v_id_integrante union
            select distinct id_legajo, null Admin, null Admin_Repres, null Sindico, null Repres, 'S' Benef_Fidei, null Fideicomisario, null Fiduciante, null Fiduciario, null Socio, null Socio_Repres, null Socio_Cop, null Socio_Usuf, null Denunciado, null Normalizador from ipj.t_entidades_benef i where id_integrante = v_id_integrante union
            select distinct id_legajo, null Admin, null Admin_Repres, null Sindico, null Repres, null Benef_Fidei, 'S' Fideicomisario, null Fiduciante, null Fiduciario, null Socio, null Socio_Repres, null Socio_Cop, null Socio_Usuf, null Denunciado, null Normalizador from ipj.t_entidades_fideicom i where id_integrante = v_id_integrante union
            select distinct id_legajo, null Admin, null Admin_Repres, null Sindico, null Repres, null Benef_Fidei, null Fideicomisario, 'S' Fiduciante, null Fiduciario, null Socio, null Socio_Repres, null Socio_Cop, null Socio_Usuf, null Denunciado, null Normalizador from ipj.t_entidades_fiduciantes i where id_integrante = v_id_integrante union
            select distinct id_legajo, null Admin, null Admin_Repres, null Sindico, null Repres, null Benef_Fidei, null Fideicomisario, null Fiduciante, 'S' Fiduciario, null Socio, null Socio_Repres, null Socio_Cop, null Socio_Usuf, null Denunciado, null Normalizador from ipj.t_entidades_fiduciarios i where id_integrante = v_id_integrante union
            select distinct id_legajo, null Admin, null Admin_Repres, null Sindico, null Repres, null Benef_Fidei, null Fideicomisario, null Fiduciante, null Fiduciario, 'S' Socio, null Socio_Repres, null Socio_Cop, null Socio_Usuf, null Denunciado, null Normalizador from ipj.t_entidades_socios i where id_integrante = v_id_integrante union
            select distinct id_legajo, null Admin, null Admin_Repres, null Sindico, null Repres, null Benef_Fidei, null Fideicomisario, null Fiduciante, null Fiduciario, null Socio, 'S' Socio_Repres, null Socio_Cop, null Socio_Usuf, null Denunciado, null Normalizador from ipj.t_entidades_socios i where id_integrante_representante = v_id_integrante union
            select distinct s.id_legajo, null Admin, null Admin_Repres, null Sindico, null Repres, null Benef_Fidei, null Fideicomisario, null Fiduciante, null Fiduciario, null Socio, null Socio_Repres, 'S' Socio_Cop, null Socio_Usuf, null Denunciado, null Normalizador from ipj.t_entidades_socios_copro i join ipj.t_entidades_socios s on i.id_entidad_socio = s.id_entidad_socio where i.id_integrante = v_id_integrante union
            select distinct s.id_legajo, null Admin, null Admin_Repres, null Sindico, null Repres, null Benef_Fidei, null Fideicomisario, null Fiduciante, null Fiduciario, null Socio, null Socio_Repres, null Socio_Cop, 'S' Socio_Usuf, null Denunciado, null Normalizador from ipj.t_entidades_socios_usuf i join ipj.t_entidades_socios s on i.id_entidad_socio = s.id_entidad_socio where i.id_integrante = v_id_integrante union
            select distinct id_legajo, null Admin, null Admin_Repres, null Sindico, null Repres, null Benef_Fidei, null Fideicomisario, null Fiduciante, null Fiduciario, null Socio, null Socio_Repres, null Socio_Cop, null Socio_Usuf, 'S' Denunciado, null Normalizador from ipj.t_denunciados i where id_integrante = v_id_integrante union
            select distinct id_legajo, null Admin, null Admin_Repres, null Sindico, null Repres, null Benef_Fidei, null Fideicomisario, null Fiduciante, null Fiduciario, null Socio, null Socio_Repres, null Socio_Cop, null Socio_Usuf, null Denunciado, 'S' Normalizador from ipj.t_cn_normalizador i where id_integrante = v_id_integrante
          ) t
          group by id_legajo
          order by tipo_entidad, denominacion asc
        ) LOOP

      v_TabEntPers(v_Indice).c_id_legajo := r.id_legajo;
      v_TabEntPers(v_Indice).c_denominacion := r.denominacion;
      v_TabEntPers(v_Indice).c_cuit := r.cuit;
      v_TabEntPers(v_Indice).c_tipo_entidad := r.tipo_entidad;
      v_TabEntPers(v_Indice).c_es_administrador := r.administrador;
      v_TabEntPers(v_Indice).c_es_Admin_repres := r.Admin_repres;
      v_TabEntPers(v_Indice).c_es_Sindico := r.Sindico;
      v_TabEntPers(v_Indice).c_es_Repres := r.Repres;
      v_TabEntPers(v_Indice).c_es_Benef_Fidei := r.Benef_Fidei;
      v_TabEntPers(v_Indice).c_es_Fideicomisario := r.Fideicomisario;
      v_TabEntPers(v_Indice).c_es_Fiduciante := r.Fiduciante;
      v_TabEntPers(v_Indice).c_es_Fiduciario := r.Fiduciario;
      v_TabEntPers(v_Indice).c_es_Socio := r.Socio;
      v_TabEntPers(v_Indice).c_es_Socio_Repres := r.Socio_Repres;
      v_TabEntPers(v_Indice).c_es_Socio_Cop := r.Socio_Cop;
      v_TabEntPers(v_Indice).c_es_Socio_Usuf := r.Socio_Usuf;
      v_TabEntPers(v_Indice).c_es_Denunciado := r.Denunciado;
      v_TabEntPers(v_Indice).c_es_Normalizador := r.Normalizador;

      v_Indice:= v_Indice + 1;
      v_Existe := v_Existe + 1;
    END LOOP;

    if v_Existe > 0 then
      o_Tab_Empresas := v_TabEntPers;
      o_rdo := IPJ.TYPES.C_RESP_OK;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    else
      o_rdo := 'No existen entidades asociadas en IPJ para la persona de CUIL ' || p_cuil;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      RETURN;
    end if;


  EXCEPTION
    WHEN OTHERS THEN
      o_rdo := 'SP_Entidades_x_Persona: ' || To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;

  END SP_Entidades_x_Persona;

  PROCEDURE SP_Datos_Doc_Digital(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_Tab_DocDigital OUT t_TabDocDigital,
    p_cuit in VARCHAR2)
  IS
  /*****************************************************************************************************************************
    Retorna los documentos digitales aprobados de la entidad solicitada.
  *****************************************************************************************************************************/

    v_id_legajo ipj.t_legajos.id_legajo%TYPE;
    v_Indice Number;
    v_TabDocDig t_TabDocDigital;
    v_Row_DocDigital t_RegDoc_Digital;
    v_Existe NUMBER := 0;
    v_SQL varchar2(4000);
    v_Cursor IPJ.TYPES.CURSORTYPE;
    v_lista_pulic_cdd varchar2(1000);
  BEGIN
    v_Indice:= 1;

    v_id_legajo := FC_Existe_Legajo(o_rdo, o_tipo_mensaje, p_Cuit);

    if v_id_legajo IS NOT NULL then
      o_rdo := IPJ.TYPES.C_RESP_OK;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    else
      o_rdo := 'No existe una empresa con CUIT ' || p_cuit || ' inscripta en IPJ.';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      RETURN;
    end if;

    v_lista_pulic_cdd := IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('PUBLICIDAD_CDD');

    v_sql :=
      ' select da.id_documento, da.n_documento, da.id_tipo_documento_cdd' ||
      ' from ipj.t_tramitesipj tr join ipj.t_entidades e' ||
      '     on tr.id_Tramite_ipj = e.id_tramite_ipj' ||
      '   join IPJ.T_ENTIDADES_DOC_ADJUNTOS da' ||
      '     on da.id_tramite_ipj = e.id_tramite_ipj and da.id_legajo = e.id_legajo' ||
      ' where' ||
      '   e.id_legajo = ' || to_char(v_id_legajo) || ' and' ||
      '   tr.id_estado_ult between 100 and 199 and' ||
      '   da.id_tipo_documento_cdd in (' || v_lista_pulic_cdd || ') and' ||
      '   da.id_estado_doc = 2' ||
      ' UNION ALL' ||
      ' select ac.id_documento, ac.n_documento, ta.id_tipo_documento_cdd' ||
      ' from ipj.t_tramitesipj tr join ipj.t_tramitesipj_acciones ac' ||
      '     on tr.id_Tramite_ipj = ac.id_tramite_ipj' ||
      '   join ipj.t_tipos_Accionesipj ta' ||
      '     on ac.id_Tipo_accion = ta.id_tipo_accion' ||
      ' where' ||
      '   ac.id_legajo = ' || to_char(v_id_legajo) || ' and' ||
      '   ( case' ||
      '       when ac.id_tipo_accion in (146, 147, 154, 157, 161, 176) and tr.id_estado_ult = 203 then 110' ||
      '       when ac.id_tipo_accion in (156, 155, 153, 159, 160, 175) and tr.id_estado_ult = 204 then 110' ||
      '       else tr.id_estado_ult' ||
      '   end) between 100 and 199 and' ||
      '   ( case' ||
      '       when ac.id_tipo_accion in (146, 147, 154, 157, 161, 176) and ac.id_estado = 203 then 110' ||
      '       when ac.id_tipo_accion in (156, 155, 153, 159, 160, 175) and ac.id_estado = 204 then 110' ||
      '       else ac.id_estado' ||
      '   end) between 100 and 199 and' ||
      '   ta.id_tipo_documento_cdd in (' || v_lista_pulic_cdd || ') and' ||
      '   nvl(ac.id_documento, 0) > 0';

    open v_Cursor for v_SQL;

    loop
      fetch v_Cursor into v_Row_DocDigital;
      EXIT WHEN v_Cursor%NOTFOUND or v_Cursor%NOTFOUND is null;

      v_TabDocDig(v_Indice).c_id_documento := v_Row_DocDigital.c_id_documento;
      v_TabDocDig(v_Indice).c_documento := v_Row_DocDigital.c_documento;
      v_TabDocDig(v_Indice).c_id_tipo_documento_cdd := v_Row_DocDigital.c_id_tipo_documento_cdd;

      dbms_output.put_line('ID Documento: '||v_TabDocDig(v_Indice).c_id_documento);
      dbms_output.put_line('Documento: '||v_TabDocDig(v_Indice).c_documento);
      dbms_output.put_line('Tipo Doc CDD: '||v_TabDocDig(v_Indice).c_id_tipo_documento_cdd);

      v_Indice:= v_Indice + 1;
      v_Existe := v_Existe + 1;

    END LOOP;
    close v_Cursor;

    if v_Existe > 0 then
      o_Tab_DocDigital:= v_TabDocDig;
      o_rdo := IPJ.TYPES.C_RESP_OK;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    else
      o_rdo := 'No existen datos registrados en IPJ para la sociedad con CUIT ' || p_cuit;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_WARNING;
      RETURN;
    end if;


  EXCEPTION
    WHEN OTHERS THEN
      o_rdo := 'SP_Datos_Cierre: ' || To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
  END SP_Datos_Doc_Digital;


END PCK_DATOS_PF;
/

