CREATE OR REPLACE PACKAGE IPJ.PKG_SND AS
  /*  Paquete          :  pkg_snd
  *  Autor            :  Lucio Fernandez Herrera
  *  Descripci�n      :  Adaptacion del algoritmo fonetico SOUNDEX para el idioma espanol
  *  Agradecimientos  :  A todos los que colaboran para mejorar este script (en el siguiente link)
  *                   :  http://oraclenotepad.blogspot.com/2008/03/soundex-en-espaol.html
  *
  *  FECHA          VERSION     DESCRIPCION
  *  -------------  ---------   ------------------------------------------------------------
  *  08-MAR-2008     1.0         Version inicial
  *  05-ABR-2008     1.1         [BUG] Fallo con muchas vocales o mas de 30 caracteres
  *  05-ABR-2008     1.2         [BUG] Fallo en funcion eli_acc con may�sculas
  *                              [FIX] Caracteres multibyte son eliminados antes de almacenar
  *                              [FIX] Se eliminan espacios en la entrada
  *  25-ABR-2008     1.3         [FIX] Se corrije valor equivalente para LL inicial en cnv_dos
  *  03-DEC-2008     1.5         [FIX] Letra V con equivalente numerico equivocado
  *                              [FIX] Correccion fonetica GE,GI = JE,JI
  *  13-DEC-2008     1.6         [ADD] NY suena ahora como NI
  *  17-AUG-2009     1.7         [FIX] Correccion en codificacion num�rica de Y+vocal  
  *  04-MAR-2010     1.8         [FIX] Correccion en codificacion de map_num para G  
  */

  -- Funcion Principal
  FUNCTION SOUNDESP(P_PAL IN VARCHAR2) RETURN VARCHAR2;
  FUNCTION FC_REEMPLAZO_FONETICO(P_CLAVE IN VARCHAR2) RETURN VARCHAR2;
  FUNCTION FC_COMPARAR_FRASES(P_FRASE_CLAVE    IN VARCHAR2,
                              P_FRASE_COMPARAR IN VARCHAR2) RETURN NUMBER;
  FUNCTION FC_COMPARAR_FONETICA(P_FRASE_CLAVE    IN VARCHAR2,
                                P_FRASE_COMPARAR IN VARCHAR2) RETURN NUMBER;

END PKG_SND;
/

CREATE OR REPLACE PACKAGE BODY IPJ.pkg_snd AS
  
  FUNCTION FC_Reemplazo_Fonetico(p_clave IN VARCHAR2) RETURN VARCHAR2 AS
  /* Para palabras cortas (3 o menos), en controles f�neticos, se realizan algunas 
     tranformaciones foneticas para comparar las palabras */
     v_return varchar2(250);
  BEGIN
    v_return := replace(upper(p_clave), 'ZA', 'SA');
    v_return := replace(upper(v_return), 'ZE', 'SE');
    v_return := replace(upper(v_return), 'ZI', 'SI');
    v_return := replace(upper(v_return), 'ZO', 'SO');
    v_return := replace(upper(v_return), 'ZU', 'SU');
    v_return := replace(upper(v_return), 'SE', 'CE');
    v_return := replace(upper(v_return), 'SI', 'CI');
    RETURN v_return; 
      
  END FC_Reemplazo_Fonetico;
  
  FUNCTION eli_acc(p_pal IN VARCHAR2) RETURN VARCHAR2 AS
  BEGIN
    -- en caso de quedar caracteres raros por la transferencia, reemplazarlos con el string formado por:
    -- enie mayuscula seguido de vocales mayuscula con acentos
    RETURN translate(ltrim(upper(p_pal), 'H'), '������', 'NAEIOU');
  END eli_acc;

  FUNCTION com_pal(p_pal IN VARCHAR2) RETURN VARCHAR2 AS
    pri_letra VARCHAR2(1) := substr(p_pal, 1, 1);
    resto     VARCHAR2(30) := substr(p_pal, 2);
    letra_ret VARCHAR2(1) := pri_letra;
  BEGIN
    CASE
      WHEN pri_letra = 'V' THEN
        letra_ret := 'B';
      WHEN pri_letra IN ('Z', 'X') THEN
        letra_ret := 'S';
      WHEN pri_letra = 'G' AND
           substr(p_pal, 2, 1) IN ('E', 'I') THEN
        letra_ret := 'J';
      WHEN pri_letra IN ('C') AND
           substr(p_pal, 2, 1) NOT IN ('H', 'E', 'I') THEN
        letra_ret := 'K';
      ELSE
        NULL;
    END CASE;
    RETURN letra_ret || resto;
  END com_pal;

  FUNCTION cnv_dos(p_pal IN VARCHAR2) RETURN VARCHAR2 AS
    ret VARCHAR2(30);
  BEGIN
    ret := REPLACE(p_pal, 'CH', 'V');
    ret := REPLACE(ret, 'QU', 'K');
    ret := REPLACE(ret, 'LL', 'J');
    ret := REPLACE(ret, 'CE', 'S');
    ret := REPLACE(ret, 'CI', 'S');
    ret := REPLACE(ret, 'YA', 'J');
    ret := REPLACE(ret, 'YE', 'J');
    ret := REPLACE(ret, 'YI', 'J');
    ret := REPLACE(ret, 'YO', 'J');
    ret := REPLACE(ret, 'YU', 'J');
    ret := REPLACE(ret, 'GE', 'J');
    ret := REPLACE(ret, 'GI', 'J');
    ret := REPLACE(ret, 'NY', 'N');
    RETURN ret;
  END cnv_dos;

  FUNCTION eli_let(p_pal IN VARCHAR2) RETURN VARCHAR2 AS
  BEGIN
    RETURN translate(p_pal, '@AEIOUHWY', '@');
  END eli_let;

  FUNCTION map_num(p_pal IN VARCHAR2) RETURN VARCHAR2 AS
  BEGIN
    RETURN translate(p_pal, 'BPFVCGKSXZDTLMNRQJ', '111122222233455677');
  END map_num;

  FUNCTION eli_ady(p_num IN VARCHAR2) RETURN VARCHAR2 AS
    ant     VARCHAR(1) := substr(p_num, 1, 1);
    act     VARCHAR(1);
    ret_num VARCHAR2(30) := ant;
  BEGIN
    IF p_num IS NOT NULL AND length(p_num) > 1 THEN
      FOR i IN 2 .. length(p_num) LOOP
        act := substr(p_num, i, 1);
        IF act <> ant THEN
          ret_num := ret_num || act;
          ant     := act;
        END IF;
      END LOOP;
      ret_num := substr(ret_num, 1, 3); --Carlitos
    ELSIF length(p_num) = 1 THEN
      ret_num := p_num;
    ELSE
      ret_num := NULL;
    END IF;
    RETURN ret_num;
  END eli_ady;

  FUNCTION soundesp(p_pal IN VARCHAR2) RETURN VARCHAR2 AS
    pri       VARCHAR2(1);
    subcadena VARCHAR2(30);
    ret       VARCHAR2(30);
  BEGIN
    -- 1. eliminar letra h a la izquierda, acentos, enie
    ret := substr(eli_acc(p_pal), 1, 30);
    -- 2. asociar letras foneticamente parecidas para la primera letra de la palabra
    ret := com_pal(ret);
    -- 3. simplificar combinaciones dobles
    ret := cnv_dos(ret);
    -- 4. retener la primera letra
    pri := substr(ret, 1, 1);
    -- 5. tomar la subcadena derecha
    subcadena := substr(ret, 2, length(ret));
    -- 6. eliminar vocales foneticas
    subcadena := eli_let(subcadena);
    -- 7. mapeo letras foneticamente equivalentes a numeros
    subcadena := map_num(subcadena);
    -- 8. elimino numeros iguales adyacentes
    subcadena := eli_ady(subcadena);
    -- 9. retorno
    ret := pri || subcadena;
    IF length(ret) < 4 THEN
      ret := rpad(ret, 4, '0');
    END IF;
    RETURN(ret);
  END soundesp;
  
 FUNCTION FC_Comparar_Frases (p_frase_Clave IN VARCHAR2, p_frase_comparar in VARCHAR2) RETURN number AS
   ret number;
   v_ret number;
   str varchar2(2000);
   str_aux varchar2(200);
   i number;
   indice number;
   name_array_clave dbms_sql.varchar2_table;
  BEGIN
  
  /**** ARMO UN CONJUNTO CON LAS PALABRAS CLAVES ********/
    --reemplazo los 2 espacios por seguidos por 1
    str := REGEXP_REPLACE(trim(p_frase_Clave), '  *', ' ');
    --reemplazo los espacios por guiones para parsear las palabras
    str := replace(trim(str), ' ', '_');
    
    -- Separo la primer palabra y comienzo a armar la lista de palabas
    if instr(str, '_') > 0 then
      i := instr(str, '_');
    else
      i := length(str)+1;
    end if;
    str_aux := substr(str, 1, i-1);
    str := substr(str, i+1);
    indice := 1;
    WHILE length(str_aux) > 0 LOOP
      --cargo la palabra
      name_array_clave(indice) := upper(str_aux);
      indice := indice + 1;
      -- Separo la siguiente palabra o me quedo con la ultima
      if instr(str, '_') > 0 then
        i := instr(str, '_');
      else
        i := length(str)+1;
      end if;
      str_aux := substr(str, 1, i-1);
      str := substr(str, i+1);
    END LOOP;
    
    /**** COMPARO LAS PALABRAS ********/
    ret := 0;
    FOR i IN 1 .. indice-1
    LOOP
      begin
        -- Busco si hay algo parecido
        select 1 into v_ret from dual where upper(p_frase_comparar) like '%' || upper(name_array_clave(i))|| '%';
        
      exception
        when NO_DATA_FOUND then
          -- Si no se encontr� nada, y es una palabra corta, busco con ajustes fon�ticos
          if length(p_frase_Clave) <= 3 then
            begin
              select 1 into v_ret 
              from dual
              where 
                IPJ.PKG_SND.FC_Reemplazo_Fonetico(p_frase_comparar) like '%' || IPJ.PKG_SND.FC_Reemplazo_Fonetico(name_array_clave(i))|| '%';
            exception
              when OTHERS then
                v_ret := 0;
            end;
          else
            v_ret := 0;
          end if;
      end;
      ret := ret +nvl( v_ret, 0);
    END LOOP;

    RETURN ret;
  END FC_Comparar_Frases; 
  
  FUNCTION FC_Comparar_Fonetica (p_frase_Clave IN VARCHAR2, p_frase_comparar in VARCHAR2) RETURN number AS
   ret number;
   v_ret number;
   str varchar2(2000);
   str_aux varchar2(200);
   i number;
   j number;
   indice_clave number;
   indice_comp number;
   name_array_clave dbms_sql.varchar2_table;
   name_array_comp dbms_sql.varchar2_table;
  BEGIN
  
  /**** ARMO UN CONJUNTO CON LAS PALABRAS CLAVES ********/
    --reemplazo los espacios por guiones para parsear las palabras
    str := replace(trim(p_frase_Clave), ' ', '_');
    
    -- Separo la primer palabra y comienzo a armar la lista de palabas
    if instr(str, '_') > 0 then
      i := instr(str, '_');
    else
      i := length(str)+1;
    end if;
    str_aux := substr(str, 1, i-1);
    str := substr(str, i+1);
    indice_clave := 1;
    WHILE length(str_aux) > 0 LOOP
      -- solo tengo en cuenta palabras de mas de 3 letras, y guardo su valor fonetico.
      if length(str_aux) > 3 then
        name_array_clave(indice_clave) := IPJ.PKG_SND.soundesp(str_aux);
        indice_clave := indice_clave + 1;
      end if;
      -- Separo la siguiente palabra o me quedo con la ultima
      if instr(str, '_') > 0 then
        i := instr(str, '_');
      else
        i := length(str)+1;
      end if;
      str_aux := substr(str, 1, i-1);
      str := substr(str, i+1);
    END LOOP;
    
    /**** ARMO UN CONJUNTO CON LAS PALABRAS A COMPARAR ********/
    --reemplazo los espacios por guiones para parsear las palabras
    str := replace(trim(p_frase_comparar), ' ', '_');
    
    -- Separo la primer palabra y comienzo a armar la lista de palabas
    if instr(str, '_') > 0 then
      i := instr(str, '_');
    else
      i := length(str)+1;
    end if;
    str_aux := substr(str, 1, i-1);
    str := substr(str, i+1);
    indice_comp := 1;
    
    WHILE length(str_aux) > 0 LOOP
      -- solo tengo en cuenta palabras de mas de 3 letras, y guardo su valor fionetico.
      if length(str_aux) > 3 then
        name_array_comp(indice_comp) := IPJ.PKG_SND.soundesp(str_aux);
        indice_comp := indice_comp +1;
      end if;
      -- Separo la siguiente palabra o me quedo con la ultima
      if instr(str, '_') > 0 then
        i := instr(str, '_');
      else
        i := length(str)+1;
      end if;
      str_aux := substr(str, 1, i-1);
      str := substr(str, i+1);
    END LOOP;
    
    
    /**** COMPARO LAS PALABRAS ********/
    ret := 0;
    FOR i IN 1 .. indice_clave-1 LOOP
      j := 1;
      WHILE j < indice_comp LOOP
        -- Si foneticamente son similares (70%), la acepto
        --DBMS_OUTPUT.PUT_LINE('Palaba Clave = ' || name_array_clave(i));
        --DBMS_OUTPUT.PUT_LINE('Palaba Comp = ' || name_array_comp(j));
        if name_array_clave(i) = name_array_comp(j)  then
        --if name_array_clave(i) like '%' || name_array_comp(j) || '%' then
          v_ret := 1;
          j := indice_comp;-- Si lo encontro, avanzo para salir del LOOP
        else
          v_ret := 0;
          j := j + 1;
        end if;
        
      END LOOP;
      ret := ret +nvl( v_ret, 0);
    END LOOP;

    RETURN ret;
  END FC_Comparar_Fonetica;

END pkg_snd;
/

