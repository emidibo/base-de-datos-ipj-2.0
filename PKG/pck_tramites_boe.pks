create or replace package ipj.PCK_TRAMITES_BOE
as
/*********************************************************
  En este package se incluyten todos los procesos a los que debe acceder BOE para
  poder manejar la integracion de los edictos.
  Debe tener permiso de ejecucion el usuario de BOE
*********************************************************/

  PROCEDURE SP_BOE_NOTIF_PUBLIC_CANCEL (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_nro_edicto in number
  );

  PROCEDURE SP_BOE_NOTIF_PUBLIC_FINALIZ (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_nro_edicto in number,
    p_fecha_publicacion in date
  );

  PROCEDURE SP_BOE_CONTINGENCIA (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_nro_edicto in number,
    p_fecha_publicacion in date,
    p_estado IN NUMBER
  );

end PCK_TRAMITES_BOE;
/

create or replace package body ipj.PCK_TRAMITES_BOE is
/*********************************************************
  En este package se incluyten todos los procesos a los que debe acceder BOE para
  poder manejar la integracion de los edictos.
  Debe tener permiso de ejecucion el usuario de BOE
*********************************************************/

  PROCEDURE SP_BOE_NOTIF_PUBLIC_CANCEL (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_nro_edicto in number
  )
  /*********************************************************
    Este procedimiento marca el edicto indicado como cancelado, asignandole como
    fecha de cancelaci�n el d�a en que se llama al procedimiento
  *********************************************************/
  IS
    v_id_tramite_ipj NUMBER;
    v_id_tramite_ol  NUMBER;
    v_cant_trm       NUMBER;
    v_cant_trm_ol    NUMBER;
    v_area_tramite   NUMBER;
    v_acc_edicto     NUMBER;
    PRAGMA AUTONOMOUS_TRANSACTION;
  BEGIN

    if IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('HAB_LOG_BOE') = '1' then
      IPJ.VARIOS.SP_GUARDAR_LOG(
        P_METODO => 'SP_BOE_NOTIF_PUBLIC_CANCEL',
        P_NIVEL => 'BOE',
        P_ORIGEN => 'DB',
        P_USR_LOG  => 'DB',
        P_USR_WINDOWS => 'Sistema',
        P_IP_PC  => NULL,
        P_EXCEPCION =>
          ' Nro Edicto = ' || to_char(p_nro_edicto)
      );
    end if;

   --Veo que exista un tramite para el edicto en gestion o en portal.
   SELECT COUNT(1)
     INTO v_cant_trm
     FROM ipj.t_entidades_edicto_boe
    WHERE nro_edicto = p_nro_edicto;

   SELECT COUNT(1)
     INTO v_cant_trm_ol
     FROM ipj.t_ol_entidades_edicto_boe
    WHERE nro_edicto = p_nro_edicto;

    --Si no encuentro ninguno logueo Error
    IF v_cant_trm = 0 AND v_cant_trm_ol = 0 THEN

      o_rdo := 'ERROR: No se encontr� un tr�mite asociado al Edicto Nro. ' || to_char(p_nro_edicto);
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;

      IF IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('HAB_LOG_BOE') = '1' THEN
        IPJ.VARIOS.SP_GUARDAR_LOG(
          P_METODO => 'SP_BOE_NOTIF_PUBLIC_CANCEL',
          P_NIVEL => 'BOE',
          P_ORIGEN => 'DB',
          P_USR_LOG  => 'DB',
          P_USR_WINDOWS => 'Sistema',
          P_IP_PC  => NULL,
          P_EXCEPCION => 'Error Edict ' || to_char(p_nro_edicto) || '  - ' || o_rdo
        );
      END IF;
      RETURN;

    -- Si encuentro en Gesti�n actualizo
    ELSIF v_cant_trm <> 0 THEN

      SELECT id_tramite_ipj
        INTO v_id_tramite_ipj
        FROM ipj.t_entidades_edicto_boe
       WHERE nro_edicto = p_nro_edicto;

      BEGIN
        SELECT id_ubicacion_origen
          INTO v_area_tramite
          FROM ipj.t_tramitesipj
         WHERE id_tramite_ipj = v_id_tramite_ipj;
      EXCEPTION
        WHEN no_data_found THEN
         v_area_tramite:= ipj.types.c_area_sxa;
        WHEN OTHERS THEN
         v_area_tramite:= ipj.types.c_area_sxa;
      END;

      -- Corroboro la ubicaci�n del tramite
      IF v_area_tramite IN (ipj.types.c_area_sxa) THEN
        -- Obtengo el id de Generacion Edicto BOE
        v_acc_edicto := to_number(IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('ACC_EDICTO_BOE'));
      ELSIF v_area_tramite IN (ipj.types.c_area_srl) THEN
        v_acc_edicto := to_number(IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('ACC_EDICTO_BOE_RP'));
      END IF;

      -- Actualizo los datos del edicto
      update ipj.T_Entidades_Edicto_Boe
      set
        fecha_cancela = sysdate,
        id_estado_boe = 4, -- CANCELADO BOE
        enviar_mail = 'S'
      where
        nro_edicto = p_nro_edicto;

      -- Marco la accion del Edicto como Cancelado BOE
      update ipj.t_tramitesipj_acciones
      set id_estado = 17 -- Cancelado BOE
      where
        id_tramite_ipj = v_id_tramite_ipj and
        id_estado = 18 AND --Estaba en 15 - PendienteBOE. Se modifica al agregar el estado pendiente nuevo
        id_tipo_accion = v_acc_edicto;

      -- Paso las otras acciones del tr�mite de Pendiuentes de BOE, como En Proceso
      update ipj.t_tramitesipj_acciones
      set id_estado = IPJ.TYPES.C_ESTADOS_PROCESO
      where
        id_tramite_ipj = v_id_tramite_ipj and
        id_estado = IPJ.TYPES.C_ESTADOS_BOE and
        id_tipo_accion <> v_acc_edicto;

      o_rdo := IPJ.TYPES.C_RESP_OK;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
      commit;

      -- Si encuentro en Portal actualizo
    ELSE

/*      -- VEr que se define

       SELECT codigo_online
        INTO v_id_tramite_ol
        FROM ipj.t_ol_entidades_edicto_boe
       WHERE nro_edicto = p_nro_edicto;*/

        -- Actualizo los datos del edicto
      update ipj.T_ol_Entidades_Edicto_Boe
      set
        fecha_cancela = SYSDATE
      where
        nro_edicto = p_nro_edicto;
      commit;

    END IF;


  EXCEPTION
     WHEN OTHERS THEN
       o_rdo := 'SP_BOE_NOTIF_PUBLIC_CANCEL: ' || To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
       rollback;

       if IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('HAB_LOG_BOE') = '1' then
          IPJ.VARIOS.SP_GUARDAR_LOG(
            P_METODO => 'SP_BOE_NOTIF_PUBLIC_CANCEL',
            P_NIVEL => 'BOE',
            P_ORIGEN => 'DB',
            P_USR_LOG  => 'DB',
            P_USR_WINDOWS => 'Sistema',
            P_IP_PC  => NULL,
            P_EXCEPCION => 'Error Edict ' || to_char(p_nro_edicto) || '  - ' || o_rdo
          );
          commit;
        end if;
  END SP_BOE_NOTIF_PUBLIC_CANCEL;

  PROCEDURE SP_BOE_NOTIF_PUBLIC_FINALIZ (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_nro_edicto in number,
    p_fecha_publicacion in date
  )
  IS
    v_id_tramite_ipj NUMBER;
    v_id_tramite_ol  NUMBER;
    v_cant_trm       NUMBER;
    v_cant_trm_ol    NUMBER;
    v_area_tramite   NUMBER;
    v_acc_edicto     NUMBER;
    PRAGMA AUTONOMOUS_TRANSACTION;
  BEGIN

    if IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('HAB_LOG_BOE') = '1' then
      IPJ.VARIOS.SP_GUARDAR_LOG(
        P_METODO => 'SP_BOE_NOTIF_PUBLIC_FINALIZ',
        P_NIVEL => 'BOE',
        P_ORIGEN => 'DB',
        P_USR_LOG  => 'DB',
        P_USR_WINDOWS => 'Sistema',
        P_IP_PC  => NULL,
        P_EXCEPCION =>
          ' Nro Edicto = ' || to_char(p_nro_edicto)
          || ' / Fecha Publicacion = ' || to_char(p_fecha_publicacion, 'dd/mm/rrrr')
      );
    end if;

    if p_fecha_publicacion is null or IPJ.VARIOS.Valida_Fecha(p_fecha_publicacion) = 'N'  then
      o_rdo := 'ERROR: La fecha de publicaci�n est� vac�a o es inv�lida (dd/mm/yyyy).';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;

      if IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('HAB_LOG_BOE') = '1' then
          IPJ.VARIOS.SP_GUARDAR_LOG(
            P_METODO => 'SP_BOE_NOTIF_PUBLIC_FINALIZ',
            P_NIVEL => 'BOE',
            P_ORIGEN => 'DB',
            P_USR_LOG  => 'DB',
            P_USR_WINDOWS => 'Sistema',
            P_IP_PC  => NULL,
            P_EXCEPCION => 'Error Edicto ' || to_char(p_nro_edicto) || '  - ' || o_rdo
          );
      end if;

      return;
    end if;

    -- Veo que exista un tr�mite para el edicto en gestion con estado <> 5 (Cancelado IPJ), o en portal cancelado
   SELECT COUNT(1)
     INTO v_cant_trm
     FROM ipj.t_entidades_edicto_boe
    WHERE nro_edicto = p_nro_edicto
      AND id_estado_boe <> 5;

   SELECT COUNT(1)
     INTO v_cant_trm_ol
     FROM ipj.t_ol_entidades_edicto_boe
    WHERE nro_edicto = p_nro_edicto
      AND fecha_cancela IS NULL;

   --Si no encuentro ninguno logueo Error
   IF v_cant_trm = 0 AND v_cant_trm_ol = 0 THEN

        o_rdo := 'ERROR: No se encontr� un tr�mite asociado al Edicto Nro. ' || to_char(p_nro_edicto);
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;

        IF IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('HAB_LOG_BOE') = '1' THEN
          IPJ.VARIOS.SP_GUARDAR_LOG(
            P_METODO => 'SP_BOE_NOTIF_PUBLIC_FINALIZ',
            P_NIVEL => 'BOE',
            P_ORIGEN => 'DB',
            P_USR_LOG  => 'DB',
            P_USR_WINDOWS => 'Sistema',
            P_IP_PC  => NULL,
            P_EXCEPCION => 'EDICTO CANCELADO IPJ (' || to_char(p_nro_edicto) || ')  - ' || o_rdo
          );
        END IF;
        RETURN;

   -- Si encuentro en Gesti�n actualizo
   ELSIF v_cant_trm <> 0 THEN

      SELECT id_tramite_ipj
        INTO v_id_tramite_ipj
        FROM ipj.t_entidades_edicto_boe
       WHERE nro_edicto = p_nro_edicto
         AND id_estado_boe <> 5;

      BEGIN
        SELECT id_ubicacion_origen
          INTO v_area_tramite
          FROM ipj.t_tramitesipj
         WHERE id_tramite_ipj = v_id_tramite_ipj;
      EXCEPTION
        WHEN no_data_found THEN
         v_area_tramite:= ipj.types.c_area_sxa;
        WHEN OTHERS THEN
         v_area_tramite:= ipj.types.c_area_sxa;
      END;

      -- Corroboro la ubicaci�n del tramite
      IF v_area_tramite IN (ipj.types.c_area_sxa) THEN
        -- Obtengo el id de Generacion Edicto BOE
        v_acc_edicto := to_number(IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('ACC_EDICTO_BOE'));
      ELSIF v_area_tramite IN (ipj.types.c_area_srl) THEN
        v_acc_edicto := to_number(IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('ACC_EDICTO_BOE_RP'));
      END IF;

      -- Actualizo los datos del edicto
      update ipj.T_Entidades_Edicto_Boe
      set
        id_estado_boe = 3, -- FINALIZADO BOE
        fecha_publica = p_fecha_publicacion,
        enviar_mail = 'S'
      where
        nro_edicto = p_nro_edicto;

      -- Marco la accion del Edicto como Publicado BOE
      update ipj.t_tramitesipj_acciones
      set id_estado = 16 -- Publicado BOE
      where
        id_tramite_ipj = v_id_tramite_ipj and
        id_estado = 18 and --Estaba en 15 - PendienteBOE. Se modifica al agregar el estado pendiente nuevo
        id_tipo_accion = v_acc_edicto;

      -- Paso las otras acciones del tr�mite de Pendiuentes de BOE, como En Proceso
      update ipj.t_tramitesipj_acciones
      set id_estado = IPJ.TYPES.C_ESTADOS_PROCESO
      where
        id_tramite_ipj = v_id_tramite_ipj and
        id_estado = IPJ.TYPES.C_ESTADOS_BOE and
        id_tipo_accion <> v_acc_edicto;

      o_rdo := IPJ.TYPES.C_RESP_OK;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
      commit;

   -- Si encuentro en Portal actualizo
   ELSE

      /*      -- VEr que se define

       SELECT codigo_online
        INTO v_id_tramite_ol
        FROM ipj.t_ol_entidades_edicto_boe
       WHERE nro_edicto = p_nro_edicto
         AND fecha_cancela IS NULL;*/

     -- Actualizo los datos del edicto
      update ipj.T_ol_Entidades_Edicto_Boe
      set
        tramite_cerrado = 'S', -- FINALIZADO BOE ????
        fecha_publica = p_fecha_publicacion
      where
        nro_edicto = p_nro_edicto;
      commit;

   END IF;

    EXCEPTION
      WHEN OTHERS THEN
        o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
        rollback;

        if IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('HAB_LOG_BOE') = '1' then
          IPJ.VARIOS.SP_GUARDAR_LOG(
            P_METODO => 'SP_BOE_NOTIF_PUBLIC_FINALIZ',
            P_NIVEL => 'BOE',
            P_ORIGEN => 'DB',
            P_USR_LOG  => 'DB',
            P_USR_WINDOWS => 'Sistema',
            P_IP_PC  => NULL,
            P_EXCEPCION => 'Error Edicto ' || to_char(p_nro_edicto) || '  - ' || o_rdo
          );
          commit;
        end if;
  END SP_BOE_NOTIF_PUBLIC_FINALIZ;

  PROCEDURE SP_BOE_CONTINGENCIA (
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_nro_edicto in number,
    p_fecha_publicacion in date,
    p_estado IN NUMBER
  )
  IS
    v_id_tramite_ipj number;
    v_area_tramite   NUMBER;
    v_acc_edicto     NUMBER;
    PRAGMA AUTONOMOUS_TRANSACTION;
  BEGIN
    if IPJ.Types.c_habilitar_log_SP = 'S' then
      IPJ.VARIOS.SP_GUARDAR_LOG(
        P_METODO => 'SP_BOE_CONTINGENCIA',
        P_NIVEL => 'BOE',
        P_ORIGEN => 'DB',
        P_USR_LOG  => 'DB',
        P_USR_WINDOWS => 'Sistema',
        P_IP_PC  => NULL,
        P_EXCEPCION =>
          ' Nro Edicto = ' || to_char(p_nro_edicto)
          || ' / Fecha Publicacion = ' || to_char(p_fecha_publicacion, 'dd/mm/rrrr') || ' Estado = ' || p_estado
      );
    end if;

    --Busco el tramite asociado al edicto
    begin
      select id_tramite_ipj into v_id_tramite_ipj
      from ipj.T_Entidades_Edicto_Boe
      where
        nro_edicto = p_nro_edicto;
    exception
      when NO_DATA_FOUND then
        o_rdo := 'ERROR: No se encontr� un tr�mite asociado al Edicto Nro. ' || to_char(p_nro_edicto);
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;

        if IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('HAB_LOG_BOE') = '1' then
          IPJ.VARIOS.SP_GUARDAR_LOG(
            P_METODO => 'SP_BOE_CONTINGENCIA',
            P_NIVEL => 'BOE',
            P_ORIGEN => 'DB',
            P_USR_LOG  => 'DB',
            P_USR_WINDOWS => 'Sistema',
            P_IP_PC  => NULL,
            P_EXCEPCION => 'Error Edicto ' || to_char(p_nro_edicto) || '  - ' || o_rdo
          );
        end if;
        return;
    end;

    BEGIN
        SELECT id_ubicacion_origen
          INTO v_area_tramite
          FROM ipj.t_tramitesipj
         WHERE id_tramite_ipj = v_id_tramite_ipj;
      EXCEPTION
        WHEN no_data_found THEN
         v_area_tramite:= ipj.types.c_area_sxa;
        WHEN OTHERS THEN
         v_area_tramite:= ipj.types.c_area_sxa;
      END;

      -- Corroboro la ubicaci�n del tramite
      IF v_area_tramite IN (ipj.types.c_area_sxa) THEN
        -- Obtengo el id de Generacion Edicto BOE
        v_acc_edicto := to_number(IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('ACC_EDICTO_BOE'));
      ELSIF v_area_tramite IN (ipj.types.c_area_srl) THEN
        v_acc_edicto := to_number(IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('ACC_EDICTO_BOE_RP'));
      END IF;

    -- Actualizo los datos del edicto
    update ipj.T_Entidades_Edicto_Boe
    set
      id_estado_boe = decode(p_estado,11,3,21,4,id_estado_boe), -- Seteo estado BOE
      fecha_publica = decode(p_estado,11,p_fecha_publicacion,NULL),
      fecha_cancela = decode(p_estado,21,p_fecha_publicacion,NULL),
      enviar_mail = 'S'
    where
      nro_edicto = p_nro_edicto;

    -- Marco la accion del Edicto como Publicado BOE
    update ipj.t_tramitesipj_acciones
    set id_estado = decode(p_estado,11,16,21,17,id_estado) -- Seteo estado tr�mite
    where
      id_tramite_ipj = v_id_tramite_ipj and
      id_estado = 18 and
      id_tipo_accion = v_acc_edicto;

    -- Paso las otras acciones del tr�mite de Pendientes de BOE, como En Proceso
    update ipj.t_tramitesipj_acciones
    set id_estado = IPJ.TYPES.C_ESTADOS_PROCESO
    where
      id_tramite_ipj = v_id_tramite_ipj and
      id_estado = IPJ.TYPES.C_ESTADOS_BOE and
      id_tipo_accion <> v_acc_edicto;

    o_rdo := IPJ.TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    commit;
  EXCEPTION
     WHEN OTHERS THEN
       o_rdo := To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
       rollback;

       if IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('HAB_LOG_BOE') = '1' then
          IPJ.VARIOS.SP_GUARDAR_LOG(
            P_METODO => 'SP_BOE_CONTINGENCIA',
            P_NIVEL => 'BOE',
            P_ORIGEN => 'DB',
            P_USR_LOG  => 'DB',
            P_USR_WINDOWS => 'Sistema',
            P_IP_PC  => NULL,
            P_EXCEPCION => 'Error Edicto ' || to_char(p_nro_edicto) || '  - ' || o_rdo
          );
          Commit;
       end if;
  END SP_BOE_CONTINGENCIA;

end PCK_TRAMITES_BOE;
/

