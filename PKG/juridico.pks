create or replace package ipj.Juridico
as
  PROCEDURE SP_Traer_Listado_Boletin(
    o_Cursor OUT types.cursorType,
    o_rdo out varchar2,
    p_fec_desde in varchar2,
    p_fec_hasta in varchar2
  );
    
  PROCEDURE SP_Guardar_Envio_Boletin(  
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in number,
    p_id_tramiteipj_accion in number,
    p_id_tipo_accion in number,
    p_cuil_usuario in varchar2
  );
    
  PROCEDURE SP_Traer_Sentencias(
    o_Cursor OUT types.cursorType,
    p_id_tramite_ipj in number,
    p_id_tramiteipj_accion in number
  );
  
  PROCEDURE SP_Traer_Denuncia(
    o_Cursor OUT types.cursorType,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number
  );
  
  PROCEDURE SP_Traer_Denunciantes(
    o_Cursor OUT types.cursorType,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number
  );
  
  PROCEDURE SP_Traer_Denunciados(
    o_Cursor OUT types.cursorType,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number
  );
  
  PROCEDURE SP_Traer_Denuncia_Medios(
    o_Cursor OUT types.cursorType,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number
  );
  
  PROCEDURE SP_Traer_Denuncia_Motivo(
    o_Cursor OUT types.cursorType,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number
  );
  
  PROCEDURE SP_GUARDAR_DENUNCIA_MEDIOS(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number,
    p_medio_prueba varchar2,
    p_observacion varchar2,
    p_eliminar in number -- 1 elimina
  );
  
  PROCEDURE SP_GUARDAR_DENUNCIA_MOTIVO(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number,
    p_id_motivo_denuncia in number,
    p_eliminar in number -- 1 elimina
  );
  
  PROCEDURE SP_GUARDAR_DENUNCIA(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number,
    p_uso_via_interna in varchar2,
    p_hecho_denuncia in clob,
    p_desconoce_dom in char,
    p_fec_resol_cn in varchar2, -- Es Fecha
    p_fecha_cargos in varchar2 -- Es Fecha
  );
  
  PROCEDURE SP_GUARDAR_DENUNCIADOS(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number,
    -- Datos Persona
    p_id_integrante in number,
    p_id_sexo in varchar2,
    p_nro_documento in varchar2,
    p_pai_cod_pais in varchar2,
    p_id_numero in number,
    p_detalle in varchar2,
    p_nombre in varchar2,
    p_apellido in varchar2,
    p_n_tipo_documento in varchar2,
    -- Domicilio Denunciado
    p_id_vin_den in number,
    p_id_provincia in varchar2,
    p_id_departamento in number,
    p_id_localidad in number,
    p_barrio in varchar2,
    p_calle in varchar2,
    p_altura in number,
    p_depto in varchar2,
    p_piso in varchar2,
    p_torre in varchar2,
    p_manzana in varchar2,
    p_lote in varchar2,
    p_id_tipocalle in number,
    p_km in varchar,
    p_id_calle in number, 
    p_id_barrio in number,
    -- Otros
    p_eliminar in number -- 1 elimina
  );
  
  PROCEDURE SP_GUARDAR_DENUNCIANTES(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number,
    -- Datos Personas
    p_id_integrante in number,
    p_id_sexo in varchar2,
    p_nro_documento in varchar2,
    p_pai_cod_pais in varchar2,
    p_id_numero in number,
    p_detalle in varchar2,
    p_nombre in varchar2,
    p_apellido in varchar2,
    p_n_tipo_documento in varchar2,
     -- Domicilio Real
    p_id_vin in number,
    p_id_provincia in varchar2,
    p_id_departamento in number,
    p_id_localidad in number,
    p_barrio in varchar2,
    p_calle in varchar2,
    p_altura in number,
    p_depto in varchar2,
    p_piso in varchar2,
    p_torre in varchar2,
    p_manzana in varchar2,
    p_lote in varchar2,
    p_id_tipocalle in number,
    p_km in varchar,
    p_id_calle in number, 
    p_id_barrio in number,
    -- Otros
    p_eliminar in number -- 1 elimina
  );
  
  PROCEDURE SP_GUARDAR_DENUNCIA_DOMICILIO(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_vin out number,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number,
    p_id_vin_den in number,
    P_ID_PROVINCIA in varchar2,
    P_ID_DEPARTAMENTO in number,
    P_ID_LOCALIDAD in number,
    P_BARRIO in varchar2,
    P_CALLE in varchar2,
    P_ALTURA in number,
    P_DEPTO in varchar2,
    P_PISO in varchar2,
    p_torre in varchar2,
    p_manzana in varchar2,
    p_lote in varchar2,
    p_id_tipocalle in number,
    p_km in varchar,
    p_id_calle in number, 
    p_id_barrio in number
  );
  
  PROCEDURE SP_Traer_CN_Normalizadores(
    o_Cursor OUT types.cursorType,
    p_id_tramite_ipj in number,
    p_id_legajo in number
  );
  
  PROCEDURE SP_Traer_CN_Solicitante(
    o_Cursor OUT types.cursorType,
    p_id_tramite_ipj in number,
    p_id_legajo in number
  );
  
  PROCEDURE SP_Traer_CN_Motivo(
    o_Cursor OUT types.cursorType,
    p_id_tramite_ipj in number,
    p_id_legajo in number
  );
  
  PROCEDURE Sp_Guardar_CN_Motivo(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in Number,
    p_id_legajo in number,
    p_id_motivo_comision in number,
    p_eliminar in number -- 1 Elimina
  );
  
  PROCEDURE SP_Guardar_CN_Normalizador(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in number,
    p_id_legajo in number,
    -- Datos Persona
    p_id_integrante in number,
    p_id_sexo in varchar2,
    p_nro_documento in varchar2,
    p_pai_cod_pais in varchar2,
    p_id_numero in number,
    p_detalle in varchar2,
    p_nombre in varchar2,
    p_apellido in varchar2,
    p_n_tipo_documento in varchar2,
    -- Particulares
    p_cuil in varchar2,
    p_id_normalizador in number,
    p_id_motivo_baja in number, 
    p_id_estado_normalizador in number,
    p_mail in varchar2,
    p_telefono in varchar2,
    p_eliminar in number, -- 1 Elimina
    -- Domicilio Real
    p_id_vin_real in number,
    p_Id_Provincia_real In Varchar2,
    p_Id_Departamento_real In Number,
    p_Id_Localidad_real In Number,
    p_Barrio_real In Varchar2,
    p_Calle_real In Varchar2,
    p_Altura_real In Number,
    p_Depto_real In Varchar2,
    p_Piso_real In Varchar2,
    p_Torre_real In Varchar2,
    p_manzana_real in varchar2,
    p_lote_real in varchar2,
    p_id_barrio_real in number,
    p_id_calle_real in number,
    p_id_tipocalle_real in number,
    p_km_real in varchar2
  );
  
  PROCEDURE SP_Guardar_CN_Solicitante(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in number,
    p_id_legajo in number,
    -- Datos Personas
    p_id_integrante in number,
    p_id_sexo in varchar2,
    p_nro_documento in varchar2,
    p_pai_cod_pais in varchar2,
    p_id_numero in number,
    p_detalle in varchar2,
    p_nombre in varchar2,
    p_apellido in varchar2,
    p_n_tipo_documento in varchar2,
    -- Varios
    p_mail in varchar2,
    p_telefono in varchar2,
    p_eliminar in number, -- 1 Elimina
    -- Domicilio Real
    p_id_vin_real in number,
    p_Id_Provincia_real In Varchar2,
    p_Id_Departamento_real In Number,
    p_Id_Localidad_real In Number,
    p_Barrio_real In Varchar2,
    p_Calle_real In Varchar2,
    p_Altura_real In Number,
    p_Depto_real In Varchar2,
    p_Piso_real In Varchar2,
    p_Torre_real In Varchar2,
    p_manzana_real in varchar2,
    p_lote_real in varchar2,
    p_id_barrio_real in number,
    p_id_calle_real in number,
    p_id_tipocalle_real in number,
    p_km_real in varchar2,
    -- Particulares
    p_id_solicitante_cn in number
  );
  
end Juridico;
/

create or replace package body ipj.Juridico is

  PROCEDURE SP_Traer_Listado_Boletin(
    o_Cursor OUT types.cursorType,
    o_rdo out varchar2,
    p_fec_desde in varchar2,
    p_fec_hasta in varchar2)
  IS
    v_valid_parametros varchar2(500);
  BEGIN
    -- Valida que el rango de fechas sea correcto
     if IPJ.VARIOS.Valida_Fecha(p_fec_desde) = 'N' or
        IPJ.VARIOS.Valida_Fecha(p_fec_hasta) = 'N' or
        To_Date(p_fec_hasta, 'dd/mm/rrrr') < To_Date(p_fec_desde, 'dd/mm/rrrr') then
        v_valid_parametros := v_valid_parametros || IPJ.VARIOS.MENSAJE_ERROR('FEC_NOT');
     end if;

    if v_valid_parametros is not null then
      o_rdo := 'GUARDAR ENTIDAD - Parametros : ' || v_valid_parametros;
      return;
    end if;

  -- Cuerpo del SP
    OPEN o_Cursor FOR
      select
        VT_SUAC.nro_sticker Sticker, VT_SUAC.NRO_TRAMITE Expediente,
        (case tIPJ.id_proceso
           when 4 then 'GENERACION DE HISTORICOS'
           when 5 then 'carlitos'
           else VT_SUAC.Asunto
        end) Asunto,
        AccIPJ.id_tramite_ipj, AccIPJ.Id_tramiteipj_Accion, ACCIPJ.ID_TIPO_ACCION,
        TA.N_TIPO_ACCION, U.DESCRIPCION,
        nvl(nvl(L.DENOMINACION_SIA, I.DETALLE), FC.N_FONDO_COMERCIO) Identificacion
      from
        ipj.t_tramitesipj_acciones AccIPJ JOIN ipj.t_tramitesIPJ tIPJ
          ON AccIPJ.Id_Tramite_IPJ = tIPJ.id_tramite_ipj
        JOIN ipj.t_tipos_AccionesIpj ta
          ON ACCIPJ.ID_TIPO_ACCION = ta.id_tipo_accion
        JOIN NUEVOSUAC.VT_TRAMITES_IPJ_DESA vt_suac
          ON VT_SUAC.ID_TRAMITE = tIPJ.id_tramite
        join IPJ.t_USUARIOS u
          on U.CUIL_USUARIO = ACCIPJ.CUIL_USUARIO
        LEFT JOIN T_TramitesIpj_PersJur TRPJ
          ON TIPJ.ID_TRAMITE_IPJ = TRPJ.ID_TRAMITE_IPJ  AND ACCIPJ.id_legajo = TRPJ.id_legajo
        LEFT JOIN IPJ.T_Legajos L
          ON ACCIPJ.id_legajo = L.id_legajo
        LEFT JOIN IPJ.T_TRAMITESIPJ_INTEGRANTE trInt
          ON ACCIPJ.ID_INTEGRANTE = TRINT.ID_INTEGRANTE AND AccIPJ.Id_tramite_ipj = trInt.id_tramite_ipj
        LEFT JOIN IPJ.T_INTEGRANTES i
          ON AccIPJ.ID_INTEGRANTE = i.ID_INTEGRANTE
        LEFT JOIN IPJ.t_Fondos_comercio fc
          ON AccIPJ.ID_FONDO_COMERCIO = FC.ID_FONDO_COMERCIO
      where
        UPPER(TA.INFORMAR_BOLETIN) = 'S' and
        nvl(tIPJ.id_proceso, 0) <> 5 and
        AccIPJ.id_estado >= IPJ.TYPES.c_Estados_Completado and
        AccIPJ.id_estado < IPJ.TYPES.C_ESTADOS_RECHAZADO and
        not exists (select * from IPJ.T_ACCIONESIPJ_BOLETIN ab
                        where
                          ab.id_tramite_ipj = AccIPJ.id_tramite_ipj and
                          ab.id_tramiteipj_accion = AccIPJ.id_tramiteipj_accion and
                          ab.id_tipo_accion = AccIPJ.id_tipo_accion)
      UNION

      select
        '' Sticker, '' Expediente,
        (case tIPJ.id_proceso
           when 4 then 'GENERACION DE HISTORICOS'
           when 5 then 'carlitos'
           else ''
        end) Asunto,
        AccIPJ.id_tramite_ipj, AccIPJ.Id_tramiteipj_Accion, ACCIPJ.ID_TIPO_ACCION,
        TA.N_TIPO_ACCION, U.DESCRIPCION,
        nvl(nvl(L.DENOMINACION_SIA, I.DETALLE), FC.N_FONDO_COMERCIO) Identificacion
      from
        ipj.t_tramitesipj_acciones AccIPJ JOIN ipj.t_tramitesIPJ tIPJ
          ON AccIPJ.Id_Tramite_IPJ = tIPJ.id_tramite_ipj
        JOIN ipj.t_tipos_AccionesIpj ta
          ON ACCIPJ.ID_TIPO_ACCION = ta.id_tipo_accion
        join IPJ.t_USUARIOS u
          on U.CUIL_USUARIO = ACCIPJ.CUIL_USUARIO
        LEFT JOIN T_TramitesIpj_PersJur TRPJ
          ON TIPJ.ID_TRAMITE_IPJ = TRPJ.ID_TRAMITE_IPJ  AND ACCIPJ.id_legajo = TRPJ.id_legajo
        LEFT JOIN IPJ.T_Legajos L
          ON ACCIPJ.id_legajo = L.id_legajo
        LEFT JOIN IPJ.T_TRAMITESIPJ_INTEGRANTE trInt
          ON ACCIPJ.ID_INTEGRANTE = TRINT.ID_INTEGRANTE AND AccIPJ.Id_tramite_ipj = trInt.id_tramite_ipj
        LEFT JOIN IPJ.T_INTEGRANTES i
          ON AccIPJ.ID_INTEGRANTE = i.ID_INTEGRANTE
        LEFT JOIN IPJ.t_Fondos_comercio fc
          ON AccIPJ.ID_FONDO_COMERCIO = FC.ID_FONDO_COMERCIO
      where
        tIPJ.id_tramite is null and
        --UPPER(TA.INFORMAR_BOLETIN) = 'S' and
        AccIPJ.id_estado >= IPJ.TYPES.c_Estados_Completado and
        AccIPJ.id_estado < IPJ.TYPES.C_ESTADOS_RECHAZADO and
        not exists (select * from IPJ.T_ACCIONESIPJ_BOLETIN ab
                        where
                          ab.id_tramite_ipj = AccIPJ.id_tramite_ipj and
                          ab.id_tramiteipj_accion = AccIPJ.id_tramiteipj_accion and
                          ab.id_tipo_accion = AccIPJ.id_tipo_accion)
    ;

    o_rdo := IPJ.TYPES.C_RESP_OK;

  END SP_Traer_Listado_Boletin;

  PROCEDURE SP_Guardar_Envio_Boletin(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in number,
    p_id_tramiteipj_accion in number,
    p_id_tipo_accion in number,
    p_cuil_usuario in varchar2)
  IS
  BEGIN

    insert into IPJ.T_ACCIONESIPJ_BOLETIN (id_tramite_ipj, id_tramiteipj_accion, id_tipo_Accion, fecha, cuil_usuario)
    values (p_id_tramite_ipj, p_id_tramiteipj_accion, p_id_tipo_Accion, sysdate, p_cuil_usuario);

    o_rdo := TYPES.C_RESP_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;

  END SP_Guardar_Envio_Boletin;

  PROCEDURE SP_Traer_Sentencias(
    o_Cursor OUT types.cursorType,
    p_id_tramite_ipj in number,
    p_id_tramiteipj_accion in number)
  IS
    v_id_legajo number(10);
    v_id_integrante number(10);
    v_id_fondo_comercio number(10);
  BEGIN
    -- Busco la Entidad, Persona o Fondo asociado al tramite
    select id_legajo, id_integrante, id_fondo_comercio into v_id_legajo, v_id_integrante, v_id_fondo_comercio
    from IPJ.t_tramitesipj_acciones
    where
      id_tramite_ipj = p_id_tramite_ipj and
      id_tramiteipj_accion = p_id_tramiteipj_accion;

    -- Listo todas las acciones para la misma entidad, persona o fondo, que esten cerradas y no rechazadas.
    OPEN o_Cursor FOR
       select
         AccIPJ.id_tramite_ipj, AccIPJ.id_tramiteipj_accion, to_char(tIPJ.Fecha_Inicio, 'dd/mm/rrrr') Fecha_Inicio,
         ta.N_tipo_Accion, u.descripcion, AccIPJ.n_documento, AccIPJ.Id_Documento,
         TIPJ.EXPEDIENTE
        from
          ipj.t_tramitesipj_acciones AccIPJ JOIN ipj.t_tramitesIPJ tIPJ
            ON AccIPJ.Id_Tramite_IPJ = tIPJ.id_tramite_ipj
          JOIN ipj.t_tipos_AccionesIpj ta
            ON ACCIPJ.ID_TIPO_ACCION = ta.id_tipo_accion
          JOIN ipj.t_tipos_clasif_ipj clas
            on ta.id_clasif_ipj = clas.id_clasif_ipj
          join IPJ.t_USUARIOS u
            on U.CUIL_USUARIO = ACCIPJ.CUIL_USUARIO
        where
          AccIPJ.id_estado >= IPJ.TYPES.c_Estados_Completado and
          AccIPJ.id_estado < IPJ.TYPES.C_ESTADOS_RECHAZADO and
          nvl(AccIPJ.id_legajo, 0) = nvl(v_id_legajo, 0) and
          nvl(AccIPJ.id_integrante, 0) = nvl(v_id_integrante, 0) and
          nvl(AccIPJ.id_fondo_comercio, 0) = nvl(v_id_fondo_comercio, 0) and
          clas.id_ubicacion= IPJ.Types.c_area_juridico
        order by tIPJ.Fecha_Inicio, AccIPJ.id_tramiteipj_accion desc;

  END SP_Traer_Sentencias;

  PROCEDURE SP_Traer_Denuncia(
    o_Cursor OUT types.cursorType,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number)
  IS
   /**********************************************************
    Trae los datos de una denuncia
  **********************************************************/
    v_id_vin_empresa number;
    v_n_lodalidad varchar2(100);
    v_n_departamento varchar2(100);
    v_calle_inf varchar2(2000);
    v_rdo varchar2(2000);
    v_tipo_mensaje number;
  BEGIN
    -- Cargo los documentos online que falten (post finalizar)
    IPJ.ENTIDAD_PERSJUR.SP_Bajar_Docs_Tramite(
      o_rdo => v_rdo,
      o_tipo_mensaje => v_tipo_mensaje,
      p_Id_Tramite_Ipj => p_id_tramite_ipj,
      p_id_legajo => p_id_legajo
    );
    commit;

    -- Busco el ultimo domicilio registrado por la entidad
    v_id_vin_empresa := ipj.ENTIDAD_PERSJUR.FC_Buscar_Ult_Dom(p_id_legajo, 'N');

    -- Busco los datos del domicilio de la entidad
    if nvl(v_id_vin_empresa, 0) > 0 then
      select d.n_localidad, d.n_departamento,
        ipj.varios.fc_armar_calle_dom(d.n_calle, d.altura, d.piso, d.depto, d.torre, d.mzna, d.lote, d.n_barrio, d.km, d.n_tipocalle) calle_inf
        into  v_n_lodalidad, v_n_departamento,  v_calle_inf
      from dom_manager.vt_domicilios_cond d
      where
        id_vin =  v_id_vin_empresa;
    else
      v_n_lodalidad := null;
      v_n_departamento := null;
      v_calle_inf := null;
    end if;

    -- Muestro los datos de la denuncia
    OPEN o_Cursor FOR
      select d.id_tramite_ipj, d.id_legajo, d.uso_via_interna, d.hecho_denuncia,
        d.id_vin_denuncia, d.desconoce_dom,
        IPJ.VARIOS.FC_ARMAR_CALLE_DOM(dom.n_calle, dom.altura, dom.piso, dom.depto, dom.torre, dom.mzna, dom.lote, dom.n_barrio, dom.km, dom.n_tipocalle) calle_inf,
        initcap(dom.N_Calle) N_Calle, dom.Altura, dom.Piso, dom.Depto, dom.Torre,
        initcap(dom.N_Barrio) N_Barrio, dom.mzna Manzana, dom.lote, dom.id_provincia,
        dom.Id_Departamento, dom.Id_Localidad, dom.Id_Barrio, dom.Id_Calle,
        initcap(dom.N_Localidad) Localidad, dom.km, dom.n_tipocalle, dom.id_tipocalle,
        dom.Cpa Cp, initcap(dom.N_Departamento) Departamento, initcap(dom.N_Provincia) Provincia,
        (select initcap(p.n_pais) from DOM_MANAGER.VT_PROVINCIAS pr join DOM_MANAGER.VT_PAISES p  on pr.id_pais = p.id_pais where id_provincia = dom.id_provincia) PaisDomicilio,
        v_n_lodalidad Localidad_Emp, v_n_departamento n_departamento_emp,  v_calle_inf calle_inf_emp,
        l.cuit, l.denominacion_sia, to_char(fec_resol_cn, 'dd/mm/rrrr') fec_resol_cn,
        to_char(fecha_cargos, 'dd/mm/rrrr') fecha_cargos
      from IPJ.T_denuncias d left join dom_manager.vt_domicilios_cond dom
          on decode(d.id_vin_denuncia, 0, null, d.id_vin_denuncia) = dom.id_vin
        join ipj.t_legajos l
          on l.id_legajo = d.id_legajo
      where
         d.id_legajo = p_id_legajo and
         d.id_tramite_ipj = p_id_tramite_ipj;

  END SP_Traer_Denuncia;

  PROCEDURE SP_Traer_Denunciantes(
    o_Cursor OUT types.cursorType,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number)
  IS
   /**********************************************************
    Lista las personas denunciantes
  **********************************************************/
  BEGIN
    OPEN o_Cursor FOR
      select d.id_tramite_ipj, d.id_legajo, d.Id_Integrante, p.NOV_nombre||' '||p.NOV_APELLIDO detalle,
        I.Id_Sexo, I.Id_Numero, I.Nro_Documento, I.Pai_Cod_Pais, nvl(p.cuil, I.CUIL) Cuil,
        IPJ.VARIOS.FC_Buscar_Mail(I.Id_Sexo || I.Nro_Documento || I.Pai_Cod_Pais || to_char(I.Id_Numero), IPJ.Types.c_id_Aplicacion) EMail,
        IPJ.VARIOS.FC_Buscar_Telefono(I.Id_Sexo || I.Nro_Documento || I.Pai_Cod_Pais || to_char(I.Id_Numero), IPJ.Types.c_id_Aplicacion) Telefono,
        IPJ.VARIOS.FC_Buscar_id_Profesion(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero) id_profesion,
        IPJ.VARIOS.FC_Buscar_Profesion(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero) n_profesion,
        IPJ.VARIOS.FC_Buscar_Tipo_Profesion(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero) id_tipo_caracterisica,
        i.id_sexo || i.nro_documento || i.pai_cod_pais || to_char(i.id_numero) Clave,
        dom.id_vin,
        dom.id_tipodom, dom.n_tipodom, dom.id_tipocalle, dom.n_tipocalle, dom.id_calle,
        IPJ.VARIOS.FC_ARMAR_CALLE_DOM(dom.n_calle, dom.altura, dom.piso, dom.depto, dom.torre, dom.mzna, dom.lote, dom.n_barrio, dom.km, dom.n_tipocalle) calle_inf,
        initcap(dom.n_calle) n_calle, dom.altura, dom.depto, dom.piso, dom.torre, dom.id_barrio,
        initcap(dom.n_barrio) n_barrio, dom.id_localidad, initcap(dom.n_localidad) n_localidad,
        dom.id_departamento, initcap(dom.n_departamento) n_departamento,
        dom.id_provincia, initcap(dom.n_provincia) n_provincia, dom.cpa,
        dom.mzna Manzana, dom.Lote Lote, dom.km
      from IPJ.T_DENUNCIANTES d join ipj.t_integrantes i
          on d.id_integrante = i.id_integrante
        left join RCIVIL.VT_PK_PERSONA p
          on p.id_sexo = i.id_sexo and p.nro_documento = i.nro_documento and p.pai_cod_pais = i.pai_cod_pais and p.id_numero = i.id_numero
        left join DOM_MANAGER.VT_DOMICILIOS_COND dom
          on dom.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA (i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3)
      where
        d.Id_Tramite_Ipj = p_id_tramite_ipj and
        d.id_legajo = p_id_legajo;

  END SP_Traer_Denunciantes;

  PROCEDURE SP_Traer_Denunciados(
    o_Cursor OUT types.cursorType,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number)
  IS
   /**********************************************************
    Lista las personas denunciadas en el tr�mite (si existen)
  **********************************************************/
  BEGIN
    OPEN o_Cursor FOR
      select d.id_tramite_ipj, d.id_legajo, d.Id_Integrante, p.NOV_nombre||' '||p.NOV_APELLIDO detalle,
        I.Id_Sexo, I.Id_Numero, I.Nro_Documento, I.Pai_Cod_Pais, nvl(p.cuil, I.CUIL) Cuil,
        IPJ.VARIOS.FC_Buscar_Mail(I.Id_Sexo || I.Nro_Documento || I.Pai_Cod_Pais || to_char(I.Id_Numero), IPJ.Types.c_id_Aplicacion) EMail,
        IPJ.VARIOS.FC_Buscar_Telefono(I.Id_Sexo || I.Nro_Documento || I.Pai_Cod_Pais || to_char(I.Id_Numero), IPJ.Types.c_id_Aplicacion) Telefono,
        IPJ.VARIOS.FC_Buscar_id_Profesion(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero) id_profesion,
        IPJ.VARIOS.FC_Buscar_Profesion(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero) n_profesion,
        IPJ.VARIOS.FC_Buscar_Tipo_Profesion(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero) id_tipo_caracterisica,
        i.id_sexo || i.nro_documento || i.pai_cod_pais || to_char(i.id_numero) Clave,
        d.id_vin_denuncia id_vin,
        dom.id_tipodom, dom.n_tipodom, dom.id_tipocalle, dom.n_tipocalle, dom.id_calle,
        IPJ.VARIOS.FC_ARMAR_CALLE_DOM(dom.n_calle, dom.altura, dom.piso, dom.depto, dom.torre, dom.mzna, dom.lote, dom.n_barrio, dom.km, dom.n_tipocalle) calle_inf,
        initcap(dom.n_calle) n_calle, dom.altura, dom.depto, dom.piso, dom.torre, dom.id_barrio,
        initcap(dom.n_barrio) n_barrio, dom.id_localidad, initcap(dom.n_localidad) n_localidad,
        dom.id_departamento, initcap(dom.n_departamento) n_departamento,
        dom.id_provincia, initcap(dom.n_provincia) n_provincia, dom.cpa,
        dom.mzna Manzana, dom.Lote Lote, dom.km,
        (select n_departamento from dom_manager.vt_domicilios_cond d where d.id_vin= IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA (i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3)) n_depto_real,
        (select n_localidad from dom_manager.vt_domicilios_cond d where d.id_vin= IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA (i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3)) n_localidad_real,
        IPJ.VARIOS.FC_ARMAR_CALLE_DOM(d.n_calle, d.altura, d.piso, d.depto, d.torre, d.mzna, d.lote, d.n_barrio, d.km, d.n_tipocalle) Calle_Inf_real
      from IPJ.T_DENUNCIADOS d join ipj.t_integrantes i
          on d.id_integrante = i.id_integrante
        left join RCIVIL.VT_PK_PERSONA p
          on p.id_sexo = i.id_sexo and p.nro_documento = i.nro_documento and p.pai_cod_pais = i.pai_cod_pais and p.id_numero = i.id_numero
        left join DOM_MANAGER.VT_DOMICILIOS_COND dom
          on dom.id_vin = d.id_vin_denuncia
        left join DOM_MANAGER.VT_DOMICILIOS_COND d
          on d.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA (i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3)
      where
        d.Id_Tramite_Ipj = p_id_tramite_ipj and
        d.id_legajo = p_id_legajo;

  END SP_Traer_Denunciados;

  PROCEDURE SP_Traer_Denuncia_Medios(
    o_Cursor OUT types.cursorType,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number)
  IS
  /**********************************************************
    Lista los Medios declarados en la denuncia OnLine
  **********************************************************/
  BEGIN
    OPEN o_Cursor FOR
      select dm.id_tramite_ipj, dm.id_legajo, dm.medio_prueba, dm.observacion
      from IPJ.T_DENUNCIAS_MEDIOS dm
      where
        dm.id_tramite_ipj = p_Id_Tramite_Ipj and
        dm.id_legajo = p_id_legajo;

  END SP_Traer_Denuncia_Medios;

  PROCEDURE SP_Traer_Denuncia_Motivo(
    o_Cursor OUT types.cursorType,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number)
  IS
  /**********************************************************
    Lista los ditintios motivos de denuncuas, indicando cuales hay sido seleccionados en el tr�mite
  **********************************************************/
  BEGIN
    OPEN o_Cursor FOR
      select p_id_tramite_ipj id_tramiteipj, p_id_legajo id_legajo, tmd.id_motivo_denuncia, tmd.n_motivo_denuncia,
        ( select count(1)
          from IPJ.T_DENUNCIAS_MOTIVO dm
          where dm.id_tramite_ipj = p_id_tramite_ipj and dm.id_legajo = p_id_legajo and dm.id_motivo_denuncia =tmd.id_motivo_denuncia
        ) Seleccionado
      from T_TIPOS_MOTIVOS_DENUNCIA tmd
      ;
  END SP_Traer_Denuncia_Motivo;

  PROCEDURE SP_GUARDAR_DENUNCIA_MEDIOS(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number,
    p_medio_prueba varchar2,
    p_observacion varchar2,
    p_eliminar in number -- 1 elimina
  )
  IS
  BEGIN
    /*************************************************************
      Guarda o elimina un medio de prueba de una denuncia
  *************************************************************/
    -- CUERPO PROCEDIMIENTO
    if p_eliminar = 1 then
      delete IPJ.T_DENUNCIAS_MEDIOS
      where
        Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        id_legajo = p_id_legajo and
        upper(medio_prueba) = upper(p_medio_prueba);

    else
      -- Si existe un registro igual no hago nada
      update ipj.T_DENUNCIAS_MEDIOS
      set
        observacion = p_observacion
      where
        id_tramite_ipj = p_id_tramite_ipj and
        id_legajo = p_id_legajo and
        upper(medio_prueba) = upper(p_medio_prueba);

      -- Si no actualiz� nada, lo agrego
      if sql%rowcount = 0 then
        insert into ipj.T_DENUNCIAS_MEDIOS
          (Id_Tramite_Ipj, id_legajo, medio_prueba, observacion)
        values
          (p_Id_Tramite_Ipj, p_id_legajo, p_medio_prueba, p_observacion);
      end if;
    end if;

    o_rdo := 'OK';
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    commit;
  EXCEPTION
     WHEN OTHERS THEN
       o_rdo := 'sp_guardar_denuncia_medios : ' || To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
       rollback;
  END SP_GUARDAR_DENUNCIA_MEDIOS;

  PROCEDURE SP_GUARDAR_DENUNCIA_MOTIVO(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number,
    p_id_motivo_denuncia in number,
    p_eliminar in number -- 1 elimina
  )
  IS
    v_existe number;
  BEGIN
    /*************************************************************
      Guarda o elimina un motivo de una denuncia
  *************************************************************/
    -- CUERPO PROCEDIMIENTO
    if p_eliminar = 1 then
      delete IPJ.T_DENUNCIAS_MOTIVO
      where
        Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        id_legajo = p_id_legajo and
        id_motivo_denuncia = p_id_motivo_denuncia;

    else
      -- Si existe un registro igual no hago nada
      select count(1) into v_existe
      from ipj.T_DENUNCIAS_MOTIVO
      where
        id_tramite_ipj = p_id_tramite_ipj and
        id_legajo = p_id_legajo and
        id_motivo_denuncia = p_id_motivo_denuncia;

      -- Si no actualiz� nada, lo agrego
      if v_existe = 0 then
        insert into ipj.T_DENUNCIAS_MOTIVO (Id_Tramite_Ipj, id_legajo, id_motivo_denuncia)
        values (p_Id_Tramite_Ipj, p_id_legajo, p_id_motivo_denuncia);
      end if;
    end if;

    o_rdo := 'OK';
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    commit;
  EXCEPTION
     WHEN OTHERS THEN
       o_rdo := 'sp_guardar_denuncia_motivo : ' || To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
       rollback;
  END SP_GUARDAR_DENUNCIA_MOTIVO;

  PROCEDURE SP_GUARDAR_DENUNCIA(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number,
    p_uso_via_interna in varchar2,
    p_hecho_denuncia in clob,
    p_desconoce_dom in char,
    p_fec_resol_cn in varchar2, -- Es Fecha
    p_fecha_cargos in varchar2 -- Es Fecha
  )
  IS
  BEGIN
    /*************************************************************
      Guarda o actualiza los datos de una denuncia
  *************************************************************/
    -- CUERPO PROCEDIMIENTO
    -- Si existe un registro igual no hago nada
    update ipj.T_DENUNCIAS
    set
      uso_via_interna = p_uso_via_interna,
      hecho_denuncia = p_hecho_denuncia,
      desconoce_dom = p_desconoce_dom,
      fec_resol_cn = to_date(p_fec_resol_cn, 'dd/mm/rrrr'),
      fecha_cargos = to_date( p_fecha_cargos, 'dd/mm/rrrr')
    where
      id_tramite_ipj = p_id_tramite_ipj and
      id_legajo = p_id_legajo;

    -- Si no actualiz� nada, lo agrego
    if sql%rowcount = 0 then
      insert into ipj.T_DENUNCIAS (Id_Tramite_Ipj, id_legajo, uso_via_interna, hecho_denuncia, desconoce_dom, fec_resol_cn, fecha_cargos)
      values (p_Id_Tramite_Ipj, p_id_legajo, p_uso_via_interna, p_hecho_denuncia, p_desconoce_dom, to_date(p_fec_resol_cn, 'dd/mm/rrrr'),
        to_date(p_fecha_cargos, 'dd/mm/rrrr'));
    end if;

    o_rdo := 'OK';
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    commit;
  EXCEPTION
     WHEN OTHERS THEN
       o_rdo := 'sp_guardar_denuncia : ' || To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
       rollback;
  END SP_GUARDAR_DENUNCIA;

  PROCEDURE SP_GUARDAR_DENUNCIA_DOMICILIO(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    o_id_vin out number,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number,
    p_id_vin_den in number,
    P_ID_PROVINCIA in varchar2,
    P_ID_DEPARTAMENTO in number,
    P_ID_LOCALIDAD in number,
    P_BARRIO in varchar2,
    P_CALLE in varchar2,
    P_ALTURA in number,
    P_DEPTO in varchar2,
    P_PISO in varchar2,
    p_torre in varchar2,
    p_manzana in varchar2,
    p_lote in varchar2,
    p_id_tipocalle in number,
    p_km in varchar,
    p_id_calle in number,
    p_id_barrio in number
    )
  IS
   /*************************************************************
    Inserta o Actualiza un domicilio para una denuncia en DOM_MANAGER
    No maneja transaccion, ya lo hace DOM_MANAGER
  *************************************************************/
    v_TIPO_DOM  NUMBER;
    v_N_TIPO_DOM  VARCHAR2(150);
    v_usuario varchar2(20);
  BEGIN
    if IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('HAB_LOG_JURIDICO') = '1' then
      IPJ.VARIOS.SP_GUARDAR_LOG(
        p_METODO => 'SP_GUARDAR_DENUNCIA_DOMICILIO',
        p_NIVEL => 'Parametros',
        p_ORIGEN => 'DB',
        p_USR_LOG  => 'DB',
        p_USR_WINDOWS => 'Sistema',
        p_IP_PC  => null,
        p_EXCEPCION =>
          'Id Tramite IPJ = ' || to_char(p_Id_Tramite_Ipj)
          || ' / Id Legajo = ' || to_char(p_id_legajo)
          || ' / Id Vin Real = ' || to_char(p_id_vin_den)
          || ' / Id Prov = ' || P_ID_PROVINCIA
          || ' / Id Departamento = ' || to_char(P_ID_DEPARTAMENTO)
          || ' / Id Localidad = ' || to_char(P_ID_LOCALIDAD)
          || ' / Barrio = ' || P_BARRIO
          || ' / Calle = ' || P_CALLE
          || ' / Altura = ' || to_char(P_ALTURA)
          || ' / Depto = ' || P_DEPTO
          || ' / Piso = ' || P_PISO
          || ' / Torre = ' || p_torre
          || ' / Manzana = ' || p_manzana
          || ' / Lote = ' || p_lote
          || ' / Tipo Calle = ' || to_char(p_id_tipocalle)
          || ' / Km = ' || p_km
          || ' / Id Calle = ' || to_char(p_id_calle)
          || ' / Id Barrio = ' || to_char(p_id_barrio)
      );
    end if;

    -- Busco el responsable de la primer accion
    select cuil_usuario into v_usuario
    from ipj.t_tramitesipj_acciones
    where
      id_tramite_ipj = p_id_tramite_ipj and
      id_estado < 100 and
      rownum = 1;

    if nvl(p_id_departamento, 0) > 0 then
      IPJ.VARIOS.SP_GUARDAR_DOMICILIO(
        o_id_vin => o_id_vin,
        o_rdo => o_rdo,
        o_tipo_mensaje => o_tipo_mensaje,
        p_id_vin => p_id_vin_den,
        p_usuario => v_usuario,
        p_tipo_domicilio => 0, -- 3 Real //  0 Sin Asignar
        p_id_integrante => 0,
        p_id_legajo => 0,
        p_cuit_empresa => null,
        p_id_fondo_comercio => 0,
        p_id_entidad_acta => 0,
        p_es_comerciante => 'N',
        p_es_admin_entidad => 'N',
        p_id_provincia => p_id_provincia,
        p_id_departamento => p_id_departamento,
        p_id_localidad => p_id_localidad,
        p_barrio =>  p_barrio,
        p_calle => p_calle,
        p_altura => p_altura,
        p_depto => p_depto,
        p_piso => p_piso,
        p_torre => p_torre,
        p_manzana => p_manzana,
        p_lote => p_lote,
        p_id_barrio => p_id_barrio,
        p_id_calle => p_id_calle,
        p_id_tipocalle => p_id_tipocalle,
        p_km => p_km
      );
    else
      o_id_vin := null;
      o_rdo := IPJ.TYPES.C_RESP_OK;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    end if;

    -- Si se guard� bien el domicilio, lo actualizo en la denuncia
    if o_rdo = IPJ.TYPES.C_RESP_OK then
      update ipj.t_denuncias
      set id_vin_denuncia = o_id_vin
      where
        id_tramite_ipj = p_id_tramite_ipj and
        id_legajo = p_id_legajo;

      commit;
    end if;

  EXCEPTION
     WHEN OTHERS THEN
        o_rdo := 'Error SP_GUARDAR_DENUNCIA_DOMICILIO: ' || To_Char(SQLCODE) || '-' || SQLERRM;
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;

  END SP_GUARDAR_DENUNCIA_DOMICILIO;

  PROCEDURE SP_GUARDAR_DENUNCIADOS(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number,
    -- Datos Persona
    p_id_integrante in number,
    p_id_sexo in varchar2,
    p_nro_documento in varchar2,
    p_pai_cod_pais in varchar2,
    p_id_numero in number,
    p_detalle in varchar2,
    p_nombre in varchar2,
    p_apellido in varchar2,
    p_n_tipo_documento in varchar2,
    -- Domicilio Denunciado
    p_id_vin_den in number,
    p_id_provincia in varchar2,
    p_id_departamento in number,
    p_id_localidad in number,
    p_barrio in varchar2,
    p_calle in varchar2,
    p_altura in number,
    p_depto in varchar2,
    p_piso in varchar2,
    p_torre in varchar2,
    p_manzana in varchar2,
    p_lote in varchar2,
    p_id_tipocalle in number,
    p_km in varchar,
    p_id_calle in number,
    p_id_barrio in number,
    -- Otros
    p_eliminar in number -- 1 elimina
  )
  IS
    v_id_vin number;
    v_usuario varchar2(20);
    v_rdo_integ varchar2(1000);
    v_tipo_mensaje_integ number;
    v_id_integrante number;
    v_existe_rcivil number;
    v_nombre varchar2(250);
    v_apellido varchar2(250);
  BEGIN
    /*************************************************************
      Guarda o elimina un denunciado
  *************************************************************/
    if IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('HAB_LOG_JURIDICO') = '1' then
      IPJ.VARIOS.SP_GUARDAR_LOG(
        p_METODO => 'SP_GUARDAR_DENUNCIADOS',
        p_NIVEL => 'DB - Gesti�n',
        p_ORIGEN => 'DB',
        p_USR_LOG  => 'DB',
        p_USR_WINDOWS => 'Sistema',
        p_IP_PC  => null,
        p_EXCEPCION =>
          'Id Tramite IPJ = ' || to_char(p_Id_Tramite_Ipj)
          || ' / Id Legajo = ' || to_char(p_id_legajo)
          || ' / Id Integrante = ' || to_char( p_id_integrante)
          || ' / Id Sexo = ' || p_id_sexo
          || ' / Nro Documento = ' || p_nro_documento
          || ' / Pai Cod Pais = ' || p_pai_cod_pais
          || ' / ID Numero = ' || to_char(p_id_numero)
          || ' / Detalle = ' || p_detalle
          || ' / Nombre = ' || p_nombre
          || ' / Apellido = ' || p_apellido
          || ' / Tipo Documento = ' || p_n_tipo_documento
          || ' / Id Vin Denun = ' || to_char(p_id_vin_den)
          || ' / Id Provicia = ' || p_id_provincia
          || ' / Id Departamento = ' || to_char(p_id_departamento)
          || ' / Id_Localidad = ' || to_char(p_id_localidad)
          || ' / Barrio = ' || p_barrio
          || ' / Calle = ' || p_calle
          || ' / Altura = ' || to_char(p_altura)
          || ' / Depto = ' || p_depto
          || ' / Piso = ' || p_piso
          || ' / Torre = ' || p_torre
          || ' / Manzana = ' || p_manzana
          || ' / Lote = ' || p_lote
          || ' / Id Tipo Calle = ' || to_char(p_id_tipocalle)
          || ' / Km = ' || p_km
          || ' / id Calle = ' || to_char(p_id_calle)
          || ' / Id Barrio = ' || to_char(p_id_barrio)
          || ' / Eliminar = ' || to_char(p_eliminar)
      );
    end if;

    -- Controlo que venga una persona
    if nvl(p_id_integrante, 0) = 0 then
      -- Verifico si esta o no en RCIVIL para ajustar los par�metros en el guardar
      select count(1) into v_existe_rcivil
      from rcivil.vt_pk_persona
      where
        id_sexo = p_id_sexo and
        nro_documento = p_nro_documento and
        pai_cod_pais = p_pai_cod_pais and
        id_numero = p_id_numero;

      if v_existe_rcivil > 0 then
        v_nombre := null;
        v_apellido := null;
      else
        v_nombre := p_nombre;
        v_apellido := p_apellido;
      end if;

      IPJ.ENTIDAD_PERSJUR.SP_GUARDAR_INTEGRANTES(
        o_rdo => v_rdo_integ,
        o_tipo_mensaje => v_tipo_mensaje_integ,
        o_id_integrante => v_id_integrante,
        p_id_sexo => p_id_sexo,
        p_nro_documento => p_nro_documento,
        p_pai_cod_pais => p_pai_cod_pais,
        p_id_numero => p_id_numero,
        p_cuil => null,
        p_detalle => p_detalle,
        p_nombre => v_nombre,
        p_apellido => v_apellido,
        p_error_dato => 'N',
        p_n_tipo_documento => p_n_tipo_documento
      );

      if v_rdo_integ <> TYPES.c_Resp_OK then
        o_rdo := 'ERROR: No se pudo registrar la persona';
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
        return;
      end if;
    else
      v_id_integrante := p_id_integrante;
    end if;

    -- CUERPO PROCEDIMIENTO
    if p_eliminar = 1 then
      delete IPJ.T_DENUNCIADOS
      where
        Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        id_legajo = p_id_legajo and
        id_integrante = v_id_integrante;

    else
      -- Si viene Departamento, intento guardar el domicilio
      if nvl(p_id_departamento, 0) > 0 then
        -- Busco el responsable de la primer accion
        select cuil_usuario into v_usuario
        from ipj.t_tramitesipj_acciones
        where
          id_tramite_ipj = p_id_tramite_ipj and
          id_estado < 100 and
          rownum = 1;

        IPJ.VARIOS.SP_GUARDAR_DOMICILIO(
          o_id_vin => v_id_vin,
          o_rdo => o_rdo,
          o_tipo_mensaje => o_tipo_mensaje,
          p_id_vin => p_id_vin_den,
          p_usuario => v_usuario,
          p_Tipo_Domicilio => 0, -- 3 Real //  0 Sin Asignar
          p_id_integrante => 0,
          p_id_legajo => 0,
          p_cuit_empresa => null,
          p_id_fondo_comercio => 0,
          p_id_entidad_acta => 0,
          p_es_comerciante => 'N',
          p_es_admin_entidad => 'N',
          P_ID_PROVINCIA => p_id_provincia,
          P_ID_DEPARTAMENTO => p_id_departamento,
          P_ID_LOCALIDAD => p_id_localidad,
          P_BARRIO =>  p_barrio,
          P_CALLE => p_calle,
          P_ALTURA => p_altura,
          P_DEPTO => p_depto,
          P_PISO => p_piso,
          p_torre => p_torre,
          p_manzana => p_manzana,
          p_lote => p_lote,
          p_id_barrio => p_id_barrio,
          p_id_calle => p_id_calle,
          p_id_tipocalle => p_id_tipocalle,
          p_km => p_km
        );
      else
        v_id_vin := p_id_vin_den;
      end if;

      -- Si existe un registro igual no hago nada
      update ipj.T_DENUNCIADOS
      set
        id_vin_denuncia = v_id_vin
      where
        id_tramite_ipj = p_id_tramite_ipj and
        id_legajo = p_id_legajo and
        id_integrante = v_id_integrante;

      -- Si no actualiz� nada, lo agrego
      if sql%rowcount = 0 then
        insert into ipj.T_DENUNCIADOS (Id_Tramite_Ipj, id_legajo, id_integrante, id_vin_denuncia)
        values
          (p_Id_Tramite_Ipj, p_id_legajo, v_id_integrante, v_id_vin);
      end if;
    end if;

    o_rdo := 'OK';
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    commit;
  EXCEPTION
     WHEN OTHERS THEN
       o_rdo := 'SP_GUARDAR_DENUNCIADOS : ' || To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
       rollback;
  END SP_GUARDAR_DENUNCIADOS;

  PROCEDURE SP_GUARDAR_DENUNCIANTES(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_Id_Tramite_Ipj in number,
    p_id_legajo in number,
    -- Datos Persona
    p_id_integrante in number,
    p_id_sexo in varchar2,
    p_nro_documento in varchar2,
    p_pai_cod_pais in varchar2,
    p_id_numero in number,
    p_detalle in varchar2,
    p_nombre in varchar2,
    p_apellido in varchar2,
    p_n_tipo_documento in varchar2,
    -- Domicilio Real
    p_id_vin in number,
    p_id_provincia in varchar2,
    p_id_departamento in number,
    p_id_localidad in number,
    p_barrio in varchar2,
    p_calle in varchar2,
    p_altura in number,
    p_depto in varchar2,
    p_piso in varchar2,
    p_torre in varchar2,
    p_manzana in varchar2,
    p_lote in varchar2,
    p_id_tipocalle in number,
    p_km in varchar,
    p_id_calle in number,
    p_id_barrio in number,
    -- Otros
    p_eliminar in number -- 1 elimina
  )
  IS
    v_existe number;
    v_id_vin number;
    v_usuario varchar2(20);v_rdo_integ varchar2(1000);
    v_tipo_mensaje_integ number;
    v_id_integrante number;
    v_existe_rcivil number;
    v_nombre varchar2(250);
    v_apellido varchar2(250);
  BEGIN
    /*************************************************************
      Guarda o elimina un denunciante
  *************************************************************/
    if IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('HAB_LOG_JURIDICO') = '1' then
      IPJ.VARIOS.SP_GUARDAR_LOG(
        p_METODO => 'SP_GUARDAR_DENUNCIANTES',
        p_NIVEL => 'DB - Gesti�n',
        p_ORIGEN => 'DB',
        p_USR_LOG  => 'DB',
        p_USR_WINDOWS => 'Sistema',
        p_IP_PC  => null,
        p_EXCEPCION =>
          'Id Tramite IPJ = ' || to_char(p_Id_Tramite_Ipj)
          || ' / Id Legajo = ' || to_char(p_id_legajo)
          || ' / Id Integrante = ' || to_char(p_id_integrante)
          || ' / Id Sexo = ' || p_id_sexo
          || ' / Nro Documento = ' || p_nro_documento
          || ' / Pai Cod Pais = ' || p_pai_cod_pais
          || ' / Id numero = ' || to_char(p_id_numero)
          || ' / Detalle = ' || p_detalle
          || ' / Nombre = ' || p_nombre
          || ' / Apellido = ' || p_apellido
          || ' / Tipo Documento = ' || p_n_tipo_documento
          || ' / Id Vin = ' || to_char(p_id_vin)
          || ' / Id Provicia = ' || p_id_provincia
          || ' / Id Departamento = ' || to_char(p_id_departamento)
          || ' / Id_Localidad = ' || to_char(p_id_localidad)
          || ' / Barrio = ' || p_barrio
          || ' / Calle = ' || p_calle
          || ' / Altura = ' || to_char(p_altura)
          || ' / Depto = ' || p_depto
          || ' / Piso = ' || p_piso
          || ' / Torre = ' || p_torre
          || ' / Manzana = ' || p_manzana
          || ' / Lote = ' || p_lote
          || ' / Id Tipo Calle = ' || to_char(p_id_tipocalle)
          || ' / Km = ' || p_km
          || ' / Id Calle = ' || to_char(p_id_calle)
          || ' / Id Barrio = ' || to_char(p_id_barrio)
          || ' / Eliminar = ' || to_char(p_eliminar)
      );
    end if;

    -- Controlo que venga una persona
    if nvl(p_id_integrante, 0) = 0 then
      -- Verifico si esta o no en RCIVIL para ajustar los par�metros en el guardar
      select count(1) into v_existe_rcivil
      from rcivil.vt_pk_persona
      where
        id_sexo = p_id_sexo and
        nro_documento = p_nro_documento and
        pai_cod_pais = p_pai_cod_pais and
        id_numero = p_id_numero;

      if v_existe_rcivil > 0 then
        v_nombre := null;
        v_apellido := null;
      else
        v_nombre := p_nombre;
        v_apellido := p_apellido;
      end if;

      IPJ.ENTIDAD_PERSJUR.SP_GUARDAR_INTEGRANTES(
        o_rdo => v_rdo_integ,
        o_tipo_mensaje => v_tipo_mensaje_integ,
        o_id_integrante => v_id_integrante,
        p_id_sexo => p_id_sexo,
        p_nro_documento => p_nro_documento,
        p_pai_cod_pais => p_pai_cod_pais,
        p_id_numero => p_id_numero,
        p_cuil => null,
        p_detalle => p_detalle,
        p_nombre => v_nombre,
        p_apellido => v_apellido,
        p_error_dato => 'N',
        p_n_tipo_documento => p_n_tipo_documento
      );

      if v_rdo_integ <> TYPES.c_Resp_OK then
        o_rdo := 'ERROR: No se pudo registrar la persona';
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
        return;
      end if;
    else
      v_id_integrante := p_id_integrante;
    end if;

    -- CUERPO PROCEDIMIENTO
    if p_eliminar = 1 then
      delete IPJ.T_DENUNCIANTES
      where
        Id_Tramite_Ipj = p_Id_Tramite_Ipj and
        id_legajo = p_id_legajo and
        id_integrante = v_id_integrante;

    else
      -- Si viene Departamento, intento guardar el domicilio
      if nvl(p_id_departamento, 0) > 0 then
        -- Busco el responsable de la primer accion
        select cuil_usuario into v_usuario
        from ipj.t_tramitesipj_acciones
        where
          id_tramite_ipj = p_id_tramite_ipj and
          id_estado < 100 and
          rownum = 1;

        IPJ.VARIOS.SP_GUARDAR_DOMICILIO(
          o_id_vin => v_id_vin,
          o_rdo => o_rdo,
          o_tipo_mensaje => o_tipo_mensaje,
          p_id_vin => p_id_vin,
          p_usuario => v_usuario,
          p_Tipo_Domicilio => 3, -- 3 Real //  0 Sin Asignar
          p_id_integrante => v_id_integrante,
          p_id_legajo => 0,
          p_cuit_empresa => null,
          p_id_fondo_comercio => 0,
          p_id_entidad_acta => 0,
          p_es_comerciante => 'N',
          p_es_admin_entidad => 'N',
          P_ID_PROVINCIA => p_id_provincia,
          P_ID_DEPARTAMENTO => p_id_departamento,
          P_ID_LOCALIDAD => p_id_localidad,
          P_BARRIO =>  p_barrio,
          P_CALLE => p_calle,
          P_ALTURA => p_altura,
          P_DEPTO => p_depto,
          P_PISO => p_piso,
          p_torre => p_torre,
          p_manzana => p_manzana,
          p_lote => p_lote,
          p_id_barrio => p_id_barrio,
          p_id_calle => p_id_calle,
          p_id_tipocalle => p_id_tipocalle,
          p_km => p_km
        );
      else
        v_id_vin := p_id_vin;
      end if;

      -- Si existe un registro igual no hago nada
      select count(1) into v_existe
      from ipj.T_DENUNCIANTES
      where
        id_tramite_ipj = p_id_tramite_ipj and
        id_legajo = p_id_legajo and
        id_integrante = v_id_integrante;

      -- Si no actualiz� nada, lo agrego
      if v_existe = 0 then
        insert into ipj.T_DENUNCIANTES (Id_Tramite_Ipj, id_legajo, id_integrante)
        values (p_Id_Tramite_Ipj, p_id_legajo, v_id_integrante);
      end if;
    end if;

    o_rdo := 'OK';
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    commit;
  EXCEPTION
     WHEN OTHERS THEN
       o_rdo := 'SP_GUARDAR_DENUNCIADOS : ' || To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
       rollback;
  END SP_GUARDAR_DENUNCIANTES;

  PROCEDURE SP_Traer_CN_Normalizadores(
    o_Cursor OUT types.cursorType,
    p_id_tramite_ipj in number,
    p_id_legajo in number)
  IS
   /**********************************************************
    Lista las personas solicitantes de una Comisi�n Normalizadora
  **********************************************************/
  BEGIN
    OPEN o_Cursor FOR
      select n.id_tramite_ipj, n.id_legajo, n.Id_Integrante, n.id_estado_normalizador, n.id_motivo_baja, n.id_normalizador,
        (select tn.n_normalizador from ipj.t_tipos_normalizador tn where tn.id_normalizador = n.id_normalizador) n_normalizador,
        (select tb.n_motivo_baja from ipj.t_tipos_motivos_baja tb where tb.id_motivo_baja = n.id_motivo_baja) n_motivo_baja,
        (select en.n_estado_normalizador from ipj.t_tipos_estado_norm en where en.id_estado_normalizador = n.id_estado_normalizador) n_estado_normalizador,
        p.NOV_nombre||' '||p.NOV_APELLIDO detalle,
        I.Id_Sexo, I.Id_Numero, I.Nro_Documento, I.Pai_Cod_Pais, nvl(p.cuil, I.CUIL) Cuil,
        IPJ.VARIOS.FC_Buscar_Mail(I.Id_Sexo || I.Nro_Documento || I.Pai_Cod_Pais || to_char(I.Id_Numero), IPJ.Types.c_id_Aplicacion) EMail,
        IPJ.VARIOS.FC_Buscar_Telefono(I.Id_Sexo || I.Nro_Documento || I.Pai_Cod_Pais || to_char(I.Id_Numero), IPJ.Types.c_id_Aplicacion) Telefono,
        IPJ.VARIOS.FC_Buscar_id_Profesion(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero) id_profesion,
        IPJ.VARIOS.FC_Buscar_Profesion(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero) n_profesion,
        IPJ.VARIOS.FC_Buscar_Tipo_Profesion(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero) id_tipo_caracterisica,
        i.id_sexo || i.nro_documento || i.pai_cod_pais || to_char(i.id_numero) Clave,
        replace(to_char(I.Nro_Documento, '99,999,999'), ',', '.') Nro_Documento_Format,
        replace(to_char(nvl(p.cuil, I.CUIL), '99,99999999,9'), ',', '-') Cuil_Format,
        IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3) id_vin,
        IPJ.VARIOS.FC_ARMAR_CALLE_DOM(
            (select dom.n_calle from DOM_MANAGER.VT_DOMICILIOS_COND dom where dom.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3)),
            (select dom.altura from DOM_MANAGER.VT_DOMICILIOS_COND dom where dom.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3)),
            (select dom.piso from DOM_MANAGER.VT_DOMICILIOS_COND dom where dom.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3)),
            (select dom.depto from DOM_MANAGER.VT_DOMICILIOS_COND dom where dom.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3)),
            (select dom.torre from DOM_MANAGER.VT_DOMICILIOS_COND dom where dom.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3)),
            (select dom.mzna from DOM_MANAGER.VT_DOMICILIOS_COND dom where dom.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3)),
            (select dom.lote from DOM_MANAGER.VT_DOMICILIOS_COND dom where dom.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3)),
            (select dom.n_barrio from DOM_MANAGER.VT_DOMICILIOS_COND dom where dom.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3)),
            (select dom.km from DOM_MANAGER.VT_DOMICILIOS_COND dom where dom.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3)),
            (select dom.n_tipocalle from DOM_MANAGER.VT_DOMICILIOS_COND dom where dom.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3))
          ) calle_inf,
        (select dom.id_tipodom from DOM_MANAGER.VT_DOMICILIOS_COND dom where dom.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3)) id_tipodom,
        (select dom.n_tipodom from DOM_MANAGER.VT_DOMICILIOS_COND dom where dom.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3)) n_tipodom,
        (select dom.id_tipocalle from DOM_MANAGER.VT_DOMICILIOS_COND dom where dom.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3)) id_tipocalle,
        (select dom.n_tipocalle from DOM_MANAGER.VT_DOMICILIOS_COND dom where dom.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3)) n_tipocalle,
        (select dom.id_calle from DOM_MANAGER.VT_DOMICILIOS_COND dom where dom.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3)) id_calle,
        (select initcap(dom.n_calle) from DOM_MANAGER.VT_DOMICILIOS_COND dom where dom.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3)) n_calle,
        (select dom.altura from DOM_MANAGER.VT_DOMICILIOS_COND dom where dom.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3)) altura,
        (select dom.depto from DOM_MANAGER.VT_DOMICILIOS_COND dom where dom.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3)) depto,
        (select dom.piso from DOM_MANAGER.VT_DOMICILIOS_COND dom where dom.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3)) piso,
        (select dom.torre from DOM_MANAGER.VT_DOMICILIOS_COND dom where dom.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3)) torre,
        (select dom.id_barrio from DOM_MANAGER.VT_DOMICILIOS_COND dom where dom.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3)) id_barrio,
        (select initcap(dom.n_barrio) n_barrio from DOM_MANAGER.VT_DOMICILIOS_COND dom where dom.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3)) n_barrio,
        (select dom.id_localidad from DOM_MANAGER.VT_DOMICILIOS_COND dom where dom.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3)) id_localidad,
        (select initcap(dom.n_localidad) from DOM_MANAGER.VT_DOMICILIOS_COND dom where dom.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3)) n_localidad,
        (select dom.id_departamento from DOM_MANAGER.VT_DOMICILIOS_COND dom where dom.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3)) id_departamento,
        (select initcap(dom.n_departamento) from DOM_MANAGER.VT_DOMICILIOS_COND dom where dom.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3)) n_departamento,
        (select dom.id_provincia from DOM_MANAGER.VT_DOMICILIOS_COND dom where dom.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3)) id_provincia,
        (select initcap(dom.n_provincia) from DOM_MANAGER.VT_DOMICILIOS_COND dom where dom.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3)) n_provincia,
        (select dom.cpa from DOM_MANAGER.VT_DOMICILIOS_COND dom where dom.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3)) cpa,
        (select dom.mzna from DOM_MANAGER.VT_DOMICILIOS_COND dom where dom.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3)) Manzana,
        (select dom.Lote from DOM_MANAGER.VT_DOMICILIOS_COND dom where dom.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3)) lote,
        (select dom.km from DOM_MANAGER.VT_DOMICILIOS_COND dom where dom.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3)) km,
        to_char(p.fec_nacimiento, 'dd/mm/rrrr') fecha_nacimiento,
        (select se.tipo_sexo from t_comunes.vt_sexos se where se.id_sexo = p.id_sexo) Sexo,
        (select lower(n_estado_civil) from rcivil.vt_estados_civil where id_estado_civil = (case when p.id_estado_civil is not null then p.id_estado_civil else i.Id_Estado_Civil end)) n_estado_civil,
        decode(p.pai_cod_pais_nacionalidad, 'DES', 'Argentina', (select pa.pai_nacionalidad from rcivil.vt_paises pa where pa.id_pais = p.pai_cod_pais_nacionalidad)) nacionalidad,
        (select r.nivel from gestion_ciudadanos.vt_usuarios_cidi r where cuil = p.cuil) Nivel_CiDi
      from IPJ.T_CN_NORMALIZADOR n join ipj.t_integrantes i
          on n.id_integrante = i.id_integrante
        left join RCIVIL.VT_PK_PERSONA p
          on p.id_sexo = i.id_sexo and p.nro_documento = i.nro_documento and p.pai_cod_pais = i.pai_cod_pais and p.id_numero = i.id_numero
      where
        n.id_tramite_ipj = p_id_tramite_ipj and
        n.id_legajo = p_id_legajo
      order by id_normalizador asc, id_integrante asc;

  END SP_Traer_CN_Normalizadores;

  PROCEDURE SP_Traer_CN_Solicitante(
    o_Cursor OUT types.cursorType,
    p_id_tramite_ipj in number,
    p_id_legajo in number)
  IS
   /**********************************************************
    Lista las personas solicitantes de una Comisi�n Normalizadora
  **********************************************************/
  BEGIN
    OPEN o_Cursor FOR
      select s.id_tramite_ipj, s.id_legajo, s.Id_Integrante, p.NOV_nombre||' '||p.NOV_APELLIDO detalle,
        I.Id_Sexo, I.Id_Numero, I.Nro_Documento, I.Pai_Cod_Pais, nvl(p.cuil, I.CUIL) Cuil,
        IPJ.VARIOS.FC_Buscar_Mail(I.Id_Sexo || I.Nro_Documento || I.Pai_Cod_Pais || to_char(I.Id_Numero), IPJ.Types.c_id_Aplicacion) EMail,
        IPJ.VARIOS.FC_Buscar_Telefono(I.Id_Sexo || I.Nro_Documento || I.Pai_Cod_Pais || to_char(I.Id_Numero), IPJ.Types.c_id_Aplicacion) Telefono,
        IPJ.VARIOS.FC_Buscar_id_Profesion(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero) id_profesion,
        IPJ.VARIOS.FC_Buscar_Profesion(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero) n_profesion,
        IPJ.VARIOS.FC_Buscar_Tipo_Profesion(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero) id_tipo_caracterisica,
        i.id_sexo || i.nro_documento || i.pai_cod_pais || to_char(i.id_numero) Clave,
        replace(to_char(I.Nro_Documento, '99,999,999'), ',', '.') Nro_Documento_Format,
        replace(to_char(nvl(p.cuil, I.CUIL), '99,99999999,9'), ',', '-') Cuil_Format,
        IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3) id_vin,
        IPJ.VARIOS.FC_ARMAR_CALLE_DOM(
            (select dom.n_calle from DOM_MANAGER.VT_DOMICILIOS_COND dom where dom.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3)),
            (select dom.altura from DOM_MANAGER.VT_DOMICILIOS_COND dom where dom.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3)),
            (select dom.piso from DOM_MANAGER.VT_DOMICILIOS_COND dom where dom.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3)),
            (select dom.depto from DOM_MANAGER.VT_DOMICILIOS_COND dom where dom.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3)),
            (select dom.torre from DOM_MANAGER.VT_DOMICILIOS_COND dom where dom.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3)),
            (select dom.mzna from DOM_MANAGER.VT_DOMICILIOS_COND dom where dom.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3)),
            (select dom.lote from DOM_MANAGER.VT_DOMICILIOS_COND dom where dom.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3)),
            (select dom.n_barrio from DOM_MANAGER.VT_DOMICILIOS_COND dom where dom.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3)),
            (select dom.km from DOM_MANAGER.VT_DOMICILIOS_COND dom where dom.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3)),
            (select dom.n_tipocalle from DOM_MANAGER.VT_DOMICILIOS_COND dom where dom.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3))
          ) calle_inf,
        (select dom.id_tipodom from DOM_MANAGER.VT_DOMICILIOS_COND dom where dom.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3)) id_tipodom,
        (select dom.n_tipodom from DOM_MANAGER.VT_DOMICILIOS_COND dom where dom.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3)) n_tipodom,
        (select dom.id_tipocalle from DOM_MANAGER.VT_DOMICILIOS_COND dom where dom.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3)) id_tipocalle,
        (select dom.n_tipocalle from DOM_MANAGER.VT_DOMICILIOS_COND dom where dom.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3)) n_tipocalle,
        (select dom.id_calle from DOM_MANAGER.VT_DOMICILIOS_COND dom where dom.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3)) id_calle,
        (select initcap(dom.n_calle) from DOM_MANAGER.VT_DOMICILIOS_COND dom where dom.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3)) n_calle,
        (select dom.altura from DOM_MANAGER.VT_DOMICILIOS_COND dom where dom.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3)) altura,
        (select dom.depto from DOM_MANAGER.VT_DOMICILIOS_COND dom where dom.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3)) depto,
        (select dom.piso from DOM_MANAGER.VT_DOMICILIOS_COND dom where dom.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3)) piso,
        (select dom.torre from DOM_MANAGER.VT_DOMICILIOS_COND dom where dom.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3)) torre,
        (select dom.id_barrio from DOM_MANAGER.VT_DOMICILIOS_COND dom where dom.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3)) id_barrio,
        (select initcap(dom.n_barrio) n_barrio from DOM_MANAGER.VT_DOMICILIOS_COND dom where dom.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3)) n_barrio,
        (select dom.id_localidad from DOM_MANAGER.VT_DOMICILIOS_COND dom where dom.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3)) id_localidad,
        (select initcap(dom.n_localidad) from DOM_MANAGER.VT_DOMICILIOS_COND dom where dom.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3)) n_localidad,
        (select dom.id_departamento from DOM_MANAGER.VT_DOMICILIOS_COND dom where dom.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3)) id_departamento,
        (select initcap(dom.n_departamento) from DOM_MANAGER.VT_DOMICILIOS_COND dom where dom.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3)) n_departamento,
        (select dom.id_provincia from DOM_MANAGER.VT_DOMICILIOS_COND dom where dom.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3)) id_provincia,
        (select initcap(dom.n_provincia) from DOM_MANAGER.VT_DOMICILIOS_COND dom where dom.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3)) n_provincia,
        (select dom.cpa from DOM_MANAGER.VT_DOMICILIOS_COND dom where dom.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3)) cpa,
        (select dom.mzna from DOM_MANAGER.VT_DOMICILIOS_COND dom where dom.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3)) Manzana,
        (select dom.Lote from DOM_MANAGER.VT_DOMICILIOS_COND dom where dom.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3)) lote,
        (select dom.km from DOM_MANAGER.VT_DOMICILIOS_COND dom where dom.id_vin = IPJ.VARIOS.FC_BUSCAR_DOM_PERSONA(i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, 3)) km,
        s.id_solicitante_cn, (select n_solicitante_cn from IPJ.T_TIPOS_SOLIC_CN sc where sc.id_solicitante_cn = s.id_solicitante_cn) n_solicitante_cn,
        to_char(p.fec_nacimiento, 'dd/mm/rrrr') fecha_nacimiento,
        (select se.tipo_sexo from t_comunes.vt_sexos se where se.id_sexo = p.id_sexo) Sexo,
        (select lower(n_estado_civil) from rcivil.vt_estados_civil where id_estado_civil = (case when p.id_estado_civil is not null then p.id_estado_civil else i.Id_Estado_Civil end)) n_estado_civil,
        decode(p.pai_cod_pais_nacionalidad, 'DES', 'Argentina', (select pa.pai_nacionalidad from rcivil.vt_paises pa where pa.id_pais = p.pai_cod_pais_nacionalidad)) nacionalidad
      from IPJ.T_CN_SOLICITANTE s join ipj.t_integrantes i
          on s.id_integrante = i.id_integrante
        left join RCIVIL.VT_PK_PERSONA p
          on p.id_sexo = i.id_sexo and p.nro_documento = i.nro_documento and p.pai_cod_pais = i.pai_cod_pais and p.id_numero = i.id_numero
      where
        s.id_tramite_ipj = p_id_tramite_ipj and
        s.id_legajo = p_id_legajo;

  END SP_Traer_CN_Solicitante;

  PROCEDURE SP_Traer_CN_Motivo(
    o_Cursor OUT types.cursorType,
    p_id_tramite_ipj in number,
    p_id_legajo in number)
  IS
  /**********************************************************
    Lista los ditintios motivos de una Comision, indicando cuales hay sido seleccionados en el tr�mite
  **********************************************************/
  BEGIN
    OPEN o_Cursor FOR
      select p_id_tramite_ipj id_tramite_ipj, p_id_legajo id_legajo, tmc.id_motivo_comision, tmc.n_motivo_comision,
        ( select count(1)
          from IPJ.T_CM_MOTIVO cm
          where cm.id_tramite_ipj = p_id_tramite_ipj and cm.id_legajo = p_id_legajo and cm.id_motivo_comision = tmc.id_motivo_comision
        ) Seleccionado
      from T_TIPOS_MOTIVOS_COMISION tmc
      order by tmc.n_motivo_comision;

  END SP_Traer_CN_Motivo;

  PROCEDURE Sp_Guardar_CN_Motivo(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in Number,
    p_id_legajo in number,
    p_id_motivo_comision in number,
    p_eliminar in number) -- 1 Elimina
  IS
  /***********************************************************
    Inserta un nuevo Motivo o lo Elimina, no se permite modificar nada
  ************************************************************/
    v_existe number;
  BEGIN
    if IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('HAB_LOG_JURIDICO') = '1' then
      IPJ.VARIOS.SP_GUARDAR_LOG(
        p_METODO => 'Sp_Guardar_CN_Motivo',
        p_NIVEL => 'DB - Gesti�n',
        p_ORIGEN => 'DB',
        p_USR_LOG  => 'DB',
        p_USR_WINDOWS => 'Sistema',
        p_IP_PC  => null,
        p_EXCEPCION =>
          'Tr�mite IPJ = ' || to_char(p_id_tramite_ipj)
          || ' /  Legajo = ' || to_char(p_id_legajo)
          || ' /  Motivo CM = ' || to_char(p_id_motivo_comision)
          || ' /  Elimina = ' || to_char(p_eliminar)
      );
    end if;

    if p_eliminar = 1 then
      delete ipj.T_CM_MOTIVO
      where
        id_tramite_ipj = p_id_tramite_ipj and
        id_legajo = p_id_legajo and
        id_motivo_comision = p_id_motivo_comision;

    else
      select count(1) into v_existe
      from ipj.T_CM_MOTIVO
      where
        id_tramite_ipj = p_id_tramite_ipj and
        id_legajo = p_id_legajo and
        id_motivo_comision = p_id_motivo_comision;

      -- si no existe, lo agrego
      if v_existe = 0 then
        insert into ipj.T_CM_MOTIVO (id_tramite_ipj, id_legajo, id_motivo_comision)
        values (p_id_tramite_ipj, p_id_legajo, p_id_motivo_comision);
      end if;
    end if;

    o_rdo := 'OK';
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    commit;
  EXCEPTION
    WHEN OTHERS THEN
      o_rdo := 'ERROR Sp_Guardar_CN_Motivo: ' || To_Char(SQLCODE) || '-' || SQLERRM;
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      rollback;
  END Sp_Guardar_CN_Motivo;

  PROCEDURE SP_Guardar_CN_Normalizador(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in number,
    p_id_legajo in number,
    -- Datos Persona
    p_id_integrante in number,
    p_id_sexo in varchar2,
    p_nro_documento in varchar2,
    p_pai_cod_pais in varchar2,
    p_id_numero in number,
    p_detalle in varchar2,
    p_nombre in varchar2,
    p_apellido in varchar2,
    p_n_tipo_documento in varchar2,
    -- Datos Particulaes
    p_cuil in varchar2,
    p_id_normalizador in number,
    p_id_motivo_baja in number,
    p_id_estado_normalizador in number,
    p_mail in varchar2,
    p_telefono in varchar2,
    p_eliminar in number, -- 1 Elimina
    -- Domicilio Real
    p_id_vin_real in number,
    p_Id_Provincia_real In Varchar2,
    p_Id_Departamento_real In Number,
    p_Id_Localidad_real In Number,
    p_Barrio_real In Varchar2,
    p_Calle_real In Varchar2,
    p_Altura_real In Number,
    p_Depto_real In Varchar2,
    p_Piso_real In Varchar2,
    p_Torre_real In Varchar2,
    p_manzana_real in varchar2,
    p_lote_real in varchar2,
    p_id_barrio_real in number,
    p_id_calle_real in number,
    p_id_tipocalle_real in number,
    p_km_real in varchar2
    )
  IS
    v_bloque varchar2(100);
    v_id_vin_real number(20);
    v_usuario varchar2(20);
    v_Usr_CiDi2 number;
    v_habilitado char;
    v_nombre varchar2(200);
    v_rdo_integ varchar2(1000);
    v_tipo_mensaje_integ number;
    v_id_integrante number;
    v_existe_rcivil number;
    v_nombre_rc varchar2(250);
    v_apellido varchar2(250);
  BEGIN
  /*****************************************************
    Agrega, modifica o elimina los autorizados para el tr�mite online.
    Guarda los datos asociados en RCivil.
    No maneja transaccion, ya que se usa en cascada desde la aplicacion y lo administra el codigo.
  ******************************************************/
    if IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('HAB_LOG_JURIDICO') = '1' then
      IPJ.VARIOS.SP_GUARDAR_LOG(
        p_METODO => 'SP_Guardar_CN_Normalizador',
        p_NIVEL => 'DB - Gestion',
        p_ORIGEN => 'DB',
        p_USR_LOG  => 'DB',
        p_USR_WINDOWS => 'Sistema',
        p_IP_PC  => null,
        p_EXCEPCION =>
          'Tramite IPJ = ' || to_char(p_id_tramite_ipj)
          || ' / Id Legajo = ' || to_char(p_id_legajo)
          || ' / Id Integrante = ' || to_char(p_id_integrante)
          || ' / Id Sexo = ' || p_id_sexo
          || ' / Nro Documento = ' || p_nro_documento
          || ' / Pai Cod Pais = ' || p_pai_cod_pais
          || ' / Id Numero = ' || to_char(p_id_numero)
          || ' / Detalle = ' || p_detalle
          || ' / Nombre = ' || p_nombre
          || ' / Apellido = ' || p_apellido
          || ' / Tipo Documento = ' || p_n_tipo_documento
          || ' / Cuil = ' || p_Cuil
          || ' / Id Normalizador = ' || to_char(p_id_normalizador)
          || ' / Id Motivo Baja = ' || to_char(p_id_motivo_baja)
          || ' / Id Estado Norm = ' || to_char(p_id_estado_normalizador)
          || ' / Mail = ' || p_mail
          || ' / TE = ' || p_telefono
          || ' / Eliminar = ' || to_char(p_eliminar)
          || ' / Id Vin = ' || to_char(p_id_vin_real)
          || ' / Id Prov = ' || p_Id_Provincia_real
          || ' / Id Depto = ' || to_char(p_Id_Departamento_real)
          || ' / Id Localidad = ' || to_char(p_Id_Localidad_real)
          || ' / Barrio = ' || p_Barrio_real
          || ' / Calle = ' || p_Calle_real
          || ' / Altura = ' || to_char(p_Altura_real)
          || ' / Depto = ' || p_Depto_real
          || ' / Piso = ' || p_Piso_real
          || ' / Torre = ' || p_Torre_real
          || ' / Manzana = ' || p_manzana_real
          || ' / Lote = ' || p_lote_real
          || ' / Id Barrio = ' || to_char(p_id_barrio_real)
          || ' / Id Calle = ' || to_char(p_id_calle_real)
          || ' / Id Tipo Calle = ' || to_char(p_id_tipocalle_real)
          || ' / Km = ' || p_km_real
      );
    end if;

    -- Controlo que venga una persona
    if nvl(p_id_integrante, 0) = 0 then
      -- Verifico si esta o no en RCIVIL para ajustar los par�metros en el guardar
      select count(1) into v_existe_rcivil
      from rcivil.vt_pk_persona
      where
        id_sexo = p_id_sexo and
        nro_documento = p_nro_documento and
        pai_cod_pais = p_pai_cod_pais and
        id_numero = p_id_numero;

      if v_existe_rcivil > 0 then
        v_nombre_rc := null;
        v_apellido := null;
      else
        v_nombre_rc := p_nombre;
        v_apellido := p_apellido;
      end if;

      IPJ.ENTIDAD_PERSJUR.SP_GUARDAR_INTEGRANTES(
        o_rdo => v_rdo_integ,
        o_tipo_mensaje => v_tipo_mensaje_integ,
        o_id_integrante => v_id_integrante,
        p_id_sexo => p_id_sexo,
        p_nro_documento => p_nro_documento,
        p_pai_cod_pais => p_pai_cod_pais,
        p_id_numero => p_id_numero,
        p_cuil => p_cuil,
        p_detalle => p_detalle,
        p_nombre => v_nombre_rc,
        p_apellido => v_apellido,
        p_error_dato => 'N',
        p_n_tipo_documento => p_n_tipo_documento
      );

      if v_rdo_integ <> TYPES.c_Resp_OK then
        o_rdo := 'ERROR: No se pudo registrar la persona';
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
        return;
      end if;
    else
      v_id_integrante := p_id_integrante;
    end if;

    --Busco el responsable del tramite
    select t.cuil_ult_estado into v_usuario
    from ipj.t_tramitesipj t
    where
      id_tramite_ipj = p_id_tramite_ipj;

    /**** ****************************************************
      VALIDO QUE LA PERSONA NO PERTENEZCA A LA ULTIMAS AUTORIDADES ELECTAS
    ********************************************************/
    -- Si no esta habilitado, y no se elimina o rechaza, lo informo
    v_habilitado := IPJ.PK_TRAMITES_ONLINE.FC_Normaliz_Autorizado (p_id_legajo, v_id_integrante);
    if v_Habilitado = 'N' and p_eliminar <> 1 and p_id_estado_normalizador <> 3 then
      -- Busco el nombre de la persona para el mensaje de error
      select (select nombre || ' ' || apellido from rcivil.vt_pk_persona p where p.id_Sexo = i.id_sexo and p.nro_documento = i.nro_documento and p.pai_cod_pais = i.pai_cod_pais and p.id_numero = i.id_numero) into v_nombre
      from ipj.t_integrantes i
      where
        id_integrante = v_id_integrante;

      o_rdo := 'El Normalizador ' || v_nombre || ' pertenece a la �ltimas autoridades inscriptas en IPJ.';
      o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
      return;
    end if;

    --******************************************************
    -- CUERPO DEL PROCEDIMEINTO
    --******************************************************

    -- Al eliminar, se quitan los detalles relacionados al socio
    if p_eliminar = 1 then
      delete IPJ.T_CN_NORMALIZADOR
      where
        id_tramite_ipj = p_id_tramite_ipj and
        id_legajo = p_id_legajo and
        id_integrante = v_id_integrante;

    else
      -- Domicilo Integrante
      if p_id_departamento_real <> 0 then
        v_bloque := 'Comision Normalizador - Domicilio Integ';
        IPJ.VARIOS.SP_GUARDAR_DOMICILIO(
          o_id_vin => v_id_vin_real,
          o_rdo => o_rdo,
          o_tipo_mensaje => o_tipo_mensaje,
          p_id_vin => p_id_vin_real,
          p_usuario => v_usuario,
          p_Tipo_Domicilio => 3, -- 3 Real //  0 Sin Asignar
          p_id_integrante => v_id_integrante,
          p_id_legajo => 0,
          p_cuit_empresa => null,
          p_id_fondo_comercio => NULL,
          p_id_entidad_acta => NULL,
          p_es_comerciante => 'N', -- S / N
          p_es_admin_entidad => 'N',  -- S / N
          p_Id_Provincia => p_id_provincia_real,
          p_Id_Departamento => p_id_departamento_real,
          p_Id_Localidad => p_id_localidad_real,
          p_Barrio => p_barrio_real,
          p_Calle => p_calle_real,
          p_Altura => p_altura_real,
          p_Depto => p_depto_real,
          p_Piso => p_piso_real,
          p_torre => p_torre_real,
          p_manzana => p_manzana_real,
          p_lote => p_lote_real,
          p_id_barrio => p_id_barrio_real,
          p_id_calle => p_id_calle_real,
          p_id_tipocalle => p_id_tipocalle_real,
          p_km => p_km_real
        );

        v_id_vin_real := p_id_vin_real;

        if o_rdo  <> IPJ.TYPES.C_RESP_OK then
          o_rdo := v_bloque || ': ' || o_rdo;
          rollback;
          return;
        end if;
      end if;

      -- actualizo el tipo de solicitante
      update IPJ.T_CN_NORMALIZADOR
      set
        id_normalizador = p_id_normalizador,
        id_motivo_baja = decode(p_id_motivo_baja, 0, null, p_id_motivo_baja),
        id_estado_normalizador = decode(p_id_estado_normalizador, 0, null, p_id_estado_normalizador)
      where
        id_tramite_ipj = p_id_tramite_ipj and
        id_legajo = p_id_legajo and
        id_integrante = v_id_integrante;

       -- si no existe, lo agrego
      if sql%rowcount = 0  then
        INSERT INTO IPJ.T_CN_NORMALIZADOR (id_tramite_ipj, id_legajo, id_integrante, id_normalizador, id_motivo_baja, id_estado_normalizador)
        VALUES (p_id_tramite_ipj, p_id_legajo, v_id_integrante, p_id_normalizador, decode(p_id_motivo_baja, 0, null, p_id_motivo_baja), decode(p_id_estado_normalizador, 0, null, p_id_estado_normalizador));
      end if;

      -- Guardo el TELEFONO si viene
      if p_telefono is not null then
        IPJ.VARIOS.SP_Guardar_Telefono_Mail(
          o_rdo => o_rdo,
          o_tipo_mensaje => o_tipo_mensaje,
          p_mail => NULL,
          p_telefono => p_telefono,
          p_id_sexo => p_id_sexo,
          p_nro_documento => p_nro_documento,
          p_pai_cod_pais => p_pai_cod_pais,
          p_id_numero => p_id_numero,
          p_cuit_empresa => NULL,
          p_sede_empresa => NULL,
          p_tramite => NULL,
          p_te_caract => null
        );

        if o_rdo  <> IPJ.TYPES.C_RESP_OK then
          rollback;
          return;
        end if;
      end if;

      -- GUARDO EL MAIL SI VIENE
      if p_mail is not null then
        IPJ.VARIOS.SP_Guardar_Telefono_Mail(
          o_rdo => o_rdo,
          o_tipo_mensaje => o_tipo_mensaje,
          p_mail => p_mail,
          p_telefono =>NULL,
          p_id_sexo => p_id_sexo,
          p_nro_documento => p_nro_documento,
          p_pai_cod_pais => p_pai_cod_pais,
          p_id_numero => p_id_numero,
          p_cuit_empresa => NULL,
          p_sede_empresa => NULL,
          p_tramite => NULL,
          p_te_caract => null
        );

        if o_rdo  <> IPJ.TYPES.C_RESP_OK then
          rollback;
          return;
        end if;
      end if;

      -- GUARDO EL CUIL EN RCIVIL
      if p_cuil is not null then
        v_bloque := 'Comision Normalizador - CUIL ';
        IPJ.VARIOS.SP_Guardar_CUIL_RCivil(
          o_rdo => o_rdo,
          o_tipo_mensaje => o_tipo_mensaje,
          p_cuil => replace(p_cuil, '-', ''),
          p_id_sexo => p_id_sexo,
          p_nro_documento => p_nro_documento,
          p_pai_cod_pais => p_pai_cod_pais,
          p_id_numero => p_id_numero
        );
      end if;

      if o_rdo  <> IPJ.TYPES.C_RESP_OK then
        o_rdo := v_bloque || ': ' || o_rdo;
        rollback;
        return;
      end if;
    end if;

    o_rdo := TYPES.c_Resp_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    commit;
  EXCEPTION
    WHEN OTHERS THEN
       o_rdo := 'ERROR SP_Guardar_CN_Normalizador (' || v_bloque || ') :' || To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
       rollback;
  END SP_Guardar_CN_Normalizador;

  PROCEDURE SP_Guardar_CN_Solicitante(
    o_rdo out varchar2,
    o_tipo_mensaje out number,
    p_id_tramite_ipj in number,
    p_id_legajo in number,
    -- Datos Personas
    p_id_integrante in number,
    p_id_sexo in varchar2,
    p_nro_documento in varchar2,
    p_pai_cod_pais in varchar2,
    p_id_numero in number,
    p_detalle in varchar2,
    p_nombre in varchar2,
    p_apellido in varchar2,
    p_n_tipo_documento in varchar2,
    -- Varios
    p_mail in varchar2,
    p_telefono in varchar2,
    p_eliminar in number, -- 1 Elimina
    -- Domicilio Real
    p_id_vin_real in number,
    p_Id_Provincia_real In Varchar2,
    p_Id_Departamento_real In Number,
    p_Id_Localidad_real In Number,
    p_Barrio_real In Varchar2,
    p_Calle_real In Varchar2,
    p_Altura_real In Number,
    p_Depto_real In Varchar2,
    p_Piso_real In Varchar2,
    p_Torre_real In Varchar2,
    p_manzana_real in varchar2,
    p_lote_real in varchar2,
    p_id_barrio_real in number,
    p_id_calle_real in number,
    p_id_tipocalle_real in number,
    p_km_real in varchar2,
    -- Particulares
    p_id_solicitante_cn in number
    )
  IS
    v_bloque varchar2(100);
    v_tipo_mensaje number;
    v_id_vin_real number(20);
    v_usuario varchar2(20);
    v_rdo_integ varchar2(1000);
    v_tipo_mensaje_integ number;
    v_id_integrante number;
    v_existe_rcivil number;
    v_nombre varchar2(250);
    v_apellido varchar2(250);
  BEGIN
  /*****************************************************
    Agrega, modifica o elimina los autorizados para el tr�mite online.
    Guarda los datos asociados en RCivil.
    No maneja transaccion, ya que se usa en cascada desde la aplicacion y lo administra el codigo.
  ******************************************************/
    if IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('HAB_LOG_JURIDICO') = '1' then
      IPJ.VARIOS.SP_GUARDAR_LOG(
        p_METODO => 'SP_Guardar_CN_Solicitante',
        p_NIVEL => 'DB - Gestion',
        p_ORIGEN => 'DB',
        p_USR_LOG  => 'DB',
        p_USR_WINDOWS => 'Sistema',
        p_IP_PC  => null,
        p_EXCEPCION =>
          'Tramite IPJ = ' || to_char(p_id_tramite_ipj)
          || ' / Id Legajo = ' || to_char(p_id_legajo)
          || ' / Id Integrante = ' || to_char(p_id_integrante)
          || ' / Id Sexo = ' || p_id_sexo
          || ' / Nro Documento = ' || p_nro_documento
          || ' / Pai Cod Pais = ' || p_pai_cod_pais
          || ' / Id Numero = ' || to_char(p_id_numero)
          || ' / Detalle = ' || p_detalle
          || ' / Nombre = ' || p_nombre
          || ' / Apellido = ' || p_apellido
          || ' / Tipo Documento = ' || p_n_tipo_documento
          || ' / Mail = ' || p_mail
          || ' / TE = ' || p_telefono
          || ' / Eliminar = ' || to_char(p_eliminar)
          || ' / Id Vin Real = ' || to_char(p_id_vin_real)
          || ' / Id Prov = ' || p_Id_Provincia_real
          || ' / Id Depart = ' || to_char(p_Id_Departamento_real)
          || ' / Id Local = ' || to_char(p_Id_Localidad_real)
          || ' / Barrio = ' || p_Barrio_real
          || ' / Calle = ' || p_Calle_real
          || ' / Altura = ' || to_char(p_Altura_real)
          || ' / Depto = ' || p_Depto_real
          || ' / Piso = ' || p_Piso_real
          || ' / Torre = ' || p_Torre_real
          || ' / Id Barrio = ' || to_char(p_id_barrio_real)
          || ' / Id Calle = ' || to_char(p_id_calle_real)
          || ' / Tipo Calle = ' || to_char(p_id_tipocalle_real)
          || ' / Km = ' || p_km_real
          || ' / Tipo Solicitante = ' || to_char(p_id_solicitante_cn)
      );
    end if;

    -- CUERPO DEL PROCEDIMEINTO
    -- Controlo que venga una persona
    if nvl(p_id_integrante, 0) = 0 then
      -- Verifico si esta o no en RCIVIL para ajustar los par�metros en el guardar
      select count(1) into v_existe_rcivil
      from rcivil.vt_pk_persona
      where
        id_sexo = p_id_sexo and
        nro_documento = p_nro_documento and
        pai_cod_pais = p_pai_cod_pais and
        id_numero = p_id_numero;

      if v_existe_rcivil > 0 then
        v_nombre := null;
        v_apellido := null;
      else
        v_nombre := p_nombre;
        v_apellido := p_apellido;
      end if;

      IPJ.ENTIDAD_PERSJUR.SP_GUARDAR_INTEGRANTES(
        o_rdo => v_rdo_integ,
        o_tipo_mensaje => v_tipo_mensaje_integ,
        o_id_integrante => v_id_integrante,
        p_id_sexo => p_id_sexo,
        p_nro_documento => p_nro_documento,
        p_pai_cod_pais => p_pai_cod_pais,
        p_id_numero => p_id_numero,
        p_cuil => null,
        p_detalle => p_detalle,
        p_nombre => v_nombre,
        p_apellido => v_apellido,
        p_error_dato => 'N',
        p_n_tipo_documento => p_n_tipo_documento
      );

      if v_rdo_integ <> TYPES.c_Resp_OK then
        o_rdo := 'ERROR: No se pudo registrar la persona';
        o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
        return;
      end if;
    else
      v_id_integrante := p_id_integrante;
    end if;

    --Busco el responsable del tramite
    select t.cuil_ult_estado into v_usuario
    from ipj.t_tramitesipj t
    where
      id_tramite_ipj = p_id_tramite_ipj;

    -- Al eliminar, se quitan los detalles relacionados al socio
    if p_eliminar = 1 then
      delete IPJ.T_CN_SOLICITANTE
      where
        id_tramite_ipj = p_id_tramite_ipj and
        id_legajo = p_id_legajo and
        id_integrante = v_id_integrante;

    else
      -- Domicilo Integrante
      if p_id_departamento_real <> 0 then
        v_bloque := 'Comision Solicitante - Domicilio Integ';
        IPJ.VARIOS.SP_GUARDAR_DOMICILIO(
          o_id_vin => v_id_vin_real,
          o_rdo => o_rdo,
          o_tipo_mensaje => o_tipo_mensaje,
          p_id_vin => p_id_vin_real,
          p_usuario => v_usuario,
          p_Tipo_Domicilio => 3, -- 3 Real //  0 Sin Asignar
          p_id_integrante => v_id_integrante,
          p_id_legajo => 0,
          p_cuit_empresa => null,
          p_id_fondo_comercio => NULL,
          p_id_entidad_acta => NULL,
          p_es_comerciante => 'N', -- S / N
          p_es_admin_entidad => 'N',  -- S / N
          p_Id_Provincia => p_id_provincia_real,
          p_Id_Departamento => p_id_departamento_real,
          p_Id_Localidad => p_id_localidad_real,
          p_Barrio => p_barrio_real,
          p_Calle => p_calle_real,
          p_Altura => p_altura_real,
          p_Depto => p_depto_real,
          p_Piso => p_piso_real,
          p_torre => p_torre_real,
          p_manzana => p_manzana_real,
          p_lote => p_lote_real,
          p_id_barrio => p_id_barrio_real,
          p_id_calle => p_id_calle_real,
          p_id_tipocalle => p_id_tipocalle_real,
          p_km => p_km_real
        );

        v_id_vin_real := p_id_vin_real;

        if o_rdo  <> IPJ.TYPES.C_RESP_OK then
          o_rdo := v_bloque || ': ' || o_rdo;
          rollback;
          return;
        end if;
      end if;

      -- actualizo el tipo de solicitante
      update IPJ.T_CN_SOLICITANTE
      set id_solicitante_cn = p_id_solicitante_cn
      where
        id_tramite_ipj = p_id_tramite_ipj and
        id_legajo = p_id_legajo and
        id_integrante = v_id_integrante;

       -- si no existe, lo agrego
      if sql%rowcount = 0  then
        INSERT INTO IPJ.T_CN_SOLICITANTE (id_tramite_ipj, id_legajo, id_integrante, id_solicitante_cn)
        VALUES (p_id_tramite_ipj, p_id_legajo, v_id_integrante, p_id_solicitante_cn);
      end if;

      -- Guardo el TELEFONO si viene
      if p_telefono is not null then
        IPJ.VARIOS.SP_Guardar_Telefono_Mail(
          o_rdo => o_rdo,
          o_tipo_mensaje => o_tipo_mensaje,
          p_mail => NULL,
          p_telefono => p_telefono,
          p_id_sexo => p_id_sexo,
          p_nro_documento => p_nro_documento,
          p_pai_cod_pais => p_pai_cod_pais,
          p_id_numero => p_id_numero,
          p_cuit_empresa => NULL,
          p_sede_empresa => NULL,
          p_tramite => NULL,
          p_te_caract => null
        );

        if o_rdo  <> IPJ.TYPES.C_RESP_OK then
          rollback;
          return;
        end if;
      end if;

      -- GUARDO EL MAIL SI VIENE
      if p_mail is not null then
        IPJ.VARIOS.SP_Guardar_Telefono_Mail(
          o_rdo => o_rdo,
          o_tipo_mensaje => o_tipo_mensaje,
          p_mail => p_mail,
          p_telefono =>NULL,
          p_id_sexo => p_id_sexo,
          p_nro_documento => p_nro_documento,
          p_pai_cod_pais => p_pai_cod_pais,
          p_id_numero => p_id_numero,
          p_cuit_empresa => NULL,
          p_sede_empresa => NULL,
          p_tramite => NULL,
          p_te_caract => null
        );

        if o_rdo  <> IPJ.TYPES.C_RESP_OK then
          rollback;
          return;
        end if;
      end if;
    end if;

    o_rdo := TYPES.c_Resp_OK;
    o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_OK;
    commit;
  EXCEPTION
    WHEN OTHERS THEN
       o_rdo := 'ERROR SP_Guardar_CN_Solicitante (' || v_bloque || ') :' || To_Char(SQLCODE) || '-' || SQLERRM;
       o_tipo_mensaje := IPJ.TYPES.C_TIPO_MENS_ERROR;
       rollback;
  END SP_Guardar_CN_Solicitante;

end Juridico;
/

