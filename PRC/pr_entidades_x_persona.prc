CREATE OR REPLACE PROCEDURE IPJ.PR_Entidades_x_Persona(
    p_Cuil in VARCHAR2,
    o_Cursor OUT TYPES.cursorType,
    o_Nombre out varchar2)
  IS
  /**********************************************************************
    Lista todas las entidades sonde la eprsona figura de alguina manera asociada
  ***********************************************************************/
    v_id_integrante ipj.t_integrantes.id_integrante%TYPE;
  BEGIN
    -- Busco si existe un integrante en IPJ
    begin
      select id_integrante, nombre || ' ' || apellido into v_id_integrante, o_nombre
      from rcivil.vt_pk_persona p left join ipj.t_integrantes i
          on p.id_sexo = i.id_sexo and p.nro_documento = i.nro_documento and p.pai_cod_pais = i.pai_cod_pais and p.id_numero = i.id_numero
      where
        p.cuil = p_cuil;
    exception
      when NO_DATA_FOUND then
        v_id_integrante := -1;
        o_nombre:= 'Persona no encontrada.';
    end;

    OPEN o_Cursor FOR
      select id_legajo,
        (select denominacion_sia from ipj.t_legajos l where l.id_legajo = t.id_legajo) Denominacion,
        (select cuit from ipj.t_legajos l where l.id_legajo = t.id_legajo) Cuit,
        (select tipo_entidad from ipj.t_legajos l join ipj.t_tipos_entidades t on l.id_tipo_entidad = t.id_tipo_entidad where l.id_legajo = t.id_legajo) Tipo_Entidad,
        max(Admin) Administrador, max(Admin_Repres) Admin_repres, Max(Sindico) Sindico, Max(Repres) Repres, Max(Benef_Fidei) Benef_Fidei, max(Fideicomisario) Fideicomisario, max(Fiduciante) Fiduciante,
        Max(Fiduciario) Fiduciario, Max(Socio) Socio, max(Socio_Repres) Socio_Repres, Max(Socio_Cop) Socio_Cop, max(Socio_Usuf) Socio_Usuf, Max(Denunciado) Denunciado, max(Normalizador) Normalizador
      from (
      select distinct id_legajo, 'S' Admin, null Admin_Repres, null Sindico, null Repres, null Benef_Fidei, null Fideicomisario, null Fiduciante, null Fiduciario, null Socio, null Socio_Repres, null Socio_Cop, null Socio_Usuf, null Denunciado, null Normalizador from ipj.t_entidades_admin i where id_integrante = v_id_integrante union
      select distinct id_legajo, null Admin, 'S' Admin_Repres, null Sindico, null Repres, null Benef_Fidei, null Fideicomisario, null Fiduciante, null Fiduciario, null Socio, null Socio_Repres, null Socio_Cop, null Socio_Usuf, null Denunciado, null Normalizador from ipj.t_entidades_admin i where id_integrante_repres = v_id_integrante union
      select distinct id_legajo, null Admin, null Admin_Repres, 'S' Sindico, null Repres, null Benef_Fidei, null Fideicomisario, null Fiduciante, null Fiduciario, null Socio, null Socio_Repres, null Socio_Cop, null Socio_Usuf, null Denunciado, null Normalizador from ipj.t_entidades_sindico i where id_integrante = v_id_integrante union
      select distinct id_legajo, null Admin, null Admin_Repres, null Sindico, 'S' Repres, null Benef_Fidei, null Fideicomisario, null Fiduciante, null Fiduciario, null Socio, null Socio_Repres, null Socio_Cop, null Socio_Usuf, null Denunciado, null Normalizador from ipj.t_entidades_representante i where id_integrante = v_id_integrante union
      select distinct id_legajo, null Admin, null Admin_Repres, null Sindico, null Repres, 'S' Benef_Fidei, null Fideicomisario, null Fiduciante, null Fiduciario, null Socio, null Socio_Repres, null Socio_Cop, null Socio_Usuf, null Denunciado, null Normalizador from ipj.t_entidades_benef i where id_integrante = v_id_integrante union
      select distinct id_legajo, null Admin, null Admin_Repres, null Sindico, null Repres, null Benef_Fidei, 'S' Fideicomisario, null Fiduciante, null Fiduciario, null Socio, null Socio_Repres, null Socio_Cop, null Socio_Usuf, null Denunciado, null Normalizador from ipj.t_entidades_fideicom i where id_integrante = v_id_integrante union
      select distinct id_legajo, null Admin, null Admin_Repres, null Sindico, null Repres, null Benef_Fidei, null Fideicomisario, 'S' Fiduciante, null Fiduciario, null Socio, null Socio_Repres, null Socio_Cop, null Socio_Usuf, null Denunciado, null Normalizador from ipj.t_entidades_fiduciantes i where id_integrante = v_id_integrante union
      select distinct id_legajo, null Admin, null Admin_Repres, null Sindico, null Repres, null Benef_Fidei, null Fideicomisario, null Fiduciante, 'S' Fiduciario, null Socio, null Socio_Repres, null Socio_Cop, null Socio_Usuf, null Denunciado, null Normalizador from ipj.t_entidades_fiduciarios i where id_integrante = v_id_integrante union
      select distinct id_legajo, null Admin, null Admin_Repres, null Sindico, null Repres, null Benef_Fidei, null Fideicomisario, null Fiduciante, null Fiduciario, 'S' Socio, null Socio_Repres, null Socio_Cop, null Socio_Usuf, null Denunciado, null Normalizador from ipj.t_entidades_socios i where id_integrante = v_id_integrante union
      select distinct id_legajo, null Admin, null Admin_Repres, null Sindico, null Repres, null Benef_Fidei, null Fideicomisario, null Fiduciante, null Fiduciario, null Socio, 'S' Socio_Repres, null Socio_Cop, null Socio_Usuf, null Denunciado, null Normalizador from ipj.t_entidades_socios i where id_integrante_representante = v_id_integrante union
      select distinct s.id_legajo, null Admin, null Admin_Repres, null Sindico, null Repres, null Benef_Fidei, null Fideicomisario, null Fiduciante, null Fiduciario, null Socio, null Socio_Repres, 'S' Socio_Cop, null Socio_Usuf, null Denunciado, null Normalizador from ipj.t_entidades_socios_copro i join ipj.t_entidades_socios s on i.id_entidad_socio = s.id_entidad_socio where i.id_integrante = v_id_integrante union
      select distinct s.id_legajo, null Admin, null Admin_Repres, null Sindico, null Repres, null Benef_Fidei, null Fideicomisario, null Fiduciante, null Fiduciario, null Socio, null Socio_Repres, null Socio_Cop, 'S' Socio_Usuf, null Denunciado, null Normalizador from ipj.t_entidades_socios_usuf i join ipj.t_entidades_socios s on i.id_entidad_socio = s.id_entidad_socio where i.id_integrante = v_id_integrante union
      select distinct id_legajo, null Admin, null Admin_Repres, null Sindico, null Repres, null Benef_Fidei, null Fideicomisario, null Fiduciante, null Fiduciario, null Socio, null Socio_Repres, null Socio_Cop, null Socio_Usuf, 'S' Denunciado, null Normalizador from ipj.t_denunciados i where id_integrante = v_id_integrante union
      select distinct id_legajo, null Admin, null Admin_Repres, null Sindico, null Repres, null Benef_Fidei, null Fideicomisario, null Fiduciante, null Fiduciario, null Socio, null Socio_Repres, null Socio_Cop, null Socio_Usuf, null Denunciado, 'S' Normalizador from ipj.t_cn_normalizador i where id_integrante = v_id_integrante
      ) t
      group by id_legajo
      order by tipo_entidad, denominacion asc;

  END PR_Entidades_x_Persona;
/

