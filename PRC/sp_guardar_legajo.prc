CREATE OR REPLACE PROCEDURE IPJ.SP_Guardar_LEGAJO(p_id_legajo        in number,
                                                  p_nro_matricula    in VARCHAR2,
                                                  p_nro_ficha        in VARCHAR2,
                                                  p_denominacion_sia in VARCHAR2,
                                                  p_cuit             in VARCHAR2,
                                                  p_sede             in VARCHAR2,
                                                  p_folio            in number,
                                                  p_tomo             in number,
                                                  p_anio             in number,
                                                  p_id_proceso       in number,
                                                  p_id_tipo_entidad  in number,
                                                  p_usuario_alta     in varchar2,
                                                  p_usuario_modif    in varchar2,
                                                  p_id_legajo_Rdo    OUT varchar2,
                                                  p_rdo              OUT number,
                                                  p_rdoError         OUT VARCHAR2,
                                                  p_Sticker          in varchar2,
                                                  p_registro         in number,
                                                  p_denominacion_sia_ant in VARCHAR

                                                  ) IS
  p_legajo            number(15);
  v_Legajo_formado    varchar2(200);
  v_Row_PersJur       T_COMUNES.vt_pers_juridicas_completa%ROWTYPE;
  v_PersJur_NewSede   varchar2(3);
  v_result_PersJur    varchar2(200);
  v_id_tramite_legajo number(15);
  v_Cant              number(15);

BEGIN

  --p_id_legajo =0 nuevo sino es una modificacion
  if p_id_legajo = 0 then
    -- Nuevo legajo
    SELECT seq_legajos.nextval INTO p_legajo FROM dual;
    -- muestro el legajo nuevo o modificado
    -- p_id_legajo_Rdo := p_legajo;

    insert into ipj.t_legajos
      (id_legajo,
       nro_matricula,
       nro_ficha,
       denominacion_sia,
       cuit,
       sede,
       folio,
       tomo,
       anio,
       id_proceso,
       id_tipo_entidad,
       fecha_alta,
       usuario_alta,
       eliminado,
       registro,
       denominacion_sia_ant)
    values
      (p_legajo,
       p_nro_matricula,
       p_nro_ficha,
       upper(p_denominacion_sia),
       p_cuit,
       p_sede,
       p_folio,
       p_tomo,
       p_anio,
       p_id_proceso,
       p_id_tipo_entidad,
       sysdate(),
       p_usuario_alta,
       '0',
       p_registro,
       p_denominacion_sia_ant);
    --Verifico si se realizaron los cambios
    p_rdo := sql%rowcount;
    --Formo el legajo
    if p_rdo = 1 and not p_id_tipo_entidad is null and
       p_id_tipo_entidad <> '0' then
      select (lpad(to_char(p_legajo), 10, '0') || '-' || te.contrato || '-' ||
             te.sigla)
        into v_Legajo_formado
        from ipj.t_tipos_entidades te
       where te.id_tipo_entidad = p_id_tipo_entidad;
    else
      v_Legajo_formado := lpad(to_char(p_legajo), 10, '0') || '-' || '-';
    end if;
    /*Historicos*/

      insert into ipj.t_legajos_hist
      (id_legajo,       nro_matricula,
       nro_ficha,       denominacion_sia,
       cuit,       sede,       folio,
       tomo,       anio,
       id_tipo_entidad,       fecha,
       usuario,       eliminado,       registro,denominacion_sia_ant)
    values
      (p_legajo,       p_nro_matricula,
       p_nro_ficha,       upper(p_denominacion_sia),
       p_cuit,       p_sede,
       p_folio,       p_tomo,
       p_anio,       p_id_tipo_entidad,
       sysdate(),       p_usuario_alta,
       '0',       p_registro,p_denominacion_sia_ant);


  else
    -- modificacion datos en legajo
    update ipj.t_legajos
       set cuit               = p_cuit,
           sede               = '00',
           id_tipo_entidad    = p_id_tipo_entidad,
           nro_matricula      = p_nro_matricula,
           nro_ficha          = p_nro_ficha,
           denominacion_sia   = upper(p_denominacion_sia),
           folio              = p_folio,
           tomo               = p_tomo,
           anio               = p_anio,
           fecha_modificacion = sysdate(),
           usuario_modif      = p_usuario_modif,
           registro=p_registro,
           denominacion_sia_ant=p_denominacion_sia_ant
     where id_legajo = p_id_legajo;

    --Verifico si se realizaron los cambios
    p_rdo := sql%rowcount;

    --Formo el legajo
    select (lpad(to_char(l.id_legajo), 10, '0') || '-' || te.contrato || '-' ||
           te.sigla)
      into v_Legajo_formado
      from ipj.t_legajos l
      left join ipj.t_tipos_entidades te
        on te.id_tipo_entidad = l.id_tipo_entidad
     where l.id_legajo = p_id_legajo;


     /*Historicos*/

      insert into ipj.t_legajos_hist
      (id_legajo,       nro_matricula,
       nro_ficha,       denominacion_sia,
       cuit,       sede,
       folio,       tomo,
       anio,       id_tipo_entidad,
       fecha,       usuario,
       eliminado,       registro,denominacion_sia_ant)
    values
      (p_id_legajo,       p_nro_matricula,
       p_nro_ficha,       upper(p_denominacion_sia),
       p_cuit,       '00',
       p_folio,       p_tomo,
       p_anio,       p_id_tipo_entidad,
       sysdate(),       p_usuario_modif,
       '0',       p_registro,p_denominacion_sia_ant);


  end if;
  p_id_legajo_Rdo := v_Legajo_formado;

  --- Actualizo persona juridica

  if p_rdo > 0 and not p_cuit is null then

    begin
      select *
        into v_Row_PersJur
        from T_COMUNES.vt_pers_juridicas_completa
       where cuit = p_cuit
         and id_sede = '00';

      T_COMUNES.pack_persona_juridica.MODIFICA_PERSJUR_VERT(P_CUIT             => p_cuit,
                                                            P_RAZON_SOCIAL     => upper(p_denominacion_sia),
                                                            P_NOM_FAN          => v_Row_PersJur.nombre_fantasia,
                                                            P_ID_FOR_JUR       => v_Row_PersJur.id_forma_juridica,
                                                            P_ID_COND_IVA      => v_Row_PersJur.id_condicion_iva,
                                                            P_ID_APLICACION    => Types.c_id_Aplicacion,
                                                            P_NRO_ING_BRUTO    => v_Row_PersJur.nro_ingbruto,
                                                            P_ID_COND_INGBRUTO => v_Row_PersJur.id_condicion_ingbruto,
                                                            O_RESULTADO        => v_result_PersJur);

      --Verifico si se realizaron los cambios
      p_rdo := sql%rowcount;

    exception
      when NO_DATA_FOUND then

        T_COMUNES.pack_persona_juridica.INSERTA_PERSJUR_VERT(P_CUIT             => p_cuit,
                                                             P_RAZON_SOCIAL     => upper(p_denominacion_sia),
                                                             P_NOM_FAN          => null,
                                                             P_ID_FOR_JUR       => null,
                                                             P_ID_COND_IVA      => null,
                                                             P_ID_APLICACION    => Types.c_id_Aplicacion,
                                                             P_FEC_INICIO_ACT   => sysdate,
                                                             P_NRO_ING_BRUTO    => null,
                                                             P_ID_COND_INGBRUTO => null,
                                                             O_ID_SEDE          => v_PersJur_NewSede,
                                                             O_RESULTADO        => v_result_PersJur);
        --Verifico si se realizaron los cambios
        p_rdo := sql%rowcount;

    end;

    if upper(v_result_PersJur) = 'OK' then
      p_rdoError := '';
    else
      p_rdoError := v_result_PersJur;
    end if;

  end if;

  /*Se agrega los tramites de suac a tramite legajo
  */
  if not p_Sticker is null then
    select count(*)
      into v_Cant
      from ipj.t_tramites_legajo l
     where l.sticker = p_Sticker
       and l.id_legajo = p_id_legajo;

    if v_Cant = 0 then

      -- Nuevo tramite legajo
      SELECT ipj.seq_tramites_legajo.nextval
        INTO v_id_tramite_legajo
        FROM dual;

      if p_id_legajo = 0 then

        insert into ipj.t_tramites_legajo
          (id_tramite_legajo, id_legajo, sticker)
        values
          (v_id_tramite_legajo, p_legajo, p_Sticker);
      else

        insert into ipj.t_tramites_legajo
          (id_tramite_legajo, id_legajo, sticker)
        values
          (v_id_tramite_legajo, p_id_legajo, p_Sticker);
      end if;
    end if;
  end if;

EXCEPTION
  WHEN OTHERS THEN
    p_rdoError := To_Char(SQLCODE) || '-' || SQLERRM;

END;
/

