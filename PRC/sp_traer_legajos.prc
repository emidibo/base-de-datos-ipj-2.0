CREATE OR REPLACE PROCEDURE IPJ.SP_Traer_legajos(p_id_legajo      in number,
                                             p_nro_matricula  in varchar2,
                                             p_nro_ficha      in varchar2,
                                             p_cuit           in varchar2,
                                             p_folio          in number,
                                             p_tomo           in number,
                                             p_anio           in number,
                                             p_razon_socialBD out varchar2,
                                             p_CuitBD         out varchar2,
                                             p_Cursor         OUT types.cursorType
                                             ,p_denominacion      in varchar2
                                             ,p_eliminado         in VARCHAR2,
                                             p_registro       in number
                                             ) IS


BEGIN
--  datos de ejemplos  30707394028 PENTACOMB S.A.


-- Controla en pjbd y legajo si viene cuit
  if not p_cuit is null then
    --buscamos PJ BD
    begin
      select pj.razon_social,  pj.cuit
        into p_razon_socialBD, p_CuitBD
        from t_comunes.vt_pers_juridicas_completa pj
       where pj.cuit = p_cuit
         and pj.id_sede = '00';

    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        --no existe en PJ BD
        p_razon_socialBD := 'No existe';
        p_CuitBD         := 'No existe';

    end;
  --buscamos en legajo
    OPEN p_Cursor FOR
    select  l.* , (lpad( to_char(l.id_legajo),10,'0')||'-'|| te.contrato ||'-' || te.sigla)  legajo
        ,um.descripcion Usuario_Modif_desc, ua.descripcion Usuario_Alta_desc
        from ipj.t_legajos l left join ipj.t_tipos_entidades te
        on te.id_tipo_entidad=l.id_tipo_entidad
        left join ipj.t_usuarios um on l.usuario_modif=um.cuil_usuario
         left join ipj.t_usuarios ua on l.usuario_alta=ua.cuil_usuario
       where (l.id_legajo = p_id_legajo or p_id_legajo is null)
       --  and (l.nro_matricula = p_nro_matricula or p_nro_matricula is null)
         and (upper(l.nro_ficha) =upper( p_nro_ficha) or p_nro_ficha is null)
         and (l.cuit = p_cuit or p_cuit is null)
          and (l.folio = p_folio or p_folio is null)
         and (l.tomo = p_tomo or p_tomo is null)
         and (l.anio = p_anio or p_anio is null)
         and (l.registro = p_registro or p_registro is null)
         and (upper(l.denominacion_sia)Like '%' || p_denominacion ||'%' or p_denominacion is null)
         and l.eliminado= p_eliminado;
  else
     --no existe en PJ BD
    p_razon_socialBD := 'No existe';
    p_CuitBD         := 'No existe';

    --- Busca el legajo
    OPEN p_Cursor FOR
      select  l.*
      , (lpad( to_char(l.id_legajo),10,'0')||'-'|| te.contrato ||'-' || te.sigla)  legajo
        ,um.descripcion Usuario_Modif_desc, ua.descripcion Usuario_Alta_desc
        from ipj.t_legajos l left join ipj.t_tipos_entidades te
        on te.id_tipo_entidad=l.id_tipo_entidad
          left join ipj.t_usuarios um on l.usuario_modif=um.cuil_usuario
          left join ipj.t_usuarios ua on l.usuario_alta=ua.cuil_usuario
       where (l.id_legajo = p_id_legajo or p_id_legajo is null)
         --and (l.nro_matricula = p_nro_matricula or p_nro_matricula is null)
         and (upper(l.nro_ficha) =upper( p_nro_ficha) or p_nro_ficha is null)
         and (l.folio = p_folio or p_folio is null)
         and (l.tomo = p_tomo or p_tomo is null)
         and (l.anio = p_anio or p_anio is null)
         and (l.registro = p_registro or p_registro is null)
         and (upper(l.denominacion_sia)Like '%' || p_denominacion ||'%' or p_denominacion is null)
          and l.eliminado= p_eliminado
          and ROWNUM  <100
             ;


  end if;



END;
/

