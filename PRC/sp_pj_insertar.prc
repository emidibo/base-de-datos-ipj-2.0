create or replace procedure ipj.SP_PJ_Insertar(

p_denominacion_sia in VARCHAR2,
p_cuit             in VARCHAR2,
p_rdoError         OUT VARCHAR2)

IS
  v_PersJur_NewSede varchar2(3):='00';
   v_result_PersJur varchar2(200);
BEGIN
      T_COMUNES.pack_persona_juridica.INSERTA_PERSJUR_VERT(
            P_CUIT => p_cuit,
            P_RAZON_SOCIAL => upper(p_denominacion_sia),
            P_NOM_FAN => null,
            P_ID_FOR_JUR => null,
            P_ID_COND_IVA => null,
            P_ID_APLICACION => Types.c_id_Aplicacion ,
            P_FEC_INICIO_ACT => sysdate,
            P_NRO_ING_BRUTO => null,
            P_ID_COND_INGBRUTO => null,
            O_ID_SEDE => v_PersJur_NewSede,
            O_RESULTADO =>  v_result_PersJur);
p_rdoError:=v_result_PersJur;
  commit;
END ;
/

