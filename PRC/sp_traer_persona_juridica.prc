CREATE OR REPLACE PROCEDURE IPJ.SP_Traer_Persona_Juridica(p_cuit         varchar2,
                                                      p_razon_social varchar2,
                                                      p_Cursor       OUT types.cursorType) IS

  vcant number;
BEGIN

  select count(*)
    into vcant
    from T_COMUNES.vt_pers_juridicas_completa
   where cuit = p_cuit
     and id_sede = '00';
  --Si existe cuit
  if vcant = 1 then

    OPEN p_Cursor FOR
      select razon_social,
             cuit,
             n_sede,
             n_forma_juridica,
             n_condicion_iva,
             to_char(fec_inicio_act, 'dd/mm/rrrr') fec_inicio_act,
             to_char(fec_fin_act, 'dd/mm/rrrr') fec_fin_act
        from T_COMUNES.vt_pers_juridicas_completa t
       where cuit = p_cuit
         and id_sede = '00';
  end if;
  if vcant = 0 and not p_razon_social is null then
    OPEN p_Cursor FOR
      select razon_social, cuit
        from T_COMUNES.vt_pers_juridicas_completa
       where trim(razon_social) = trim(p_razon_social);
  elsif vcant = 0 and p_razon_social is null then
        OPEN p_Cursor FOR
     select t.denominacion_sia razon_social, t.cuit cuit
        from ipj.t_legajos t
       where 1<>1;

  end if;

END;
/

