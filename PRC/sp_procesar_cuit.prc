CREATE OR REPLACE PROCEDURE IPJ.SP_PROCESAR_CUIT  is

  get_cuit types.cursorType;

  v_SQL varchar2(4000);
   v_Row_vtSuac ipj.tmp_proc_cuit%ROWTYPE;

BEGIN
 v_SQL:= 'SELECT *
                     FROM ipj.tmp_proc_cuit h
                     WHERE h.obs is null';
  
 open get_cuit for v_SQL  ;


 LOOP
   fetch get_cuit into v_Row_vtSuac;
   EXIT WHEN get_cuit%NOTFOUND or get_cuit%NOTFOUND is null;
 
 if(v_Row_vtSuac.Ficha is not null) then
   
      update ipj.t_legajos col set col.cuit = v_Row_vtSuac.cuit
     WHERE ipj.varios.FC_Formatear_Matricula(col.nro_ficha) = ipj.varios.FC_Formatear_Matricula(v_Row_vtSuac.ficha)
     and upper(trim(col.denominacion_sia)) = upper(trim(v_Row_vtSuac.descripcion))
     AND col.cuit is null;

 update ipj.tmp_proc_cuit o set o.obs = 'PROCESADO FICHA - ' || TO_CHAR(SYSDATE)
     WHERE o.ficha = v_Row_vtSuac.ficha and o.descripcion =v_Row_vtSuac.descripcion;
     
else
  if(nvl(v_Row_vtSuac.Registro,0)  > 0) then

  update ipj.t_legajos col set col.cuit = v_Row_vtSuac.cuit
     WHERE col.registro = v_Row_vtSuac.Registro
     and upper(trim(col.denominacion_sia)) = upper(trim(v_Row_vtSuac.descripcion))
     AND col.cuit is null;

update ipj.tmp_proc_cuit o set o.obs = 'PROCESADO REGISTRO - ' || TO_CHAR(SYSDATE)
     WHERE o.Registro = v_Row_vtSuac.Registro and o.descripcion =v_Row_vtSuac.descripcion;
     else
       
   
  update ipj.t_legajos col set col.cuit = v_Row_vtSuac.cuit
     WHERE upper(trim(col.denominacion_sia)) = upper(trim(v_Row_vtSuac.descripcion))
     AND col.cuit is null;  
     
     update ipj.tmp_proc_cuit o set o.obs = 'PROCESADO DESCRIP - ' || TO_CHAR(SYSDATE)
     WHERE   o.descripcion =v_Row_vtSuac.descripcion;
 end if;
  
 end if;  
 
     commit;
    END LOOP;
 
 close get_cuit;
 /*
 CONTROL
 
  SELECT substr(h.obs,1, instr(h.obs,'-')-1) Obs, count(1) Cant
  FROM ipj.tmp_proc_cuit h
  GROUP BY substr(h.obs,1, instr(h.obs,'-')-1)
  order by 2 desc
 */ 
 
END;
/

