CREATE OR REPLACE PROCEDURE IPJ.SP_TRAER_Grupos_UsuarioLegajo(
    o_Cursor OUT types.cursorType,
    p_cuil in varchar2)
  IS
  BEGIN
    /*Busca un cuil si tiene permisos en el grupo 6 Admin y 7 Mesa- legajo unico*/
  
    OPEN o_Cursor FOR
    
    select G.ID_GRUPO, G.CUIL_USUARIO, u.descripcion
      from ipj.t_grupos_usuario g join IPJ.t_usuarios u
        on G.CUIL_USUARIO = U.CUIL_USUARIO
      where
        g.id_grupo in ( 6,7)   and g.cuil_usuario=p_cuil and rownum=1
        order by g.id_grupo   ;

  END ;
/

