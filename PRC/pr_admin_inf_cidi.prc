CREATE OR REPLACE PROCEDURE IPJ.PR_Admin_Inf_CIDI(
    p_Cuit in VARCHAR2,
    p_Cursor OUT TYPES.cursorType)
  IS
  /**********************************************************************
    Este procedimiento lista las autoridades distintas de SOCIO de los 
    organos de administracion y fiscalizacion de una empresa para CIDI
  ***********************************************************************/
    v_Id_Tramite_Ipj ipj.t_tramitesipj.id_tramite_ipj%TYPE;
    v_Id_Tramiteipj_Accion ipj.t_tramitesipj_acciones.id_tramiteipj_accion%TYPE;
    v_Id_Legajo ipj.t_legajos.id_legajo%TYPE;
  BEGIN
  
    if IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('HAB_LOG_GESTION') = '1' then 
      IPJ.VARIOS.SP_GUARDAR_LOG(
        p_METODO => 'SP_Admin_Inf_CIDI',
        p_NIVEL => 'Gestion',
        p_ORIGEN => 'DB',
        p_USR_LOG  => 'DB',
        p_USR_WINDOWS => 'Sistema',
        p_IP_PC  => null,
        p_EXCEPCION =>
          'CUIT = ' || p_Cuit
      );
    end if;
    
    --Busco el legajo de la entidad
    BEGIN
      SELECT l.id_legajo
        INTO v_Id_Legajo
        FROM ipj.t_legajos l
       WHERE l.cuit = replace(p_Cuit, '-', '')
         AND nvl(l.eliminado,0) = 0
         AND l.id_legajo IN (SELECT e.id_legajo
                               FROM ipj.t_entidades e
                               JOIN ipj.t_tramitesipj t ON e.id_tramite_ipj = t.id_tramite_ipj);
    EXCEPTION
      WHEN OTHERS THEN
        v_Id_Legajo := NULL;
    END;
    
    IF v_Id_Legajo IS NOT NULL THEN
      BEGIN
        --Busco el �ltimo tr�mite de la entidad
        IPJ.Entidad_PersJur.SP_Buscar_Entidad_Tramite(
            o_Id_Tramite_Ipj => v_Id_Tramite_Ipj,
            o_id_tramiteipj_accion => v_Id_Tramiteipj_Accion,
            p_id_legajo => v_Id_Legajo,
            p_abiertos => 'N');
      EXCEPTION
        WHEN OTHERS THEN
          v_Id_Tramite_Ipj := NULL;
          v_Id_Tramiteipj_Accion := NULL;
      END;

      --Busco las autoridades de la entidad
      IPJ.Entidad_PersJur.SP_Traer_PersJur_Admin_Inf(
          p_id_tramite_ipj => v_Id_Tramite_Ipj,
          p_id_legajo => v_Id_Legajo,
          p_Cursor => p_Cursor);

    ELSE
      OPEN p_Cursor FOR
        SELECT 1 FROM dual WHERE 1 = 0;
    END IF;
  
  END PR_Admin_Inf_CIDI;
/

