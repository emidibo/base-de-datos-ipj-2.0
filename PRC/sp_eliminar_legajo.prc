CREATE OR REPLACE PROCEDURE IPJ.SP_Eliminar_Legajo(p_id_legajo        in number,
                                              p_rdo              OUT number,
                                              p_eliminado         in VARCHAR2,
                                              p_rdoError         OUT VARCHAR2,
                                               p_usuario_modif    in varchar2
                                              ) IS

BEGIN

delete from ipj.t_tramites_legajo t where t.id_legajo=p_id_legajo;

update ipj.t_legajos   set eliminado = p_eliminado,
fecha_modificacion = sysdate(), usuario_modif= p_usuario_modif
where id_legajo = p_id_legajo;
p_rdo := sql%rowcount;



EXCEPTION
  WHEN OTHERS THEN
    p_rdoError := To_Char(SQLCODE) || '-' || SQLERRM;

END;
/

