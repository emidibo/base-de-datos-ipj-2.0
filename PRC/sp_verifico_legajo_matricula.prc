CREATE OR REPLACE PROCEDURE IPJ.SP_Verifico_Legajo_Matricula(
p_matricula   in      varchar2,
p_legajo in varchar2,
p_existe out number) IS

  vlegajo number;
BEGIN
--Nueva legajo
if p_legajo=0 then


  select id_legajo  into vlegajo     from ipj.t_legajos
  where nro_ficha = p_matricula
   and ROWNUM  =1  ;

else
 -- Que no exista matricula en otro legajo
  select id_legajo   into vlegajo
    from ipj.t_legajos    where nro_ficha = p_matricula
     and id_legajo<>p_legajo
      and ROWNUM  =1  ;

  end if;
p_existe:=vlegajo;


  exception
      when NO_DATA_FOUND then
        p_existe:=0;

END;
/

