CREATE OR REPLACE PROCEDURE IPJ.SP_Traer_Tipo_Entidad
(
p_id_tipo_entidad number ,
p_tipo_entidad varchar2 ,
p_Cursor OUT types.cursorType                                          
) IS
BEGIN

  OPEN p_Cursor FOR
    select t.* from ipj.t_tipos_entidades t
    where (p_id_tipo_entidad is null or p_id_tipo_entidad=t.id_tipo_entidad)
    and (upper(t.tipo_entidad)=upper(p_tipo_entidad) or p_tipo_entidad is null)
    and t.contrato <>'R'
     order by t.tipo_entidad;

END;
/

