CREATE OR REPLACE PROCEDURE IPJ.SP_Traer_Tramite_Suac_Leg(p_sticker  in varchar2,
                                                      p_idLegajo in number,
                                                      p_existe   OUT number) IS

  vlegajo number;
BEGIN
  /*Busca un tramite legajo*/
  select t.id_legajo
    into vlegajo
    from ipj.t_tramites_legajo t
   where t.sticker = p_sticker
         -- sticker de prueba e historico
         and t.sticker<>'43889804050015'
          and t.sticker<>'45797604040315'
         and id_legajo <> p_idLegajo
         and ROWNUM = 1;

  p_existe := vlegajo;

exception
  when NO_DATA_FOUND then
    p_existe := 0;

END;
/

