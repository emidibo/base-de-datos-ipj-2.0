CREATE OR REPLACE PROCEDURE IPJ.SP_Verifico_legajo_Historico(
p_tomo   in      number,
p_folio   in      number,
p_anio  in      number,
p_libro   in      varchar2,
p_legajo in varchar2,
p_existe out number) IS

  vlegajo number;
BEGIN
--Nueva legajo 
if p_legajo=0 then 
  
  
  select id_legajo  into vlegajo     from ipj.t_legajos  t  
  where t.tomo=p_tomo and  t.folio=p_folio
   and t.anio=p_anio and t.libros=p_libro
   and ROWNUM  =1  ;
 
else
 -- Que no exista matricula en otro legajo 
  select t.id_legajo   into vlegajo
    from ipj.t_legajos t    
    where t.tomo=p_tomo and  t.folio=p_folio
   and t.anio=p_anio and t.libros=p_libro
     and t.id_legajo<>p_legajo
      and ROWNUM  =1  ;
  
  end if;
p_existe:=vlegajo;


  exception
      when NO_DATA_FOUND then
        p_existe:=0;
      
END;
/

