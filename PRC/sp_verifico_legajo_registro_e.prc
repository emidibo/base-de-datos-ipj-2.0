CREATE OR REPLACE PROCEDURE IPJ.SP_Verifico_Legajo_registro_E(p_registro in number,
                                                        p_legajo   in varchar2,
                                                        p_id_tipo_entidad   in varchar2,
                                                        p_existe   out number) IS

  vlegajo number;
BEGIN
  -- Legajo nuevo
  if p_legajo = 0 then
   -- Que no exista registro en otro legajo 
    select id_legajo
      into vlegajo
      from ipj.t_legajos 
     where registro = p_registro and id_tipo_entidad=p_id_tipo_entidad
       and ROWNUM = 1;
  
  else
    -- Que no exista registro en otro legajo y que no sea el legajo actual
    select id_legajo
      into vlegajo
      from ipj.t_legajos
     where registro = p_registro
       and id_legajo <> p_legajo  
       and id_tipo_entidad=p_id_tipo_entidad
       and ROWNUM = 1;
  
  end if;
  p_existe := vlegajo;

exception
  when NO_DATA_FOUND then
    p_existe := 0;
  
END;
/

