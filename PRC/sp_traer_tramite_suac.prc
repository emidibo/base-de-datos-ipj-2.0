CREATE OR REPLACE PROCEDURE IPJ.SP_Traer_Tramite_Suac(p_sticker in varchar2,
                                                  p_asunto  OUT varchar2) IS

  v_id                       varchar2(4000);
  v_nro_tramite              varchar2(4000);
  v_nro_sticker_completo     varchar2(4000);
  v_no_informatizado         varchar2(4000);
  v_estado                   varchar2(4000);
  v_reparticion_caratuladora varchar2(4000);
  v_expediente               varchar2(4000);
  v_tipo                     varchar2(4000);
  v_subtipo                  varchar2(4000);
  v_asunto                   varchar2(4000);
  v_cuerpos                  varchar2(4000);
  v_fojas                    varchar2(4000);
  v_mesa_propietaria         varchar2(4000);
  v_origen_iniciador         varchar2(4000);
  v_tipo_iniciador           varchar2(4000);
  v_iniciador                varchar2(4000);
  v_iniciador_sexo           varchar2(4000);
  v_iniciador_nro_documento  varchar2(4000);
  v_iniciador_pai_cod_pais   varchar2(4000);
  v_iniciador_id_numero      varchar2(4000);
  v_iniciador_cuit           varchar2(4000);
  v_iniciador_unidad_suac    varchar2(4000);
  v_unidad_actual            varchar2(4000);
  v_usuario_actual           varchar2(4000);
  v_fecha_creacion           varchar2(4000);
  v_fecha_inicio             varchar2(4000);
  v_nro_tramite_origen       varchar2(4000);
  v_horas                    varchar2(4000);
  v_fecha_archivo            varchar2(4000);
  v_ubicacion                varchar2(4000);
  v_armario                  varchar2(4000);
  v_caja                     varchar2(4000);
  v_estante                  varchar2(4000);
  v_carpeta                  varchar2(4000);
  v_fecha_reactivacion       varchar2(4000);

  vrow sys_refcursor;

BEGIN
  /*Busca un tramite en suac*/
  nuevosuac.pck_interfaz_verticales.p_get_tramite(p_sticker, vrow);
  dbms_output.put_line('Rows: ' || vrow%ROWCOUNT);

  loop
    fetch vrow
      into v_id,
           v_nro_tramite,
           v_nro_sticker_completo,
           v_no_informatizado,
           v_estado,
           v_reparticion_caratuladora,
           v_expediente,
           v_tipo,
           v_subtipo,
           v_asunto,
           v_cuerpos,
           v_fojas,
           v_mesa_propietaria,
           v_origen_iniciador,
           v_tipo_iniciador,
           v_iniciador,
           v_iniciador_sexo,
           v_iniciador_nro_documento,
           v_iniciador_pai_cod_pais,
           v_iniciador_id_numero,
           v_iniciador_cuit,
           v_iniciador_unidad_suac,
           v_unidad_actual,
           v_usuario_actual,
           v_fecha_creacion,
           v_fecha_inicio,
           v_nro_tramite_origen,
           v_horas,
           v_fecha_archivo,
           v_ubicacion,
           v_armario,
           v_caja,
           v_estante,
           v_carpeta,
           v_fecha_reactivacion;
  
    exit when vrow%notfound;
    
    
  end loop;
  if not v_asunto is null then
    p_asunto := v_asunto;
  else
    p_asunto := ' ';
  end if;
  close vrow;

exception
  when NO_DATA_FOUND then
    p_asunto := ' ';
END;
/

