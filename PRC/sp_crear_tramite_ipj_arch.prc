CREATE OR REPLACE PROCEDURE IPJ.SP_Crear_Tramite_IPJ_Arch (
  o_rdo out varchar2, p_expediente in varchar2
 ) IS
  v_existe_arch number;
  v_row_archivo IPJ.T_ARCHIVO_TRAMITE%rowtype;
  v_existe_gest number;
  v_id_tramite_ipj number;
  v_tramite_suac NUEVOSUAC.VT_TRAMITES_IPJ_CRUCE_SGES%rowtype;
  v_Existe_real_Gest number;
  v_id_legajo number;     
BEGIN
  -- Busco si existe en Archivo y cuantos hay (para evitar duplicados)
  begin
    select * into v_row_archivo
    from ipj.t_archivo_tramite
    where
      expediente = p_expediente;
    
    v_existe_arch := 1;
  exception
    when NO_DATA_FOUND then
      v_existe_arch := 0;
    when OTHERS then
      v_existe_arch := 2;
  end;
    
  -- Busco si existe en Gesti�n y cuantos hay (para evitar duplicados)
  select max(id_tramite_ipj), count(1) into v_id_tramite_ipj, v_existe_gest
  from ipj.t_tramitesipj
  where
    expediente = p_expediente;
  
  -- Busco si existe en SUAC
  begin
    select * into v_tramite_suac
    from NUEVOSUAC.VT_TRAMITES_IPJ_CRUCE_SGES
    where
      nro_tramite = p_expediente;
   
    -- Busco si existe en Gesti�n y cuantos hay (para evitar duplicados)
    select count(1) into v_Existe_real_Gest
    from ipj.t_tramitesipj
    where
      id_tramite = v_tramite_suac.id_tramite;
  exception
    when NO_DATA_FOUND then 
      v_tramite_suac.id_tramite := 0;
      v_Existe_real_Gest := 0;
  end;
  
  -- Busco si tiene una empresa asociada, o existe la empresa en Gesti�n
  if nvl(v_row_archivo.id_legajo, 0) <> 0 then
    v_id_legajo := v_row_archivo.id_legajo; 
  else
    if nvl(v_tramite_suac.cuit_iniciador, '0') <> '0' then
      begin
        select id_legajo into v_id_legajo 
        from ipj.t_legajos
        where 
          cuit =  v_tramite_suac.cuit_iniciador;
      exception
        when OTHERS then
          v_id_legajo := null;
      end;
    end if;
    
    -- Si no la busque o encontr� por CUI, busco por nombre
    if v_id_legajo is null and v_tramite_suac.n_iniciador is not null then 
      begin
        select id_legajo into v_id_legajo 
        from ipj.t_legajos
        where 
          denominacion_sia =  v_tramite_suac.n_iniciador;
      exception
        when OTHERS then
          v_id_legajo := null;
      end;
    end if; 
  end if;
  
  o_rdo := 'Exp. ' || p_expediente;
  /* ***************************************************
    VERIFICO CASOS INV�LIDOS Y SALGO POR ERROR
  ****************************************************/
  if v_tramite_suac.id_tramite = 0 then
    o_rdo := o_rdo || ' - No se encuentra en SUAC.';
  end if;
  if v_existe_gest > 1 then
    o_rdo := o_rdo || ' - Existe m�s de 1 tr�mite en Gesti�n.';
  end if;
  if v_existe_arch <> 1 then
    o_rdo := o_rdo || ' - No existe 1 solo tr�mite en Archivo.';
  end if;
  if nvl(v_id_legajo, 0) = 0 then
    o_rdo := o_rdo || ' - No existe una empresa en Gesti�n.';
  end if;
  
  -- Si cargue alg�n error, no hago nada y salgo
  if  o_rdo <> 'Exp. ' || p_expediente then
    return;
  end if;
  
  /****************************************************
    ARMO LOS REGISTROS EN ARCHIVO Y GESTION
  ****************************************************/
  if v_existe_arch = 1 and v_Existe_real_Gest = 0 then
  
    -- Inserto el tr�mite en Gesti�n, archivado
    SELECT ipj.SEQ_tramites.nextval INTO v_Id_Tramite_Ipj FROM dual;

    insert into ipj.t_tramitesipj (Id_Tramite_Ipj, id_tramite, cuil_ult_estado, id_estado_ult, 
      fecha_inicio, observacion, id_ubicacion, id_ubicacion_origen, id_proceso, sticker, expediente, id_clasif_ipj)
    values ( 
      v_Id_Tramite_Ipj, --id_tramite_ipj 
      v_tramite_suac.id_tramite, -- Tramite SUAC 
      '20304742136', -- Ingelmo
      110, -- Cerrado OK 
      to_date(sysdate, 'dd/mm/rrrr'), 
      'Registrado por Sistemas', -- Observacion 
      8, -- Ubicacion actual Archivo 
      v_row_archivo.id_ubicacion, -- Ubicacion Origen 
      2,-- Proceso Normal 
      substr(v_tramite_suac.nro_sticker, 1, 9) || substr(v_tramite_suac.nro_sticker, 12, 14), -- Sticker 
      p_expediente, -- expediente
      9 -- Clasif ARCHIVO
    );
    
    -- **** INSERTO LA EMPRESA EN EL TR�MITE *****
    insert into IPJ.T_TRAMITESIPJ_PERSJUR (Id_Tramite_Ipj, id_legajo, id_sede, error_dato)
    values (v_Id_Tramite_Ipj, v_id_legajo, '00', 'N');
    
    -- Asocio el tr�mite de Gestion y SUAC en Archivo
    update IPJ.T_ARCHIVO_TRAMITE
    set 
      id_tramite_ipj = v_id_tramite_ipj,
      id_tramite =  v_tramite_suac.id_tramite
    where
      id_archivo_tramite = v_row_archivo.id_archivo_tramite; 
  end if;
  
  o_rdo := o_rdo || ' - OK.';
  
EXCEPTION
   WHEN OTHERS THEN
       o_rdo := 'Fallo el SP (exp ' || p_expediente || ') : ' || SQLERRM ||'--'|| SQLCODE;
       rollback;
END SP_Crear_Tramite_IPJ_Arch;
/

