create or replace force view ipj.vt_carga_legajo_areah as
select usuario,u.descripcion,to_date(fecha,'dd/mm/rrrr') Fecha, to_char(fecha,'HH24:MI:SS') hora, count(*) cantidad
  from (select t.usuario_alta usuario, t.fecha_alta fecha
          from ipj.t_legajos t
         where t.usuario_alta <> 'MIGRACION'
        union all
        select t.usuario_modif usuario, t.fecha_modificacion fecha
          from ipj.t_legajos t
         where t.usuario_modif <> 'MIGRACION'
          and t.fecha_alta <>
          t.fecha_modificacion
          and not t.fecha_alta is null
          )
         , ipj.t_usuarios u
   where  usuario=u.cuil_usuario
 group by usuario,u.descripcion, fecha;

