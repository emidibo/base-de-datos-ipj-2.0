create or replace force view ipj.v_entidades_ipj as
Select l.denominacion_sia Denominacion, l.cuit, te.tipo_entidad Tipo, l.fecha_baja_entidad Baja,
  ipj.ENTIDAD_PERSJUR.FC_ESTADO_GRAVAMEN_PERSJUR(l.id_legajo) Gravamen,
  ipj.ENTIDAD_PERSJUR.FC_ESTADO_GRAVAMEN_INTERV(l.id_legajo) Intervencion,
  (select max(a.fec_acta ) from ipj.t_entidades_acta a join ipj.t_tramitesipj tr on a.id_Tramite_ipj = tr.id_tramite_ipj where a.id_legajo = l.id_legajo and tr.id_estado_ult between 100 and 199) Ultima_Acta,
  (select ee.estado_entidad from ipj.t_entidades e join ipj.t_estados_entidades ee on e.id_estado_entidad = ee.id_estado_entidad where e.id_legajo = l.id_legajo and e.id_tramite_ipj = IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Tram(l.id_legajo, 'N')) Estado,
  (select e.fec_inscripcion from ipj.t_entidades e where e.id_legajo = l.id_legajo and e.id_tramite_ipj = IPJ.ENTIDAD_PERSJUR.FC_Buscar_Ult_Tram(l.id_legajo, 'N')) Inscipcion,
  (select d.n_departamento from dom_manager.vt_domicilios_cond d where d.id_vin = IPJ.ENTIDAD_PERSJUR.FC_BUSCAR_ULT_DOM(l.id_legajo, 'N')) Departamento,
  (select d.n_localidad from dom_manager.vt_domicilios_cond d where d.id_vin = IPJ.ENTIDAD_PERSJUR.FC_BUSCAR_ULT_DOM(l.id_legajo, 'N')) Localidad
from ipj.t_legajos l join ipj.t_tipos_entidades te
    on l.id_tipo_entidad = te.id_tipo_entidad
where
  nvl(eliminado, 0) <> 1 -- Entidades habilitadas
-- Que tengan 1 tr�mite de datos aprobado
  and exists (
    select *
    from ipj.t_entidades e join ipj.t_tramitesipj tr
    on e.id_tramite_ipj = tr.id_tramite_ipj
    where
    e.id_legajo = l.id_legajo and
    tr.id_estado_ult between 100 and 199
  )
;

