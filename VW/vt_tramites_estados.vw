CREATE OR REPLACE FORCE VIEW IPJ.VT_TRAMITES_ESTADOS AS
SELECT tIPJ.sticker,
            tIPJ.FECHA_INI_SUAC Fecha_Inicio,
            TO_DATE (GREATEST ( (SELECT MAX (fecha_pase)
                                   FROM ipj.t_tramites_ipj_estado
                                  WHERE id_tramite_ipj = tIPJ.id_tramite_ipj),
                               (SELECT MAX (fecha_pase)
                                  FROM ipj.t_tramitesipj_acciones_estado
                                 WHERE id_tramite_ipj = tIPJ.id_tramite_ipj)),
                     'dd/mm/rrrr')
               Fecha_Ultima,
            IPJ.TRAMITES_SUAC.FC_Buscar_Ult_Fec_SUAC (tIPJ.id_tramite)
               Fecha_Ultima_SUAC,
            u.n_ubicacion Area,
            INITCAP (
               IPJ.TRAMITES_SUAC.FC_Buscar_SubTipo_SUAC (tipj.id_tramite))
               Tramite,
            (SELECT DISTINCT
                    LISTAGG (ta.n_tipo_accion, '; ')
                       WITHIN GROUP (ORDER BY ta.n_tipo_accion)
                       OVER (PARTITION BY tra.id_tramite_ipj)
                       LISTA_NOMBRES
               FROM    ipj.t_tramitesipj_acciones tra
                    JOIN
                       ipj.t_tipos_Accionesipj ta
                    ON ta.id_tipo_accion = tra.id_tipo_accion
              WHERE id_tramite_ipj = tIPJ.id_tramite_ipj
                    AND tra.id_tipo_accion NOT IN (40, 92))
               Acciones,
            (CASE
                WHEN id_estado_ult < 100
                THEN
                   'Pendiente'
                WHEN id_estado_ult >= 100 AND id_estado_ult < 200
                THEN
                   'Completado'
                WHEN id_estado_ult >= 200
                THEN
                   'Rechazado / Cancelado'
             END)
               Estado,
            (CASE
                WHEN NVL (tIPJ.codigo_online, 0) = 0 THEN 'Presencial'
                ELSE 'On-Line'
             END)
               Modalidad
       FROM    ipj.t_tramitesIPJ tIPJ
            JOIN
               ipj.t_ubicaciones u
            ON u.id_ubicacion = tIPJ.id_ubicacion_origen
      WHERE     TIPJ.FECHA_INI_SUAC >= TO_DATE ('01/01/2017', 'dd/mm/rrrr')
            AND NVL (TIPJ.id_tramite, 0) > 0
            AND tIPJ.id_ubicacion_origen IN (1, 4, 5)
   ORDER BY n_ubicacion,
            fecha_ini_suac,
            tramite,
            Modalidad;

