CREATE OR REPLACE FORCE VIEW IPJ.VT_ADMIN_VIGENTES AS
SELECT e.cuit,
            e.denominacion_1 razon_social, TO_DATE (e.acta_contitutiva, 'dd/mm/rrrr') Fecha_Acta,
            (CASE
                WHEN i.cuil IS NOT NULL  THEN i.cuil
                ELSE
                   (SELECT cuil
                      FROM RCIVIL.VT_PK_PERSONA_CUIL vtP
                     WHERE  vtP.id_sexo = i.id_sexo
                           AND vtP.nro_documento = i.nro_documento
                           AND vtP.pai_cod_pais = i.pai_cod_pais
                           AND vtP.id_numero = i.id_numero)
             END) Cuil,
            i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero, i.detalle Nombre,
            o.n_tipo_organismo Organismo, ti.n_tipo_integrante cargo, ea.fecha_inicio,
            ea.fecha_fin, o.nro_orden Orden_Org, ti.nro_orden Orden_Cargo
       FROM ipj.t_tramitesipj tr  JOIN ipj.t_entidades e
               ON tr.id_tramite_ipj = e.id_tramite_ipj
            JOIN ipj.t_entidades_admin ea
               ON ea.id_tramite_ipj = e.id_tramite_ipj AND ea.id_legajo = e.id_legajo
            JOIN ipj.t_integrantes i
               ON ea.id_integrante = i.id_integrante
            JOIN ipj.t_tipos_integrante ti
               ON ea.id_tipo_integrante = ti.id_tipo_integrante
            LEFT JOIN ipj.t_tipos_organismo o
               ON ti.id_tipo_organismo = o.id_tipo_organismo
      WHERE
        tr.id_estado_ult BETWEEN 100 AND 199 AND  -- estados completos
        nvl(ea.fecha_fin, sysdate) >= to_date(SYSDATE, 'dd/mm/rrrr')
   ORDER BY e.denominacion_1, i.detalle
;

