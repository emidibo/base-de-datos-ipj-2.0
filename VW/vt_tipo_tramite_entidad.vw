CREATE OR REPLACE FORCE VIEW IPJ.VT_TIPO_TRAMITE_ENTIDAD AS
SELECT tt.id_tipo_tramite_ol, tt.n_tipo_tramite_ol, tr.id_tramite_ipj,eo.id_legajo
  FROM ipj.t_ol_entidades eo, ipj.t_tramitesipj tr, ipj.t_tipos_tramites_ol tt
 WHERE eo.codigo_online = tr.codigo_online
   AND eo.id_tipo_tramite_ol = tt.id_tipo_tramite_ol
   AND tr.id_estado_ult BETWEEN 100 AND 199;

