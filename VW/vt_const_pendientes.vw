create or replace force view ipj.vt_const_pendientes as
select v.nro_sticker, v.nro_tramite expedinete, v.fecha_inicio,
    nvl(E.DENOMINACION_1, l.denominacion_sia) Razon_Social,
    to_date(E.ACTA_CONTITUTIVA, 'dd/mm/rrrr') Fec_Acta_Const, l.id_legajo,
    nvl(e.cuit, l.cuit) cuit, nvl(e.anio, l.anio) anio
  from ipj.t_tramitesipj tr join nuevosuac.vt_tramites_ipj_desa v
      on tr.id_tramite = v.id_tramite
    join ipj.t_tramitesipj_persjur p
      on tr.id_tramite_ipj = p.id_tramite_ipj
    join ipj.t_legajos l
      on p.id_legajo = l.id_legajo
    left join ipj.t_entidades e
      on tr.id_tramite_ipj = e.id_tramite_ipj and p.id_legajo = e.id_legajo
  where
    tr.id_estado_ult < 100 and
    v.id_subtipo_tramite in
      ( select id_subtipo_tramite
        from nuevosuac.vt_subtipos_tramite st
        where
          -- #####  EN PRODUCCION VA ESTO  ######
          id_tipo_tramite in (8195, 8193, 8196, 8194) and
          n_subtipo_tramite like 'CONSTITUC%'
      )
;

