CREATE OR REPLACE FORCE VIEW IPJ.VT_EDICTOS_CANCELADOS_IPJ AS
SELECT e.nro_edicto, e.fecha_cancela
  FROM ipj.t_entidades_edicto_boe e
 WHERE e.id_estado_boe = 5
ORDER BY e.nro_edicto;

