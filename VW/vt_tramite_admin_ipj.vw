CREATE OR REPLACE FORCE VIEW IPJ.VT_TRAMITE_ADMIN_IPJ AS
SELECT IPJ.varios.FC_Habilitar_Botones (
             ub.id_ubicacion,
             tIPJ.id_clasif_ipj,
             0,
             tIPJ.id_estado_ult,
             IPJ.TRAMITES.FC_Acciones_Abiertas (tIPJ.id_tramite_ipj),
             0,
             1,
             tIPJ.id_proceso,
             tIPJ.simple_tramite,
             NVL (gu.CUIL_USUARIO, u.cuil_usuario),
             NVL (gu.CUIL_USUARIO, u.cuil_usuario),
             tIPJ.id_tramite_ipj,
             0)
             botones,
          tIPJ.Id_Tramite,
          tIPJ.id_tramite_ipj,
          tIPJ.id_clasif_ipj,
          tIPJ.Observacion,
          tIPJ.id_estado_ult Id_Estado,
          tIPJ.id_estado_ult id_estado_tramite,
          0 id_estado_accion,
          u.cuil_usuario,
          e.n_estado,
          tc.N_Clasif_Ipj,
          IPJ.TRAMITES.FC_Ultima_Fecha_Tramite (tIPJ.id_tramite_ipj)
             Fecha_Asignacion,
          IPJ.TRAMITES.FC_Acciones_Abiertas (tIPJ.id_tramite_ipj)
             Acc_Abiertas,
          IPJ.TRAMITES.FC_Acciones_Cerradas (tIPJ.id_tramite_ipj)
             Acc_Cerradas,
          IPJ.TRAMITES.FC_Acciones_Observadas (tIPJ.id_tramite_ipj)
             Acc_Observadas,
          TIPJ.URGENTE,
          NVL (u.descripcion, gr.n_grupo) usuario,
          ub.n_ubicacion,
          tIPJ.Cuil_Creador,
          ub.id_ubicacion,
          gr.id_grupo,
          tIPJ.id_proceso,
          gu.CUIL_USUARIO CUIL_USUARIO_grupo,
          TIPJ.sticker,
          TIPJ.EXPEDIENTE,
          TIPJ.SIMPLE_TRAMITE,
          tipj.codigo_online,
          tipj.id_tramite_ipj_padre,
          to_char(tipj.fecha_ini_suac, 'dd/mm/rrrr') fecha_ini_suac
     FROM IPJ.t_tramitesIPJ tIPJ
          JOIN IPJ.t_estados e
             ON tIPJ.id_estado_ult = e.id_estado
          LEFT JOIN IPJ.t_usuarios u
             ON tIPJ.Cuil_Ult_Estado = u.cuil_usuario
          LEFT JOIN IPJ.t_grupos_Usuario gu
             ON tIPJ.id_Grupo_Ult_Estado = gu.id_grupo
          LEFT JOIN IPJ.t_Grupos gr
             ON tIPJ.id_Grupo_Ult_Estado = gr.id_grupo
          LEFT JOIN IPJ.t_tipos_Clasif_ipj tc
             ON TIPJ.Id_Clasif_Ipj = tc.Id_Clasif_Ipj
          LEFT JOIN IPJ.T_Ubicaciones ub
             ON TIPJ.ID_UBICACION = ub.id_ubicacion
    WHERE NVL (tIPJ.id_estado_ult, 1) <> 110;

