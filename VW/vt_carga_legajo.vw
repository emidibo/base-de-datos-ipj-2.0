create or replace force view ipj.vt_carga_legajo as
select usuario,u.descripcion, to_date(fecha, 'dd/mm/rrrr')fecha, count(*) cantidad
  from (select t.usuario_alta usuario, t.fecha_alta fecha
          from ipj.t_legajos t
         where t.usuario_alta <> 'MIGRACION'
        union all
        select t.usuario_modif usuario, t.fecha_modificacion fecha
          from ipj.t_legajos t
         where t.usuario_modif <> 'MIGRACION'
          and to_date(t.fecha_alta, 'dd/mm/rrrr') <> to_date(t.fecha_modificacion, 'dd/mm/rrrr')
          )
         , ipj.t_usuarios u
   where  usuario=u.cuil_usuario
 group by usuario,u.descripcion, to_date(fecha, 'dd/mm/rrrr')
 order by 3,1;

