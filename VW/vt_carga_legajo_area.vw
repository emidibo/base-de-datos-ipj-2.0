create or replace force view ipj.vt_carga_legajo_area as
select t.descripcion, sum( t.cantidad) cantidad, u.n_ubicacion, t.fecha  from
ipj.vt_carga_legajo t,
ipj.t_grupos_trab_ubicacion g , ipj.t_ubicaciones u
where u.id_ubicacion= g.id_ubicacion and t.usuario=g.cuil and g.id_grupo_trabajo=4
and u.id_ubicacion not in (1,4,5)
group by t.descripcion, u.n_ubicacion  , t.fecha;

