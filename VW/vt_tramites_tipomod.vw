CREATE OR REPLACE FORCE VIEW IPJ.VT_TRAMITES_TIPOMOD AS
SELECT Area,
            Periodo,
            INITCAP (Tramite) Tramite,
            Modalidad,
            Pendientes,
            Completos,
            Rechazado,
            total
       FROM (  SELECT n_ubicacion Area,
                      fecha_ini Periodo,
                      Tramite,
                      Modalidad,
                      SUM ( (CASE WHEN id_estado_ult < 100 THEN 1 ELSE 0 END))
                         Pendientes,
                      SUM (
                         (CASE
                             WHEN id_estado_ult BETWEEN 100 AND 199 THEN 1
                             ELSE 0
                          END))
                         Completos,
                      SUM ( (CASE WHEN id_estado_ult > 199 THEN 1 ELSE 0 END))
                         Rechazado,
                      COUNT (*) total
                 FROM (SELECT TIPJ.FECHA_INI_SUAC Fecha,
                              TO_CHAR (TIPJ.FECHA_INI_SUAC, 'mm/rrrr') Fecha_Ini,
                              u.n_ubicacion,
                              tIPJ.id_estado_ult,
                              (CASE
                                  WHEN NVL (tIPJ.codigo_online, 0) = 0
                                  THEN
                                     'Presencial'
                                  ELSE
                                     'On-Line'
                               END)
                                 Modalidad,
                              IPJ.TRAMITES_SUAC.FC_Buscar_SubTipo_SUAC (
                                 tipj.id_tramite)
                                 Tramite
                         FROM    ipj.t_tramitesIPJ tIPJ
                              JOIN
                                 ipj.t_ubicaciones u
                              ON u.id_ubicacion = tIPJ.id_ubicacion_origen
                        WHERE TIPJ.FECHA_INI_SUAC >=
                                 TO_DATE ('01/01/2017', 'dd/mm/rrrr')
                              AND NVL (TIPJ.id_tramite, 0) > 0
                              AND tIPJ.id_ubicacion_origen IN (1, 4, 5)) tmp
             GROUP BY n_ubicacion,
                      fecha_ini,
                      Tramite,
                      Modalidad
             UNION
               SELECT --Area, Periodo, Tramite, Modalidad, Pendientes, Completos, Rechazado, total
                     u.n_ubicacion Area,
                      TO_CHAR (ol.fecha_creacion, 'mm/rrrr') Periodo,
                      'Consulta Homonimia' Tramite,
                      'On-Line' Modalidad,
                      0 Pendientes,
                      COUNT (1) Completos,
                      0 Rechazado,
                      COUNT (1) total
                 FROM ipj.t_ol_entidades ol
                      JOIN ipj.t_tipos_tramites_ol tr
                         ON ol.id_tipo_tramite_ol = tr.id_tipo_tramite_ol
                      JOIN ipj.t_ubicaciones u
                         ON u.id_ubicacion = tr.id_ubicacion
                WHERE ol.id_tipo_tramite_ol IN (14, 15, 16)
                      AND ol.fecha_creacion >=
                             TO_DATE ('01/01/2017', 'dd/mm/rrrr')
             GROUP BY n_ubicacion, TO_CHAR (ol.fecha_creacion, 'mm/rrrr')) t2
   ORDER BY Area,
            Periodo,
            Tramite,
            Modalidad
;

