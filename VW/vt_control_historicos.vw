CREATE OR REPLACE FORCE VIEW IPJ.VT_CONTROL_HISTORICOS AS
SELECT Usuario, cuil_usuario, Fecha, Turno, Inicio, Fin, Cantidad
    FROM
      (SELECT Usuario, cuil_usuario, Fecha, MIN (fecha_pase) Inicio, MAX (fecha_pase) Fin,
         COUNT (DISTINCT id_tramite_ipj) Cantidad, 'M' Turno
       FROM (SELECT U.DESCRIPCION Usuario, u.cuil_usuario,
                    TO_CHAR (tacc.fecha_pase, 'dd/mm/rrrr') Fecha,
                     tacc.fecha_pase, tacc.id_tramite_ipj
                  FROM ipj.t_tramitesipj_acciones_estado tacc
                    JOIN ipj.t_usuarios u
                      ON TACC.CUIL_USUARIO = u.cuil_usuario
                    JOIN ipj.t_tramitesipj t
                      ON tacc.id_tramite_ipj = t.id_tramite_ipj
                  WHERE T.ID_PROCESO = 5) t
       WHERE
         fecha_pase < to_date(TO_CHAR (fecha_pase, 'dd/mm/rrrr') || ' 02:00:00 PM', 'dd/mm/rrrr HH:MI:SS AM')
       GROUP BY Usuario, cuil_usuario,fecha

       UNION

       SELECT Usuario, cuil_usuario, Fecha, MIN (fecha_pase) Inicio, MAX (fecha_pase) Fin,
         COUNT (DISTINCT id_tramite_ipj) Cantidad, 'T' Turno
       FROM (SELECT U.DESCRIPCION Usuario, u.cuil_usuario,
                    TO_CHAR (tacc.fecha_pase, 'dd/mm/rrrr') Fecha,
                     tacc.fecha_pase, tacc.id_tramite_ipj
                  FROM ipj.t_tramitesipj_acciones_estado tacc
                    JOIN ipj.t_usuarios u
                      ON TACC.CUIL_USUARIO = u.cuil_usuario
                    JOIN ipj.t_tramitesipj t
                      ON tacc.id_tramite_ipj = t.id_tramite_ipj
                  WHERE T.ID_PROCESO = 5) t
       WHERE
         fecha_pase >= to_date(TO_CHAR (fecha_pase, 'dd/mm/rrrr') || ' 02:00:00 PM', 'dd/mm/rrrr HH:MI:SS AM')
       GROUP BY Usuario, cuil_usuario,fecha
      ) tt
    ORDER BY usuario, fecha;

