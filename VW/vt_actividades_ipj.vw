CREATE OR REPLACE FORCE VIEW IPJ.VT_ACTIVIDADES_IPJ AS
SELECT r.id_tramite_ipj, r.id_legajo, to_char(r.fecha_desde, 'dd/mm/rrrr') fecha_desde,
       to_char(r.fecha_hasta, 'dd/mm/rrrr') fecha_hasta, r.id_rubro, act.n_rubro,
       r.id_tipo_actividad, tact.n_tipo_actividad, r.id_actividad, act.n_actividad
  FROM ipj.t_entidades_rubros r, t_comunes.vt_actividades act, t_comunes.vt_tipos_actividad tact,
       t_comunes.vt_rubros ru, ipj.t_entidades e
 WHERE r.id_actividad = act.id_actividad
   AND r.id_rubro = act.id_rubro
   AND r.id_tipo_actividad = act.id_tipo_actividad
   AND tact.id_tipo_actividad = r.id_tipo_actividad
   AND act.id_rubro = tact.id_rubro
   AND ru.id_rubro = r.id_rubro
   AND r.id_tramite_ipj = e.id_tramite_ipj
   AND r.id_legajo = e.id_legajo
   AND (r.fecha_hasta IS NULL OR r.fecha_hasta > SYSDATE)
 ORDER BY r.nro_orden ASC, r.fecha_desde;

