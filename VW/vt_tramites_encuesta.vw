create or replace force view ipj.vt_tramites_encuesta as
select
  o.codigo_online, o.fecha_creacion, o.fecha_fin,
  (select n_tipo_tramite_ol from ipj.t_tipos_tramites_ol tol where tol.id_tipo_tramite_ol = o.id_tipo_tramite_ol) tipo_tramite,
  (select TOL.N_SUB_TRAMITE_OL from ipj.t_tipos_sub_tramites_ol tol where tol.id_sub_tramite_ol = o.id_sub_tramite_ol) subtipo_tramite,
  (select n_ubicacion from ipj.t_tipos_tramites_ol tol join ipj.t_ubicaciones u on tol.id_ubicacion = u.id_ubicacion where tol.id_tipo_tramite_ol = o.id_tipo_tramite_ol) area,
  (select denominacion_sia from ipj.t_legajos l where l.id_legajo = o.id_legajo) Entidad,
  tr.sticker, tr.expediente,
  (p.nombre || ' ' || p.apellido) usuario,
  ipj.varios.fc_buscar_telefono (p.id_sexo || p.nro_documento || p.pai_cod_pais || to_char(p.id_numero), null) telefono,
  ipj.varios.fc_buscar_mail (p.id_sexo || p.nro_documento || p.pai_cod_pais || to_char(p.id_numero), null) mail,
  o.cuil_usuario
from ipj.t_ol_entidades o join rcivil.vt_pk_persona p
    on o.cuil_usuario = p.cuil
  left join ipj.t_tramitesipj tr
    on o.codigo_online = tr.codigo_online
where
  o.id_estado > 1 and
  o.fecha_creacion between to_date(IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('FECHA_ENCUESTA_INI'), 'dd/mm/rrrr') and to_date(IPJ.VARIOS.FC_BUSCAR_VARIABLE_CONFIG('FECHA_ENCUESTA_FIN'), 'dd/mm/rrrr');

