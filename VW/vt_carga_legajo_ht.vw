create or replace force view ipj.vt_carga_legajo_ht as
select T.usuario, T.descripcion, T.fecha, U.TURNO, count(*) cantidad
from ipj.vt_carga_legajo_areah t, ipj.t_usuarios U
where t.usuario=U.CUIL_USUARIO
AND  U.TURNO IN ('T')
AND TO_DATE(T.fecha,'dd/mm/rrrr') > to_date ('19/07/2015','dd/mm/rrrr')
and to_number(SUBSTR(t.hora ,1,2))<14
GROUP BY T.usuario, T.descripcion, T.fecha, U.TURNO;

