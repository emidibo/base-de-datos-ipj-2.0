create or replace force view ipj.vt_socios_ipj as
select 
    l.id_legajo, 
    l.denominacion_sia Denominacion, 
    l.cuit, 
    (select min(fec_inscripcion) from ipj.t_entidades e where e.id_tramite_ipj = s.id_tramite_ipj and e.id_legajo = s.id_legajo) Fec_Inscr,
    (select min(to_date(acta_contitutiva, 'dd/mm/rrrr')) from ipj.t_entidades e where e.id_tramite_ipj = s.id_tramite_ipj and e.id_legajo = s.id_legajo) Acta_Const, 
    (case when te.id_ubicacion = 4 then 'Socio Fundador' else 'Socio' end) Cargo, 
    i.detalle Nombre, 
    i.cuil, 
    i.id_sexo, 
    i.nro_documento, 
    i.pai_cod_pais, 
    i.id_numero, 
    s.fecha_inicio, 
    s.fecha_fin
  from ipj.t_entidades_socios s join ipj.t_tramitesipj tr
      on tr.id_tramite_ipj = s.id_tramite_ipj
    join ipj.t_legajos l   
      on L.ID_LEGAJO = s.id_legajo
    join ipj.t_tipos_entidades te
      on l.id_tipo_entidad = te.id_tipo_entidad 
    join ipj.t_integrantes i
      on i.id_integrante = s.id_integrante
  where
    tr.id_estado_ult >= 100 and tr.id_estado_ult < 200 
  order by l.id_legajo, fecha_inicio, detalle;

