CREATE OR REPLACE FORCE VIEW IPJ.VT_TRAMITE_TECN_IPJ AS
SELECT IPJ.varios.FC_Habilitar_Botones (
             tIPJ.id_ubicacion,
             tIPJ.id_clasif_ipj,
             0,
             tIPJ.id_estado_ult,
             IPJ.TRAMITES.FC_Acciones_Abiertas (tIPJ.id_tramite_ipj),
             0,
             2,
             tIPJ.id_proceso,
             tIPJ.simple_tramite,
             NVL (gu.CUIL_USUARIO, tIpj.cuil_ult_estado),
             NVL (gu.CUIL_USUARIO, tIpj.cuil_ult_estado),
             tIPJ.id_tramite_ipj,
             0)
             botones,
          1 Clase,
          tIPJ.id_tramite,
          tIPJ.id_tramite_ipj,
          tIPJ.id_clasif_ipj,
          0 id_tipo_accion,
          tIPJ.Observacion,
          tIPJ.id_estado_ult id_estado,
          tIPJ.id_estado_ult id_estado_tramite,
          0 id_estado_accion,
          tIpj.cuil_ult_estado CUIL_USUARIO,
          (SELECT n_estado
             FROM IPJ.t_estados e
            WHERE tIPJ.id_estado_ult = e.id_estado)
             n_estado,
          (SELECT N_Clasif_Ipj
             FROM IPJ.t_tipos_Clasif_ipj tc
            WHERE TIPJ.Id_Clasif_Ipj = tc.Id_Clasif_Ipj)
             Tipo_IPJ,
          IPJ.TRAMITES.FC_Ultima_Fecha_Tramite (tIPJ.id_tramite_ipj)
             Fecha_Asignacion,
          IPJ.TRAMITES.FC_Acciones_Abiertas (tIPJ.id_tramite_ipj)
             Acc_Abiertas,
          IPJ.TRAMITES.FC_Acciones_Cerradas (tIPJ.id_tramite_ipj)
             Acc_Cerradas,
          TIPJ.URGENTE,
          NVL ( (SELECT descripcion
                   FROM ipj.t_usuarios u
                  WHERE u.cuil_usuario = tIpj.cuil_ult_estado),
               gr.n_grupo)
             usuario,
          'N' Error_Dato,
          '' cuit,
          '' razon_social,
          0 id_legajo,
          (SELECT n_ubicacion
             FROM ipj.t_ubicaciones u
            WHERE u.id_ubicacion = tIPJ.id_ubicacion)
             N_UBICACION,
          TIPJ.CUIL_CREADOR,
          tIPJ.id_ubicacion,
          0 id_tramiteipj_accion,
          0 id_protocolo,
          0 ID_INTEGRANTE,
          NULL NRO_DOCUMENTO,
          NULL Cuit_PersJur,
          '' Obs_Rubrica,
          '' identificacion,
          '' persona,
          NULL ID_FONDO_COMERCIO,
          tIPJ.id_proceso,
          0 id_pagina,
          0 Agrupable,
          gu.CUIL_USUARIO CUIL_USUARIO_grupo,
          tIPJ.id_Grupo_Ult_Estado,
          TIPJ.STICKER,
          TIPJ.EXPEDIENTE,
          NULL id_documento,
          NULL n_documento,
          TIPJ.SIMPLE_TRAMITE,
          (SELECT n_ubicacion
             FROM IPJ.T_Ubicaciones
            WHERE id_ubicacion = TIPJ.ID_UBICACION_ORIGEN)
             n_ubicacion_origen,
          tipj.codigo_online,
          tipj.id_tramite_ipj_padre,
          TO_CHAR (tipj.fecha_ini_suac, 'dd/mm/rrrr') fecha_ini_suac
     FROM IPJ.t_tramitesIPJ tIPJ
          LEFT JOIN IPJ.t_grupos_Usuario gu
             ON tIPJ.id_Grupo_Ult_Estado = gu.id_grupo
          LEFT JOIN IPJ.t_Grupos gr
             ON tIPJ.id_Grupo_Ult_Estado = gr.id_grupo
    WHERE NVL (tIPJ.id_estado_ult, 0) <> 110     --IPJ.TYPES.C_ESTADOS_CERRADO
   UNION ALL
   SELECT IPJ.varios.FC_Habilitar_Botones (TIPJ.id_ubicacion,
                                           tIPJ.id_clasif_ipj,
                                           AccIPJ.id_tipo_Accion,
                                           0,
                                           -1,
                                           AccIPJ.id_estado,
                                           2,
                                           tIPJ.id_proceso,
                                           tIPJ.simple_tramite,
                                           AccIPJ.cuil_usuario,
                                           AccIPJ.cuil_usuario,
                                           tIPJ.id_tramite_ipj,
                                           AccIPJ.id_tramiteipj_accion)
             botones,
          2 Clase,
          tIPJ.id_tramite,
          AccIPJ.id_tramite_ipj,
          (SELECT ta.id_clasif_ipj
             FROM ipj.t_tipos_AccionesIpj ta
            WHERE ACCIPJ.ID_TIPO_ACCION = ta.id_tipo_accion)
             id_clasif_ipj,
          AccIPJ.id_tipo_accion,
          AccIPJ.Observacion,
          AccIPJ.id_estado,
          0 id_estado_tramite,
          AccIPJ.id_estado id_estado_accion,
          AccIPJ.cuil_usuario,
          (SELECT n_estado
             FROM IPJ.t_estados e
            WHERE AccIPJ.id_estado = e.id_estado)
             n_estado,
          (SELECT ta.N_Tipo_Accion
             FROM ipj.t_tipos_AccionesIpj ta
            WHERE ACCIPJ.ID_TIPO_ACCION = ta.id_tipo_accion)
             Tipo_IPJ,
          IPJ.TRAMITES.FC_Ultima_Fecha_Tramite (AccIPJ.id_tramite_ipj)
             Fecha_Asignacion,
          0 Acc_Abiertas,
          0 Acc_Cerradas,
          tIPJ.URGENTE,
          (SELECT u.descripcion
             FROM IPJ.t_usuarios u
            WHERE AccIPJ.Cuil_Usuario = u.cuil_usuario)
             usuario,
          NVL (TRPJ.ERROR_DATO, trInt.Error_Dato) Error_Dato,
          (SELECT L.CUIT
             FROM IPJ.T_Legajos L
            WHERE ACCIPJ.id_legajo = L.id_legajo)
             cuit,
          (SELECT L.DENOMINACION_SIA
             FROM IPJ.T_Legajos L
            WHERE ACCIPJ.id_legajo = L.id_legajo)
             razon_social,
          ACCIPJ.id_legajo,
          (SELECT n_ubicacion
             FROM IPJ.T_Ubicaciones ub
            WHERE TIPJ.ID_UBICACION = ub.id_ubicacion)
             N_UBICACION,
          '' CUIL_CREADOR,
          TIPJ.id_ubicacion,
          AccIPJ.id_tramiteipj_accion,
          (SELECT NVL (ta.id_protocolo, 0)
             FROM ipj.t_tipos_AccionesIpj ta
            WHERE ACCIPJ.ID_TIPO_ACCION = ta.id_tipo_accion)
             id_protocolo,
          ACCIPJ.ID_INTEGRANTE,
          (SELECT I.NRO_DOCUMENTO
             FROM IPJ.T_INTEGRANTES i
            WHERE AccIPJ.ID_INTEGRANTE = i.ID_INTEGRANTE)
             nro_documento,
          (SELECT L.CUIT
             FROM IPJ.T_Legajos L
            WHERE ACCIPJ.id_legajo = L.id_legajo)
             Cuit_PersJur,
          AccIPJ.Obs_Rubrica,
          NVL ( (SELECT I.NRO_DOCUMENTO
                   FROM IPJ.T_INTEGRANTES i
                  WHERE AccIPJ.ID_INTEGRANTE = i.ID_INTEGRANTE),
               (SELECT L.CUIT
                  FROM IPJ.T_Legajos L
                 WHERE ACCIPJ.id_legajo = L.id_legajo))
             identificacion,
          NVL ( (SELECT fc.n_fondo_comercio
                   FROM IPJ.t_Fondos_comercio fc
                  WHERE AccIPJ.ID_FONDO_COMERCIO = FC.ID_FONDO_COMERCIO),
               NVL ( (SELECT I.DETALLE
                        FROM IPJ.T_INTEGRANTES i
                       WHERE AccIPJ.ID_INTEGRANTE = i.ID_INTEGRANTE),
                    (SELECT L.DENOMINACION_SIA
                       FROM IPJ.T_Legajos L
                      WHERE ACCIPJ.id_legajo = L.id_legajo)))
             persona,
          AccIPJ.ID_FONDO_COMERCIO,
          tIPJ.id_proceso,
          (SELECT ta.id_pagina
             FROM ipj.t_tipos_AccionesIpj ta
            WHERE ACCIPJ.ID_TIPO_ACCION = ta.id_tipo_accion)
             id_pagina,
          (SELECT agrupable
             FROM IPJ.t_paginas p
            WHERE p.id_pagina IN
                     (SELECT ta.id_pagina
                        FROM ipj.t_tipos_AccionesIpj ta
                       WHERE ACCIPJ.ID_TIPO_ACCION = ta.id_tipo_accion))
             AGRUPABLE,
          '' CUIL_USUARIO_grupo,
          0 id_Grupo_Ult_Estado,
          TIPJ.STICKER,
          TIPJ.EXPEDIENTE,
          AccIPJ.id_documento,
          AccIPJ.n_documento,
          TIPJ.SIMPLE_TRAMITE,
          (SELECT n_ubicacion
             FROM IPJ.T_Ubicaciones
            WHERE id_ubicacion = TIPJ.ID_UBICACION_ORIGEN)
             n_ubicacion_origen,
          tipj.codigo_online,
          tipj.id_tramite_ipj_padre,
          TO_CHAR (tipj.fecha_ini_suac, 'dd/mm/rrrr') fecha_ini_suac
     FROM ipj.t_tramitesIPJ tIPJ
          JOIN ipj.t_tramitesipj_acciones AccIPJ
             ON AccIPJ.Id_Tramite_IPJ = tIPJ.id_tramite_ipj
          LEFT JOIN ipj.T_TramitesIpj_PersJur TRPJ
             ON TIPJ.ID_TRAMITE_IPJ = TRPJ.ID_TRAMITE_IPJ
                AND ACCIPJ.id_legajo = TRPJ.id_legajo
          LEFT JOIN IPJ.T_TRAMITESIPJ_INTEGRANTE trInt
             ON ACCIPJ.ID_INTEGRANTE = TRINT.ID_INTEGRANTE
                AND AccIPJ.Id_tramite_ipj = trInt.id_tramite_ipj
    WHERE NVL (AccIPJ.Id_Estado, 0) <> 110
;

