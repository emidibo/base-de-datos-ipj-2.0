CREATE OR REPLACE FORCE VIEW IPJ.VT_ESTADO_TRAM_IPJ AS
SELECT tr.sticker,
          tr.id_tramite,
          tr.id_tramite_ipj,
          DECODE (NVL (tr.codigo_online, 0), 0, 'Presencial', 'On-Line')
             Modo_Ingreso,
          (SELECT o.fecha_fin
             FROM ipj.t_ol_entidades o
            WHERE o.codigo_online = tr.codigo_online)
             Fec_Fin_Portal,
          (SELECT TO_DATE (MIN (tp.fecha_pase), 'dd/mm/rrrr')
             FROM ipj.t_tramites_ipj_estado tp
            WHERE tp.id_tramite_ipj = tr.id_tramite_ipj AND nvl(tp.cuil_usuario, '1') <> '1')
             Fec_Ini_Area,
          (SELECT TO_DATE (MIN (tp.fecha_pase), 'dd/mm/rrrr')
             FROM ipj.t_tramites_ipj_estado tp
            WHERE tp.id_tramite_ipj = tr.id_tramite_ipj
                  AND id_estado IN (100, 200))
             Fec_Fin_Area,
          U.N_UBICACION Area,
          (SELECT ua.n_ubicacion
             FROM ipj.t_ubicaciones ua
            WHERE ua.id_ubicacion = tr.id_ubicacion)
             Area_Actual,
          (SELECT MIN (n.fec_modif)
             FROM ipj.t_entidades_notas n
            WHERE n.id_tramite_ipj = tr.id_tramite_ipj)
             Fec_Ini_Obs,
          (SELECT MAX (n.fec_cumpl)
             FROM ipj.t_entidades_notas n
            WHERE n.id_tramite_ipj = tr.id_tramite_ipj
                  AND N.fec_cumpl IS NOT NULL)
             Fec_Ult_Obs,
          (SELECT COUNT (1)
             FROM ipj.t_entidades_notas n
            WHERE n.id_tramite_ipj = tr.id_tramite_ipj
                  AND N.id_estado_nota NOT IN (1, 3))       -- OK o SIN EFECTO
             Cant_Obs_Pend,
          (SELECT TO_DATE (MIN (tp.fecha_pase), 'dd/mm/rrrr')
             FROM ipj.t_tramites_ipj_estado tp
            WHERE tp.id_tramite_ipj = tr.id_tramite_ipj
                  AND tp.cuil_usuario IN
                         (SELECT tu.cuil
                            FROM ipj.t_grupos_trab_ubicacion tu
                           WHERE tu.id_ubicacion = tr.id_ubicacion))
             Fec_Ini_Area_Actual,
          (SELECT TO_DATE (MIN (tap.fecha_pase), 'dd/mm/rrrr')
             FROM ipj.t_tramitesipj_acciones_estado tap
            WHERE tap.id_tramite_ipj = tr.id_tramite_ipj AND id_estado > 1
                  AND EXISTS
                         (SELECT ta.id_tipo_accion
                            FROM    ipj.t_tipos_accionesipj ta
                                 JOIN
                                    ipj.t_tipos_clasif_ipj tc
                                 ON ta.id_clasif_ipj = tc.id_clasif_ipj
                           WHERE ta.id_tipo_accion = tap.id_tipo_accion
                                 AND tc.id_ubicacion = tr.id_ubicacion))
             Fec_Ini_Est,
          e.n_estado
     FROM ipj.t_tramitesipj tr
          JOIN ipj.t_ubicaciones u
             ON tr.id_ubicacion_origen = u.id_ubicacion
          JOIN IPJ.T_ESTADOS e
             ON e.id_estado = tr.id_estado_ult
    WHERE NVL (tr.id_tramite, 0) > 0
          AND tr.fecha_ini_suac >= TO_DATE ('01/01/2017', 'dd/mm/rrrr')
--order by tr.id_tramite desc
;

