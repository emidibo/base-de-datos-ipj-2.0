create or replace force view ipj.vt_entidades_admin as
select
  -- Datos Tr�mite
  decode(TR.ID_PROCESO, 5, '--', tr.expediente) Expediente, decode(TR.ID_PROCESO, 5, '--', tr.sticker) Sticker,
  -- Datos de los Administradores
  ad.id_tramite_ipj, ad.id_legajo, ad.id_tipo_integrante, ad.id_integrante, i.id_Sexo, i.nro_documento, i.pai_cod_pais, i.id_numero,
  ad.fecha_inicio, ad.fecha_fin, ad.borrador, ad.id_vin, ad.id_tipo_organismo
  , ad.id_entidades_accion, ad.id_motivo_baja, ad.monto_garantia, ad.habilitado_present, ad.persona_expuesta, ad.id_moneda, ad.porc_garantia
  , ad.id_admin, ad.id_integrante_repres, ad.id_vin_empresa, ad.cuit_empresa, ad.n_empresa, ad.matricula, ad.fec_acta, ad.id_legajo_empresa
  , ad.folio, ad.anio, ad.id_tipo_entidad, ad.es_admin_afip
from ipj.t_entidades_admin ad join ipj.t_entidades e
    on ad.id_Tramite_ipj = e.id_tramite_ipj and ad.id_legajo = e.id_legajo
  join ipj.t_tramitesipj tr
    on e.id_tramite_ipj = tr.id_Tramite_ipj
  left join ipj.t_integrantes i
    on ad.id_integrante = i.id_integrante
where
  TR.ID_ESTADO_ULT between 100 and 199
  and nvl(e.borrador, 'S') = 'N'
;

