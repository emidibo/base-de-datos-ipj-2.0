create or replace force view ipj.vt_certif_gobierno_ipj as
select c.fecha, l.cuit, l.denominacion_sia,
  (select UINT_ACT from IPJ.vt_empleados emp where emp.nro_documento = to_char(to_number(substr(c.cuil_empleado, 3, length(c.cuil_empleado)-3))) and rownum = 1) UINT_ACT,
  (select OP_ACT from IPJ.vt_empleados emp where emp.nro_documento = to_char(to_number(substr(c.cuil_empleado, 3, length(c.cuil_empleado)-3))) and rownum = 1) OP_ACT,
  c.cuil_empleado,
  (select nov_nombre || ' ' || nov_apellido from RCIVIL.VT_PERSONAS_CUIL where nro_documento = to_char(to_number(substr(c.cuil_empleado, 3, length(c.cuil_empleado)-3))) and cuil = c.cuil_empleado and rownum = 1) nombre
from ipj.t_solicitud_certificado c join ipj.t_legajos l
    on c.id_legajo = l.id_legajo
order by c.fecha asc;

