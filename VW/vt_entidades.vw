create or replace force view ipj.vt_entidades as
select
  -- Datos Tr�mite
  decode(TR.ID_PROCESO, 5, '--', tr.expediente) Expediente, decode(TR.ID_PROCESO, 5, '--', tr.sticker) Sticker, IPJ.ENTIDAD_PERSJUR.FC_BUSCAR_PRIM_ACTA(e.id_tramite_ipj) Fecha_Acta,
  -- Datos Entidad
  e.cuit, e.id_moneda, e.matricula, e.fec_inscripcion, e.folio, e.libro_nro, e.tomo, e.anio, e.monto, e.acta_contitutiva, e.vigencia, e.cierre_ejercicio, e.fec_vig, e.id_vin_real
  , e.borrador, e.fec_acto, e.denominacion_1, e.alta_temporal, e.baja_temporal, e.tipo_vigencia, e.valor_cuota, e.matricula_version, e.cuotas, e.id_tipo_entidad, e.id_tramite_ipj
  , e.id_legajo, e.id_tipo_administracion, e.id_estado_entidad, e.obs_cierre, e.nro_resolucion, e.fec_resolucion, e.nro_boletin, e.sede_estatuto, e.requiere_sindico, e.origen
  , e.observacion, e.id_tipo_origen, e.cierre_fin_mes, e.objeto_social, e.fiscalizacion_ejerc, e.incluida_lgs, e.condicion_fideic, e.obs_fiduciario, e.obs_fiduciante
  , e.obs_beneficiario, e.obs_fideicomisario, e.fecha_versionado, e.sin_denominacion, e.contrato_privado, e.fec_vig_hasta, e.cant_fiduciario, e.cant_fiduciante
  , e.cant_beneficiario, e.cant_fideicomisario, e.id_obj_social, e.fec_estatuto, e.acredita_grupo, e.cant_fiduciario_otros, e.cant_fiduciante_otros, e.cant_beneficiario_otros,
  e.cant_fideicomisario_otros,
  (select d.n_provincia from dom_manager.vt_domicilios_cond d where d.id_vin = NVL(IPJ.ENTIDAD_PERSJUR.FC_BUSCAR_ULT_DOM(e.id_legajo, 'N'),e.id_vin_real)) Provincia,
  (select d.n_departamento from dom_manager.vt_domicilios_cond d where d.id_vin = NVL(IPJ.ENTIDAD_PERSJUR.FC_BUSCAR_ULT_DOM(e.id_legajo, 'N'),e.id_vin_real)) Departamento,
  (select d.n_localidad from dom_manager.vt_domicilios_cond d where d.id_vin = NVL(IPJ.ENTIDAD_PERSJUR.FC_BUSCAR_ULT_DOM(e.id_legajo, 'N'),e.id_vin_real)) Localidad,
  (select d.n_barrio from dom_manager.vt_domicilios_cond d where d.id_vin = NVL(IPJ.ENTIDAD_PERSJUR.FC_BUSCAR_ULT_DOM(e.id_legajo, 'N'),e.id_vin_real)) Barrio,
  (select d.n_calle from dom_manager.vt_domicilios_cond d where d.id_vin = NVL(IPJ.ENTIDAD_PERSJUR.FC_BUSCAR_ULT_DOM(e.id_legajo, 'N'),e.id_vin_real)) Calle,
  (select d.altura from dom_manager.vt_domicilios_cond d where d.id_vin = NVL(IPJ.ENTIDAD_PERSJUR.FC_BUSCAR_ULT_DOM(e.id_legajo, 'N'),e.id_vin_real)) Altura
from ipj.t_entidades e join ipj.t_tramitesipj tr
    on e.id_tramite_ipj = tr.id_Tramite_ipj
where
  TR.ID_ESTADO_ULT between 100 and 199
  and nvl(e.borrador, 'S') = 'N'
;

