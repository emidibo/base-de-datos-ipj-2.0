create or replace force view ipj.vt_entidades_socios as
select
  -- Datos Tr�mite
  decode(TR.ID_PROCESO, 5, '--', tr.expediente) Expediente, decode(TR.ID_PROCESO, 5, '--', tr.sticker) Sticker,
  -- Datos de los Socios
  s.id_tramite_ipj, s.id_legajo, s.id_tipo_integrante, s.id_integrante, i.id_sexo, i.nro_documento, i.pai_cod_pais, i.id_numero,
  s.cuota, s.fecha_inicio, s.fecha_fin, s.borrador, s.id_legajo_socio, s.cuit_empresa
  , s.n_empresa, s.cuota_compartida, s.id_entidad_socio, s.cant_votos, s.porc_capital, s.id_tipo_socio, s.en_formacion, s.matricula, s.fec_acta, s.id_tipo_entidad
  , s.certificacion_contable, s.matricula_certificante, s.id_modo_part, s.id_integrante_representante, s.id_tipo_repres, s.fecha_repres, s.folio, s.anio, s.id_vin_empresa
  , s.persona_expuesta
from ipj.t_entidades_socios s join ipj.t_entidades e
    on s.id_Tramite_ipj = e.id_tramite_ipj and s.id_legajo = e.id_legajo
  join ipj.t_tramitesipj tr
    on e.id_tramite_ipj = tr.id_Tramite_ipj
  left join ipj.t_integrantes i
    on s.id_integrante = i.id_integrante
where
  TR.ID_ESTADO_ULT between 100 and 199
  and nvl(e.borrador, 'S') = 'N'
;

