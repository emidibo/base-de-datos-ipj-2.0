create or replace force view ipj.vt_empleados as
select r.SEXO,
       r.NRO_DOCUMENTO,
       r.TIPO_DOCUMENTO,
       r.APELLIDO,
       r.NOMBRE,
       r.EMPRESA,
       r.MINISTERIO,
       r.UINT_ACT,
       r.OP_ACT,
       r.cba_cuil
from meta4.m4cba_vw_ruper_4q r;

