CREATE OR REPLACE FUNCTION IPJ.FC_Buscar_Ult_Dom_real_si(p_id_legajo in number)
  RETURN NUMBER is

  /*****************************************************************
    Este procedimiento devuelve el utlimo trámite y Accion de una entidad, aunque este en borrador.
  ******************************************************************/

  v_id_ubicacion_leg number;
  v_id_vin           number := null;

BEGIN

  -- Busco la ultima area de la entidad, para filtrar las busquedas históricas
  select nvl(id_ubicacion, 0)
    into v_id_ubicacion_leg
    from ipj.t_legajos l
    left join ipj.t_tipos_entidades te
      on l.id_tipo_entidad = te.id_tipo_entidad
   where l.id_legajo = p_id_legajo;

  -- Ordeno trámites para RP y SxA
  if v_id_ubicacion_leg in (ipj.types.c_area_SxA, ipj.types.c_area_SRL) then

    select (select id_vin_real
              from ipj.t_entidades e1
             where e1.id_tramite_ipj = v.id_tramite_ipj)
      into v_id_vin
      from (select e.Id_Tramite_Ipj

              from ipj.t_entidades e
              join ipj.t_tramitesipj tr
                on e.Id_Tramite_Ipj = tr.Id_Tramite_Ipj
              left join IPJ.t_TramitesIPJ_Acciones tacc
                on tacc.id_legajo = e.id_legajo
               and tacc.Id_Tramite_Ipj = e.Id_Tramite_Ipj
             where e.id_legajo = p_id_legajo
               and (nvl(tr.id_estado_ult, 0) < 200 or
                   nvl(tr.id_estado_ult, 0) = 210)
               and -- Tramite no rechazado
                   e.id_tipo_entidad in
                   (select id_tipo_entidad
                      from ipj.t_tipos_entidades
                     where id_ubicacion = v_id_ubicacion_leg)
               and -- para soporta cambios de SA a SRL
                   nvl(tacc.id_estado, 0) < IPJ.TYPES.C_ESTADOS_RECHAZADO
               and tacc.id_tipo_accion not in
                   (IPJ.Types.c_Tipo_Acc_Retiro,
                    IPJ.Types.c_Tipo_Acc_Archivar_Expediente)
               and tacc.id_tipo_accion not in
                   (select id_tipo_accion
                      from ipj.t_informes_tramite
                     where id_informe <> 18) -- Informe Caratula
             order by matricula asc,
                      matricula_version desc,
                      nvl(anio, 0) desc,
                      folio desc,
                      Id_Tramite_Ipj desc) v
     where rownum = 1;

  end if;

  -- Ordeno trámites para ACyF
  if v_id_ubicacion_leg in (ipj.types.c_area_CyF) then

    select (select id_vin_real
              from ipj.t_entidades e1
             where e1.id_tramite_ipj = v.id_tramite_ipj)
      into v_id_vin
      from (select e.Id_Tramite_Ipj,
                   IPJ.ENTIDAD_PERSJUR.FC_BUSCAR_PRIM_ACTA(tr.id_tramite_ipj) Fecha_Acta
              from ipj.t_entidades e
              join ipj.t_tramitesipj tr
                on e.Id_Tramite_Ipj = tr.Id_Tramite_Ipj
              left join IPJ.t_TramitesIPJ_Acciones tacc
                on tacc.id_legajo = e.id_legajo
               and tacc.Id_Tramite_Ipj = e.Id_Tramite_Ipj
             where e.id_legajo = p_id_legajo
               and (nvl(tr.id_estado_ult, 0) < 200 or
                   nvl(tr.id_estado_ult, 0) = 210)
               and -- Tramite no rechazado
                   e.id_tipo_entidad in
                   (select id_tipo_entidad
                      from ipj.t_tipos_entidades
                     where id_ubicacion = v_id_ubicacion_leg)
               and -- para soporta cambios de SA a SRL
                   nvl(tacc.id_estado, 0) < IPJ.TYPES.C_ESTADOS_RECHAZADO
               and tacc.id_tipo_accion not in
                   (IPJ.Types.c_Tipo_Acc_Retiro,
                    IPJ.Types.c_Tipo_Acc_Archivar_Expediente)
               and tacc.id_tipo_accion not in
                   (select id_tipo_accion
                      from ipj.t_informes_tramite
                     where id_informe <> 18) -- Informe Caratula
             order by Fecha_Acta desc, tacc.Id_Tramite_Ipj desc) v
     where rownum = 1;

  end if;

  return v_id_vin;

EXCEPTION

  when NO_DATA_FOUND then
    return null;

  When Others Then
    Return null;

END FC_Buscar_Ult_Dom_real_si;
/

